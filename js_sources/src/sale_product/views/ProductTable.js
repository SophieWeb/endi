/*
 * Module name : ProductTable
 */
import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';
import ProductView  from './ProductView.js';
import ProductEmptyView  from './ProductEmptyView.js';
import { sortCollection } from "../../tools";

const template = require('./templates/ProductTable.mustache');

const ProductCollectionView = Mn.CollectionView.extend({
    tagName: 'tbody',
    childView: ProductView,
    emptyView: ProductEmptyView,
    collectionEvents: {
        'sync': 'render'
    },
    // Bubble up child view events
    childViewTriggers: {
        'model:delete': 'model:delete',
        'model:duplicate': 'model:duplicate',
        'model:edit': 'model:edit'
    }
});

const ProductTable = Mn.View.extend({
    template: template,
    regions: {
        tbody: {el: 'tbody', replaceElement: true}
    },
    ui: {},
    // Listen to the current's view events
    events: {
        "click .sortable": "sortTable"
    },
    // Listen to child view events
    childViewEvents: {},
    // Bubble up child view events
    childViewTriggers: {
        'model:delete': 'model:delete',
        'model:duplicate': 'model:duplicate',
        'model:edit': 'model:edit'
    },
    initialize(){
        this.facade = Radio.channel('facade');
    },
    onBeforeRender(){
        sortCollection(this.collection, 'id', 'desc');
    },
    onRender(){
        this.showChildView('tbody', new ProductCollectionView({collection: this.collection}));
    },
    sortTable(e){
        e.preventDefault();

        const $e = $(e.currentTarget);
        const sortBy = $e.data('sort');
        const direction = $e.hasClass('asc') ? 'desc' : 'asc';
        const baseSvgFragment = '#sort-arrow';

        $(".sortable").each(function(){
            $(this).removeClass('asc desc');
            $(this).children('a').removeClass('asc desc current');
            const currentSvgFragment = $(this).find('use').attr('href').split('#');
            $(this).find('use').attr('href', `${currentSvgFragment[0]}${baseSvgFragment}`)
        });

        $e.addClass(`${direction} current`);
        $e.children('a').addClass(`${direction} current`);
        const currentSvgFragment = $e.find("use").attr("href").split("#");
        const nextSvgFragment = direction === "asc" ? "#sort-asc" : "#sort-desc";
        $e.find("use").attr("href", `${currentSvgFragment[0]}${nextSvgFragment}`) ;

        sortCollection(this.collection, sortBy, direction);
    }
});
export default ProductTable
