/*
 * Module name : ProductForm
 */
import Mn from 'backbone.marionette';
import Bb from 'backbone';
import Radio from 'backbone.radio';

import { scrollTop } from '../../tools.js';
import ActionButtonCollection from '../../base/models/ActionButtonCollection.js';
import ButtonCollectionWidget from '../../widgets/ButtonCollectionWidget.js';
import FormBehavior from '../../base/behaviors/FormBehavior.js';
import InputWidget from '../../widgets/InputWidget.js';
import TextAreaWidget from '../../widgets/TextAreaWidget.js';
import CheckboxWidget from '../../widgets/CheckboxWidget.js';
import DatePickerWidget from '../../widgets/DatePickerWidget.js';
import SelectWidget from '../../widgets/SelectWidget.js';
import TvaProductFormMixin from '../../base/views/TvaProductFormMixin.js';
import MessageView from '../../base/views/MessageView.js';
import ErrorView from '../../base/views/ErrorView.js';

import WorkItemComponent from './WorkItemComponent.js';
import StockOperationComponent from './StockOperationComponent.js';
import ProductResume from './ProductResume.js';


const ProductForm = Mn.View.extend(TvaProductFormMixin).extend({
    template: require('./templates/ProductForm.mustache'),
    behaviors: [FormBehavior],
    partial: true,
    className: 'main_content',
    regions: {
        resume: '.resume',
        messageContainer: '.message-container',
        errors: '.errors',
        type_: '.field-type_',
        label: '.field-label',
        title: ".field-title",
        description: '.field-description',
        ht: '.field-ht',
        unity: '.field-unity',
        tva_id: '.field-tva_id',
        product_id: '.field-product_id',
        supplier_id: '.field-supplier_id',
        supplier_ref: '.field-supplier_ref',
        supplier_unity_amount: '.field-supplier_unity_amount',
        supplier_ht: '.field-supplier_ht',
        stocks: '.stocks',
        items: '.items',
        category_id: '.field-category_id',
        ref: '.field-ref',
        general_overhead: '.field-general_overhead',
        margin_rate: '.field-margin_rate',
        notes: '.field-notes',
        // Training
        goals: ".field-goals",
        prerequisites: ".field-prerequisites",
        for_who: ".field-for_who",
        duration: ".field-duration",
        content: ".field-content",
        teaching_method: ".field-teaching_method",
        logistics_means: ".field-logistics_means",
        more_stuff: ".field-more_stuff",
        evaluation: ".field-evaluation",
        place: ".field-place",
        modality_one: ".field-modality_one",
        modality_two: ".field-modality_two",
        types: ".field-types",
        date: ".field-date",
        price: ".field-price",
        free_1: ".field-free_1",
        free_2: ".field-free_2",
        free_3: ".field-free_3",
        other_buttons: {el: '.other_buttons', replaceElement: true},
    },
    events: {
        /*'data:invalid': 'onDataInvalid',*/
    },
    modelEvents: {
        /*'updated:ht': 'renderHT',*/
        sync: 'renderHT',
        'change:tva_id': 'refreshTvaProductSelect',
        'change:type_': 'render',  // Si on change le type_ on veut (ou pas) les stocks
        'validated:invalid': 'showErrors',
        'validated:valid': 'hideErrors',
    },
    childViewEvents: {
        'action:clicked': 'onActionClicked',
    },
    childViewTriggers: {
        'finish': 'data:persist',
        'change': 'data:modified',
    },
    initialize(){
        this.config = Radio.channel('config');
        this.facade = Radio.channel('facade');
        this.app = Radio.channel('app');
        this.unity_options = this.config.request(
            'get:options',
            'unities'
        );
        this.tva_options = this.config.request(
            'get:options',
            'tvas'
        );
        this.product_options = this.config.request(
            'get:options',
            'products'
        );
        this.all_product_options = this.config.request(
            'get:options',
            'products'
        );
        this.supplier_options = this.config.request(
            'get:options',
            'suppliers'
        );
        this.category_options = this.facade.request(
            'get:collection',
            'categories'
        ).toJSON();
        this.work_form = false;
        this.training_form = false;

        // On traîte le cas des services dynamiquement car pour les types
        // simples on permet le changement de type à la volée
        if (this.model.get('type_') == 'sale_product_work'){
            this.work_form = true;
        } else if (this.model.get('type_') == 'sale_product_training'){
            this.training_form = true;
        }
        // Form should have no tva by default
        this.tva_options.forEach(function(item){
            item['default'] = false;
        });
        this.model.setupSyncEvents();
    },
    isServiceForm(){
        /* Permet de savoir si on doit afficher les stocks */
        return this.model.get('type_') == 'sale_product_service_delivery';
    },
    showMessageView(){
        var model = new Bb.Model();
        var view = new MessageView({model: model});
        this.showChildView('messageContainer', view);
    },
    renderHT(){
        if ((! this.work_form) && (!this.training_form)){
            let label = "Montant HT";
            let description = "NB : Si vous remplissez le champ 'Coût d’achat HT', le calcul grâce aux coefficients de marge et de frais généraux supplantera les modifications apportées ici";
            let editable = true;
            if (this.model.get('supplier_ht')){
                label = "Montant HT calculé";
                description = "Ce montant a été calculé depuis le montant 'Coût d’achat HT' grâce aux coefficients de marge et de frais généraux";
                editable = false;
            }
            this.showChildView('ht', new InputWidget(
                {
                    title: label,
                    field_name: 'ht',
                    value: this.model.get('ht'),
                    description: description,
                    editable: editable
                }
            ));
        }
    },
    showCommonFields(){
        if (this.work_form || this.training_form){
            this.showChildView(
                'type_',
                new InputWidget(
                    {
                        type: 'hidden',
                        field_name: 'type_',
                        value: this.model.get('type_')
                    }
                )
            );
        } else {
            this.showChildView(
                'type_',
                new SelectWidget(
                    {
                        field_name: "type_",
                        label: "Type de produit",
                        value: this.model.get('type_'),
                        options: this.config.request(
                            'get:options',
                            'base_product_types'
                        ),
                        label_key: 'label',
                        id_key: 'value',
                    }
                )
            );
        }
        this.showChildView(
            'description',
            new TextAreaWidget(
                {
                    title: "Description",
                    field_name: 'description',
                    value: this.model.get('description'),
                    tinymce: true,
                }
            ));
        this.showChildView(
            'unity',
            new SelectWidget(
                {
                    title: "Unité",
                    field_name: 'unity',
                    options: this.unity_options,
                    value: this.model.get('unity'),
                    defaultOption: {'value': '', 'label': 'Choisir une unité'}
                }
            )
        );
        this.showChildView(
            'tva_id',
            new SelectWidget(
                {
                    title: "TVA",
                    field_name: 'tva_id',
                    options: this.tva_options,
                    id_key: 'id',
                    value: this.model.get('tva_id'),
                    defaultOption: {'id': '', 'label': 'Choisir un taux de TVA'}
                }
            )
        );
        this.product_options = this.getProductOptions(
            this.tva_options,
            this.all_product_options,
        );
        this.showChildView(
            'product_id',
            new SelectWidget(
                {
                    title: "Compte produit",
                    field_name: 'product_id',
                    options: this.product_options,
                    id_key: 'id',
                    value: this.model.get('product_id'),
                    description: "Les comptes produits sont proposés après le choix de la TVA",
                    defaultOption: {'id': '', 'label': 'Choisir un compte produit'}
                }
            )
        );
        /* Section Informations internes */
        this.showChildView('label', new InputWidget(
            {
                title: "Nom interne",
                field_name: 'label',
                value: this.model.get('label'),
                description: "Nom du produit dans le catalogue",
                required: true,
            }
        ));
        this.showChildView(
            'category_id',
            new SelectWidget(
                {
                    title: "Catégorie",
                    field_name: 'category_id',
                    options: this.category_options,
                    id_key: 'id',
                    label_key: 'title',
                    value: this.model.get('category_id'),
                    defaultOption: {'id': '', 'title': 'Choisir une catégorie'}
                }
            )
        );
        this.showChildView(
            'ref',
            new InputWidget(
                {
                    title: "Référence interne",
                    field_name: 'ref',
                    value: this.model.get('ref'),
                }
            )
        );
        this.showChildView(
            'general_overhead',
            new InputWidget(
                {
                    title: "Coefficient de frais généraux",
                    field_name: 'general_overhead',
                    value: this.model.get('general_overhead'),
                    description: "Utilisé pour calculer le 'Prix de revient' depuis le coût d'achat ou 'Déboursé sec' selon la formule 'coût d'achat * (1 + Coefficient de frais généraux)'"
                }
            )
        );
        this.showChildView(
            'margin_rate',
            new InputWidget(
                {
                    title: "Coefficient de marge",
                    field_name: 'margin_rate',
                    value: this.model.get('margin_rate'),
                    description: "Utilisé pour calculer le 'Prix intermédiaire' depuis le 'Prix de revient' selon la formule 'prix de revient / (1 - Coefficient marge)'"
                }
            )
        );
        /* Section Notes */
        this.showChildView(
            'notes',
            new TextAreaWidget(
                {
                    title: "Notes",
                    field_name: 'notes',
                    value: this.model.get('notes')
                }
            )
        );
    },
    showWorkFields(){
        this.showChildView(
            'title',
            new InputWidget({
                label: "Titre du produit composé",
                description: "Titre du produit composé dans le document final",
                field_name: "title",
                value: this.model.get('title'),
                required: true,
            })
        );
        /* Section Produit composé (Ouvrage) */
        this.showChildView(
            'items',
            new WorkItemComponent({collection: this.model.items})
        );
    },
    showSupplierFields(){
        this.showChildView(
            'supplier_id',
            new SelectWidget(
                {
                    title: "Fournisseur",
                    field_name: 'supplier_id',
                    options: this.supplier_options,
                    id_key: 'id',
                    label_key: 'label',
                    value: this.model.get('supplier_id'),
                    defaultOption: {'id': '', 'label': 'Choisir un fournisseur'}
                }
            )
        );
        this.showChildView(
            'supplier_ref',
            new InputWidget(
                {
                    title: "Référence Fournisseur",
                    field_name: 'supplier_ref',
                    value: this.model.get('supplier_ref'),
                }
            )
        );
        this.showChildView(
            'supplier_unity_amount',
            new InputWidget(
                {
                    title: "Unité de vente Fournisseur",
                    field_name: 'supplier_unity_amount',
                    value: this.model.get('supplier_unity_amount'),
                }
            )
        );
        this.showChildView(
            'supplier_ht',
            new InputWidget(
                {
                    title: "Coût d’achat HT",
                    field_name: 'supplier_ht',
                    value: this.model.get('supplier_ht'),
                    description: "Déboursé sec du produit, utilisé pour calculer le HT grâce au coefficients de marge et de frais généraux"
                }
            )
        );
    },
    showStockFields(){
        this.showChildView(
            'stocks',
            new StockOperationComponent({collection: this.model.stock_operations})
        );
    },
    showTrainingFields(){
        let fields = {
            goals: {
                label: "Objectifs à atteindre à l'issue de la formation",
                description: "Les objectifs doivent être obligatoirement décrit avec des verbes d'actions",
                widget: TextAreaWidget
            },
            prerequisites: {
                'label': "Pré-requis obligatoire de la formation",
                widget: TextAreaWidget
            },
            for_who: {
                label: "Pour qui ?",
                description: "Public susceptible de participer à cette formation.",
                widget: TextAreaWidget
            },
            duration: {
                label: "Durée en heures et en jour(s) pour la formation",
                description: "Durée obligatoire minimale 7 heures soit 1 jour.",
                widget: InputWidget
            },
            content: {
                label: "Contenu détaillé de la formation",
                description: "Trame par étapes.",
                widget: TextAreaWidget
            },
            teaching_method: {
                label: "Les moyens pédagogiques utilisés",
                widget: TextAreaWidget
            },
            logistics_means: {
                label: "Les moyens logistiques à disposition",
                widget: TextAreaWidget
            },
            more_stuff: {
                label: "Quels sont les plus de cette formation ?",
                widget: TextAreaWidget
            },
            evaluation: {
                label: "Modalités d'évaluation de la formation",
                description: "Par exemple : questionnaire d'évaluation, exercices-tests, questionnaire de satisfaction, évaluation formative.",
                widget: TextAreaWidget
            },
            place: {
                label: "Lieu de la formation",
                description: "Villes, zones géographiques où la formation peut être mise en place.",
                widget: TextAreaWidget
            },
            modality_one: {
                label: "Formation intra-entreprise",
                widget: CheckboxWidget,
            },
            modality_two: {
                label: "Formation inter-entreprise",
                widget: CheckboxWidget,
            },
            types: {
                label: "Type de formation",
                options: this.config.request('get:options', 'training_types'),
                widget: SelectWidget,
                multiple: true,
                id_key: "id",
                label_key: "label"
            },
            date: {
                label: "Date de la formation",
                widget: DatePickerWidget,
            },
            free_1: {
                label: "Champ libre 1",
                widget: TextAreaWidget,
            },
            free_2: {
                label: "Champ libre 2",
                widget: TextAreaWidget,
            },
            free_3: {
                label: "Champ libre 3",
                widget: TextAreaWidget,
            }
        }

        let this_ = this;
        _.each(fields, function(field, key){
            let options = _.clone(field);
            options['value'] = this_.model.get(key);
            options['field_name'] = key;
            this_.showChildView(
                key,
                new field['widget'](options)
            );
        });
    },
    showOtherActionButtons(){
        let collection = new ActionButtonCollection();
        let buttons = [
            {
                label: 'Dupliquer',
                action: 'duplicate',
                icon: 'copy',
                showLabel: false
            }
        ];
        if (! this.model.get('locked')){
            buttons.push(
                {
                    label: "Supprimer",
                    action: "delete",
                    icon: "trash-alt",
                    showLabel: false,
                    css: 'negative'
                }
            );
        }
        collection.add(buttons);
        let view = new ButtonCollectionWidget({collection: collection});
        this.showChildView('other_buttons', view);
    },
    templateContext(){
        return {
            complex: this.work_form || this.training_form,
            work_form: this.work_form,
            service_form: this.isServiceForm(),
            training_form: this.training_form
        };
    },
    onRender(){
        this.showMessageView();
        this.showCommonFields();
        this.renderHT();
        if ((! this.work_form) && (!this.training_form)){
            this.showSupplierFields();
        }
        if (this.work_form || this.training_form){
            this.showWorkFields();
        }
        if (this.training_form){
            this.showTrainingFields();
        }
        // Le champ type_ peut changer en cours de route pour les produits
        // simples on utilise donc une méthode plutôt qu'un attribut
        if (!this.isServiceForm()){
            this.showStockFields();
        }
        this.showChildView(
            'resume',
            new ProductResume({model: this.model})
        );
        this.showOtherActionButtons();
    },
    onAttach(){
        scrollTop();
    },
    onActionClicked(actionName){
        console.log("Action clicked : %s", actionName);
        this.app.trigger("product:" + actionName, this);
    },
    onFormSubmitted(){
        this.app.trigger('navigate', 'index');
    },
    onCancelForm(){
        this.app.trigger('navigate', 'index');
    },
    onDataInvalid(model, errors){
        console.log("ProductForm.onInvalid");
        this.showChildView('errors', new ErrorView({errors: errors}));
    }
});
export default ProductForm
