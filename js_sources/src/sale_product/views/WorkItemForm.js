/*
 * Module name : WorkItemForm
 */
import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';

import { getOpt } from '../../tools.js';
import ModalFormBehavior from '../../base/behaviors/ModalFormBehavior.js';
import InputWidget from '../../widgets/InputWidget.js';
import CheckboxWidget from '../../widgets/CheckboxWidget.js';
import TextAreaWidget from '../../widgets/TextAreaWidget.js';
import SelectWidget from '../../widgets/SelectWidget.js';
import TvaProductFormMixin from '../../base/views/TvaProductFormMixin.js';
import CatalogComponent from '../../common/views/CatalogComponent.js';

const template = require('./templates/WorkItemForm.mustache');

const WorkItemForm = Mn.View.extend(TvaProductFormMixin).extend({
    template: template,
    tagName: 'section',
    id: 'workitem_form',
    partial: false,
    behaviors: [ModalFormBehavior],
    regions: {
        erros: ".errors",
        locked: '.field-locked',
        label: ".field-label",
        type_: '.field-type_',
        description: '.field-description',
        supplier_ht: '.field-supplier_ht',
        general_overhead: '.field-general_overhead',
        margin_rate: '.field-margin_rate',
        ht: '.field-ht',
        quantity: '.field-quantity',
        unity: '.field-unity',
        tva_id: '.field-tva_id',
        product_id: '.field-product_id',
        catalogContainer: "#catalog-container",
        sync_catalog: ".field-sync_catalog"
    },
    ui: {
        unlockButton: 'button.unlock',
        lockContainer: 'div.alert-locked',
        main_tab: "ul.nav-tabs li:first a",
    },
    // event_prefix: "work:item",
    // Listen to the current's view events
    events: {
        'change': 'updateTotalHt',
        'click @ui.unlockButton': 'unlockForm'
    },
    // Listen to child view events
    childViewEvents: {
        'catalog:insert': "onCatalogInsert",
        'finish': 'onDatasChanged'
    },
    // Bubble up child view events
    childViewTriggers: {
        'finish': 'data:modified'
    },
    modelEvents:{
        'change:tva_id': 'refreshTvaProductSelect',
    },
    initialize(){
        if (this.getOption('edit')){
            // Only partial edit needed
            this.partial = true;
        } else {
            // All model datas will be submitted
            this.partial = false;
        }
        this.config = Radio.channel('config');
        this.unity_options = this.config.request(
            'get:options',
            'unities'
        );
        this.tva_options = this.config.request(
            'get:options',
            'tvas'
        );
        this.product_options = this.config.request(
            'get:options',
            'products'
        );
        this.all_product_options = this.config.request(
            'get:options',
            'products'
        );
    },
    refreshForm(){
        // All the field are editable if not locked or no base_sale_product_id set
        this.showLocked();
        this.showLabelAndType();
        this.showDescription();
        this.showSupplierHt();
        this.showGeneralOverhead();
        this.showMarginRate();
        this.showHt();
        this.showQuantity();
        this.showUnity();
        this.showTva();
        this.showProduct();
        this.showSyncCatalog();
    },
    getFieldCommonOptions(attribute, base_label){
        let result = {
            label: base_label,
            field_name: attribute,
            editable: this.model.get(attribute + "_editable"),
            value: this.model.get(attribute)
        };
        if (this.model.isFromParent(attribute)){
            result.label += " hérité(e) du produit composé";
            result.editable = false
        } else if (this.model.isFromCatalog(attribute)){
            result.label += " hérité(e) du catalogue";
            result.editable = false;
        } else {
            result.editable = true;
        }
        return result;
    },
    showLocked(){
        this.showChildView(
        'locked',
            new InputWidget({
                type: 'hidden',
                field_name: 'locked',
                value: this.model.get('locked')
            })
        );
    },
    showLabelAndType(){
        if (! this.model.has('base_sale_product_id')){
            this.showChildView('label', new InputWidget(
                {
                    title: "Nom interne du produit (utilisé dans le catalogue)",
                    field_name: 'label',
                    value: this.model.get('label'),
                    description: "Après insertion dans le produit composé courant, ce produit sera également inséré dans votre catalogue produit",
                    required: true,
                }
            ));

            let type_options = this.config.request(
                'get:options', 'base_product_types'
            );

            this.showChildView('type_', new SelectWidget(
                {
                    title: "Type de produit (utilisé dans le catalogue)",
                    field_name: 'type_',
                    value: this.model.get('type_'),
                    options: type_options,
                    label_key: 'label',
                    id_key: 'value',
                    required: true,
                }
            ));
        } else {
            let view = this.getChildView('label')
            if (view){
                view.destroy();
                view = this.getChildView('type_');
                if (view){
                    view.destroy();
                }
                this.$el.find('div.alert-info').hide();
            }
        }
    },
    showDescription(){
        this.showChildView(
            'description',
            new TextAreaWidget(
                {
                    label: "Description",
                    description: "Description utilisée dans les devis/factures",
                    field_name: 'description',
                    value: this.model.get('description'),
                    tinymce: true,
                    required: true,
                }
            )
        );
    },
    showSupplierHt(){
        let options = this.getFieldCommonOptions('supplier_ht', 'Coût unitaire HT');
        let view = new InputWidget(options);
        this.showChildView('supplier_ht', view);
    },
    showGeneralOverhead(){
        let options = this.getFieldCommonOptions('general_overhead', "Coefficient de frais généraux");
        let view = new InputWidget(options);
        this.showChildView('general_overhead', view);
    },
    showMarginRate(){
        let options = this.getFieldCommonOptions('margin_rate', "Coefficient de marge");
        let view = new InputWidget(options);
        this.showChildView('margin_rate', view);
    },
    showHt(editable){
        let options = this.getFieldCommonOptions("ht", "Montant unitaire HT");

        let supplier_value = this.model.get('supplier_ht');

        if (editable === false){
            options.label += " sera calculé depuis le coût unitaire";
            options.editable = false;
        } else if (Boolean(supplier_value)){
            options.label += " calculé depuis le coût unitaire";
            options.editable = false;
        }

        let view = new InputWidget(options);
        this.showChildView('ht', view);
    },
    showQuantity(){
        this.showChildView(
            'quantity',
            new InputWidget(
                {
                    label: "Quantité",
                    field_name: "quantity",
                    value: this.model.get('quantity')
                }
            )
        );
    },
    showUnity(){
        let options = this.getFieldCommonOptions('unity', "Unité");

        options['options'] = this.unity_options;
        options['defaultOption'] = {value: '', label: 'Choisir une unité'};

        let view = new SelectWidget(options);
        this.showChildView('unity', view);
    },
    showTva(){
        let options = this.getFieldCommonOptions('tva_id', 'TVA');

        // NB : ici les tva_options sont différentes de celles utilisées dans
        // le reset de la view car elles sont modifiées par le SelectWidget et
        // on perd donc l'info de la tva par défaut
        const tva_options = this.config.request(
            'get:options',
            'tvas'
        );
        options.options = tva_options;
        options.defaultOption = {id: '', label: 'Choisir un taux de TVA'};

        options.id_key = 'id';
        let view = new SelectWidget(options);
        this.showChildView('tva_id', view);
    },
    showProduct(){
        this.product_options = this.getProductOptions(
            this.tva_options,
            this.all_product_options,
            this.model.get('tva_id')
        );

        let options = this.getFieldCommonOptions('product_id', "Compte produit");
        options.options = this.product_options;
        options.id_key = 'id';
        options.defaultOption = {id: '', label: 'Choisir un compte produit'};
        let view = new SelectWidget(options);
        this.showChildView('product_id', view);
    },
    showSyncCatalog(open_form){
        if (this.model.get('base_sale_product_id')){
            this.showChildView(
                'sync_catalog',
                new CheckboxWidget({
                    label: "Synchroniser avec le catalogue",
                    description: "Appliquer les modifications ci-dessus au produit " + this.model.get('label') + " du catalogue",
                    field_name: "sync_catalog",
                    value: false,
                    true_val: true,
                    false_val: false,
                })
            )
        }
    },
    templateContext(){
        let title = "Ajouter un produit à ce produit composé"
        let raw_create = false;
        let help_text = "";

        if (this.getOption('edit')){
            title = "Modifier le produit " + this.model.get('label') + " de ce produit composé";
        }
        if (!this.model.get('base_sale_product_id')){
            help_text = "Lorsque vous ajoutez une entrée directement dans ce produit composé, un produit correspondant sera également créé dans le catalogue";
            raw_create = true;
        }
        return {
            title: title, help_text: help_text, raw_create: raw_create,
            add: !this.getOption('edit'),
            parent_margin_rate: this.parent_margin_rate,
            parent_general_overhead: this.parent_general_overhead
        };
    },
    onRender(){
        this.refreshForm();
        if (! this.getOption('edit')){
            const view = new CatalogComponent(
                {
                    query_params: {type_: 'product'},
                    collection_name: 'catalog_tree',
                    multiple: true
                }
            );
            this.showChildView('catalogContainer', view);
        }
    },
    unlockForm(){
        this.model.set('locked', false);
        this.ui.lockContainer.hide();
        this.refreshForm();
    },
    onDatasChanged(attribute, value){
       if (attribute == 'supplier_ht'){
           this.model.set('supplier_ht', value);
           this.refreshForm();
       }
    },
    onAttach(){
        this.getUI('main_tab').tab('show');
    },
    onCatalogInsert(sale_products){
        const collection = this.getOption("destCollection");
        let req = collection.load_from_catalog(sale_products);
        req.then(() => this.triggerMethod('modal:close'));
    },
});
export default WorkItemForm;
