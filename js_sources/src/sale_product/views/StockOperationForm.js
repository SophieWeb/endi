/*
 * Module name : StockOperationForm
 */
import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';

import { getOpt } from '../../tools.js';
import ModalFormBehavior from '../../base/behaviors/ModalFormBehavior.js';
import DatePickerWidget from '../../widgets/DatePickerWidget.js';
import TextAreaWidget from '../../widgets/TextAreaWidget.js';
import InputWidget from '../../widgets/InputWidget.js';

const template = require('./templates/StockOperationForm.mustache');

const StockOperationForm = Mn.View.extend({
    template: template,
    tagName: 'section',
    id: 'stock_operation_form',
    behaviors: [ModalFormBehavior],
    partial: false,
    regions: {
        erros: ".errors",
        date: ".field-date",
        description: '.field-description',
        stock_variation: '.field-stock_variation',
        base_sale_product_id: '.field-base_sale_product_id',
    },
    ui: {
    },
    // event_prefix: "stock:operation",
    // Listen to the current's view events
    events: {
    },
    // Listen to child view events
    childViewEvents: {},
    // Bubble up child view events
    childViewTriggers: {
        'finish': 'data:modified'
    },
    initialize(){
        this.config = Radio.channel('config');
        this.bb_sync = this.getOption('bb_sync');
        this.forceEditForm = getOpt(this, 'edit', false);
    },
    onRender(){
        if (! this.model.has('base_sale_product_id')){
        }
        this.showChildView('date', new DatePickerWidget(
            {
                title: "Date",
                field_name: 'date',
                value: this.model.get('date')
            }
        ));
        this.showChildView(
            'description',
            new TextAreaWidget(
                {
                    label: "Description",
                    field_name: 'description',
                    value: this.model.get('description')
                }
            )
        );
        this.showChildView(
            'stock_variation',
            new InputWidget(
                {
                    label: "Variation du stock",
                    field_name: 'stock_variation',
                    value: this.model.get('stock_variation'),
                    description: "Quantité entrée ou sortie du stock (en négatif pour une sortie)"
                }
            )
        );
    },
    templateContext(){
        let title = "Nouveau mouvement de stock";
        if (this.getOption('edit')){
            title = "Modifier le mouvement de stock";
        }
        return {title: title};
    },
});
export default StockOperationForm;
