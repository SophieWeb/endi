/*
 * File Name : BaseProductModel.js
 *
 */
import BaseModel from "../../base/models/BaseModel.js";
import Radio from 'backbone.radio';
import { formatAmount, strToFloat, round } from '../../math.js';
import DuplicableMixin from '../../base/models/DuplicableMixin.js';
import WorkItemModel from './WorkItemModel.js';
import StockOperationCollection from './StockOperationCollection.js';


const BaseProductModel = BaseModel.extend(DuplicableMixin).extend({
    props: [
        'id',
        'type_',
        'label',
        'description',
        'ht',
        'unity',
        'tva_id',
        'product_id',
        'supplier_id',
        'supplier_ref',
        'supplier_unity_amount',
        'supplier_ht',
        'purchase_type_id',
        'category_id',
        'category_label',
        'ref',
        'general_overhead',
        'margin_rate',
        'current_stock',
        'stock_operations',
        'notes',
        "locked",
    ],
    validation: {
        'label': {required: true, msg: "Veuillez saisir un nom"},
         ht: {
            required: false,
            pattern: "amount",
            msg: "Veuillez saisir un coût unitaire, dans la limite de 5 chiffres après la virgule",
        },
        supplier_ht: {
            required: false,
            pattern: "amount",
            msg: "Veuillez saisir un coût d'achat, dans la limite de 5 chiffres après la virgule",
        },
		general_overhead: [
            {
                required: false,
                pattern: 'amount',
                msg: "Le coefficient de frais généraux doit être un nombre, dans la limite de 5 chiffres après la virgule"
            },
            {
                range: [0, 9],
                msg: "Doit être compris entre 0 et 9"
            },
        ],
        margin_rate: [
            {
                required: false,
                pattern: 'amount',
                msg: "Le coefficient de marge doit être un nombre, dans la limite de 5 chiffres après la virgule"
            },
            {
                range: [0, 9],
                msg: "Doit être compris entre 0 et 9"
            }
        ]
    },
    icons: {
        sale_product_product: 'box',
        sale_product_material: 'box',
        sale_product_composite: 'product-composite',
        sale_product_training: 'chalkboard-teacher',
        sale_product_work_force: "user",
        sale_product_service_delivery: "hands-helping",
    },
    initialize: function(){
        BaseProductModel.__super__.initialize.apply(this, arguments);
        this.facade = Radio.channel('facade');
        this.config = Radio.channel('config');
        this.tva_options = this.config.request(
            'get:options',
            'tvas'
        );
        this.supplier_options = this.config.request(
            'get:options',
            'suppliers'
        );
        this.is_complex = false;
        this.populate_stock_operations();
    },
    setupSyncEvents(){
        console.log("BaseProduct.onSetId");
        this.stopListening(this.stock_operations);
        this.listenTo(this.stock_operations, 'add', this.syncStockOperations);
        this.listenTo(this.stock_operations, 'sync', this.syncStockOperations);
        this.listenTo(this.stock_operations, 'remove', this.syncStockOperations);
        this.listenTo(this.stock_operations, 'change', this.syncStockOperations);
    },
    populate_stock_operations(){
        if (this.has('stock_operations')){
            this.stock_operations = new StockOperationCollection(this.get('stock_operations'));
        } else {
            this.stock_operations = new StockOperationCollection([]);
        }
        var this_ = this;
        this.stock_operations.url = function(){
            return this_.url() + '/' + "stock_operations";
        }
    },
    syncStockOperations(){
        this.set('stock_operations', this.stock_operations.toJSON());
    },
    category_label(){
        return this.get('category_label');
    },
    tva_label(){
        return this.findLabelFromId('tva_id', 'label', this.tva_options);
    },
    ht_label(){
        return formatAmount(this.get('ht'), false, false);
    },
    supplier_ht_label(){
        return formatAmount(this.get('supplier_ht'), false, false);
    },
    supplier_label(){
        return this.findLabelFromId('supplier_id', 'label', this.supplier_options);
    },
    matchPattern(search){
        if (
            (this.get('label').indexOf(search) !== -1) ||
            (this.get('description').indexOf(search) !== -1) ||
            (this.category_label().indexOf(search) !== -1)
            ) {
            return true;
        }
        return false;
    },
    asWorkItem(){
        return new WorkItemModel({
            description: this.get('description'),
            type_: this.get('type_'),
            ht: this.get('supplier_ht'),
            unity: this.get('unity'),
            tva_id: this.get("tva_id"),
            product_id: this.get("product_id"),
            general_overhead: this.get('general_overhead'),
            margin_rate: this.get('margin_rate'),
            base_sale_product_id: this.get('id'),
            label: this.get('label'),
        });
    },
    getIcon(){
        if (_.has(this.icons, this.get('type_'))){
            return this.icons[this.get('type_')];
        } else {
            return false;
        }
    }
});
export default BaseProductModel;
