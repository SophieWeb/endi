import Mn from 'backbone.marionette';
import Bb from 'backbone';
import Radio from 'backbone.radio';

import StatusView from '../../common/views/StatusView.js';
import SupplierInvoiceLineModel from '../models/SupplierInvoiceLineModel.js';
import SupplierInvoiceLineTableView from './SupplierInvoiceLineTableView.js';
import SupplierInvoiceLineFormPopupView from './SupplierInvoiceLineFormPopupView.js';
import SupplierInvoiceFormView from './SupplierInvoiceFormView.js';
import SupplierInvoiceLineDuplicateFormView from './SupplierInvoiceLineDuplicateFormView.js';
import TotalView from './TotalView.js';
import MessageView from '../../base/views/MessageView.js';
import LoginView from '../../base/views/LoginView.js';
import NodeFileCollectionView from '../../common/views/NodeFileCollectionView.js';
import {displayServerSuccess, displayServerError} from '../../backbone-tools.js';
import { showLoader, hideLoader } from '../../tools.js';
import ErrorView from '../../base/views/ErrorView.js';


const MainView = Mn.View.extend({
    className: 'container-fluid page-content',
    template: require('./templates/MainView.mustache'),
    regions: {
        modalRegion: '.modalRegion',
        supplierInvoiceForm: '.supplier-invoice',
        linesRegion: '.lines-region',
        files: '.files',
        totals: '.totals',
        messages: {
            el: '.messages-container',
            replaceElement: true,
        },
        errors: '.group-errors',
    },
    childViewEvents: {
        'line:add': 'onLineAdd',
        'line:edit': 'onLineEdit',
        'line:delete': 'onLineDelete',
        'invoice:modified': 'onDataModified',
        'line:duplicate': 'onLineDuplicate',
        'status:change': 'onStatusChange',
    },
    onDataModified(name, value) {
        if (name == 'supplier_id') {
            this.onSupplierModified(value);
        }
        let totals = this.facade.request('get:model', 'total');
        let ttc = totals.get('ttc');
    },
    onSupplierModified(supplier_id) {
        /* jQuery hack to update supplier static part
         * defined in supplier_invoice.mako
         */
        let suppliers = this.config.request('get:options', 'suppliers');
        let supplier = _.find(suppliers, x => x.value == supplier_id);

        let elA = $("[data-backbone-var=supplier_id]");
        elA.text(supplier.label);
        elA.attr('href', `/suppliers/${supplier_id}`);
    },
    initialize(){
        this.facade = Radio.channel('facade');
        this.config = Radio.channel('config');
        
        this.listenTo(this.facade, 'status:change', this.onStatusChange);
    },
    showSupplierInvoiceForm(){
        let edit = this.config.request('get:form_section', 'general')['edit'];
        var model = this.facade.request('get:model', 'supplierInvoice');
        var view = new SupplierInvoiceFormView({
            model:model,
            edit: edit,
        });

        this.showChildView('supplierInvoiceForm', view);
    },
    onLineAdd(childView){
        var model = new SupplierInvoiceLineModel({});
        this.showLineForm(model, true, "Ajouter une ligne");
    },
    onLineEdit(childView){
        this.showLineForm(childView.model, false, "Modifier une ligne");
    },
    showLineForm(model, add, title){
        var view = new SupplierInvoiceLineFormPopupView({
            title: title,
            add:add,
            model:model,
            destCollection: this.facade.request('get:collection', 'lines'),
        });
        this.showChildView('modalRegion', view);
    },
    showDuplicateForm(model){
        var view = new SupplierInvoiceLineDuplicateFormView({model: model});
        this.showChildView('modalRegion', view);
    },
    onLineDuplicate(childView){
        this.showDuplicateForm(childView.model);
    },
    onDeleteSuccess: function(){
        displayServerSuccess("Vos données ont bien été supprimées");
    },
    onDeleteError: function(){
        displayServerError("Une erreur a été rencontrée lors de la " +
                            "suppression de cet élément");
    },
    onLineDelete: function(childView){
        var result = window.confirm("Êtes-vous sûr de vouloir supprimer cette ligne ?");
        if (result){
            childView.model.destroy(
                {
                    success: this.onDeleteSuccess,
                    error: this.onDeleteError
                }
            );
        }
     },
    showLinesRegion(){
        const section = this.config.request('get:form_section', 'lines')
        var collection = this.facade.request(
            'get:collection',
            'lines'
        );
        var view = new SupplierInvoiceLineTableView(
            {
                collection: collection,
                section: section
            }
        );
        this.showChildView('linesRegion', view);
    },
    showFilesRegion(){
        let collection = this.facade.request(
            'get:collection',
            'attachments'
        );
        let view = new NodeFileCollectionView(
            {
                collection: collection,
                edit: this.edit
            }
        )
        this.showChildView('files', view);
    },
    showMessages(){
        var model = new Bb.Model();
        var view = new MessageView({model: model});
        this.showChildView('messages', view);
    },
    showTotals(){
        let model = this.facade.request('get:model', 'total');
        var view = new TotalView({model: model});
        this.showChildView('totals', view);
    },
    showLogin: function(){
        var view = new LoginView({});
        this.showChildView('modalRegion', view);
    },
    onRender(){
        this.showFilesRegion();
        this.showSupplierInvoiceForm();
        this.showLinesRegion();
        this.showTotals();
        this.showMessages();
    },
    _showStatusModal(action_model) {
        var view = new StatusView({action: action_model});
        this.showChildView('modalRegion', view);
    },

    formOk(){
        var result = true;
        var errors = this.facade.request('is:valid');
        if (!_.isEmpty(errors)){
            console.log(errors);
            this.showChildView(
                'errors',
                new ErrorView({errors:errors})
            );
            result = false;
        } else {
            this.detachChildView('errors');
        }
        return result;
    },
    onStatusChange(action_model){
        if (this.config.request('get:form_section', 'general')['edit']) {
            if (! action_model.get('status')){
                return;
            }
            showLoader();
    
            if (action_model.get('status') != 'draft'){
                if (! this.formOk()){
                    document.body.scrollTop = document.documentElement.scrollTop = 0;
                    hideLoader();
                    return;
                }
            }
            // Prior to any status change, we want to save and make sure it went OK
            
            this.facade.request('save:all').then(
                () => {hideLoader(); this._showStatusModal(action_model)},
                () => {hideLoader(); displayServerError("Erreur pendant la sauvegarde")}
            );
        } else {
            this._showStatusModal(action_model);
        }
    },
});
export default MainView;
