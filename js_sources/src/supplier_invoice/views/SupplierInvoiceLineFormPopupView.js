import Mn from 'backbone.marionette';
import ModalBehavior from '../../base/behaviors/ModalBehavior.js';
import SupplierInvoiceLineFormView from './SupplierInvoiceLineFormView.js';
import Radio from 'backbone.radio';

const SupplierInvoiceLineFormPopupView = Mn.View.extend({
    behaviors: [ModalBehavior],
    id: "supplierorderline-form-popup-modal",
    template: require('./templates/SupplierInvoiceLineFormPopupView.mustache'),
    regions: {
        main: '#mainform-container',
    },
    ui: {
        main_tab: 'ul.nav-tabs li.main a',
    },
    childViewEvents: {
        'success:sync': 'onSuccessSync',
    },
    // Here we bind the child FormBehavior with our ModalBehavior
    // Like it's done in the ModalFormBehavior
    childViewTriggers: {
        'cancel:form': 'modal:close',
    },
    initialize(){
        var facade = Radio.channel('facade');
        this.add = this.getOption('add');
    },
    onSuccessSync(){
        if (this.add){
            this.triggerMethod('modal:notifySuccess');
        } else {
            this.triggerMethod('modal:close');
        }
    },
    onModalAfterNotifySuccess(){
        this.triggerMethod('line:add');
    },
    onModalBeforeClose(){
        this.model.rollback();
    },
    refreshForm(){
        this.showMainForm();
        this.getUI('main_tab').tab('show');
    },
    showMainForm(){
        if ((!this.tel) || this.add){
            var view = new SupplierInvoiceLineFormView({
                model: this.model,
                destCollection: this.getOption('destCollection'),
                title: this.getOption('title'),
                add: this.add,
            });
            this.showChildView('main', view);
        }
    },
    templateContext(){
        var show_main = this.add || ! this.tel;
        return {
            title: this.getOption('title'),
            add: this.add,
            show_main: show_main,
        };
    },
    onRender: function(){
        this.refreshForm();
    },
});
export default SupplierInvoiceLineFormPopupView;
