import Mn from 'backbone.marionette';
import FormBehavior from '../../base/behaviors/FormBehavior.js';
// import DatePickerWidget from '../../widgets/DatePickerWidget.js';
import SelectBusinessWidget from '../../widgets/SelectBusinessWidget.js';
import InputWidget from '../../widgets/InputWidget.js';
import SelectWidget from '../../widgets/SelectWidget.js';
import Radio from 'backbone.radio';


const SupplierInvoiceLineFormView = Mn.View.extend({
    id: 'supplierorderline-form',
    behaviors: [FormBehavior],
    template: require('./templates/SupplierInvoiceLineFormView.mustache'),
	regions: {
		// 'date': '.date',
		'type_id': '.type_id',
		'description': '.description',
		'ht': '.ht',
		'tva': '.tva',
		'business_link': '.business_link',
    },
    // Bubble up child view events
    //
    childViewTriggers: {
        'change': 'data:modified',
    },
    initialize(){
        this.channel = Radio.channel('config');
        this.type_options = this.getTypeOptions();
        this.businesses = this.channel.request(
            'get:options',
            'businesses'
        );
    },

    getTypeOptions() {
        return this.channel.request(
            'get:typeOptions',
            'regular'
        );
    },
    onRender(){
        var view;
        view = new InputWidget({
            value: this.model.get('description'),
            title: 'Description',
            field_name: 'description'
        });
        this.showChildView('description', view);
        let ht_editable = this.channel.request('get:form_section', 'lines:ht')['edit'];
        view = new InputWidget({
            value: this.model.get('ht'),
            title: 'Montant HT',
            field_name: 'ht',
            addon: "€",
            required: true && ht_editable,
            editable: ht_editable,
        });
        this.showChildView('ht', view);
        let tva_editable = this.channel.request('get:form_section', 'lines:tva')['edit'];
        view = new InputWidget({
            value: this.model.get('tva'),
            title: 'Montant TVA',
            field_name: 'tva',
            addon: "€",
            required: true && tva_editable,
            editable: tva_editable
        });
        this.showChildView('tva', view);


        view = new SelectWidget({
            value: this.model.get('type_id'),
            title: 'Type de dépense',
            field_name: 'type_id',
            options: this.type_options,
            add_default: true,
            id_key: 'id',
            required: true
        });

        this.showChildView('type_id', view);
        view = new SelectBusinessWidget({
            title: 'Client concerné',
            options: this.businesses,
            customer_value: this.model.get('customer_id'),
            project_value: this.model.get('project_id'),
            business_value: this.model.get('business_id'),
        });
        this.showChildView('business_link', view);
    },
    templateContext: function(){
        return {
            title: this.getOption('title'),
            add: this.getOption('add'),
        };
    }
});
export default SupplierInvoiceLineFormView;
