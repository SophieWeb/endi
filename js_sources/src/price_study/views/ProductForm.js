/*
 * Module name : ProductForm
 */
import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';

import InputWidget from '../../widgets/InputWidget.js';
import SelectWidget from '../../widgets/SelectWidget.js';
import TextAreaWidget from '../../widgets/TextAreaWidget.js';
import TvaProductFormMixin from '../../base/views/TvaProductFormMixin.js';

import ModalFormBehavior from '../../base/behaviors/ModalFormBehavior.js';
import CatalogComponent from '../../common/views/CatalogComponent.js';

const template = require('./templates/ProductForm.mustache');

const ProductForm = Mn.View.extend(TvaProductFormMixin).extend({
    partial: false,
    template: template,
    behaviors: [ModalFormBehavior],
    regions: {
        'order': '.field-order',
        'description': '.field-description',
        'supplier_ht': '.field-supplier_ht',
        'general_overhead': '.field-general_overhead',
        'margin_rate': '.field-margin_rate',
        'ht': '.field-ht',
        'quantity': '.field-quantity',
        'unity': '.field-unity',
        'tva_id': '.field-tva_id',
        'product_id': '.field-product_id',
        'catalogContainer': '#catalog-container'
    },
    ui: {
        main_tab: "ul.nav-tabs li:first a",
    },
    // Listen to the current's view events
    events: {},
    // Listen to child view events
    childViewEvents: {
        'catalog:insert': 'onCatalogInsert'
    },
    // Bubble up child view events
    childViewTriggers: {
        'change': 'data:modified',
        'finish': 'data:modified',
        'cancel:click': 'cancel:click',
    },
    modelEvents: {
        'set:product': 'refreshForm',
        'change:tva_id': 'refreshTvaProductSelect',
    },
    initialize(){
        this.config = Radio.channel('config');
        this.workunit_options = this.config.request(
            'get:options',
            'workunits'
        );
        this.tva_options = this.config.request(
            'get:options',
            'tvas'
        );
        this.product_options = this.config.request(
            'get:options',
            'products',
        );
        this.all_product_options = this.config.request(
            'get:options',
            'products'
        );
        this.facade = Radio.channel('facade');
    },
    getCommonFieldOptions(attribute, label){
        let result = {
            field_name: attribute,
            value: this.model.get(attribute),
            label: label,
            editable: true,
        }
        return result;
    },
    refreshForm(){
        this.showOrder();
        this.showDescription();
        this.showSupplierHt();
        this.showGeneralOverhead();
        this.showMarginRate();
        this.showHt();
        this.showQuantity();
        this.showUnity();
        this.showTva();
        this.showProduct();
        if (!this.getOption('edit')){
            this.getUI('main_tab').tab('show');
        }
    },
    showOrder(){
        this.showChildView(
            'order',
            new InputWidget({
                value: this.model.get('order'),
                field_name:'order',
                type: 'hidden',
            })
        );
    },
    showDescription(){
        let options = this.getCommonFieldOptions('description', "Description");
        options.description = "Description utilisée dans les devis/factures";
        options.tinymce = true;
        options.required = true;
        options.cid = this.model.cid;
        options.required = true;
         let view = new TextAreaWidget(options);
        this.showChildView('description', view);
    },
    showSupplierHt(){
        let options = this.getCommonFieldOptions('supplier_ht', "Coût unitaire HT");
        let view = new InputWidget(options);
        this.showChildView('supplier_ht', view);
    },
    showGeneralOverhead(){
        let options = this.getCommonFieldOptions('general_overhead', 'Coefficient de frais généraux');
        options.description = "Utilisé pour calculer le 'Prix de revient' depuis le coût d'achat ou 'Déboursé sec' selon la formule 'coût d'achat * (1 + Coefficient de frais généraux)'";
        let view = new InputWidget(options);
        this.showChildView('general_overhead', view);
    },
    showMarginRate(){
        let options = this.getCommonFieldOptions('margin_rate', 'Coefficient de marge');
        options.description =  "Utilisé pour calculer le 'Prix intermédiaire' depuis le 'Prix de revient' selon la formule 'prix de revient / (1 - Coefficient marge)'";

        let view = new InputWidget(options);
        this.showChildView('margin_rate', view);
    },
    showHt(){
        let options = this.getCommonFieldOptions('ht', 'Montant unitaire HT');
        let supplier_value = this.model.get('supplier_ht');


        if (Boolean(supplier_value)){
            options.description = "Calculé depuis le coût unitaire";
            options.editable = false;
        }

        let view = new InputWidget(options);
        this.showChildView('ht', view);
    },
    showQuantity(){
        let options = this.getCommonFieldOptions('quantity', "Quantité");
        options.required = true;
        let view = new InputWidget(options);
        this.showChildView('quantity', view);
    },
    showUnity(){
        let options = this.getCommonFieldOptions('unity', "Unité");
        options.options = this.workunit_options;
        options.defaultOption = {'value': '', 'label': 'Choisir une unité'};
        options.id_key = 'value';

        let view = new SelectWidget(options);
        this.showChildView("unity", view);
    },
    showTva(){
        // NB : ici les tva_options sont différentes de celles utilisées dans le reset de la view
        // car elles sont modifiées par le SelectWidget et on perd donc l'info de la
        // tva par défaut
        const tva_options = this.config.request(
            'get:options',
            'tvas'
        );
        let options = this.getCommonFieldOptions('tva_id', 'TVA');
        options.id_key = 'id';
        options.options = tva_options;
        options.required = true;
        options.defaultOption = {'id': '', 'label': 'Choisir un taux de TVA'};
        let view = new SelectWidget(options);
        this.showChildView('tva_id', view);
    },
    showProduct(){
        let options = this.getCommonFieldOptions('product_id', 'Compte produit');
        this.product_options = this.getProductOptions(
            this.tva_options,
            this.all_product_options,
            this.model.get('tva_id'),
        );
        options.required = true;
        options.options = this.product_options;
        options.id_key = 'id';
        options.defaultOption = {'id': '', 'label': 'Choisir un compte produit'};
        let view = new SelectWidget(options);
        this.showChildView('product_id', view);
    },

    templateContext(){
        let title = "Ajouter un produit";
        if (this.getOption('edit')){
            title = "Modifier un produit";
        }
        return {add: ! this.getOption('edit'), title: title};
    },
    onRender(){
        this.refreshForm();
        if (! this.getOption('edit')){

            this.showChildView(
                'catalogContainer',
                new CatalogComponent(
                    {
                        query_params: {type_: 'product'},
                        collection_name: 'catalog_tree',
                        multiple: true
                    }
                )
            );
        }
    },
    onCatalogInsert(sale_products){
        console.log("ProductForm.onCatalogInsert");
        let req = this.facade.request('insert:from:catalog', sale_products);
        console.log(req);
        req.then(() => this.triggerMethod('modal:close'));
    },
    onDestroyModal(){
        console.log("Modal close");
        let app = Radio.channel('app');
        app.trigger('navigate', 'index');
    }
});
export default ProductForm
