/*
 * Module name : PriceStudyView
 */
import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';

import FormBehavior from "../../base/behaviors/FormBehavior.js";
import TextAreaWidget from '../../widgets/TextAreaWidget.js';
import InputWidget from '../../widgets/InputWidget.js';

const template = require('./templates/PriceStudyView.mustache');

const PriceStudyView = Mn.View.extend({
    className: 'form-section',
    behaviors: [FormBehavior],
    template: template,
    regions: {
        name: '.field-name', notes: '.field-notes'
    },
    // Bubble up child view events
    childViewTriggers: {
        'change': 'data:modified',
        'finish': 'data:persist'
    },
    onRender(){
        this.showChildView(
            'name',
            new InputWidget({
                field_name: 'name',
                label: "Nom de l'étude",
                value: this.model.get('name')
            })
        );
        this.showChildView(
            'notes',
            new TextAreaWidget({
                field_name: 'notes',
                label: "Notes",
                value: this.model.get('notes')
            })
        );
    },

});
export default PriceStudyView
