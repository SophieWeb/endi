/*
 * Module name : WorkForm
 */
import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';

import ActionButtonCollection from '../../../base/models/ActionButtonCollection.js';
import ButtonCollectionWidget from '../../../widgets/ButtonCollectionWidget.js';
import TvaProductFormMixin from '../../../base/views/TvaProductFormMixin.js';

import FormBehavior from '../../../base/behaviors/FormBehavior.js';
import InputWidget from '../../../widgets/InputWidget.js';
import CheckboxWidget from '../../../widgets/CheckboxWidget.js';
import SelectWidget from '../../../widgets/SelectWidget.js';
import TextAreaWidget from '../../../widgets/TextAreaWidget.js';
import WorkItemComponent from './WorkItemComponent.js';
import ProductResume from './ProductResume.js';
import ErrorView from '../../../base/views/ErrorView.js';


const template = require('./templates/WorkForm.mustache');

const WorkForm = Mn.View.extend(TvaProductFormMixin).extend({
    template: template,
    partial: true,
    behaviors: [FormBehavior],
    regions: {
        resume: '.resume',
        messageContainer: '.message-container',
        errors: '.errors',
        title: ".field-title",
        description: '.field-description',
        quantity: '.field-quantity',
        total_ht: '.field-total_ht',
        unity: '.field-unity',
        tva_id: '.field-tva_id',
        product_id: '.field-product_id',
        margin_rate: '.field-margin_rate',
        general_overhead: '.field-general_overhead',
        display_details: '.field-display_details',
        items:'.items-container',
        other_buttons: {el: '.other_buttons', replaceElement: true}, 
    },
    ui: {
        'btn_submit': 'button[type=submit]'
    },
    // Listen to child view events
    childViewEvents: {
        'action:clicked': 'onActionClicked',
        'finish': 'onDatasChanged',
    },
    // Bubble up child view events
    childViewTriggers: {
        'finish': 'data:persist',
        'change': 'data:modified'
    },
    events:{
        'data:invalid': 'onDataInvalid',
    },
    modelEvents:{
        'change:tva_id': 'refreshTvaProductSelect',
    },
    initialize(){
        this.config = Radio.channel('config');
        this.app = Radio.channel('app');
        this.unity_options = this.config.request(
            'get:options',
            'workunits'
        );
        this.tva_options = this.config.request(
            'get:options',
            'tvas'
        );
        this.product_options = this.config.request(
            'get:options',
            'products',
        );
        this.all_product_options = this.config.request(
            'get:options',
            'products'
        );
        // On active les évènements de synchro item/work
        this.model.setupSyncEvents();
    },
    showMessageView(){
        var model = new Bb.Model();
        var view = new MessageView({model: model});
        this.showChildView('messageContainer', view);
    },
    showCommonFields(){
        this.showChildView(
            'title',
            new InputWidget({
                label: "Titre du produit composé",
                description: "Titre du produit composé dans le document final",
                field_name: "title",
                value: this.model.get('title'),
                required: true,
            })
        );
        this.showChildView(
            'description',
            new TextAreaWidget(
                {
                    title: "Description",
                    field_name: 'description',
                    value: this.model.get('description'),
                    tinymce: true,
                }
            ));
        this.showChildView(
            'quantity',
            new InputWidget({
                title: "Quantité",
                field_name: "quantity",
                value: this.model.get('quantity')
            })
        );
        this.showChildView(
            'display_details',
            new CheckboxWidget({
                title: "Afficher le détail des prestations dans le document final",
                description: "En décochant cette case, le produit composé apparaîtra comme une seule ligne de prestation, sans le détail des produits ci-dessous",
                field_name: "display_details",
                value: this.model.get('display_details')
            })
        );
        this.showChildView(
            'items',
            new WorkItemComponent({collection: this.model.items})
        );

        this.showChildView(
            'unity',
            new SelectWidget(
                {
                    title: "Unité",
                    field_name: 'unity',
                    options: this.unity_options,
                    value: this.model.get('unity'),
                    defaultOption: {'value': '', 'label': 'Choisir une unité'}
                }
            )
        );

        this.showChildView(
            'tva_id',
            new SelectWidget(
                {
                    title: "TVA",
                    field_name: 'tva_id',
                    options: this.tva_options,
                    id_key: 'id',
                    value: this.model.get('tva_id'),
                    defaultOption: {'id': '', 'label': 'Choisir un taux de TVA'}
                }
            )
        );
        this.product_options = this.getProductOptions(
            this.tva_options,
            this.all_product_options,
        );
        this.showChildView(
            'product_id',
            new SelectWidget(
                {
                    title: "Compte produit",
                    field_name: 'product_id',
                    options: this.product_options,
                    id_key: 'id',
                    value: this.model.get('product_id'),
                    defaultOption: {'id': '', 'label': 'Choisir un compte produit'}
                }
            )
        );
        this.showChildView(
            'general_overhead',
            new InputWidget(
                {
                    title: "Coefficient de frais généraux",
                    field_name: 'general_overhead',
                    value: this.model.get('general_overhead'),
                    description: "Utilisé pour calculer le 'Prix de revient' depuis le coût d'achat ou 'Déboursé sec' selon la formule 'coût d'achat * (1 + Coefficient de frais généraux)'"
                }
            )
        );
        this.showChildView(
            'margin_rate',
            new InputWidget(
                {
                    title: "Coefficient de marge",
                    field_name: 'margin_rate',
                    value: this.model.get('margin_rate'),
                    description: "Utilisé pour calculer le 'Prix intermédiaire' depuis le 'Prix de revient' selon la formule 'prix de revient / (1 - Coefficient marge)'"
                }
            )
        );
    },
    showOtherActionButtons(){
        let collection = new ActionButtonCollection();
        let buttons = [
            {
                label: 'Dupliquer',
                action: 'duplicate',
                icon: 'copy',
                showLabel: false
            },
            {
                label: "Supprimer",
                action: "delete",
                icon: "trash-alt",
                showLabel: false,
                css: 'negative'
            }
        ];

        collection.add(buttons);
        let view = new ButtonCollectionWidget({collection: collection});
        this.showChildView('other_buttons', view);
    },
    onRender(){
        this.showCommonFields();
        this.showChildView(
            'resume',
            new ProductResume({model: this.model})
        );
        this.showOtherActionButtons();
    },
    onCancelForm(){
        console.log("We cancel the form in WorkForm!!!");
        this.app.trigger('navigate', 'index');
    },
    templateContext(){
        return {};
    },
    onDataInvalid(model, errors){
        console.log("WorkForm.onInvalid");
        this.showChildView('errors', new ErrorView({errors: errors}));
    },
    onActionClicked(actionName){
        console.log("Action clicked : %s", actionName);
        this.app.trigger("product:" + actionName, this);
    },
    onFormSubmittedFull(){
        console.log("success sync in WorkForm!!!");
        this.app.trigger('navigate', 'index');
    }
});
export default WorkForm
