/*
 * Module name : WorkItemForm
 */
import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';

import { getOpt } from '../../../tools.js';
import ModalFormBehavior from '../../../base/behaviors/ModalFormBehavior.js';
import ButtonModel from '../../../base/models/ButtonModel.js';
import ButtonWidget from '../../../widgets/ButtonWidget.js';
import InputWidget from '../../../widgets/InputWidget.js';
import CheckboxWidget from '../../../widgets/CheckboxWidget.js';
import TextAreaWidget from '../../../widgets/TextAreaWidget.js';
import SelectWidget from '../../../widgets/SelectWidget.js';
import TvaProductFormMixin from '../../../base/views/TvaProductFormMixin.js';
import CatalogComponent from '../../../common/views/CatalogComponent.js';

const template = require('./templates/WorkItemForm.mustache');

const WorkItemForm = Mn.View.extend(TvaProductFormMixin).extend({
    template: template,
    tagName: 'section',
    id: 'workitem_form',
    behaviors: [ModalFormBehavior],
    partial: false,
    regions: {
        erros: ".errors",
        quantity_inherited: ".field-quantity_inherited",
        description: '.field-description',
        supplier_ht: '.field-supplier_ht',
        general_overhead: '.field-general_overhead',
        margin_rate: '.field-margin_rate',
        ht: '.field-ht',
        work_unit_quantity: '.field-work_unit_quantity',
/*
        total_quantity: '.field-total_quantity',
*/
        unity: '.field-unity',
        tva_id: '.field-tva_id',
        product_id: '.field-product_id',
        catalogContainer: "#catalog-container"
    },
    ui: {
        main_tab: "ul.nav-tabs li:first a",
    },
    // event_prefix: "work:item",
    // Listen to the current's view events
    events: {
        'change': 'updateTotalHt',
    },
    // Listen to child view events
    childViewEvents: {
        'catalog:insert': "onCatalogInsert",
        'action:clicked': "onUnlockClicked",
        'finish': 'onDatasChanged'
    },
    // Bubble up child view events
    childViewTriggers: {
        'finish': 'data:modified'
    },
    modelEvents:{
        'change:tva_id': 'refreshTvaProductSelect',
    },
    initialize(){
        this.config = Radio.channel('config');
        this.unity_options = this.config.request(
            'get:options',
            'workunits'
        );
        this.tva_options = this.config.request(
            'get:options',
            'tvas'
        );
        this.product_options = this.config.request(
            'get:options',
            'products',
        );
        this.all_product_options = this.config.request(
            'get:options',
            'products'
        );
        this.facade = Radio.channel('facade');
    },
    getCommonFieldOptions(attribute, label){
        let result = {
            field_name: attribute,
            value: this.model.get(attribute),
            label: label,
            editable: true,
        }
        if (this.model.isFromParent(attribute)){
            result.label += " hérité du produit composé";
            result.editable = false;
        }
        return result;
    },
    refreshForm(){
        this.showToggleLockButton();
        this.showDescription();
        this.showSupplierHt();
        this.showGeneralOverhead();
        this.showMarginRate();
        this.showHt();
        this.showWorkQuantity();
        this.showUnity();
        this.showTva();
        this.showProduct();
    },
    showToggleLockButton(){
        let datas = {};
        if (this.model.get('quantity_inherited')){
            datas.label = "Modifier librement";
            datas.title = "Modifier les quantités indépendament du produit composé";
            datas.icon = 'lock'
        } else {
            datas.label = "Saisir une quantité unitaire";
            datas.title = "Les quantités seront dépendantes de la configuration du produit composé";
            datas.icon = "lock-open";
        }
        this.showChildView('quantity_inherited', new ButtonWidget({model: new ButtonModel(datas)}));
    },
    showDescription(){
        let options = this.getCommonFieldOptions('description', "Description");
        options.description = "Description utilisée dans les devis/factures";
        options.tinymce = true;
        options.required = true;
        let view = new TextAreaWidget(options);
        this.showChildView('description', view);
    },
    showSupplierHt(){
        let options = this.getCommonFieldOptions('supplier_ht', "Coût unitaire HT");
        let view = new InputWidget(options);
        this.showChildView('supplier_ht', view);
    },
    showGeneralOverhead(){
        let options = this.getCommonFieldOptions('general_overhead', 'Coefficient de frais généraux');
        let view = new InputWidget(options);
        this.showChildView('general_overhead', view);
    },
    showMarginRate(){
        let options = this.getCommonFieldOptions('margin_rate', 'Coefficient de marge');
        let view = new InputWidget(options);
        this.showChildView('margin_rate', view);
    },
    showHt(){
        let options = this.getCommonFieldOptions('ht', 'Montant unitaire HT');
        let supplier_value = this.model.get('supplier_ht');

        if (options.editable === false){
            options.description = "Sera calculé depuis le coût unitaire";
        } else if (Boolean(supplier_value)){
            console.log("supplier_value %s", supplier_value);
            options.description = "Calculé depuis le coût unitaire";
            options.editable = false;
        }

        let view = new InputWidget(options);
        this.showChildView('ht', view);
    },
    showWorkQuantity(){
        let label = "Quantité par produit composé";
        let description_text = "Quantité de ce produit dans chaque unité de produit composé";

        if (! this.model.get('quantity_inherited')){
            label = "Quantité : saisie libre";
            description_text = "";
        }

        let options = this.getCommonFieldOptions('work_unit_quantity', label);
        
        if ( description_text != "" ) {
        	options.description = description_text;
        }

        let view = new InputWidget(options);
        this.showChildView('work_unit_quantity', view);
    },
    showUnity(){
        let options = this.getCommonFieldOptions('unity', "Unité");
        options.options = this.unity_options;
        options.defaultOption = {'value': '', 'label': 'Choisir une unité'};
        options.required = true;

        let view = new SelectWidget(options);
        this.showChildView("unity", view);
    },
    showTva(){
        // NB : ici les tva_options sont différentes de celles utilisées dans le reset de la view
        // car elles sont modifiées par le SelectWidget et on perd donc l'info de la
        // tva par défaut
        const tva_options = this.config.request(
            'get:options',
            'tvas'
        );
        let options = this.getCommonFieldOptions('tva_id', 'TVA');
        options.id_key = 'id';
        options.options = tva_options;
        options.defaultOption = {'id': '', 'label': 'Choisir un taux de TVA'};
        let view = new SelectWidget(options);
        this.showChildView('tva_id', view);
    },
    showProduct(){
        let options = this.getCommonFieldOptions('product_id', 'Compte produit');
        this.product_options = this.getProductOptions(
            this.tva_options,
            this.all_product_options,
            this.model.get('tva_id'),
        );
        options.options = this.product_options;
        options.id_key = 'id';
        options.defaultOption = {'id': '', 'label': 'Choisir un compte produit'};
        let view = new SelectWidget(options);
        this.showChildView('product_id', view);
    },
    templateContext(){
        let title = "Ajouter un produit"

        if (this.getOption('edit')){
            title = "Modifier";
        }

        return {
            title: title,
            add: ! this.getOption('edit')
        };
    },
    onRender(){
        this.refreshForm();
        if (! this.getOption('edit')){
            const view = new CatalogComponent(
                {
                    query_params: {type_: 'product'},
                    collection_name: 'catalog_tree',
                    multiple: true
                }
            );
            this.showChildView('catalogContainer', view);
        }
    },
    onUnlockClicked(){
        let confirm;
        if (this.model.get('quantity_inherited')){
            confirm = window.confirm("En éditant librement ce produit les quantités ne se mettront plus à jour lorsque vous modifierez la quantité du produit composé. Confirmez-vous ?");
        } else {
            confirm = true;
        }

        if (confirm){
            this.model.set('quantity_inherited', !this.model.get('quantity_inherited'));
            this.refreshForm();
        }
    },
    onDatasChanged(attribute, value){
       if (attribute == 'supplier_ht'){
           this.model.set('supplier_ht', value);
           this.showHt(true);
       }
    },
    onAttach(){
        this.getUI('main_tab').tab('show');
    },
    onCatalogInsert(sale_products){
        console.log(this.getOption("destCollection"));
        console.log(this.facade);
        let req = this.facade.request('workitem:from:catalog', this.getOption("destCollection"), sale_products);
        console.log(req);
        req.then(() => this.triggerMethod('modal:close'));
    }
});
export default WorkItemForm;
