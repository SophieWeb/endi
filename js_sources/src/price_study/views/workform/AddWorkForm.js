/*
 * Module name : AddWorkForm
 */
import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';

import ModalFormBehavior from '../../../base/behaviors/ModalFormBehavior.js';
import InputWidget from '../../../widgets/InputWidget.js';
import TextAreaWidget from '../../../widgets/TextAreaWidget.js';
import CatalogComponent from '../../../common/views/CatalogComponent.js';


const template = require('./templates/AddWorkForm.mustache');

const AddWorkForm = Mn.View.extend({
    template: template,
    partial: true,
    className: 'main_content',
    behaviors: [ModalFormBehavior],
    regions: {
        title: '.field-title',
        description: '.field-description',
        catalogContainer: '#catalog-container'
    },
    ui: {main_tab: "ul.nav-tabs li:first a"},
    // Listen to the current's view events
    events: {},
    // Listen to child view events
    childViewEvents: {
        'catalog:insert': "onCatalogInsert",
    },
    // Bubble up child view events
    childViewTriggers: {
        'change': 'data:modified',
        'finish': 'data:modified',
        'cancel:click': 'cancel:click'
    },
    initialize(){
        this.config = Radio.channel('config');
        this.facade = Radio.channel('facade');
        this.app = Radio.channel('app');
    },
    refreshForm(){
        console.log(this.model);
        let view = new InputWidget(
            {
                field_name: 'title',
                label: "Titre du produit composé",
                value: this.model.get('title'),
                required: true,
            }
        );
        this.showChildView('title', view);

        view = new TextAreaWidget({
            value: this.model.get('description'),
            title: "Description",
            field_name: "description",
            tinymce: true,
            cid: this.model.cid,
        });
        this.showChildView('description', view);
    },
    onRender(){
        this.refreshForm();
        this.showChildView(
            'catalogContainer',
            new CatalogComponent(
                {
                    query_params: {type_: 'work'},
                    collection_name: 'catalog_tree',
                    multiple: true,
                })
        );
    },
    onAttach(){
        this.getUI('main_tab').tab('show');
    },
    onCatalogInsert(sale_products){
        console.log("AddWorkForm.onCatalogInsert");
        let req = this.facade.request('insert:from:catalog', sale_products);
        req.then(() => this.triggerMethod('modal:close'));
    },
    onSuccessSync(){
        this.app.trigger('navigate', 'works/' + this.model.get('id'));
    },
    onDestroyModal(){
        console.log("Modal close");
        let app = Radio.channel('app');
        this.app.trigger('navigate', 'index');
    }
});
export default AddWorkForm;
