/*
 * File Name :  WorkModel
 */

import { formatAmount, round } from '../../math.js';
import WorkItemCollection from './WorkItemCollection.js';
import ProductModel from './ProductModel.js';

const WorkModel = ProductModel.extend({
    /* props spécifique à ce modèle */
    props: [
        'title',
        'items',
        'sale_product_work_id',
    ],
    validation: {
        items: function(value){
            if (this.has('items')){
                if (value.length === 0){
                    return `Produit composé ${this.get('title')} : Veuillez saisir au moins un produit`;
                }
            }
        },
        title: {
            required: true,
            msg: "Veuillez saisir un titre"
        }
    },
    defaults(){
        console.log("In work model defaults");
        return {quantity: 1, type_: "price_study_work"}
    },
    initialize(){
        console.log("WorkItem.initialize");
        WorkModel.__super__.initialize.apply(this, arguments);
        this.populate();

        console.log("WorkItem.initialized");
    },
    setupSyncEvents(){
        this.stopListening(this.items);
        this.listenTo(this.items, 'saved', this.syncWithItems);
        this.listenTo(this.items, 'destroyed', this.syncWithItems);
    },
    populate(){
        console.log("WorkModel.populate");
        if (this.has('items')){
            this.items = new WorkItemCollection(this.get('items'));
        } else {
            this.items = new WorkItemCollection([]);
        }
        this.items._parent = this;
        this.items.stopListening(this);
        this.items.listenTo(this, 'saved', this.items.syncAll.bind(this.items));
        var this_ = this;
        this.items.url = function(){
            return this_.url() + '/' + "work_items";
        }
        this.items.syncAll();
    },
    syncWithItems(){
        console.log("WorkModel.syncWithItems");
        this.set('items', this.items.toJSON());
        this.fetch();
    },
    supplier_ht_label(){
        return formatAmount(round(this.get('flat_cost'), 5));
    },
    validateModel(){
        let result = this.validate();
        let label = this.get('title');
        let item_validation = this.items.validate();
        _.each(item_validation, function(item, index){
            item_validation[index] = label + item;
        })
        _.extend(result, item_validation);
        return result;
    }
});
/*
 * On complète les 'props' du ProductModel avec celle du WorkModel
 */

WorkModel.prototype.props = WorkModel.prototype.props.concat(ProductModel.prototype.props);
Object.assign(WorkModel.prototype.validation, ProductModel.prototype.validation);
delete WorkModel.prototype.validation.product_id;
delete WorkModel.prototype.validation.tva_id;
delete WorkModel.prototype.validation.description;
export default WorkModel;
