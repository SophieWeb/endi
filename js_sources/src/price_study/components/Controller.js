import Radio from 'backbone.radio';
import Mn from 'backbone.marionette';
import ProductModel from '../models/ProductModel.js';
import WorkModel from '../models/WorkModel.js';
import DiscountModel from '../models/DiscountModel.js';

const Controller = Mn.Object.extend({
    initialize(options){
        this.facade = Radio.channel('facade');
        this.config = Radio.channel('config');
        console.log("Controller.initialize");
        this.rootView = options['rootView'];
        this.productDefaults = this.config.request('get:options', 'defaults');
    },
    index(){
        console.log("Controller.index");
        this.rootView.index();
    },
    showModal(view){
        this.rootView.showModal(view);
    },
    // Product related views
    addProduct(){
        const collection = this.facade.request('get:collection', 'products');
        let defaults = {order: collection.getMaxOrder() + 1}
        Object.assign(defaults, this.productDefaults);
        let model = new ProductModel(defaults);
        this.rootView.showAddProductForm(model, collection);
    },
    editProduct(modelId){
        let collection = this.facade.request('get:collection', 'products');
        let request = collection.fetch();
        request = request.then(function(){
            let model = collection.get(modelId);
            return model;
        });
        request.then(this.rootView.showEditProductForm.bind(this.rootView));
    },
    productDuplicate(childView){
        console.log("Controller.productDuplicate");
        let request = childView.model.duplicate();
        var this_ = this;
        request.done(
            function(model){
                let route;
                if (model.get('type_') == 'price_study_work'){
                    route = "/works/";
                    this_.rootView.showEditWorkForm(model);
                } else {
                    route = "/products";
                    this_.rootView.showEditProductForm(model);
                }
                const dest_route = route + model.get('id');
                window.location.hash = dest_route;
            }
        );
    },
    // Work related views
    addWork(){
        const collection = this.facade.request('get:collection', 'products');
        let defaults = {order: collection.getMaxOrder() + 1}
        let model = new WorkModel(defaults);
        this.rootView.showAddWorkForm(model, collection);
    },
    editWork(modelId){
        let collection = this.facade.request('get:collection', 'products');
        let request = collection.fetch();
        request = request.then(function(){
            let model = collection.get(modelId);
            return model;
        });
        request.then(this.rootView.showEditWorkForm.bind(this.rootView));
    },
    addDiscount(){
        const collection = this.facade.request('get:collection', 'discounts');
        let model = new DiscountModel({order: collection.getMaxOrder() + 1, type_: 'amount'});
        this.rootView.showAddDiscountForm(model, collection);
    },
    editDiscount(modelId){
        let collection = this.facade.request('get:collection', 'discounts');
        let request = collection.fetch();
        request = request.then(function(){
            let model = collection.get(modelId);
            return model;
        });
        request.then(this.rootView.showEditDiscountForm.bind(this.rootView));
    },
    // Common model views
    _onModelDeleteSuccess: function(){
        let messagebus = Radio.channel('message');
        messagebus.trigger('success', this, "Vos données ont bien été supprimées");
        this.rootView.index();
    },
    _onModelDeleteError: function(){
        let messagebus = Radio.channel('message');
        messagebus.trigger('error', this,
                           "Une erreur a été rencontrée lors de la " +
                           "suppression de cet élément");
    },
    modelDelete(childView){
        console.log("Controller.modelDelete");
        var result = window.confirm("Êtes-vous sûr de vouloir supprimer cet élément ?");
        if (result){
            childView.model.destroy(
                {
                    success: this._onModelDeleteSuccess.bind(this),
                    error: this._onModelDeleteError.bind(this),
                    wait: true
                }
            );
        }
    },
});
export default Controller;
