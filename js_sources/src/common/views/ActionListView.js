import _ from 'underscore';
import Mn from 'backbone.marionette';
import AnchorWidget from '../../widgets/AnchorWidget.js';
import ToggleWidget from '../../widgets/ToggleWidget.js';
import StatusButtonWidget from '../../widgets/StatusButtonWidget';
import POSTButtonWidget from '../../widgets/POSTButtonWidget.js';
import ActionCollection from '../models/ActionCollection.js';
import ToolbarButtonWidget from '../../widgets/ToolbarButtonWidget.js';

const ActionListView = Mn.CollectionView.extend({
    childTemplates: {
        'anchor': AnchorWidget,
        'toggle': ToggleWidget,
        'POSTButton': POSTButtonWidget,
        'status': StatusButtonWidget,
        'button': ToolbarButtonWidget,
    },
    childViewTriggers: {
        'status:change': 'status:change'
    },
    tagName: 'div',
    attributes: {
        'role': 'group'
    },
    initialize(options){
        this.collection = options['collection'] = new ActionCollection(options['actions']);
        console.log(arguments);
        Mn.CollectionView.__super__.initialize.apply(this, arguments);

    },
    childView: function(item){
        const widget = this.childTemplates[item.get('widget')];
        if (_.isUndefined(widget)){
            console.log("Error : invalid widget type %s", item.get('widget'));
        }
        return widget;
    }
});
export default ActionListView;
