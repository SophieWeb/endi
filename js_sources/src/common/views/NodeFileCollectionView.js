import _ from 'underscore';
import Mn from "backbone.marionette";
import NodeFileItemView from "./NodeFileItemView";
import Radio from "backbone.radio";

const template = require('./templates/NodeFileCollectionView.mustache');

const Emptyview = Mn.View.extend({
    template: _.template(
        '<p>Aucun justificatif n\'a été déposé pour l\'instant</p>'
    ),
})

const NodeFileCollectionView = Mn.CollectionView.extend({
    template: template,
    childView: NodeFileItemView,
    emptyView: Emptyview,
    emptyChildViewContainer: 'div',
    childViewContainer: 'ul',
    ui: {
      'add_btn': '.add-btn',
    },
    events: {
        'click @ui.add_btn': 'onAdd',
    },
    initialize: function(options) {
        this.facade = Radio.channel('facade');
    },
    onAdd() {
        this.facade.request('save:all');
        window.openPopup(window.location.pathname + '/addfile');
    },
});
export default NodeFileCollectionView;
