/*
 * Module name : CatalogTreeCollectionView
 */
import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';
import CatalogTreeItemView from './CatalogTreeItemView.js';


const template = require('./templates/CatalogTreeCollectionView.mustache');

const CatalogTreeCollectionView = Mn.CollectionView.extend({
    template: template,
    childView: CatalogTreeItemView,
    childViewContainer: 'tbody',
    childViewOptions(model) {
        return {
            compute_mode: this.getOption('compute_mode'), 
            multiple: this.getOption('multiple')
        }
    }
    
});
export default CatalogTreeCollectionView
