import Bb from 'backbone';

import NodeFileModel from './NodeFileModel.js';

const NodeFileCollection = Bb.Collection.extend({
    model: NodeFileModel,
});
export default NodeFileCollection;
