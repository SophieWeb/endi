import Mn from 'backbone.marionette';
import FormBehavior from '../../base/behaviors/FormBehavior.js';
import DatePickerWidget from '../../widgets/DatePickerWidget.js';
import InputWidget from '../../widgets/InputWidget.js';
import SelectWidget from '../../widgets/SelectWidget.js';
import Select2Widget from '../../widgets/Select2Widget.js';
import Radio from 'backbone.radio';


const BaseExpenseFormView = Mn.View.extend({
    behaviors: [FormBehavior],
    template: require('./templates/ExpenseFormView.mustache'),
	regions: {
        'category': '.category',
		'date': '.date',
		'type_id': '.type_id',
		'description': '.description',
		'ht': '.ht',
		'manual_ttc': '.manual_ttc',
		'tva': '.tva',
		'business_link': '.business_link',
    },
    // Bubble up child view events
    //
    childViewTriggers: {
        'change': 'data:modified',
    },
    childViewEvents: {
        'finish': 'onChildChange',
    },
    initialize(){
        // Common initialization.
        var channel = Radio.channel('config');
        this.type_options = this.getTypeOptions();
        console.log('this.type_options', this.getTypeOptions());
        this.today = channel.request(
            'get:options',
            'today',
        );
        // If we have no type (eg: new expense form), adopt the first option of
        // the select list.
        if (this.model.get('type_id') === undefined) {
            this.model.set('type_id', String(this.type_options[0].id));
        }
    },
    onRender(){
        var view;
        view = new DatePickerWidget({
            date: this.model.get('date'),
            title: "Date",
            field_name: "date",
            default_value: this.today,
            required: true,
        });
        this.showChildView("date", view);
        console.log('Options are : ', this.type_options);
        let previousType = this.model.get('type_id');
        view = new Select2Widget({
            value: this.model.get('type_id'),
            title: 'Type de dépense',
            field_name: 'type_id',
            options: this.type_options,
            id_key: 'id',
            required: true,
        });
        this.showChildView('type_id', view);

        // Syncs model to allow proppper rendering of the form based on wether
        // we have TVA or not.
        this.triggerMethod('data:modified', 'type_id', view.getCurrentValues()[0])
        if (previousType != view.getCurrentValues()[0]) {
            /* re-render to get correct hide/show of amount fields
             * Handle cases where we changed tab and default type
             * option of the new tab has different field presence requirements (tva/ht/ttc)
             * than previous tab's one.
             * A bit hackish
             */
            this.render();
            return;
        }

        let htParams = {
            value: this.model.get('ht'),
            title: 'Montant HT',
            field_name: 'ht',
            addon: "€",
            required: true,
        }

        let tvaParams = {
            value: this.model.get('tva'),
            title: 'Montant TVA',
            field_name: 'tva',
            addon: "€",
            required: true,
        }

        let manual_ttcParams = {
            value: 0,
            title: 'Montant TTC',
            field_name: 'manual_ttc',
            addon: "€",
            required: true,
        }
        if (this.model.hasTvaOnMargin()) {
            tvaParams.value = 0;
            htParams.value = 0;

            manual_ttcParams.value = this.model.get('manual_ttc');

        } else if (! this.model.hasDeductibleTva()) {
            manual_ttcParams.value = 0;
            tvaParams.value = 0;
            htParams.title = 'Montant TTC'
        }

        view = new InputWidget(manual_ttcParams);
        this.showChildView('manual_ttc', view);
        view = new InputWidget(htParams);
        this.showChildView('ht', view);
        view = new InputWidget(tvaParams);
        this.showChildView('tva', view);
    },
    onChildChange(field_name, value) {
        this.triggerMethod('data:modified', field_name, value);

        if (field_name == 'type_id') {
            this.render();
        }
    },
    afterSerializeForm(datas){
        /* We also want the category to be pushed to server,
         * even if not present as form field
         */
        let modifiedDatas = _.clone(datas);
        modifiedDatas['category'] = this.model.get('category');
        return modifiedDatas;
    },
    templateContext: function(){
        let hasTvaOnMargin = this.model.hasTvaOnMargin();
        return {
            title: this.getOption('title'),
            button_title: this.getOption('buttonTitle'),
            add: this.getOption('add'),
            hidden_ht: hasTvaOnMargin,
            hidden_tva: ! this.model.hasDeductibleTva() || hasTvaOnMargin,
            hidden_manual_ttc: ! hasTvaOnMargin,
        };
    }
});
export default BaseExpenseFormView;
