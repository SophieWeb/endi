import Mn from 'backbone.marionette';
import ModalFormBehavior from '../../base/behaviors/ModalFormBehavior.js';
import DatePickerWidget from '../../widgets/DatePickerWidget.js';
import InputWidget from '../../widgets/InputWidget.js';
import SelectWidget from '../../widgets/SelectWidget.js';
import Select2Widget from '../../widgets/Select2Widget.js';
import SelectBusinessWidget from '../../widgets/SelectBusinessWidget.js';
import Radio from 'backbone.radio';


const ExpenseKmFormView = Mn.View.extend({
    behaviors: [ModalFormBehavior],
    template: require('./templates/ExpenseKmFormView.mustache'),
    id: "expensekm-form-popup-modal",
    regions: {
        'category': '.category',
        'date': '.date',
        'type_id': '.type_id',
        'start': '.start',
        'end': '.end',
        'km': '.km',
        'description': '.description',
        'business_link': '.business_link',
    },
    ui: {
        modalbody: "main",
    },
    // Bubble up child view events
    //
    childViewTriggers: {
        'change': 'data:modified',
        'finish': 'data:modified',
    },
    childViewEvents: {
        'finish': 'onChildChange',
    },
    initialize(){
        var channel = Radio.channel('config');
        this.type_options = channel.request(
            'get:options',
            'expensekm_types',
        );
        this.today = channel.request(
            'get:options',
            'today',
        );
        this.businesses = channel.request(
            'get:options',
            'businesses',
        );
    },
    showCategorySelect(){
        const view = new SelectWidget({
            options: [{'value': 1, 'label': "Frais généraux"}, {'value': 2, 'label': "Achats clients"}],
            title: "Catégorie",
            field_name: "category",
            value: this.model.get('category'),
        });
        this.showChildView('category', view);
    },
    refreshForm(){
        let view;

        if (! this.add){
            this.showCategorySelect();
        }

        view = new DatePickerWidget({
            date: this.model.get('date'),
            title: "Date",
            field_name: "date",
            current_year: true,
            default_value: this.today,
            required: true,
        });
        this.showChildView("date", view);

        view = new Select2Widget({
            value: this.model.get('type_id'),
            title: 'Type de dépense',
            field_name: 'type_id',
            options: this.type_options,
            id_key: 'id',
            required: true,
        });
        this.showChildView('type_id', view);

        view = new InputWidget({
            value: this.model.get('start'),
            title: 'Point de départ',
            field_name: 'start',
            required: true,
        });
        this.showChildView('start', view);

        view = new InputWidget({
            value: this.model.get('end'),
            title: "Point d'arrivée",
            field_name: 'end',
            required: true,
        });
        this.showChildView('end', view);

        view = new InputWidget({
            value: this.model.get('km'),
            title: "Nombre de kilomètres",
            field_name: 'km',
            addon: "km",
            required: true,
        });
        this.showChildView('km', view);

        view = new InputWidget({
            value: this.model.get('description'),
            title: 'Motif de déplacement',
            field_name: 'description'
        });
        this.showChildView('description', view);

        if(this.model.get('category')!=1) {
            view = new SelectBusinessWidget({
                title: 'Client concerné',
                options: this.businesses,
                customer_value: this.model.get('customer_id'),
                project_value: this.model.get('project_id'),
                business_value: this.model.get('business_id'),
                required: true,
            });
            this.showChildView('business_link', view);
        }
    },
    afterSerializeForm(datas){
        /* We also want the category to be pushed to server,
         * even if not present as form field
         */
        let modifiedDatas = _.clone(datas);
        modifiedDatas['category'] = this.model.get('category');
        return modifiedDatas;
    },
    templateContext: function(){
        return {
            title: this.getOption('title'),
            button_title: this.getOption('buttonTitle'),
        };
    },
    onRender(){
        this.refreshForm();
    },
    onSuccessSync(){
        if (this.add){
            var this_ = this;
            var modalbody = this.getUI('modalbody');

            modalbody.effect(
                'highlight',
                {color: 'rgba(0,0,0,0)'},
                800,
                this_.refresh.bind(this)
            );
            modalbody.addClass('action_feedback success');
        } else {
            this.triggerMethod('modal:close');
        }
    },
    onModalBeforeClose(){
        this.model.rollback();
    },
});
export default ExpenseKmFormView;
