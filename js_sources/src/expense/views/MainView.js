import Mn from 'backbone.marionette';
import Bb from 'backbone';
import Radio from 'backbone.radio';

import StatusView from '../../common/views/StatusView.js';
import ExpenseModel from '../models/ExpenseModel.js';
import ExpenseKmModel from '../models/ExpenseKmModel.js';
import ExpenseTableView from './ExpenseTableView.js';
import ExpenseKmTableView from './ExpenseKmTableView.js';
import ExpenseFormPopupView from './ExpenseFormPopupView.js';
import ExpenseKmFormView from './ExpenseKmFormView.js';
import ExpenseDuplicateFormView from './ExpenseDuplicateFormView.js';
import TotalView from './TotalView.js';
import TabTotalView from './TabTotalView.js';
import MessageView from '../../base/views/MessageView.js';
import LoginView from '../../base/views/LoginView.js';
import {displayServerSuccess, displayServerError} from '../../backbone-tools.js';


/** Contruct the form popup title for a given expense
 *
 * @param expense: ExpenseBaseModel instance
 */
function makePopupTitle(expense) {
    let expenseLabel, actionLabel;
    if (expense.get('type') == 'km') {
        expenseLabel = 'dépense kilométrique';
    } else {
        expenseLabel = 'dépense';
    }

    if (expense.id) {
        actionLabel = 'Modifier';
    } else {
        actionLabel = 'Ajouter';
    }
    return `${actionLabel} une ${expenseLabel} (${expense.getCategoryLabel()})`
}

const MainView = Mn.View.extend({
   className: 'container-fluid page-content',
   template: require('./templates/MainView.mustache'),
   regions: {
       modalRegion: '.modalRegion',
       internalLines: '.internal-lines',
       internalKmLines: '.internal-kmlines',
       internalTotal: '.internal-total',
       activityLines: '.activity-lines',
       activityKmLines: '.activity-kmlines',
       activityTotal: '.activity-total',
       totals: '.totals',
       messages: {
           el: '.messages-container',
           replaceElement: true,
       }
   },
   ui:{
       internal: '#internal-container',
       activity: '#activity-container',
   },
   childViewEvents: {
       'line:add': 'onLineAdd',
       'line:edit': 'onLineEdit',
       'line:delete': 'onLineDelete',
       'kmline:add': 'onKmLineAdd',
       'kmline:edit': 'onKmLineEdit',
       'kmline:delete': 'onLineDelete',
       'line:duplicate': 'onLineDuplicate',
       'kmline:duplicate': 'onLineDuplicate',
       'bookmark:add': 'onBookMarkAdd',
       'bookmark:delete': 'onBookMarkDelete',
       "status:change": 'onStatusChange',
   },
   templateContext(){
   	return {
   		internalDescription: this.categories[0].description, 
   		externalDescription: this.categories[1].description,
   		internalTabLabel: this.categories[0].label, 
   		externalTabLabel: this.categories[1].label,
   		}
   },
   initialize(){
       this.facade = Radio.channel('facade');
       this.config = Radio.channel('config');
       this.categories = this.config.request('get:options', 'categories');
       this.edit = this.config.request('get:form_section', 'general')['edit'];
       this.listenTo(this.facade, 'status:change', this.onStatusChange)
   },
   onLineAdd(childView){
       /*
        * Launch when a line should be added
        *
        * :param childView: category 1/2 or a childView with a category option
        *   (category arg type is used only when called from
        *   ExpenseFormPopupView.refresh())
        */
       var category;
       if (_.isNumber(childView) || _.isString(childView)){
           category = childView;
       }else{
           category = childView.getOption('category').value;
       }
       var model = new ExpenseModel({category: category});
       this.showLineForm(model, true);
   },
   onKmLineAdd(childView){
       var category = childView.getOption('category').value;
       var model = new ExpenseKmModel({category: category});
       this.showKmLineForm(model, true);
   },
   onLineEdit(childView){
       this.showLineForm(childView.model, false);
   },
   onKmLineEdit(childView){
       this.showKmLineForm(childView.model, false);
   },
   showLineForm(model, add){
       var view = new ExpenseFormPopupView({
           title: makePopupTitle(model),
           buttonTitle: add ? 'Ajouter' : 'Modifier',
           add:add,
           model:model,
           destCollection: this.facade.request('get:collection', 'lines'),
       });
       this.showChildView('modalRegion', view);
   },
   showKmLineForm(model, add){
       var view = new ExpenseKmFormView({
           title: makePopupTitle(model),
           buttonTitle: add ? 'Ajouter' : 'Modifier',
           add:add,
           model:model,
           destCollection: this.facade.request('get:collection', 'kmlines'),
       });
       this.showChildView('modalRegion', view);
   },
   showDuplicateForm(model){
       var view = new ExpenseDuplicateFormView({model: model});
       this.showChildView('modalRegion', view);
   },
   onLineDuplicate(childView){
       this.showDuplicateForm(childView.model);
   },
   onDeleteSuccess: function(){
       displayServerSuccess("Vos données ont bien été supprimées");
   },
   onDeleteError: function(){
       displayServerError("Une erreur a été rencontrée lors de la " +
                           "suppression de cet élément");
   },
   onLineDelete: function(childView){
       var result = window.confirm("Êtes-vous sûr de vouloir supprimer cette dépense ?");
       if (result){
           childView.model.destroy(
               {
                   success: this.onDeleteSuccess,
                   error: this.onDeleteError
               }
           );
       }
    },
   onBookMarkAdd(childView){
       var collection = this.facade.request('get:bookmarks');
       collection.addBookMark(childView.model);
       childView.highlightBookMark();
   },
   onBookMarkDelete(childView){
       var result = window.confirm("Êtes-vous sûr de vouloir supprimer ce favoris ?");
       if (result){
           childView.model.destroy(
               {
                   success: this.onDeleteSuccess,
                   error: this.onDeleteError
               }
           );
       }
   },
   showInternalTab(){
       var collection = this.facade.request(
           'get:collection',
           'lines'
       );
       var view = new ExpenseTableView({
           collection: collection,
           category: this.categories[0],
           edit: this.edit,
       });
       this.showChildView('internalLines', view);

       var km_type_options = this.config.request(
            'get:options',
            'expensekm_types',
       );
       if (!_.isEmpty(km_type_options)){
           collection = this.facade.request(
               'get:collection',
               'kmlines'
           );
           view = new ExpenseKmTableView(
               {
                   collection: collection,
                   category: this.categories[0],
                   edit: this.edit,
               }
           );
           this.showChildView('internalKmLines', view);
       }
   },
   showActitityTab(){
       var collection = this.facade.request(
           'get:collection',
           'lines'
       );
       var view = new ExpenseTableView(
           {
               collection: collection,
               category: this.categories[1],
               edit: this.edit,
           }
       );
       this.showChildView('activityLines', view);

       var km_type_options = this.config.request(
            'get:options',
            'expensekm_types',
       );
       if (!_.isEmpty(km_type_options)){
           collection = this.facade.request(
               'get:collection',
               'kmlines'
           );
           view = new ExpenseKmTableView(
               {
                   collection: collection,
                   category: this.categories[1],
                   edit: this.edit,
               }
           );
           this.showChildView('activityKmLines', view);
       }
   },
   showMessages(){
       var model = new Bb.Model();
       var view = new MessageView({model: model});
       this.showChildView('messages', view);
   },
   showTotals(){
       let model = this.facade.request('get:totalmodel');
       var view = new TotalView({model: model});
       this.showChildView('totals', view);

       view = new TabTotalView({model: model, category: 1});
       this.showChildView('internalTotal', view);
       view = new TabTotalView({model: model, category: 2});
       this.showChildView('activityTotal', view);
   },
    showLogin: function(){
        var view = new LoginView({});
        this.showChildView('modalRegion', view);
    },
   onRender(){
       this.showInternalTab();
       this.showActitityTab();
       this.showTotals();
       this.showMessages();
   },
   onStatusChange(model){
       console.log("Status change");
       var view = new StatusView({action: model});
       this.showChildView('modalRegion', view);
   },
});
export default MainView;
