import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';

import BaseExpenseFormView from './BaseExpenseFormView.js';
import SelectBusinessWidget from '../../widgets/SelectBusinessWidget.js';
import SelectWidget from '../../widgets/SelectWidget.js';
import InputWidget from '../../widgets/InputWidget.js';


const RegularExpenseFormView = BaseExpenseFormView.extend({
    childViewEvents: {
        'finish': 'onChildChange',
    },
    initialize(){
        BaseExpenseFormView.prototype.initialize.apply(this);

        var channel = Radio.channel('config');
        this.businesses = channel.request(
            'get:options',
            'businesses'
        );
    },
    getTypeOptions() {
        var channel = Radio.channel('config');
        return channel.request(
            'get:typeOptions',
            'regular'
        );
    },
    showCategorySelect(){
        const view = new SelectWidget({
            options: [{'value': 1, 'label': "Frais généraux"}, {'value': 2, 'label': "Achats clients"}],
            title: "Catégorie",
            field_name: "category",
            value: this.model.get('category'),
        });
        this.showChildView('category', view);
    },
    onRender(){
        BaseExpenseFormView.prototype.onRender.apply(this);
        let view;
        if (! this.getOption('add')){
            this.showCategorySelect();
        }
        if(this.model.get('category')!=1) {
            view = new SelectBusinessWidget({
                title: 'Client concerné',
                options: this.businesses,
                customer_value: this.model.get('customer_id'),
                project_value: this.model.get('project_id'),
                business_value: this.model.get('business_id'),
                required: false,
            });
            this.showChildView('business_link', view);
        }

        view = new InputWidget({
            value: this.model.get('description'),
            title: 'Description',
            field_name: 'description'
        });
        this.showChildView('description', view);
    },
});
export default RegularExpenseFormView;
