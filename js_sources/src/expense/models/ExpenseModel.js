import ExpenseBaseModel from './ExpenseBaseModel.js';
import { formatPaymentDate } from '../../date.js';
import Radio from 'backbone.radio';

const ExpenseModel = ExpenseBaseModel.extend({
    defaults:{
      category: null,
      description:"",
      ht:null,
      tva:null,
      customer_id:null,
      project_id:null,
      business_id:null
    },
    // Constructor dynamically add a altdate if missing
    // (altdate is used in views for jquery datepicker)
    initialize(options){
      if ((options['altdate'] === undefined)&&(options['date']!==undefined)){
        this.set('altdate', formatPaymentDate(options['date']));
      }
      this.config = Radio.channel('config');
    },
    // Validation rules for our model's attributes
    validation:{
      type_id:{
        required:true,
        msg:"est requis"
      },
      date: {
        required:true,
        pattern:/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/,
        msg:"est requise"
      },
      ht: {
        required:true,
        // Match"es 19,6 19.65 but not 19.654"
        pattern:/^[\+\-]?[0-9]+(([\.\,][0-9]{1})|([\.\,][0-9]{2}))?$/,
        msg:"doit être un nombre"
      },
      tva: {
        required:true,
        pattern:/^[\+\-]?[0-9]+(([\.\,][0-9]{1})|([\.\,][0-9]{2}))?$/,
        msg:"doit être un nombre"
      },
      ttc_input: {
          required: true,
          pattern: 'amount',
          msg:"doit être un nombre",
      }
    },
    total: function(){
      if (this.hasTvaOnMargin()) {
        return parseFloat(this.get('manual_ttc'));
      } else {
        return this.getHT() + this.getTva();
      }
    },
    getTva:function(){
      var result = parseFloat(this.get('tva'));
      return this.getType().computeAmount(result);
    },
    getHT:function(){
      var result = parseFloat(this.get('ht'));
      return this.getType().computeAmount(result);
    },
    isTelType: function() {
      let type = this.getType();

      if (type == undefined) {
        console.warn('Should not happen');
        return false;
      } else {
          return type.get('family') == 'tel';
      }
    },
    hasDeductibleTva: function() {
      let type = this.getType();
      if (type == undefined) {
        return true;
      } else {
        return type.get('is_tva_deductible');
      }
    },
    hasTvaOnMargin: function() {
      let type = this.getType();
      if (type == undefined) {
        return false;
      } else {
        return type.get('tva_on_margin');
      }
    },
    loadBookMark(bookmark){
        var attributes = _.omit(bookmark.attributes, function(value, key){
            if (_.indexOf(['id', 'cid'], key) > -1){
                return true;
            } else if (_.isNull(value) || _.isUndefined(value)){
                return true;
            }
            return false;
        });
        this.set(attributes);
        this.trigger('set:bookmark');
    },
});
export default ExpenseModel;
