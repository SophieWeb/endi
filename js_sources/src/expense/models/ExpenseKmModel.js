import ExpenseBaseModel from './ExpenseBaseModel.js';
import { formatPaymentDate } from '../../date.js';
import Radio from 'backbone.radio';

const ExpenseKmModel = ExpenseBaseModel.extend({
    defaults:{
        type: 'km',
        category:null,
        start:"",
        end:"",
        description:"",
        customer_id:null,
        project_id:null,
        business_id:null
    },
    initialize(options){
        if ((options['altdate'] === undefined)&&(options['date']!==undefined)){
            this.set('altdate', formatPaymentDate(options['date']));
        }
        this.config = Radio.channel('config');
    },
    validation:{
        type_id:{
            required:true,
            msg:"est requis"
        },
        date: {
            required:true,
            pattern:/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/,
            msg:"est requise"
        },
        km: {
            required:true,
            // Match"es 19,6 19.65 but not 19.654"
            pattern:/^[\+\-]?[0-9]+(([\.\,][0-9]{1})|([\.\,][0-9]{2}))?$/,
            msg:"doit être un nombre"
        },
        customer_id: {
            required:true,
            msg:"a minima, le client doit être indiqué"
        },
    },
    getIndice(){
        /*
         *  Return the indice used for compensation of km fees
         */
        let type = this.getType();
        if (type === undefined){
            return 0;
        } else {
            return parseFloat(type.get('amount'));
        }
    },
    getHT(){
        return this.total();
    },
    getTva(){
        return 0;
    },
    total(){
        var km = this.getKm();
        var amount = this.getIndice();
        return km * amount;
    },
    getKm(){
        return parseFloat(this.get('km'));
    },
});
export default ExpenseKmModel;
