import Radio from 'backbone.radio';
import Validation from 'backbone-validation';
import {
    showLoader,
    setupDeferredExceptionHandling,
    setupAjaxCallbacks,
    setupAjaxCSRF
} from './tools.js';
import UserSession from 'base/components/UserSession.js';
import ConfigBus from 'base/components/ConfigBus.js';
import LoginApp from 'base/components/LoginApp.js';
import 'bootstrap/js/tab.js';


const showFieldSetError = function(control, error){
    /*
    Show errors at fieldset level (used for error related to specific components)
    */
    var fieldset = control.closest('fieldset');
    fieldset.addClass('error');

    if (fieldset.find('.alert-danger').length === 0){
        fieldset.find('.title').after(
            "<div class=\"alert alert-danger\"></div>"
        );
    }
    let fieldsetBlock = fieldset.find('.alert-danger');
    return fieldsetBlock.text(error);
}
const showFieldError = function(control, error){
    /*
    Show an error at field level

    control

        The form field jquery object (input, select ...)
        We look for the parent form-group tag

    error

        The message as a string
   */
    var group = control.parents(".form-group");
    group.addClass("has-error");

    if (group.find(".help-block").length === 0){
        group.append(
            "<span class=\"help-block error-message\"></span>"
        );
    }

    var groupBlock = group.find(".help-block");
    return groupBlock.text(error);
}

const hideFieldSetError = function(control){
  /*
  Hide an error set with showFieldsetError
   */
    var fieldset = control.closest('fieldset');
    fieldset.removeClass('error');
    fieldset.find('.alert-danger').remove();
}
const hideFieldError = function(control){
    /*
    Hide an error shown at field level
    */
    var group = control.parents(".form-group");
    group.removeClass("has-error");
    group.find(".error-message").remove();
    return control;
}

export const BootstrapOnValidForm = function(view, attr, selector){
    var control, group;
    control = view.$('[' + selector + '=' + attr + ']');
    if (control.length === 0){
        // Fallback looking for an item with attr css class
        control = view.$('.' + attr);
        hideFieldSetError(control);
    } else {
        hideFieldError(control);
    }
}

export const BootstrapOnInvalidForm = function(view, attr, error, selector) {
    var control, group, position, target;
    control = view.$('[' + selector + '=' + attr + ']');
    if (control.length === 0){
        // Fallback looking for an item with attr css class
        control = view.$('.' + attr);
        showFieldSetError(control, error);
    } else {
        showFieldError(control, error);
    }
}

export const setupBbValidationCallbacks = function(bb_module){
    _.extend(bb_module, {
        valid: BootstrapOnValidForm,
        invalid: BootstrapOnInvalidForm
    });
}

export const displayServerError = function(msg){
  /*
   *  Show errors in a message box
   */
    let messagebus = Radio.channel('message');
    messagebus.trigger('error', msg);
}

export const displayServerSuccess = function(msg){
  /*
   *  Show errors in a message box
   */
    let messagebus = Radio.channel('message');
    messagebus.trigger('success', msg);
}
require('jquery');
import _ from 'underscore';
import $ from 'jquery';

export const setupBbValidationPatterns = function(bb_module){
    _.extend(bb_module.patterns, {
        amount: /^-?(\d+(?:[\.\,]\d{1,5})?)$/,
        amount2: /^-?(\d+(?:[\.\,]\d{1,2})?)$/
    });
    _.extend(bb_module.messages, {
        amount: "Doit être un nombre avec au maximum 5 chiffres après la virgule",
        amount2: "Doit être un nombre avec au maximum 2 chiffres après la virgule"
    });
};
export const setupLibraries = function(){
    setupDeferredExceptionHandling();
    setupAjaxCallbacks();
    setupBbValidationCallbacks(Validation.callbacks);
    setupBbValidationPatterns(Validation);
}

export const applicationStartup = function(options, app, facade, appaction, customRadioChannel){
    /*
     *
     * Backbone marionette application startup process in the new fashion
     *
     * options : The AppOption global variable
     * app : The marionette application to start
     * facade : The facade radio channel used to manage model / collections
     * interactions
     * appaction : Optionnal application handling actions
     * customRadioChannel: Optionnal radio channelservice
     */
    console.log("# Booting Marionette/Backbone app v2 process");
    showLoader();
    setupLibraries();
    setupAjaxCSRF(options['csrf_token']);
    options.config = ConfigBus;
    options.login = LoginApp;
    options.app = app;
    options.facade = facade;
    options.appaction = appaction;
    options.$ = $
    options.session = UserSession;

    ConfigBus.setup(options['form_config_url']);
    if (customRadioChannel){
        customRadioChannel.setup(options['form_config_url']);
    }
    facade.setup(options);
    const startApp = function(){
        console.log(" - Starting the Config component");
        let serverCall1 = ConfigBus.start();
        if (customRadioChannel){
            console.log(" - Starting a Custom component");
            serverCall1 = serverCall1.then(customRadioChannel.start.bind(customRadioChannel));
        }
        console.log(" - Starting the Facade component")
        let serverCall2 = serverCall1.then(facade.start.bind(facade));

        serverCall2.done(
            function(result1, result2){
                console.log("- Components started : Starting the application");
                app.start();
                // L'application a démarré, désormais en callback de l'auth, on
                // veut synchroniser les modifications en cours
                LoginApp.setAuthCallbacks([facade.start.bind(facade)]);
                if (appaction){
                    appaction.start(ConfigBus.form_config['actions']);
                }
            }
        );
    }


    // Au cas où on échoue lors du premier chargement, on veut pouvoir re
    // lancer la page après authentification
    LoginApp.setAuthCallbacks(
        [startApp]
    );
    LoginApp.start();
}
