import BaseFormWidget from './BaseFormWidget.js';
import { getOpt } from "../tools.js";
import { updateSelectOptions } from '../tools.js';


var template = require('./templates/RadioWidget.mustache');

const RadioWidget = BaseFormWidget.extend({
    /*
    * Build a Radio
    *
    *  :param str field_name: The form field name
    *  :param str field_value: The value of the radio field
    *  :param str value: The current value
    *
    *  Events :
    *
    *   finish : When selected
    *
    *   onFinish(field_name, field_value)
    *
    */
    template: template,
    ui:{
        radio: 'input[type=radio]'
    },
    events: {
        'click @ui.radio': 'onClick'
    },
    onClick: function(event){
        this.triggerFinish(this.getOption('field_value'));
    },
    templateContext: function(){
        let result = this.getCommonContext();
        var value = this.getOption('field_value');
        var checked = this.getOption('value') == value;

        let others = {
            value: value,
            inline_label: getOpt(this, 'inline_label', ''),
            checked: checked,
        };
        return Object.assign(result, others);
    }
});
export default RadioWidget;
