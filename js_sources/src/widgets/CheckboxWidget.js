import BaseFormWidget from './BaseFormWidget.js';
import { getOpt } from "../tools.js";


var template = require('./templates/CheckboxWidget.mustache');

const CheckboxWidget = BaseFormWidget.extend({
    /*
    * Build a Checkbox
    *
    *  :param str field_name: The form field name
    *  :param str value: The default value (true_val if checked)
    *  :param str true_val: The True value (default '1') used when checked
    *  :param str false_val: The False value (default '0') used when unchecked
    *
    *  Events :
    *
    *   finish : When select/unselect
    *
    *   onFinish(field_name, value)
    *
    *   value is one of true_val/false_val
    */
    template: template,
    ui:{
        checkbox: 'input[type=checkbox]'
    },
    events: {
        'click @ui.checkbox': 'onClick'
    },
    getCurrentValue: function(){
        var checked = this.getUI('checkbox').prop('checked');
        if (checked){
            return getOpt(this, 'true_val', '1');
        } else {
            return getOpt(this, 'false_val', '0');
        }
    },
    onClick: function(event){
        const field_value = this.getCurrentValue();
        this.triggerFinish(field_value);
    },
    templateContext: function(){
        let result = this.getCommonContext();
        var true_val = getOpt(this, 'true_val', '1');
        var checked = this.getOption('value') == true_val;

        let others = {
            inline_label: getOpt(this, 'inline_label', ''),
            checked: checked,
        };
        return Object.assign(result, others);
    }
});
export default CheckboxWidget;
