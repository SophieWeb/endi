import { getOpt } from '../tools.js';
import Mn from 'backbone.marionette';
import IconWidget from './IconWidget.js';

const ButtonWidget = Mn.View.extend({
    /*
     * View for a ButtonModel
     * Returns a button wrapped in the surroudingTagName or span (default)
     *
     * tiggers 
     * @event action:clicked
     * @property {string} actionName the 'action' string passed as option
     *
     * Supports :
     * surroundingTagName
     *
     * @param {object} model A ButtonModel instance
     * @param {string} surroudingTagName: The tagName of this view (default: span)
     * @param {string} action: The action passed as argument when the action:clicked is fired
     * @param {string} icon: Optionnal name of an icon to add to this button
     *
     * Full example :
     *
     *      let model = new ButtonModel({label: 'click me', icon: 'plus', showLabel: false});
     *      this.showChildView('button-container', new ButtonWidget({model: model}));
     */
    template: require('./templates/ButtonWidget.mustache'),
    tagName: function(){
        return getOpt(this, 'surroundingTagName', 'span');
    },
    className: function(){
        return getOpt(this, 'surroundingCss', '');
    }, 
    regions: {
        icon: {el: 'icon', replaceElement: true},
    },
    ui: {
        btn: 'button'
    },
    events: {
        'click @ui.btn': 'onButtonClicked'
    },
    onButtonClicked(event){
        let actionName = $(event.currentTarget).data('action');
        this.triggerMethod('action:clicked', actionName);
    },
    onRender(){
        let icon = this.model.get('icon');
        if (icon !== false){
            this.showChildView('icon', new IconWidget({icon: icon}));
        } else {
            this.removeRegion('icon');
        }
    },
    templateContext(){
        let css = getOpt(this, 'css', '');
        if (this.model.get('css')){
            css += ' ' + this.model.get('css');
        }
        let result = {
            surroundingTagName: getOpt(this, 'surroundingTagName', false),
            css: css
        }
        if (! this.model.has('title')){
            result['title'] = this.model.get('label');
        }
        return result;
    }
});
export default ButtonWidget;
