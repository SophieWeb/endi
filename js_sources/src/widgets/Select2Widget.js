import SelectWidget from './SelectWidget.js'
// globally assign select2 fn to $ element
import 'select2';

const Select2Widget = SelectWidget.extend({
    /*
     * A select2 widget
     *
     * Share the API of SelectWidget
     */
    onAttach: function() {
        this.getUI('select').select2({
            width: '100%'
        });
    },
});
export default Select2Widget;
