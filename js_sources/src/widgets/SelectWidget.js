import _ from 'underscore';
import BaseFormWidget from './BaseFormWidget.js';
import { getOpt } from "../tools.js";
import { updateSelectOptions } from '../tools.js';

var template = require('./templates/SelectWidget.mustache');

const SelectWidget = BaseFormWidget.extend({
    /*
     * A select widget
     *
     * Features :
     *  - single or multiple item selector
     *  - optionaly supports empty value
     * 
     * Triggers on value selection (two different events to allow two way of event bindings):
     *  - "finish", field_name, field_value 
     *  - "finish:field_name", field_value
     * 
     * Support following options
     *
     *   :param str title: The field label
     *   :param str description:
     *   :param str field_name:
     *   :param list options: The options to render list of objects
     *   :param value: The value to select
     *   :param str id_key: The key of the options used as option's "value", default is "value"
     *   :param str label_key: The key of the options used as label default is label
     *   :param bool editable:
     *   :param bool add_default: default is false
     *   :param obj defaultOption: {id_key: '', label_key: 'Choisir un elem'}
     *   :param bool: multiple: does this widget allow selecting multiple targets
     */
    tagName: 'div',
    className: 'form-group',
    template: template,
    ui: {
        select: 'select'
    },
    events: {
        'change @ui.select': "onChange"
    },
    onChange: function(event){
        this.options.value = this.getCurrentValues();
        const field_value = this.getUI('select').val();
        this.triggerFinish(field_value);
    },
    hasVoid(options){
        return !_.isUndefined(
            _.find(
                options,
                function(option){return option.label == '';}
            )
        );
    },
    /** Return currently selected values
     *
     * this.getOption('value') is not always relevant as it contains only the
     * initial value (from widget initialization).
     */
    getCurrentValues: function(){
        let optionNodes = $.makeArray(this.$el.find('option:selected'));
        return optionNodes.map((x) => x.getAttribute('value'));
    },
    templateContext: function(){
        let ctx = this.getCommonContext();

        var id_key = getOpt(this, 'id_key', 'value');
        var label_key = getOpt(this, 'label_key', 'label');
        // If some ad-hoc filtering of displayed options is required, read
        // CheckBoxListWidget implementation for inspiration.
        var options = this.getOption('options');
        var current_value = this.getOption('value');

        /* Manage default option */
        var add_default = getOpt(this, 'add_default', false);
        var default_option = getOpt(this, 'defaultOption', false);
        if (default_option){
            add_default = true;
        } else if (add_default){
            default_option = {id_key: '', label_key: ''};
        }

        if (! _.isUndefined(current_value) && ! _.isNull(current_value)){
            updateSelectOptions(options, current_value, id_key);
        }

        if ((add_default) && (!_.findWhere(options, default_option))){
            options.unshift(default_option);
        }

        let multiple = getOpt(this, 'multiple', false);
        let more_ctx =  {
            options:options,
            id_key: id_key,
            label_key: label_key,
            multiple: multiple,
        };
        return Object.assign(ctx, more_ctx);
    },
});
export default SelectWidget;
