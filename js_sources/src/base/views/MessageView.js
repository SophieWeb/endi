import Mn from 'backbone.marionette';
import Bb from 'backbone';
import Radio from 'backbone.radio';
import { scrollTop } from '../../tools.js';

const MessageView = Mn.View.extend({
    tagName: 'div',
    template: require('./templates/MessageView.mustache'),
    ui: {
        close: 'button.close-link',
        alert: '.alert'
    },
    events: {
        'click @ui.close': 'onClose',
    },
    modelEvents: {
        "change:notifyClass": 'render',
    },
    className: 'messages-static-top visuallyhidden',
    onClose(){
        this.model.set({notifyText: null});
        this.$el.hide();
    },
    initialize() {
        const channel = Radio.channel('message');
        channel.reply({
            'notify:success': this.notifySuccess.bind(this),
            'notify:error': this.notifyError.bind(this)
        });
    },
    notifyError(message) {
        console.log("Notify error");
        this.model.set({
    	    notifyClass: 'error',
  	        notifyText: message
    	});
        this.ui.alert.addClass('alert-danger');
        this.ui.alert.removeClass('alert-success');
        this.$el.removeClass('visuallyhidden');

        this.$el.show();
        scrollTop(top);
    },
    notifySuccess(message) {
        console.log("Notify success");
        this.model.set({
            notifyClass: 'success',
            notifyText: message
        });
        this.ui.alert.removeClass('alert-danger');
        this.$el.removeClass('visuallyhidden');
        this.ui.alert.addClass('alert-success');
        this.$el.show();
    },
    templateContext(){
        var has_message = false;
        if (this.model.get('notifyText')){
            has_message = true;
        }
        return {
            has_message: has_message,
            error: this.model.get('notifyClass') == 'error'
        }
    }
});
export default MessageView;
