/*
 * File Name :  ButtonModel.js
 */
import Bb from 'backbone';

const  ButtonModel = Bb.Model.extend({
    /*
     * A ButtonModel
     *
     * pass it :
     *
     *  action: The associated action name (used to fire events)
     *  label
     *  icon
     *  showLabel
     *
     */
    defaults: {
        showLabel: true,
        icon: false,
        type: 'button'
    }
});
export default ButtonModel;
