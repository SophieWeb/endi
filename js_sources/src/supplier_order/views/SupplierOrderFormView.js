import Mn from 'backbone.marionette';
import FormBehavior from 'base/behaviors/FormBehavior.js';
import InputWidget from 'widgets/InputWidget.js';
import PercentInputWidget from 'widgets/PercentInputWidget.js';
import SelectWidget from 'widgets/SelectWidget.js';
import Radio from 'backbone.radio';


const SupplierOrderFormView = Mn.View.extend({
    tagName: 'div',
    behaviors: [FormBehavior],
    template: require('./templates/SupplierOrderFormView.mustache'),
	regions: {
		name: '.name',
        advance_percent: '.advance_percent',
        supplier_id: '.supplier_id'
    },
    childViewEvents: {
        'finish': 'onFinish',
        'change': 'onChange'
    },

    initialize(){
        this.config = Radio.channel('config');
        this.suppliers_options = this.config.request(
            'get:options',
            'suppliers'
        );
    },
    onChange(name, value){
        this.model.set(name, value);
        this.triggerMethod('order:modified', name, value);
        this.triggerMethod('data:modified', name, value);
    },
    onFinish(name, value){
        this.model.set(name, value);
        this.triggerMethod('order:modified', name, value);
        this.triggerMethod('data:persist', name, value);
    },
    showSupplierId(){
        const editable = this.config.request('get:form_section', 'general:supplier_id')['edit'];
        const widget_params = {
            options: this.suppliers_options,
            title: 'Fournisseur',
            field_name: 'supplier_id',
            editable: editable,
            value: this.model.get('supplier_id'),
        }
        if (! this.model.has('supplier_id')){
            widget_params['defaultOption'] = {'value': '', 'label': 'Sélectionner'};
        }

        const view = new SelectWidget(widget_params);
        this.showChildView('supplier_id', view);
    },
    showCaePercentage(){
        const editable = this.config.request('get:form_section', 'general:cae_percentage')['edit'];
        const view = new PercentInputWidget({
            value: this.model.get('cae_percentage'),
            title: 'Part de paiement direct par la CAE',
            field_name: 'cae_percentage',
            editable: editable,
        });
        this.showChildView('advance_percent', view);
    }, 
    showName(){
        const editable = this.config.request('get:form_section', 'general')['edit'];
        const view = new InputWidget({
            value: this.model.get('name'),
            title: 'Nom',
            field_name: 'name',
            editable: editable,
        });
        this.showChildView('name', view);
    },
    onRender(){
        if (this.config.request('has:form_section', 'general:cae_percentage')){
            this.showCaePercentage();
        }
        if (this.config.request('has:form_section', 'general:supplier_id')){            
            this.showSupplierId();
        }
        this.showName();
    },
    onSuccessSync(){
        let facade = Radio.channel('facade');
        facade.trigger('navigate', 'index');
    },

});
export default SupplierOrderFormView;
