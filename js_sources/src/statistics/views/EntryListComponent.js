import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';
import EntryCollectionView from './EntryCollectionView.js'

const template = require('./templates/EntryListComponent.mustache');
const EntryListComponent = Mn.View.extend({
    template: template,
    regions: {
        list: {el: 'tbody', replaceElement: true},
    },
    childViewEvents: {
        'model:edit': "onModelEdit",
        "model:delete": "onModelDelete",
        "model:export": "onModelExport",
        'model:duplicate': "onModelDuplicate",
    },
    childViewTriggers: {},
    initialize(){
        this.config = Radio.channel('config');
        this.app = Radio.channel('app');
    },
    onRender(){
        this.showChildView('list', new EntryCollectionView({collection: this.collection}));
    },
    onModelEdit(childView){
        console.log("onModelEdit " + childView.model.get('id'));
        this.app.trigger('navigate', "/entries/" + childView.model.get('id'));
    },
    onModelDelete(childView){
        this.app.trigger('entry:delete', childView);
    },
    onModelExport(childView){
        this.app.trigger('entry:export', childView.model);
    },
    onModelDuplicate(childView){
        console.log("Duplicate");
        this.app.trigger('entry:duplicate', childView.model);
    }
});
export default EntryListComponent;