import Mn from 'backbone.marionette';
import InputWidget from '../../../widgets/InputWidget.js';
import TextAreaWidget from '../../../widgets/TextAreaWidget.js';
import ModalFormBehavior from '../../../base/behaviors/ModalFormBehavior.js';
import { getOpt } from '../../../tools.js';
import CatalogComponent from '../../../common/views/CatalogComponent.js';
import Radio from 'backbone.radio';
var template = require('./templates/TaskGroupFormView.mustache');

const TaskGroupFormView = Mn.View.extend({
    id: 'taskgroup-form-modal',
    template: template,
    regions: {
        'order': '.order',
        'title': '.taskgroup-title',
        'description': '.description',
        'catalog_container': '#catalog-container',
    },
    ui: {
        main_tab: 'ul.nav-tabs li:first a',
    },
    behaviors: [ModalFormBehavior],
    childViewEvents: {
        'catalog:edit': 'onCatalogEdit'
    },
    childViewTriggers: {
        'catalog:insert': 'catalog:insert',
        'change': 'data:modified',
    },
    modelEvents: {
        'set:product_group': 'refreshForm'
    },
    initialize(options) {
        var channel = Radio.channel('config');
        this.compute_mode = channel.request('get:options', 'compute_mode');
    },
    isAddView: function(){
        return !getOpt(this, 'edit', false);
    },
    templateContext: function(){
        return {
            title: this.getOption('title'),
            add: this.isAddView(),
        };
    },
    refreshForm: function(){
        this.showChildView(
            'order',
            new InputWidget({
                value: this.model.get('order'),
                field_name:'order',
                type: 'hidden',
            })
        );
        this.showChildView(
            'title',
            new InputWidget(
                {
                    value: this.model.get('title'),
                    title: "Titre (optionnel)",
                    description: "Titre du produit composé tel qu'affiché dans la sortie pdf, laissez vide pour ne pas le faire apparaître",
                    field_name: "title"
                }
            )
        );
        this.showChildView(
            'description',
            new TextAreaWidget({
                value: this.model.get('description'),
                title: "Description (optionnel)",
                field_name: "description",
                tinymce: true,
                cid: this.model.cid
            })
        );
        if (this.isAddView()){
            this.getUI('main_tab').tab('show');
        }
    },
    onRender: function(){
        this.refreshForm();
        if (this.isAddView()){

            this.showChildView(
                'catalog_container',
                new CatalogComponent(
                    {
                        query_params: {type_: 'work'},
                        multiple: true,
                        compute_mode: this.compute_mode,
                        collection_name: 'catalog_tree',
                    }
                )
            );
        }
    }
});
export default TaskGroupFormView;
