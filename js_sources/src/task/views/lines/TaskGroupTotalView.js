import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';

import { getOpt } from '../../../tools.js';
import { formatAmount } from "../../../math.js";
import LabelRowWidget from '../../../widgets/LabelRowWidget.js';

const TaskGroupTotalView = Mn.View.extend({
    template: require('./templates/LineContainerView.mustache'),
    tagName: 'tbody',
    regions: {
        line: {
            el: '.line',
            replaceElement: true
        }
    },
    collectionEvents: {
        'change': 'render',
        'remove': 'render',
        'add': 'render',
    },
    onRender: function(){
        var configChannel = Radio.channel('config');
        var user_prefs = Radio.channel('user_preferences');
        this.has_price_study = configChannel.request('get:options', 'has_price_study');
        let compute_mode = configChannel.request('get:options', 'compute_mode');
        let is_ttc_mode = (compute_mode == 'ttc');

        let value, label;

        if (is_ttc_mode) {
            value = this.collection.ttc();
            label = 'Sous total TTC';
        } else {
            label = 'Sous total HT';
            value = this.collection.ht();
        }
        var view = new LabelRowWidget({
            label: label,
            values: user_prefs.request('formatAmount', value, false),
            colspan: getOpt(this, 'colspan', 5)
        });
        this.showChildView('line', view);
    }
});
export default TaskGroupTotalView;
