import _ from 'underscore';
import Mn from 'backbone.marionette';
import FormBehavior from "../../base/behaviors/FormBehavior.js";
import CheckboxListWidget from '../../widgets/CheckboxListWidget.js';
import DatePickerWidget from '../../widgets/DatePickerWidget.js';
import TextAreaWidget from '../../widgets/TextAreaWidget.js';
import InputWidget from '../../widgets/InputWidget.js';
import Radio from 'backbone.radio';
import Validation from 'backbone-validation';

var template = require("./templates/CommonView.mustache");


const CommonView = Mn.View.extend({
    /*
     * Wrapper around the component making part of the 'common'
     * invoice/estimation form, provide a main layout with regions for each
     * field
     */
    behaviors: [
        {
            behaviorClass: FormBehavior,
            errorMessage: "Vérifiez votre saisie"
        }
    ],
    tagName: 'div',
    className: 'separate_block border_left_block',
    template: template,
    fields: [
        'date',
        'address',
        'description',
        'validity_duration',
        'workplace',
        'start_date',
        'mentions',
    ],
    ui:{
        'moreAccordion': '#common-more'
    },
    regions: {
        errors: '.errors',
        date: '.date',
        address: '.address',
        description: '.description',
        validity_duration: '.validity_duration',
        workplace: '.workplace',
        start_date: '.start_date',
        mentions: '.mentions',
    },
    childViewTriggers: {
        'change': 'data:modified',
        'finish': 'data:persist'
    },
    modelEvents: {
        'validated:invalid': 'showErrors',
        'validated:valid': 'hideErrors',
        'saved': 'renderField'
    },
    initialize(){
        var channel = Radio.channel('facade');
        this.listenTo(channel, 'bind:validation', this.bindValidation);
        this.listenTo(channel, 'unbind:validation', this.unbindValidation);
        this.mentions_options = Radio.channel('config').request(
            'get:options', 'mentions'
        );
    },
    showErrors(model, errors){
        this.$el.addClass('error');
    },
    hideErrors(model){
        this.$el.removeClass('error');
    },
    bindValidation(){
        console.log("Binding validation");
        Validation.bind(this);
    },
    unbindValidation(){
        Validation.unbind(this);
    },
    getMentionIds(){
        var mention_ids = this.model.get('mentions');
        return mention_ids;
    },
    isMoreSet(){
        var mention_ids = this.getMentionIds();
        if (mention_ids.length > 0){
            return true;
        }
        if (this.model.get('workplace')){
            return true;
        }
        if (this.model.get('start_date')){
            return true;
        }
        return false;
    },
    templateContext(){
        return {is_more_set: this.isMoreSet()};
    },
    showDate(){
        this.showChildView('date', new DatePickerWidget({
            date: this.model.get('date'),
            title: "Date",
            field_name: "date",
            required: true
        }));
    },
    showAddress(){
        this.showChildView('address', new TextAreaWidget({
            title: 'Adresse du client',
            value: this.model.get('address'),
            field_name: 'address',
            rows: 4
        }));
    },
    showDescription(){
        this.showChildView('description', new TextAreaWidget({
            title: 'Objet du document',
            value: this.model.get('description'),
            field_name: 'description',
            rows: 4,
            required: true,
        }));
    },
    showValidityDuration(){
        if(!_.isUndefined(this.model.get('validity_duration'))) {
            /* Pour les devis (si 'validity_duration' existe) :
            on affiche le champ de saisie de limite de validité
            avec la limite par défaut en description
            et on active la validation pour le rendre obligatoire */
            let default_duration = Radio.channel('config').request(
                'get:options', 'estimation_validity_duration_default'
            );
            this.showChildView('validity_duration', new InputWidget({
                title: "Limite de validité du devis",
                description: "Limite par défaut : "+default_duration,
                value: this.model.get('validity_duration'),
                field_name: 'validity_duration',
                required: true,
            }));
            this.model.validation.validity_duration.required = true;
        }
    },
    showWorkplace(){
        this.showChildView('workplace', new TextAreaWidget({
            title: "Lieu d'exécution",
            value: this.model.get('workplace'),
            field_name: 'workplace',
            rows: 3
        }));
    },
    showStartDate(){
        this.showChildView('start_date', new DatePickerWidget({
            date: this.model.get('start_date'),
            title: "Date de début des prestations",
            field_name: "start_date",
            default_value: "",
        }));
    },
    showMentionList(){
        const mention_list = new CheckboxListWidget({
            options: this.mentions_options,
            value: this.getMentionIds(),
            title: "Mentions facultatives",
            field_name: "mentions"
        });
        if (this.mentions_options.length > 0) {
            this.showChildView('mentions', mention_list);
        }
    },
    onRender(){
        this.showDate();
        this.showValidityDuration();
        this.showAddress();
        this.showDescription();
        this.showStartDate();
        this.showWorkplace();
        this.showMentionList();
    },
    renderField(values){
        if (values instanceof Object){
            if ('date' in values){
                this.showDate();
            }
            if ('validity_duration'){
                this.showValidityDuration();
            }
            if ('address' in values){
                this.showAddress();
            }
            if ('description' in values){
                this.showDescription();
            }
            if ('start_date' in values){
                this.showStartDate();
            }
            if ('workplace' in values){
                this.showWorkplace();
            }
            if ('mentions' in values){
                this.showMentionList();
            }
        } else {
            this.onRender();
        }
    }
});
export default CommonView;
