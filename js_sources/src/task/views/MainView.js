import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';
import FileBlockView from "./FileBlockView.js";
import GeneralView from "./GeneralView.js";
import CommonView from "./CommonView.js";
import CompositionComponent from './CompositionComponent.js';
import NotesBlockView from './NotesBlockView.js';
import PaymentConditionBlockView from './payments/PaymentConditionBlockView.js';
import PaymentBlockView from './payments/PaymentBlockView.js';
import DisplayUnitsAndTTCView from "./DisplayUnitsAndTTCView";

import StatusView from './StatusView.js';
import RelatedEstimationCollectionView from "./related_estimation/RelatedEstimationCollectionView";
import LoginView from '../../base/views/LoginView.js';
import ErrorView from '../../base/views/ErrorView.js';
import { showLoader, hideLoader } from '../../tools.js';
import RelatedEstimationCollection from '../models/RelatedEstimationCollection.js';

const template = require('./templates/MainView.mustache');

const MainView = Mn.View.extend({
    template: template,
    regions: {
        errors: '.errors',
        modalRegion: '#modalregion',
        file_requirements: "#file_requirements",
        general: '.general-informations',
        common: '.common-informations',
        display_options: '.display-options',
        composition: '.composition',
        actions_toolbar: {el: "#actions_toolbar", replaceElement: true},
        ht_before_discounts: '.ht_before_discounts',
        notes: '.notes',
        payment_conditions: '.payment-conditions',
        payments: '.payments',
        related_estimation: '.related-estimation',
    },

    initialize: function(options){
        this.config = Radio.channel('config');
        this.facade = Radio.channel('facade');
        this.listenTo(this.facade, 'status:change', this.onStatusChange);
    },
    showFileBlock: function(){
        var section = this.config.request(
            'get:form_section', 'file_requirements'
        );
        var collection = this.facade.request(
            'get:filerequirementcollection'
        );
        var view = new FileBlockView(
            {collection: collection, section:section}
        );
        this.showChildView('file_requirements', view);
    },
    showGeneralBlock: function(){
        var section = this.config.request('get:form_section', 'general');
        var model = this.facade.request('get:model', 'general');
        var view = new GeneralView({model: model, section: section});
        this.showChildView('general', view);
    },
    showCommonBlock: function(){
        var section = this.config.request('get:form_section', 'common');
        var model = this.facade.request('get:model', 'common');
        var view = new CommonView({model: model, section: section});
        this.showChildView('common', view);
        this.showDisplayOptions(model)
    },
    showDisplayOptions: function(model){
        let view = new DisplayUnitsAndTTCView({model: model});
        this.showChildView('display_options', view);
    },
    showNotesBlock: function(){
        var section = this.config.request('get:form_section', 'notes');
        var model = this.facade.request('get:model', 'notes');
        var view = new NotesBlockView({model: model, section: section});
        this.showChildView('notes', view);
    },
    showPaymentConditionsBlock: function(){
        var section = this.config.request('get:form_section', 'payment_conditions');
        var model = this.facade.request('get:model', 'payment_conditions');
        var view = new PaymentConditionBlockView({model:model});
        this.showChildView('payment_conditions', view);
    },
    showPaymentBlock: function(){
        var section = this.config.request('get:form_section', 'payments');
        var model = this.facade.request('get:model', 'common');
        var collection = this.facade.request('get:collection', 'payment_lines');
        var view = new PaymentBlockView(
            {model: model, collection: collection, section: section}
        );
        this.showChildView('payments', view);
    },
    showLogin: function(){
        var view = new LoginView({});
        this.showChildView('modalRegion', view);
    },
    showEstimationLinked: function(){
        var collection = this.facade.request('get:collection', 'related_estimations');
        console.log(collection);
        if (collection && collection.length > 0){
            console.log("The collection is not void");
            var view = new RelatedEstimationCollectionView({collection: collection});
            this.showChildView('related_estimation', view);
        }
    },
    onRender: function() {
        var totalmodel = this.facade.request('get:model', 'total');
        if (this.config.request('has:form_section', 'file_requirements')){
            this.showFileBlock();
        }
        if (this.config.request('has:form_section', 'general')){
            this.showGeneralBlock();
        }
        if (this.config.request('has:form_section', 'common')){
            this.showCommonBlock();
        }
        if (this.config.request('has:form_section', 'composition')){
            this.showCompositionBlock(totalmodel);
        }
        if (this.config.request('has:form_section', "notes")){
            this.showNotesBlock();
        }
        if (this.config.request('has:form_section', "payment_conditions")){
            this.showPaymentConditionsBlock();
        }
        if (this.config.request('has:form_section', "payments")){
            this.showPaymentBlock();
        }
        console.log("On render");
        this.showEstimationLinked();
    },
    showCompositionBlock(totalmodel){
        let section = this.config.request('get:form_section', 'composition');
        let view = new CompositionComponent(
            {section:section, totalmodel: totalmodel}
        );
        this.showChildView('composition', view);
    },
    showStatusView(action){
        var model = this.facade.request('get:model', 'common');
        var view = new StatusView({action: action,  model: model});
        this.showChildView('modalRegion', view);
    },
    formOk(){
        var result = true;
        var errors = this.facade.request('is:valid');
        if (!_.isEmpty(errors)){
            this.showChildView(
                'errors',
                new ErrorView({errors:errors})
            );
            result = false;
        } else {
            this.detachChildView('errors');
        }
        return result;
    },
    onStatusChange: function(model){
        if (! model.get('status')){
            return;
        }
        console.log("showloader");
        showLoader();

        if (model.get('status') != 'draft'){
            console.log("Status is not draft");
            if (! this.formOk()){
                document.body.scrollTop = document.documentElement.scrollTop = 0;
                hideLoader();
                return;
            }
        }
        var this_ = this;
        let deferred = this.facade.request('save:all');
        deferred.then(
            function(){
                hideLoader();
                this_.showStatusView(model)
            },
            function(){
                console.log("In the then error method");
                hideLoader();
            }
        );
    },
    onChildviewDestroyModal: function() {
    	this.getRegion('modalRegion').empty();
  	}
});
export default MainView;
