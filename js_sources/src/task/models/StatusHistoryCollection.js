import Bb from 'backbone';

const StatusHistoryModel = Bb.Model.extend({});
const StatusHistoryCollection = Bb.Collection.extend({
    model: StatusHistoryModel
});
export default StatusHistoryCollection;
