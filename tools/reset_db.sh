#!/bin/bash
echo "Resetting db"
if [ "$1" == '-f' ]
then
    echo "Forcing"
    MYSQLCMD='mysql -u root'
    if [ "$2" != "" ]
    then
        DBNAME=$2
    else
        DBNAME='endi'
    fi
    DBUSER='endi'
    DBPASS='endi'
else
    echo "Enter the mysql command line needed to have root access (default: 'mysql -u root')"
    read MYSQLCMD
    if [ "$MYSQLCMD" == '' ]
    then
        MYSQLCMD='mysql -u root'
    fi
    echo "Enter the database name (default : 'endi')"
    read DBNAME
    if [ "$DBNAME" == '' ]
    then
        DBNAME='endi'
    fi
    echo "Enter the database user (default : 'endi')"
    read DBUSER
    if [ "$DBUSER" == '' ]
    then
        DBUSER='endi'
    fi
    echo "Enter the database user password (default : 'endi')"
    read DBPASS
    if [ "$DBPASS" == '' ]
    then
        DBPASS='endi'
    fi

fi

echo "Deleting database ${DBNAME}"
echo "drop database ${DBNAME};" | ${MYSQLCMD}
echo "create database ${DBNAME};" | ${MYSQLCMD}
echo "grant all privileges on ${DBNAME}.* to ${DBUSER}@localhost identified by '${DBPASS}';" | ${MYSQLCMD}
echo "flush privileges;" | ${MYSQLCMD}
echo "Database reseted"
