==========
enDI
==========

Un progiciel de gestion pour les CAE (Coopérative d'activité et d'emploi),
les collectifs d'entrepreneurs indépendants.

Licence
-------

Ceci est un logiciel libre, pour les conditions d'accès, d'utilisation,
de copie et d'exploitation, voir LICENSE.txt

Nouvelles fonctionnalités/Anomalies
-----------------------------------

Site officiel : http://endi.coop

L'essentiel du développement est réalisé sur financement de Coopérer pour
entreprendre. Si vous souhaitez plus d'information, une offre d'hébergement,
vous pouvez les contacter info@cooperer.coop

Si vous rencontrez un bogue, ou avez une idée de fonctionnalité, il est possible
de signaler cela aux développeurs directement ou en utilisant le système de
tickets de GitLab (framagit).
Exception : pour les bogues de sécurité, merci d'écrire un courriel à votre administrateur.

Instructions pour l'installation du logiciel
--------------------------------------------

Installation des paquets (nécessaire pour l'installation dans un environnement virtuel):

Sous Debian:

.. code-block:: console

    apt install virtualenvwrapper libmariadbclient-dev libmariadb-dev-compat npm build-essential libjpeg-dev libfreetype6 libfreetype6-dev libssl-dev libxml2-dev zlib1g-dev python3-mysqldb redis-server libxslt1-dev python3-pip open-sans-fonts


Sous Fedora:

.. code-block:: console

    dnf install virtualenvwrapper mardiadb-devel python-devel libxslt-devel libxml2-devel libtiff-devel libjpeg-devel libzip-devel freetype-devel lcms2-devel libwebp-devel tcl-devel tk-devel gcc redis-server open-sans-fonts

Création d'un environnement virtuel Python.

.. code-block:: console

    mkvirtualenv endi -p python3

Téléchargement et installation de l'application

.. code-block:: console

    git clone https://framagit.org/endi/endi.git
    cd endi
    python setup.py install
    cp development.ini.sample development.ini

Téléchargement des dépendances JS (requiert nodejs >= 12.x)

.. code-block:: console

    npm --prefix js_sources install

Compilation du code JS::

.. code-block:: console

    make prodjs devjs

Éditer le fichier development.ini et configurer votre logiciel (Accès à la base
de données, différents répertoires de ressources statiques ...).

Initialiser la base de données

.. code-block:: console

    endi-admin development.ini syncdb

Si vous utilisez un paquet tiers utilisant d'autres base de données (comme
endi_payment en mode production)

.. code-block:: console

    endi-migrate app.ini syncdb --pkg=endi_payment

.. note::

    L'application synchronise alors automatiquement les modèles de données.

Puis créer un compte administrateur

.. code-block:: console

    endi-admin development.ini useradd [--user=<user>] [--pwd=<password>] [--firstname=<firstname>] [--lastname=<lastname>] [--group=<group>] [--email=<email>]

N.B : pour un administrateur, préciser

.. code-block:: console

    --group=admin


Puis lancer l'application web

.. code-block:: console

    pserve development.ini

Exécution des tâches asynchrones
---------------------------------

Un service de tâches asynchrones basé sur celery et redis est en charge de
l'exécution des tâches les plus longues.

Voir :
https://framagit.org/endi/endi_celery

pour plus d'informations.

Mise à jour
-----------

La mise à jour d'enDI s'effectue en plusieurs temps (il est préférable de
sauvegarder vos données avant de lancer les commandes suivantes)

Mise à jour de la structure de données

.. code-block:: console

    endi-migrate app.ini upgrade

Si vous utilisez un paquet tiers utilisant d'autres base de données (comme
endi_payment en mode production)

.. code-block:: console

    endi-migrate app.ini upgrade --pkg=endi_payment

Configuration des données par défaut dans la base de données

.. code-block:: console

    endi-admin app.ini syncdb

Met à jour les dépendances JS

.. code-block:: console

    npm --prefix js_sources install

Compile le JavaScript (sur une machine de dév.) :

.. code-block:: console

    make prodjs devjs

OU (sur une machine de production) :

.. code-block:: console

    make prodjs

Développement
-------------

Dans un contexte de développement, installez enDI avec les commandes
suivantes

.. code-block:: console

    git clone https://framagit.org/endi/endi.git
    cd endi
    # Ici on install enDI en mode developpement
    pip install -e .[dev]
    # Même si ça peut sembler bizarre, on peut également avoir besoin de faire :
    pip install -r requirements.txt
    cp development.ini.sample development.ini
    npm --prefix js_sources install
    make devjs


Base de données avec Vagrant
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Pour héberger la base de données dans une machine virtuelle jettable et
reproductible sans toucher à la machine hôte, une configuration Vagrant est
disponible. Pour l'utiliser :

.. code-block:: console

    apt install virtualbox vagrant


Et pour lancer cette machine :

.. code-block:: console

    vagrant up

Un serveur MariaDB est alors installé et configuré (port local 13306 de l'hôte
local, base: endi, login: endi, password: endi).

Des configurations adaptées à vagrant sont commentées dans ``test.ini.sample`` et
``developement.ini.sample``.

Au besoin, la base peut être remise à zéro avec :

.. code-block:: console

    vagrant provision


Tests
------

Installer les dépendances de test

.. code-block:: console

    pip install -r test_requirements.txt

Copier et personaliser le fichier de configuration

.. code-block:: console

    cp test.ini.sample test.ini

Lancer les tests

.. code-block:: console

   py.test endi/tests

Documentation utilisateur
--------------------------

Le guide d'utilisation se trouve à cette adresse :
https://doc.endi.coop
