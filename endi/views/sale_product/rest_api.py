import logging
import inspect
from sqlalchemy import (
    asc,
    desc,
    or_,
)
import colander
from pyramid.csrf import get_csrf_token

from endi_base.models.base import DBSESSION

from endi.models.tva import Tva, Product
from endi.models.task import WorkUnit
from endi.models.third_party.supplier import Supplier
from endi.models.sale_product.category import SaleProductCategory
from endi.models.sale_product.base import (
    BaseSaleProduct,
    SaleProductStockOperation,
)
from endi.models.sale_product.sale_product import (
    SaleProductProduct,
    SaleProductMaterial,
    SaleProductServiceDelivery,
    SaleProductWorkForce,
)
from endi.models.sale_product.work import SaleProductWork
from endi.models.sale_product.training import (
    SaleProductTraining,
    TrainingTypeOptions,
)
from endi.models.sale_product.work_item import WorkItem
from endi.models.company import Company

from endi.forms.sale_product.sale_product import (
    get_sale_product_add_edit_schema,
    get_sale_product_list_schema,
    get_stock_operation_add_edit_schema
)
from endi.forms.sale_product.work import get_work_item_add_edit_schema
from endi.forms.sale_product.category import (
    get_sale_product_category_add_edit_schema,
)
from endi.utils.rest import RestError

from endi.views import BaseRestView
from endi.views.sale_product.routes import (
    CATEGORY_ITEM_API_ROUTE,
    CATEGORY_API_ROUTE,
    PRODUCT_API_ROUTE,
    PRODUCT_ITEM_API_ROUTE,
    WORK_ITEMS_API_ROUTE,
    WORK_ITEMS_ITEM_API_ROUTE,
    STOCK_OPERATIONS_API_ROUTE,
    STOCK_OPERATIONS_ITEM_API_ROUTE,
    CATALOG_API_ROUTE,
)


logger = logging.getLogger(__name__)

# TODO find sqlAlchemy or sql engine max like param length function or
# constant: couldn't find it
# param length > 118 will return empty result
ROUNDED_MAX_LIKE_QUERY_PARAM_LENGTH = 115


class RestListMixinClass:
    """
    Base mixin for rest list views with pagination

        * It launches a query to retrieve records
        * Validates GET params regarding the given schema
        * filter the query with the provided filter_* methods
        * orders the query
        * sorts the query

    @param list_schema: Schema used to validate the GET params provided in the
                    url, the schema should inherit from
                    endi.views.forms.lists.BaseListsSchema to preserve
                    most of the processed automation
    @param sort_columns: dict of {'sort_column_key':'sort_column'...}.
        Allows to generate the validator for the sort availabilities and
        to automatically add a order_by clause to the query. sort_column
        may be equal to Table.attribute if join clauses are present in the
        main query.
    @default_sort: the default sort_column_key to be used
    @default_direction: the default sort direction (one of ['asc', 'desc'])

    you can implement methods

        list_filter_*(self, query, appstruct)

            Allows to provide several filters
            Should act on the query and returns it

        _sort_by_<sort_key>(self, query, appstruct)

            Handles sorting stuff if the given sort_key is used
            Should act on the query and returns it
    """
    list_schema = None
    default_sort = None
    sort_columns = {}
    default_direction = 'asc'

    def __get_bind_params(self):
        """
        return the params passed to the form schema's bind method
        if subclass override this method, it should call the super
        one's too
        """
        return dict(
            request=self.request,
            default_sort=self.default_sort,
            default_direction=self.default_direction,
            sort_columns=self.sort_columns,
        )

    def query(self):
        """
        The main query, should be overrided by a subclass
        """
        raise NotImplementedError("You should implement the query method")

    def __get_filters(self):
        """
        collect the filter_... methods attached to the current object
        """
        for key in dir(self):
            if key.startswith('list_filter_'):
                func = getattr(self, key)
                if inspect.ismethod(func):
                    yield func

    def __filter(self, query, appstruct):
        """
        filter the query with the configured filters
        """
        for method in self.__get_filters():
            query = method(query, appstruct)
        return query

    def __get_sort_key(self, appstruct):
        """
        Retrieve the sort key to use

        :param dict appstruct: Form submitted datas
        :rtype: str
        """
        if 'sort' in self.request.GET:
            result = self.request.GET['sort']
        elif 'sort' in appstruct:
            result = appstruct['sort']
        else:
            result = self.default_sort
        return result

    def __get_sort_direction(self, appstruct):
        """
        Retrieve the sort direction to use

        :param dict appstruct: Form submitted datas
        :rtype: str
        """
        if 'direction' in self.request.GET:
            result = self.request.GET['direction']
        elif 'direction' in appstruct:
            result = appstruct['direction']
        else:
            result = self.default_direction
        return result

    def __sort(self, query, appstruct):
        """
        Sort the results regarding the default values and
        the sort_columns dict, maybe overriden to provide a custom sort
        method
        """
        sort_column_key = self.__get_sort_key(appstruct)
        if self.sort_columns and sort_column_key:
            logger.debug("  + Sorting the query : %s" % sort_column_key)

            custom_sort_method = getattr(
                self,
                "sort_by_%s" % sort_column_key,
                None
            )
            if custom_sort_method is not None:
                query = custom_sort_method(query, appstruct)

            else:
                sort_column = self.sort_columns.get(sort_column_key)

                if sort_column:
                    sort_direction = self.__get_sort_direction(appstruct)
                    logger.debug("  + Direction : %s" % sort_direction)

                    if sort_direction == 'asc':
                        func = asc
                        query = query.order_by(func(sort_column))
                    elif sort_direction == 'desc':
                        func = desc
                        query = query.order_by(func(sort_column))
        return query

    def __collect_appstruct(self):
        """
        collect the filter options from the current url

        Use the schema to validate the GET params of the current url and
        returns the formated datas
        """
        schema = None
        appstruct = {}
        schema = self.list_schema.bind(**self.__get_bind_params())
        if schema is not None:
            try:
                logger.debug(self.request.GET)
                if self.request.GET:
                    submitted = self.request.GET
                    logger.debug(
                        "Submitted filter datas : {}".format(submitted)
                    )
                    appstruct = schema.deserialize(submitted)
                else:
                    appstruct = schema.deserialize({})
            except colander.Invalid:
                # If values are not valid, we want the default ones to be
                # provided see the schema definition
                logger.exception("  - Current search values are not valid")
                appstruct = schema.deserialize({})
                raise RestError(
                    errors=["Rest API : Les valeurs du filtre sont invalides"],
                    code=422,
                )

        return schema, appstruct

    def _paginator_state(self, appstruct, total_entries):
        """
        Build a state dict compatible with backbone.paginator

        See :

            https://github.com/backbone-paginator/backbone.paginator#fetching-data-and-managing-states
        """
        return {'total_entries': total_entries}

    def __paginate(self, query, appstruct):
        """
        Add limit and offset to the query regarding the pagination query
        parameters

        :param obj query: The query to paginate
        :param dict appstruct: The filter datas that were provided
        :returns: The paginated query
        """
        if 'page' in appstruct:
            items_per_page = appstruct.get('items_per_page', 25)
            page = appstruct['page']
            query = query.offset(page * items_per_page)
            query = query.limit(items_per_page)
        return query

    def collection_get(self):
        """
        Collection get method for the Rest api
        """
        logger.debug("# Calling the list view #")
        logger.debug(" + Collecting the appstruct from submitted datas")
        schema, appstruct = self.__collect_appstruct()
        logger.debug(appstruct)
        logger.debug(" + Launching query")
        query = self.query()
        if query is not None:
            logger.debug(" + Filtering query")
            query = self.__filter(query, appstruct)
            logger.debug(" + Sorting query")
            query = self.__sort(query, appstruct)

            # We count before paginating
            count = query.count()
            query = self.__paginate(query, appstruct)

        else:
            count = 0

        if "page" in self.request.GET:
            return self._paginator_state(appstruct, count), query.all()
        else:
            return query.all()


class RestSaleProductView(BaseRestView, RestListMixinClass):
    list_schema = get_sale_product_list_schema()

    factories = dict(
        (c.__tablename__, c)
        for c in (
            SaleProductWork, SaleProductMaterial, SaleProductWorkForce,
            SaleProductServiceDelivery, SaleProductTraining, SaleProductProduct
        )
    )

    def query(self):
        return BaseSaleProduct.query().filter_by(company_id=self.context.id)

    def list_filter_type_(self, query, appstruct):
        type_ = appstruct.get('type_')
        if type_ not in (None, colander.null, ''):
            logger.debug("Filtering by type_ : {}".format(type_))
            query = query.filter(
                BaseSaleProduct.type_ == type_
            )
        return query

    def list_filter_label(self, query, appstruct):
        label = appstruct.get('search')
        if label not in (None, colander.null, ''):
            logger.debug("Filtering by label : {}".format(label))
            query = query.filter(
                BaseSaleProduct.label.like('%{}%'.format(label))
            )
        return query

    def list_filter_description_and_notes(self, query, appstruct):
        description = appstruct.get('description')
        if description not in (None, colander.null, ''):
            logger.debug("Filtering by description : {}".format(description))
            query = query.filter(
                or_(
                    BaseSaleProduct.description.like(
                        '%{}%'.format(
                            description[:ROUNDED_MAX_LIKE_QUERY_PARAM_LENGTH]
                        )
                    ),
                    BaseSaleProduct.notes.like(
                        '%{}%'.format(
                            description[:ROUNDED_MAX_LIKE_QUERY_PARAM_LENGTH]
                        )
                    ),
                )
            )
        return query

    def list_filter_supplier_ref(self, query, appstruct):
        supplier_ref = appstruct.get('supplier_ref')
        if supplier_ref not in (None, colander.null, ''):
            logger.debug("Filtering by supplier_ref : {}".format(supplier_ref))
            query = query.filter(
                BaseSaleProduct.supplier_ref.like('%{}%'.format(supplier_ref))
            )
        return query

    def list_filter_category_id(self, query, appstruct):
        category_id = appstruct.get('category_id')
        if category_id not in (None, colander.null):
            logger.debug("Filtering by category_id : {}".format(category_id))
            query = query.filter_by(category_id=category_id)
        return query

    def list_filter_ref(self, query, appstruct):
        ref = appstruct.get('ref')
        if ref not in (None, colander.null):
            logger.debug("Filtering by ref : {}".format(ref))
            query = query.filter(BaseSaleProduct.ref.like('%{}%'.format(ref)))
        return query

    def list_filter_supplier_id(self, query, appstruct):
        supplier_id = appstruct.get('supplier_id')
        if supplier_id not in (None, colander.null):
            logger.debug("Filtering by supplier_id : {}".format(supplier_id))
            query = query.filter_by(supplier_id=supplier_id)
        return query

    def list_filter_types(self, query, appstruct):
        """
        Filter by product type_
        """
        simple_only = appstruct.get('simple_only', False)
        if simple_only is True:
            types = BaseSaleProduct.SIMPLE_TYPES
            logger.debug("Filtering by type_ : {}".format(types))
            query = query.filter(BaseSaleProduct.type_.in_(types))
        return query

    def _collect_distinct_attribute_values(self, attrname):
        """
        Collect distinct values of attrname used by the current company for its
        BaseSaleProduct
        Also clean void values

        :param str attrname: The BaseSaleProduct column name
        :rtype: list
        """
        column = getattr(BaseSaleProduct, attrname)
        query = DBSESSION().query(column.distinct())
        query = query.filter(BaseSaleProduct.company_id == self.context.id,)
        query = query.filter(column.not_in(('', None)))
        return [val[0] for val in query]

    def _collect_references(self):
        return self._collect_distinct_attribute_values('ref')

    def _collect_labels(self):
        return self._collect_distinct_attribute_values('label')

    def _collect_descriptions_and_notes(self):
        descriptions = self._collect_distinct_attribute_values('description')
        notes = self._collect_distinct_attribute_values('notes')
        return descriptions + notes

    def _collect_suppliers_refs(self):
        return self._collect_distinct_attribute_values('supplier_ref')

    def _collect_product_types(self, trainer):
        """
        Collect product types a user can add
        """
        values = list(
            zip(BaseSaleProduct.ALL_TYPES, BaseSaleProduct.TYPE_LABELS)
        )
        return [
            {'value': value[0], 'label': value[1]}
            for value in values
            if not (value[0] == 'sale_product_training' and not trainer)
        ]

    def _get_form_options(self):
        """
        The view for company products options load

        :param obj context: The context : The company object
        :param obj request: the Pyramid's request object
        """
        default_tva = Tva.get_default()
        if default_tva:
            tva_id = default_tva.id
        else:
            tva_id = ""

        trainer = self.context.has_trainer()
        product_types = self._collect_product_types(trainer)

        return dict(
            tvas=Tva.query().all(),
            unities=WorkUnit.query().all(),
            products=Product.query().filter(Product.internal == False).all(),  # noqa: E712 E501
            training_types=TrainingTypeOptions._query_active_items().all(),
            categories=SaleProductCategory.query().filter_by(
                company_id=self.context.id
            ).all(),
            suppliers=Supplier.query().filter_by(
                company_id=self.context.id
            ).all(),
            references=self._collect_references(),
            product_labels=self._collect_labels(),
            product_descriptions=self._collect_descriptions_and_notes(),
            product_suppliers_refs=self._collect_suppliers_refs(),
            product_types=product_types,
            base_product_types=[
                ptype for ptype in product_types
                if ptype['value'] in BaseSaleProduct.SIMPLE_TYPES
            ],
            trainer=trainer,
            csrf_token=get_csrf_token(self.request),
            defaults={
                'general_overhead': self.context.general_overhead,
                'margin_rate': self.context.margin_rate,
                'tva_id': tva_id
            }
        )

    def form_config(self):
        result = {
            'options': self._get_form_options()
        }
        return result

    def pre_format(self, submitted, edit):
        # Si on édite un SaleProductWork, on ne valide pas les work_items (ils
        # ont déjà été associés dynamiquement
        if 'types' in submitted:
            if not isinstance(submitted['types'], list):
                submitted['types'] = [type_ for type_ in [submitted['types']]]
        if edit and 'items' in submitted:
            submitted.pop('items')
        return submitted

    def get_schema(self, submitted):
        """
        Retrieve a colander validation schema regarding if it's an add or edit
        view

        For add form only treat label and type_ fields

        For edit form returns a schema specific to the context

        sale_product_work : specific treatment

        sale_product_product
        sale_product_material
        sale_product_service_delivery
        sale_product_work_force

        :param dict submitted: The submitted form datas
        :returns: A colanderalechmy.SQLAlchemySchemaNode
        """
        if isinstance(self.context, Company):
            type_ = submitted['type_']
            factory = self.factories[type_]

            schema = get_sale_product_add_edit_schema(
                factory,
                includes=('label', 'type_', 'general_overhead', 'margin_rate'),
                edit=False,
            )
        else:
            schema = get_sale_product_add_edit_schema(
                self.context.__class__
            )
        return schema

    def post_format(self, entry, edit, attributes):
        """
        Allows to apply post formatting to the model before flushing
        it_customize_tasklinegroup_fields
        """
        if not edit:
            entry.company_id = self.context.id

        return entry

    def after_flush(self, entry, edit, attributes):
        """
        Launched after the BaseSaleProduct was added
        """
        # Si l'on ajoute un SaleProductWork on s'assure que les work_item ont
        # un sale_product associé
        if (
            not edit and
            isinstance(entry, SaleProductWork) and
            'items' in attributes
        ):
            # NB : pas sûr que l'on puisse passer ici via l'ui existante
            logger.debug("In the after flush method of RestSaleProductView")
            for index, item in enumerate(entry.items):
                # On récupère le label depuis les données reçues
                label = attributes['items'][index]['label']

                if item.base_sale_product_id is None:
                    # create a base sale product if doesn't exists
                    new_sale_product = item.generate_sale_product(
                        label=label,
                        category_id=entry.category_id,
                        company_id=entry.company_id,
                    )
                    self.dbsession.add(new_sale_product)
                    self.dbsession.flush()
                    item.base_sale_product_id = new_sale_product.id
                    self.dbsession.merge(item)

        if edit:
            state = 'update'
        else:
            state = 'add'

        entry.on_before_commit(state, attributes)
        entry = self.dbsession.merge(entry)

        return entry

    def duplicate_view(self):
        duplicate = self.context.duplicate()
        self.dbsession.add(duplicate)
        self.dbsession.flush()
        return duplicate

    def catalog_get_view(self):
        type_ = self.request.params.get('type_')
        if type_ not in ('product', 'work'):
            raise Exception("Wrong type_ option for catalog %s" % type_)

        if type_ == 'work':
            query = SaleProductWork.query()
        else:
            query = BaseSaleProduct.query().filter(
                BaseSaleProduct.type_.in_(BaseSaleProduct.SIMPLE_TYPES)
            )

        query = query.filter_by(company_id=self.context.id)
        return query.all()

    def on_delete(self):
        self.context.on_before_commit('delete')


class RestWorkItemView(BaseRestView):
    """
    Json api for Work Items

    Collection views have a SaleProductWork context

    Context views have a BaseRestView context
    """

    def get_schema(self, submitted):
        if isinstance(self.context, SaleProductWork):
            # It's an add view
            add = True
        else:
            add = False
        return get_work_item_add_edit_schema(add=add)

    def collection_get(self):
        return self.context.items

    def post_load_from_catalog_view(self):
        logger.debug("post_load_from_catalog_view")
        sale_product_ids = self.request.json_body.get(
            'base_sale_product_ids', []
        )
        logger.debug("sale_product_ids : %s", sale_product_ids)

        items = []
        for id_ in sale_product_ids:
            sale_product = BaseSaleProduct.get(id_)
            work_item = WorkItem.from_base_sale_product(sale_product)
            work_item.sale_product_work_id = self.context.id
            self.dbsession.add(work_item)
            self.dbsession.flush()
            work_item.on_before_commit('add')
            items.append(work_item)

        return items

    def _manage_hybrid_properties(self, entry, attributes):
        """
        Manage the hybrid properties affectation that isn't managed by colander
        alchemy

        :param obj entry: The Current WorkItem instance
        :param dict attributes: The validated form values
        """
        for key, value in list(attributes.items()):
            setattr(entry, key, value)

    def post_format(self, entry, edit, attributes):
        """
        Generate a base_sale_product if there isn't one yet
        """
        logger.debug("In the work item post format view")
        if not edit:
            entry.sale_product_work_id = self.context.id
            entry.sale_product_work = self.context

            if entry.base_sale_product_id is None:
                logger.debug("Creating a new base_sale_product")
                entry.locked = False
                # create a base sale product if doesn't exists
                new_sale_product = entry.generate_sale_product(
                    category_id=self.context.category_id,
                    company_id=self.context.company_id,
                    **attributes
                )
                self.dbsession.add(new_sale_product)
                self.dbsession.flush()
                entry.base_sale_product_id = new_sale_product.id
                entry.base_sale_product = new_sale_product

        return entry

    def after_flush(self, entry, edit, attributes):
        """
        Sync base_sale_product if needed
        """
        self._manage_hybrid_properties(entry, attributes)
        self.dbsession.merge(entry)
        if edit and attributes.get('sync_catalog', False):
            base_sale_product = entry.sync_base_sale_product()
            self.dbsession.merge(base_sale_product)
            self.dbsession.flush()

        if edit:
            state = 'update'
        else:
            state = 'add'

        entry.on_before_commit(state, attributes)
        entry = self.dbsession.merge(entry)
        self.dbsession.flush()

        return entry

    def on_delete(self):
        self.context.on_before_commit('delete')


class RestStockOperationView(BaseRestView):
    """
    Json api for stock operations

    Collection views have a BaseSaleProduct context
    Context views have a BaseRestView context
    """

    def get_schema(self, submitted):
        return get_stock_operation_add_edit_schema()

    def pre_format(self, appstruct, edit):
        product_id = None
        if isinstance(self.context, BaseSaleProduct):
            if self.context.id is not None:
                product_id = self.context.id
        elif isinstance(self.context, SaleProductStockOperation):
            if self.context.base_sale_product.id is not None:
                product_id = self.context.base_sale_product.id
        appstruct['base_sale_product_id'] = product_id
        return appstruct

    def collection_get(self):
        return self.context.stock_operations


class RestCurrentStockView(BaseRestView):
    """
    Rest api for getting sale product's current stock
    """
    def get_current_stock(self):
        return self.context.get_current_stock()


class RestSaleProductCategoryView(BaseRestView):
    """
    Json api for SaleProductCategory
    """
    schema = get_sale_product_category_add_edit_schema()

    # Context is the company
    def collection_get(self):
        categories = SaleProductCategory.query()
        categories = categories.filter_by(company_id=self.context.id)
        return categories.all()

    def pre_format(self, appstruct, edit):
        """
        Force the company_id in the appstruct
        """
        logger.info("Preformatting the appstruct")
        if self.context.__name__ == 'company':
            appstruct['company_id'] = self.context.id
        return appstruct


def includeme(config):
    config.add_view(
        RestSaleProductView,
        attr='form_config',
        route_name=PRODUCT_API_ROUTE,
        request_param="form_config",
        renderer='json',
        permission='list.sale_products',
        xhr=True,
    )
    config.add_view(
        RestSaleProductView,
        attr="catalog_get_view",
        route_name=CATALOG_API_ROUTE,
        renderer='json',
        permission='list.sale_products',
        xhr=True,
    )

    config.add_rest_service(
        RestSaleProductCategoryView,
        CATEGORY_ITEM_API_ROUTE,
        collection_route_name=CATEGORY_API_ROUTE,
        view_rights="list.sale_product_categories",
        edit_rights="edit.sale_product_category",
        add_rights="add.sale_product_category",
        delete_rights="delete.sale_product_category",
    )

    config.add_rest_service(
        RestSaleProductView,
        PRODUCT_ITEM_API_ROUTE,
        collection_route_name=PRODUCT_API_ROUTE,
        view_rights="list.sale_products",
        edit_rights="edit.sale_product",
        add_rights="add.sale_product",
        delete_rights="delete.sale_product",
    )

    config.add_rest_service(
        RestWorkItemView,
        WORK_ITEMS_ITEM_API_ROUTE,
        collection_route_name=WORK_ITEMS_API_ROUTE,
        view_rights="list.work_items",
        edit_rights="edit.work_item",
        add_rights="add.work_item",
        delete_rights="delete.work_item",
    )
    config.add_view(
        RestWorkItemView,
        route_name=WORK_ITEMS_API_ROUTE,
        attr='post_load_from_catalog_view',
        request_method='POST',
        request_param="action=load_from_catalog",
        renderer='json',
        permission='add.work_item',
        xhr=True,
    )

    config.add_rest_service(
        RestStockOperationView,
        STOCK_OPERATIONS_ITEM_API_ROUTE,
        collection_route_name=STOCK_OPERATIONS_API_ROUTE,
        view_rights="list.stock_operations",
        edit_rights="edit.stock_operation",
        add_rights="add.stock_operation",
        delete_rights="delete.stock_operation",
    )
    config.add_view(
        RestCurrentStockView,
        route_name=PRODUCT_ITEM_API_ROUTE,
        attr='get_current_stock',
        request_method='GET',
        request_param="action=get_current_stock",
        renderer='json',
        permission='list.sale_products',
        xhr=True,
    )

    config.add_view(
        RestSaleProductView,
        attr='duplicate_view',
        route_name=PRODUCT_ITEM_API_ROUTE,
        request_param='action=duplicate',
        request_method='POST',
        permission="edit.sale_product",
        renderer="json",
    )
