from pyramid.httpexceptions import HTTPFound

from endi_celery.mail import (
    send_customer_new_order_mail,
    send_supplier_new_order_mail,
    send_customer_new_invoice_mail,
    send_supplier_new_invoice_mail,
)
from endi.models.task import (
    InternalInvoice,
    InternalEstimation,
)
from endi.views.business.business import BusinessOverviewView
from endi.views.invoices.invoice import InvoiceEditView


class InternalInvoiceEditView(InvoiceEditView):
    pass


def generate_order_from_estimation_view(context, request):
    """
    View launching the generation of the internal supplier order from an
    internal estimation
    """
    order = context.sync_with_customer(request)
    send_customer_new_order_mail(request, order)
    send_supplier_new_order_mail(request, order)

    msg = (
        "Une commande fournisseur a été générée dans l'espace enDI de "
        "{}".format(context.customer.label)
    )
    if request.has_permission('view.supplier_order', order):
        url = request.route_path(
            '/suppliers_orders/{id}',
            id=order.id
        )
        msg += (
            " <a href='{}' title='Voir la commande fournisseur'>Voir"
            "</a>".format(url)
        )
    request.session.flash(msg)
    if request.referer:
        redirect = request.referer
    else:
        redirect = request.route_path('/estimations/{id}', id=context.id)
    return HTTPFound(redirect)


def generate_supplier_invoice_from_invoice_view(context, request):
    """
    View launching the generation of the internal supplier invoice from an
    internal invoice
    """
    supplier_invoice = context.sync_with_customer(request)
    send_customer_new_invoice_mail(request, supplier_invoice)
    send_supplier_new_invoice_mail(request, supplier_invoice)

    msg = (
        "Une facture fournisseur a été générée dans l'espace enDI de "
        "{}".format(context.customer.label)
    )
    if request.has_permission('view.supplier_invoice', supplier_invoice):
        url = request.route_path(
            '/suppliers_invoices/{id}',
            id=supplier_invoice.id
        )
        msg += (
            " <a href='{}' title='Voir la facture fournisseur'>Voir"
            "</a>".format(url)
        )
    request.session.flash(msg)
    if request.referer:
        redirect = request.referer
    else:
        redirect = request.route_path('/invoices/{id}', id=context.id)
    return HTTPFound(redirect)


def includeme(config):
    config.add_tree_view(
        InternalInvoiceEditView,
        parent=BusinessOverviewView,
        renderer='tasks/form.mako',
        permission='view.invoice',
        context=InternalInvoice,
        layout='opa',
    )
    config.add_view(
        generate_order_from_estimation_view,
        route_name="/estimations/{id}/gen_supplier_order",
        permission="gen_supplier_order.estimation",
        context=InternalEstimation,
        request_method="POST",
    )
    config.add_view(
        generate_supplier_invoice_from_invoice_view,
        route_name="/invoices/{id}/gen_supplier_invoice",
        permission="gen_supplier_invoice.invoice",
        context=InternalInvoice,
        request_method="POST",
    )
