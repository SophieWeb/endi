import logging

from pyramid.csrf import get_csrf_token
from endi.models.tva import Tva, Product
from endi.models.task import WorkUnit
from endi.models.sale_product.base import BaseSaleProduct
from endi.models.sale_product.work import SaleProductWork

from endi.models.price_study.base import BasePriceStudyProduct
from endi.models.price_study.product import PriceStudyProduct
from endi.models.price_study.discount import PriceStudyDiscount
from endi.models.price_study.price_study import PriceStudy
from endi.models.price_study.work import PriceStudyWork
from endi.models.price_study.work_item import PriceStudyWorkItem
from endi.utils.rest import RestError

from endi.forms.price_study.price_study import get_price_study_add_edit_schema
from endi.forms.price_study.work_item import get_work_item_add_edit_schema
from endi.forms.price_study.discount import get_discount_add_edit_schema
from endi.forms.price_study.product import (
    get_product_add_schema,
    get_product_edit_schema,
)
from endi.views import BaseRestView

from .routes import (
    PRICE_STUDY_DELETE_ROUTE,
    PRICE_STUDY_DUPLICATE_ROUTE,
    PRICE_STUDY_GENEST_ROUTE,
    PRICE_STUDY_GENINV_ROUTE,
    PRICE_STUDY_ITEM_API_ROUTE,
    PRODUCT_API_ROUTE,
    PRODUCT_ITEM_API_ROUTE,
    DISCOUNT_API_ROUTE,
    DISCOUNT_ITEM_API_ROUTE,
    WORK_ITEMS_ITEM_API_ROUTE,
    WORK_ITEMS_API_ROUTE
)
logger = logging.getLogger(__name__)


class RestPriceStudyView(BaseRestView):
    """
    Rest service for Price studies, only provide item access (no collection
    routes)
    """
    schema = get_price_study_add_edit_schema()

    def _collect_product_types(self):
        return [
            {'label': value, 'value': key}
            for key, value in list(BasePriceStudyProduct.TYPE_LABELS.items())
        ]

    def _get_task_links(self):
        result = []
        context_editable = bool(
            self.request.has_permission('edit.price_study')
        )
        for estimation in self.context.estimations:
            value = {
                'type_label': 'Devis',
                'label': estimation.name,
                'url': self.request.route_path(
                    '/estimations/{id}', id=estimation.id
                ),
            }
            if context_editable and estimation.status in ('draft', 'wait'):
                value['sync_url'] = self.request.route_path(
                    '/estimations/{id}/sync_price_study',
                    id=estimation.id
                )
            result.append(value)
        for invoice in self.context.invoices:
            value = {
                'type_label': 'Facture',
                'label': invoice.name,
                'url': self.request.route_path(
                    '/invoices/{id}', id=invoice.id
                ),
            }
            if context_editable and invoice.status in ('draft', 'wait'):
                value['sync_url'] = self.request.route_path(
                    '/invoices/{id}/sync_price_study',
                    id=invoice.id
                )
            result.append(value)
        return result

    def _get_form_options(self):
        """
        The view for company products options load

        :param obj context: The context : The company object
        :param obj request: the Pyramid's request object
        """
        default_tva = Tva.get_default()
        if default_tva:
            tva_id = default_tva.id
        else:
            tva_id = ""

        return dict(
            tvas=Tva.query().all(),
            workunits=WorkUnit.query().all(),
            products=Product.query().all(),
            product_types=self._collect_product_types(),
            defaults={
                'general_overhead': self.context.company.general_overhead,
                'margin_rate': self.context.company.margin_rate,
                'tva_id': tva_id
            },
            csrf_token=get_csrf_token(self.request),
            editable=bool(self.request.has_permission('edit.price_study')),
            tasks=self._get_task_links(),
            decimal_to_display=self.context.company.decimal_to_display,
        )

    def _delete_btn(self):
        """
        Return a deletion btn description

        :rtype: dict
        """
        url = self.request.route_path(
            PRICE_STUDY_DELETE_ROUTE,
            id=self.context.id,
        )
        return {
            'widget': 'POSTButton',
            'option': {
                "url": url,
                "title": "Supprimer définitivement cette étude",
                "css": "btn icon only negative",
                "icon": "trash-alt",
                "confirm_msg": "Êtes-vous sûr de vouloir supprimer "
                "cet élément ?",
            }
        }

    def _duplicate_btn(self, with_label):
        """
        Return a duplicate btn description

        :rtype: dict
        """
        url = self.request.route_path(
            PRICE_STUDY_DUPLICATE_ROUTE,
            id=self.context.id,
        )
        if with_label:
            label = "Dupliquer"
            css = "btn icon"
        else:
            label = ""
            css = "btn icon only"
        return {
            'widget': 'anchor',
            'option': {
                "url": url,
                "title": "Créer une nouvelle étude de prix à partir "
                "de celle-ci",
                "label": label,
                "css": css,
                "icon": "copy",
            }
        }

    def _gen_estimation_btn(self):
        url = self.request.route_path(
            PRICE_STUDY_GENEST_ROUTE,
            id=self.context.id,
        )
        return {
            'widget': 'anchor',
            'option': {
                "url": url,
                "label": "Générer un devis",
                "css": "btn btn-primary icon genestimation",
                "icon": "file-list",
            }
        }

    def _gen_invoice_btn(self):
        url = self.request.route_path(
            PRICE_STUDY_GENINV_ROUTE,
            id=self.context.id,
        )
        return {
            'widget': 'anchor',
            'option': {
                "url": url,
                "label": "Générer une facture",
                "css": "btn btn-primary icon geninvoice",
                "icon": "file-invoice-euro",
            }
        }

    def _get_available_main_actions(self):
        """
        Return buttons for available main actions on the current price_study
        """
        result = []
        if self.request.has_permission('add.estimation', self.context):
            result.append(self._gen_estimation_btn())

        if self.request.has_permission('add.invoice', self.context):
            result.append(self._gen_invoice_btn())
        return result

    def _get_available_more_actions(self):
        """
        Return buttons for the more actions section
        """
        result = []
        with_label = True
        if self.request.has_permission('view.price_study'):
            result.append(self._duplicate_btn(with_label))
        if self.request.has_permission('delete.price_study'):
            result.append(self._delete_btn())
        return result

    def form_config(self):
        result = {
            'options': self._get_form_options(),
            "actions": {
                'main': self._get_available_main_actions(),
                'more': self._get_available_more_actions()
            },
        }
        logger.debug(result)
        return result


class RestPriceStudyProductView(BaseRestView):

    def get_schema(self, submitted):
        if isinstance(self.context, PriceStudy):
            type_ = submitted['type_']
            # It's an add view
            schema = get_product_add_schema(type_)
        else:
            schema = get_product_edit_schema(self.context.__class__)
        return schema

    def post_format(self, entry, edit, attributes):
        if not edit:
            entry.study = self.context

        return entry

    def after_flush(self, entry, edit, attributes):
        """
        Ensure the current edited context (and its related) keep coherent
        values

        If we're adding an element based on an existing catalog element, we
        also attach respective PriceStudyWorkItem

        :param obj entry: the new Product instance
        :param bool edit: Are we editing ?
        :param dict attributes: The submitted attributes
        """
        if not edit:
            if isinstance(entry, PriceStudyWork):
                if entry.sale_product_work_id:

                    sale_product_work = SaleProductWork.get(
                        entry.sale_product_work_id
                    )
                    # On ajoute un work depuis un élément du catalogue, le
                    # formulaire ne contient qu'une partie des donnes
                    # on s'assue que les autres champs du sale_product_work
                    # sont bien synchronisés également
                    entry.sync_from_sale_product(
                        sale_product_work,
                        excludes=list(attributes.keys())
                    )
            elif isinstance(entry, PriceStudyProduct):
                if entry.base_sale_product_id:
                    sale_product = BaseSaleProduct.get(
                        entry.base_sale_product_id
                    )
                    entry.sync_from_sale_product(
                        sale_product,
                        excludes=list(attributes.keys())
                    )
            self.dbsession.merge(entry)
            self.dbsession.flush()
            state = 'add'
        else:
            state = 'update'

        entry.on_before_commit(state, attributes)
        entry = self.dbsession.merge(entry)
        self.dbsession.flush()
        return entry

    def load_from_catalog_view(self):
        logger.debug("Loading datas from catalog")
        sale_product_ids = self.request.json_body.get(
            'sale_product_ids', []
        )
        logger.debug("sale_product_ids : %s", sale_product_ids)

        lines = []
        for id_ in sale_product_ids:
            sale_product = BaseSaleProduct.get(id_)
            if sale_product:
                if isinstance(sale_product, SaleProductWork):
                    factory = PriceStudyWork
                else:
                    factory = PriceStudyProduct
                entry = factory.from_sale_product(sale_product)
                entry.study = self.context
                self.dbsession.add(entry)
                self.dbsession.flush()
                entry.on_before_commit('add')
                lines.append(entry)
            else:
                logger.error("Unkown sale_product {}".format(id_))
        return lines

    def collection_get(self):
        return self.context.products

    def duplicate_view(self):
        duplicate = self.context.duplicate()
        self.dbsession.add(duplicate)
        self.dbsession.flush()
        return duplicate

    def on_delete(self):
        self.context.on_before_commit('delete')


class RestWorkItemView(BaseRestView):
    """
    Json api for Work Items

    Collection views have a PriceStudyWork context

    Context views have a PriceStudyWorkItem context
    """

    def get_schema(self, submitted):
        return get_work_item_add_edit_schema()

    def collection_get(self):
        return self.context.items

    def post_format(self, entry, edit, attributes):
        if not edit:
            entry.price_study_work_id = self.context.id
        return entry

    def _manage_hybrid_properties(self, entry, attributes):
        """
        Manage the hybrid properties affectation that isn't managed by colander
        alchemy

        :param obj entry: The Current WorkItem instance
        :param dict attributes: The validated form values
        """
        for key, value in list(attributes.items()):
            setattr(entry, key, value)

    def after_flush(self, entry, edit, attributes):
        """
        Keep data integrity, launches several computations if needed
        """
        self._manage_hybrid_properties(entry, attributes)
        self.dbsession.merge(entry)
        if edit:
            state = 'update'
        else:
            state = 'add'
        entry.on_before_commit(state, attributes)
        entry = self.dbsession.merge(entry)
        self.dbsession.flush()
        return entry

    def load_from_catalog_view(self):
        logger.debug("Loading work items from catalog")
        sale_product_ids = self.request.json_body.get(
            'sale_product_ids', []
        )
        logger.debug("sale_product_ids : %s", sale_product_ids)

        lines = []
        for id_ in sale_product_ids:
            sale_product = BaseSaleProduct.get(id_)
            if sale_product:
                entry = PriceStudyWorkItem.from_sale_product(
                    self.context, sale_product
                )
                entry.price_study_work = self.context
                self.dbsession.add(entry)
                self.dbsession.flush()
                entry.on_before_commit('add')
                lines.append(entry)
            else:
                logger.error("Unkown sale_product {}".format(id_))

        return lines

    def on_delete(self):
        self.context.on_before_commit('delete')


class RestPriceStudyDiscountView(BaseRestView):

    def get_schema(self, submitted):
        if 'type_' in submitted:
            schema = get_discount_add_edit_schema(submitted['type_'])
        elif isinstance(self.context, PriceStudyDiscount):
            schema = get_discount_add_edit_schema(self.context.type_)
        else:
            raise RestError("Missing mandatory argument type_")
        return schema

    def post_format(self, entry, edit, attributes):
        if not edit:
            entry.price_study = self.context

        return entry

    def after_flush(self, entry, edit, attributes):
        """
        Ensure the current edited context (and its related) keep coherent
        values

        If we're adding an element based on an existing catalog element, we
        also attach respective PriceStudyWorkItem

        :param obj entry: the new Product instance
        :param bool edit: Are we editing ?
        :param dict attributes: The submitted attributes
        """
        if not edit:
            state = 'add'
        else:
            state = 'update'

        entry.on_before_commit(state, attributes)
        entry = self.dbsession.merge(entry)
        self.dbsession.flush()
        return entry

    def collection_get(self):
        return self.context.discounts

    def on_delete(self):
        self.context.on_before_commit('delete')


def includeme(config):
    config.add_view(
        RestPriceStudyView,
        attr='form_config',
        route_name=PRICE_STUDY_ITEM_API_ROUTE,
        request_param="form_config",
        renderer='json',
        permission='view.price_study',
        xhr=True,
    )
    config.add_rest_service(
        RestPriceStudyView,
        PRICE_STUDY_ITEM_API_ROUTE,
        view_rights="view.price_study",
        edit_rights="edit.price_study",
        delete_rights="delete.price_study",
    )
    config.add_rest_service(
        RestPriceStudyProductView,
        PRODUCT_ITEM_API_ROUTE,
        collection_route_name=PRODUCT_API_ROUTE,
        add_rights="add.price_study_product",
        view_rights="view.price_study_product",
        edit_rights="edit.price_study_product",
        delete_rights="delete.price_study_product",
        collection_view_rights="list.price_study_products",
    )
    config.add_rest_service(
        RestPriceStudyDiscountView,
        DISCOUNT_ITEM_API_ROUTE,
        collection_route_name=DISCOUNT_API_ROUTE,
        add_rights="add.price_study_product",
        view_rights="view.price_study_product",
        edit_rights="edit.price_study_product",
        delete_rights="delete.price_study_product",
        collection_view_rights="list.price_study_products",
    )
    config.add_view(
        RestPriceStudyProductView,
        attr='duplicate_view',
        route_name=PRODUCT_ITEM_API_ROUTE,
        request_param='action=duplicate',
        request_method='POST',
        permission="edit.price_study_product",
        renderer="json",
    )
    config.add_view(
        RestPriceStudyProductView,
        attr='load_from_catalog_view',
        route_name=PRODUCT_API_ROUTE,
        request_param='action=load_from_catalog',
        request_method='POST',
        permission="add.price_study_product",
        renderer="json",
    )
    config.add_rest_service(
        RestWorkItemView,
        WORK_ITEMS_ITEM_API_ROUTE,
        collection_route_name=WORK_ITEMS_API_ROUTE,
        add_rights="add.work_item",
        view_rights="view.work_item",
        edit_rights="edit.work_item",
        delete_rights="delete.work_item",
        collection_view_rights="list.work_items",
    )
    config.add_view(
        RestWorkItemView,
        attr='load_from_catalog_view',
        route_name=WORK_ITEMS_API_ROUTE,
        request_param='action=load_from_catalog',
        request_method='POST',
        permission="add.work_item",
        renderer="json",
    )
