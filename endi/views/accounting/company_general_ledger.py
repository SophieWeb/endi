"""
General ledger vizualisation per company
"""

import datetime
import logging
import colander
from sqlalchemy.orm import load_only
from sqlalchemy import (
    and_,
    not_,
    or_,
)
from endi.models.accounting.operations import (
    AccountingOperationUpload,
    AccountingOperation,
)
from endi.forms.accounting import \
    get_company_general_ledger_operations_list_schema

from endi.views import (
    BaseListView,
)
from endi.views.accounting.routes import COMPANY_GENERAL_LEDGER_OPERATION
from endi.models.config import Config

logger = logging.getLogger(__name__)


class CompanyGeneralLedgerOperationsListView(BaseListView):
    """
    Return the list of operations of general legder uploads
    for the current company
    """
    schema = get_company_general_ledger_operations_list_schema()
    sort_columns = {
        "general_account": AccountingOperation.general_account,
    }
    default_sort = "general_account"
    default_direction = "asc"

    @property
    def title(self):
        return "Liste des écritures extraites du Grand livre"

    def query(self):
        accounting_operation_upload_query = AccountingOperationUpload.query()\
            .filter(AccountingOperationUpload.filetype.in_([
                'general_ledger',
                'synchronized_accounting'
            ]))
        uploads_ids = [u.id for u in accounting_operation_upload_query]

        query = AccountingOperation.query().options(
            load_only(
                AccountingOperation.id,
                AccountingOperation.analytical_account,
                AccountingOperation.general_account,
                AccountingOperation.company_id,
                AccountingOperation.label,
                AccountingOperation.debit,
                AccountingOperation.credit,
                AccountingOperation.balance,
                AccountingOperation.upload_id,
                AccountingOperation.date,
            )
        )
        return query.filter(and_(
            AccountingOperation.company_id == self.context.id,
            AccountingOperation.upload_id.in_(uploads_ids)
        ))

    def filter_cae_config_general_account(self, query, appstruct):
        """
        Filter general_account from cae configuration
        :param query:
        :param appstruct:
        :return: query
        """
        company_general_ledger_accounts_filter = Config.get_value(
            "company_general_ledger_accounts_filter", None
        )
        if company_general_ledger_accounts_filter is None:
            return query

        accounts = [
            account.strip()
            for account in company_general_ledger_accounts_filter.split(',')
        ]

        negative_accounts = [
            account for account in accounts if account.startswith('-')
        ]
        positive_accounts = [
            account for account in accounts
            if account != '' and account not in negative_accounts
        ]

        # cleaning negative sign
        negative_accounts = [account.replace('-', '') for account
                             in negative_accounts]
        # filter or on authorized accounts
        if len(positive_accounts) > 0:
            query = query.filter(or_(
                AccountingOperation.general_account.startswith(account) for
                account in positive_accounts
            ))
        # filter on unauthorized accounts
        if len(negative_accounts) > 0:
            for account in negative_accounts:
                query = query.filter(
                    not_(
                        AccountingOperation.general_account.startswith(account)
                    )
                )
        return query

    def filter_general_account(self, query, appstruct):
        """
        Filter general_account from filter form
        :param query:
        :param appstruct:
        :return: query
        """
        account = appstruct.get('general_account')
        if account not in ('', colander.null, None):
            logger.debug("    + Filtering by general_account")
            query = query.filter_by(general_account=account)
        return query

    def filter_date(self, query, appstruct):
        period = appstruct.get('period', {})
        if period.get('start') not in (colander.null, None):
            logger.debug("  + Filtering by date : %s" % period)
            start = period.get('start')
            end = period.get('end')
            if end in (None, colander.null):
                end = datetime.date.today()
            query = query.filter(AccountingOperation.date.between(start, end))
        return query

    def filter_debit(self, query, appstruct):
        debit = appstruct.get('debit', {})
        if debit.get('start') not in (None, colander.null):
            logger.info("  + Filtering by debit amount : %s" % debit)
            start = debit.get('start')
            end = debit.get('end')
            if end in (None, colander.null):
                query = query.filter(AccountingOperation.debit >= start)
            else:
                query = query.filter(
                    AccountingOperation.debit.between(start, end)
                )
        return query

    def filter_credit(self, query, appstruct):
        credit = appstruct.get('credit', {})
        if credit.get('start') not in (None, colander.null):
            logger.info("  + Filtering by credit amount : %s" % credit)
            start = credit.get('start')
            end = credit.get('end')
            if end in (None, colander.null):
                query = query.filter(AccountingOperation.credit >= start)
            else:
                query = query.filter(
                    AccountingOperation.credit.between(start, end)
                )
        return query


def includeme(config):
    config.add_view(
        CompanyGeneralLedgerOperationsListView,
        route_name=COMPANY_GENERAL_LEDGER_OPERATION,
        renderer="/accounting/general_ledger_operations.mako",
        permission="view.accounting",
    )
    config.add_company_menu(
        parent='accounting',
        order=3,
        label="Grand livre",
        route_name=COMPANY_GENERAL_LEDGER_OPERATION,
        route_id_key="company_id",
    )
