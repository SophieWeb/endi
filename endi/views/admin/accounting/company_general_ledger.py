import os
import logging

from endi.views.admin.accounting import (
    AccountingIndexView,
    ACCOUNTING_URL,
)

from endi.views.admin.tools import BaseConfigView
from endi.forms.admin import get_config_schema

logger = logging.getLogger(__name__)

BASE_URL = os.path.join(ACCOUNTING_URL, "company_general_ledger")


class GeneralLedgerAccountSettingView(BaseConfigView):
    title = "Grand livre"
    route_name = BASE_URL

    validation_msg = "Les informations ont bien été enregistrées"
    keys = (
        'company_general_ledger_accounts_filter',
    )
    schema = get_config_schema(keys)

    @property
    def info_message(self):
        return """Toutes les écritures dont le compte commence par le préfixe fourni seront utilisées pour filter la liste des remontées comptables du 
        grands livre. NB : Une liste de préfixe peut être fournie en les séparant par des virgules (ex : 42,43), un préfixe peut être exclu en 
        plaçant le signe '-' devant (ex: 42,-425 incluera tous les comptes 42… sauf les comptes 425…)"""


def add_routes(config):
    """
    Add routes related to this module
    """
    config.add_route(BASE_URL, BASE_URL)


def add_views(config):
    """
    Add views defined in this module
    """
    config.add_admin_view(
        GeneralLedgerAccountSettingView,
        parent=AccountingIndexView,
    )


def includeme(config):
    add_routes(config)
    add_views(config)

