import os

from endi.views.admin import (
    AdminIndexView,
    BASE_URL,
)
from endi.views.admin.tools import BaseAdminIndexView


EXPENSE_URL = os.path.join(BASE_URL, 'expenses')


class ExpenseIndexView(BaseAdminIndexView):
    route_name = EXPENSE_URL
    title = "Module Notes de dépenses"
    description = "Configurer les types de dépenses, les exports comptables"


def includeme(config):
    config.add_route(EXPENSE_URL, EXPENSE_URL)
    config.add_admin_view(ExpenseIndexView, parent=AdminIndexView)
    config.include('.types')
    config.include('.accounting')
    config.include('.numbers')
