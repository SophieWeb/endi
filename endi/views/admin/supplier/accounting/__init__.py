import os

from endi.views.admin.tools import BaseAdminIndexView
from endi.views.admin.supplier import (
    SUPPLIER_URL,
    SupplierIndexView,
)
SUPPLIER_ACCOUNTING_URL = os.path.join(SUPPLIER_URL, 'accounting')


class SupplierAccountingIndex(BaseAdminIndexView):
    title = "Configuration comptable du module Fournisseur"
    description = "Configurer la génération des écritures fournisseur"
    route_name = SUPPLIER_ACCOUNTING_URL


def includeme(config):
    config.add_route(SUPPLIER_ACCOUNTING_URL, SUPPLIER_ACCOUNTING_URL)
    config.add_admin_view(
        SupplierAccountingIndex,
        parent=SupplierIndexView
    )
    config.include('.supplier_invoice')
    config.include('.internalsupplier_invoice')
