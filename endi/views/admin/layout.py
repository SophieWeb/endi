import pkg_resources
from endi.resources import admin_resources


class AdminLayout:
    endi_version = pkg_resources.get_distribution('endi').version

    def __init__(self, context, request):
        admin_resources.need()


def includeme(config):
    config.add_layout(
        AdminLayout,
        template='endi:templates/admin/layout.mako',
        name='admin',
    )
