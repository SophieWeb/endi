import os

import logging
import deform

from endi.models.treasury import CustomInvoiceBookEntryModule
from endi.forms.admin import get_config_schema

from endi.views.admin.tools import (
    get_model_admin_view,
    BaseConfigView,
    BaseAdminIndexView,
)
from endi.views.admin.sale.accounting import (
    ACCOUNTING_INDEX_URL,
    SaleAccountingIndex,
)


logger = logging.getLogger(__name__)
INDEX_URL = os.path.join(ACCOUNTING_INDEX_URL, 'internalinvoice')
CONFIG_URL = os.path.join(INDEX_URL, 'config')


class IndexView(BaseAdminIndexView):
    title = "Factures internes"
    description = "Configurer les écritures des factures internes"
    route_name = INDEX_URL


class ConfigView(BaseConfigView):
    """
        Cae information configuration
    """
    title = "Configuration des informations générales et des \
modules prédéfinis"
    description = "Configuration du code journal pour les factures internes"
    route_name = CONFIG_URL

    validation_msg = "Les informations ont bien été enregistrées"
    keys = (
        'internalcode_journal',
        'internalcode_journal_encaissement',
        'internalnumero_analytique',
        'internalcompte_frais_annexes',
        'internalcompte_cg_banque',
        'internalbookentry_facturation_label_template',
        'internalcae_general_customer_account',
        'internalcae_third_party_customer_account',
        'internalcompte_rrr',
        'internalcompte_cg_contribution',
        "internalcontribution_cae",
        'internalbookentry_contribution_label_template',
        "internalsage_contribution",
    )
    schema = get_config_schema(keys)
    info_message = """Configurez les exports comptables de votre CAE.</br >
<h4>Champs indispensables aux exports</h4>\
    <ul>\
        <li>Code journal</li>\
        <li>Numéro analytique de la CAE</li>\
        <li>Compte banque de l'entrepreneur</li>\
    </ul>
<h4>Champs relatifs aux frais et remises</h4>\
    <ul>\
      <li>Compte de frais annexes</li>\
      <li>Compte RRR (Rabais, Remises et Ristournes)</li>\
    </ul>
<h4>Configurez et activez des modules de retenues optionnels</h4>\
        <ul>\
    <li>Module de contribution à la CAE</li>\
    <li>Module RG Externe (spécifique bâtiment)</li>\
    <li>Module RG Interne (spécifique bâtiment)</li>\
    </ul>
<h4>Variables utilisables dans les gabarits de libellés</h4>\
    <p>Il est possible de personaliser les libellés comptables à l'aide d'un\
    gabarit. Plusieurs variables sont disponibles :</p>\
    <ul>\
      <li><code>{invoice.customer.label}</code> : nom du client facturé</li>\
      <li><code>{invoice.customer.code}</code> : code du client facturé</li>\
      <li><code>{company.code_compta}</code> : code analytique \
      de l'enseigne établissant la facture</li>\
      <li><code>{invoice.official_number}</code> : numéro de facture \
      (pour tronquer à 9 caractères : \
      <code>{invoice.official_number:.9}</code>)</li>\
      <li><code>{company.name}</code> : nom de l'enseigne établissant \
      la facture</li>\
    </ul>\
    <p>NB : Penser à séparer les variables, par exemple par des espaces, \
    sous peine de libellés peu lisibles.</p>\
    """


BaseInternalSaleAccountingCustomView = get_model_admin_view(
    CustomInvoiceBookEntryModule,
    r_path=INDEX_URL,
)


class CustomBookEntryAdminModuleView(BaseInternalSaleAccountingCustomView):
    title = "Modules de contribution personnalisés pour les factures internes"
    description = "Configurer des écritures personnalisées pour les exports \
de factures internes"

    route_name = os.path.join(
        INDEX_URL,
        "custom_book_entry_internalinvoice"
    )

    widget_options = {
        'add_subitem_text_template': "Ajouter un module de contribution \
personnalisé",
        "orderable": False,
    }

    def query_items(self):
        return self.factory.query().filter_by(active=True).filter_by(
            doctype='internalinvoice'
        ).all()

    def customize_schema(self, schema):
        child_schema = schema.children[0].children[0]
        child_schema['doctype'].widget = deform.widget.HiddenWidget()
        child_schema['doctype'].default = 'internalinvoice'
        return schema


def add_routes(config):
    config.add_route(INDEX_URL, INDEX_URL)
    config.add_route(CONFIG_URL, CONFIG_URL)
    config.add_route(
        CustomBookEntryAdminModuleView.route_name,
        CustomBookEntryAdminModuleView.route_name,
    )


def includeme(config):
    add_routes(config)
    config.add_admin_view(IndexView, parent=SaleAccountingIndex)
    config.add_admin_view(ConfigView, parent=IndexView)
    config.add_admin_view(CustomBookEntryAdminModuleView, parent=IndexView)
