import datetime
import logging
import traceback
from typing import Iterable

import colander
from pyramid.csrf import get_csrf_token
from pyramid.httpexceptions import HTTPForbidden

from endi.compute.math_utils import (
    compute_tva_from_ttc,
    compute_ht_from_ttc,
)
from endi.events.status_changed import StatusChanged
from endi.forms.expense import (
    BookMarkSchema,
    get_add_edit_line_schema,
    get_add_edit_sheet_schema,
)
from endi.models.expense.sheet import (
    ExpenseKmLine,
    ExpenseLine,
    ExpenseSheet,
)
from endi.models.expense.types import (
    ExpenseKmType,
    ExpenseType,
)
from endi.models.status import StatusLogEntry
from endi.models.tva import Tva
from endi.utils import strings
from endi.utils.rest import (
    Apiv1Resp,
    RestError,
)
from endi.views import (
    BaseRestView,
    BaseView,
)
from endi.views.expenses.bookmarks import (
    BookMarkHandler,
    get_bookmarks,
)
from endi.views.status import StatusView

logger = logging.getLogger(__name__)


def _get_valid_duplicate_targets(
        source_sheet: ExpenseSheet,
        including_me=True
) -> Iterable[ExpenseSheet]:
    """
    :returns: valid targets for a duplicate of a line from source_sheet.
    """
    all_expenses = ExpenseSheet.query().filter_by(
        user_id=source_sheet.user_id
    )
    all_expenses = all_expenses.filter_by(company_id=source_sheet.company_id)
    if not including_me:
        all_expenses = all_expenses.filter(ExpenseSheet.id != source_sheet.id)
    all_expenses = all_expenses.filter(
        ExpenseSheet.status.in_(['draft', 'invalid'])
    )
    return all_expenses


class RestExpenseSheetView(BaseRestView):
    factory = ExpenseSheet

    def get_schema(self, submitted):
        """
        Return the schema for ExpenseSheet add

        :param dict submitted: The submitted datas
        :returns: A colander.Schema
        """
        return get_add_edit_sheet_schema()

    def post_format(self, entry, edit, attributes):
        """
        Add the company and user id after sheet add
        """
        if not edit:
            entry.company_id = self.context.id
            entry.user_id = self.request.user.id
        return entry

    def form_config(self):
        """
        Form display options

        :returns: The sections that the end user can edit, the options
        available
        for the different select boxes
        """

        result = {
            "actions": {
                'main': self._get_status_actions(),
                'more': self._get_other_actions(),
            },
            "sections": self._get_form_sections(),
        }
        if self.request.has_permission('set_justified.expensesheet'):
            result['actions']['justify'] = self._get_justified_toggle()

        result = self._add_form_options(result)
        return result

    def _get_form_sections(self):
        sections = {}
        sections['general'] = {
                "edit": bool(self.request.has_permission('edit.expensesheet')),
            }
        return sections

    def _get_status_actions(self):
        """
        Returned datas describing available actions on the current item
        :returns: List of actions
        :rtype: list of dict
        """
        actions = []
        url = self.request.current_route_path(_query={'action': 'status'})
        for action in self.context.validation_state_manager.\
                get_allowed_actions(
                    self.request
                ):
            json_resp = action.__json__(self.request)
            json_resp['url'] = url
            json_resp['widget'] = 'status'
            actions.append(json_resp)
        return actions

    def _get_other_actions(self):
        """
        Return the description of other available actions :
            duplicate
            ...
        """
        result = []

        if self.request.has_permission('add_payment.expensesheet'):
            url = self.request.route_path(
                "/expenses/{id}/addpayment",
                id=self.context.id,
            )
            result.append({
                'widget': 'anchor',
                'option': {
                    "url": url,
                    "title": "Enregistrer un paiement pour cette note "
                    "de dépenses",
                    "css": "btn icon only",
                    "icon": "euro-circle",
                }
            })

        if self.request.has_permission('view.expensesheet'):
            result.append(self._duplicate_btn())
        result.append(self._print_btn())
        result.append(self._xls_btn())

        if self.request.has_permission('delete.expensesheet'):
            result.append(self._delete_btn())
        return result

    def _delete_btn(self):
        """
        Return a deletion btn description

        :rtype: dict
        """
        url = self.request.route_path(
            "/expenses/{id}/delete",
            id=self.context.id
        )
        return {
            'widget': 'POSTButton',
            'option': {
                "url": url,
                "title": "Supprimer définitivement ce document",
                "css": "btn icon only negative",
                "icon": "trash-alt",
                "confirm_msg": "Êtes-vous sûr de vouloir supprimer "
                "cet élément ?",
            }
        }

    def _print_btn(self):
        """
        Return a print btn for frontend printing
        """
        return {
            "widget": 'button',
            'option': {
                'title': 'Imprimer',
                'css': 'btn icon only',
                'onclick': 'window.print()',
                'icon': 'print'
            }
        }

    def _xls_btn(self):
        """
        Return a button for xls rendering

        :rtype: dict
        """
        url = self.request.route_path(
            "/expenses/{id}.xlsx",
            id=self.context.id
        )
        return {
            'widget': 'anchor',
            'option': {
                "url": url,
                "title": "Export au format Excel (xls)",
                "css": "btn icon only",
                "icon": "file-excel",
            }
        }

    def _duplicate_btn(self):
        """
        Return a duplicate btn description

        :rtype: dict
        """
        url = self.request.route_path(
            "/expenses/{id}/duplicate",
            id=self.context.id
        )
        return {
            'widget': 'anchor',
            'option': {
                "url": url,
                "title": "Créer une nouvelle note de dépenses à partir "
                "de celle-ci",
                "css": "btn icon only",
                "icon": "copy",
            }
        }

    def _get_justified_toggle(self):
        """
        Return a justification toggle button description

        :rtype: dict
        """
        url = self.request.route_path(
            "/api/v1/expenses/{id}",
            id=self.context.id,
            _query={'action': 'justified_status'}
        )
        actions = self.context.justified_state_manager.get_allowed_actions(
            self.request
        )

        return {
            "widget": "toggle",
            'options': {
                "url": url,
                "name": "justified",
                "current_value": self.context.justified,
                "label": "Justificatifs",
                "buttons": actions,
                "css": "btn",
            }
        }
        return

    def _add_form_options(self, form_config):
        """
        add form options to the current configuration
        """
        options = self._get_type_options()

        options['categories'] = [
            {
                'value': '1',
                'label': 'Frais généraux',
                'description': 'Dépenses liées au fonctionnement de votre enseigne'
            },
            {
                'value': '2',
                'label': 'Achats client',
                'description': 'Dépenses concernant directement votre activité \
    auprès de vos clients'
            }]

        options['bookmarks'] = get_bookmarks(self.request)

        options['expenses'] = self._get_existing_expenses_options()

        expense_sheet = self.request.context
        month = expense_sheet.month
        year = expense_sheet.year

        date = datetime.date(year, month, 1)
        options['today'] = date
        options['csrf_token'] = get_csrf_token(self.request)

        options['businesses'] = self._get_businesses_options()

        form_config['options'] = options
        return form_config

    def _get_type_options(self):
        # Load types already used in this expense
        active_or_used = ExpenseType.query_active_or_used_in(
            self.context.lines,
        )

        options = {
            "expense_types": active_or_used.filter_by(
                type='expense'
            ).all(),
            "expensetel_types": active_or_used.filter_by(
                type='expensetel'
            ).all(),
            "expensekm_types": ExpenseKmType.query_allowed_driver_or_used_in(
                self.context.user,
                self.context.year,
                self.context.lines,
                self.context.kmlines,
            ).all(),
        }
        return options

    def _get_existing_expenses_options(self):
        """
        Return existing expenses available for expense line duplication
        """
        result = [{
            "label": "{month_label} / {year} (note courante)".format(
                month_label=strings.month_name(self.context.month),
                year=self.context.year,
            ),
            "id": self.context.id,
        }]
        all_expenses = _get_valid_duplicate_targets(
            self.context,
            including_me=False,
        )
        all_expenses = all_expenses.order_by(
            ExpenseSheet.year.desc()
        ).order_by(
            ExpenseSheet.month.desc()
        )
        result.extend([
            {
                "label": "{month_label} / {year}".format(
                    month_label=strings.month_name(e.month),
                    year=e.year
                ),
                "id": e.id
            } for e in all_expenses
        ])
        return result

    def _get_businesses_options(self):
        """
        Return list of company's businesses, grouped by customers and projects
        """
        return self.context.company.get_business_nested_options()


class RestExpenseLineView(BaseRestView):
    """
    Base rest view for expense line handling
    """
    def get_schema(self, submitted):
        return get_add_edit_line_schema(ExpenseLine)

    def collection_get(self):
        return self.context.lines

    def post_format(self, entry, edit, attributes):
        """
        Associate a newly created element to the parent task
        """
        if not edit:
            entry.sheet = self.context
            entry.expense_type = ExpenseType.get(entry.type_id)

        if entry.expense_type.tva_on_margin:
            try:
                tva_value = Tva.get_default().value
            except AttributeError:
                tva_value = 2000  # integer format of 20%
            entry.ht = compute_ht_from_ttc(entry.manual_ttc, tva_value)
            entry.tva = compute_tva_from_ttc(entry.manual_ttc, tva_value)
        else:
            entry.manual_ttc = 0
        return entry

    def duplicate(self):
        """
        Duplicate an expense line to an existing ExpenseSheet
        """
        logger.info("Duplicate ExpenseLine")
        sheet_id = self.request.json_body.get('sheet_id')
        # queries only among authorized expense sheets
        valid_sheets = _get_valid_duplicate_targets(self.context.sheet)
        sheet = valid_sheets.filter_by(id=sheet_id).first()

        if sheet is None:
            return RestError(["Wrong sheet_id"])

        if not self.request.has_permission('edit.expensesheet'):
            logger.error("Unauthorized action : possible break in attempt")
            raise HTTPForbidden()

        new_line = self.context.duplicate(sheet=sheet)
        self.request.dbsession.add(new_line)
        self.request.dbsession.flush()
        return new_line


class RestExpenseKmLineView(BaseRestView):
    """
    Base rest view for expense line handling
    """
    def get_schema(self, submitted):
        schema = get_add_edit_line_schema(ExpenseKmLine)
        return schema

    def collection_get(self):
        return self.context.kmlines

    def post_format(self, entry, edit, attributes):
        """
        Associate a newly created element to the parent task
        """
        if not edit:
            entry.sheet = self.context
        return entry

    def duplicate(self):
        """
        Duplicate an expense line to an existing ExpenseSheet
        """
        logger.info("Duplicate ExpenseKmLine")
        sheet_id = self.request.json_body.get('sheet_id')
        sheet = ExpenseSheet.get(sheet_id)

        if sheet is None:
            return RestError(["Wrong sheet_id"])

        if not self.request.has_permission('edit.expensesheet'):
            logger.error("Unauthorized action : possible break in attempt")
            raise HTTPForbidden()

        new_line = self.context.duplicate(sheet=sheet)
        if new_line is None:
            return RestError([
                "Aucun type de dépense kilométrique correspondant n'a pu être "
                "retrouvé sur l'année {0}".format(sheet.year)
            ], code=403)

        new_line.sheet_id = sheet.id
        self.request.dbsession.add(new_line)
        self.request.dbsession.flush()
        return new_line


class RestBookMarkView(BaseView):
    """
        Json rest-api for expense bookmarks handling
    """
    _schema = BookMarkSchema()

    @property
    def schema(self):
        return self._schema.bind(request=self.request)

    def get(self):
        """
            Rest GET Method : get
        """
        return get_bookmarks(self.request)

    def post(self):
        """
            Rest POST method : add
        """
        logger.debug("In the bookmark edition")

        appstruct = self.request.json_body
        try:
            bookmark = self.schema.deserialize(appstruct)
        except colander.Invalid as err:
            traceback.print_exc()
            logger.exception("  - Error in posting bookmark")
            logger.exception(appstruct)
            raise RestError(err.asdict(), 400)
        handler = BookMarkHandler(self.request)
        bookmark = handler.store(bookmark)
        return bookmark

    def put(self):
        """
            Rest PUT method : edit
        """
        self.post()

    def delete(self):
        """
            Removes a bookmark
        """
        logger.debug("In the bookmark deletion view")

        handler = BookMarkHandler(self.request)

        # Retrieving the id from the request
        id_ = self.request.matchdict.get('id')

        bookmark = handler.delete(id_)

        # if None is returned => there was no bookmark with this id
        if bookmark is None:
            raise RestError({}, 404)
        else:
            return dict(status="success")


class RestExpenseSheetStatusView(StatusView):
    def redirect(self):
        loc = self.request.route_path("/expenses/{id}", id=self.context.id)
        return dict(redirect=loc)

    def check_allowed(self, status):
        self.request.context.check_status_allowed(status, self.request)

    def pre_status_process(self, status, params):
        self.context.statuses.append(
            StatusLogEntry(
                status=status,
                comment=params.get('comment'),
                user_id=self.request.user.id,
                state_manager_key='status',
            )
        )

        return StatusView.pre_status_process(self, status, params)

    def post_status_process(self, status, params):
        """
        Launch post status process functions

        :param str status: The new status that should be affected
        :param dict params: The params that were transmitted by the associated
        State's callback
        """
        logger.debug("post_status_process")
        self.context.status_date = datetime.datetime.now()
        self.request.dbsession.merge(self.context)
        StatusView.post_status_process(self, status, params)


class RestExpenseSheetJustifiedStatusView(StatusView):
    def check_allowed(self, status):
        self.request.context.check_justified_status_allowed(
            status,
            self.request
        )

    def notify(self, status):
        if status is True:
            self.request.registry.notify(
                StatusChanged(self.request, self.context, 'justified')
            )

    def redirect(self):
        return Apiv1Resp(
            self.request, {'justified': self.context.justified}
        )

    def _get_status(self, params):
        status = params['submit']

        if status in ('True', 'true'):
            status = True
        elif status in ('False', 'false'):
            status = False
        return status

    def pre_status_process(self, status, params):
        logger.debug("In pre_status_process : %s %s" % (status, type(status)))
        self.context.statuses.append(
            StatusLogEntry(
                comment=params.get('comment'),
                user_id=self.request.user.id,
                status='justified' if status else 'waiting',
                state_manager_key='justified_status',
            )
        )

        return StatusView.pre_status_process(self, status, params)

    def status_process(self, status, params):
        return self.context.set_justified_status(
            status,
            self.request,
            **params
        )


def add_routes(config):
    """
    Add module's related routes
    """
    config.add_route("/api/v1/bookmarks/{id}", r"/api/v1/bookmarks/{id:\d+}")
    config.add_route("/api/v1/bookmarks", "/api/v1/bookmarks")

    config.add_route(
        "/api/v1/expenses",
        "/api/v1/expenses",
    )

    config.add_route(
        "/api/v1/expenses/{id}",
        r"/api/v1/expenses/{id:\d+}",
        traverse="/expenses/{id}"
    )

    config.add_route(
        "/api/v1/expenses/{id}/lines",
        "/api/v1/expenses/{id}/lines",
        traverse="/expenses/{id}"
    )

    config.add_route(
        "/api/v1/expenses/{id}/lines/{lid}",
        r"/api/v1/expenses/{id:\d+}/lines/{lid:\d+}",
        traverse="/expenselines/{lid}",
    )

    config.add_route(
        "/api/v1/expenses/{id}/kmlines",
        r"/api/v1/expenses/{id:\d+}/kmlines",
        traverse="/expenses/{id}",
    )

    config.add_route(
        "/api/v1/expenses/{id}/kmlines/{lid}",
        r"/api/v1/expenses/{id:\d+}/kmlines/{lid:\d+}",
        traverse="/expenselines/{lid}",
    )


def add_views(config):
    """
    Add rest api views
    """
    config.add_rest_service(
        RestExpenseSheetView,
        "/api/v1/expenses/{id}",
        collection_route_name="/api/v1/expenses",
        view_rights="view.expensesheet",
        add_rights="add.expensesheet",
        edit_rights="edit.expensesheet",
        delete_rights="delete.expensesheet",
    )

    # Form configuration view
    config.add_view(
        RestExpenseSheetView,
        attr='form_config',
        route_name='/api/v1/expenses/{id}',
        renderer='json',
        request_param="form_config",
        permission='view.expensesheet',
        xhr=True,
    )

    # Status view
    config.add_view(
        RestExpenseSheetStatusView,
        route_name='/api/v1/expenses/{id}',
        request_param='action=status',
        permission="view.expensesheet",
        request_method='POST',
        renderer="json",
        xhr=True,
    )

    # Status view
    config.add_view(
        RestExpenseSheetJustifiedStatusView,
        route_name='/api/v1/expenses/{id}',
        request_param='action=justified_status',
        permission="view.expensesheet",
        request_method='POST',
        renderer="json",
        xhr=True,
    )

    # Line views
    config.add_rest_service(
        RestExpenseLineView,
        "/api/v1/expenses/{id}/lines/{lid}",
        collection_route_name="/api/v1/expenses/{id}/lines",
        view_rights="view.expensesheet",
        add_rights="edit.expensesheet",
        edit_rights="edit.expensesheet",
        delete_rights="edit.expensesheet",
    )
    config.add_view(
        RestExpenseLineView,
        attr='duplicate',
        route_name="/api/v1/expenses/{id}/lines/{lid}",
        request_param='action=duplicate',
        permission="edit.expensesheet",
        request_method='POST',
        renderer="json",
        xhr=True,
    )

    # Km Line views
    config.add_rest_service(
        RestExpenseKmLineView,
        "/api/v1/expenses/{id}/kmlines/{lid}",
        collection_route_name="/api/v1/expenses/{id}/kmlines",
        view_rights="view.expensesheet",
        add_rights="edit.expensesheet",
        edit_rights="edit.expensesheet",
        delete_rights="edit.expensesheet",
    )
    config.add_view(
        RestExpenseKmLineView,
        attr='duplicate',
        route_name="/api/v1/expenses/{id}/kmlines/{lid}",
        request_param='action=duplicate',
        permission="edit.expensesheet",
        request_method='POST',
        renderer="json",
        xhr=True,
    )
    # BookMarks
    config.add_rest_service(
        RestBookMarkView,
        "/api/v1/bookmarks/{id}",
        collection_route_name="/api/v1/bookmarks",
        view_rights="view",
        add_rights="view",
        edit_rights='view',
        delete_rights="view",
    )


def includeme(config):
    add_routes(config)
    add_views(config)
