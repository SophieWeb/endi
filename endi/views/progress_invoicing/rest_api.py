import colander
import logging
from pyramid.httpexceptions import (
    HTTPNotFound,
    HTTPForbidden,
)

from endi.utils import rest
from endi.views import BaseRestView
from endi.models.progress_invoicing import ProgressInvoicingGroupStatus
from endi.forms.progress_invoicing import get_edit_schema

from .routes import (
    GROUP_ROUTE,
    GROUP_ITEM_ROUTE,
    CINV_GROUP_ITEM_ROUTE,
    CINV_GROUP_ROUTE,
)
from .tools import (
    AbstractProgressTaskLineGroup,
)

logger = logging.getLogger(__name__)


class ProgressInvoicingRestView(BaseRestView):
    """
    Rest api for progress invoicing configuration
    """
    schema = get_edit_schema()

    def collection_get(self):
        business = self.context.business
        result = [
            AbstractProgressTaskLineGroup(
                group_status,
                self.context.id
            ) for group_status in business.progress_invoicing_group_statuses
        ]
        return [group for group in result if group.current_element]

    def _get_current_status(self):
        group_id = self.request.matchdict['group_id']
        group_status = ProgressInvoicingGroupStatus.get(group_id)
        if not group_status:
            raise HTTPNotFound()
        return group_status

    def get(self):
        group_status = self._get_current_status()
        return AbstractProgressTaskLineGroup(group_status, self.context.id)

    def put(self):
        self.logger.info("PUT request")
        submitted = self.request.json_body
        logger.debug(submitted)
        schema = self.schema.bind(request=self.request)

        try:
            attributes = schema.deserialize(submitted)
        except colander.Invalid as err:
            self.logger.exception("  - Erreur")
            self.logger.exception(submitted)
            raise rest.RestError(err.asdict(), 400)

        line_config = {}
        for line in attributes['lines']:
            line_config[line['id']] = line['current_percent']
        group_status = self._get_current_status()
        group_status.get_or_generate(line_config, self.context.id)
        return AbstractProgressTaskLineGroup(group_status, self.context.id)

    def post(self):
        raise HTTPForbidden()

    def delete(self):
        raise HTTPForbidden()


def includeme(config):
    config.add_rest_service(
        ProgressInvoicingRestView,
        GROUP_ITEM_ROUTE,
        collection_route_name=GROUP_ROUTE,
        view_rights="view.invoice",
        add_rights="edit.invoice",
        edit_rights='edit.invoice',
        delete_rights='edit.invoice',
    )
    config.add_rest_service(
        ProgressInvoicingRestView,
        CINV_GROUP_ITEM_ROUTE,
        collection_route_name=CINV_GROUP_ROUTE,
        view_rights="view.cancelinvoice",
        add_rights="edit.cancelinvoice",
        edit_rights='edit.cancelinvoice',
        delete_rights='edit.cancelinvoice',
    )
