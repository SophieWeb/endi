from endi.models.progress_invoicing import (
    ProgressInvoicingGroup,
    ProgressInvoicingLine,
)
from endi.compute.math_utils import integer_to_amount


class AbstractProgressTaskItem:
    _invoice_memory_class = None

    def __init__(self, status, invoice_id):
        self.status = status
        self.invoice_id = invoice_id
        self.invoiced = self._invoice_memory_class.\
            find_by_status_and_invoice_id(
                self.status,
                self.invoice_id
            )

    def _get_current_value(self):
        """
        Try to retrieve the current percentage value

        :returns: The percentage value or None
        :rtype: float
        """
        result = None
        if self.invoiced is not None:
            result = self.invoiced.percentage
        return result

    def _get_invoiced_task_item_id(self):
        """
        Retrieve the id of the task item that has been invoiced yet
        """
        pass

    def _get_percent_left(self):
        """
        Compute the percent left before invoicing the current invoice

        :returns: The percent left for the current invoice
        :rtype: float
        """
        exclude_id = self._get_invoiced_task_item_id()
        percentage = self._invoice_memory_class.find_invoiced_percentage(
            self.status, exclude_id=exclude_id
        )
        result = None
        if percentage is not None:
            result = 100 - percentage
        return result

    def _get_source_json_repr(self):
        """
        Renvoie une représentation json de l'objet source (TaskLine ou
        TaskLineGroup)

        :rtype: dict
        """
        return {}

    def __json__(self, request):
        result = {
            "id": self.status.id,
            "percent_left": self._get_percent_left(),
            "total_ht_to_invoice": integer_to_amount(
                self.status.total_ht_to_invoice(), 5, 0
            ),
            "tva_to_invoice": integer_to_amount(
                self.status.tva_to_invoice(), 5, 0
            ),
            "total_ttc_to_invoice": integer_to_amount(
                self.status.total_ttc_to_invoice(), 5, 0
            ),
            "current_percent": self._get_current_value(),
            "current_percent_left": self.status.current_percent_left(),
            "current_total_ht_left": integer_to_amount(
                self.status.current_total_ht_left()
            ),
            "has_deposit": self.status.has_deposit(),
        }
        result.update(self._get_source_json_repr())
        return result


class AbstractProgressTaskLine(AbstractProgressTaskItem):
    """
    Task Line related object used to wrap a TaskLine and its
    ProgressInvoicingLineStatus

    It's used to store a percentage value that will be remembered and that will
    be used to generate a TaskLine
    """
    _invoice_memory_class = ProgressInvoicingLine

    def __init__(self, status, invoice_id):
        AbstractProgressTaskItem.__init__(self, status, invoice_id)
        self.source = self.status.source_task_line
        self.current_element = self.invoiced.task_line

    def _get_source_json_repr(self):
        """
        Renvoie une représentation json de l'objet source (TaskLine ou
        TaskLineGroup)

        :rtype: dict
        """
        return {
            "description": self.source.description,
            "tva": integer_to_amount(self.source.tva, 2),
            "product_id": self.source.product_id,
            "total_ht": integer_to_amount(
                self.current_element.total_ht(),
                5
            ),
            "tva_amount": integer_to_amount(
                self.current_element.tva_amount(),
                5
            ),
            "total_ttc": integer_to_amount(
                self.current_element.total(),
                5
            ),
        }

    def _get_invoiced_task_item_id(self):
        """
        Retrieve the id of the task item that has been invoiced yet
        """
        result = self.invoiced.task_line_id
        return result

    def __json__(self, request):
        result = AbstractProgressTaskItem.__json__(self, request)
        result["group_status_id"] = self.status.group_status_id
        return result


class AbstractProgressTaskLineGroup(AbstractProgressTaskItem):
    """
    Task Line group related object used to wrap a TaskLineGroup and its
    ProgressInvoicingGroupStatus

    It's used to store a percentage value that will be remembered and that will
    be used to generate a TaskLineGroup
    """
    _invoice_memory_class = ProgressInvoicingGroup

    def __init__(self, status, invoice_id):
        AbstractProgressTaskItem.__init__(self, status, invoice_id)
        self.source = self.status.source_task_line_group
        if self.invoiced:
            self.current_element = self.invoiced.task_line_group
        else:
            # Cas où on a rajouté un devis entre temps, aucun groupe de la task
            # ne correspond au statut d'avancement de la facturation
            self.current_element = None

    def _get_invoiced_task_item_id(self):
        """
        Retrieve the id of the task item that has been invoiced yet
        """
        result = self.invoiced.task_line_group_id
        return result

    def _get_source_json_repr(self):
        """
        Renvoie une représentation json de l'objet source (TaskLine ou
        TaskLineGroup)

        :rtype: dict
        """
        line_statuses = [
                AbstractProgressTaskLine(line_status, self.invoice_id)
                for line_status in self.status.line_statuses
            ]

        result = {
            "title": self.source.title,
            "description": self.source.description,
            "line_statuses": line_statuses,
            "total_ht": integer_to_amount(
                self.current_element.total_ht(),
                5
            ),
            "tva_amount": integer_to_amount(
                self.current_element.tva_amount(),
                5
            ),
            "total_ttc": integer_to_amount(
                self.current_element.total_ttc(),
                5
            ),
        }
        return result
