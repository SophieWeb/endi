import os

GROUP_ROUTE = "/api/v1/invoices/{id}/progress_invoicing/groups"
GROUP_ITEM_ROUTE = os.path.join(GROUP_ROUTE, "{group_id}")
CINV_GROUP_ROUTE = "/api/v1/cancelinvoices/{id}/progress_invoicing/groups"
CINV_GROUP_ITEM_ROUTE = os.path.join(CINV_GROUP_ROUTE, "{group_id}")


def includeme(config):
    for route in (
        GROUP_ROUTE, GROUP_ITEM_ROUTE, CINV_GROUP_ROUTE, CINV_GROUP_ITEM_ROUTE
    ):
        config.add_route(route, route, traverse="/tasks/{id}")
