import datetime
import logging

from pyramid.csrf import get_csrf_token
from pyramid.httpexceptions import (
    HTTPForbidden,
)
from sqlalchemy import inspect

from endi.models.node import Node
from endi.models.expense.types import (
    ExpenseType,
)
from endi.forms.third_party.supplier import get_company_suppliers_from_request
from endi.models.supply import (
    SupplierInvoice,
    SupplierInvoiceLine,
    SupplierOrder,
    SupplierOrderLine,
    InternalSupplierInvoice,
    InternalSupplierOrder,
)
from endi.utils.rest import (
    RestError,
)
from endi.utils.strings import format_amount
from endi.forms.supply.supplier_invoice import get_supplier_invoice_edit_schema
from endi.forms.supply import get_add_edit_line_schema
from endi.forms.supply.supplier_order import get_supplier_order_edit_schema

from endi.views import BaseRestView
from endi.views.status import StatusView

logger = logging.getLogger(__name__)


class BaseRestSupplierDocumentView(BaseRestView):
    """
    Factorize what is common among RestSupplierInvoiceView and
    RestSupplierOrderView
    """
    def post_format(self, entry, edit, attributes):
        """
        Add the company and user id after  add
        """
        if not edit:
            entry.company_id = self.context.id
            entry.user_id = self.request.user.id
        return entry

    def form_config(self):
        """
        Form display options

        :returns: The sections that the end user can edit, the options
        available for the different select boxes
        """
        result = {
            "actions": {
                'main': self._get_status_actions(),
                'more': self._get_other_actions(),
            },
            "sections": self._get_form_sections(),
        }
        result = self._add_form_options(result)
        return result

    def _get_status_actions(self):
        """
        Returned datas describing available actions on the current item
        :returns: List of actions
        :rtype: list of dict
        """
        actions = []
        url = self.request.current_route_path(
            _query={'action': 'validation_status'}
        )
        for action in self.context.validation_state_manager.\
                get_allowed_actions(
                    self.request
                ):
            json_resp = action.__json__(self.request)
            json_resp['url'] = url
            json_resp['widget'] = 'status'
            actions.append(json_resp)
        return actions

    def _delete_btn(self):
        """
        Return a deletion btn description

        :rtype: dict
        """
        url = self.get_delete_route()
        return {
            'widget': 'POSTButton',
            'option': {
                "url": url,
                "title": "Supprimer définitivement ce document",
                "css": "icon only negative",
                "icon": "trash-alt",
                "confirm_msg": "Êtes-vous sûr de vouloir "
                "supprimer cet élément ?"
            }
        }

    def _get_expense_types_options(self):
        query = ExpenseType.query_active_or_used_in(
            self.context.lines,
        )
        query = query.filter_by(type='expense')
        if self.context.internal:
            query = query.filter_by(internal=True)
        else:
            query = query.filter_by(internal=False)
        return query.all()

    def _get_suppliers_options(self):
        query = get_company_suppliers_from_request(self.request)
        result = [
            {"label": supplier.name, "value": supplier.id}
            for supplier in query
        ]
        return result

    def _add_form_options(self, form_config):
        """
        add form options to the current configuration
        """
        options = {}

        options['today'] = datetime.date.today()
        options['expense_types'] = self._get_expense_types_options()
        options['csrf_token'] = get_csrf_token(self.request)
        company = self.context.company
        options['businesses'] = company.get_business_nested_options()
        form_config['options'] = options
        return form_config

    def get_writable_instances(self):
        Model = self.model_class
        query = Model.query().filter_by(
            company_id=self.context.company_id
        )

        query = query.filter(
            Model.type_.notin_(
                ('internalsupplier_invoice', 'internalsupplier_order')
            )
        )
        query = query.filter(Model.status.in_(['draft', 'invalid']))
        query = query.order_by(
            Model.created_at.desc(),
        )
        return query


class RestSupplierOrderView(BaseRestSupplierDocumentView):
    model_class = SupplierOrder

    def get_schema(self, submited):
        return get_supplier_order_edit_schema(self.context.internal)

    def get_delete_route(self):
        return self.request.route_path(
            "/suppliers_orders/{id}",
            id=self.context.id,
            _query=dict(action="delete"),
        )

    def _get_form_sections(self):
        editable = bool(self.request.has_permission('edit.supplier_order'))
        sections = {
            'general': {
                'edit': editable,
                'supplier_id': {'edit': editable},
                'cae_percentage': {'edit': editable},
            },
            'lines': {
                'edit': editable,
                'add': editable,
                'delete': editable,
                'ht': {'edit': editable},
                'tva': {'edit': editable},
            },
        }
        return sections

    def _get_other_actions(self):
        """
        Return the description of other available actions :
            duplicate
            ...
        """
        result = []

        if self.request.has_permission('delete.supplier_order'):
            result.append(self._delete_btn())

        if self.request.has_permission('duplicate.supplier_order'):
            result.append(self._duplicate_btn())

        return result

    def _duplicate_btn(self):
        """
        Return a duplicate btn description

        :rtype: dict
        """
        url = self.request.route_path(
            "/suppliers_orders/{id}",
            id=self.context.id,
            _query=dict(action="duplicate"),
        )
        return {
            'widget': 'POSTButton',
            'option': {
                "url": url,
                "label": "",
                "title": "Créer une nouvelle commande fournisseurà partir "
                "de celle-ci",
                "css": "btn icon only",
                "icon": "copy",
            }
        }

    def _get_duplicate_targets_options(self):
        query = self.get_writable_instances()
        result = [
            {
                "label": "{}{}".format(
                    order.name,
                    " (commande courante)" if order == self.context else ''
                ),
                "id": order.id,
            }
            for order in query
        ]
        return result

    def _add_form_options(self, form_config):
        form_config = super(RestSupplierOrderView, self)._add_form_options(
            form_config,
        )
        orders_options = self._get_duplicate_targets_options()
        form_config['options']['suppliers_orders'] = orders_options
        form_config['options']['suppliers'] = self._get_suppliers_options()
        return form_config


class RestInternalSupplierOrderView(RestSupplierOrderView):
    def _get_suppliers_options(self):
        return [
            {
                'label': self.context.supplier.label,
                'value': self.context.supplier_id,
            }
        ]

    def _add_form_options(self, form_config):
        form_config = BaseRestSupplierDocumentView._add_form_options(
            self, form_config
        )
        form_config['options']['suppliers_orders'] = []
        form_config['options']['suppliers'] = self._get_suppliers_options()
        return form_config

    def _get_form_sections(self):
        sections = RestSupplierOrderView._get_form_sections(self)
        sections['lines']['add'] = False
        sections['lines']['delete'] = False
        sections['lines']['ht']['edit'] = False
        sections['lines']['tva']['edit'] = False
        sections['general']['cae_percentage']['edit'] = False
        sections['general']['supplier_id']['edit'] = False
        return sections


class RestSupplierInvoiceView(BaseRestSupplierDocumentView):
    model_class = SupplierInvoice

    def get_schema(self, submited):
        return get_supplier_invoice_edit_schema(self.context.internal)

    def post_format(self, entry, edit, attributes):
        entry = super(RestSupplierInvoiceView, self).post_format(
            entry, edit, attributes
        )
        history = inspect(entry).attrs.supplier_orders.history
        lines_query = SupplierInvoiceLine.query().filter_by(
            supplier_invoice_id=entry.id,
        )

        if history.deleted is not None:
            removed_orders_ids = [i.id for i in history.deleted]
            delete_query = lines_query.join(
                'source_supplier_order_line'
            ).filter(
                SupplierOrderLine.supplier_order_id.in_(removed_orders_ids),
            )
            lines_to_delete = SupplierInvoiceLine.query().filter(
                SupplierInvoiceLine.id.in_([i.id for i in delete_query])
            )
            if lines_to_delete.count() > 0:
                lines_to_delete.delete(synchronize_session='fetch')

        if (history.added is not None) and (len(history.added) > 0):
            for order in history.added:
                entry.import_lines_from_order(order)

            entry.supplier_id = history.added[0].supplier_id
            entry.cae_percentage = history.added[0].cae_percentage

        return entry

    def get_delete_route(self):
        return self.request.route_path(
            "/suppliers_invoices/{id}",
            id=self.context.id,
            _query=dict(action="delete"),
        )

    def _get_form_sections(self):
        editable = bool(self.request.has_permission('edit.supplier_invoice'))
        has_orders = bool(self.context.supplier_orders)
        sections = {
            'general': {
                'edit': editable,
                'date': {'edit': editable},
                'supplier_id': {'edit': not has_orders and editable},
                'cae_percentage': {'edit': not has_orders and editable},
                'payer_id': {},
                'supplier_orders': {'edit': editable},
            },
            'lines': {
                'edit': editable,
                'add': editable,
                'delete': editable,
                'ht': {'edit': editable},
                'tva': {'edit': editable},
            },
        }
        return sections

    def _get_other_actions(self):
        """
        Return the description of other available actions :
            duplicate
            ...
        """
        result = []
        if self.request.has_permission('delete.supplier_invoice'):
            result.append(self._delete_btn())

        if self.request.has_permission('add_payment.supplier_invoice'):
            if self.context.cae_percentage > 0:
                result.append(self._supplier_payment_btn())
            if self.context.cae_percentage < 100:
                result.append(self._user_payment_btn())

        # if self.request.has_permission('view.supplier_invoice'):
        #     result.append(self._duplicate_btn())

        return result

    def _get_duplicate_targets_options(self):
        """
        Build the option list to target on which document we want to duplicate
        a line.
        """
        query = self.get_writable_instances()
        result = [
            {
                "label": "{}{}".format(
                    invoice.name,
                    " (facture courante)" if invoice == self.context else ''
                ),
                "id": invoice.id,
            }
            for invoice in query
        ]
        return result

    def _get_suppliers_orders_options(self):
        # Returns available orders that belongs to the invoice company
        query = SupplierOrder.query().filter(
            SupplierOrder.company_id == self.context.company_id,
            (
                (SupplierOrder.supplier_invoice_id == self.context.id)
                |
                (SupplierOrder.supplier_invoice_id == None)  # noqa
            )
        )

        result = [
            {
                "label": "{} ({}€ TTC)".format(
                    order.name,
                    format_amount(order.total, grouping=False)
                ),
                "id": order.id,
                "supplier_id": order.supplier_id,
            }
            for order in query
        ]
        return result

    def _get_payers_options(self):
        assert isinstance(self.context, SupplierInvoice)

        # Returns active users or currently selected user
        for emp in self.context.company.employees:
            if (
                    (emp.login and emp.login.active)
                    or
                    (emp == self.context.payer)
            ):
                yield dict(label=emp.label, value=emp.id)

    def _add_form_options(self, form_config):
        form_config = super(RestSupplierInvoiceView, self)._add_form_options(
            form_config,
        )
        invoices_options = self._get_duplicate_targets_options()
        orders_options = self._get_suppliers_orders_options()
        suppliers_options = self._get_suppliers_options()
        payers_options = list(self._get_payers_options())

        form_config['options']['suppliers_invoices'] = invoices_options
        form_config['options']['suppliers_orders'] = orders_options
        form_config['options']['suppliers'] = suppliers_options
        form_config['options']['payers'] = payers_options

        return form_config

    def _supplier_payment_btn(self):
        url = self.request.route_path(
            "/suppliers_invoices/{id}/add_supplier_payment",
            id=self.context.id,
        )
        return {
            'widget': 'anchor',
            'option': {
                "url": url,
                "title": "Enregistrer un paiement fournisseur",
                "label": "Payer le fournisseur",
                "css": "btn icon btn-primary",
                "icon": "euro-circle",
            }
        }

    def _user_payment_btn(self):
        url = self.request.route_path(
            "/suppliers_invoices/{id}/add_user_payment",
            id=self.context.id,
        )
        return {
            'widget': 'anchor',
            'option': {
                "url": url,
                "title": "Enregistrer un remboursement entrepreneur",
                "label": "Rembourser l'entrepreneur",
                "css": "btn icon btn-primary",
                "icon": "euro-circle",
            }
        }


class RestInternalSupplierInvoiceView(RestSupplierInvoiceView):
    def _get_suppliers_options(self):
        return [
            {
                'label': self.context.supplier.label,
                'value': self.context.supplier_id,
            }
        ]

    def _add_form_options(self, form_config):
        form_config = BaseRestSupplierDocumentView._add_form_options(
            self, form_config
        )
        form_config['options']['suppliers_orders'] = []
        form_config['options']['suppliers'] = []
        form_config['options']['suppliers_invoices'] = []
        form_config['options']['payers'] = []
        return form_config

    def _get_form_sections(self):
        sections = RestSupplierInvoiceView._get_form_sections(self)
        sections['lines']['add'] = False
        sections['lines']['delete'] = False
        sections['lines']['ht']['edit'] = False
        sections['lines']['tva']['edit'] = False
        sections['general']['date']['edit'] = False
        sections['general'].pop('supplier_id')
        sections['general'].pop('cae_percentage')
        sections['general'].pop('payer_id')
        sections['general']['supplier_orders']['edit'] = False
        return sections


class BaseRestLineView(BaseRestView):
    """
    Logic is shared between SupplierOrderLine and SupplierInvoiceLine

    Subclass must define class attrs :

    - model_class
    - fk_field_to_container
    - duplicate_permission : the duplicate permission for destination container
        instance.
    """

    def _get_current_document(self):
        if isinstance(self.context, Node):
            return self.context
        elif isinstance(self.context, SupplierOrderLine):
            return self.context.supplier_order
        elif isinstance(self.context, SupplierInvoiceLine):
            return self.context.supplier_invoice
        else:
            raise KeyError("On ne devrait pas arriver ici")

    def get_schema(self, submitted):
        document = self._get_current_document()
        return get_add_edit_line_schema(self.model_class, document.internal)

    def collection_get(self):
        return self.context.lines

    def post_format(self, entry, edit, attributes):
        """
        Associate a newly created line to current container
        """
        if not edit:
            setattr(entry, self.fk_field_to_container, self.context.id)
        return entry

    def duplicate(self):
        """
        Duplicate line to an existing container of same type as context.
        """
        logger.info("Duplicate {}".format(self.model_class))
        container_id = self.request.json_body.get(self.fk_field_to_container)

        if container_id is None:
            return RestError(["Wrong {}".format(self.fk_field_to_container)])

        # Permission for source object is handled though add_view predicate
        dest_obj = SupplierOrder.get(container_id)
        if not self.request.has_permission(
            self.duplicate_permission, dest_obj
        ):
            logger.error("Unauthorized action : possible break in attempt")
            raise HTTPForbidden()

        duplicate_kwargs = {self.fk_field_to_container: container_id}
        new_line = self.context.duplicate(**duplicate_kwargs)
        self.request.dbsession.add(new_line)
        self.request.dbsession.flush()
        return new_line


class RestSupplierOrderLineView(BaseRestLineView):
    model_class = SupplierOrderLine
    fk_field_to_container = 'supplier_order_id'
    duplicate_permission = 'edit.supplier_order'


class RestSupplierInvoiceLineView(BaseRestLineView):
    model_class = SupplierInvoiceLine
    fk_field_to_container = 'supplier_invoice_id'
    duplicate_permission = 'edit.supplier_invoice'


class BaseSupplierValidationStatusView(StatusView):
    def check_allowed(self, status):
        document = self.request.context
        document.check_validation_status_allowed(status, self.request)

    def status_process(self, status, params):
        return self.context.set_validation_status(
            status,
            self.request,
            **params
        )

    def pre_status_process(self, status, params):
        if 'comment' in params:
            self.context.status_comment = params['comment']
        return StatusView.pre_status_process(self, status, params)

    def post_status_process(self, status, params):
        """
        Launch post status process functions

        :param str status: The new status that should be affected
        :param dict params: The params that were transmitted by the associated
        State's callback
        """
        self.request.dbsession.merge(self.context)
        logger.debug("post_status_process")
        self.context.historize_latest_status('validation_status')
        StatusView.post_status_process(self, status, params)


class RestSupplierOrderValidationStatusView(BaseSupplierValidationStatusView):
    def redirect(self):
        loc = self.request.route_path(
            "/suppliers_orders/{id}",
            id=self.context.id,
        )
        return dict(redirect=loc)


class RestSupplierInvoiceValidationStatusView(
    BaseSupplierValidationStatusView
):
    def redirect(self):
        loc = self.request.route_path(
            "/suppliers_invoices/{id}",
            id=self.context.id,
        )
        return dict(redirect=loc)


def add_suppliers_orders_routes(config):
    config.add_route(
        "/api/v1/suppliers_orders",
        "/api/v1/suppliers_orders",
    )

    config.add_route(
        "/api/v1/suppliers_orders/{id}",
        r"/api/v1/suppliers_orders/{id:\d+}",
        traverse="/suppliers_orders/{id}"
    )

    config.add_route(
        "/api/v1/suppliers_orders/{id}/lines",
        "/api/v1/suppliers_orders/{id}/lines",
        traverse="/suppliers_orders/{id}"
    )

    config.add_route(
        "/api/v1/suppliers_orders/{id}/lines/{lid}",
        r"/api/v1/suppliers_orders/{id:\d+}/lines/{lid:\d+}",
        traverse="/suppliers_orderlines/{lid}",
    )


def add_suppliers_invoices_routes(config):
    config.add_route(
        "/api/v1/suppliers_invoices",
        "/api/v1/suppliers_invoices",
    )

    config.add_route(
        "/api/v1/suppliers_invoices/{id}",
        r"/api/v1/suppliers_invoices/{id:\d+}",
        traverse="/suppliers_invoices/{id}"
    )

    config.add_route(
        "/api/v1/suppliers_invoices/{id}/lines",
        "/api/v1/suppliers_invoices/{id}/lines",
        traverse="/suppliers_invoices/{id}"
    )

    config.add_route(
        "/api/v1/suppliers_invoices/{id}/lines/{lid}",
        r"/api/v1/suppliers_invoices/{id:\d+}/lines/{lid:\d+}",
        traverse="/suppliers_invoicelines/{lid}",
    )


def add_suppliers_orders_views(config):
    config.add_rest_service(
        RestSupplierOrderView,
        "/api/v1/suppliers_orders/{id}",
        collection_route_name="/api/v1/suppliers_orders",
        view_rights="view.supplier_order",
        add_rights="add.supplier_order",
        edit_rights="edit.supplier_order",
        delete_rights="delete.supplier_order",
    )

    # Form configuration view
    config.add_view(
        RestSupplierOrderView,
        attr='form_config',
        route_name='/api/v1/suppliers_orders/{id}',
        renderer='json',
        request_param="form_config",
        permission='view.supplier_order',
        xhr=True,
    )
    config.add_view(
        RestInternalSupplierOrderView,
        attr='form_config',
        route_name='/api/v1/suppliers_orders/{id}',
        renderer='json',
        request_param="form_config",
        permission='view.supplier_order',
        xhr=True,
        context=InternalSupplierOrder,
    )

    # # Status view
    config.add_view(
        RestSupplierOrderValidationStatusView,
        route_name='/api/v1/suppliers_orders/{id}',
        request_param='action=validation_status',
        # More fine permission is checked in-view
        permission="view.supplier_order",
        request_method='POST',
        renderer="json",
    )

    # Line views
    config.add_rest_service(
        RestSupplierOrderLineView,
        "/api/v1/suppliers_orders/{id}/lines/{lid}",
        collection_route_name="/api/v1/suppliers_orders/{id}/lines",
        view_rights="view.supplier_order",
        add_rights="edit.supplier_order",
        edit_rights="edit.supplier_order",
        delete_rights="delete.supplier_order",
    )
    config.add_view(
        RestSupplierOrderLineView,
        attr='duplicate',
        route_name="/api/v1/suppliers_orders/{id}/lines/{lid}",
        request_param='action=duplicate',
        permission="edit.supplier_order",
        request_method='POST',
        renderer="json",
        xhr=True,
    )


def add_suppliers_invoices_views(config):
    """
    Add rest api views
    """

    config.add_rest_service(
        RestSupplierInvoiceView,
        "/api/v1/suppliers_invoices/{id}",
        collection_route_name="/api/v1/suppliers_invoices",
        view_rights="view.supplier_invoice",
        add_rights="add.supplier_invoice",
        edit_rights="edit.supplier_invoice",
        delete_rights="delete.supplier_invoice",
    )

    # Form configuration view
    config.add_view(
        RestSupplierInvoiceView,
        attr='form_config',
        route_name='/api/v1/suppliers_invoices/{id}',
        renderer='json',
        request_param="form_config",
        permission='view.supplier_invoice',
        xhr=True,
        context=SupplierInvoice,
    )
    config.add_view(
        RestInternalSupplierInvoiceView,
        attr='form_config',
        route_name='/api/v1/suppliers_invoices/{id}',
        renderer='json',
        request_param="form_config",
        permission='view.supplier_invoice',
        xhr=True,
        context=InternalSupplierInvoice,
    )

    # # Status view
    config.add_view(
        RestSupplierInvoiceValidationStatusView,
        route_name='/api/v1/suppliers_invoices/{id}',
        request_param='action=validation_status',
        # More fine permission is checked in-view
        permission="view.supplier_invoice",
        request_method='POST',
        renderer="json",
    )

    # Line views
    config.add_rest_service(
        RestSupplierInvoiceLineView,
        "/api/v1/suppliers_invoices/{id}/lines/{lid}",
        collection_route_name="/api/v1/suppliers_invoices/{id}/lines",
        view_rights="view.supplier_invoice",
        add_rights="edit.supplier_invoice",
        edit_rights="edit.supplier_invoice",
        delete_rights="delete.supplier_invoice",
    )
    config.add_view(
        RestSupplierInvoiceLineView,
        attr='duplicate',
        route_name="/api/v1/suppliers_invoices/{id}/lines/{lid}",
        request_param='action=duplicate',
        permission="edit.supplier_invoice",
        request_method='POST',
        renderer="json",
        xhr=True,
    )


def includeme(config):
    add_suppliers_orders_routes(config)
    add_suppliers_orders_views(config)
    add_suppliers_invoices_routes(config)
    add_suppliers_invoices_views(config)
