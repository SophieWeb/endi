"""
Common utilities used for task edition
"""
from endi.models.task import (
    PaymentConditions,
    WorkUnit,
)
from endi.models.tva import (
    Tva,
    Product,
)
from endi.compute.math_utils import (
    integer_to_amount,
)
from endi.forms.tasks.base import get_business_types_from_request
from endi.utils.widgets import Link


def get_business_types(request):
    return [
        dict(label=i.label, value=i.id, tva_on_margin=i.tva_on_margin)
        for i in get_business_types_from_request(request)
    ]


def get_mentions(request):
    """
    Collect Task mentions regarding the context's business type

    :param obj request: The current request object with a Task context
    :returns: List of TaskMention in their json repr
    """
    context = request.context
    doctype = context.type_
    business_type = context.business_type
    mentions = business_type.optionnal_mentions(doctype)
    return mentions


def get_tvas(request, customer=None):
    """
    Collect tva objects regarding the request context

    :param obj request: The current request object with a Task context
    :returns: List of Tva objects in their json repr
    """
    if customer is None:
        customer = request.context.customer
    # Pas de tva en interne mais un produit pour l'analytique
    if customer.is_internal():
        internal = Tva.get_internal()
        if internal is not None:
            result = [internal]
        else:
            query = Tva.query().filter(Tva.value <= 0)
            result = query.all()
    else:
        result = Tva.query().all()

    return result


def get_products(request, customer=None):
    """
    Collect products regarding the context

    :param obj request: The current request object with a Task context
    :returns: List of Product objects
    """
    if customer is None:
        customer = request.context.customer
    # Pas de tva en interne mais un (ou plusieurs) produit pour l'analytique
    if customer.is_internal():
        result = Product.get_internal()
        if not result:
            result = Product.query().filter(Tva.value <= 0).all()
    else:
        result = Product.query().all()

    return result


def get_workunits(request):
    """
    Collect available Workunits

    :param obj request: The current request object
    :returns: List of Workunits
    """
    return WorkUnit.query().all()


def get_payment_conditions(request):
    """
    Collect available PaymentConditions

    :param obj request: The current request object
    :returns: List of PaymentConditions
    """
    return PaymentConditions.query().all()


def get_default_tva(request, tva_options):
    """
    Return the default tva to be used in this specific context

    :param obj request: The pyramid request with a Task context
    :param list tva_options: The available options for the end user
    :rtype: str
    """
    if request.context.customer.is_internal():
        result = tva_options[0]
        for tva_opt in tva_options:
            if tva_opt.default:
                result = tva_opt
                break
        result = integer_to_amount(result.value, 2)
    else:
        default_tva = Tva.get_default()
        if default_tva:
            result = integer_to_amount(default_tva.value, 2)
        else:
            result = ""
    return result


def get_default_product_id(request, products):
    """
    Try to find a default product_id regarding the current context

    :param obj request: A pyramid request with a Task context
    :param list products: A list of Product instances

    :rtype: int or None
    """
    result = ""
    default_tva = Tva.get_default()
    if request.context.customer.is_internal():
        if len(products) == 1:
            result = products[0].id
    elif default_tva:
        query = Product.query().filter(Tva.id == default_tva.id)
        if query.count() == 1:
            result = query.first().id
    return result


def get_task_view_type(task):
    """
    Compute the view/acl label for the given task based on its type_
    """
    type_ = task.type_
    if type_ == 'internalinvoice':
        type_ = 'invoice'
    elif type_ == 'internalestimation':
        type_ = 'estimation'
    return type_


def get_task_url(
    request,
    task=None,
    _query={},
    suffix='',
    api=False,
    _anchor=None,
    absolute=False,
):
    """
    Return the route_name associated to the given Task

    :param request: Pyramid request
    :param task: Task instance
    :param dict _query: Query parameters
    :param bool api: Do we ask for api url
    :param str _anchor: Url anchor to add at the end e.g : #payment
    :param bool absolute: Absolute url ?
    """
    if task is None:
        task = request.context

    type_ = get_task_view_type(task)
    route = "/%ss/{id}" % type_

    if suffix:
        route += suffix

    if api:
        route = "/api/v1%s" % route

    params = dict(id=task.id, _query=_query)
    if _anchor is not None:
        params['_anchor'] = _anchor

    if absolute:
        method = request.route_url
    else:
        method = request.route_path
    return method(route, **params)


def task_pdf_link(request, task=None):
    """
    Return the route_name associated to the given Task

    :param request: Pyramid request
    :param task: Task instance
    """
    if task is None:
        task = request.context
    type_ = get_task_view_type(task)
    route = "/%ss/{id}.pdf" % type_
    return Link(
        request.route_path(route, id=task.id),
        label="",
        title="Voir le PDF de ce document",
        css='btn icon only',
        icon="file-pdf"
    )
