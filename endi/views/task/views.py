"""
Base Task views
"""
import datetime
import logging

from pyramid.httpexceptions import HTTPFound

from endi.interfaces import (
    ITaskPdfRenderingService,
)
from endi.utils.widgets import ViewLink
from endi.models.company import Company
from endi.models.third_party.customer import Customer
from endi.models.project import (
    Phase,
    Project,
)
from endi.models.task.task import TaskLine
from endi.forms.tasks.base import (
    get_duplicate_schema,
    get_new_task_schema,
    get_task_metadatas_edit_schema,
)
from endi.forms.tasks.invoice import (
    SetProductsSchema,
)
from endi.export.utils import (
    write_file_to_request,
)
from endi.export.task_pdf import ensure_task_pdf_persisted
from endi.models.status import StatusLogEntry
from endi.resources import (
    task_resources,
    task_preview_css,
    task_add_js,
)
from endi.views import (
    BaseView,
    BaseFormView,
    submit_btn,
    cancel_btn,
    TreeMixin,
)
from endi.views.sale_product.routes import CATALOG_API_ROUTE

from endi.views.status import StatusView
from endi.views.project.routes import (
    PROJECT_ITEM_PHASE_ROUTE,
    PROJECT_ITEM_ROUTE,
)
from .utils import (
    get_task_url,
    get_task_view_type,
)
from endi.models.task import (
    Estimation, Invoice
)


logger = logging.getLogger(__name__)


def get_project_redirect_btn(request, id_):
    """
        Button for "go back to project" link
    """
    return ViewLink(
        "Revenir au dossier",
        path=PROJECT_ITEM_ROUTE,
        id=id_
    )


class TaskAddView(BaseFormView):
    """
    Base task add view

    Handles stuff common to all Tasks
    """
    title = "Nouveau document"
    schema = get_new_task_schema()
    buttons = (submit_btn,)
    factory = None

    def before(self, form):
        BaseFormView.before(self, form)
        self.populate_actionmenu()
        task_add_js.need()

    def get_factory(self, appstruct):
        """
        Retrieve the factory class used to build the new object
        """
        if self.factory is None:
            raise NotImplementedError("Forgot to set the factory attribute")
        return self.factory

    def submit_success(self, appstruct):
        project_id = appstruct.pop('project_id')
        appstruct['project'] = Project.get(project_id)

        customer_id = appstruct.pop('customer_id')
        appstruct['customer'] = Customer.get(customer_id)

        appstruct['company'] = Company.get(
            self.context.get_company_id()
        )

        factory = self.get_factory(appstruct)

        new_object = factory(
            user=self.request.user,
            **appstruct
        )

        if hasattr(self, "_more_init_attributes"):
            self._more_init_attributes(new_object, appstruct)

        self.dbsession.add(new_object)
        self.dbsession.flush()

        if hasattr(self, "_after_flush"):
            self._after_flush(new_object)

        url = get_task_url(self.request, new_object)
        return HTTPFound(url)

    def populate_actionmenu(self):
        """
            Add buttons in the request actionmenu attribute
        """
        request = self.request
        if request.context.type_ == 'project':
            project_id = request.context.id
        else:
            if request.context.phase:
                project_id = request.context.phase.project_id
            else:
                project_id = request.context.project_id
        request.actionmenu.add(get_project_redirect_btn(request, project_id))


class TaskEditView(BaseView, TreeMixin):

    def title(self):
        return "Modification du document {task.name}".format(
            task=self.context
        )

    def catalog_tree_url(self):
        return self.request.route_path(
            CATALOG_API_ROUTE,
            id=self.context.company_id,
        )

    def context_url(self, _query={}):
        return get_task_url(
            self.request, _query=_query, api=True
        )

    def form_config_url(self):
        return self.context_url(_query={'form_config': '1'})

    def task_line_group_api_url(self):
        return self.context_url() + "/task_line_groups"

    def more_js_app_options(self):
        """
        Add options passed to the AppOption object
        """
        return {}

    def __call__(self):
        if not self.request.has_permission(
                'edit.%s' % get_task_view_type(self.context)
        ):
            return HTTPFound(self.request.current_route_path() + '.html')

        if hasattr(self, '_before'):
            self._before()

        self.populate_navigation()

        task_resources.need()
        result = dict(
            context=self.context,
            title=self.title,
            js_app_options={
                'context_url': self.context_url(),
                'form_config_url': self.form_config_url(),
                'catalog_tree_url': self.catalog_tree_url(),
                'task_line_group_api_url': self.task_line_group_api_url(),
            }
        )
        result['js_app_options'].update(self.more_js_app_options())
        return result


class TaskDeleteView(BaseView):
    """
    Base task deletion view
    """
    msg = "Le document {context.name} a bien été supprimé."

    def __call__(self):
        logger.info(
            "# {user.login.login} deletes {task.type_} {task.id}".format(
                user=self.request.user,
                task=self.context,
            )
        )
        project = self.context.project

        if hasattr(self, 'pre_delete'):
            self.pre_delete()

        try:
            self.request.dbsession.delete(self.context)
        except Exception:
            logger.exception("Unknown error")
            self.request.session.flash(
                "Une erreur inconnue s'est produite",
                queue="error",
            )
        else:
            if hasattr(self, 'post_delete'):
                self.post_delete()
            message = self.msg.format(context=self.context)
            self.request.session.flash(message)

        self.request.dbsession.flush()
        return self.get_redirect(project)

    def get_redirect(self, project):
        """
        Return a redirect url after task deletion
        """
        return HTTPFound(
            self.request.route_path(PROJECT_ITEM_ROUTE, id=project.id)
        )


class TaskDuplicateView(BaseFormView):
    """
    Task duplication view
    """
    schema = get_duplicate_schema()

    @property
    def title(self):
        return "Dupliquer {0} {1}".format(self.label, self.context.name)

    def before(self, form):
        BaseFormView.before(self, form)
        task_add_js.need()

    def submit_success(self, appstruct):
        logger.debug("# Duplicating a document #")
        logger.debug(appstruct)

        project_id = appstruct.pop('project_id')
        appstruct['project'] = Project.get(project_id)

        customer_id = appstruct.pop('customer_id')
        appstruct['customer'] = Customer.get(customer_id)

        task = self.context.duplicate(
            user=self.request.user,
            **appstruct
        )
        if hasattr(self, "_after_task_duplicate"):
            self._after_task_duplicate(task, appstruct)

        self.dbsession.add(task)
        self.dbsession.flush()
        logger.debug(
            "The {t.type_} {t.id} has been duplicated to {new_t.id}".format(
                t=self.context,
                new_t=task
            )
        )
        return HTTPFound(get_task_url(self.request, task))


class TaskMoveToPhaseView(BaseView):
    """
    View used to move a document to a specific directory/phase

    expects a get arg "phase" containing the destination phase_id
    """
    def __call__(self):
        phase_id = self.request.params.get('phase')
        if phase_id:
            phase = Phase.get(phase_id)
            if phase in self.context.project.phases:
                self.context.phase_id = phase_id
                self.request.dbsession.merge(self.context)

        return HTTPFound(
            self.request.route_path(
                PROJECT_ITEM_PHASE_ROUTE,
                id=self.context.project_id,
                _query={'phase': phase_id}
            )
        )


class TaskHtmlView(BaseView, TreeMixin):
    """
    Base Task html view
    """
    label = "Objet"

    @property
    def title(self):
        return "{0} : {1}".format(
            self.label,
            self.context.internal_number
        )

    def actions(self):
        """
        Return the description of the action buttons

        :returns: A list of dict (url, icon, label)
        :rtype: list
        """
        return []

    def _collect_file_indicators(self):
        """
        Collect file requirements attached to the given context
        """
        return self.context.file_requirements

    def stream_main_actions(self):
        return []

    def stream_more_actions(self):
        return []

    def __call__(self):
        self.populate_navigation()
        task_preview_css.need()
        # If the task is editable, we go the edit page
        if self.request.has_permission(
                'edit.%s' % get_task_view_type(self.context)
        ):
            return HTTPFound(get_task_url(self.request))

        return dict(
            title=self.title,
            task=self.context,
            actions=self.actions(),
            indicators=self._collect_file_indicators(),
            main_actions=self.stream_main_actions(),
            more_actions=self.stream_more_actions(),
        )


class TaskPdfView(BaseView):
    """
    Return A pdf representation of the current context
    """
    def __call__(self):
        rendering_service = self.request.find_service(
            ITaskPdfRenderingService,
            context=self.context,
        )
        filename = rendering_service.filename()
        pdf_buffer = ensure_task_pdf_persisted(self.context, self.request)
        write_file_to_request(
            self.request,
            filename,
            pdf_buffer,
            'application/pdf'
        )
        return self.request.response


class TaskPdfDevView(BaseView):
    """
    Return the html structure used in pdf generation
    """
    def __call__(self):
        from endi.resources import pdf_css
        pdf_css.need()
        return dict(task=self.context)


class TaskSetMetadatasView(BaseFormView):
    schema = get_task_metadatas_edit_schema()
    buttons = (submit_btn, cancel_btn)

    @property
    def title(self):
        return "Modification du document {task.name}".format(
            task=self.context
        )

    def before(self, form):
        task_add_js.need()
        self.request.actionmenu.add(
            ViewLink(
                "Revenir au document",
                path='/%ss/{id}.html' % get_task_view_type(self.context),
                id=self.context.id,
            ),
        )
        appstruct = {
            'name': self.context.name,
            "customer_id": self.context.customer_id,
            "project_id": self.context.project_id,
        }
        if self.context.phase_id:
            appstruct['phase_id'] = self.context.phase_id

        form.set_appstruct(appstruct)

    def redirect(self):
        url = get_task_url(self.request)
        return HTTPFound(url)

    def _get_related_elements(self):
        """
        List elements related to the current estimation
        Produce a list of visible elements that will be moved
        and a list of all elements that will be moved

        :returns: a 2-uple (visible elements, list of elements to be moved)
        :rtype: tuple
        """
        all_items = []
        visible_items = []
        business = self.context.business
        if business:
            if business.is_visible():
                visible_items.append(business)
            all_items.append(business)

            all_items.extend(business.invoices)
            visible_items.extend(business.invoices)
            for estimation in business.estimations:
                if estimation != self.context:
                    visible_items.append(estimation)
                    all_items.append(estimation)
        return visible_items, all_items

    def _handle_move_to_project(self, appstruct):
        """
        Handle the specific case where a document is moved to another project

        :param dict appstruct: The appstruct returned after form validation
        """
        visible_items, all_items = self._get_related_elements()
        if visible_items:
            logger.debug(
                "We want the user to confirm the Move to project action"
            )

        self._apply_modifications(appstruct)
        # We move all elements to the other project
        for element in all_items:
            element.project_id = appstruct['project_id']
            if hasattr(element, 'phase_id') and 'phase_id' in appstruct:
                element.phase_id = appstruct['phase_id']
            self.dbsession.merge(element)

        result = self.redirect()

        return result

    def _apply_modifications(self, appstruct):
        """
        Apply the modification described by appstruct to the current context

        :param dict appstruct: The appstruct returned after form validation
        """
        if 'customer_id' in appstruct:
            # Dans le cas où le document peut être déplacé d'un dossier à un
            # autre, on a aussi besoin du customer_id dans le formulaire pour
            # que la partie js fonctionne, mais on ne veut pas pour autant
            # qu'il soit modifiable
            # Donc si il est présent, on l'enlève
            appstruct.pop('customer_id')
        for key, value in appstruct.items():
            setattr(self.context, key, value)
        return self.request.dbsession.merge(self.context)

    def submit_success(self, appstruct):
        """
        Handle successfull modification

        :param dict appstruct: The appstruct returned after form validation
        :rtype: HTTPFound
        """
        logger.debug(
            "TaskSetMetadatasView.submit_success : %s" % appstruct
        )
        project_id = appstruct.get('project_id')

        if project_id not in (None, self.context.project_id):
            result = self._handle_move_to_project(appstruct)
        else:
            self._apply_modifications(appstruct)
            result = self.redirect()

        return result

    def cancel_success(self, appstruct):
        return self.redirect()

    cancel_failure = cancel_success


class TaskSetProductsView(BaseFormView):
    """
    Base view for setting product codes (on invoices and cancelinvoices)

    context

        invoice or cancelinvoice
    """
    schema = SetProductsSchema()

    def before(self, form):
        form.set_appstruct(
            {
                'lines': [
                    line.appstruct() for line in self.context.all_lines
                ]
            }
        )
        self.request.actionmenu.add(
            ViewLink(
                "Revenir au document",
                path="/%ss/{id}.html" % get_task_view_type(self.context),
                id=self.context.id
            )
        )

    def submit_success(self, appstruct):
        for line in appstruct['lines']:
            line_id = line.get('id')
            product_id = line.get('product_id')
            if line_id is not None and product_id is not None:
                taskline = TaskLine.get(line_id)
                if taskline.task == self.context:
                    taskline.product_id = product_id
                    self.request.dbsession.merge(taskline)
                else:
                    logger.error(
                        "Possible break in attempt: trying to set product id "
                        "on the wrong task line (not belonging to this task)"
                    )
        return HTTPFound(get_task_url(self.request))


class TaskSetDraftView(BaseView):
    """
    Set the current task status to draft
    """
    def __call__(self):
        self.request.context.status = 'draft'
        return HTTPFound(get_task_url(self.request))


class TaskStatusView(StatusView):
    """
    View handling base status for tasks (estimation/invoice/cancelinvoice)

    Status related views should implement

    - the validate function to ensure data
      integrity

    - state_manager_key (str) to where the status is stored on document model
    """
    @property
    def state_manager_key(self) -> str:
        return NotImplementedError()

    def validate(self):
        raise NotImplementedError()

    def check_allowed(self, status):
        self.request.context.check_status_allowed(status, self.request)

    def get_redirect_url(self):
        project_id = self.request.context.project.id
        loc = self.request.route_path(PROJECT_ITEM_ROUTE, id=project_id)
        return loc

    def redirect(self):
        loc = self.get_redirect_url()
        if self.request.is_xhr:
            return dict(redirect=loc)
        else:
            return HTTPFound(loc)

    def pre_status_process(self, status, params):
        if 'comment' in params:
            self.context.status_comment = params.get('comment')
            logger.debug(self.context.status_comment)

        if 'change_date' in params and params['change_date'] in ('1', 1):
            logger.debug("Forcing the document's date !!!")
            self.context.date = datetime.date.today()

        return StatusView.pre_status_process(self, status, params)

    def pre_wait_process(self, status, params):
        """
        Launched before the wait status is set

        :param str status: The new status that should be affected
        :param dict params: The params that were transmitted by the associated
        State's callback
        """
        self.validate()
        return {}

    def pre_valid_process(self, status, params):
        """
        Launched before the valid status is set

        :param str status: The new status that should be affected
        :param dict params: The params that were transmitted by the associated
        State's callback
        """
        self.validate()
        return {}

    def post_valid_process(self, status, params):
        """
        Launched after the status is set to valid

        :param str status: The new status that should be affected
        :param dict params: The params that were transmitted by the associated
        State's callback
        """

        # Check if document was auto validated
        if status == 'valid':
            # Needed only for invoices and estimations
            if isinstance(params, (Estimation, Invoice)):
                user = self.request.user
                user_companies = [company.id for company in user.companies]
                task_company = params.company_id

                if task_company in user_companies:
                    params.set_auto_validated()

    def post_status_process(self, status, params):
        """
        Launch post status process functions

        :param str status: The new status that should be affected
        :param dict params: The params that were transmitted by the associated
        State's callback
        """
        logger.debug("post_status_process")
        logger.debug(self.context.status_comment)
        # Record a task status change
        self.context.status_date = datetime.datetime.now()
        status_record = StatusLogEntry(
            node_id=self.context.id,
            status=status,
            user_id=self.request.user.id,
            comment=self.context.status_comment,
            state_manager_key=self.state_manager_key,
        )
        self.context.statuses.append(status_record)
        self.request.dbsession.merge(self.context)
        StatusView.post_status_process(self, status, params)


class TaskSyncWithPriceStudyView(BaseView):
    def __call__(self):
        self.context.sync_with_price_study()
        self.dbsession.merge(self.context)
        return self.redirect()

    def redirect(self):
        return HTTPFound(get_task_url(self.request))
