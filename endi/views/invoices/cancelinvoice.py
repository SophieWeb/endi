"""
    View for assets
"""
import logging

from pyramid.httpexceptions import HTTPFound

from endi_base.utils.date import format_date

from endi.utils.widgets import (
    ViewLink,
    Link,
    POSTButton,
)
from endi.models.task import (
    CancelInvoice,
)
from endi.resources import (
    task_preview_css,
)
from endi.forms.tasks.invoice import get_add_edit_cancelinvoice_schema
from endi.views import (
    BaseEditView,
    add_panel_page_view,
)
from endi.views.business.business import BusinessOverviewView
from endi.views.files.views import FileUploadView
from endi.views.task.utils import task_pdf_link
from endi.views.task.views import (
    TaskEditView,
    TaskHtmlView,
    TaskPdfView,
    TaskSetMetadatasView,
    TaskSetProductsView,
    TaskSetDraftView,
    TaskMoveToPhaseView,
)
from .invoice import InvoiceDeleteView


log = logging.getLogger(__name__)


class CancelInvoiceEditView(TaskEditView):
    route_name = '/cancelinvoices/{id}'

    @property
    def title(self):
        customer = self.context.customer
        customer_label = customer.label
        if customer.code is not None:
            customer_label += " ({0})".format(customer.code)

        return (
            "Modification de l’{tasktype_label} « {task.name} » "
            "avec le client {customer}".format(
                task=self.context,
                customer=customer_label,
                tasktype_label=self.context.get_type_label().lower(),
            )
        )

    def task_line_group_api_url(self):
        if self.context.invoicing_mode == self.context.PROGRESS_MODE:
            url = self.context_url() + "/progress_invoicing/groups"
        else:
            url = self.context_url() + "/task_line_groups"
        return url

    def more_js_app_options(self):
        return {
            'invoicing_mode': self.context.invoicing_mode,
        }


class CancelInvoiceDeleteView(InvoiceDeleteView):
    msg = "L'avoir {context.name} a bien été supprimé."


class CancelInvoiceHtmlView(TaskHtmlView):
    label = "Avoir"
    route_name = '/cancelinvoices/{id}.html'

    def stream_main_actions(self):
        if self.request.has_permission('set_treasury.cancelinvoice'):
            yield Link(
                self.request.route_path(
                    '/cancelinvoices/{id}/set_products',
                    id=self.context.id
                ),
                label="Codes<span class='no_mobile'>&nbsp;produits</span>",
                title="Configurer les codes produits de cet avoir",
                icon="cog",
                css="btn"
            )

    def stream_more_actions(self):
        if (
            self.request.has_permission('draft.cancelinvoice') and
            self.context.status != 'draft'
        ):
            yield POSTButton(
                self.request.route_path(
                    '/cancelinvoices/{id}/set_draft',
                    id=self.context.id
                ),
                label='',
                icon="pen",
                css="btn icon only",
                title="Repasser cet avoir en brouillon pour "
                "pouvoir le modifier"
            )
        yield Link(
            self.request.route_path(
                '/cancelinvoices/{id}/set_metadatas', id=self.context.id
            ),
            label='',
            icon="folder-move",
            css="btn icon only",
            title="Déplacer ou renommer cet avoir"
        )
        yield task_pdf_link(self.request)


class CancelInvoicePdfView(TaskPdfView):
    pass


class CancelInvoiceAdminView(BaseEditView):
    factory = CancelInvoice
    schema = get_add_edit_cancelinvoice_schema(isadmin=True)


class CancelInvoiceSetTreasuryiew(BaseEditView):
    """
    View used to set treasury related informations

    context

        An invoice

    perms

        set_treasury.invoice
    """
    factory = CancelInvoice
    schema = get_add_edit_cancelinvoice_schema(
        includes=('financial_year',),
        title="Modifier l'année fiscale de référence du numéro de facture",
    )

    def redirect(self):
        return HTTPFound(
            self.request.route_path(
                "/invoices/{id}.html",
                id=self.context.id,
                _anchor="treasury"
            )
        )

    def before(self, form):
        BaseEditView.before(self, form)
        self.request.actionmenu.add(
            ViewLink(
                label="Revenir à la facture",
                path="/invoices/{id}.html",
                id=self.context.id,
                _anchor="treasury",
            )
        )

    @property
    def title(self):
        return "Facture numéro {0} en date du {1}".format(
            self.context.official_number,
            format_date(self.context.date),
        )


class CancelInvoiceSetMetadatasView(TaskSetMetadatasView):
    """
    View used for editing invoice metadatas
    """

    @property
    def title(self):
        return "Modification de l’{tasktype_label} {task.name}".format(
            task=self.context,
            tasktype_label=self.context.get_type_label().lower(),
        )


class CancelInvoiceSetProductsView(TaskSetProductsView):
    @property
    def title(self):
        return (
            "Configuration des codes produits pour l’avoir {0.name}".format(
                self.context
            )
        )


def add_routes(config):
    """
    Add module related routes
    """
    config.add_route(
        '/cancelinvoices/{id}',
        r'/cancelinvoices/{id:\d+}',
        traverse='/tasks/{id}'
    )
    for extension in ('html', 'pdf', 'preview'):
        config.add_route(
            '/cancelinvoices/{id}.%s' % extension,
            r'/cancelinvoices/{id:\d+}.%s' % extension,
            traverse='/tasks/{id}'
        )
    for action in (
        'addfile',
        'delete',
        'admin',
        'set_treasury',
        'set_products',
        'set_metadatas',
        'set_draft',
        'move',
    ):
        config.add_route(
            '/cancelinvoices/{id}/%s' % action,
            r'/cancelinvoices/{id:\d+}/%s' % action,
            traverse='/tasks/{id}'
        )


def includeme(config):
    add_routes(config)

    # Here it's only view.cancelinvoice to allow redirection to the html view
    config.add_tree_view(
        CancelInvoiceEditView,
        parent=BusinessOverviewView,
        renderer="tasks/form.mako",
        permission='view.cancelinvoice',
        context=CancelInvoice,
    )

    config.add_view(
        CancelInvoiceAdminView,
        route_name='/cancelinvoices/{id}/admin',
        renderer="base/formpage.mako",
        request_param="token=admin",
        permission="admin",
        context=CancelInvoice,
    )

    config.add_view(
        CancelInvoiceDeleteView,
        route_name='/cancelinvoices/{id}/delete',
        permission='delete.cancelinvoice',
        require_csrf=True,
        request_method='POST',
        context=CancelInvoice,
    )

    config.add_view(
        CancelInvoicePdfView,
        route_name='/cancelinvoices/{id}.pdf',
        permission='view.cancelinvoice',
        context=CancelInvoice,
    )

    config.add_tree_view(
        CancelInvoiceHtmlView,
        parent=BusinessOverviewView,
        renderer='tasks/cancelinvoice_view_only.mako',
        permission='view.cancelinvoice',
        context=CancelInvoice,
    )

    add_panel_page_view(
        config,
        'task_pdf_content',
        js_resources=(task_preview_css,),
        route_name='/cancelinvoices/{id}.preview',
        permission="view.cancelinvoice",
        context=CancelInvoice,
    )

    config.add_view(
        FileUploadView,
        route_name='/cancelinvoices/{id}/addfile',
        renderer='base/formpage.mako',
        permission='edit.cancelinvoice',
        context=CancelInvoice,
    )

    config.add_view(
        CancelInvoiceSetTreasuryiew,
        route_name="/cancelinvoices/{id}/set_treasury",
        renderer='base/formpage.mako',
        permission="set_treasury.cancelinvoice",
        context=CancelInvoice,
    )
    config.add_view(
        CancelInvoiceSetMetadatasView,
        route_name="/cancelinvoices/{id}/set_metadatas",
        renderer='tasks/add.mako',
        permission="view.cancelinvoice",
        context=CancelInvoice,
    )
    config.add_view(
        TaskSetDraftView,
        route_name="/cancelinvoices/{id}/set_draft",
        permission="draft.cancelinvoice",
        require_csrf=True,
        request_method="POST",
        context=CancelInvoice,
    )
    config.add_view(
        CancelInvoiceSetProductsView,
        route_name="/cancelinvoices/{id}/set_products",
        permission="set_treasury.cancelinvoice",
        renderer='base/formpage.mako',
        context=CancelInvoice,
    )
    config.add_view(
        TaskMoveToPhaseView,
        route_name="/cancelinvoices/{id}/move",
        permission="view.cancelinvoice",
        require_csrf=True,
        request_method="POST",
        context=CancelInvoice,
    )
