"""
Rest views for invoices and cancelinvoices
"""
import os
import logging
import colander

from endi.models.task import (
    Invoice,
    CancelInvoice,
)
from endi.forms.tasks.invoice import (
    validate_invoice,
    validate_cancelinvoice,
    get_add_edit_invoice_schema,
    get_add_edit_cancelinvoice_schema,
)
from endi.views.task.utils import get_task_url
from endi.views.task.rest_api import (
    TaskRestView,
    TaskLineGroupRestView,
    TaskLineRestView,
    DiscountLineRestView,
    TaskFileRequirementRestView,
)
from endi.views.task.views import TaskStatusView
from endi.views.task.utils import get_payment_conditions


logger = logging.getLogger(__name__)


class InvoiceRestView(TaskRestView):
    factory = Invoice

    def get_schema(self, submitted):
        """
        Return the schema for Invoice add/edition

        :param dict submitted: The submitted datas
        :returns: A colander.Schema
        """
        excludes = ('status', 'children', 'parent', 'business_type_id')
        return get_add_edit_invoice_schema(excludes=excludes)

    def _more_form_sections(self, sections):
        """
        Add invoice specific form sections to the sections returned to the
        end user

        :param dict sections: The sections to return
        :returns: The sections
        """
        sections['composition']['discounts'] = {'mode': 'classic'}
        sections['payment_conditions'] = {'edit': True}

        if self.request.has_permission('set_treasury.invoice'):
            sections['general']['financial_year'] = True

        if self.context.invoicing_mode == self.context.PROGRESS_MODE:
            # Facturesde situation
            sections['composition']['is_cancelinvoice'] = False
            sections['composition']['lines']['mode'] = 'progress'
            sections['composition']['lines']['cost']['edit'] = False
            sections['composition']['lines']['tva']['edit'] = False
            sections['composition']['lines']['can_add'] = False
            sections['composition']['lines']['can_delete'] = False

        elif (
            self.context.business.invoicing_mode ==
            self.context.business.PROGRESS_MODE
        ):
            # Cas des factures d'acompte
            sections['composition']['lines']['quantity']['edit'] = False
            sections['composition']['lines']['cost']['edit'] = False
            sections['composition']['lines']['tva']['edit'] = False
            sections['composition']['lines']['can_add'] = False
            sections['composition']['lines']['can_delete'] = False

        return sections

    def _more_form_options(self, form_options):
        """
        Add invoice specific form options to the options returned to the end
        user

        :param dict form_options: The options returned to the end user
        :returns: The form_options with new elements
        """
        form_options.update({
            "payment_conditions": get_payment_conditions(self.request),
        })
        return form_options

    def _get_other_actions(self):
        """
        Return the description of other available actions :
            signed_status
            duplicate
            ...
        """
        result = []
        if self.request.has_permission('duplicate.invoice'):
            result.append(self._get_duplicate_button())
        result.extend(TaskRestView._get_other_actions(self))
        return result

    def _get_duplicate_button(self):
        """
        Return the description for the duplicate link
        """
        url = self.request.route_path(
            "/invoices/{id}/duplicate",
            id=self.context.id,
        )
        return {
            "widget": "anchor",
            "option": {
                "url": url,
                "title": "Créer une nouvelle facture à partir de celle-ci",
                "css": "btn icon only",
                "icon": "copy",
            }
        }

    def post_format(self, entry, edit, attributes):
        if edit:
            if 'date' in attributes and 'financial_year' not in attributes:
                if attributes['date'].year != entry.financial_year:
                    entry.financial_year = attributes['date'].year
        return entry

    def related_estimation(self):
        """
        Collect data about a related estimation(s)
        """
        result = []
        if self.context.estimation_id:
            estimation = self.context.estimation
            result.append(
                {
                    'id': estimation.id,
                    'label': "{} {}".format(
                        estimation.name, estimation.internal_number
                    ),
                    'url': get_task_url(self.request, estimation)
                }
            )
        elif self.context.business.invoicing_mode == 'progress':
            for estimation in self.context.business.estimations:
                result.append(
                    {
                        'id': estimation.id,
                        'label': "{} {}".format(
                            estimation.name, estimation.internal_number
                        ),
                        'url': get_task_url(self.request, estimation)
                    }
                )
        return result


class CancelInvoiceRestView(TaskRestView):
    factory = CancelInvoice

    def get_schema(self, submitted):
        """
        Return the schema for CancelInvoice add/edition

        :param dict submitted: The submitted datas
        :returns: A colander.Schema
        """
        excludes = ('status', 'children', 'parent',)
        return get_add_edit_cancelinvoice_schema(excludes=excludes)

    def _more_form_sections(self, sections):
        """
        Update form sections to set cancelinvoice specific rights

        :param dict sections: The sections to return
        :returns: The sections
        """
        if self.request.has_permission('set_treasury.invoice'):
            sections['general']['financial_year'] = True

        if self.context.invoicing_mode == self.context.PROGRESS_MODE:
            sections['composition']['is_cancelinvoice'] = True
            sections['composition']['lines']['mode'] = 'progress'
            sections['composition']['lines']['cost']['edit'] = False
            sections['composition']['lines']['tva']['edit'] = False
            sections['composition']['lines']['can_add'] = False
            sections['composition']['lines']['can_delete'] = False
        return sections


class InvoiceStatusRestView(TaskStatusView):
    validation_function = staticmethod(validate_invoice)
    state_manager_key = 'status'

    def get_redirect_url(self):
        if self.context.project.project_type.name == 'default':
            from endi.views.project.routes import PROJECT_ITEM_INVOICE_ROUTE
            project_id = self.context.project_id
            result = self.request.route_path(
                PROJECT_ITEM_INVOICE_ROUTE,
                id=project_id
            )
        else:
            from endi.views.business.routes import BUSINESS_ITEM_INVOICE_ROUTE
            business_id = self.context.business_id
            result = self.request.route_path(
                BUSINESS_ITEM_INVOICE_ROUTE,
                id=business_id
            )
        return result

    def validate(self):
        try:
            f = self.validation_function
            f(self.context, self.request)
        except colander.Invalid as err:
            logger.exception(
                "An error occured when validating this Invoice (id:%s)" % (
                    self.request.context.id
                )
            )
            raise err
        return {}


class CancelInvoiceStatusRestView(TaskStatusView):
    state_manager_key = 'status'

    def get_redirect_url(self):
        if self.context.project.project_type.name == 'default':
            from endi.views.project.routes import PROJECT_ITEM_INVOICE_ROUTE
            project_id = self.context.project_id
            result = self.request.route_path(
                PROJECT_ITEM_INVOICE_ROUTE,
                id=project_id
            )
        else:
            from endi.views.business.routes import BUSINESS_ITEM_INVOICE_ROUTE
            business_id = self.context.business_id
            result = self.request.route_path(
                BUSINESS_ITEM_INVOICE_ROUTE,
                id=business_id
            )
        return result

    def validate(self):
        try:
            validate_cancelinvoice(self.context, self.request)
        except colander.Invalid as err:
            logger.exception(
                "An error occured when validating this CancelInvoice "
                "(id:%s)" % (
                    self.request.context.id
                )
            )
            raise err
        return {}

    def post_valid_process(self, status, params):
        TaskStatusView.post_valid_process(self, status, params)
        self.context.invoice.check_resulted()
        self.context.invoice.historize_paid_status(self.request.user)


def add_invoice_routes(config):
    """
    Add invoice rest related routes to the current configuration

    :param obj config: Pyramid config object
    """
    COLLECTION_ROUTE = "/api/v1/invoices"
    config.add_route(COLLECTION_ROUTE, COLLECTION_ROUTE)
    ITEM_ROUTE = os.path.join(COLLECTION_ROUTE, "{id}")
    config.add_route(ITEM_ROUTE, ITEM_ROUTE, traverse='/tasks/{id}')
    for collection in (
        'task_line_groups', 'discount_lines', 'file_requirements'
    ):
        route = os.path.join(ITEM_ROUTE, collection)
        config.add_route(route, route, traverse='/tasks/{id}')

    FILE_REQ_ITEM_ROUTE = os.path.join(
        COLLECTION_ROUTE, "{eid}", "file_requirements", "{id}"
    )
    config.add_route(
        FILE_REQ_ITEM_ROUTE,
        FILE_REQ_ITEM_ROUTE,
        traverse="/indicators/{id}",
    )

    config.add_route(
        "/api/v1/invoices/{eid}/task_line_groups/{id}",
        r"/api/v1/invoices/{eid}/task_line_groups/{id:\d+}",
        traverse='/task_line_groups/{id}',
    )
    config.add_route(
        "/api/v1/invoices/{eid}/task_line_groups/{id}/task_lines",
        r"/api/v1/invoices/{eid}/task_line_groups/{id:\d+}/task_lines",
        traverse='/task_line_groups/{id}',
    )
    config.add_route(
        "/api/v1/invoices/{eid}/task_line_groups/{tid}/task_lines/{id}",
        r"/api/v1/invoices/{eid}/task_line_groups/{tid}/task_lines/{id:\d+}",
        traverse='/task_lines/{id}',
    )
    config.add_route(
        "/api/v1/invoices/{eid}/discount_lines/{id}",
        r"/api/v1/invoices/{eid}/discount_lines/{id:\d+}",
        traverse='/discount_lines/{id}',
    )


def add_cancelinvoice_routes(config):
    """
    Add routes specific to cancelinvoices edition

    :param obj config: Pyramid config object
    """
    COLLECTION_ROUTE = "/api/v1/cancelinvoices"
    config.add_route(COLLECTION_ROUTE, COLLECTION_ROUTE)
    ITEM_ROUTE = os.path.join(COLLECTION_ROUTE, "{id}")
    config.add_route(ITEM_ROUTE, ITEM_ROUTE, traverse='/tasks/{id}')
    for collection in (
        'task_line_groups', 'file_requirements'
    ):
        route = os.path.join(ITEM_ROUTE, collection)
        config.add_route(route, route, traverse='/tasks/{id}')

    FILE_REQ_ITEM_ROUTE = os.path.join(
        COLLECTION_ROUTE, "{eid}", "file_requirements", "{id}"
    )
    config.add_route(
        FILE_REQ_ITEM_ROUTE,
        FILE_REQ_ITEM_ROUTE,
        traverse="/indicators/{id}",
    )

    config.add_route(
        "/api/v1/cancelinvoices/{eid}/task_line_groups/{id}",
        r"/api/v1/cancelinvoices/{eid}/task_line_groups/{id:\d+}",
        traverse='/task_line_groups/{id}',
    )
    config.add_route(
        "/api/v1/cancelinvoices/{eid}/task_line_groups/{id}/task_lines",
        r"/api/v1/cancelinvoices/{eid}/task_line_groups/{id:\d+}/task_lines",
        traverse='/task_line_groups/{id}',
    )
    config.add_route(
        "/api/v1/cancelinvoices/{eid}/task_line_groups/{tid}/task_lines/{id}",
        r"/api/v1/cancelinvoices/{eid}/task_line_groups/"
        r"{tid}/task_lines/{id:\d+}",
        traverse='/task_lines/{id}',
    )


def add_invoice_views(config):
    """
    Add Invoice related views to the current configuration
    """
    config.add_rest_service(
        InvoiceRestView,
        '/api/v1/invoices/{id}',
        collection_route_name='/api/v1/invoices',
        edit_rights='edit.invoice',
        view_rights='view.invoice',
        delete_rights='delete.invoice',
        context=Invoice
    )

    # Form configuration view
    config.add_view(
        InvoiceRestView,
        attr='form_config',
        route_name='/api/v1/invoices/{id}',
        renderer='json',
        request_param="form_config",
        permission='edit.invoice',
        xhr=True,
        context=Invoice,
    )

    # Status View
    config.add_view(
        InvoiceStatusRestView,
        route_name="/api/v1/invoices/{id}",
        request_param='action=status',
        permission="edit.invoice",
        request_method='POST',
        renderer="json",
        xhr=True,
        context=Invoice,
    )
    # Related estimation informations
    config.add_view(
        InvoiceRestView,
        attr='related_estimation',
        route_name='/api/v1/invoices/{id}',
        renderer='json',
        request_param="related_estimation",
        permission='edit.invoice',
        xhr=True,
        context=Invoice,
    )

    # Task linegroup views
    config.add_rest_service(
        TaskLineGroupRestView,
        "/api/v1/invoices/{eid}/task_line_groups/{id}",
        collection_route_name="/api/v1/invoices/{id}/task_line_groups",
        view_rights="view.invoice",
        add_rights="edit.invoice",
        edit_rights='edit.invoice',
        delete_rights='edit.invoice',
        collection_context=Invoice
    )
    config.add_view(
        TaskLineGroupRestView,
        route_name="/api/v1/invoices/{id}/task_line_groups",
        attr='post_load_groups_from_catalog_view',
        request_param="action=load_from_catalog",
        request_method='POST',
        renderer='json',
        permission='edit.invoice',
        xhr=True,
        context=Invoice
    )
    # Task line views
    config.add_rest_service(
        TaskLineRestView,
        route_name="/api/v1/invoices/{eid}/"
        "task_line_groups/{tid}/task_lines/{id}",
        collection_route_name="/api/v1/invoices/{eid}/"
        "task_line_groups/{id}/task_lines",
        view_rights="view.invoice",
        add_rights="edit.invoice",
        edit_rights='edit.invoice',
        delete_rights='edit.invoice',
    )
    config.add_view(
        TaskLineRestView,
        route_name="/api/v1/invoices/{eid}/task_line_groups/{id}/task_lines",
        attr='post_load_from_catalog_view',
        request_param="action=load_from_catalog",
        request_method='POST',
        renderer='json',
        permission='edit.invoice',
        xhr=True,
    )
    # Discount line views
    config.add_rest_service(
        DiscountLineRestView,
        "/api/v1/invoices/{eid}/discount_lines/{id}",
        collection_route_name="/api/v1/invoices/{id}/discount_lines",
        view_rights="view.invoice",
        add_rights="edit.invoice",
        edit_rights='edit.invoice',
        delete_rights='edit.invoice',
        collection_context=Invoice
    )
    config.add_view(
        DiscountLineRestView,
        route_name="/api/v1/invoices/{id}/discount_lines",
        attr='post_percent_discount_view',
        request_param="action=insert_percent",
        request_method='POST',
        renderer='json',
        permission='edit.invoice',
        context=Invoice,
        xhr=True,
    )
    # File requirements views
    config.add_rest_service(
        TaskFileRequirementRestView,
        route_name="/api/v1/invoices/{eid}/file_requirements/{id}",
        collection_route_name="/api/v1/invoices/{id}/file_requirements",
        collection_view_rights="view.invoice",
        view_rights="view.indicator",
        collection_context=Invoice,
    )
    config.add_view(
        TaskFileRequirementRestView,
        route_name="/api/v1/invoices/{eid}/file_requirements/{id}",
        attr="validation_status",
        permission="valid.indicator",
        request_method="POST",
        request_param="action=validation_status",
        renderer='json',
        xhr=True,
    )


def add_cancelinvoice_views(config):
    """
    Add cancelinvoice related views to the current configuration

    :param obj config: The current Pyramid configuration
    """
    config.add_rest_service(
        CancelInvoiceRestView,
        route_name='/api/v1/cancelinvoices/{id}',
        collection_route_name='/api/v1/cancelinvoices',
        edit_rights='edit.cancelinvoice',
        view_rights='view.cancelinvoice',
        delete_rights='delete.cancelinvoice',
    )

    # Form configuration view
    config.add_view(
        CancelInvoiceRestView,
        attr='form_config',
        route_name='/api/v1/cancelinvoices/{id}',
        renderer='json',
        request_param="form_config",
        permission='edit.cancelinvoice',
        xhr=True,
    )

    # Status View
    config.add_view(
        CancelInvoiceStatusRestView,
        route_name="/api/v1/cancelinvoices/{id}",
        request_param='action=status',
        permission="edit.cancelinvoice",
        request_method='POST',
        renderer="json",
        xhr=True,
    )

    # Task linegroup views
    config.add_rest_service(
        TaskLineGroupRestView,
        route_name="/api/v1/cancelinvoices/{eid}/task_line_groups/{id}",
        collection_route_name="/api/v1/cancelinvoices/{id}/task_line_groups",
        view_rights="view.cancelinvoice",
        add_rights="edit.cancelinvoice",
        edit_rights='edit.cancelinvoice',
        delete_rights='edit.cancelinvoice',
    )
    config.add_view(
        TaskLineGroupRestView,
        route_name="/api/v1/cancelinvoices/{id}/task_line_groups",
        attr='post_load_groups_from_catalog_view',
        request_param="action=load_from_catalog",
        request_method='POST',
        renderer='json',
        permission='edit.cancelinvoice',
        xhr=True,
    )
    # Task line views
    config.add_rest_service(
        TaskLineRestView,
        route_name="/api/v1/cancelinvoices/{eid}/"
        "task_line_groups/{tid}/task_lines/{id}",
        collection_route_name="/api/v1/cancelinvoices/{eid}/"
        "task_line_groups/{id}/task_lines",
        view_rights="view.cancelinvoice",
        add_rights="edit.cancelinvoice",
        edit_rights='edit.cancelinvoice',
        delete_rights='edit.cancelinvoice',
    )
    config.add_view(
        TaskLineRestView,
        route_name="/api/v1/cancelinvoices/{eid}/"
        "task_line_groups/{id}/task_lines",
        attr='post_load_from_catalog_view',
        request_param="action=load_from_catalog",
        request_method='POST',
        renderer='json',
        permission='edit.cancelinvoice',
        xhr=True,
    )
    # File requirements views
    config.add_rest_service(
        TaskFileRequirementRestView,
        route_name="/api/v1/cancelinvoices/{eid}/file_requirements/{id}",
        collection_route_name="/api/v1/cancelinvoices/{id}/file_requirements",
        collection_view_rights="view.cancelinvoice",
        view_rights="view.indicator",
    )
    config.add_view(
        TaskFileRequirementRestView,
        route_name="/api/v1/cancelinvoices/{eid}/file_requirements/{id}",
        attr="validation_status",
        permission="valid.indicator",
        request_method="POST",
        request_param="action=validation_status",
        renderer='json',
        xhr=True,
    )


def includeme(config):
    add_invoice_routes(config)
    add_cancelinvoice_routes(config)
    add_invoice_views(config)
    add_cancelinvoice_views(config)
