from endi.utils.widgets import Link
from endi.utils.strings import format_account

from endi.models.project.business import Business
from endi.models.project.project import Project
from endi.forms.training.training import get_training_list_schema

from .lists import GlobalTrainingListView
from .routes import (
    TRAINING_DASHBOARD_URL,
    USER_TRAINER_EDIT_URL,
)


class TrainingDashboardView(GlobalTrainingListView):
    """
    Dashboard view allowing an employee to have an overview of its training
    activity

    Context : Company instance
    """
    is_admin = False
    schema = get_training_list_schema(is_global=False)
    title = "Mon activité de formation"

    def filter_company_id(self, query, appstruct):
        query = query.join(Business.project)
        query = query.filter(Project.company_id == self.context.id)
        return query

    def _trainer_datas_links(self):
        result = []
        for user in self.context.employees:
            if user.trainerdatas:
                if not self.request.has_permission('edit.trainerdatas', user):
                    # Je ne peux pas éditer les infos formateurs de mes
                    # collègues
                    continue

                if user.id == self.request.user.id:
                    label = "Voir ma fiche formateur"
                else:
                    label = "Voir la fiche formateur de {}".format(
                        format_account(user)
                    )
                result.append(
                    Link(
                        self.request.route_path(
                            USER_TRAINER_EDIT_URL,
                            id=user.id
                        ),
                        label,
                        icon="search",
                        popup=True,
                        css='btn',
                    )
                )
        return result

    def more_template_vars(self, result):
        result = GlobalTrainingListView.more_template_vars(self, result)
        result["trainer_datas_links"] = self._trainer_datas_links()
        return result


def includeme(config):
    config.add_view(
        TrainingDashboardView,
        route_name=TRAINING_DASHBOARD_URL,
        renderer="endi:/templates/training/dashboard.mako",
        permission="list.training",
    )

    def deferred_permission(menu, kw):
        return kw['request'].has_permission('list.training', kw['company'])

    config.add_company_menu(
        parent='worktools',
        order=0,
        label="Formation",
        route_name=TRAINING_DASHBOARD_URL,
        route_id_key='company_id',
        permission=deferred_permission
    )
