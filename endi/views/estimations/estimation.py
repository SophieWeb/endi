"""
    Estimation views


Estimation datas edition :
    date
    address
    customer
    object
    note
    mentions
    ....

Estimation line edition :
    description
    quantity
    cost
    unity
    tva
    ...

Estimation line group edition :
    title
    description

Estimation discount edition

Estimation payment edition

"""
import logging

from pyramid.httpexceptions import HTTPFound
from endi.forms.tasks.estimation import InvoiceAttachSchema
from endi.models.task import (
    Estimation,
    PaymentLine,
    Invoice,
)
from endi.utils.widgets import (
    ViewLink,
    Link,
    POSTButton,
)
from endi.resources import (
    estimation_signed_status_js,
    task_preview_css,
)
from endi.forms.tasks.estimation import get_add_edit_estimation_schema
from endi.views import (
    BaseEditView,
    BaseFormView,
    submit_btn,
    cancel_btn,
    add_panel_page_view,
)
from endi.views.files.views import FileUploadView
from endi.views.project.routes import PROJECT_ITEM_ESTIMATION_ROUTE
from endi.views.business.business import BusinessOverviewView
from endi.views.task.utils import (
    get_task_url,
    task_pdf_link,
)
from endi.views.task.views import (
    TaskAddView,
    TaskEditView,
    TaskDeleteView,
    TaskHtmlView,
    TaskPdfView,
    TaskDuplicateView,
    TaskSetMetadatasView,
    TaskSetDraftView,
    TaskMoveToPhaseView,
    TaskSyncWithPriceStudyView,
)

log = logger = logging.getLogger(__name__)


class EstimationAddView(TaskAddView):
    """
    Estimation add view
    context is a project or company
    """
    title = "Nouveau devis"

    def get_factory(self, appstruct):
        customer = appstruct['customer']
        return Estimation.get_customer_task_factory(customer)

    def _more_init_attributes(self, estimation, appstruct):
        """
        Add Estimation's specific attribute while adding this task
        """
        estimation.payment_lines = [PaymentLine(description='Solde', amount=0)]
        estimation.initialize_business_datas()
        estimation.set_default_validity_duration()
        return estimation

    def _after_flush(self, estimation):
        """
        Launch after the new estimation has been flushed
        """
        logger.debug(
            "  + Estimation successfully added : {0}".format(estimation.id)
        )

    def populate_actionmenu(self):
        if self.request.context.__name__ == 'company':
            self.request.actionmenu.add(
                ViewLink(
                    "Revenir à la liste des devis",
                    path="company_estimations",
                    id=self.request.context.id,
                )
            )
        else:
            super().populate_actionmenu()


class EstimationEditView(TaskEditView):
    route_name = '/estimations/{id}'

    @property
    def title(self):
        customer = self.context.customer
        customer_label = customer.label
        if customer.code is not None:
            customer_label += " ({0})".format(customer.code)
        return (
            "Modification du {tasktype_label} « {task.name} » avec le client "
            "{customer}".format(
                task=self.context,
                customer=customer_label,
                tasktype_label=self.context.get_type_label().lower(),
            )
        )

    def _before(self):
        """
        Ensure some stuff on the current context
        """
        if not self.context.payment_lines:
            self.context.payment_lines = [
                PaymentLine(description='Solde', amount=self.context.ttc)
            ]
            self.request.dbsession.merge(self.context)
            self.request.dbsession.flush()

    def discount_api_url(self):
        return self.context_url() + "/discount_lines"

    def more_js_app_options(self):
        return {
            'discount_api_url': self.discount_api_url(),
        }


class EstimationDeleteView(TaskDeleteView):
    msg = "Le devis {context.name} a bien été supprimé."

    def post_delete(self):
        if self.context.business and self.context.business.is_void():
            self.request.dbsession.delete(self.context.business)


class EstimationAdminView(BaseEditView):
    factory = Estimation
    schema = get_add_edit_estimation_schema(isadmin=True)


class EstimationHtmlView(TaskHtmlView):
    label = "Devis"
    route_name = "/estimations/{id}.html"

    def actions(self):
        estimation_signed_status_js.need()
        actions = []
        for action in self.context.signed_state_manager.get_allowed_actions(
            self.request
        ):
            actions.append(action)
        return actions

    def stream_main_actions(self):
        has_invoices = len(self.context.invoices) > 0

        if self.request.has_permission('geninv.estimation'):
            params = {
                'url': get_task_url(self.request, suffix='/geninv'),
                'label': "Facturer",
                "icon": "file-invoice-euro",
                "title": "Transformer ce devis en facture",
                "css": "btn icon btn-primary"
            }
            if has_invoices or self.context.geninv:
                params['label'] = "Re-facturer"
                params['title'] = "Transformer à nouveau ce devis en facture"
                params['icon'] = 'copy'

            yield POSTButton(**params)
        elif self.request.has_permission('genbusiness.estimation'):
            if self.context.business_id:
                yield Link(
                    self.request.route_path(
                        '/businesses/{id}', id=self.context.business_id
                    ),
                    label="Voir l'affaire",
                    title="Voir l’affaire : {}".format(
                        self.context.business.name
                    ),
                    icon="folder"
                )
            else:
                yield POSTButton(
                    get_task_url(self.request, suffix="/genbusiness"),
                    "Générer une affaire",
                    icon="folder",
                    css="btn icon btn-primary",
                    title="Générer une affaire ({}) au sein de "
                    "laquelle facturer".format(
                        self.context.business_type.label
                    )
                )

        if not has_invoices and not self.context.internal:
            yield Link(
                get_task_url(self.request, suffix="/attach_invoices"),
                'Rattacher<span class="no_mobile">&nbsp;à des factures</span>',
                title="Rattacher ce devis à des factures",
                icon="link",
            )

        if self.context.price_study_id is not None:
            yield Link(
                self.request.route_path(
                    '/price_studies/{id}', id=self.context.price_study_id
                ),
                "Voir l’étude de prix",
                title="Voir l’étude de prix à l’origine de ce devis",
                icon="calculator"
            )
        if self.request.has_permission('draft.estimation'):
            yield POSTButton(
                get_task_url(self.request, suffix="/set_draft"),
                label='',
                icon="pen",
                css="btn icon only",
                title="Repasser ce devis en brouillon pour pouvoir le modifier"
            )

        if self.request.has_permission('gen_supplier_order.estimation'):
            yield POSTButton(
                get_task_url(self.request, suffix='/gen_supplier_order'),
                "Commande fournisseur",
                icon="plus",
                title="Générer la commande fournisseur dans l'espace de "
                "l'enseigne {}".format(self.context.customer.label)
            )

    def stream_more_actions(self):
        if self.request.has_permission('duplicate.estimation'):
            yield Link(
                get_task_url(self.request, suffix="/duplicate"),
                label="",
                title="Dupliquer ce devis",
                icon="copy"
            )
        yield Link(
            get_task_url(self.request, suffix="/set_metadatas"),
            "",
            title="Déplacer ou renommer ce devis",
            icon="folder-move",
        )
        yield task_pdf_link(self.request)


class EstimationPdfView(TaskPdfView):
    pass


class EstimationDuplicateView(TaskDuplicateView):
    label = "le devis"

    def _after_task_duplicate(self, task, appstruct):
        task.initialize_business_datas()
        return task


class EstimationSetMetadatasView(TaskSetMetadatasView):
    @property
    def title(self):
        return "Modification du {tasktype_label} {task.name}".format(
            task=self.context,
            tasktype_label=self.context.get_type_label().lower(),
        )


class EstimationAttachInvoiceView(BaseFormView):
    schema = InvoiceAttachSchema()
    buttons = (submit_btn, cancel_btn,)

    def before(self, form):
        self.request.actionmenu.add(
            ViewLink(
                label="Revenir au devis",
                path="/estimations/{id}.html",
                id=self.context.id,
            )
        )
        form.set_appstruct(
            {
                'invoice_ids': [
                    str(invoice.id) for invoice in self.context.invoices
                ]
            }
        )

    def redirect(self):
        return HTTPFound(
            self.request.route_path(
                '/estimations/{id}.html',
                id=self.context.id,
            )
        )

    def submit_success(self, appstruct):
        invoice_ids = appstruct.get('invoice_ids')
        for invoice_id in invoice_ids:
            invoice = Invoice.get(invoice_id)
            invoice.estimation_id = self.context.id
            self.request.dbsession.merge(invoice)

        if invoice_ids:
            self.context.geninv = True
            self.request.dbsession.merge(self.context)
        return self.redirect()

    def cancel_success(self, appstruct):
        return self.redirect()

    cancel_failure = cancel_success


def estimation_geninv_view(context, request):
    """
    Invoice generation view : used in shorthanded workflow

    :param obj context: The current context (estimation)
    """
    business = context.gen_business()
    invoices = business.gen_invoices(request.user)

    context.geninv = True
    request.dbsession.merge(context)

    if len(invoices) > 1:
        msg = "{0} factures ont été générées".format(len(invoices))
    else:
        msg = "Une facture a été générée"
    request.session.flash(msg)
    request.dbsession.flush()
    return HTTPFound(
        request.route_path('/invoices/{id}', id=invoices[0].id)
    )


def estimation_genbusiness_view(context, request):
    """
    Business generation view : used in long handed workflows

    :param obj context: The current estimation
    """
    logger.info("Generating a business for estimation {}".format(context.id))
    business = context.gen_business()
    return HTTPFound(request.route_path("/businesses/{id}", id=business.id))


def add_routes(config):
    """
    Add module's specific routes
    """
    config.add_route(
        '/estimations/{id}',
        r'/estimations/{id:\d+}',
        traverse='/tasks/{id}'
    )
    for extension in ('html', 'pdf', 'preview'):
        config.add_route(
            '/estimations/{id}.%s' % extension,
            r'/estimations/{id:\d+}.%s' % extension,
            traverse='/tasks/{id}'
        )
    for action in (
        'addfile',
        'delete',
        'duplicate',
        'admin',
        'geninv',
        'genbusiness',
        'set_metadatas',
        'attach_invoices',
        'set_draft',
        'move',
        "sync_price_study",
    ):
        config.add_route(
            '/estimations/{id}/%s' % action,
            r'/estimations/{id:\d+}/%s' % action,
            traverse='/tasks/{id}'
        )


def includeme(config):
    add_routes(config)

    config.add_view(
        EstimationAddView,
        route_name=PROJECT_ITEM_ESTIMATION_ROUTE,
        renderer='tasks/add.mako',
        permission='add.estimation',
        request_param="action=add",
        layout="default"
    )
    config.add_view(
        EstimationAddView,
        route_name='company_estimations',
        renderer='tasks/add.mako',
        permission='add.estimation',
        request_param="action=add",
        layout="default"
    )
    config.add_tree_view(
        EstimationEditView,
        parent=BusinessOverviewView,
        renderer='tasks/form.mako',
        permission='view.estimation',
        layout='opa',
    )

    config.add_view(
        EstimationDeleteView,
        route_name='/estimations/{id}/delete',
        permission='delete.estimation',
        request_method='POST',
        require_csrf=True,
    )

    config.add_view(
        EstimationAdminView,
        route_name='/estimations/{id}/admin',
        renderer="base/formpage.mako",
        permission="admin",
    )

    config.add_view(
        EstimationDuplicateView,
        route_name="/estimations/{id}/duplicate",
        permission="duplicate.estimation",
        renderer='tasks/add.mako',
    )
    config.add_tree_view(
        EstimationHtmlView,
        parent=BusinessOverviewView,
        renderer='tasks/estimation_view_only.mako',
        permission='view.estimation',
    )
    add_panel_page_view(
        config,
        'task_pdf_content',
        js_resources=(task_preview_css,),
        route_name="/estimations/{id}.preview",
        permission='view.estimation',
    )

    config.add_view(
        EstimationPdfView,
        route_name='/estimations/{id}.pdf',
        permission='view.estimation',
    )

    config.add_view(
        FileUploadView,
        route_name="/estimations/{id}/addfile",
        renderer='base/formpage.mako',
        permission='add.file',
    )

    config.add_view(
        estimation_geninv_view,
        route_name="/estimations/{id}/geninv",
        permission='geninv.estimation',
        request_method='POST',
        require_csrf=True,
    )

    config.add_view(
        estimation_genbusiness_view,
        route_name="/estimations/{id}/genbusiness",
        permission='genbusiness.estimation',
        require_csrf=True,
        request_method="POST",
    )

    config.add_view(
        EstimationSetMetadatasView,
        route_name="/estimations/{id}/set_metadatas",
        permission='view.estimation',
        renderer='tasks/add.mako',
    )
    config.add_view(
        TaskMoveToPhaseView,
        route_name="/estimations/{id}/move",
        permission='view.estimation',
        require_csrf=True,
        request_method="POST",
    )
    config.add_view(
        TaskSetDraftView,
        route_name="/estimations/{id}/set_draft",
        permission="draft.estimation",
        require_csrf=True,
        request_method="POST",
    )

    config.add_view(
        EstimationAttachInvoiceView,
        route_name="/estimations/{id}/attach_invoices",
        permission='view.estimation',
        renderer="/base/formpage.mako",
    )
    config.add_view(
        TaskSyncWithPriceStudyView,
        route_name="/estimations/{id}/sync_price_study",
        permission="edit.estimation",
    )
