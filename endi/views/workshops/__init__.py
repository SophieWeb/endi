def includeme(config):
    config.include('.workshop')
    config.include('.lists')
    config.include('.export')
