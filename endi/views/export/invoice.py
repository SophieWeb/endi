"""
Invoice related exports views
"""
import logging
from collections import OrderedDict

from sqlalchemy import or_
from sqlalchemy.orm.exc import MultipleResultsFound

from endi.interfaces import (
    ITreasuryProducer,
    ITreasuryInvoiceWriter,
)
from endi.export.utils import write_file_to_request
from endi.utils.widgets import ViewLink
from endi.utils.files import get_timestamped_filename
from endi.views.export.utils import query_invoices_for_export

from endi.models.task import (
    Invoice,
    CancelInvoice,
    InternalInvoice,
)
from endi.export.task_pdf import ensure_task_pdf_persisted

from endi.views.admin.sale.accounting import (
    ACCOUNTING_INDEX_URL,
)
from endi.views.admin.sale.accounting.invoice import (
    CONFIG_URL as INVOICE_CONFIG_URL
)
from endi.views.admin.sale.accounting.internalinvoice import (
    CONFIG_URL as INTERNALINVOICE_CONFIG_URL
)
from endi.views.task.utils import (
    get_task_view_type,
    get_task_url,
)
from endi.views.export import (
    BaseExportView,
    SAGE_COMPATIBLE_ENCODING,
)
from endi.views.export.utils import (
    get_invoice_period_form,
    get_invoice_all_form,
    get_invoice_number_form,
)
from endi.models.task import (
    Task
)
from endi.models.status import (
    StatusLogEntry
)

logger = logging.getLogger(__name__)


MISSING_PRODUCT_ERROR_MSG = """Le document <a target="_blank" href="{2}">{0}</a> n'est pas exportable :
 des comptes produits sont manquants.
<a onclick="window.openPopup('{1}');" href='#'>
        Voir le document
</a>"""

COMPANY_ERROR_MSG = """Le document <a target="_blank" href="{3}">{0}</a> n'est pas exportable :
Le code analytique de l'enseigne {1} n'a pas été configuré.
<a onclick="window.openPopup('{2}');" href='#'>
    Voir l'enseigne
</a>"""

CUSTOMER_ERROR_MSG = """Le document <a target="_blank" href="{3}">{0}</a> n'est pas exportable :
Le compte général du client {1} n'a pas été configuré.
<a onclick="window.openPopup('{2}');" href='#'>
    Voir le client
</a>"""


MISSING_RRR_CONFIG_ERROR_MSG = """Le document <a target="_blank" href="{2}">{0}</a> n'est pas exportable :
Il contient des remises et les comptes RRR ne sont pas configurés.
<a onclick="window.openPopup('{1}');" href='#'>
Configurer les comptes RRR
</a>"""


class SageSingleInvoiceExportPage(BaseExportView):
    """
    Single invoice export page
    """
    admin_route_name = ACCOUNTING_INDEX_URL

    @property
    def title(self):
        return "Export de la facture {0} au format CSV".format(
            self.context.official_number,
        )

    def populate_action_menu(self):
        """
        Add a back button to the action menu
        """
        self.request.actionmenu.add(
            ViewLink(
                label="Retour au document",
                path='/%ss/{id}.html' % get_task_view_type(self.context),
                id=self.context.id,
                _anchor='treasury'
            )
        )

    def before(self):
        self.populate_action_menu()

    def validate_form(self, forms):
        """
        Return a a void form name and an appstruct so processing goes on
        """
        return "", {}

    def query(self, appstruct, formname):
        force = self.request.params.get('force', False)
        if not force and self.context.exported:
            return []
        else:
            return [self.context]

    def _check_invoice_line(self, line):
        """
        Check the invoice line is ok for export

        :param obj line: A TaskLine instance
        """
        return line.product is not None

    def _check_company(self, company):
        """
            Check the invoice's company is configured for exports
        """
        if not company.code_compta:
            return False
        return True

    def _check_customer(self, customer, invoice):
        """
        Check the invoice's customer is configured for exports
        """
        prefix = ''
        if invoice.internal:
            prefix = 'internal'

        if not customer.get_general_account(prefix):
            return False

        if self.request.config.get('sage_rgcustomer'):
            if not customer.get_third_party_account(prefix):
                return False
        return True

    def check_num_invoices(self, invoices):
        """
        Return the number of invoices to export
        """
        return len(invoices)

    def _check_discount_config(self, internal=False):
        """
        Check that the rrr accounts are configured
        """
        if internal:
            # Pour les factures internes, on reste sur le compte cg de la tva
            check = bool(self.request.config.get("internalcompte_rrr"))
        else:
            check = self.request.config.get("compte_rrr") and \
                self.request.config.get("compte_cg_tva_rrr")
        return check

    def check(self, invoices):
        """
            Check that the given invoices are 'exportable'
        """
        logger.debug("    + Checking number of invoices to export")
        logger.debug(invoices)
        count = self.check_num_invoices(invoices)
        if count == 0:
            title = "Il n'y a aucune facture à exporter"
            res = {
                'title': title,
                'errors': [],
            }
            return False, res
        logger.debug("done")
        title = "Vous vous apprêtez à exporter {0} factures".format(
                count)
        res = {'title': title, 'errors': []}

        for invoice in invoices:
            official_number = invoice.official_number
            logger.debug("    + Checking invoice {}".format(official_number))
            invoice_url = get_task_url(self.request, invoice)
            logger.debug("      + Checking lines")
            for line in invoice.all_lines:
                if not self._check_invoice_line(line):
                    set_product_url = get_task_url(
                        self.request, invoice, suffix='/set_products'
                    )
                    message = MISSING_PRODUCT_ERROR_MSG.format(
                        official_number, set_product_url, invoice_url
                    )
                    res['errors'].append(message)
                    break

            if invoice.discounts:
                logger.debug("      + Checking discounts")
                if not self._check_discount_config(
                    invoice.internal
                ):
                    if invoice.internal:
                        admin_url = self.request.route_path(
                            INTERNALINVOICE_CONFIG_URL,
                        )
                    else:
                        admin_url = self.request.route_path(
                            INVOICE_CONFIG_URL,
                        )

                    message = MISSING_RRR_CONFIG_ERROR_MSG.format(
                        official_number,
                        admin_url,
                        invoice_url,
                    )
                    res['errors'].append(message)

            logger.debug("      + Checking company")
            if not self._check_company(invoice.company):
                company_url = self.request.route_path(
                    'company',
                    id=invoice.company.id,
                    _query={'action': 'edit'}
                )
                message = COMPANY_ERROR_MSG.format(
                    official_number,
                    invoice.company.name,
                    company_url,
                    invoice_url,
                )
                res['errors'].append(message)
                continue

            logger.debug("      + Checking customer")
            if not self._check_customer(invoice.customer, invoice):
                customer_url = self.request.route_path(
                    'customer',
                    id=invoice.customer.id,
                    _query={'action': 'edit'})

                message = CUSTOMER_ERROR_MSG.format(
                    official_number,
                    invoice.customer.label,
                    customer_url,
                    invoice_url,
                )
                res['errors'].append(message)
                continue
            logger.debug("  + Checks finished")

        logger.debug("  + Checks finished")
        return len(res['errors']) == 0, res

    def record_exported(self, invoices, form_name, appstruct):
        for invoice in invoices:
            ensure_task_pdf_persisted(invoice, self.request)
            logger.info(
                "The {0.type_} number {0.official_number} (id : {0.id})"
                "has been exported".format(invoice)
            )
            invoice.exported = True

            self.request.dbsession.merge(invoice)

    def _collect_export_data(self, invoices):
        """
        Produce the data to export
        """
        result = []
        for invoice in invoices:
            exporter = self.request.find_service(
                ITreasuryProducer, context=invoice
            )
            result.extend(exporter.get_item_book_entries(invoice))
        return result

    def write_file(self, invoices, form_name, appstruct):
        """
        Write the exported csv file to the request
        """
        writer = self.request.find_service(ITreasuryInvoiceWriter)

        logger.debug(writer.headers)
        logger.debug(getattr(writer, 'extra_headers', ()))
        data = self._collect_export_data(invoices)
        writer.set_datas(data)
        write_file_to_request(
            self.request,
            get_timestamped_filename("export_facture", writer.extension),
            writer.render(),
            "application/csv",
            encoding=SAGE_COMPATIBLE_ENCODING,
        )
        return self.request.response


class SageInvoiceExportPage(SageSingleInvoiceExportPage):
    """
        Provide a sage export view compound of :
            * a form for date to date invoice exports
            * a form for number to number invoice export
    """
    title = "Export des factures au format CSV pour Sage"

    def populate_action_menu(self):
        self.request.actionmenu.add(
            ViewLink(
                label="Liste des factures",
                path='invoices',
            )
        )

    def get_forms(self):
        """
            Return the different invoice search forms
        """
        result = OrderedDict()
        all_form = get_invoice_all_form(self.request)
        period_form = get_invoice_period_form(self.request, all_form.counter)
        number_form = get_invoice_number_form(
            self.request, all_form.counter,
        )

        for form in all_form, number_form, period_form:
            result[form.formid] = {'form': form, 'title': form.schema.title}

        return result

    def query(self, query_params_dict, form_name):
        filters = {
            'doctypes': query_params_dict['doctypes']
        }

        if form_name == 'period_form':
            filters.update(dict(
                start_date=query_params_dict['start_date'],
                end_date=query_params_dict['end_date'],
            ))

        elif form_name == 'invoice_number_form':
            filters.update(dict(
                start_number=query_params_dict['start'],
                end_number=query_params_dict['end'],
                year=query_params_dict['financial_year'],
            ))

        try:
            query = query_invoices_for_export(**filters)
        except MultipleResultsFound:
            self.request.session.flash(
                "Votre filtre n'est pas assez précis, plusieurs factures "
                "portent le même numéro, veuillez spécifier une année"
            )
            query = None

        if query:
            exported = query_params_dict.get('exported')
            if not exported:
                if query_params_dict.get('doctypes') == 'internal':
                    query = query.filter(
                        InternalInvoice.exported == False # NOQA
                    )
                elif query_params_dict.get('doctypes') == 'external':
                    query = query.filter(
                        or_(
                            Invoice.exported == False,  # NOQA
                            CancelInvoice.exported == False,  # NOQA
                        )
                    )
                else:
                    query = query.filter(
                        or_(
                            Invoice.exported == False,  # NOQA
                            CancelInvoice.exported == False,  # NOQA
                            InternalInvoice.exported == False,  # NOQA
                        )
                    )

            query = self._filter_by_validator(query, query_params_dict)
        return query

    def _filter_by_validator(self, query, query_params_dict):
        """
        Filter regarding who validated the invoice
        Will only keep all expenses validated by the designated user.
        :param obj query: A sqlalchemy query
        :param dict appstruct: The form datas
        """
        if 'validator_id' in query_params_dict:
            validator_id = query_params_dict['validator_id']
            query = Task.query_by_validator_id(validator_id, query)

        return query

    def check_num_invoices(self, invoices):
        return invoices.count()

    def validate_form(self, forms):
        return BaseExportView.validate_form(self, forms)


def add_routes(config):
    config.add_route(
        '/export/treasury/invoices',
        '/export/treasury/invoices'
    )
    config.add_route(
        '/export/treasury/invoices/{id}',
        '/export/treasury/invoices/{id}',
        traverse="/tasks/{id}"
    )


def add_views(config):
    config.add_view(
        SageSingleInvoiceExportPage,
        route_name='/export/treasury/invoices/{id}',
        renderer='/export/single.mako',
        permission='admin_treasury',
    )

    config.add_view(
        SageInvoiceExportPage,
        route_name='/export/treasury/invoices',
        renderer="/export/main.mako",
        permission='admin_treasury',
    )


def includeme(config):
    add_routes(config)
    add_views(config)
    config.add_admin_menu(
        parent='accounting',
        order=0,
        href="/export/treasury/invoices",
        label="Export des factures",
        permisison="admin_treasury",
    )
