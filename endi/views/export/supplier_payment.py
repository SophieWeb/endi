import logging
import datetime
from collections import OrderedDict

from endi.export.utils import write_file_to_request
from endi.utils.files import get_timestamped_filename
from endi.utils import strings
from endi.models.supply import (
    BaseSupplierInvoicePayment,
    SupplierInvoiceUserPayment,
    SupplierInvoice,
)
from endi.interfaces import (
    ITreasuryProducer,
    ITreasurySupplierPaymentWriter,
)

from endi.utils.widgets import ViewLink
from endi.views.admin.supplier.accounting import SUPPLIER_ACCOUNTING_URL
from endi.views.export.utils import (
    get_supplier_payment_period_form,
    get_supplier_payment_all_form,
    get_supplier_payment_number_form
)
from endi.views.export import (
    BaseExportView,
    SAGE_COMPATIBLE_ENCODING,
)
from endi.views.user.routes import USER_ACCOUNTING_URL


logger = logging.getLogger(__name__)

ERR_CONFIG = """Des éléments de configuration sont manquants pour exporter
les paiements des factures fournisseurs.
<br/>
<a onclick="window.openPopup('{0}')" href='#'>
Configuration comptable du module Fournisseur
</a>"""

ERR_COMPANY_CONFIG = """Un paiement de la facture fournisseur {0}
n'est pas exportable : Le code analytique de l'enseigne {1} n'a pas été
configuré
<a onclick="window.openPopup('{2}');" href='#'>Voir l'enseigne</a>"""
ERR_USER_CONFIG = """Un paiement de la facture fournisseur {0}
n'est pas exportable : Le compte tiers de l'entrepreneur {1} n'a pas été
configuré
<a onclick="window.openPopup('{2}');" href='#'>Voir l'entrepreneur</a>"""
ERR_BANK_CONFIG = """Un paiement de la facture fournisseur {0}
n'est pas exportable : Le paiement n'est associé à aucune banque
<a onclick="window.openPopup('{1}');" href='#'>Voir le paiement</a>"""
ERR_WAIVER_CONFIG = """Le compte pour les abandons de créances n'a pas
été configuré, vous pouvez le configurer
<a onclick="window.openPopup('{}');" href='#'>Ici</a>
"""


class SageSupplierPaymentExportPage(BaseExportView):
    """
    Provide a supplier payment export page
    """
    title = "Export des paiements des factures fournisseurs \
        au format CSV pour Sage"

    def _populate_action_menu(self):
        self.request.actionmenu.add(
            ViewLink(
                label="Liste des factures fournisseurs",
                path='/suppliers_invoices',
            )
        )

    def before(self):
        self._populate_action_menu()

    def get_forms(self):
        """
        Implement parent get_forms method
        """
        result = OrderedDict()
        all_form = get_supplier_payment_all_form(self.request)

        period_form = get_supplier_payment_period_form(
            self.request, all_form.counter
        )

        supplier_invoice_id_form = get_supplier_payment_number_form(
            self.request,
            all_form.counter,
            title="Exporter les paiements d'une facture fournisseur",
        )

        for form in all_form, period_form, supplier_invoice_id_form:
            result[form.formid] = {'form': form, 'title': form.schema.title}
        return result

    def _filter_date(self, query, start_date, end_date):
        if end_date == datetime.date.today():
            query = query.filter(
                BaseSupplierInvoicePayment.date >= start_date
            )
        else:
            query = query.filter(
                BaseSupplierInvoicePayment.date.between(
                    start_date, end_date
                )
            )
        return query

    def _filter_by_supplier_invoice_number(self, query, appstruct):
        """
        Add an id filter on the query
        :param obj query: A sqlalchemy query
        :param dict appstruct: The form datas
        """
        if 'official_number' in appstruct:
            query = query.filter(
                BaseSupplierInvoicePayment.supplier_invoice
            )
            query = query.filter(
                SupplierInvoice.official_number == appstruct['official_number']
            )

        return query

    def _filter_by_issuer(self, query, appstruct):
        """
        Filter according to the issuer of the supplier payment
        :param obj query: A sqlalchemy query
        :param dict appstruct: The form datas
        """
        if 'issuer_id' in appstruct:
            query = query.filter(
                BaseSupplierInvoicePayment.user_id ==
                appstruct['issuer_id']
            )

        return query

    def query(self, query_params_dict, form_name):
        """
            Retrieve the exports we want to export
        """
        query = BaseSupplierInvoicePayment.query()

        if form_name == 'period_form':
            start_date = query_params_dict['start_date']
            end_date = query_params_dict['end_date']
            query = self._filter_date(query, start_date, end_date)

        elif form_name == 'official_number_form':
            query = self._filter_by_supplier_invoice_number(
                query,
                query_params_dict
            )

        if 'exported' not in query_params_dict or \
                not query_params_dict.get('exported'):
            query = query.filter(
                BaseSupplierInvoicePayment.exported == False  # noqa: E712
            )

        query = self._filter_by_issuer(query, query_params_dict)

        return query

    def _check_config(self, payment_query):
        # On regarde si on doit checker la configuration des paiements internes
        has_internal = False
        if payment_query.filter(
            BaseSupplierInvoicePayment.type_ == 'internalsupplier_payment'
        ).count() > 0:
            has_internal = True

        if not self.request.config.get('code_journal_frns'):
            return False

        if has_internal:
            if not self.request.config.get('internalbank_general_account'):
                return False
            if not self.request.config.get('internalcode_journal_frns'):
                return False
        return True

    def _check_bank(self, supplier_payment):
        if not supplier_payment.bank:
            return False
        return True

    def _check_company(self, company):
        if not company.code_compta:
            return False
        return True

    def _check_user(self, user):
        if not user.compte_tiers:
            return False
        return True

    def _check_waiver(self, payment):
        """
        Check that the wayver cg account has been configured
        """
        if not self.request.config.get('compte_cg_waiver_ndf'):
            return False
        return True

    def check(self, supplier_payments):
        """
        Check that the given supplier_payments can be exported

        :param obj supplier_payments: A SQLA query of
        SupplierInvoiceSupplierPayment objects
        """
        count = supplier_payments.count()
        if count == 0:
            title = "Il n'y a aucun paiement fournisseur à exporter"
            res = {
                'title': title,
                'errors': [],
            }
            return False, res

        title = "Vous vous apprêtez à exporter {0} \
            paiements fournisseurs".format(count)
        res = {'title': title, 'errors': []}

        if not self._check_config(supplier_payments):
            config_url = self.request.route_path(
                SUPPLIER_ACCOUNTING_URL
            )
            message = ERR_CONFIG.format(config_url)
            res['errors'].append(message)

        for payment in supplier_payments:
            supplier_invoice = payment.supplier_invoice
            company = supplier_invoice.company
            if not self._check_company(company):
                company_url = self.request.route_path(
                    "company",
                    id=company.id,
                    _query={'action': 'edit'},
                )
                message = ERR_COMPANY_CONFIG.format(
                    supplier_invoice.id,
                    company.name,
                    company_url,
                )
                res['errors'].append(message)
                continue

            if isinstance(payment, SupplierInvoiceUserPayment):
                user = supplier_invoice.payer
                if not self._check_user(user):
                    user_url = self.request.route_path(
                        USER_ACCOUNTING_URL,
                        id=user.id,
                        _query={'action': 'edit'},
                    )
                    message = ERR_USER_CONFIG.format(
                        supplier_invoice.id,
                        strings.format_account(user),
                        user_url,
                    )
                    res['errors'].append(message)
                    continue
                if payment.waiver and not self._check_waiver(payment):
                    admin_url = self.request.route_path(
                        self.admin_route_name
                    )
                    message = ERR_WAIVER_CONFIG.format(admin_url)
                    res['errors'].append(message)
                    continue

            if not payment.internal and not self._check_bank(payment):
                payment_url = self.request.route_path(
                    'supplier_payment',
                    id=payment.id,
                    _query={'action': 'edit'}
                )
                message = ERR_BANK_CONFIG.format(
                    supplier_invoice.id,
                    payment_url)
                res['errors'].append(message)
                continue

        return len(res['errors']) == 0, res

    def record_exported(self, supplier_payments, form_name, appstruct):
        """
        Record that those supplier_payments have already been exported
        """
        for payment in supplier_payments:
            logger.info(
                f"The supplier payment id : {payment.id} (supplier invoice id "
                f"{payment.supplier_invoice.id} / official number "
                f"{payment.supplier_invoice.official_number} ) "
                f"has been exported"
            )
            payment.exported = True
            self.request.dbsession.merge(payment)

    def _collect_export_data(self, supplier_payments):
        result = []
        for supplier_payment in supplier_payments:
            exporter = self.request.find_service(
                ITreasuryProducer, context=supplier_payment
            )
            result.extend(exporter.get_item_book_entries(supplier_payment))
        return result

    def write_file(self, supplier_payments, form_name, appstruct):
        writer = self.request.find_service(ITreasurySupplierPaymentWriter)
        data = self._collect_export_data(supplier_payments)
        writer.set_datas(data)

        write_file_to_request(
            self.request,
            get_timestamped_filename("export_paiements_frn", writer.extension),
            writer.render(),
            "application/csv",
            encoding=SAGE_COMPATIBLE_ENCODING,
        )
        return self.request.response


def add_routes(config):
    config.add_route(
        '/export/treasury/supplier_payments',
        '/export/treasury/supplier_payments'
    )
    config.add_route(
        '/export/treasury/supplier_payments/{id}',
        '/export/treasury/supplier_payments/{id}'
    )


def add_views(config):
    config.add_view(
        SageSupplierPaymentExportPage,
        route_name='/export/treasury/supplier_payments',
        renderer='/export/main.mako',
        permission='admin_treasury',
    )


def includeme(config):
    add_routes(config)
    add_views(config)
    config.add_admin_menu(
        parent='accounting',
        order=5,
        label="Export des paiements de facture fournisseur",
        href="/export/treasury/supplier_payments",
        permission="admin_treasury",
    )
