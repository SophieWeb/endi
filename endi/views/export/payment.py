import logging

from sqlalchemy import or_
from sqlalchemy.orm.exc import MultipleResultsFound
from collections import OrderedDict

from endi.export.utils import write_file_to_request
from endi.models.task import (
    Payment,
    InternalPayment,
    BaseTaskPayment,
    BankRemittance,
)
from endi.interfaces import ITreasuryProducer
from endi.interfaces import ITreasuryPaymentWriter
from endi.utils.files import get_timestamped_filename
from endi.utils.widgets import ViewLink
from endi.views.export.utils import query_invoices_for_export
from endi.views.accounting.routes import BANK_REMITTANCE_ROUTE
from endi.views.admin.sale.receipts import RECEIPT_URL
from endi.views.export import (
    BaseExportView,
    SAGE_COMPATIBLE_ENCODING,
)
from endi.views.export.utils import (
    get_payment_period_form,
    get_payment_all_form,
    get_invoice_number_form,
)

logger = logging.getLogger(__name__)

PAYMENT_VOID_ERROR_MSG = "Il n'y a aucun encaissement à exporter"

PAYMENT_CUSTOMER_ERROR_MSG = """Un encaissement de la facture {0} n'est pas
exportable : Des informations sur le client {1} (compte général) sont
manquantes
 <a onclick="window.openPopup('{2}');" href='#'>Voir le client</a>"""

PAYMENT_BANK_ERROR_MSG = """Un encaissement de la facture {0}
n'est pas exportable : L'encaissement n'est associé à aucune banque
<a onclick="window.openPopup('{1}');" href='#'>
    Voir l'encaissement
</a>"""

PAYMENT_REMITTANCE_INFO_MSG = """Les encaissements associés à une remise en
banque non clôturée ne seront pas exportées en comptabilité.<br/><br/>
<a onclick="window.openPopup('{}');" href='#'>Voir les remises ouvertes</a>"""


class SagePaymentExportPage(BaseExportView):
    """
    Provide a sage export view compound of multiple forms for payment exports
    """
    title = "Export des encaissements au format CSV pour Sage"
    help_message = None
    admin_route_name = RECEIPT_URL

    def _populate_action_menu(self):
        self.request.actionmenu.add(
            ViewLink(
                label="Liste des factures",
                path='invoices',
            )
        )

    def before(self):
        self._populate_action_menu()
        self.help_message = PAYMENT_REMITTANCE_INFO_MSG.format(
            self.request.route_path(
                BANK_REMITTANCE_ROUTE,
                _query=dict(__formid__="deform")
            )
        )

    def get_forms(self):
        """
        Return the different payment search forms
        """
        result = OrderedDict()
        all_form = get_payment_all_form(self.request)
        period_form = get_payment_period_form(self.request, all_form.counter)

        number_form = get_invoice_number_form(
            self.request,
            all_form.counter,
            title="Exporter les encaissements correspondant des factures",
        )

        for form in all_form, number_form, period_form:
            result[form.formid] = {'form': form, 'title': form.schema.title}

        return result

    def _filter_doctypes(self, query, doctypes):
        if doctypes == 'internal':
            query = query.filter_by(type_='internalpayment')
        elif doctypes == 'external':
            query = query.filter_by(type_='payment')
        return query

    def _filter_date(self, query, start_date, end_date):
        return query.filter(
            BaseTaskPayment.date.between(start_date, end_date)
        )

    def _filter_number(self, query, start, end, year, doctypes):
        filters = dict(
            start_number=start,
            end_number=end,
            year=year,
            doctypes=doctypes,
        )
        try:
            task_query = query_invoices_for_export(**filters)
            task_ids = [t.id for t in task_query]
        except MultipleResultsFound:
            self.request.session.flash(
                "Votre filtre n'est pas assez précis, plusieurs factures "
                "portent le même numéro, veuillez spécifier une année"
            )
            task_ids = []

        return query.filter(BaseTaskPayment.task_id.in_(task_ids))

    def _filter_open_remittances(self, query):
        br_query = self.request.dbsession.query(BankRemittance.id)
        br_query = br_query.filter(BankRemittance.closed == 0)
        open_br_ids = [item[0] for item in br_query]
        return query.filter(or_(
            Payment.bank_remittance_id.notin_(open_br_ids),
            Payment.bank_remittance_id == None,  # noqa: E711
            BaseTaskPayment.type_ == 'internal'
        ))

    def _filter_by_issuer(self, query, query_params_dict):
        if 'issuer_id' in query_params_dict:
            issuer_id = query_params_dict['issuer_id']
            query = query.filter(
                BaseTaskPayment.user_id == issuer_id
            )

        return query

    def query(self, query_params_dict, form_name):
        # NB : si on veut exporter les paiements internes, il faut le gérer ici
        query = BaseTaskPayment.query().with_polymorphic(
            [Payment, InternalPayment]
        )
        query = self._filter_open_remittances(query)

        if form_name == "period_form":
            start_date = query_params_dict['start_date']
            end_date = query_params_dict['end_date']
            query = self._filter_date(query, start_date, end_date)
            query = self._filter_doctypes(query, query_params_dict['doctypes'])

        elif form_name == 'invoice_number_form':
            start = query_params_dict['start']
            end = query_params_dict['end']
            financial_year = query_params_dict['financial_year']
            query = self._filter_number(
                query, start, end, financial_year,
                query_params_dict['doctypes']
            )
        else:
            query = self._filter_doctypes(query, query_params_dict['doctypes'])

        if 'exported' not in query_params_dict or \
                not query_params_dict.get('exported'):
            query = query.filter(
                BaseTaskPayment.exported == False  # noqa: E712
            )  # noqa: E712

        query = self._filter_by_issuer(query, query_params_dict)

        return query

    def _check_customer(self, customer, invoice):
        """
            Check the invoice's customer is configured for exports
        """
        prefix = ''
        if invoice.internal:
            prefix = 'internal'
        if not customer.get_general_account(prefix):
            return False
        return True

    def _check_bank(self, payment):
        if payment.bank is None:
            return False
        return True

    def check(self, payments):
        """
        Check that the given payments are 'exportable'
        :param obj payments: a SQLA query of BaseTaskPayments
        """
        count = payments.count()
        if count == 0:
            res = {
                'title': PAYMENT_VOID_ERROR_MSG,
                'errors': [],
            }
            return False, res

        title = "Vous vous apprêtez à exporter {0} encaissements".format(
                count)
        res = {'title': title, 'errors': []}
        for payment in payments:
            invoice = payment.invoice
            if not self._check_customer(invoice.customer, invoice):
                customer_url = self.request.route_path(
                    'customer',
                    id=invoice.customer.id,
                    _query={'action': 'edit'})
                message = PAYMENT_CUSTOMER_ERROR_MSG.format(
                    invoice.official_number,
                    invoice.customer.label,
                    customer_url)
                res['errors'].append(message)
                continue

            if not payment.internal and not self._check_bank(payment):
                payment_url = self.request.route_path(
                    'payment',
                    id=payment.id,
                    _query={'action': 'edit'}
                )
                message = PAYMENT_BANK_ERROR_MSG.format(
                    invoice.official_number,
                    payment_url)
                res['errors'].append(message)
                continue

        return len(res['errors']) == 0, res

    def record_exported(self, payments, form_name, appstruct):
        for payment in payments:
            logger.info(
                "The payment id : {0} (invoice {1} id:{2}) has been exported"
                .format(
                    payment.id,
                    payment.invoice.official_number,
                    payment.invoice.id,
                )
            )
            payment.exported = True
            self.request.dbsession.merge(payment)

    def _collect_export_data(self, payments):
        """
        Produce the data to export
        """
        result = []
        for payment in payments:
            exporter = self.request.find_service(
                ITreasuryProducer, context=payment
            )
            result.extend(exporter.get_item_book_entries(payment))
        return result

    def write_file(self, payments, form_name, appstruct):
        """
            Write the exported csv file to the request
        """
        writer = self.request.find_service(ITreasuryPaymentWriter)

        data = self._collect_export_data(payments)
        writer.set_datas(data)
        write_file_to_request(
            self.request,
            get_timestamped_filename("export_encaissement", writer.extension),
            writer.render(),
            "application/csv",
            encoding=SAGE_COMPATIBLE_ENCODING,
        )
        return self.request.response


def add_routes(config):
    config.add_route(
        '/export/treasury/payments',
        '/export/treasury/payments'
    )
    config.add_route(
        '/export/treasury/payments/{id}',
        '/export/treasury/payments/{id}'
    )


def add_views(config):
    config.add_view(
        SagePaymentExportPage,
        route_name='/export/treasury/payments',
        renderer='/export/main.mako',
        permission='admin_treasury',
    )


def includeme(config):
    add_routes(config)
    add_views(config)

    config.add_admin_menu(
        parent='accounting',
        order=1,
        label="Export des encaissements",
        href="/export/treasury/payments",
        permission="admin_treasury",
    )
