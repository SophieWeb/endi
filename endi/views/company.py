"""
    Views related to the Company model

    Permissions :
        admin_company : disable/enable add/remove employees
        view.company : view all informations concerning the company
        edit_company : edit company options

    A - Des permissions à définir sur l'ensemble de l'application
    B - Des groupes génériques qui rassemblent des permissions
    C - Des permissions spécifiques à certains users
    D - Des permissions par objets
"""

from collections import namedtuple
import logging
import colander

from deform_extensions import AccordionFormWidget
from pyramid.decorator import reify
from pyramid.httpexceptions import HTTPFound

from endi.models.company import (
    Company,
    CompanyActivity,
)
from endi.models.user.user import User
from endi.utils.widgets import (
    ViewLink,
    Link,
    POSTButton,
)
from endi.forms import (
    merge_session_with_post,
)
from endi.views import (
    BaseFormView,
    submit_btn,
    BaseListView,
    add_panel_view,
    DisableView,
)
from endi.views.render_api import format_account
from endi.views.user.routes import (
    USER_LOGIN_URL,
    USER_URL,
)
from endi.views.files.routes import FILE_PNG_ITEM
from endi.forms.company import (
    COMPANYSCHEMA,
    CompanySchema,
    get_list_schema,
)
from endi.resources import dashboard_resources


logger = logging.getLogger(__name__)


ENABLE_MSG = "L'enseigne {0} a été (ré)activée."
DISABLE_MSG = "L'enseigne {0} a été désactivée."

ENABLE_ERR_MSG = "Erreur à l'activation de l'enseigne {0}."
DISABLE_ERR_MSG = "Erreur à la désactivation de l'enseigne {0}."
PUBLIC_DATA_INFO = """
  Les <em>Informations publiques</em> apparaissent sur les devis/factures,
  dans l'annuaire des entrepreneurs,
  et peuvent être publiées à l'extérieur de la CAE.
"""


ShortcutButton = namedtuple('ShortcutButton', ['url', 'icon', 'text', 'title'])


def _get_company_shortcuts(company, user, request) -> dict:
    buttons = []
    msg = ''
    if company.customers:
        if company.projects:
            buttons.append(ShortcutButton(
                url=request.route_path(
                    "company_estimations",
                    id=company.id,
                    _query=dict(action='add'),
                ),
                icon='file-list',
                text="Créer un devis",
                title="Créer un nouveau devis",
            ))
            if request.has_permission('add.invoice', company):
                buttons.append(ShortcutButton(
                    url=request.route_path(
                        "company_invoices",
                        id=company.id,
                        _query=dict(action='add'),
                    ),
                    icon='file-invoice-euro',
                    text="Créer une facture",
                    title="Créer une nouvelle facture",
                ))
        else:
            msg = "Ajoutez un dossier qui contiendra des devis et factures"
    else:
        msg = "Pour commencer, ajoutez un client"

    if len(company.employees) == 1 or request.user in company.employees:
        if request.user in company.employees:
            expense_user = request.user
        else:
            # EA externe à l'enseigne
            expense_user = company.employees[0]

        buttons.append(ShortcutButton(
                url=request.route_path(
                    "user_expenses", id=company.id, uid=expense_user.id,
                ),
                icon='credit-card',
                text="Créer une note de dépense",
                title="Créer une nouvelle note de dépense",
        )),
    # EA externe + multi-employés : on affiche pas le bouton.

    if company.customers:
        buttons.append(ShortcutButton(
            url=request.route_path(
                '/companies/{id}/projects',
                id=company.id,
                _query=dict(action='add')
            ),
            icon='folder',
            text="Ajouter un dossier",
            title="Ajouter un nouveau dossier",
        ))

    # Ref https://framagit.org/endi/endi/-/issues/2485
    # buttons.append(ShortcutButton(
    #     url=request.route_path(
    #         'company_customers',
    #         id=company.id,
    #         _query=dict(action='add'),
    #     ),
    #     icon='user',
    #     text="Ajouter un client",
    #     title="Ajouter un nouveau client",
    # ))
    return dict(
        shortcuts_msg=msg,
        shortucts_buttons=buttons,
    )


def company_index(request):
    """
        index page for the company shows latest news :
            - last validated estimation/invoice
            - To be relaunched bill
            - shortcut buttons
    """
    dashboard_resources.need()
    company = request.context

    shortcuts = _get_company_shortcuts(company, request.user, request)

    ret_val = dict(
        title=company.name.title(),
        company=company,
        elapsed_invoices=request.context.get_late_invoices(),
    )
    ret_val.update(shortcuts)
    return ret_val


def company_view(context, request):
    """
        Company main view
    """
    company = request.context
    populate_actionmenu(request)

    actions = []
    if request.has_permission('edit_company'):
        actions.append(
            Link(
                request.route_path(
                    'company', id=context.id, _query=dict(action='edit')
                ),
                "Modifier",
                title="Modifier l´enseigne",
                icon='pen',
                css="btn btn-primary icon"
            )
        )

    if request.has_permission('admin_company'):
        url = request.route_path(
            'company',
            id=context.id,
            _query=dict(action="disable")
        )
        if company.active:
            actions.append(
                POSTButton(
                    url,
                    "Désactiver",
                    title="Désactiver l'enseigne",
                    icon='lock',
                    css="icon"
                )
            )
        else:
            actions.append(
                POSTButton(
                    url,
                    "Activer",
                    title="Activer l'enseigne",
                    icon='lock-open',
                    css="icon"
                )
            )
    return dict(
        title=company.name.title(),
        company=company,
        actions=actions,
    )


class CompanyDisableView(DisableView):
    def on_disable(self):
        """
        Disable logins of users that are only attached to this company
        """
        for user in self.context.employees:
            other_enabled_companies = [
                company
                for company in user.companies
                if company.active and company.id != self.context.id
            ]
            if getattr(user, 'login') and user.login.active and \
                    len(other_enabled_companies) == 0:
                user.login.active = False
                self.request.dbsession.merge(user.login)
                user_url = self.request.route_path(
                    USER_LOGIN_URL, id=user.id
                )
                self.request.session.flash(
                    "Les identifiants de <a href='{0}'>{1}</a> ont été \
                    désactivés".format(user_url, user.label)
                )

    def redirect(self):
        return HTTPFound(self.request.referrer)


class CompanyList(BaseListView):
    title = "Annuaire des enseignes"
    schema = get_list_schema()
    sort_columns = dict(name=Company.name)
    default_sort = 'name'
    default_direction = 'asc'

    add_template_vars = (
        'title',
        'stream_actions',
    )

    def query(self):
        return Company.query(active=False)

    def filter_include_inactive(self, query, appstruct):
        include_inactive = appstruct.get('include_inactive', False)

        if include_inactive in ("false", False, colander.null):
            query = query.filter_by(active=True)

        return query

    def filter_include_internal(self, query, appstruct):
        include_internal = appstruct.get('include_internal', False)

        if include_internal in ("false", False, colander.null):
            query = query.filter_by(internal=False)

        return query

    def filter_search(self, query, appstruct):
        search = appstruct.get('search')
        if search:
            query = query.filter(Company.name.like('%' + search + '%'))
        return query

    def stream_actions(self, company):
        yield Link(
            self.request.route_path('company', id=company.id),
            "Modifier",
            title="Modifier l'enseigne",
            icon='pen',
            css="icon"
        )
        url = self.request.route_path(
            'company',
            id=company.id,
            _query=dict(action="disable")
        )
        if company.active:
            yield POSTButton(
                url,
                "Désactiver",
                title="Désactiver l'enseigne",
                icon='lock',
                css="icon"
            )
        else:
            yield POSTButton(
                url,
                "Activer",
                title="Activer l'enseigne",
                icon='lock-open',
                css="icon"
            )


def fetch_activities_objects(appstruct):
    """
    Fetch company activities in order to be able to associate them to the
    company
    """
    activities = appstruct.pop('activities', None)
    if activities:
        return [
            CompanyActivity.get(activity_id)
            for activity_id in activities
        ]
    return []


class CompanyAdd(BaseFormView):
    """
    View class for company add

    Have support for a user_id request param that allows to add the user
    directly on company creation

    """
    add_template_vars = ('title',)
    title = "Ajouter une enseigne"
    schema = CompanySchema()
    buttons = (submit_btn,)

    def before(self, form):
        """
        prepopulate the form and the actionmenu
        """
        populate_actionmenu(self.request)
        if 'user_id' in self.request.params:
            appstruct = {"user_id": self.request.params['user_id']}

            come_from = self.request.referrer
            if come_from:
                appstruct['come_from'] = come_from

            form.set_appstruct(appstruct)

    def submit_success(self, appstruct):
        """
        Edit the database entry and return redirect
        """
        come_from = appstruct.pop('come_from', None)
        user_id = appstruct.get('user_id')
        company = Company()
        company.activities = fetch_activities_objects(appstruct)
        company = merge_session_with_post(company, appstruct)
        if user_id is not None:
            user_account = User.get(user_id)
            if user_account is not None:
                company.employees.append(user_account)

        self.dbsession.add(company)
        self.dbsession.flush()
        message = "L'enseigne '{0}' a bien été ajoutée".format(company.name)
        self.session.flash(message)

        if come_from is not None:
            return HTTPFound(come_from)
        else:
            return HTTPFound(self.request.route_path("company", id=company.id))


class CompanyEdit(BaseFormView):
    """
        View class for company editing
    """
    add_template_vars = ('title', 'info_message')
    schema = COMPANYSCHEMA
    buttons = (submit_btn,)
    info_message = PUBLIC_DATA_INFO

    @reify
    def title(self):
        """
            title property
        """
        return "Modification de {0}".format(self.context.name.title())

    def before(self, form):
        """
            prepopulate the form and the actionmenu
        """
        appstruct = self.context.appstruct()
        appstruct['activities'] = [
            a.id for a in self.request.context.activities
        ]

        for filetype in ('logo', 'header'):
            # On récupère un éventuel id de fichier
            file_id = appstruct.pop('%s_id' % filetype, '')
            if file_id:
                # Si il y a déjà un fichier de ce type dans la base on
                # construit un appstruct avec un uid, un filename et une url de
                # preview
                appstruct[filetype] = {
                    'uid': file_id,
                    'filename': '%s.png' % file_id,
                    'preview_url': self.request.route_path(
                        FILE_PNG_ITEM,
                        id=file_id,
                    )
                }

        form.set_appstruct(appstruct)
        form.widget = AccordionFormWidget()
        populate_actionmenu(self.request, self.context)
        self.old_general_overhead = self.context.general_overhead
        self.old_margin_rate = self.context.margin_rate

    def _sync_sale_coefficients(self):
        """
        Sync the sale coefficients along all the SaleProduct related elements
        """
        self.context.sync_general_overhead(
            self.old_general_overhead, self.context.general_overhead
        )
        self.context.sync_margin_rate(
            self.old_margin_rate, self.context.margin_rate
        )

    def submit_success(self, appstruct):
        """
        Edit the database entry and return redirect
        """
        self.request.context.activities = fetch_activities_objects(appstruct)
        logger.debug("New company attributes : {}".format(appstruct))
        company = merge_session_with_post(
            self.request.context, appstruct, remove_empty_values=False
        )
        company = self.dbsession.merge(company)
        self.dbsession.flush()

        self._sync_sale_coefficients()

        message = "Votre enseigne a bien été modifiée"
        self.session.flash(message)
        # Clear all informations stored in session by the tempstore used for
        # the file upload widget
        self.request.session.pop('substanced.tempstore')
        self.request.session.changed()
        return HTTPFound(self.request.route_path("company", id=company.id))


def populate_actionmenu(request, company=None):
    """
        add item in the action menu
    """
    request.actionmenu.add(get_list_view_btn())
    if company is not None:
        request.actionmenu.add(get_view_btn(company.id))


def get_list_view_btn():
    """
        Return a link to the CAE's directory
    """
    return ViewLink("Annuaire", "visit", path=USER_URL)


def get_view_btn(company_id):
    """
        Return a link to the view page
    """
    return ViewLink("Voir", "visit", path="company", id=company_id)


def company_remove_employee_view(context, request):
    """
    Enlève un employé de l'enseigne courante
    """
    uid = request.params.get('uid')
    if not uid:
        request.session.flash('Missing uid parameter', 'error')
    else:
        user = User.get(uid)
        if not user:
            request.session.flash('User not found', 'error')

        if user in context.employees:
            context.employees = [
                employee for employee in context.employees if employee != user
            ]
            request.session.flash(
                "L'utilisateur {0} ne fait plus partie de l'enseigne "
                "{1}".format(
                    format_account(user), context.name
                )
            )
    url = request.referer
    if url is None:
        url = request.route_path('company', id=context.id)
    return HTTPFound(url)


def add_routes(config):
    """
    Configure routes for this module
    """
    config.add_route(
        'companies',
        "/companies",
    )
    config.add_route(
        'company',
        r'/companies/{id:\d+}',
        traverse='/companies/{id}'
    )
    config.add_route(
        'company_index',
        r'/company/{id:\d+}/dashboard',
        traverse='/companies/{id}',
    )
    return config


def includeme(config):
    """
        Add all company related views
    """
    config = add_routes(config)
    config.add_view(
        CompanyList,
        route_name='companies',
        renderer='companies.mako',
        permission='admin_companies',
    )
    config.add_view(
        CompanyAdd,
        route_name='companies',
        renderer="base/formpage.mako",
        request_param="action=add",
        permission="admin_companies",
    )
    config.add_view(
        company_index,
        route_name='company_index',
        renderer='company_index.mako',
        permission='view.company',
    )
    config.add_view(
        company_index,
        route_name='company',
        renderer='company_index.mako',
        request_param='action=index',
        permission='view.company',
    )
    config.add_view(
        company_view,
        route_name='company',
        renderer='company.mako',
        permission="visit",
    )
    config.add_view(
        CompanyEdit,
        route_name='company',
        renderer='base/formpage.mako',
        request_param='action=edit',
        permission="edit_company",
    )
    config.add_view(
        CompanyDisableView,
        route_name='company',
        request_param='action=disable',
        permission="admin_company",
        require_csrf=True,
        request_method='POST',
    )
    config.add_view(
        company_remove_employee_view,
        route_name="company",
        request_param='action=remove',
        permission="admin_company",
        require_csrf=True,
        request_method="POST",
    )
    # same panel as html view
    for panel, request_param in (
            ('company_recent_tasks', 'action=tasks_html',),
            ('company_coming_events', 'action=events_html',),
            ):
        add_panel_view(
            config,
            panel,
            route_name='company',
            request_param=request_param,
            permission="view.company",
        )
