import logging

from sqlalchemy import desc
from pyramid.httpexceptions import (
    HTTPFound,
)
from sqla_inspect import csv

from endi.resources import statistic_resources
from endi.models.user.userdatas import UserDatas
from endi.models.statistics import (
    StatisticSheet,
)
from endi.statistics import (
    EntryQueryFactory,
    SheetQueryFactory,
    get_inspector,
)
from endi.utils.widgets import (
    Link,
    POSTButton,
)
from endi_base.utils import ascii
from endi.forms.statistics import get_sheet_add_edit_schema
from endi.views import (
    BaseView,
    DisableView,
    DeleteView,
    DuplicateView,
    BaseAddView,
    BaseEditView,
    TreeMixin,
)
from endi.views.userdatas.lists import UserDatasCsvView
from endi.export.utils import write_file_to_request

from .routes import (
    get_sheet_url,
    STATISTICS_ROUTE,
    STATISTIC_ITEM_ROUTE,
    STATISTIC_ITEM_CSV_ROUTE,
    ENTRY_ITEM_CSV_ROUTE,
    API_ENTRIES_ROUTE,
)


logger = logging.getLogger(__name__)


class SheetListView(BaseView, TreeMixin):
    """
    Listview of statistic sheets
    """
    title = "Feuilles de statistiques"
    route_name = STATISTICS_ROUTE

    def stream_actions(self, sheet):
        """
        Collect action button definitions for the given sheet
        """
        yield Link(
            get_sheet_url(self.request, sheet),
            "Voir / Modifier",
            "Voir cette feuille de statistiques",
            icon="pen",
        )
        yield POSTButton(
            get_sheet_url(self.request, sheet, _query={'action': 'duplicate'}),
            "Dupliquer",
            "Dupliquer cette feuille de statistiques",
            icon="copy",
        )
        if sheet.active:
            label = "Désactiver"
            title = "Désactiver cette feuille de statistiques"
            icon = "lock"
        else:
            label = "Activer"
            title = "Activer cette feuille de statistiques"
            icon = "lock-open"

        yield POSTButton(
            get_sheet_url(self.request, sheet, _query={'action': 'disable'}),
            label,
            title=title,
            icon=icon,
        )
        if not sheet.active:
            yield POSTButton(
                get_sheet_url(
                    self.request, sheet, _query={'action': 'delete'}
                ),
                'Supprimer',
                title="Supprimer définitivement",
                icon='trash-alt',
                css='negative',
            )

    def _get_links(self):
        return [
            Link(
                self.request.route_path(
                    STATISTICS_ROUTE,
                    _query=dict(action='add'),
                ),
                'Ajouter',
                'Ajouter une nouvelle feuille de statistiques',
                icon="plus"
            )
        ]

    def __call__(self):
        sheets = StatisticSheet.query()
        sheets = sheets.order_by(desc(StatisticSheet.active)).all()

        return dict(
            title=self.title,
            sheets=sheets,
            links=self._get_links(),
            stream_actions=self.stream_actions,
        )


class SheetAddView(BaseAddView, TreeMixin):
    title = "Ajouter une feuille de statistiques"
    schema = get_sheet_add_edit_schema()
    msg = "La feuille a été ajoutée avec succès"
    factory = StatisticSheet
    route_name = STATISTICS_ROUTE

    def before(self, form):
        BaseAddView.before(self, form)
        self.populate_navigation()

    def redirect(self, new_model):
        return HTTPFound(
            self.request.route_path(
                STATISTIC_ITEM_ROUTE,
                id=new_model.id
            )
        )


class SheetEditView(BaseEditView, TreeMixin):
    title = "Modifier une feuille de statistiques"
    schema = get_sheet_add_edit_schema()
    msg = "La feuille a bien été modifiée"
    route_name = STATISTIC_ITEM_ROUTE

    @property
    def tree_url(self):
        return get_sheet_url(self.request, _query={'action': 'edit'})

    def before(self, form):
        BaseEditView.before(self, form)
        self.populate_navigation()

    def redirect(self):
        return HTTPFound(
            self.request.route_path(
                STATISTIC_ITEM_ROUTE,
                id=self.context.id
            )
        )


class SheetView(BaseView, TreeMixin):
    """
    Statistic sheet view
    """
    route_name = STATISTIC_ITEM_ROUTE

    @property
    def title(self):
        return f"{self.context.title}"

    def __call__(self):
        self.populate_navigation()
        statistic_resources.need()
        form_config_url = get_sheet_url(
            self.request,
            api=True,
            _query=dict(form_config='1')
        )

        return dict(
            title=self.title,
            js_app_options={
                'context_url': get_sheet_url(self.request, api=True),
                'form_config_url': form_config_url,
                'entries_url': self.request.route_path(
                    API_ENTRIES_ROUTE, id=self.context.id
                )
            }
        )


class StatisticDisableView(DisableView):
    """
    Sheet Disable view
    """
    enable_msg = "La feuille de statistiques a été activée"
    disable_msg = "La feuille de statistiques a été désactivée"
    redirect_route = STATISTICS_ROUTE


class StatisticDuplicateView(DuplicateView):
    """
    Sheet Duplication view
    """
    message = "Vous avez été redirigé vers la nouvelle feuille de statistique"
    route_name = STATISTIC_ITEM_ROUTE

    def redirect(self, item):
        """
        Default redirect implementation

        :param obj item: The newly created element (flushed)
        :returns: The url to redirect to
        :rtype: str
        """
        return HTTPFound(
            self.request.route_path(
                self.route_name, id=item.id, _query={'action': 'edit'}
            )
        )


class StatisticDeleteView(DeleteView):
    """
    Sheet Deletion view
    """
    delete_msg = "La feuille de statistiques a bien été supprimée"
    redirect_route = STATISTICS_ROUTE


class CsvEntryView(UserDatasCsvView):
    """
    The view used to stream a the items matching a statistic entry
    """
    model = UserDatas

    def query(self):
        inspector = get_inspector()
        try:
            query_factory = EntryQueryFactory(
                UserDatas, self.context, inspector
            )
            query = query_factory.query()
        except KeyError as exc:
            logger.exception("Error in csv entry view")
            query = None
            self.current_exception = exc
        return query

    @property
    def filename(self):
        filename = "{0}.csv".format(ascii.force_ascii(self.context.title))
        return filename

    def show_error(self, msg: str, show_refresh_button=False) -> dict:
        return {'info_msg': msg}

    def _build_return_value(self, schema, appstruct, query):
        if query is None:
            return {'info_msg': str(self.current_exception)}
        else:
            return UserDatasCsvView._build_return_value(
                self, schema, appstruct, query
            )


class CsvSheetView(BaseView):
    """
    Return a csv sheet as a csv response
    """
    @property
    def filename(self):
        return "{0}.csv".format(ascii.force_ascii(self.context.title))

    def __call__(self):
        try:
            query_factory = SheetQueryFactory(
                UserDatas,
                self.context,
                get_inspector()
            )

            writer = csv.CsvExporter()
            writer.set_headers(query_factory.headers)
            for row in query_factory.rows:
                writer.add_row(row)

            write_file_to_request(self.request, self.filename, writer.render())
            return self.request.response
        except KeyError as exc:
            return {'info_msg': str(exc)}


def includeme(config):
    """
    Include views in the app's configuration
    """
    config.add_tree_view(
        SheetListView,
        renderer="statistics/list.mako",
        permission='list_statistics',
    )

    config.add_tree_view(
        SheetAddView,
        parent=SheetListView,
        request_param="action=add",
        permission='add_statistic',
        renderer='endi:templates/base/formpage.mako',
        layout='default'
    )
    config.add_tree_view(
        SheetEditView,
        parent=SheetListView,
        request_param="action=edit",
        permission='edit_statistic',
        renderer='endi:templates/base/formpage.mako',
        layout='default'
    )

    config.add_tree_view(
        SheetView,
        parent=SheetListView,
        renderer='statistics/edit.mako',
        permission="view_statistic",
        layout='opa',
    )

    config.add_view(
        StatisticDisableView,
        route_name=STATISTIC_ITEM_ROUTE,
        request_param="action=disable",
        permission='edit_statistic',
        request_method="POST",
        require_csrf=True,
    )
    config.add_view(
        StatisticDeleteView,
        route_name=STATISTIC_ITEM_ROUTE,
        request_param="action=delete",
        permission='edit_statistic',
        request_method="POST",
        require_csrf=True,
    )

    config.add_view(
        StatisticDuplicateView,
        route_name=STATISTIC_ITEM_ROUTE,
        request_param="action=duplicate",
        permission='edit_statistic',
        request_method="POST",
        require_csrf=True,
    )

    # Csv export views
    config.add_view(
        CsvEntryView,
        route_name=ENTRY_ITEM_CSV_ROUTE,
        permission='view_statistic',
        renderer='statistics/info.mako',
    )

    config.add_view(
        CsvSheetView,
        route_name=STATISTIC_ITEM_CSV_ROUTE,
        permission='view_statistic',
        renderer='statistics/info.mako',
    )
    config.add_admin_menu(
        parent="userdata",
        order=1,
        label="Statistiques",
        href="/statistics"
    )
