def includeme(config):
    config.include('.routes')
    config.include('.statistics')
    config.include('.rest_api')
