from deform import Form
from deform_extensions import GridFormWidget

from endi.forms.third_party.supplier import (
    get_supplier_schema,
)
from endi.views import submit_btn
from endi.views.third_party.customer.base import (
    COMPANY_FORM_GRID,
)


def get_supplier_form(request, counter=None):
    """
    Returns the supplier add/edit form
    :param obj request: Pyramid's request object
    :param obj counter: An iterator for field number generation
    :returns: a deform.Form instance
    """
    schema = get_supplier_schema()
    schema = schema.bind(request=request)
    form = Form(
        schema,
        buttons=(submit_btn,),
        counter=counter,
        formid='supplier',
    )
    form.widget = GridFormWidget(named_grid=COMPANY_FORM_GRID)
    return form
