
def includeme(config):
    config.add_route(
        'customer',
        '/customers/{id}',
        traverse='/customers/{id}',
    )
    config.add_route(
        "/api/v1/companies/{id}/customers",
        "/api/v1/companies/{id}/customers",
        traverse="/companies/{id}",
    )
    config.add_route(
        "/api/v1/customers/{id}",
        "/api/v1/customers/{id}",
        traverse='/customers/{id}',
    )

    config.add_route(
        'company_customers',
        r'/company/{id:\d+}/customers',
        traverse='/companies/{id}',
    )

    config.add_route(
        'customers.csv',
        r'/company/{id:\d+}/customers.csv',
        traverse='/companies/{id}'
    )
