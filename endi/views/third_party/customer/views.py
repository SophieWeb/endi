import logging
import re
from pyramid.decorator import reify
from pyramid.httpexceptions import HTTPFound

from deform import Form
from deform_extensions import GridFormWidget

from endi.models.third_party.customer import Customer
from endi.models.project.project import Project

from endi.views import (
    BaseFormView,
    submit_btn,
    cancel_btn,
    BaseRestView,
)
from endi.utils.widgets import (
    Link,
    POSTButton,
)
from endi.forms.third_party.customer import CustomerAddToProjectSchema
from endi.forms.third_party.customer import (
    get_add_edit_customer_schema,
)
from endi.views.project.routes import (
    COMPANY_PROJECTS_ROUTE,
)
from endi.views.csv_import import (
    CsvFileUploadView,
    ConfigFieldAssociationView,
)
from .base import (
    populate_actionmenu,
    SCHEMA_FACTORIES,
    FORM_GRIDS,
)

logger = logging.getLogger(__name__)


class CustomerView(BaseFormView):
    """
        Return the view of a customer
    """

    def get_company_projects_form(self):
        """
        Return a form object for project add
        :param obj request: The pyramid request object
        :returns: A form
        :rtype: class:`deform.Form`
        """
        schema = CustomerAddToProjectSchema().bind(
            request=self.request,
            context=self.context
        )
        form = Form(
            schema,
            buttons=(submit_btn,),
            action=self.request.route_path(
                'customer',
                id=self.context.id,
                _query={'action': 'addcustomer'}
            ),
        )
        return form

    def _get_buttons(self):
        yield Link(
            self.request.route_path(
                "customer",
                id=self.context.id,
                _query={'action': 'edit'},
            ),
            "Modifier les informations générales",
            title="",
            icon="pen",
            css="btn btn-primary",
        )
        if self.request.has_permission('delete_customer'):
            yield POSTButton(
                self.request.route_path(
                    "customer",
                    id=self.context.id,
                    _query=dict(action="delete"),
                ),
                "Supprimer",
                title="Supprimer définitivement ce client",
                icon="trash-alt",
                css="negative",
                confirm="Êtes-vous sûr de vouloir supprimer ce client ?",
            )
        elif self.request.has_permission('edit_customer'):
            if self.context.archived:
                label = "Désarchiver"
                css = ""
            else:
                label = "Archiver"
                css = "negative"

            yield POSTButton(
                self.request.route_path(
                    "customer",
                    id=self.context.id,
                    _query=dict(action="archive"),
                ),
                label,
                icon="archive",
                css=css,
            )

    def __call__(self):
        populate_actionmenu(self.request)
        title = "Client : {0}".format(self.context.label)
        if self.request.context.code:
            title += " {0}".format(self.context.code)

        return dict(
            title=title,
            customer=self.request.context,
            project_form=self.get_company_projects_form(),
            add_project_url=self.request.route_path(
                COMPANY_PROJECTS_ROUTE,
                id=self.context.company.id,
                _query={'action': 'add', 'customer': self.context.id}
            ),
            actions=list(self._get_buttons()),
        )


def customer_archive(request):
    """
    Archive the current customer
    """
    customer = request.context
    if not customer.archived:
        customer.archived = True
    else:
        customer.archived = False
    request.dbsession.merge(customer)
    return HTTPFound(request.referer)


def customer_delete(request):
    """
        Delete the current customer
    """
    customer = request.context
    company_id = customer.company_id
    request.dbsession.delete(customer)
    request.session.flash(
        "Le client '{0}' a bien été supprimé".format(customer.label)
    )
    # On s'assure qu'on ne redirige pas vers la route courante
    if re.compile('.*customers/[0-9]+.*').match(request.referer):
        redirect = request.route_path('company_customers', id=company_id)
    else:
        redirect = request.referer
    return HTTPFound(redirect)


class CustomerAddToProject(BaseFormView):
    """
        Catch customer id and update project customers
    """
    schema = CustomerAddToProjectSchema()
    validation_msg = "Le dossier a été ajouté avec succès"

    def submit_success(self, appstruct):
        project_id = appstruct['project_id']
        project = self.dbsession.query(Project).filter_by(id=project_id).one()
        customer = self.context
        project.customers.append(customer)
        self.dbsession.flush()
        self.session.flash(self.validation_msg)
        redirect = self.request.route_path(
            'customer',
            id=self.context.id,
        )
        return HTTPFound(redirect)


class CustomerAdd(BaseFormView):
    """
    Customer add form
    """
    add_template_vars = ('title', 'customers', )
    title = "Ajouter un client"
    _schema = None
    buttons = (submit_btn, cancel_btn)
    validation_msg = "Le client a bien été ajouté"
    edit = False

    @property
    def form_options(self):
        formid = self.form_label()
        return (('formid', formid),)

    @property
    def customers(self):
        codes = self.context.get_customer_codes_and_names()
        return codes

    # Schema is here a property since we need to build it dynamically regarding
    # the current request (the same should have been built using the after_bind
    # method ?)
    @property
    def schema(self):
        """
        The getter for our schema property
        """
        if self._schema is None:
            label = self.form_label()
            self._schema = SCHEMA_FACTORIES[label](self.edit)
        return self._schema

    @schema.setter
    def schema(self, value):
        """
        A setter for the schema property
        The BaseClass in pyramid_deform gets and sets the schema attribute that
        is here transformed as a property
        """
        self._schema = value

    def before(self, form):
        populate_actionmenu(self.request, self.context)
        grid = FORM_GRIDS[self.form_label()]
        form.widget = GridFormWidget(named_grid=grid)

    def submit_success(self, appstruct):
        model = self.schema.objectify(appstruct)
        model.company = self.context
        # Au cas où ce n'est pas déjà fait
        model.type = self.form_label()

        self.dbsession.add(model)

        self.dbsession.flush()
        self.session.flash(self.validation_msg)
        return HTTPFound(
            self.request.route_path(
                'customer',
                id=model.id
            )
        )

    def cancel_success(self, appstruct):
        return HTTPFound(
            self.request.route_path('company_customers', id=self.context.id)
        )
    cancel_failure = cancel_success


class CustomerEdit(CustomerAdd):
    """
    Customer edition form
    """
    add_template_vars = ('title', 'customers',)
    validation_msg = "Le client a été modifié avec succès"
    edit = True

    def form_label(self):
        """
        :returns: True if it's a company customer add
        :rtype: bool
        """
        return self.context.type

    def appstruct(self):
        """
        Populate the form with the current edited context (customer)
        """
        return self.schema.dictify(self.request.context)

    @reify
    def title(self):
        return "Modifier le client '{0}' de l'enseigne '{1}'".format(
            self.context.name,
            self.context.company.name
        )

    @property
    def customers(self):
        company = self.context.company
        codes = company.get_customer_codes_and_names()
        codes.filter(Customer.id != self.context.id)
        logger.debug(codes.all())
        return codes

    def submit_success(self, appstruct):
        model = self.schema.objectify(appstruct, self.context)
        model = self.dbsession.merge(model)
        self.dbsession.flush()
        self.session.flash(self.validation_msg)
        return HTTPFound(
            self.request.route_path(
                'customer',
                id=model.id
            )
        )

    def cancel_success(self, appstruct):
        return HTTPFound(
            self.request.route_path(
                'customer',
                id=self.context.id
            )
        )


class CustomerImportStep1(CsvFileUploadView):
    title = "Import des clients, étape 1 : chargement d'un fichier au \
format csv"
    model_types = ("customers",)
    default_model_type = 'customers'

    def get_next_step_route(self, args):
        return self.request.route_path(
            "company_customers_import_step2",
            id=self.context.id,
            _query=args
        )


class CustomerImportStep2(ConfigFieldAssociationView):

    title = "Import de clients, étape 2 : associer les champs"
    model_types = CustomerImportStep1.model_types

    def get_previous_step_route(self):
        return self.request.route_path(
            "company_customers_import_step1",
            id=self.context.id,
        )

    def get_default_values(self):
        logger.info("Asking for default values : %s" % self.context.id)
        return dict(company_id=self.context.id)


class CustomerRestView(BaseRestView):
    """
    Customer rest view

    collection : context Root

        GET : return list of customers (company_id should be provided)
    """
    def get_schema(self, submitted):
        if 'formid' in submitted:
            schema = SCHEMA_FACTORIES[submitted['formid']]()
        else:
            excludes = ('company_id',)
            schema = get_add_edit_customer_schema(excludes=excludes)
        return schema

    def collection_get(self):
        return self.context.customers

    def post_format(self, entry, edit, attributes):
        """
        Associate a newly created element to the parent company
        """
        if not edit:
            entry.company = self.context
        return entry


def includeme(config):
    """
        Add module's views
    """
    for i in range(2):
        index = i + 1
        route_name = 'company_customers_import_step%d' % index
        path = r'/company/{id:\d+}/customers/import/%d' % index
        config.add_route(route_name, path, traverse='/companies/{id}')

    config.add_view(
        CustomerAdd,
        route_name='company_customers',
        renderer='customers/edit.mako',
        request_method='POST',
        permission='add_customer',
    )

    config.add_view(
        CustomerAdd,
        route_name='company_customers',
        renderer='customers/edit.mako',
        request_param='action=add',
        permission='add_customer',
    )

    config.add_view(
        CustomerEdit,
        route_name='customer',
        renderer='customers/edit.mako',
        request_param='action=edit',
        permission='edit_customer',
    )

    config.add_view(
        CustomerView,
        route_name='customer',
        renderer='customers/view.mako',
        request_method='GET',
        permission='view_customer',
    )
    config.add_view(
        customer_delete,
        route_name="customer",
        request_param="action=delete",
        permission='delete_customer',
        request_method="POST",
        require_csrf=True,
    )
    config.add_view(
        customer_archive,
        route_name="customer",
        request_param="action=archive",
        permission='edit_customer',
        request_method="POST",
        require_csrf=True,
    )

    config.add_view(
        CustomerImportStep1,
        route_name="company_customers_import_step1",
        permission="add_customer",
        renderer="base/formpage.mako",
    )

    config.add_view(
        CustomerImportStep2,
        route_name="company_customers_import_step2",
        permission="add_customer",
        renderer="base/formpage.mako",
    )
    config.add_view(
        CustomerAddToProject,
        route_name="customer",
        request_param="action=addcustomer",
        permission='edit_customer',
        renderer="base/formpage.mako",
    )

    config.add_rest_service(
        factory=CustomerRestView,
        route_name="/api/v1/customers/{id}",
        collection_route_name="/api/v1/companies/{id}/customers",
        view_rights="view_customer",
        edit_rights="edit_customer",
        add_rights="add_customer",
        delete_rights="delete_customer",
        collection_view_rights="list_customers",
    )

    config.add_company_menu(
        parent="third_party",
        order=0,
        label="Clients",
        route_name="company_customers",
        route_id_key="company_id",
        routes_prefixes=['customer'],
    )
