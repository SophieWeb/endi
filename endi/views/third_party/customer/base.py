import logging
from deform import Form
from deform_extensions import GridFormWidget
from endi.views import (
    submit_btn,
)
from endi.forms.third_party.customer import (
    get_company_customer_schema,
    get_individual_customer_schema,
    get_internal_customer_schema,
)
from endi.utils.widgets import (
    ButtonLink,
    ViewLink,
)

logger = logging.getLogger(__name__)


COMPANY_FORM_GRID = (
    (('code', 4),),
    (('company_name', 12),),
    (('civilite', 6),),
    (('lastname', 6), ('firstname', 6),),
    (('function', 12),),
    (('address', 12),),
    (('zip_code', 4), ('city', 8),),
    (('country', 6),),
    (('tva_intracomm', 6), ('registration', 6),),
    (('email', 12),),
    (('mobile', 6), ('phone', 6),),
    (('fax', 6),),
    (('comments', 12),),
    (('compte_cg', 6), ('compte_tiers', 6),)
)
INDIVIDUAL_FORM_GRID = (
    (('code', 4),),
    (('civilite', 6),),
    (('lastname', 6), ('firstname', 6),),
    (('address', 12),),
    (('zip_code', 4), ('city', 8),),
    (('country', 6),),
    (('email', 12),),
    (('mobile', 6), ('phone', 6),),
    (('fax', 6),),
    (('comments', 12),),
    (('compte_cg', 6), ('compte_tiers', 6),)
)
INTERNAL_FORM_GRID = (
    (('code', 4),),
    (('company_name', 12),),
    (('source_company_id', 12),),
    (('code', 4),),
    (('civilite', 6),),
    (('lastname', 6), ('firstname', 6),),
    (('address', 12),),
    (('zip_code', 4), ('city', 8),),
    (('country', 6),),
    (('email', 12),),
    (('mobile', 6), ('phone', 6),),
    (('fax', 6),),
    (('comments', 12),),
    (('compte_cg', 6), ('compte_tiers', 6),)
)


SCHEMA_FACTORIES = {
    'company': get_company_customer_schema,
    'individual': get_individual_customer_schema,
    'internal': get_internal_customer_schema,
}
FORM_GRIDS = {
    'company': COMPANY_FORM_GRID,
    'individual': INDIVIDUAL_FORM_GRID,
    'internal': INTERNAL_FORM_GRID,
}


def get_company_customer_form(request, counter=None):
    """
    Returns the customer add/edit form
    :param obj request: Pyramid's request object
    :param obj counter: An iterator for field number generation
    :returns: a deform.Form instance
    """
    schema = get_company_customer_schema()
    schema = schema.bind(request=request)
    form = Form(
        schema,
        buttons=(submit_btn,),
        counter=counter,
        formid='company',
    )
    form.widget = GridFormWidget(named_grid=COMPANY_FORM_GRID)
    return form


def get_individual_customer_form(request, counter=None):
    """
    Return a form for an individual customer
    :param obj request: Pyramid's request object
    :param obj counter: An iterator for field number generation
    :returns: a deform.Form instance
    """
    schema = get_individual_customer_schema()
    schema = schema.bind(request=request)
    form = Form(
        schema,
        buttons=(submit_btn,),
        counter=counter,
        formid='individual'
    )
    form.widget = GridFormWidget(named_grid=INDIVIDUAL_FORM_GRID)
    return form


def get_internal_customer_form(request, counter=None):
    """
    Return a form for an internal customer
    :param obj request: Pyramid's request object
    :param obj counter: An iterator for field number generation
    :returns: a deform.Form instance
    """
    schema = get_internal_customer_schema()
    schema = schema.bind(request=request)
    form = Form(
        schema,
        buttons=(submit_btn, ),
        counter=counter,
        formid='internal'
    )
    form.widget = GridFormWidget(named_grid=INTERNAL_FORM_GRID)
    return form


def populate_actionmenu(request, context=None):
    """
        populate the actionmenu for the different views (list/add/edit ...)
    """
    company_id = request.context.get_company_id()
    request.actionmenu.add(get_list_view_btn(company_id))
    if context is not None and context.__name__ == 'customer':
        request.actionmenu.add(get_view_btn(context.id))


def get_list_view_btn(id_):
    return ButtonLink(
        "Liste des clients",
        path="company_customers",
        id=id_)


def get_view_btn(customer_id):
    return ViewLink(
        "Revenir au client",
        "view_customer",
        path="customer",
        id=customer_id
    )


def get_edit_btn(customer_id):
    return ViewLink(
        "Modifier",
        "edit_customer",
        path="customer",
        id=customer_id,
        _query=dict(action="edit")
    )
