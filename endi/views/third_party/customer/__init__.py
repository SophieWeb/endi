def includeme(config):
    config.include('.routes')
    config.include('.views')
    config.include('.lists')
