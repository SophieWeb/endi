"""
    Customer views
"""
import logging
import colander
import itertools

from collections import OrderedDict
from sqlalchemy import (
    or_,
    not_,
)
from sqlalchemy.orm import undefer_group

from endi.models.third_party.customer import Customer
from endi.utils.widgets import (
    Link,
    POSTButton,
)
from endi.forms.third_party.customer import (
    get_list_schema,
)
from endi.views import (
    BaseListView,
    BaseCsvView,
)
from endi.views.project.routes import (
    COMPANY_PROJECTS_ROUTE,
)
from .base import (
    get_company_customer_form,
    get_individual_customer_form,
    get_internal_customer_form,
)

logger = log = logging.getLogger(__name__)


class CustomersListTools:
    """
    Customer list tools
    """
    title = "Liste des clients"
    schema = get_list_schema()
    sort_columns = {
        'label': Customer.label,
        "code": Customer.code,
        "lastname": Customer.lastname,
        "created_at": Customer.created_at,
    }
    default_sort = "created_at"
    default_direction = "desc"

    def query(self):
        company = self.request.context
        return Customer.query().filter_by(company_id=company.id)

    def filter_archived(self, query, appstruct):
        archived = appstruct.get('archived', False)
        if archived in (False, colander.null):
            query = query.filter_by(archived=False)
        return query

    def filter_type(self, query, appstruct):
        individual = appstruct.get('individual')
        company = appstruct.get('company')
        internal = appstruct.get('internal')
        if not individual:
            query = query.filter(not_(Customer.type == 'individual'))
        if not company:
            query = query.filter(not_(Customer.type == 'company'))
        if not internal:
            query = query.filter(not_(Customer.type == 'internal'))
        return query

    def filter_name_or_contact(self, records, appstruct):
        """
        Filter the records by customer name or contact lastname
        """
        search = appstruct.get('search')
        if search:
            records = records.filter(
                or_(Customer.label.like("%" + search + "%"),
                    Customer.lastname.like("%" + search + "%")))
        return records


class CustomersListView(CustomersListTools, BaseListView):
    """
    Customer listing view
    """
    add_template_vars = (
        'stream_actions',
        'title',
        'forms',
    )

    @property
    def forms(self):
        res = OrderedDict()
        field_counter = itertools.count()
        res['company'] = {
            'title': "Personne morale <small>(entreprise, "
            "administration, association…)</small>",
            'link': "Personne morale",
            'form': get_company_customer_form(self.request, field_counter),
            'selected': 'true'
        }
        res['individual'] = {
            'title': "Personne physique <small>(particulier)</small>",
            'link': 'Personne physique',
            'form': get_individual_customer_form(
                self.request, field_counter
            )
        }
        res['internal'] = {
            'title': "Interne : Enseigne de la CAE",
            'link': 'Enseigne de la CAE',
            'form': get_internal_customer_form(self.request, field_counter)
        }
        return res

    def stream_actions(self, customer):
        """
            Return action buttons with permission handling
        """

        if self.request.has_permission('delete_customer', customer):
            yield POSTButton(
                self.request.route_path(
                    "customer",
                    id=customer.id,
                    _query=dict(action="delete"),
                ),
                "Supprimer",
                title="Supprimer définitivement ce client",
                icon="trash-alt",
                css="negative",
                confirm="Êtes-vous sûr de vouloir supprimer ce client ?",
            )

        yield Link(
            self.request.route_path("customer", id=customer.id),
            "Voir",
            title="Voir/Modifier ce client",
            icon="pen",
        )

        yield Link(
            self.request.route_path(
                COMPANY_PROJECTS_ROUTE,
                id=customer.company.id,
                _query=dict(action="add", customer=customer.id)
            ),
            "Ajouter un dossier",
            title="Ajouter un dossier pour ce client",
            icon="folder-plus",
        )

        if customer.archived:
            label = "Désarchiver"
        else:
            label = "Archiver"
        yield POSTButton(
            self.request.route_path(
                "customer",
                id=customer.id,
                _query=dict(action="archive"),
            ),
            label,
            icon="archive",
        )


class CustomersCsv(CustomersListTools, BaseCsvView):
    """
        Customer csv view
    """
    model = Customer

    @property
    def filename(self):
        return "clients.csv"

    def query(self):
        company = self.request.context
        query = Customer.query().options(undefer_group('edit'))
        return query.filter(Customer.company_id == company.id)


def includeme(config):
    config.add_view(
        CustomersListView,
        route_name='company_customers',
        renderer='customers/list.mako',
        request_method='GET',
        permission='list_customers',
    )

    config.add_view(
        CustomersCsv,
        route_name='customers.csv',
        request_method='GET',
        permission='list_customers',
    )
