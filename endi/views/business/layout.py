import logging

from endi.models.project.business import Business
from endi.utils.menu import (
    MenuItem,
    Menu,
)
from endi.utils.widgets import POSTButton
from endi.default_layouts import DefaultLayout
from endi.views.business.routes import (
    BUSINESS_ITEM_ROUTE,
    BUSINESS_ITEM_OVERVIEW_ROUTE,
    # BUSINESS_ITEM_ESTIMATION_ROUTE,
    BUSINESS_ITEM_INVOICE_ROUTE,
    BUSINESS_ITEM_FILE_ROUTE,
    BUSINESS_ITEM_EXPENSES_ROUTE,
    BUSINESS_ITEM_PY3O_ROUTE,
)
from endi.views.training.routes import (
    BUSINESS_BPF_DATA_FORM_URL,
    BUSINESS_BPF_DATA_LIST_URL,
)


logger = logging.getLogger(__name__)


BusinessMenu = Menu(name="businessmenu")
BusinessMenu.add(
    MenuItem(
        name='overview',
        label="Vue générale",
        route_name=BUSINESS_ITEM_OVERVIEW_ROUTE,
        icon="info-circle",
        perm="view.business",
    )
)
# BusinessMenu.add(
#     MenuItem(
#         name='business_estimations',
#         label="Devis",
#         route_name=BUSINESS_ITEM_ESTIMATION_ROUTE,
#         icon="file-list",
#         perm="list.estimations",
#     )
# )
BusinessMenu.add(
    MenuItem(
        name='business_invoices',
        label="Factures",
        route_name=BUSINESS_ITEM_INVOICE_ROUTE,
        icon="file-invoice-euro",
        perm="list.invoices",
    )
)
BusinessMenu.add(
    MenuItem(
        name='business_py3o',
        label="Génération de documents",
        route_name=BUSINESS_ITEM_PY3O_ROUTE,
        icon="file-alt",
    )
)
BusinessMenu.add(
    MenuItem(
        name='business_files',
        label="Fichiers attachés",
        route_name=BUSINESS_ITEM_FILE_ROUTE,
        icon="paperclip",
    )
)
BusinessMenu.add(
    MenuItem(
        name='expenses',
        label="Achats liés",
        route_name=BUSINESS_ITEM_EXPENSES_ROUTE,
        icon="box",
        perm="view.business",
    )
)
BusinessMenu.add(
    MenuItem(
        name='bpf_data',
        label="Données BPF",
        route_name=BUSINESS_BPF_DATA_LIST_URL,
        other_route_name=BUSINESS_BPF_DATA_FORM_URL,
        perm="edit.bpf",
        icon="chart-pie",
    )
)


class BusinessLayout(DefaultLayout):
    """
    Layout for business related pages

    Provide the main page structure for project view
    """

    def __init__(self, context, request):
        DefaultLayout.__init__(self, context, request)

        if isinstance(context, Business):
            self.current_business_object = context
        elif hasattr(context, "business"):
            self.current_business_object = context.business
        else:
            raise Exception(
                "Can't retrieve the current business used in the "
                "business layout, context is : %s" % context
            )

    @property
    def edit_url(self):
        return self.request.route_path(
            BUSINESS_ITEM_ROUTE,
            id=self.current_business_object.id,
            _query={'action': 'edit'}
        )

    @property
    def close_button(self):
        if self.request.has_permission(
                'close.business', self.current_business_object
        ):
            return POSTButton(
                self.request.route_path(
                    BUSINESS_ITEM_ROUTE,
                    id=self.current_business_object.id,
                    _query={'action': 'close'}
                ),
                label="Clôturer cette affaire",
                icon="lock",
                css="btn icon",
                confirm="Êtes-vous sûr de vouloir clôturer cette affaire ?"
            )

    @property
    def businessmenu(self):
        BusinessMenu.set_current(self.current_business_object)
        return BusinessMenu


def includeme(config):
    config.add_layout(
        BusinessLayout,
        template="endi:templates/business/layout.mako",
        name='business',
    )
