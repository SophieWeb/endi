"""
Views related to payments edition
"""
import logging

from pyramid.httpexceptions import HTTPFound
from deform_extensions import GridFormWidget

from endi.interfaces import (
    IPaymentRecordService,
)
from endi.utils.widgets import ViewLink
from endi.utils.strings import format_amount
from endi.events.status_changed import StatusChanged
from endi.models.task import BankRemittance
from endi.models.services.user import UserPrefsService
from endi.forms.tasks.payment import (
    PaymentSchema,
    get_payment_schema,
    get_form_grid_from_request,
)
from endi.forms import merge_session_with_post
from endi.forms.supply.supplier_invoice import (
    get_supplier_invoice_payment_schema,
)
from endi.forms.expense import (
    ExpensePaymentSchema,
)
from endi.export.task_pdf import ensure_task_pdf_persisted
from endi.views import (
    BaseView,
    BaseFormView,
    submit_btn,
    cancel_btn,
    TreeMixin,
)
from endi.views.task.utils import get_task_url
from endi.views.invoices.invoice import InvoiceHtmlView

logger = logging.getLogger(__name__)


def populate_invoice_payment_actionmenu(context, request, is_edit):
    """
    Set the menu items in the request context
    """
    link = ViewLink(
        "Voir la facture",
        path="/invoices/{id}.html",
        id=context.parent.id,
    )
    request.actionmenu.add(link)
    if is_edit:
        link = ViewLink(
            "Retour au paiement",
            path="payment",
            id=context.id,
        )
        request.actionmenu.add(link)


class PaymentView(BaseView, TreeMixin):
    """
    Simple payment view
    """
    @property
    def title(self):
        return "Paiement pour la facture {0}".format(
                self.context.task.official_number,
            ),

    def __call__(self):
        edit_url = self.request.route_path(
            "payment", id=self.context.id, _query=dict(action="edit")
        )
        parent_url = get_task_url(
            self.request,
            self.context.task,
            suffix='.html'
        )
        del_url = self.request.route_path(
            "payment",
            id=self.context.id,
            _query=dict(action="delete", come_from=parent_url)
        )
        return dict(
            edit_url=edit_url,
            del_url=del_url,
            money_flow_type='encaissement',
        )


class PaymentRemittanceMixin:
    # allows easy override by SAP plugin
    @staticmethod
    def schema_factory(*args, **kwargs):
        return get_payment_schema(*args, **kwargs)

    def get_remittance_error(self, error_type, remittance_id, p1="", p2=""):
        if error_type == "remittance_closed":
            remittance_route = self.request.route_path(
                "/accounting/bank_remittances/{id}",
                id=remittance_id
            )
            return "<strong>La remise en banque {0} est déjà clôturée.</strong>\
            <br/><br/>Vous pouvez modifier le numéro de la remise en \
            banque ou rouvrir la remise depuis sa fiche : <a href='#' \
            onclick=\"window.openPopup('{1}');\">Remise en banque {0}</a>\
            ".format(remittance_id, remittance_route)
        if error_type == "payment_mode":
            return "<strong>La remise en banque {0} ne correspond pas à cet \
            encaissement.</strong><br/><br/>La remise en banque <strong>{0}\
            </strong> existe déjà et est configurée pour le mode de paiement \
            '<strong>{1}</strong>' ; vous ne pouvez pas y adjoindre un \
            encaissement '<strong>{2}</strong>'.<br/><br/>Vous pouvez \
            modifier le numéro de la remise en banque ou corriger le mode \
            de paiement.".format(remittance_id, p1, p2)
        if error_type == "payment_bank":
            return "<strong>La remise en banque {0} ne correspond pas à cet \
            encaissement.</strong><br/><br/>La remise en banque <strong>{0}\
            </strong> existe déjà et est configurée pour le compte bancaire \
            '<strong>{1}</strong>' ; vous ne pouvez pas y adjoindre un \
            paiement sur un autre compte.<br/><br/>Vous pouvez modifier \
            le numéro de la remise en banque ou corriger le compte \
            bancaire.".format(remittance_id, p1)
        if error_type == "remittance_unknown":
            return "<strong>La remise en banque {} n'existe pas.</strong>\
            <br/><br/>Vous pouvez confirmer la création de cette remise en \
            cliquant sur le bouton ci-dessous ou modifier le numéro de la \
            remise en banque.<br/><br/><button class='btn btn-primary' \
            onclick=\"document.getElementsByName('new_remittance_confirm')[0]\
            .value='true'; document.getElementById('deform').submit.click();\
            \"> Confirmer la création de cette remise et enregistrer \
            l'encaissement</button>".format(remittance_id)
        if error_type == "old_remittance_closed":
            remittance_route = self.request.route_path(
                "/accounting/bank_remittances/{id}",
                id=remittance_id
            )
            return "<strong>La remise en banque {0} a été clôturée.</strong>\
            <br/><br/>Il n'est pas possible de modifier le numéro de \
            remise en banque de cet encaissement car la remise a été clôturée.\
            <br/><br/>Vous pouvez remettre l'ancien numéro <strong>{0}\
            </strong> ou rouvrir la remise depuis sa fiche : <a href='#' \
            onclick=\"window.openPopup('{1}');\">Remise en banque {0}</a>\
            ".format(remittance_id, remittance_route)
        return "Erreur inconnue !"

    def get_new_remittance_confirm_form(self, appstruct):
        """
        Get the payment schema form with a checkbox for
        remittance creation confirmation
        """
        confirm_schema = self.schema_factory(
            self.request,
            with_new_remittance_confirm=True
        ).bind(request=self.request)
        form = self.form_class(confirm_schema, buttons=self.buttons)
        form.set_appstruct(appstruct)
        grid = get_form_grid_from_request(self.request)
        form.widget = GridFormWidget(named_grid=grid)
        return form.render()

    def check_payment_remittance(self, appstruct, old_remittance_id=None):
        """
        Check the bank remittance id before saving the payment
        Fills the error_msg attribute if an error was found

        :returns: A dict with a confirmation form or None
        :rtype: None or dict
        """
        result = None

        remittance_id = appstruct['bank_remittance_id']
        if remittance_id.__contains__("/"):
            # Remplace les '/' par des '_' et prévient l'utilisateur
            remittance_id = remittance_id.replace("/", "_")
            appstruct['bank_remittance_id'] = remittance_id
            self.warn_message = "Le symbole <strong>/</strong> n'est pas \
            autorisé dans les numéros de remise en banque. Ce dernier a \
            été automatiquement modifié en : <strong>{}</strong>\
            ".format(remittance_id)

        if old_remittance_id:
            br = BankRemittance.query().filter_by(id=old_remittance_id).first()
            if br.closed:
                # Erreur : ancienne remise fermée
                self.error_msg = self.get_remittance_error(
                    "old_remittance_closed",
                    old_remittance_id
                )

        br = BankRemittance.query().filter_by(id=remittance_id).first()
        if br:
            if br.payment_mode == appstruct['mode'] \
                and br.bank_id == appstruct['bank_id']:  # noqa
                if br.closed:
                    # Erreur : remise déjà fermée
                    self.error_msg = self.get_remittance_error(
                        "remittance_closed",
                        remittance_id
                    )
            else:
                # Erreur : mode de paiement ou banque ne correspondent pas
                if br.payment_mode != appstruct['mode']:
                    self.error_msg = self.get_remittance_error(
                        "payment_mode",
                        remittance_id,
                        br.payment_mode,
                        appstruct['mode'],
                    )
                if br.bank_id != appstruct['bank_id']:
                    if br.bank:
                        bank_label = br.bank.label
                    else:
                        bank_label = "-NON DEFINI-"
                    self.error_msg = self.get_remittance_error(
                        "payment_bank",
                        remittance_id,
                        bank_label,
                    )
        else:
            if 'new_remittance_confirm' in self.request.POST \
                and self.request.POST['new_remittance_confirm'] == "true":  # noqa
                # Création de la remise
                remittance = BankRemittance(
                    id=remittance_id,
                    payment_mode=appstruct['mode'],
                    bank_id=appstruct['bank_id'],
                    closed=0,
                )
                self.request.dbsession.merge(remittance)
            else:
                # Erreur : remise inexistante
                self.error_msg = self.get_remittance_error(
                    "remittance_unknown",
                    remittance_id
                )
                result = {
                    'form': self.get_new_remittance_confirm_form(appstruct)
                }

        return result


class InvoicePaymentView(BaseFormView, PaymentRemittanceMixin):
    buttons = (submit_btn, cancel_btn)
    add_template_vars = ('help_message', 'error_msg', 'warn_message')
    error_msg = None
    warn_message = None

    @property
    def help_message(self):
        return (
            "Enregistrer un paiement pour la facture {0} dont le montant "
            "ttc restant à payer est de {1} €".format(
                self.context.official_number,
                format_amount(self.context.topay(), precision=5)
            )
        )

    @property
    def schema(self):
        return self.schema_factory(self.request).bind(request=self.request)

    @schema.setter
    def schema(self, value):
        """
        A setter for the schema property
        The BaseClass in pyramid_deform gets and sets the schema attribute that
        is here transformed as a property
        """
        self._schema = value

    @property
    def title(self):
        return (
            "Enregistrer un encaissement pour la facture "
            "{0.official_number}".format(self.context)
        )

    def before(self, form):
        BaseFormView.before(self, form)
        self.request.actionmenu.add(
            ViewLink(
                label="Revenir à la facture",
                path="/invoices/{id}.html",
                id=self.context.id,
                _anchor="payment",
            )
        )
        appstruct = {'amount': self.context.topay()}
        form.set_appstruct(appstruct)
        grid = get_form_grid_from_request(self.request)
        form.widget = GridFormWidget(named_grid=grid)

    def redirect(self):
        return HTTPFound(
            self.request.route_path(
                '/invoices/{id}.html',
                id=self.context.id,
                _anchor='payment',
            )
        )

    def notify(self):
        self.request.registry.notify(
            StatusChanged(
                self.request,
                self.context,
                self.context.paid_status,
            )
        )

    def submit_success(self, appstruct):
        """
        Launched when the form was successfully validated
        """
        logger.debug("Submitting a new Payment")
        # Vérification du numéro de remise
        if 'bank_remittance_id' not in appstruct:
            remittance_id = ""
        else:
            remittance_check_return = self.check_payment_remittance(appstruct)
            remittance_id = appstruct['bank_remittance_id']
            if remittance_check_return is not None:
                logger.debug("  + Returning a confirmation form")
                return remittance_check_return

        # Si on a pas d'erreur on continue le traitement
        if self.error_msg is None:
            logger.debug("  + There was no error checking payment remittance")
            # On s'assure qu'on a bien un pdf dans le cache
            ensure_task_pdf_persisted(self.context, self.request)
            # Enregistrement de l'encaissement
            payment_service = self.request.find_service(IPaymentRecordService)
            force_resulted = appstruct.pop('resulted', False)

            # Computing the resulting payment depending on what has already
            # been paid and the different TVA rates
            submitted_amount = appstruct['amount']
            payments = self.context.compute_payments(submitted_amount)

            for payment in payments:
                # Construire un nouvel appstruct correspondant
                tva_payment = {}
                # BaseTaskPayment fields
                tva_payment['date'] = appstruct['date']
                tva_payment['amount'] = payment['amount']
                tva_payment['tva_id'] = payment['tva_id']

                # Payment fields
                if 'mode' in appstruct:
                    tva_payment['mode'] = appstruct['mode']
                if 'bank_id' in appstruct:
                    tva_payment['bank_id'] = appstruct.get('bank_id')
                if 'bank_remittance_id' in appstruct:
                    tva_payment['bank_remittance_id'] = remittance_id
                if 'check_number' in appstruct:
                    tva_payment['check_number'] = appstruct['check_number']
                if 'customer_bank_id' in appstruct:
                    tva_payment['customer_bank_id'] =\
                        appstruct['customer_bank_id']

                # Record the payment
                payment_service.add(self.context, tva_payment)

            self.context.check_resulted(force_resulted=force_resulted)
            self.context.historize_paid_status(self.request.user)
            self.request.dbsession.merge(self.context)
            # Mémorisation du dernier numéro de remise utilisé
            UserPrefsService.set(
                self.request,
                'last_bank_remittance_id',
                remittance_id
            )
            # Notification et redirection
            self.notify()
            return self.redirect()
        else:
            logger.debug("  - There are error messages to display")

    def cancel_success(self, appstruct):
        return self.redirect()

    cancel_failure = cancel_success


class BasePaymentEditView(BaseFormView):
    """
    Edit payment view
    """
    title = "Modification d'un paiement"

    def populate_actionmenu(self):
        return populate_invoice_payment_actionmenu(
            self.context, self.request, True
        )

    def before(self, form):
        form.set_appstruct(self.context.appstruct())
        self.populate_actionmenu()
        return form

    def get_default_redirect(self):
        """
        Get the default redirection path
        """
        return self.request.route_path(
            "payment",
            id=self.context.id
        )

    def edit_payment(self, appstruct):
        """
        Edit the payment object, handles form datas persistence
        """
        payment_service = self.request.find_service(IPaymentRecordService)
        # update the payment
        payment = payment_service.update(self.context, appstruct)
        return payment

    def submit_success(self, appstruct):
        """
        handle successfull submission of the form
        """
        force_resulted = appstruct.pop('resulted', False)
        payment = self.edit_payment(appstruct)
        # Check the invoice status
        parent = payment.parent
        parent = parent.check_resulted(
            force_resulted=force_resulted,
        )
        self.dbsession.merge(parent)
        if hasattr(self, 'after_parent_save'):
            self.after_parent_save(parent)
        come_from = appstruct.pop('come_from', None)
        if come_from is not None:
            redirect = come_from
        else:
            redirect = self.get_default_redirect()
        return HTTPFound(redirect)


class InvoicePaymentEditView(BasePaymentEditView, PaymentRemittanceMixin):
    title = "Modification d'un encaissement"
    add_template_vars = ('help_message', 'error_msg', 'warn_message')
    error_msg = None
    warn_message = None

    @property
    def schema(self):
        return self.schema_factory(self.request).bind(request=self.request)

    @schema.setter
    def schema(self, value):
        """
        A setter for the schema property
        The BaseClass in pyramid_deform gets and sets the schema attribute that
        is here transformed as a property
        """
        self._schema = value

    @property
    def help_message(self):
        return (
            "Modifier le paiement pour la facture {0} d'un montant \
            de {1} €".format(
                self.context.invoice.official_number,
                format_amount(self.context.amount, precision=5)
            )
        )

    def before(self, form):
        form.set_appstruct(self.context.appstruct())
        self.populate_actionmenu()
        grid = get_form_grid_from_request(self.request)
        form.widget = GridFormWidget(named_grid=grid)
        return form

    def after_parent_save(self, parent):
        # After Invoice has been saved, historize its status
        parent.historize_paid_status(self.request.user)

    def submit_success(self, appstruct):
        """
        handle successfull submission of the form
        """
        logger.debug("Submitting a Payment edition")
        # Vérification du numéro de remise
        if 'bank_remittance_id' in appstruct and \
        self.context.bank_remittance_id != appstruct["bank_remittance_id"] : # noqa
            remittance_check_return = self.check_payment_remittance(
                appstruct,
                self.context.bank_remittance_id
            )
            if remittance_check_return is not None:
                logger.debug("  + Returning a confirmation form")
                return remittance_check_return
        # Si on a pas d'erreur on continue le traitement
        if self.error_msg is None:
            logger.debug("  + There was no error checking payment remittance")
            force_resulted = appstruct.pop('resulted', False)
            payment = self.edit_payment(appstruct)
            # Check the invoice status
            parent = payment.parent
            parent = parent.check_resulted(
                force_resulted=force_resulted,
            )
            self.dbsession.merge(parent)
            if hasattr(self, 'after_parent_save'):
                self.after_parent_save(parent)
            come_from = appstruct.pop('come_from', None)
            if come_from is not None:
                redirect = come_from
            else:
                redirect = self.get_default_redirect()
            return HTTPFound(redirect)
        else:
            logger.debug("  - There are error messages to display")


class BasePaymentDeleteView(BaseView):
    def on_after_delete(self):
        """
        Called once the payment has been deleted (optional)
        """
        pass

    def delete_payment(self):
        """
        Delete the payment instance from the database
        """
        raise NotImplementedError

    def parent_url(self, parent_id):
        """
        Parent url to use if a come_from parameter is missing

        :param int parent_id: The id of the parent object
        :returns: The url to redirect to
        :rtype: str
        """
        raise NotImplementedError

    def __call__(self):
        logger.debug("Deleting a payment")
        parent = self.context.parent
        self.delete_payment()
        parent = parent.check_resulted()
        self.on_after_delete()
        self.dbsession.merge(parent)
        self.session.flash("Le paiement a bien été supprimé")

        if 'come_from' in self.request.GET:
            redirect = self.request.GET['come_from']
        else:
            redirect = self.parent_url(parent.id)
        return HTTPFound(redirect)


class InvoicePaymentDeleteView(BasePaymentDeleteView):
    def on_after_delete(self):
        self.context.parent.historize_paid_status(self.request.user)

    def delete_payment(self):
        """
        Delete the payment instance from the database
        """
        # On fait appel au pyramid_service définit pour l'interface
        # IPaymentRecordService (voir pyramid_services)
        # Il est possible de changer de service en en spécifiant un autre dans
        # le fichier .ini de l'application
        payment_service = self.request.find_service(IPaymentRecordService)
        payment_service.delete(self.context)

    def parent_url(self, parent_id):
        """
        Parent url to use if a come_from parameter is missing

        :param int parent_id: The id of the parent object
        :returns: The url to redirect to
        :rtype: str
        """
        return self.request.route_path(
            "/invoices/{id}.html", id=parent_id, _anchor="#payment"
        )


def populate_expense_payment_actionmenu(context, request):
    link = ViewLink(
        "Voir la note de dépenses",
        path="/expenses/{id}",
        id=context.parent.id,
    )
    request.actionmenu.add(link)


def expense_payment_view(context, request):
    """
    Simple expense payment view
    """
    populate_expense_payment_actionmenu(context, request)

    edit_url = request.route_path(
        "expense_payment",
        id=request.context.id,
        _query=dict(action="edit"),
    )
    parent_url = request.route_path(
        "/expenses/{id}",
        id=request.context.parent.id,
    )
    del_url = request.route_path(
        "expense_payment",
        id=request.context.id,
        _query=dict(action="delete", come_from=parent_url),
    )

    return dict(
        title="Paiement pour la note de dépenses {0}".format(
            context.parent.id
        ),
        edit_url=edit_url,
        del_url=del_url,
        money_flow_type='décaissement',
    )


class ExpensePaymentEdit(BasePaymentEditView):
    schema = ExpensePaymentSchema()

    def populate_actionmenu(self):
        return populate_expense_payment_actionmenu(
            self.context,
            self.request,
        )

    def get_default_redirect(self):
        """
        Get the default redirection path
        """
        return self.request.route_path(
            "expense_payment",
            id=self.context.id
        )

    def edit_payment(self, appstruct):
        payment_obj = self.context
        # update the payment
        merge_session_with_post(payment_obj, appstruct)
        self.dbsession.merge(payment_obj)
        return payment_obj


class ExpensePaymentDeleteView(BasePaymentDeleteView):
    def delete_payment(self):
        self.dbsession.delete(self.context)

    def parent_url(self, parent_id):
        return self.request.route_path("expensesheet", id=parent_id)


def populate_supplier_payment_actionmenu(context, request):
    link = ViewLink(
        "Voir la facture fournisseur",
        path="/suppliers_invoices/{id}",
        id=context.parent.id,
    )
    request.actionmenu.add(link)


def supplier_payment_view(context, request):
    populate_supplier_payment_actionmenu(context, request)

    edit_url = request.route_path(
        "supplier_payment",
        id=request.context.id,
        _query=dict(action="edit"),
    )
    parent_url = request.route_path(
        "/suppliers_invoices/{id}",
        id=request.context.parent.id,
    )
    del_url = request.route_path(
        "supplier_payment",
        id=request.context.id,
        _query=dict(action="delete", come_from=parent_url),
    )
    return dict(
        title="Paiement pour la facture fournisseur {0}".format(
            context.parent.id
        ),
        edit_url=edit_url,
        del_url=del_url,
        money_flow_type="décaissement",
    )


class SupplierPaymentEdit(BasePaymentEditView):
    @property
    def schema(self):
        return self.schema_factory(self.request).bind(request=self.request)

    @schema.setter
    def schema(self, value):
        """
        A setter for the schema property
        The BaseClass in pyramid_deform gets and sets the schema attribute that
        is here transformed as a property
        """
        self._schema = value

    def schema_factory(self, request):
        return get_supplier_invoice_payment_schema(request)

    def populate_actionmenu(self):
        return populate_supplier_payment_actionmenu(
            self.context,
            self.request,
        )

    def get_default_redirect(self):
        return self.request.route_path(
            "supplier_payment",
            id=self.context.id
        )

    def edit_payment(self, appstruct):
        invoice = self.context.supplier_invoice
        payment_obj = self.context
        # update the payment
        merge_session_with_post(payment_obj, appstruct)
        self.dbsession.merge(payment_obj)
        invoice.check_resulted()
        self.dbsession.merge(invoice)
        return payment_obj


class SupplierPaymentDeleteView(BasePaymentDeleteView):
    def delete_payment(self):
        invoice = self.context.supplier_invoice
        self.dbsession.delete(self.context)
        invoice.check_resulted()
        self.dbsession.merge(invoice)

    def parent_url(self, parent_id):
        return self.request.route_path("/suppliers_orders/{id}", id=parent_id)


def add_routes(config):
    """
    Add module's related routes
    """
    config.add_route(
        '/invoices/{id}/addpayment',
        r'/invoices/{id:\d+}/addpayment',
        traverse='/tasks/{id}'
    )
    config.add_route(
        "payment",
        r"/payments/{id:\d+}",
        traverse="/base_task_payments/{id}",
    )
    config.add_route(
        "expense_payment",
        r"/expense_payments/{id:\d+}",
        traverse="/expense_payments/{id}",
    )

    config.add_route(
        "supplier_payment",
        r"/supplier_payments/{id:\d+}",
        traverse="/supplier_payments/{id}",
    )


def includeme(config):
    add_routes(config)
    config.add_tree_view(
        PaymentView,
        parent=InvoiceHtmlView,
        route_name="payment",
        permission="view.payment",
        renderer="/payment.mako",
    )
    config.add_view(
        InvoicePaymentView,
        route_name="/invoices/{id}/addpayment",
        permission="add_payment.invoice",
        renderer='base/formpage.mako',
    )
    config.add_view(
        InvoicePaymentEditView,
        route_name="payment",
        permission="edit.payment",
        request_param='action=edit',
        renderer="/base/formpage.mako",
    )
    config.add_view(
        InvoicePaymentDeleteView,
        route_name="payment",
        permission="delete.payment",
        request_param="action=delete",
    )

    config.add_view(
        expense_payment_view,
        route_name="expense_payment",
        permission="view.payment",
        renderer="/payment.mako",
    )
    config.add_view(
        ExpensePaymentEdit,
        route_name="expense_payment",
        permission="edit.payment",
        request_param='action=edit',
        renderer="/base/formpage.mako",
    )
    config.add_view(
        ExpensePaymentDeleteView,
        route_name="expense_payment",
        permission="delete.payment",
        request_param="action=delete",
    )

    config.add_view(
        supplier_payment_view,
        route_name="supplier_payment",
        permission="view.payment",
        renderer="/payment.mako",
    )
    config.add_view(
        SupplierPaymentEdit,
        route_name="supplier_payment",
        permission="edit.payment",
        request_param='action=edit',
        renderer="/base/formpage.mako",
    )
    config.add_view(
        SupplierPaymentDeleteView,
        route_name="supplier_payment",
        permission="delete.payment",
        request_param="action=delete",
    )
