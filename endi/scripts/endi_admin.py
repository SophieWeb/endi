"""
    Command to add an admin to enDI
"""
import logging
import os
import base64
from sqlalchemy.orm.exc import NoResultFound
from endi.scripts.utils import (
    command,
    get_value,
)
from endi_base.models.base import DBSESSION
from endi.forms.user.user import User
from endi.forms.user.login import Login


PWD_LENGTH = 10


def get_pwd():
    """
        Return a random password
    """
    return base64.b64encode(os.urandom(PWD_LENGTH))

def user_add_command(arguments, env):
    """
        Add a user in the database
    """
    logger = logging.getLogger(__name__)

    login = get_value(arguments, 'user', 'admin')
    logger.debug("Adding a user {0}".format(login))

    password = get_value(arguments, 'pwd', get_pwd())

    login = Login(login=login)
    login.set_password(password)

    group = get_value(arguments, 'group', None)
    if group:
        try:
            login.groups.append(group)
        except NoResultFound:
            print((
                """

ERROR : group %s doesn't exist, did you launched the syncdb command :

    endi-admin <fichier.ini> syncdb
                """ % (
                    group,
                )
            ))
            return

    db = DBSESSION()
    db.add(login)
    db.flush()

    firstname = get_value(arguments, 'firstname', 'Admin')
    lastname = get_value(arguments, 'lastname', 'enDI')
    email = get_value(arguments, 'email', 'admin@example.com')
    user = User(
        login=login,
        firstname=firstname,
        lastname=lastname,
        email=email
    )
    db.add(user)
    db.flush()
    print(("""
    User Account created :
          ID        : {0.id}
          Login     : {0.login.login}
          Firstname : {0.firstname}
          Lastname  : {0.lastname}
          Email     : {0.email}
          Groups    : {0.login.groups}
          """.format(user)))

    if 'pwd' not in arguments:
        print(("""
          Password  : {0}""".format(password)
              ))

    logger.debug("-> Done")
    return user


def test_mail_command(arguments, env):
    """
    Test tool for mail sending
    """
    from endi_base.mail import send_mail
    dest = get_value(arguments, 'to', 'contact@endi.coop')
    request = env['request']
    subject = "Test d'envoi de mail"
    body = """Il semble que le test d'envoi de mail a réussi.
    Ce test a été réalisé depuis le script endi-admin

Bonne et belle journée !!!"""
    send_mail(
        request,
        [dest],
        body,
        subject
    )


def syncdb_command(arguments, env):
    """
    Populate the database
    """
    from endi.models.populate import populate_database
    logger = logging.getLogger(__name__)

    populate_database()
    pkg = get_value(arguments, 'pkg', 'endi')
    from endi.scripts.endi_migrate import (
        fetch_head_command,
        is_alembic_initialized,
    )
    # Do not fetch current head on an existing instance (risk of skipping migrations):
    if not is_alembic_initialized(pkg):
        logger.info('Fetching current alembic head, skipping all migrations (new installation)')
        fetch_head_command(pkg)


def resize_headers_command(arguments, env):
    """
    Resize headers to limit their size

    :param dict arguments: The arguments parsed from the command-line
    :param obj env: The pyramid environment
    """
    from sqlalchemy import distinct
    from endi.models.files import File
    from endi.models.company import Company
    from endi.forms.company import HEADER_RESIZER

    limit = get_value(arguments, 'limit', None)
    offset = get_value(arguments, "offset", None)

    session = DBSESSION()
    file_id_query = session.query(distinct(Company.header_id))
    if limit:
        file_id_query = file_id_query.limit(limit)
        if offset:
            file_id_query = file_id_query.offset(offset)

    header_ids = [i[0] for i in file_id_query]
    header_files = File.query().filter(File.id.in_(header_ids))
    for header_file in header_files:
        file_datas = header_file.data_obj
        if file_datas:
            print((
                "Resizing header with id : {}".format(
                    header_file.id
                )
            ))
            header_file.data = HEADER_RESIZER.complete(file_datas)
            session.merge(header_file)


def admin_entry_point():
    """enDI administration tool
    Usage:
        endi-admin <config_uri> useradd [--user=<user>] [--pwd=<password>] [--firstname=<firstname>] [--lastname=<lastname>] [--email=<email>] [--group=<group>]
        endi-admin <config_uri> testmail [--to=<mailadress>]
        endi-admin <config_uri> syncdb  [--pkg=<pkg>]
        endi-admin <config_uri> resize_headers [--limit=<limit>] [--offset=<offset>]

    o useradd : Add a user in the database
    o testmail : Send a test mail to the given address
    o syncdb : Populate the database with the initial datas
    o resize_headers : bulk resize company header files to limit pdf size

    Options:

        -h --help     Show this screen.
    """
    def callback(arguments, env):
        if arguments['useradd']:
            func = user_add_command
        elif arguments['testmail']:
            func = test_mail_command
        elif arguments['syncdb']:
            func = syncdb_command
        elif arguments['resize_headers']:
            func = resize_headers_command
        return func(arguments, env)
    try:
        return command(callback, admin_entry_point.__doc__)
    finally:
        pass
