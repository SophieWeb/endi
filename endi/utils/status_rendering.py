"""
Document statuses presentation

Centralize in a place (here) css classes (which handle colors) and icons to use
for document statuses.
"""

# Devis
SIGNED_STATUS_ICON = dict(
    (
        ('waiting', 'clock'),
        ('sent', 'envelope'),
        ('aborted', 'times'),
        ('signed', 'check'),
    )
)

# ExpenseSheet / SupplierInvoice
JUSTIFIED_STATUS_ICON = dict(
    (
        ('waiting', 'clock'),
        ('justified', 'check'),

    )
)


# Catch-all : couvre autant de cas que possible
STATUS_ICON = dict(
    (
        ('draft', 'pen'),
        ('wait', 'clock'),
        ('valid', 'check-circle'),
        ('invalid', 'times-circle'),

        ('aborted', 'times'),
        ('geninv', 'euro-sign'),
        ('invoiced', 'euro-sign'),
        ('justified', 'file-check'),
        ('paid', 'euro-sign'),
        ('resulted', 'euro-sign'),
        ('sent', 'envelope'),
        ('signed', 'check'),
        ('waiting', 'euro-slash'),
    )
)
SUPPLIER_ORDER_STATUS_ICON = STATUS_ICON
ESTIMATION_STATUS_ICON = STATUS_ICON
INVOICE_STATUS_ICON = STATUS_ICON
EXPENSE_STATUS_ICON = STATUS_ICON

# Catch-all : couvre autant de cas que possible
STATUS_CSS_CLASS = dict(
    (
        ('draft', 'draft'),
        ('wait', 'caution'),
        ('valid', 'valid'),
        ('invalid', 'invalid'),

        ('waiting', 'valid'),
        ('sent', 'valid'),
        ('signed', 'valid'),
        ('aborted', 'closed'),


    )
)

JUSTIFIED_STATUS_CSS_CLASS = dict(
    (
        ('justified', 'valid'),
        ('waiting', 'caution'),
    )
)

EXPENSE_STATUS_CSS_CLASS = dict(
    (
        ('resulted', 'valid'),
        ('paid', 'partial_unpaid'),
    )
)

