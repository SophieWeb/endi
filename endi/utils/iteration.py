import itertools
from typing import (
    Any,
    Callable, Iterable,
    Tuple,
)


def groupby(elements: Iterable, attrname: str) -> Tuple[Any, Iterable]:
    """
    Specialized version of itertools.groupby grouping on attribute

    Note that for this to work, elements should be already ordered to allow
    groupping ; see itertools.groupby doc.
    """
    def f(x):
        try:
            return getattr(x, attrname)
        except AttributeError:
            raise ValueError(
                f'Attempted to groupby {x} on inexistant attr {attrname}'
            )

    return itertools.groupby(elements, f)
