import colander

from sqlalchemy import inspect


def patched_objectify(self, dict_, context=None):
    # Workaround ColanderAlchemy bug #101 (FlushError)
    # https://github.com/stefanofontanelli/ColanderAlchemy/issues/101
    # A PR is ongoing, if merged/released, that function/file should be removed
    # https://github.com/stefanofontanelli/ColanderAlchemy/pull/103
    def get_context(obj, session, class_, pk):
        '''return context of obj in session'''
        if isinstance(pk, tuple):
            ident = tuple(obj.get(v,None) for v in pk)
        else:
            ident = obj.get(pk,None)
        context = session.query(class_).get(ident) if ident else None

        return context

    mapper = self.inspector
    context =  context if context else mapper.class_()
    insp = inspect(context, raiseerr=False)
    session = insp.session if (insp and insp.session) else None

    for attr in dict_:
        if attr in mapper.relationships.keys():
            # handle relationship
            prop = mapper.get_property(attr)
            prop_class = prop.mapper.class_
            prop_pk = tuple(v.key for v in inspect(prop_class).primary_key)
            if len(prop_pk) == 1:
                prop_pk = prop_pk[0]

            if prop.uselist:
                # relationship is x_to_many, value is list
                subschema = self[attr].children[0]
                value = [patched_objectify(
                            subschema,
                            obj,
                            get_context(obj,
                                        session,
                                        prop_class,
                                        prop_pk
                                        )
                            ) for obj in dict_[attr]]
            else:
                # relationship is x_to_one, value is not a list

                subschema = self[attr]
                obj = dict_[attr]
                value = objectify(
                    subschema,
                    obj,
                    get_context(obj,
                                session,
                                prop_class,
                                prop_pk)
                    )

        elif attr in mapper.columns.keys():
            # handle column
            value = dict_[attr]
            if value is colander.null:
                value = None

        else:
            # Ignore attributes if they are not mapped
            log.debug(
                'SQLAlchemySchemaNode.objectify: %s not found on '
                '%s. This property has been ignored.',
                attr, self
            )
            continue

        # persist value
        setattr(context, attr, value)
    return context
