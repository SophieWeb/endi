"""
    avatar related utilities
"""
import logging

from pyramid.httpexceptions import HTTPFound
from pyramid.security import forget
from sqlalchemy.orm import (
    load_only,
    contains_eager,
    selectinload,
)

from endi.models.user.user import User
from endi.models.user.login import Login


logger = logging.getLogger(__name__)


def get_avatar(request):
    """
        Returns the current User object
    """
    logger.info("# Retrieving avatar #")
    login = request.unauthenticated_userid
    logger.info("  + Login : %s" % login)
    result = None
    if login is not None:
        logger.info("  + Returning the user")
        query = request.dbsession.query(User)
        query = query.join(Login)
        query = query.options(load_only("firstname", "lastname"))
        query = query.options(
            contains_eager(User.login).load_only('login').selectinload(
                Login._groups).load_only('name'),
            selectinload(User.companies).load_only('id', 'active'),
        )
        query = query.filter(Login.login == login)
        result = query.first()
        if result is None:
            forget(request)
            raise HTTPFound("/")
    else:
        logger.info("  + No user found")
    logger.debug("-> End of the avatar collection")
    return result
