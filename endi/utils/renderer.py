"""
    MultiRenderer tools to allow multiple renderers to be used with deform
"""
import json
import logging
import deform

from endi_base.utils.renderers import (
    configure_export,
    set_json_renderer,
)
from pkg_resources import resource_filename

from deform.template import ZPTRendererFactory
from pyramid.threadlocal import get_current_request


logger = logging.getLogger(__name__)


class CustomRenderer(ZPTRendererFactory):
    """
    Custom renderer needed to ensure our buttons (see utils/widgets.py) can be
    added in the form actions list
    It adds the current request object to the rendering context
    """
    def __call__(self, template_name, **kw):
        if "request" not in kw:
            kw['request'] = get_current_request()
        return ZPTRendererFactory.__call__(self, template_name, **kw)


def get_search_path():
    """
    Add enDI's deform custom templates to the loader
    """
    path = resource_filename('endi', 'templates/deform')
    default_paths = deform.form.Form.default_renderer.loader.search_path
    if path not in default_paths:
        result = (path,) + default_paths
    else:
        result = default_paths
    return result


def set_custom_form_renderer(config):
    """
    Uses an extended renderer that ensures the request object is on our form
    rendering context
    Code largely inspired from pyramid_deform/__init__.py
    """
    # Add translation directories
    config.add_translation_dirs('colander:locale', 'deform:locale')
    config.add_static_view(
        "static-deform",
        'deform:static',
        cache_max_age=3600
    )
    # Initialize the Renderer
    from pyramid_deform import translator
    renderer = CustomRenderer(get_search_path(), translator=translator)

    deform.form.Form.default_renderer = renderer


def customize_renderers(config):
    """
    Customize the different renderers
    """
    logger.debug("Setting renderers related hacks")
    # Json
    set_json_renderer(config)
    # deform
    set_custom_form_renderer(config)
    # Exporters
    configure_export()


def set_close_popup_response(
    request, message=None, error=None, refresh=True, force_reload=False
):
    """
    Write directly js code inside the request reponse's body to call popup close

    :param obj request: The Pyramid request object
    :param str message: The information message we want to return
    :param str error: The optionnal error messahe to send
    :param bool refresh: Should a refresh link be included
    :param bool force_reload: Shoud we reload the parent window automatically ?
    """
    options = {'refresh': refresh}

    if message is not None:
        options['message'] = message
    if error is not None:
        options['error'] = error
    if force_reload:
        options['force_reload'] = True

    request.response.text = """<!DOCTYPE html>
    <html><head><title></title></head><body>
    <script type="text/javascript">
    opener.dismissPopup(window, %s);
    </script></body></html>""" % (json.dumps(options))
    return request
