import pytest
import colander

from endi.forms.progress_invoicing import GroupStatusSchema


def test_invoice_percent_validation(mk_invoice, get_csrf_request):
    invoice = mk_invoice()
    req = get_csrf_request(context=invoice)

    schema = GroupStatusSchema()
    inv_schema = schema.bind(request=req)
    res = inv_schema.deserialize({'lines': [{'current_percent': 10, 'id': 1}]})
    assert res['lines'][0]['current_percent'] == 10.0
    with pytest.raises(colander.Invalid):
        inv_schema.deserialize({'lines': [{'current_percent': -10, 'id': 1}]})
    with pytest.raises(colander.Invalid):
        inv_schema.deserialize({'lines': [{'current_percent': 101, 'id': 1}]})


def test_cancelinvoice_percent_validation(mk_cancelinvoice, get_csrf_request):
    cinvoice = mk_cancelinvoice()
    req = get_csrf_request(context=cinvoice)

    schema = GroupStatusSchema()
    cinv_schema = schema.bind(request=req)
    res = cinv_schema.deserialize(
        {'lines': [{'current_percent': -10, 'id': 1}]}
    )
    assert res['lines'][0]['current_percent'] == -10.0
    with pytest.raises(colander.Invalid):
        cinv_schema.deserialize({'lines': [{'current_percent': 10, 'id': 1}]})
    with pytest.raises(colander.Invalid):
        cinv_schema.deserialize(
            {'lines': [{'current_percent': -101, 'id': 1}]}
        )
