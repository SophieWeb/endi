from unittest.mock import Mock

from endi.forms import (
    merge_session_with_post,
    flatten_appstruct,
)


def test_merge_session_with_post():
    session = Mock()
    post = dict(id=12, name="Dupont", lastname="Jean",
                            accounts=['admin', 'user'])
    merge_session_with_post(session, post)
    assert session.name == 'Dupont'
    assert "admin" in session.accounts


def test_flatten_appstruct():
    appstruct = {'key1':'value1', 'key2': {'key3': 'value3'}}
    assert flatten_appstruct(appstruct) == {'key1': 'value1', 'key3': 'value3'}
