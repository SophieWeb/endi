

def test_supplier_invoice_validation_sets_official_number(
        mk_supplier_invoice,
        pyramid_request,
        csrf_request_with_db_and_user,
):
    from endi.models.config import Config
    Config.set('supplierinvoice_number_template', '{SEQGLOBAL}')

    supplier_invoice_1 = mk_supplier_invoice()
    supplier_invoice_2 = mk_supplier_invoice()

    assert supplier_invoice_1.official_number is None
    assert supplier_invoice_2.official_number is None

    supplier_invoice_1.set_validation_status('valid', csrf_request_with_db_and_user)
    assert supplier_invoice_1.official_number == '1'

    supplier_invoice_2.set_validation_status('valid', csrf_request_with_db_and_user)
    assert supplier_invoice_2.official_number == '2'
