import pytest


@pytest.fixture
def task_line2(mk_task_line):
    return mk_task_line()


@pytest.fixture
def progress_invoicing_line_status2(
    mk_progress_invoicing_line_status, task_line2
):
    return mk_progress_invoicing_line_status(source_task_line=task_line2)


@pytest.fixture
def progress_invoicing_line_status_with_deposit(
    mk_progress_invoicing_line_status, task_line2
):
    return mk_progress_invoicing_line_status(
        source_task_line=task_line2,
        percent_to_invoice=50,
    )
