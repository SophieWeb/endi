import pytest
from unittest.mock import MagicMock
from endi.models.progress_invoicing.service import (
    ProgressInvoicingLineStatusService,
    ProgressInvoicingBaseStatusService,
    ProgressInvoicingGroupStatusService,
    ProgressInvoicingLineService,
    ProgressInvoicingGroupService,
)


@pytest.fixture
def task(dbsession, user, project, customer, company):
    from endi.models.task import Task
    task = Task(
        user=user,
        company=company,
        project=project,
        customer=customer
    )
    dbsession.add(task)
    dbsession.flush()
    return task


class TestProgressInvoicingBaseStatusService:

    def test_update_percent_left(self, progress_invoicing_line_status):
        ProgressInvoicingBaseStatusService.update_percent_left(
            progress_invoicing_line_status, 20
        )
        assert progress_invoicing_line_status.percent_left == 80
        # test si l'on passe un pourcentage de référence (cas de l'édition)
        ProgressInvoicingBaseStatusService.update_percent_left(
            progress_invoicing_line_status, 30, 100
        )
        assert progress_invoicing_line_status.percent_left == 70

    def test_current_percent_left(self):
        status = MagicMock(percent_left=15)
        assert ProgressInvoicingBaseStatusService.current_percent_left(
            status
        ) == 15


class TestProgressInvoicingLineStatusService:
    def test_get_cost(self, progress_invoicing_line_status):
        result = ProgressInvoicingLineStatusService.get_cost(
            progress_invoicing_line_status, 20, 100
        )
        assert result == 2000000

        progress_invoicing_line_status.percent_to_invoice = 80
        result = ProgressInvoicingLineStatusService.get_cost(
            progress_invoicing_line_status, 20, 80
        )
        assert result == 1600000

    def test_update_or_generate(self, progress_invoicing_line_status):
        result = ProgressInvoicingLineStatusService.update_or_generate(
            progress_invoicing_line_status, 20
        )
        assert result.cost == 2000000
        assert result.tva == \
            progress_invoicing_line_status.source_task_line.tva
        assert result.product == \
            progress_invoicing_line_status.source_task_line.product
        assert result.quantity == 1
        assert result.group_id is None

        record = progress_invoicing_line_status.invoiced_elements[0]
        assert record.percentage == 20

        result = ProgressInvoicingLineStatusService.update_or_generate(
            progress_invoicing_line_status, 20
        )
        assert progress_invoicing_line_status.percent_left == 60

    def test_is_completely_invoiced(self, progress_invoicing_line_status):
        assert not ProgressInvoicingLineStatusService.is_completely_invoiced(
            progress_invoicing_line_status
        )
        progress_invoicing_line_status.percent_left = 0
        assert ProgressInvoicingLineStatusService.is_completely_invoiced(
            progress_invoicing_line_status
        )

    def test_current_total_ht_left(self, progress_invoicing_line_status):
        result = ProgressInvoicingLineStatusService.update_or_generate(
            progress_invoicing_line_status, 20
        )
        result = ProgressInvoicingLineStatusService.current_total_ht_left(
            progress_invoicing_line_status
        )
        assert result == 8000000

    def test_update_percent_left_with_deposit(
        self, progress_invoicing_line_status_with_deposit
    ):
        result = ProgressInvoicingLineStatusService.update_or_generate(
            progress_invoicing_line_status_with_deposit, 20
        )
        assert result.cost == 1000000
        assert result.tva == \
            progress_invoicing_line_status_with_deposit.source_task_line.tva
        assert result.product == \
            progress_invoicing_line_status_with_deposit.source_task_line.product
        assert result.quantity == 1
        assert result.group_id is None

        record = progress_invoicing_line_status_with_deposit.invoiced_elements[0]
        assert record.percentage == 20

        result = ProgressInvoicingLineStatusService.update_or_generate(
            progress_invoicing_line_status_with_deposit, 20
        )
        assert progress_invoicing_line_status_with_deposit.percent_left == 60



class TestProgressInvoicingGroupStatusService:

    def test__find_percentage_to_invoice(
        self,
        progress_invoicing_group_status,
        progress_invoicing_line_status,
    ):
        result = ProgressInvoicingGroupStatusService\
            ._find_percentage_to_invoice(
                progress_invoicing_group_status,
            )
        assert result == 100

    def test_get_or_generate_global(
        self,
        progress_invoicing_group_status,
        progress_invoicing_line_status,
        progress_invoicing_line_status2,
        mk_invoice,
    ):
        appstruct = {
            progress_invoicing_line_status.id: 20,
            progress_invoicing_line_status2.id: 20
        }
        result = ProgressInvoicingGroupStatusService.get_or_generate(
            progress_invoicing_group_status, appstruct
        )
        invoice = mk_invoice()
        result.task = invoice

        assert progress_invoicing_group_status.percent_left == 80
        assert len(result.lines) == 2
        assert result.total_ht() == 4000000

    def test_get_or_generate_single(
        self,
        progress_invoicing_group_status,
        progress_invoicing_line_status,
        progress_invoicing_line_status2,
        mk_invoice,
    ):
        appstruct = {
            progress_invoicing_line_status.id: 20,
        }
        result = ProgressInvoicingGroupStatusService.get_or_generate(
            progress_invoicing_group_status, appstruct
        )
        invoice = mk_invoice()
        result.task = invoice

        assert len(result.lines) == 2
        assert result.total_ht() == 2000000
        assert progress_invoicing_group_status.percent_left is None

        assert progress_invoicing_line_status.percent_left == 80
        assert progress_invoicing_line_status2.percent_left == 100

    def test_get_or_generate_different(
        self,
        progress_invoicing_group_status,
        progress_invoicing_line_status,
        progress_invoicing_line_status2,
        mk_invoice,
    ):
        appstruct = {
            progress_invoicing_line_status.id: 20,
            progress_invoicing_line_status2.id: 30,
        }
        result = ProgressInvoicingGroupStatusService.get_or_generate(
            progress_invoicing_group_status, appstruct
        )
        invoice = mk_invoice()
        result.task = invoice

        assert len(result.lines) == 2
        assert result.total_ht() == 5000000
        assert progress_invoicing_group_status.percent_left is None

    def test_is_completely_invoiced(
        self,
        progress_invoicing_group_status,
        progress_invoicing_line_status
    ):
        assert not ProgressInvoicingGroupStatusService.is_completely_invoiced(
            progress_invoicing_group_status
        )
        progress_invoicing_line_status.percent_left = 0
        assert ProgressInvoicingGroupStatusService.is_completely_invoiced(
            progress_invoicing_group_status
        )


class TestProgressInvoicingLineService:

    def build_one(
        self,
        dbsession,
        progress_invoicing_line_status,
        mk_progress_invoicing_line,
        mk_invoice,
        mk_task_line,
        mk_task_line_group,
        percentage1,
        percentage2,
    ):
        invoice = mk_invoice()
        group = mk_task_line_group()
        line = mk_task_line(
            group=group,
            group_id=group.id,
            cost=int(10000000 * percentage1 / 100.0)
        )
        invoice.line_groups = [group]
        dbsession.merge(invoice)
        dbsession.flush()
        mk_progress_invoicing_line(
            task_line_id=line.id,
            percentage=percentage1,
            base_status_id=progress_invoicing_line_status.id,
        )

        invoice2 = mk_invoice()
        group2 = mk_task_line_group()
        line2 = mk_task_line(
            group=group2,
            group_id=group2.id,
            cost=int(10000000 * percentage2 / 100.0),
        )
        invoice2.line_groups = [group2]
        dbsession.merge(invoice2)
        dbsession.flush()
        mk_progress_invoicing_line(
            task_line_id=line2.id,
            percentage=percentage2,
            base_status_id=progress_invoicing_line_status.id,
        )
        return invoice, invoice2

    def test_find_invoiced_percentage(
        self,
        dbsession,
        progress_invoicing_line_status,
        mk_progress_invoicing_line,
        mk_invoice,
        mk_task_line,
        mk_task_line_group,
    ):
        invoice1, invoice2 = self.build_one(
            dbsession,
            progress_invoicing_line_status,
            mk_progress_invoicing_line,
            mk_invoice,
            mk_task_line,
            mk_task_line_group,
            15,
            30
        )
        assert ProgressInvoicingLineService.find_invoiced_percentage(
            progress_invoicing_line_status,
        ) == 45

        assert ProgressInvoicingLineService.find_invoiced_percentage(
            progress_invoicing_line_status, invoice2.all_lines[0].id
        ) == 15

    def test_find_invoiced_total_ht(
        self,
        dbsession,
        progress_invoicing_line_status,
        mk_progress_invoicing_line,
        mk_invoice,
        mk_task_line,
        mk_task_line_group,
    ):
        invoice1, invoice2 = self.build_one(
            dbsession,
            progress_invoicing_line_status,
            mk_progress_invoicing_line,
            mk_invoice,
            mk_task_line,
            mk_task_line_group,
            15,
            30
        )
        from endi.models.progress_invoicing.service import (
            ProgressInvoicingLineService,
        )
        assert ProgressInvoicingLineService.find_invoiced_total_ht(
            progress_invoicing_line_status,
        ) == int(10000000 * 45 / 100.0)
        assert ProgressInvoicingLineService.find_invoiced_total_ht(
            progress_invoicing_line_status,
            invoice2.all_lines[0].id
        ) == int(10000000 * 15 / 100.0)


class TestProgressInvoicingGroupService:
    def test_find_invoiced_percentage(
        self,
        dbsession,
        progress_invoicing_group_status,
        mk_progress_invoicing_group,
        mk_invoice,
        mk_task_line,
        mk_task_line_group,
    ):
        invoice = mk_invoice()
        group = mk_task_line_group()
        line = mk_task_line(group=group)
        invoice.line_groups = [group]
        dbsession.merge(invoice)
        dbsession.flush()
        progress_line = mk_progress_invoicing_group(
            task_line_group_id=group.id,
            percentage=15,
        )

        invoice2 = mk_invoice()
        group2 = mk_task_line_group()
        line2 = mk_task_line(group=group2)
        invoice.line_groups = [group2]
        dbsession.merge(invoice2)
        dbsession.flush()
        progress_line2 = mk_progress_invoicing_group(
            task_line_group_id=group2.id,
            percentage=30,
        )

        assert ProgressInvoicingGroupService.find_invoiced_percentage(
            progress_invoicing_group_status,
        ) == 45

        assert ProgressInvoicingGroupService.find_invoiced_percentage(
            progress_invoicing_group_status, group2.id
        ) == 15
