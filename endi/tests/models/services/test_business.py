import pytest
from endi.models.services.business import BusinessService


def test_to_invoice(business, full_estimation):
    assert BusinessService.to_invoice(business) == full_estimation.ht


def test_populate_deadlines(business, full_estimation):
    BusinessService.populate_deadlines(business)
    assert len(business.payment_deadlines) == 3
    for deadline in business.payment_deadlines:
        assert deadline.invoiced is False
    assert business.status == "danger"


def test_populate_find_deadline(business):
    BusinessService.populate_deadlines(business)
    id_ = business.payment_deadlines[1].id
    assert BusinessService.find_deadline(business, id_) == \
        business.payment_deadlines[1]


def test_find_deadline_from_invoice(dbsession, business, invoice):
    BusinessService.populate_deadlines(business)
    business.payment_deadlines[1].invoice = invoice
    dbsession.merge(business.payment_deadlines[1])
    dbsession.flush()

    assert BusinessService.find_deadline_from_invoice(business, invoice) == \
        business.payment_deadlines[1]


def test_gen_invoices_one(business, full_estimation, user):
    BusinessService.populate_deadlines(business)

    invoices = BusinessService.gen_invoices(
        business,
        user,
        business.payment_deadlines[1]
    )
    assert len(invoices) == 1
    assert business.payment_deadlines[1].invoice == invoices[0]


def test_gen_invoices_all(business, full_estimation, user):
    BusinessService.populate_deadlines(business)

    invoices = BusinessService.gen_invoices(
        business,
        user,
    )
    assert len(invoices) == 3
    for i in range(2):
        assert business.payment_deadlines[i].invoice == invoices[i]


def test_is_visible(dbsession, business, project, mk_project_type):
    business.project = project
    dbsession.merge(business)
    assert BusinessService.is_complex_project_type(business) is False
    project.project_type = mk_project_type(name="newone", with_business=True)
    dbsession.merge(project)
    assert BusinessService.is_complex_project_type(business) is True


def test_get_customer(business, full_estimation):
    result = BusinessService.get_customer(business)
    assert result == full_estimation.customer


def test_add_invoice_bug1077(business, user, customer, full_estimation):
    result = business.add_invoice(user)
    assert result.address == customer.full_address
    assert result.business_id == business.id
    assert result.business_type_id == business.business_type.id
    assert result.company == business.project.company
    assert result.project == business.project


def test_add_estimation_bug1077(business, user, customer):
    result = business.add_estimation(user)
    assert result.address == customer.full_address
    assert result.business_id == business.id
    assert result.business_type_id == business.business_type.id
    assert result.company == business.project.company
    assert result.project == business.project


def test_is_void(business, mk_business, user):
    assert not business.is_void()

    void_business = mk_business(name="test")
    assert void_business.is_void()


def test__get_estimations_to_invoice(business):
    assert BusinessService._get_estimations_to_invoice(business) == []
    business.estimations[0].status = 'valid'
    assert len(BusinessService._get_estimations_to_invoice(business)) == 1
    business.estimations[0].signed_status = 'aborted'
    assert BusinessService._get_estimations_to_invoice(business) == []


# PROGRESS INVOICING
def test_add_progress_invoicing_invoice(
    dbsession,
    business_with_progress_invoicing,
    user,
    full_estimation,
    mk_estimation,
):
    invoice = BusinessService.add_progress_invoicing_invoice(
        business_with_progress_invoicing,
        user
    )
    assert invoice.display_units == 1
    assert invoice.notes == full_estimation.notes
    assert invoice.workplace == full_estimation.workplace
    assert invoice.description == full_estimation.description
    assert invoice.address == full_estimation.address
    assert invoice.payment_conditions == full_estimation.payment_conditions
    assert invoice.start_date == full_estimation.start_date
    assert invoice.estimation == full_estimation

    est = mk_estimation(business_id=business_with_progress_invoicing.id)
    business_with_progress_invoicing.estimations.append(est)
    dbsession.merge(business_with_progress_invoicing)
    invoice = BusinessService.add_progress_invoicing_invoice(
        business_with_progress_invoicing,
        user
    )
    assert invoice.estimation_id is None


def test_populate_progress_invoicing_status(business):
    from endi.models.progress_invoicing.progress_invoicing import (
        ProgressInvoicingLineStatus,
        ProgressInvoicingGroupStatus,
    )
    # Invoicing Mode is not correct
    with pytest.raises(Exception):
        BusinessService.populate_progress_invoicing_status(
            business
        )

    business.invoicing_mode = business.PROGRESS_MODE

    # No valid estimation
    BusinessService.populate_progress_invoicing_status(
        business
    )
    assert ProgressInvoicingGroupStatus.query().filter_by(
        business_id=business.id
    ).count() == 0

    # Correct use case
    business.estimations[0].status = 'valid'
    BusinessService.populate_progress_invoicing_status(
        business
    )

    group_statuses = ProgressInvoicingGroupStatus.query().filter_by(
        business_id=business.id
    ).all()

    assert len(group_statuses) == 1
    assert group_statuses[0].percent_to_invoice == 90
    assert group_statuses[0].percent_left == 100

    line_statuses = ProgressInvoicingLineStatus.query().filter_by(
        business_id=business.id
    ).all()
    assert len(line_statuses) == 2
    for line_status in line_statuses:
        assert line_status.percent_to_invoice == 90
        assert line_status.percent_left == 100

    # Test la non duplication
    BusinessService.populate_progress_invoicing_status(
        business
    )
    line_statuses = ProgressInvoicingLineStatus.query().filter_by(
        business_id=business.id
    ).all()
    assert len(line_statuses) == 2


def test_on_estimation_status_changed(business_with_progress_invoicing):
    business = business_with_progress_invoicing
    # Set it to aborted, no invoice should be expected after that
    business.estimations[0].signed_status = 'aborted'
    BusinessService.populate_progress_invoicing_status(
        business
    )
    from endi.models.progress_invoicing.progress_invoicing import (
        ProgressInvoicingLineStatus,
        ProgressInvoicingGroupStatus,
    )
    assert ProgressInvoicingGroupStatus.query().filter_by(
        business_id=business.id
    ).count() == 0
    assert ProgressInvoicingLineStatus.query().filter_by(
        business_id=business.id
    ).count() == 0


def test_on_invoice_postdelete_delete_business(
    dbsession, mk_business, invoice, default_business_type
):
    from endi.models.project.business import Business
    business = mk_business()
    bid = business.id
    business.invoices = [invoice]
    dbsession.merge(business)
    invoice.business_type_id = default_business_type.id
    invoice.businesses = [business]
    dbsession.delete(invoice)
    BusinessService.on_invoice_postdelete(business, invoice.id)
    dbsession.flush()
    assert Business.get(bid) is None


def test_on_invoice_predelete_clean_progress_status(
    business_with_progress_invoicing, progress_invoicing_invoice
):
    BusinessService.on_invoice_predelete(
        business_with_progress_invoicing, progress_invoicing_invoice.id
    )

    all_status = business_with_progress_invoicing.progress_invoicing_statuses
    for status in all_status:
        assert status.percent_left == 100
        assert status.invoiced_elements == []


def test_populate_progress_invoicing_lines(
    business_with_progress_invoicing, progress_invoicing_invoice
):
    invoice = progress_invoicing_invoice
    # On a déjà eu une facture d'accompte 10000000 * 90/100 * 10/100 * 2
    assert invoice.total_ht() == 1800000
    tva_parts = invoice.tva_ht_parts()
    assert not set(tva_parts.keys()).difference(set([700, 2000]))
    assert tva_parts[700] == 900000
    assert tva_parts[2000] == 900000

    all_status = business_with_progress_invoicing.progress_invoicing_statuses
    for status in all_status:
        assert status.percent_left == 90
        assert len(status.invoiced_elements) == 1
        assert status.invoiced_elements[0].percentage == 10


def test_populate_progress_invoicing_with_no_datas(
    business_with_progress_invoicing, user, full_estimation
):
    invoice = business_with_progress_invoicing.add_progress_invoicing_invoice(
        user
    )
    BusinessService.populate_progress_invoicing_lines(
        business_with_progress_invoicing,
        invoice,
        {}
    )
    assert invoice.estimation == full_estimation
    assert len(invoice.all_lines) == 2


def test_populate_progress_invoicing_lines_several_times(
    business_with_progress_invoicing, progress_invoicing_invoice, user
):
    invoice = business_with_progress_invoicing.add_progress_invoicing_invoice(
        user
    )

    # On construit la structure de données attendues pour la génération des
    # lignes de prestation
    # Ici on a un pourcentage différent par produit
    appstruct = {}
    for status in business_with_progress_invoicing.\
            progress_invoicing_group_statuses:
        appstruct[status.id] = {}
        for index, line_status in enumerate(status.line_statuses):
            # une ligne à 10%, une ligne à 20%
            appstruct[status.id][line_status.id] = (index + 1) * 10

    # On populate notre facture
    BusinessService.populate_progress_invoicing_lines(
        business_with_progress_invoicing,
        invoice,
        appstruct,
    )

    # Check les TaskLine et TaskLineGroup
    assert len(invoice.line_groups) == 1
    assert len(invoice.all_lines) == 2
    assert invoice.total_ht() == 2700000
    tva_parts = invoice.tva_ht_parts()
    assert tva_parts[2000] == 900000
    assert tva_parts[700] == 1800000

    group_status = business_with_progress_invoicing\
        .progress_invoicing_group_statuses[0]

    # Check des status de facturation
    assert group_status.percent_left is None
    assert business_with_progress_invoicing\
        .progress_invoicing_statuses[1].percent_left == 80
    assert business_with_progress_invoicing\
        .progress_invoicing_statuses[2].percent_left == 70

    # Check du suivi des éléments facturés
    invoiced_elements = group_status.invoiced_elements

    assert len(invoiced_elements) == 2
    assert invoiced_elements[1].percentage is None

    # ETAPE 2 :
    # On repasse sur un pourcentage global au groupe
    appstruct = {}
    for status in business_with_progress_invoicing.\
            progress_invoicing_group_statuses:
        appstruct[status.id] = {}
        for line_status in status.line_statuses:
            # une ligne à 10%, une ligne à 20%
            appstruct[status.id][line_status.id] = 10

    # On populate notre facture
    BusinessService.populate_progress_invoicing_lines(
        business_with_progress_invoicing,
        invoice,
        appstruct,
    )

    # Check les TaskLine et TaskLineGroup
    assert len(invoice.line_groups) == 1
    assert len(invoice.all_lines) == 2
    assert invoice.total_ht() == 1800000
    tva_parts = invoice.tva_ht_parts()
    assert tva_parts[2000] == 900000
    assert tva_parts[700] == 900000

    group_status = business_with_progress_invoicing\
        .progress_invoicing_group_statuses[0]

    # Check des status de facturation
    assert group_status.percent_left == 80
    assert business_with_progress_invoicing\
        .progress_invoicing_statuses[1].percent_left == 80
    assert business_with_progress_invoicing\
        .progress_invoicing_statuses[2].percent_left == 80

    # Check du suivi des éléments facturés
    invoiced_elements = group_status.invoiced_elements

    assert len(invoiced_elements) == 2
    assert invoiced_elements[1].percentage == 10

    # ETAPE 3 : on revient en arrière, on vérifie qu'on a pas des résidus de
    # l'étape 2
    appstruct = {}
    for status in business_with_progress_invoicing.\
            progress_invoicing_group_statuses:
        appstruct[status.id] = {}
        for index, line_status in enumerate(status.line_statuses):
            # une ligne à 10%, une ligne à 20%
            appstruct[status.id][line_status.id] = (index + 1) * 10

    # On populate notre facture
    BusinessService.populate_progress_invoicing_lines(
        business_with_progress_invoicing,
        invoice,
        appstruct,
    )

    # Check les TaskLine et TaskLineGroup
    assert len(invoice.line_groups) == 1
    assert len(invoice.all_lines) == 2
    assert invoice.total_ht() == 2700000
    tva_parts = invoice.tva_ht_parts()
    assert tva_parts[2000] == 900000
    assert tva_parts[700] == 1800000

    group_status = business_with_progress_invoicing\
        .progress_invoicing_group_statuses[0]

    # Check des status de facturation
    assert group_status.percent_left is None
    assert business_with_progress_invoicing\
        .progress_invoicing_statuses[1].percent_left == 80
    assert business_with_progress_invoicing\
        .progress_invoicing_statuses[2].percent_left == 70

    # Check du suivi des éléments facturés
    invoiced_elements = group_status.invoiced_elements

    assert len(invoiced_elements) == 2
    assert invoiced_elements[1].percentage is None
    assert invoiced_elements[0].percentage == 10

    # ETAPE 4 : on crée une nouvelle facture et on vérifie que le percent_left
    # à None est bien pris en compte
    invoice = business_with_progress_invoicing.add_progress_invoicing_invoice(
        user
    )

    # On construit la structure de données attendues pour la génération des
    # lignes de prestation
    # Ici on a un pourcentage configuré niveau produit composé (semble-t-il)
    appstruct = {}
    for status in business_with_progress_invoicing.\
            progress_invoicing_group_statuses:
        appstruct[status.id] = {}
        for line_status in status.line_statuses:
            # une ligne à 10%, une ligne à 20%
            appstruct[status.id][line_status.id] = 10

    # On populate notre facture
    BusinessService.populate_progress_invoicing_lines(
        business_with_progress_invoicing,
        invoice,
        appstruct,
    )
    group_status = business_with_progress_invoicing\
        .progress_invoicing_group_statuses[0]

    # Check des status de facturation
    assert group_status.percent_left is None
    assert business_with_progress_invoicing\
        .progress_invoicing_statuses[1].percent_left == 70
    assert business_with_progress_invoicing\
        .progress_invoicing_statuses[2].percent_left == 60


def test_progress_invoicing_is_complete(
    dbsession, business_with_progress_invoicing
):
    business = business_with_progress_invoicing
    assert not BusinessService.progress_invoicing_is_complete(business)
    for group_status in business.progress_invoicing_group_statuses:
        for line_status in group_status.line_statuses:
            line_status.percent_left = 0
            dbsession.merge(line_status)
            dbsession.flush()
    assert BusinessService.progress_invoicing_is_complete(business)


def test_get_invoice_total_depending_on_project_mode_ht(
        dbsession, business, full_invoice):
    business.invoices = [full_invoice]
    # project mode is ht
    assert BusinessService\
        .get_invoice_total_depending_on_project_mode(business) == 10000000


def test_get_invoice_total_depending_on_project_mode_ttc(
        dbsession, business_with_project_mode_ttc, full_invoice):
    business_with_project_mode_ttc.invoices = [full_invoice]
    # project mode is ttc
    assert BusinessService \
        .get_invoice_total_depending_on_project_mode(
            business_with_project_mode_ttc) == 11000000.0


def test_get_current_invoice(dbsession, business, mk_invoice):
    inv = mk_invoice(business_id=business.id)
    assert BusinessService.get_current_invoice(business) == inv
    inv.status = 'valid'
    dbsession.merge(inv)
    assert BusinessService.get_current_invoice(business) is None
