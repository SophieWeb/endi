import pytest
from endi.models.price_study.product import PriceStudyProduct
from endi.models.price_study.price_study import PriceStudy
from endi.models.price_study.services import (
    PriceStudyProductService,
    PriceStudyWorkService,
)
from endi.tests.tools import Dummy
from endi.compute.math_utils import amount


@pytest.fixture
def study(company, user, project, dbsession):
    study = PriceStudy(
        company_id=company.id, owner_id=user.id, project_id=project.id,
        name="study",
    )
    dbsession.add(study)
    dbsession.flush()
    return study


@pytest.fixture
def product(study, dbsession):
    product = PriceStudyProduct(
        study_id=study.id,
        supplier_ht=100,
        general_overhead=0.1,
        margin_rate=0.1,
    )
    dbsession.add(product)
    dbsession.flush()
    return product


class TestPriceStudyProductService:

    def test_flat_cost(self, product):
        assert amount(PriceStudyProductService.flat_cost(product), 2) == 10000
        assert PriceStudyProductService.flat_cost(Dummy(supplier_ht=None)) == 0

    def test_cost_price(self, product):
        assert amount(PriceStudyProductService.cost_price(product), 2) == 11000

    def test_intermediate_price(self, product):
        assert amount(
            PriceStudyProductService.intermediate_price(product),
            2
        ) == 12222

    def test_unit_ht(self, product):
        assert amount(
            PriceStudyProductService.unit_ht(product, 10),
            2,
        ) == 13580
        # Ref #1877
        assert amount(
            PriceStudyProductService.unit_ht(product, 10.0),
            2,
        ) == 13580


class TestPriceStudyWorkService:
    def test_find_common_value_fix_2432(
            self,
            dbsession,
            mk_price_study_work,
            mk_price_study_work_item,
            tva,
    ):
        work = mk_price_study_work(tva=None)

        workitem1 = mk_price_study_work_item(ht=10000, _tva=tva)
        workitem2 = mk_price_study_work_item(ht=10000, _tva=tva)
        work.items.append(workitem1)
        work.items.append(workitem2)
        dbsession.merge(work)
        dbsession.flush()

        assert PriceStudyWorkService.find_common_value(work, 'tva') == tva

        workitem1._tva = None
        workitem2._tva = None

        dbsession.merge(workitem1)
        dbsession.merge(workitem2)
        dbsession.flush()

        ret = PriceStudyWorkService.find_common_value(work, 'tva')
        assert ret == None  # noqa

        work.tva = tva
        dbsession.merge(work)
        dbsession.flush()

        assert PriceStudyWorkService.find_common_value(work, 'tva') == tva
