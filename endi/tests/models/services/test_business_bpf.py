import pytest

from endi.models.services.bpf import BusinesssBPFDataMigrator_15to16
from endi.models.training.bpf import (
    IncomeSource,
    TraineeCount,
)


@pytest.fixture
def mk_income_source(
        fixture_factory,
        invoice
):
    return fixture_factory(
        IncomeSource,
        invoice_id=invoice.id,
        income_category_id=1,
    )


@pytest.fixture
def mk_trainee_count(
        fixture_factory,
):
    return fixture_factory(
        TraineeCount,
        trainee_type_id=1,
        headcount=10,
        total_hours=100,
    )


@pytest.fixture
def bpf_data_wrong_cerfa_full(
        mk_business_bpf_data,
        mk_income_source,
        mk_trainee_count,
        training_business,
):
    bpf_data = mk_business_bpf_data(
        financial_year=2020,
        cerfa_version='10443*15',
        training_goal_id=8,
        business=training_business,
    )
    mk_income_source(business_bpf_data_id=bpf_data.id)
    mk_trainee_count(business_bpf_data_id=bpf_data.id)
    return bpf_data


def test_BusinesssBPFDataMigrator_15to16(bpf_data_wrong_cerfa_full):
    bpf_data = bpf_data_wrong_cerfa_full

    BusinesssBPFDataMigrator_15to16.migrate(bpf_data)

    assert bpf_data.cerfa_version == '10443*16'
    assert bpf_data.financial_year == 2020
    assert len(bpf_data.income_sources) == 1
    assert bpf_data.income_sources[0].income_category_id == 2
    assert len(bpf_data.trainee_types) == 1
    assert bpf_data.trainee_types[0].trainee_type_id == 2

    assert bpf_data.training_goal_id == 14
