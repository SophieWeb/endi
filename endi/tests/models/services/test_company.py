from endi.models.services.company import CompanyService


def test_get_business_nested_options(company, customer, project, business):
    assert (
    list(CompanyService.get_business_nested_options(company))
        ==
        [
            {
                'id': customer.id,
                'label': customer.label,
                'projects': [
                    {
                        'id': project.id,
                        'label': project.name,
                        'businesses': [
                            {'id': business.id, 'label': business.name},
                        ]
                    },
                ],
            }
        ]
    )
