import datetime


def test_estimation_set_numbers(full_estimation):
    full_estimation.date = datetime.date(1969, 7, 1)
    full_estimation.set_numbers(5, 18)
    assert full_estimation.internal_number == "Company 1969-07 D5"
    assert full_estimation.name == "Devis 18"
    assert full_estimation.project_index == 18


def test_duplicate_payment_line(payment_line):
    newline = payment_line.duplicate()
    for i in ('order', 'description', 'amount'):
        assert getattr(newline, i) == getattr(payment_line, i)

    today = datetime.date.today()
    assert newline.date == today


def test_set_default_validity_duration(mk_estimation):
    # https://framagit.org/endi/endi/-/issues/2181
    from endi.models.config import Config
    Config.set("estimation_validity_duration_default", "AAA")

    estimation1 = mk_estimation()
    estimation1.set_default_validity_duration()
    assert estimation1.validity_duration == 'AAA'

    mk_estimation(validity_duration='BBB')

    estimation3 = mk_estimation()
    estimation3.set_default_validity_duration()
    assert estimation3.validity_duration == 'BBB'
