import datetime
from endi.compute.math_utils import integer_to_amount
from endi.models.task.services import (
    EstimationInvoicingService,
    EstimationService,
    InternalEstimationProcessService,
)


class TestEstimationInvoicingService:
    def test_get_common_invoice(self, estimation, user):
        common_invoice = EstimationInvoicingService._get_common_invoice(
            estimation,
            user,
        )
        for key in (
            'company',
            'customer',
            'project',
            'business_id',
            'business_type_id',
            'phase_id',
            'payment_conditions',
            'description',
            'address',
            'workplace',
            'mentions',
            'start_date',
        ):
            assert getattr(common_invoice, key) == getattr(estimation, key)

        assert common_invoice.estimation == estimation
        assert common_invoice.status_person == user
        assert common_invoice.owner == user

    def test_get_common_invoice_ht_mode(self, estimation, user):
        common_invoice = EstimationInvoicingService._get_common_invoice(
            estimation,
            user,
        )
        assert common_invoice.mode == estimation.mode
        assert common_invoice.mode == 'ht'

    def test_get_common_invoice_ttc_mode(self, estimation, user):
        estimation.mode = 'ttc'
        common_invoice = EstimationInvoicingService._get_common_invoice(
            estimation,
            user,
        )
        assert common_invoice.mode == estimation.mode
        assert common_invoice.mode == 'ttc'

    def test_get_task_line(self):
        task_line = EstimationInvoicingService._get_task_line(
            100000, "Description", tva=700
        )
        assert task_line.description == "Description"
        assert task_line.cost == 100000
        assert task_line.tva == 700

    def test_get_deposit_task_line(self):
        task_line = EstimationInvoicingService._get_deposit_task_line(
            100000, 700
        )
        assert task_line.description == "Facture d'acompte"
        assert task_line.cost == 100000
        assert task_line.tva == 700

    def test_get_deposit_task_lines(self, full_estimation, tva):
        task_lines = EstimationInvoicingService._get_deposit_task_lines(
            full_estimation
        )
        assert len(task_lines) == 2
        assert task_lines[0].cost + task_lines[1].cost == 2 * 1000000
        assert task_lines[0].tva == tva.value
        assert task_lines[1].tva == 700

    def test_gen_deposit_invoice(self, full_estimation, user, plugin_active):
        deposit_invoice = EstimationInvoicingService.gen_deposit_invoice(
            full_estimation,
            user,
        )
        assert deposit_invoice.all_lines[0].cost \
            + deposit_invoice.all_lines[1].cost == \
            0.1 * full_estimation.total_ht()

        import datetime
        today = datetime.date.today()
        # Ref : https://framagit.org/endi/endi/issues/1812
        if not plugin_active('sap'):
            assert deposit_invoice.display_units == 0
        assert deposit_invoice.date == today

    def test_get_intermediate_invoiceable_amounts(self, full_estimation, tva):
        amounts = EstimationInvoicingService.\
            _get_intermediate_invoiceable_amounts(
                full_estimation
            )
        assert 700 in list(amounts[0].keys())
        assert tva.value in list(amounts[0].keys())

    def test_gen_intermediate_invoice(
        self,
        full_estimation,
        payment_line,
        plugin_active,
        user,
    ):
        invoice = EstimationInvoicingService.gen_intermediate_invoice(
            full_estimation, payment_line, user
        )

        # Ref : https://framagit.org/endi/endi/issues/1812
        if not plugin_active('sap'):
            assert invoice.display_units == 0
        assert invoice.total_ttc() == payment_line.amount

    def test__get_all_intermediate_invoiceable_task_lines(
        self, full_estimation, estimation
    ):
        lines = EstimationInvoicingService.\
            _get_all_intermediate_invoiceable_task_lines(full_estimation)
        assert len(lines) == 4  # 2 pour l'acompte + 2 pour le premier paiement

    def test_gen_sold_invoice(self, full_estimation, user):
        invoice = EstimationInvoicingService.gen_sold_invoice(
            full_estimation, user
        )
        # Ref : https://framagit.org/endi/endi/issues/1812
        assert invoice.display_units == 1

        lines = invoice.all_lines
        for index in range(2):
            assert lines[index].cost == full_estimation.all_lines[index].cost
            assert lines[index].tva == full_estimation.all_lines[index].tva

        assert len(lines) == 6
        assert len(invoice.discounts) == 1

    def test_all_invoices(self, full_estimation, payment_line, user):
        deposit_invoice = EstimationInvoicingService.gen_deposit_invoice(
            full_estimation, user
        )
        intermediate_invoice = EstimationInvoicingService.\
            gen_intermediate_invoice(
                full_estimation, payment_line, user
            )
        sold_invoice = EstimationInvoicingService.gen_sold_invoice(
            full_estimation, user
        )
        assert deposit_invoice.total_ht() + intermediate_invoice.total_ht() + \
            sold_invoice.total_ht() == full_estimation.total_ht()

    def test_gen_invoice_ref450(self, full_estimation, user):
        sold_invoice = EstimationInvoicingService.gen_sold_invoice(
            full_estimation, user
        )

        for line in full_estimation.all_lines:
            print((line.product_id))
            print(line)

        for line in sold_invoice.all_lines:
            assert line.product_id is not None


class TestInternalEstimationProcessService:
    def test_sync_with_customer(
        self, internal_customer, mk_internalestimation, mk_expense_type,
        task_line, task_line_group, dbsession, get_csrf_request_with_db
    ):
        task_line_group.lines = [task_line]
        estimation = mk_internalestimation(status='valid')
        estimation.line_groups = [task_line_group]
        dbsession.add(estimation)
        dbsession.flush()

        request = get_csrf_request_with_db(context=estimation)
        order = estimation.sync_with_customer(request)

        assert estimation.supplier_order_id == order.id
        assert integer_to_amount(order.total, 2) == \
            integer_to_amount(estimation.total(), 5)
        assert order.supplier.source_company_id == estimation.company_id

        typ = mk_expense_type(internal=True, label="Type interne")
        order = estimation.sync_with_customer(request)
        assert order.lines[0].type_id == typ.id


class TestEstimationService:
    def test_duplicate(self, full_estimation, user, customer, project, phase):
        result = EstimationService.duplicate(
            full_estimation, user, project=project,
            customer=customer, phase=phase
        )
        assert result.internal_number.startswith(
            "Company {0:%Y-%m}".format(datetime.date.today())
        )
        assert len(full_estimation.default_line_group.lines) == len(
            result.default_line_group.lines
        )
        assert len(full_estimation.payment_lines) == len(
            result.payment_lines
        )
        assert len(full_estimation.discounts) == len(result.discounts)
