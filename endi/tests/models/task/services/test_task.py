import pytest
from endi.models.task.services import (
    TaskService,
)


from endi.models.task import Task


@pytest.fixture
def task(
    dbsession, tva, product, customer, project, user, company, phase,
    mk_task_line,
):
    result = Task(
        project=project,
        customer=customer,
        company=company,
        user=user,
        phase=phase,
    )
    result.line_groups[0].lines.append(
        mk_task_line(group=result.line_groups[0])
    )
    dbsession.add(result)
    dbsession.flush()
    return result


@pytest.fixture
def valid_status_log_entry(mk_status_log_entry, task, user):
    return mk_status_log_entry(
        node_id=task.id,
        status='valid',
        user_id=user.id,
        state_manager_key='status',
    )


@pytest.fixture
def draft_status_log_entry(mk_status_log_entry, task, user2):
    return mk_status_log_entry(
        node_id=task.id,
        status='draft',
        user_id=user2.id,
        state_manager_key='status',
    )


class TestTaskService:
    def test_duplicate(self, task, user, project, customer, phase):
        result = TaskService.duplicate(
            task, user, project=project, customer=customer, phase=phase,
        )
        for field in (
            'description',
            'mode',
            'display_units',
            'display_ttc',
            'expenses_ht',
            'workplace',
            'payment_conditions',
            'notes',
            'start_date',
            'company',
            'customer',
            'project',
            'phase',
            'address',
        ):
            assert getattr(result, field) == getattr(task, field)
        assert result.status == 'draft'
        assert result.owner == user
        assert result.status_person == user

    def test_duplicate_mode(self, task, user, project, customer, phase):
        task.mode = 'ttc'
        result = TaskService.duplicate(
            task, user, project=project, customer=customer, phase=phase,
        )
        assert result.mode == 'ttc'
        task.mode = 'ht'
        result = TaskService.duplicate(
            task, user, project=project, customer=customer, phase=phase,
        )
        assert result.mode == 'ht'

    def test_duplicate_decimal_to_display(
        self, task, user, project, customer, phase, company,
    ):
        result = TaskService.duplicate(
            task, user, project=project, customer=customer, phase=phase,
        )
        assert result.decimal_to_display == 2
        company.decimal_to_display = 5
        result = TaskService.duplicate(
            task, user, project=project, customer=customer, phase=phase,
        )
        assert result.decimal_to_display == 5

    def test_filter_by_validator_id_no_status(self, task, user, user2):
        assert TaskService.query_by_validator_id(Task, user2.id).count() == 0
        assert TaskService.query_by_validator_id(Task, user.id).count() == 0

    def test_filter_by_validator_id(
            self,
            task,
            draft_status_log_entry,
            valid_status_log_entry,
    ):
        validator_id = valid_status_log_entry.user_id
        draftor_id = draft_status_log_entry.user_id
        assert TaskService.query_by_validator_id(Task, draftor_id).count() == 0
        assert TaskService.query_by_validator_id(Task,
                                                 validator_id).count() == 1
        assert TaskService.query_by_validator_id(Task,
                                                 validator_id).first() == task

    def test_filter_by_validator_id_w_custom_query(
            self,
            task,
            valid_status_log_entry,
    ):
        validator_id = valid_status_log_entry.user_id
        query = Task.query().filter_by(official_number='DONOTEXIST')
        assert TaskService.query_by_validator_id(Task, validator_id,
                                                 query=query).count() == 0
