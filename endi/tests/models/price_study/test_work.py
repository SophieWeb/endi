
def test_work_compute(mk_price_study_work, mk_price_study_work_item):
    work = mk_price_study_work(quantity=3.33)

    workitem1 = mk_price_study_work_item(
        supplier_ht=100000,
        _general_overhead=0.11,
        _margin_rate=0.12,
        work_unit_quantity=2,
    )

    workitem2 = mk_price_study_work_item(
        ht=100000,
        _general_overhead=0.11,
        _margin_rate=0.12,
        work_unit_quantity=2,
    )
    work.items.append(workitem1)
    work.items.append(workitem2)

    work.on_before_commit('add')


    assert work.flat_cost() == 666000
    assert int(work.unit_ht()) == 452272  # 452272.773
    assert int(work.compute_total_ht()) == 1506068  # 1506068,1818
