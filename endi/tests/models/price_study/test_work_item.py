
def test_work_item_compute(
    mk_price_study_work_item, mk_price_study_work, tva, mk_tva
):
    work = mk_price_study_work(quantity=3.33)

    work_item = mk_price_study_work_item(
        supplier_ht=100000,
        _general_overhead=0.11,
        _margin_rate=0.12,
        _tva=tva,
        work_unit_quantity=2,
        total_quantity=6.66
    )
    work_item.price_study_work = work
    work_item.price_study_work_id = work.id

    work_item.on_before_commit('add')
    assert work_item.flat_cost(unitary=True, work_level=False) == 100000
    assert work_item.flat_cost(unitary=True, work_level=True) == 200000
    assert work_item.flat_cost(unitary=False) == 666000

    assert int(work_item.cost_price(unitary=True)) == 111000
    assert int(work_item.cost_price(unitary=False)) == 739260

    assert int(work_item.intermediate_price(unitary=True)) == 126136
    assert int(work_item.intermediate_price(unitary=False)) == 840068

    assert int(work_item.unit_ht()) == 126136
    assert int(work_item.compute_work_unit_ht()) == 252272
    assert int(work_item.compute_total_ht()) == 840068

    # tva à 20% (fixture tva par défaut)
    assert int(work_item.compute_total_tva()) == 168013
    # Valeur imprécise, les arrondis se font au niveau de la facture
    assert int(work_item.ttc()) == 1008081

    new_tva = mk_tva(name="tva 2", value="1000")
    work.tva = new_tva

    # La tva est héritée
    assert int(work_item.compute_total_tva()) == 84006
    # Valeur imprécise, les arrondis se font au niveau de la facture
    assert int(work_item.ttc()) == 924075


def test_work_item_inheritance(
    mk_price_study_work_item, mk_price_study_work, tva, product,
    mk_tva, mk_product
):
    work = mk_price_study_work(quantity=3.33)

    work_item = mk_price_study_work_item(
        supplier_ht=100000,
        _general_overhead=0.11,
        _margin_rate=0.12,
        _tva=tva,
        _product=product,
        work_unit_quantity=2,
        total_quantity=6.66
    )
    work_item.price_study_work = work
    work_item.price_study_work_id = work.id

    assert work_item.tva_id_editable
    assert work_item.product_id_editable

    assert work_item.tva_id == tva.id
    assert work_item.product_id == product.id
    assert work_item.tva == tva
    assert work_item.product == product

    new_tva = mk_tva(name="tva 2", value="1000")
    work.tva = new_tva
    work.tva_id = new_tva.id
    new_product = mk_product(name="product 2", tva=new_tva)
    work.product = new_product
    work.product_id = new_product.id

    assert work_item.tva_id == new_tva.id
    assert work_item.product_id == new_product.id
    assert work_item.tva == new_tva
    assert work_item.product == new_product


def test_work_item_sync_quantities(
    mk_price_study_work_item, mk_price_study_work
):
    work = mk_price_study_work(quantity=3.33)

    work_item = mk_price_study_work_item(
        work_unit_quantity=2,
    )
    work_item.price_study_work = work
    work_item.price_study_work_id = work.id

    work_item.quantity_inherited = False
    work_item.sync_quantities()
    assert work_item.total_quantity == 2

    work_item.quantity_inherited = True
    work_item.sync_quantities()
    assert work_item.total_quantity == 6.66
