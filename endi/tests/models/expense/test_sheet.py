from endi.models.expense.sheet import (
    ExpenseKmLine,
    ExpenseSheet,
)


def test_expensekm_duplicate_ref_7774(mk_expense_type):
    kmtype = mk_expense_type(amount=1, year=2018)
    line = ExpenseKmLine(description="", km="", expense_type=kmtype)

    sheet = ExpenseSheet(year=2019, month=1)
    assert line.duplicate(sheet) is None

    kmtype2019 = mk_expense_type(amount=1, year=2019)
    assert line.duplicate(sheet).expense_type == kmtype2019


def test_expense_sheet_validation_sets_official_number(
        mk_expense_sheet,
        pyramid_request,
        csrf_request_with_db_and_user,
):
    from endi.models.config import Config
    Config.set('expensesheet_number_template', '{SEQGLOBAL}')
    expense_sheet_1 = mk_expense_sheet()
    expense_sheet_2 = mk_expense_sheet()

    assert expense_sheet_1.official_number is None
    assert expense_sheet_2.official_number is None

    expense_sheet_1.set_status('valid', csrf_request_with_db_and_user)
    assert expense_sheet_1.official_number == '1'

    expense_sheet_2.set_status('valid', csrf_request_with_db_and_user)
    assert expense_sheet_2.official_number == '2'

    # https://framagit.org/endi/endi/-/issues/2596
    Config.set('expensesheet_number_template', '{SEQGLOBAL}-{YYYY}-{YY}-{MM}')
    expense_sheet_3 = mk_expense_sheet(year=2021, month=2)
    expense_sheet_3.set_status('valid', csrf_request_with_db_and_user)
    assert expense_sheet_3.official_number == '3-2021-21-02'
