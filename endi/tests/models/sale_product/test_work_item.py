
def test_work_item_locked(
    dbsession,
    mk_sale_product_work, mk_sale_product_work_item, mk_sale_product, mk_product, mk_tva
):
    sale_product_work = mk_sale_product_work()
    base_product = mk_sale_product(
        supplier_ht=1,
        general_overhead=0.11,
        margin_rate=0.15,
        unity="mot",
    )
    tva = mk_tva(name='test', value=1000)
    product = mk_product(name="test1", tva=tva)
    item = mk_sale_product_work_item(
        locked=True,
        _supplier_ht=2,
        _general_overhead=0.12,
        _margin_rate=0.16,
        _unity="feuille",
        _tva_id=tva.id,
        _product_id=product.id,
        base_sale_product_id=base_product.id,
        sale_product_work_id=sale_product_work.id
    )
    assert item.supplier_ht == 1
    assert item.general_overhead == 0.11
    assert item.margin_rate == 0.15
    assert item.unity == 'mot'
    assert item.tva_id == base_product.tva_id
    assert item.product_id == base_product.product_id

    # If not locked, we use the _ prefixed attributes
    item.locked = False
    assert item.supplier_ht == 2
    assert item.general_overhead == 0.12
    assert item.margin_rate == 0.16
    assert item.unity == 'feuille'
    assert item.tva_id == item._tva_id
    assert item.product_id == item._product_id

    # Test editable state
    item.locked = True
    assert not item.supplier_ht_editable
    assert not item.ht_editable
    assert not item.general_overhead_editable
    assert not item.margin_rate_editable
    assert not item.unity_editable
    assert not item.tva_id_editable
    assert not item.product_id_editable

    for key in (
        'general_overhead', 'margin_rate', 'tva_id', 'product_id'
    ):
        setattr(base_product, key, None)
        dbsession.merge(base_product)
        dbsession.flush()
        assert getattr(item, '%s_editable' % key)

    # Special cases for supplier_ht/ht
    base_product.supplier_ht = None
    assert item.supplier_ht_editable
    assert item.ht_editable
    base_product.ht = 15
    assert item.ht == 15
    assert not item.supplier_ht_editable
    assert not item.ht_editable


def test_work_item_work_inheritance(
    mk_sale_product_work, mk_sale_product_work_item, mk_sale_product, mk_product, mk_tva,
    tva, product
):
    sale_product_work = mk_sale_product_work()
    base_product = mk_sale_product(
        supplier_ht=1,
        tva_id=None,
        product_id=None
    )
    new_tva = mk_tva(name='test', value=1000)
    new_product = mk_product(name="test1", tva=tva)
    item = mk_sale_product_work_item(
        locked=True,
        _supplier_ht=2,
        _general_overhead=0.12,
        _margin_rate=0.16,
        _unity="feuille",
        _tva_id=new_tva.id,
        _product_id=new_product.id,
        base_sale_product_id=base_product.id,
        sale_product_work_id=sale_product_work.id
    )
    assert item.general_overhead == 0.12
    assert item.margin_rate == 0.16
    assert item.tva_id == new_tva.id
    assert item.product_id == new_product.id

    sale_product_work.tva_id = tva.id
    assert item.tva_id == tva.id

    sale_product_work.product_id = product.id
    assert item.product_id == product.id

    sale_product_work.general_overhead = 0.1
    assert item.general_overhead == 0.1

    sale_product_work.margin_rate = 0.2
    assert item.margin_rate == 0.2


def test_work_item_compute(
    mk_sale_product_work, mk_sale_product_work_item, mk_sale_product, tva, product
):
    sale_product_work = mk_sale_product_work()

    base_product = mk_sale_product(
        supplier_ht=100000,
        general_overhead=0.11,
        margin_rate=0.12,
        unity="mot",
    )
    assert int(base_product.unit_ht()) == 126136

    item = mk_sale_product_work_item(
        locked=True,
        quantity=2,
        base_sale_product_id=base_product.id,
        sale_product_work_id=sale_product_work.id
    )

    assert int(item.flat_cost()) == 200000
    assert int(item.flat_cost(unitary=True)) == 100000
    assert int(item.cost_price()) == 222000
    assert int(item.cost_price(unitary=True)) == 111000
    assert int(item.intermediate_price()) == 252272
    assert int(item.intermediate_price(unitary=True)) == 126136
    assert int(item.unit_ht()) == 126136
    assert int(item.compute_total_ht()) == 252272

    assert int(item.unit_ht(contribution=10)) == 140151
    # Ref #1877
    assert int(item.unit_ht(contribution=10.0)) == 140151

    item.sync_amounts()


    assert int(sale_product_work.flat_cost()) == 200000
    assert int(sale_product_work.unit_ht()) == 252272
    assert int(sale_product_work.ttc()) == 302727

    assert int(item.total_ht) == 252272
    assert int(item._ht) == 126136
    assert int(sale_product_work.ht) == 252272


def test_work_item_on_before_commit(
    mk_sale_product_work, mk_sale_product_work_item, mk_sale_product, tva, product,
    mk_tva
):
    sale_product_work = mk_sale_product_work()

    base_product = mk_sale_product()

    new_tva = mk_tva(value=22, name='test')
    item = mk_sale_product_work_item(
        locked=False,
        _tva_id=new_tva.id,
        _product_id=product.id,
        base_sale_product_id=base_product.id,
        sale_product_work_id=sale_product_work.id
    )
    item.on_before_commit('add')
    assert item.product_id is None
