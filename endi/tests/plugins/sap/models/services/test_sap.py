import pytest

from endi.plugins.sap.models.services.attestation import SAPAttestationLineService

pytestmark = [pytest.mark.plugin_sap]


@pytest.fixture
def overpaid_invoice_w_discount(
        customer_1,
        dbsession,
        discount_line,
        mk_fully_paid_invoice,
):
    invoice = mk_fully_paid_invoice(customer=customer_1)
    invoice.discounts.append(discount_line)  # - 12€TTC
    # there is now an overpayment of 12€
    dbsession.merge(invoice)
    dbsession.flush()
    return invoice

@pytest.fixture
def underpaid_invoice_w_expenses(
        customer_1,
        dbsession,
        discount_line,
        mk_fully_paid_invoice,
        mk_task_line,
):
    invoice = mk_fully_paid_invoice(customer=customer_1)
    # 2 expenses of 12€TTC and 6€TTC
    l1 = mk_task_line(cost=500000, unity='km')
    l2 = mk_task_line(cost=1000000, unity='km')
    invoice.line_groups[0].lines = invoice.line_groups[0].lines + [l1, l2]
    dbsession.merge(invoice)
    dbsession.flush()
    return invoice

@pytest.fixture
def fully_paid_invoice_w_expenses(
    dbsession,
    date_20190101,
    mk_payment,
    underpaid_invoice_w_expenses
):
     invoice = underpaid_invoice_w_expenses
     invoice.payments = invoice.payments + [
         mk_payment(date=date_20190101, amount=1800000)
     ]
     dbsession.merge(invoice)
     dbsession.flush()
     return invoice

@pytest.fixture
def overpaid_invoice_w_negative_task_line(
        dbsession,
        customer_2,
        mk_fully_paid_invoice,
        mk_task_line
):
    invoice = mk_fully_paid_invoice(customer=customer_2)
    invoice.line_groups[0].lines.append(
        mk_task_line(cost=-500000)  # - 6€TTC
    )
    # there is now an overpayment of 6€
    dbsession.merge(invoice)
    dbsession.flush()
    return invoice


def test_get_regular_tasklines_no_enough_data():
    with pytest.raises(ValueError):
        list(SAPAttestationLineService().query(year=None))


def test_get_regular_tasklines_excluded(
        draft_invoice,
        unpaid_invoice,
):
    lines = SAPAttestationLineService().query(2019,set(),set())
    assert len(list(lines)) == 0

def test_get_alien_invoices_negative_taskline(
        overpaid_invoice_w_negative_task_line,
):
    query = SAPAttestationLineService()._get_invoices(2019, set(), set())
    assert query.count() == 1


def test_get_alien_invoices(partly_paid_invoice):
    query = SAPAttestationLineService()._get_invoices(year=2019,
                                                      companies_ids=set(),
                                                      customers_ids=set())
    assert query.count() == 1


def test_get_alien_invoices_discount(overpaid_invoice_w_discount):
    query = SAPAttestationLineService()._get_invoices(2019, set(), set())
    assert query.count() == 1


def test_query(fully_paid_invoice):
    sap_lines = list(SAPAttestationLineService().query(year=2019))
    assert len(sap_lines) == 1
    assert sap_lines[0].amount == 12000000


def test_query_w_expenses(underpaid_invoice_w_expenses):
    sap_lines = list(SAPAttestationLineService().query(year=2019))
    assert len(sap_lines) == 3
    service_line, expense_line, epsilon_line = sap_lines
    assert sum(i.amount for i in sap_lines) == 12000000

    assert service_line.unit == 'h'
    assert expense_line.unit == 'frais'
    assert epsilon_line.unit == 'h'


def test_query_w_expenses_fully_paid(fully_paid_invoice_w_expenses):
    sap_lines = list(SAPAttestationLineService().query(year=2019))
    assert len(sap_lines) == 2
    service_line, expense_line = sap_lines
    assert service_line.amount == 12000000
    assert expense_line.amount == 1800000


def test_query_partly_paid_invoice(partly_paid_invoice):
    sap_lines = list(SAPAttestationLineService().query(year=2019))
    assert len(sap_lines) == 2
    assert sap_lines[0].amount == 400000
    assert sap_lines[1].amount == 800000


def test_query_overpaid_invoice_w_discount(overpaid_invoice_w_discount):
    sap_lines = list(SAPAttestationLineService().query(year=2019))
    assert len(sap_lines) == 1
    assert sap_lines[0].amount == 10800000


def test_query_overpaid_invoice_w_negative_task_line(
        overpaid_invoice_w_negative_task_line,
):
    sap_lines = list(SAPAttestationLineService().query(year=2019))
    assert len(sap_lines) == 1
    assert sap_lines[0].amount == 11400000


def test_query_complex(
        fully_paid_invoice,                     # 120€
        overpaid_invoice_w_discount,            # 108€
        overpaid_invoice_w_negative_task_line,  # 114€
        partly_paid_invoice,                    # 4€ + 8€
):
    lines = list(SAPAttestationLineService().query(year=2019))
    lines_amounts = [line.amount for line in lines]
    lines_amounts.sort()

    assert lines_amounts == [
        400000,
        800000,
        10800000,
        11400000,
        12000000,
    ]


def test_query_payment_on_different_year(
    date_20210101,
    dbsession,
    fully_paid_invoice,
):
    fully_paid_invoice.payments[0].date = date_20210101
    dbsession.merge(fully_paid_invoice.payments[0])
    dbsession.flush()

    # 2019
    sap_lines = list(SAPAttestationLineService().query(year=2019))
    assert len(sap_lines) == 1
    assert sap_lines[0].amount == 12000000

    # 2020
    sap_lines = list(SAPAttestationLineService().query(year=2020))
    assert len(sap_lines) == 0

    # 2021
    sap_lines = list(SAPAttestationLineService().query(year=2021))
    assert len(sap_lines) == 0


@pytest.fixture
def fully_paid_invoice_multitva_payments(
    bank_remittance,
    customer_2,
    date_20210101,
    dbsession,
    invoice_multitva_payments,
    mk_payment,
):
    payment = mk_payment(
        amount=87405000 - 100000,  # 100% - 1€
        bank_remittance_id="REM_ID",
        exported=1,
        date=date_20210101,
    )
    invoice_multitva_payments.payments = [payment]
    invoice_multitva_payments.customer = customer_2
    dbsession.merge(invoice_multitva_payments)
    dbsession.flush()
    return invoice_multitva_payments


def test_query_ignored(fully_paid_invoice_multitva_payments):
    line_service = SAPAttestationLineService()
    lines = list(line_service.query(year=2021))
    assert len(lines) == 0
    rejects = line_service.get_rejects()
    assert len(rejects) == 1
    assert rejects[0].invoice == fully_paid_invoice_multitva_payments
    assert 'TVA' in rejects[0].msg
