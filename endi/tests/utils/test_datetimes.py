import datetime
import pytest


def test_parse_date():
    from endi.utils.datetimes import parse_date

    assert parse_date("2019-01-03") == datetime.date(2019, 1, 3)
    assert parse_date("2019-03-01", format_="%Y-%d-%m") == datetime.date(
        2019, 1, 3
    )

    assert parse_date("toto", default="") == ""

    with pytest.raises(ValueError):
        parse_date("toto")
