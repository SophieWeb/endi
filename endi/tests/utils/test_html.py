"""
Test for the forms package
"""
import colander
from endi.utils.html import (
    strip_whitespace,
    strip_linebreaks,
    strip_void_lines,
    clean_html,
)


def test_strip_whitespace():
    value = "toto"
    assert strip_whitespace(value) == value
    value = "     toto\t   "
    assert strip_whitespace(value) == 'toto'
    value = "   to    toto \t toto"
    assert strip_whitespace(value) == "to    toto \t toto"
    assert strip_whitespace(None) is None
    assert strip_whitespace(colander.null) == colander.null


def test_strip_linebreaks():
    value = "\n toto \n <br /><br><br/>"
    assert strip_linebreaks(value) == "toto"
    assert strip_linebreaks(None) is None
    assert strip_linebreaks(colander.null) == colander.null


def test_strip_void_lines():
    value = "<div></div><p>toto</p><p> </p>"
    assert strip_void_lines(value) == "<div></div><p>toto</p>"


def test_clean_html():
    value = ("<span "
    "style=\"font-family: 'AGaramondPro'; color: rgb(0.217500%, "
    "0.530000%, 0.950000%);\">Test</span>")

    assert clean_html(value) == (
        "<span style=\"font-family:"
        " 'AGaramondPro';\">Test</span>")
