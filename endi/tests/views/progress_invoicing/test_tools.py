from endi.views.progress_invoicing.tools import (
    AbstractProgressTaskLine,
)


class TestAbstractProgressTaskLine:
    def test__get_current_value(
        self,
        progress_business,
        progress_invoice,
        user,
    ):
        new_invoice = progress_business.add_progress_invoicing_invoice(user)
        group_status = progress_business.progress_invoicing_group_statuses[0]
        status = group_status.line_statuses[0]

        progress_business.populate_progress_invoicing_lines(
            new_invoice,
            {group_status.id: {status.id: 15}}
        )
        item = AbstractProgressTaskLine(status, new_invoice.id)
        assert item._get_current_value() == 15.0

    def test__get_percent_left(
        self, progress_business, progress_invoice, user
    ):
        new_invoice = progress_business.add_progress_invoicing_invoice(user)
        group_status = progress_business.progress_invoicing_group_statuses[0]
        status = group_status.line_statuses[0]

        progress_business.populate_progress_invoicing_lines(
            new_invoice,
            {group_status.id: {status.id: 15}}
        )
        item = AbstractProgressTaskLine(status, new_invoice.id)
        assert item._get_percent_left() == 90
