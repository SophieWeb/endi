
class TestPdfFromHtmlService:
    def _init_service(
        self, context, get_csrf_request_with_db, user, pdf_config
    ):
        from endi.views.task.pdf_rendering_service import (
            TaskPdfFromHtmlService,
        )

        request = get_csrf_request_with_db(
            request_config=pdf_config, user=user, context=context
        )
        service = TaskPdfFromHtmlService(context, request)
        return service

    def _duplicate_invoice(self, dbsession, invoice, user, mk_business_type):
        result = invoice.duplicate(
            user,
            project=invoice.project,
            customer=invoice.customer,
        )
        result.business_type = mk_business_type("training")
        dbsession.add(result)
        dbsession.flush()
        return result

    def test_render_returns_pdf(
        self, full_invoice, get_csrf_request_with_db, user, pdf_config,
        dbsession, mk_business_type,
    ):
        service = self._init_service(
            full_invoice, get_csrf_request_with_db, user, pdf_config
        )
        full_invoice_training = self._duplicate_invoice(
            dbsession, full_invoice, user, mk_business_type
        )
        pdf = service.render()
        assert hasattr(pdf, "seek")

    def test_get_facturx_xml(
        self, full_invoice, get_csrf_request_with_db, user, pdf_config
    ):
        from facturx import check_facturx_xsd
        service = self._init_service(
            full_invoice, get_csrf_request_with_db, user, pdf_config
        )
        xml_string = service._get_facturx_xml()
        assert check_facturx_xsd(xml_string)
