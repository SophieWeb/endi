from pytest import fixture

from endi.views.price_study.rest_api import RestPriceStudyView
from endi.views.price_study.rest_api import RestPriceStudyProductView
from endi.views.price_study.rest_api import RestWorkItemView
from endi.views.price_study.rest_api import RestPriceStudyDiscountView


@fixture
def full_price_study(
    price_study,
    mk_price_study_product,
    mk_price_study_work,
    mk_price_study_work_item,
    tva,
    product
):
    ps_product = mk_price_study_product(
        description="Description",
        ht=100000,
        quantity=5,
        study=price_study,
        tva_id=tva.id,
    )
    price_study_work = mk_price_study_work(
        title="Title",
        description="Description",
        study=price_study,
        quantity=5
    )
    work_item = mk_price_study_work_item(
        ht=100000,
        _tva=tva,
        work_unit_quantity=2,
        price_study_work=price_study_work
    )
    work_item.on_before_commit('add')
    ps_product.on_before_commit('add')
    price_study.sync_amounts()
    return price_study


class TestRestPriceStudyView:

    def test_edit_price_study(
        self, get_csrf_request_with_db, mk_price_study
    ):
        price_study = mk_price_study(name="Name")
        request = get_csrf_request_with_db(
            post={'name': 'New name', 'notes': 'New notes'}
        )

        view = RestPriceStudyView(price_study, request)
        result = view.put()
        result = result.__json__(request)

        assert result['name'] == 'New name'
        assert result['notes'] == 'New notes'


class TestRestPriceStudyProductView:

    def test_add_product(
        self, get_csrf_request_with_db, mk_price_study, tva, product
    ):
        price_study = mk_price_study(name="Name")
        request = get_csrf_request_with_db(
            post={
                'type_': 'price_study_product',
                'description': "Description",
                'quantity': 5,
                'tva_id': tva.id,
                'product_id': product.id,
                'supplier_ht': 1,
                'general_overhead': 0.11,
                'margin_rate': 0.12,
            }
        )

        view = RestPriceStudyProductView(price_study, request)
        result_obj = view.post()
        result = result_obj.__json__(request)
        assert result['type_'] == 'price_study_product'
        assert result['id'] is not None
        assert result['description'] == 'Description'
        assert result['quantity'] == 5
        assert result['base_sale_product_id'] is None
        assert result['product_id'] == product.id
        assert result['tva_id'] == tva.id
        # Check amounts where synced
        assert result['ht'] == 1.26136
        assert result['total_ht'] == 6.30682
        # Check parent amounts were synced
        assert int(price_study.ht) == 631000

    def test_add_work(
        self, mk_price_study, mk_price_study_work, get_csrf_request_with_db,
    ):
        price_study = mk_price_study(name="Name")
        from endi.views.price_study.rest_api import RestPriceStudyProductView
        request = get_csrf_request_with_db(
            post={
                'type_': 'price_study_work',
                'title': 'Title',
                'description': "Description",
            }
        )
        view = RestPriceStudyProductView(price_study, request)

        result = view.post()
        result = result.__json__(request)

        assert result['type_'] == 'price_study_work'
        assert result['id'] is not None
        assert result['description'] == 'Description'
        assert result['title'] == 'Title'

    def test_edit_product(
        self,
        mk_price_study,
        mk_price_study_product,
        get_csrf_request_with_db,
        tva,
        product
    ):
        price_study = mk_price_study(name="Name")
        price_study_product = mk_price_study_product(
            description="Description",
            quantity=5,
            tva_id=tva.id,
            product_id=product.id,
            supplier_ht=1,
            general_overhead=0.11,
            margin_rate=0.12,
            study=price_study,
        )
        request = get_csrf_request_with_db(
            post={
                'description': "New description",
                'quantity': 10,
                'supplier_ht': 2,
                'general_overhead': 0.11,
                'margin_rate': 0.12,
            }
        )

        view = RestPriceStudyProductView(price_study_product, request)
        result = view.put()
        result = result.__json__(request)

        assert result['type_'] == 'price_study_product'
        assert result['id'] == price_study_product.id
        assert result['description'] == 'New description'
        assert result['quantity'] == 10
        assert result['base_sale_product_id'] is None
        assert result['tva_id'] == tva.id
        # Check amounts where synced
        assert result['ht'] == 2.52273
        assert result['total_ht'] == 25.22727

    def test_edit_work(
        self,
        mk_price_study,
        mk_price_study_work,
        mk_price_study_work_item,
        get_csrf_request_with_db,
        tva, product,
        mk_tva,
    ):
        price_study = mk_price_study(name="Name")
        price_study_work = mk_price_study_work(
            title="Title",
            description="Description",
            study=price_study,
        )
        work_item = mk_price_study_work_item(
            supplier_ht=100000,
            _general_overhead=0.11,
            _margin_rate=0,
            _tva=tva,
            _product=product,
            work_unit_quantity=2,
            total_quantity=6.66,
            price_study_work=price_study_work
        )

        new_tva = mk_tva(name="tva 2", value=1000)
        request = get_csrf_request_with_db(
            post={
                'title': 'New title',
                'description': "New description",
                'quantity': 10,
                'supplier_ht': 2,
                'general_overhead': "",
                'margin_rate': 0.12,
                'tva_id': new_tva.id
            }
        )

        view = RestPriceStudyProductView(price_study_work, request)
        result = view.put()
        result = result.__json__(request)

        assert result['type_'] == 'price_study_work'
        assert result['id'] == price_study_work.id
        assert result['description'] == 'New description'
        assert result['title'] == 'New title'
        assert result['quantity'] == 10
        assert result['sale_product_work_id'] is None
        assert result['tva_id'] == new_tva.id
        # Test encure_tva
        assert result['product_id'] is None
        assert work_item.product_id is None
        assert work_item.tva == new_tva
        assert result['general_overhead'] is None
        assert result['margin_rate'] == 0.12
        # Check amounts where synced
        assert result['ht'] == 2.52273
        assert result['total_ht'] == 25.22727

    def test_product_load_from_catalog(
        self,
        price_study,
        get_csrf_request_with_db,
        mk_sale_product,
        tva,
        product,
    ):
        sale_product = mk_sale_product(
            supplier_ht=100000,
            general_overhead=0.11,
            margin_rate=0.15,
            unity="unity",
        )

        request = get_csrf_request_with_db(
            post={
                'sale_product_ids': [sale_product.id]
            }
        )

        view = RestPriceStudyProductView(price_study, request)
        result = view.load_from_catalog_view()
        result = result[0].__json__(request)

        assert result['type_'] == 'price_study_product'
        assert result['base_sale_product_id'] == sale_product.id
        assert result['supplier_ht'] == 1
        assert result['quantity'] == 1
        assert result['general_overhead'] == 0.11
        assert result['margin_rate'] == 0.15
        assert result['unity'] == "unity"
        assert result['ht'] == 1.30588
        assert result['total_ht'] == 1.30588

    def test_work_load_from_catalog(
        self,
        dbsession,
        price_study,
        get_csrf_request_with_db,
        mk_sale_product_work,
        mk_sale_product_work_item,
        tva,
        product,
    ):
        sale_product_work = mk_sale_product_work(
            title="Title",
            description="Description",
            tva_id=tva.id,
            product_id=product.id,
            general_overhead=0.11,
            margin_rate=0.15,
        )

        mk_sale_product_work_item(
            sale_product_work=sale_product_work,
            _supplier_ht=100000,
            _general_overhead=0.11,
            _margin_rate=0.12,
        )
        print(sale_product_work.items)
        request = get_csrf_request_with_db(
            post={
                'sale_product_ids': [sale_product_work.id]
            }
        )

        view = RestPriceStudyProductView(price_study, request)
        result = view.load_from_catalog_view()
        result = result[0].__json__(request)

        assert result['type_'] == 'price_study_work'
        assert result['sale_product_work_id'] == sale_product_work.id
        assert result['general_overhead'] == 0.11
        assert result['margin_rate'] == 0.15
        assert result['tva_id'] == tva.id
        assert result['product_id'] == product.id
        # Computed values
        assert result['ht'] == 1.30588
        assert result['total_ht'] == 1.30588

    def test_product_delete(
        self,
        price_study,
        get_csrf_request_with_db,
        mk_sale_product,
        mk_price_study_product,
        tva,
    ):
        price_study_product = mk_price_study_product(
            description="Description",
            quantity=5,
            supplier_ht=100000,
            general_overhead=0.11,
            margin_rate=0.12,
            study=price_study,
            tva_id=tva.id,
        )
        request = get_csrf_request_with_db()
        price_study_product.on_before_commit('add')
        # Check total is set
        assert price_study.ht == 631000
        view = RestPriceStudyProductView(price_study_product, request)
        view.delete()
        # Check it's set to 0
        assert price_study.ht == 0

    def test_percent_discount_refresh(
        self,
        full_price_study,
        get_csrf_request_with_db,
        mk_price_study_discount,
        tva,
    ):
        discount = mk_price_study_discount(
            description='Description',
            type_='percentage',
            percentage=5,
            tva=tva,
            price_study=full_price_study,
        )
        discount.on_before_commit('add')

        request = get_csrf_request_with_db(
            post={
                'ht': 2,
            }
        )
        # On édite le premier produit
        view = RestPriceStudyProductView(full_price_study.products[0], request)
        view.put()

        # On vérifie que la remise et les totaux de l’étude sont justes
        assert discount.total_ht() == 100000
        assert full_price_study.ht == 1900000


class TestRestWorkItemView:
    def test_load_from_catalog_view(
        self,
        dbsession,
        price_study,
        get_csrf_request_with_db,
        mk_sale_product,
        mk_price_study_work,
        tva,
        product,
    ):
        price_study_work = mk_price_study_work(
            title="Title",
            description="Description",
            study=price_study,
            quantity=5
        )
        sale_product = mk_sale_product(
            supplier_ht=100000,
            general_overhead=0.11,
            margin_rate=0.15,
            unity="unity",
        )

        request = get_csrf_request_with_db(post={
            'sale_product_ids': [sale_product.id]
        })
        view = RestWorkItemView(price_study_work, request)
        result = view.load_from_catalog_view()
        result = result[0].__json__(request)

        from endi.models.price_study.work import PriceStudyWork
        assert len(PriceStudyWork.get(price_study_work.id).items) == 1
        assert len(price_study_work.items) == 1
        assert result['margin_rate'] == 0.15
        assert result['general_overhead'] == 0.11
        assert result['unity'] == "unity"
        assert result['supplier_ht'] == 1


class TestRestPriceStudyDiscountView:
    def test_add_discount(
        self,
        full_price_study,
        get_csrf_request_with_db,
        tva,
    ):
        assert full_price_study.ht == 1500000

        request = get_csrf_request_with_db(
            post={
                'type_': 'amount',
                'description': "Description",
                'amount': 1,
                'tva_id': tva.id
            }
        )
        view = RestPriceStudyDiscountView(full_price_study, request)
        result = view.post()

        assert result.amount == 100000
        assert full_price_study.ht == 1400000

    def test_edit_discount(
        self,
        full_price_study,
        get_csrf_request_with_db,
        mk_price_study_discount,
        tva,
    ):
        discount = mk_price_study_discount(
            description='Description',
            type_='amount',
            amount=100000,
            tva=tva,
            price_study=full_price_study,
        )
        discount.on_before_commit('add')

        request = get_csrf_request_with_db(
            post={
                'description': "New description",
                'percentage': 5,
                'type_': 'percentage',
                'tva_id': tva.id
            }
        )
        view = RestPriceStudyDiscountView(discount, request)
        result = view.put()

        assert result.percentage == 5
        assert result.total_ht() == 75000

        assert full_price_study.ht == 1425000

