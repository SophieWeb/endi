import colander

from endi.models.third_party.customer import Customer
from endi.views.third_party.customer.views import (
    CustomerAdd,
    CustomerEdit,
    customer_delete,
    customer_archive,
)
from endi.tests.base import Dummy


COMPANY_APPSTRUCT = {
    'company_name': 'Company',
    'lastname': 'Lastname',
    'address': 'Address',
    'zip_code': "21000",
    "city": "Dijon",
    'compte_cg': "Compte CG1515",
    'compte_tiers': "Compte Tiers",
    'code': 'CODE'
}


INDIVIDUAL_APPSTRUCT = {
    'lastname': 'Lastname 2',
    'firstname': 'FirstName',
    'address': 'Address',
    'zip_code': "21000",
    "city": "Dijon",
    'compte_cg': "Compte CG1515",
    'compte_tiers': "Compte Tiers",
    'code': 'CODE'
}


def get_company_customer():
    return Customer.query().filter(Customer.company_name == 'Company').one()


def get_individual_customer():
    return Customer.query().filter(Customer.lastname == 'Lastname 2').one()


class TestCustomerAdd():

    def test_form_label(self, get_csrf_request_with_db):
        pyramid_request = get_csrf_request_with_db(
            post={'__formid__': 'test'}
        )
        view = CustomerAdd(pyramid_request)
        assert view.form_label() == 'test'

    def test_schema(self, get_csrf_request_with_db):
        pyramid_request = get_csrf_request_with_db(
            post={'__formid__': 'company'}
        )
        view = CustomerAdd(pyramid_request)
        schema = view.schema
        assert schema['company_name'].missing == colander.required

    def test_submit_company_success(
        self, config, get_csrf_request_with_db, company
    ):
        config.add_route('customer', '/')
        pyramid_request = get_csrf_request_with_db(
            post={'__formid__': 'company'}
        )
        pyramid_request.context = company

        view = CustomerAdd(pyramid_request)
        view.submit_success(COMPANY_APPSTRUCT)
        customer = get_company_customer()
        for key, value in list(COMPANY_APPSTRUCT.items()):
            assert getattr(customer, key) == value
        assert customer.type == 'company'
        assert customer.company == company

    def test_submit_individual_success(
        self, config, get_csrf_request_with_db, company
    ):
        config.add_route('customer', '/')
        pyramid_request = get_csrf_request_with_db(
            post={'__formid__': 'individual'}
        )
        pyramid_request.context = company

        view = CustomerAdd(pyramid_request)
        view.submit_success(INDIVIDUAL_APPSTRUCT)
        customer = get_individual_customer()
        for key, value in list(INDIVIDUAL_APPSTRUCT.items()):
            assert getattr(customer, key) == value
        assert customer.type == 'individual'
        assert customer.company == company
        assert customer.label == customer._get_label()


class TestCustomerEdit():

    def test_customer_edit(self, config, customer, get_csrf_request_with_db):
        config.add_route('customer', '/')
        req = get_csrf_request_with_db()
        req.context = customer
        req.user = Dummy()

        appstruct = COMPANY_APPSTRUCT.copy()
        appstruct['lastname'] = "Changed Lastname"
        appstruct['compte_cg'] = "1"
        appstruct['compte_tiers'] = "2"
        view = CustomerEdit(req)
        view.submit_success(appstruct)
        customer = get_company_customer()
        assert customer.lastname == 'Changed Lastname'
        assert customer.compte_cg == "1"
        assert customer.compte_tiers == "2"
        assert customer.label == customer._get_label()


def test_customer_delete(customer, get_csrf_request_with_db):
    req = get_csrf_request_with_db()
    cid = customer.id
    req.context = customer
    req.referer = '/'
    customer_delete(req)
    req.dbsession.flush()
    assert Customer.get(cid) is None


def test_customer_archive(customer, get_csrf_request_with_db):
    req = get_csrf_request_with_db()
    cid = customer.id
    req.context = customer
    req.referer = '/'
    customer_archive(req)
    req.dbsession.flush()
    assert Customer.get(cid).archived
    customer_archive(req)
    req.dbsession.flush()
    assert Customer.get(cid).archived is False
