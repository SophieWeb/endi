import pytest


@pytest.fixture
def full_cancelinvoice(
    dbsession, mk_cancelinvoice, full_invoice, task_line_group, task_line,
    user,
    mention, discount_line, date_20190101, mk_task_line_group, mk_task_line
):
    cancelinvoice = mk_cancelinvoice()
    # TTC  : -1 * (120 - 12  + 12 €)
    cancelinvoice.invoice_id = full_invoice.id
    group = mk_task_line_group()
    mk_task_line(
        cost=-1 * task_line.cost, group=group, tva=task_line.tva
    )
    cancelinvoice.line_groups = [group]

    cancelinvoice.discounts = [discount_line]
    cancelinvoice.workplace = 'workplace'
    cancelinvoice.mentions = [mention]
    cancelinvoice.expenses_ht = 1000000
    cancelinvoice.payment_conditions = "Test"
    cancelinvoice.description = "Description"
    cancelinvoice.start_date = date_20190101
    cancelinvoice = dbsession.merge(cancelinvoice)
    dbsession.flush()
    return cancelinvoice
