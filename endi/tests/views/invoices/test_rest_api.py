from endi.views.project.routes import PROJECT_ITEM_INVOICE_ROUTE


def test_invoice_valid_view(
    config, get_csrf_request_with_db, full_invoice, user
):
    config.add_route(PROJECT_ITEM_INVOICE_ROUTE, PROJECT_ITEM_INVOICE_ROUTE)
    config.testing_securitypolicy(
        userid="test",
        identity='admin',
        permissive=True
    )
    from endi.views.invoices.rest_api import InvoiceStatusRestView

    request = get_csrf_request_with_db(
        post={'submit': 'valid', 'comment': "Test comment"},
        context=full_invoice,
    )
    request.user = user
    request.is_xhr = True

    view = InvoiceStatusRestView(request)
    result = view.__call__()
    assert result == {
        'redirect': PROJECT_ITEM_INVOICE_ROUTE.format(
            id=full_invoice.project_id
        )
    }
    assert full_invoice.status == 'valid'
    assert full_invoice.statuses[-1].comment == "Test comment"
    assert full_invoice.statuses[-1].status == 'valid'


def test_invoice_datechange_callback(
    dbsession, config, get_csrf_request_with_db, full_invoice, user
):
    import datetime
    from endi.views.invoices.rest_api import InvoiceRestView

    config.add_route(PROJECT_ITEM_INVOICE_ROUTE, PROJECT_ITEM_INVOICE_ROUTE)

    full_invoice.financial_year = 2015
    full_invoice.date = datetime.date(2015, 1, 1)
    dbsession.merge(full_invoice)

    request = get_csrf_request_with_db(
        post={'date': '2016-01-01'},
        context=full_invoice,
    )
    request.user = user
    request.is_xhr = True

    view = InvoiceRestView(request)
    view.put()

    assert full_invoice.financial_year == 2016


def test_cancelinvoice_valid_view(
    config, get_csrf_request_with_db, full_cancelinvoice, full_invoice, user
):
    config.add_route(PROJECT_ITEM_INVOICE_ROUTE, PROJECT_ITEM_INVOICE_ROUTE)
    config.testing_securitypolicy(
        userid="test",
        identity='admin',
        permissive=True
    )
    from endi.views.invoices.rest_api import CancelInvoiceStatusRestView

    request = get_csrf_request_with_db(
        post={'submit': 'valid', 'comment': "Test comment"},
        context=full_cancelinvoice,
    )
    request.user = user
    request.is_xhr = True

    view = CancelInvoiceStatusRestView(request)
    result = view.__call__()
    assert result == {
        'redirect': PROJECT_ITEM_INVOICE_ROUTE.format(
            id=full_cancelinvoice.project_id
        )
    }
    assert full_cancelinvoice.status == 'valid'
    assert full_cancelinvoice.statuses[-1].comment == "Test comment"
    assert full_cancelinvoice.statuses[-1].status == 'valid'
    assert full_invoice.paid_status == 'resulted'
