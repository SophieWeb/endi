import pytest

from endi.models.config import (
    get_config,
)
from endi.views.admin.supplier.accounting import (
    SUPPLIER_ACCOUNTING_URL,
)
from endi.views.admin.supplier.accounting.supplier_invoice import (
    SupplierAccountingConfigView,
)
pytest.mark.usefixtures("config")


def test_config_cae_success(config, dbsession, get_csrf_request_with_db):

    SupplierAccountingConfigView.back_link = SUPPLIER_ACCOUNTING_URL

    appstruct = {
        'cae_general_supplier_account': "00000111",
        'cae_third_party_supplier_account': "000002222",
    }
    view = SupplierAccountingConfigView(get_csrf_request_with_db())
    view.submit_success(appstruct)
    config = get_config()
    for key, value in list(appstruct.items()):
        assert config[key] == value
