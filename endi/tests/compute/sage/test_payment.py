import datetime
import pytest
from endi.compute.sage.payment import (
    SagePaymentMain,
    SagePaymentTva,
    SagePaymentRemittance,
    InternalSagePaymentMain,
)


@pytest.fixture
def remittance(bank, mk_bank_remittance):
    return mk_bank_remittance(
        id="REM_ID",
        payment_mode="chèque",
        remittance_date=datetime.date.today(),
        closed=1,
        bank=bank,
        bank_id=bank.id,
    )


@pytest.fixture
def internalinvoice(
    dbsession,
    mk_internalinvoice,
    mk_product,
    mk_tva,
    mk_task_line,
    mk_discount_line,
    mk_task_line_group,
    customer,
    company,
):
    tva = mk_tva(name="test", value=0)
    product = mk_product(
        tva=tva,
        name="interne",
        internal=True,
        compte_cg="70400000",
    )
    line = mk_task_line(
        description="presta", cost=100000000, quantity=1, tva=tva.value,
        product=product
    )
    customer.compte_cg = '41100000'
    dbsession.merge(customer)
    dbsession.flush()
    invoice = mk_internalinvoice(customer=customer, company=company)
    invoice.official_number = 'INV_I01'
    invoice.date = datetime.date(2013, 2, 2)
    invoice.line_groups = [mk_task_line_group(lines=[line])]
    invoice.discounts.append(mk_discount_line(amount=5000000, tva=0))
    dbsession.merge(invoice)
    dbsession.flush()
    return invoice


@pytest.fixture
def invoice(
    mk_invoice,
    dbsession,
    def_dummy_tva,
    dummy_tva_computer,
    mk_product,
    customer,
    company,
    mk_task_line,
    mk_task_line_group,
):
    p1 = mk_product(name="product 1", compte_cg="P0001", tva=def_dummy_tva)
    p2 = mk_product(name="product 2", compte_cg="P0002",
                    tva=dummy_tva_computer)
    invoice = mk_invoice(mode='ht')
    lines = []
    line1 = mk_task_line(
        cost=10000000,
        tva=def_dummy_tva.value,
        product=p1,
    )
    lines.append(line1)
    line2 = mk_task_line(
        cost=10000000,
        tva=def_dummy_tva.value,
        product=p1,
    )
    lines.append(line2)
    line3 = mk_task_line(
        cost=10000000,
        tva=dummy_tva_computer.value,
        product=p2,
    )
    lines.append(line3)
    invoice.company = company
    invoice.customer = customer
    invoice.line_groups = [mk_task_line_group(lines=lines)]
    invoice.default_tva = def_dummy_tva.value
    invoice.expenses_tva = def_dummy_tva.value
    invoice.date = datetime.date(2013, 2, 2)
    invoice.official_number = "INV_001"
    invoice.expenses_ht = 10000000
    invoice.expenses = 10000000
    return invoice


@pytest.fixture
def payment(invoice, def_dummy_tva, bank, remittance):
    from endi.models.task.payment import Payment
    p = Payment(
        bank_remittance_id="REM_ID",
        amount=10000000,
        mode="chèque",
        date=datetime.date.today(),
        tva=def_dummy_tva,
        bank=bank,
        bank_remittance=remittance,
    )
    invoice.payments = [p]
    return p


@pytest.fixture
def internalpayment(internalinvoice):
    from endi.models.task.internalpayment import InternalPayment
    p = InternalPayment(
        amount=10000000,
        date=datetime.date.today(),
    )
    internalinvoice.payments = [p]
    return internalinvoice.payments[0]


@pytest.fixture
def sagepayment(payment, config_request):
    factory = SagePaymentMain(None, config_request)
    factory.set_payment(payment)
    return factory


@pytest.fixture
def internalsagepayment_module(internalpayment, config_request):
    factory = InternalSagePaymentMain(None, config_request)
    factory.set_payment(internalpayment)
    return factory


@pytest.fixture
def sagepayment_tva(payment, config_request):
    factory = SagePaymentTva(None, config_request)
    factory.set_payment(payment)
    return factory


@pytest.fixture
def sagepayment_remittance(payment, config_request):
    factory = SagePaymentRemittance(None, config_request)
    factory.set_payment(payment)
    br_amounts = {}
    br_amounts["REM_ID"] = 5000000
    factory.set_remittances_amounts(br_amounts)
    return factory


@pytest.mark.payment
class TestSagePaymentMain():
    def test_base_entry(self, sagepayment):
        today = datetime.date.today()
        assert sagepayment.reference == "INV_001/REM_ID"
        assert sagepayment.code_journal == "CODE_JOURNAL_BANK"
        assert sagepayment.date == today.strftime("%d%m%y")
        assert sagepayment.mode == "chèque"
        assert sagepayment.libelle == "company / Rgt customer"

    def test_credit_client(self, sagepayment):
        g_entry, entry = sagepayment.credit_client(10000000)
        assert entry['compte_cg'] == 'CG_CUSTOMER'
        assert entry['compte_tiers'] == 'CUSTOMER'
        assert entry['credit'] == 10000000

    def test_debit_banque(self, sagepayment):
        g_entry, entry = sagepayment.debit_banque(10000000)
        assert entry['compte_cg'] == "COMPTE_CG_BANK"
        assert entry['debit'] == 10000000


@pytest.mark.payment
class TestSagePaymentTva():
    def test_get_amount(self, sagepayment_tva, dummy_tva_sans_code, payment):
        payment.tva = dummy_tva_sans_code
        sagepayment_tva.set_payment(payment)
        amount = sagepayment_tva.get_amount()
        # tva inversée d'un paiement de 10000000 à 20%
        assert amount == 1666667

    def test_credit_tva(self, sagepayment_tva, dummy_tva_sans_code, payment):
        g_entry, entry = sagepayment_tva.credit_tva(10000000)
        assert entry['credit'] == 10000000
        assert entry['compte_cg'] == 'TVAAPAYER0001'
        assert entry['code_taxe'] == 'CTVA0001'

        # Test if there is no tva code
        payment.tva = dummy_tva_sans_code
        sagepayment_tva.set_payment(payment)
        g_entry, entry = sagepayment_tva.credit_tva(10000000)
        assert 'code_taxe' not in entry

    def test_debit_tva(self, sagepayment_tva):
        g_entry, entry = sagepayment_tva.debit_tva(10000000)
        assert entry['debit'] == 10000000
        assert entry['compte_cg'] == 'TVA0001'
        assert entry['code_taxe'] == 'CTVA0001'


@pytest.mark.payment
class TestSagePaymentRemittance():
    def test_debit_banque(self, sagepayment_remittance):
        g_entry, entry = sagepayment_remittance.debit_banque()
        assert entry['debit'] == 5000000
        assert entry['compte_cg'] == "COMPTE_CG_BANK"
        assert entry['reference'] == "REM_ID"


@pytest.mark.payment
class TestInternalSagePaymentMain():
    def test_base_entry(self, internalsagepayment_module):
        today = datetime.date.today()
        assert internalsagepayment_module.reference == "INV_I01"
        assert internalsagepayment_module.code_journal == \
            "INTERNAL_JOURNAL_ENCAISSEMENT"
        assert internalsagepayment_module.date == today.strftime("%d%m%y")
        assert internalsagepayment_module.libelle == \
            "company / Rgt Interne customer"

    def test_credit_client(
        self, dbsession, internalsagepayment_module, customer, company
    ):
        g_entry, entry = internalsagepayment_module.credit_client(95000000)
        assert entry['compte_cg'] == '41100000'
        assert entry['compte_tiers'] == 'CUSTOMER'
        assert entry['credit'] == 95000000

        customer.compte_tiers = None
        company.internalthird_party_customer_account = None
        dbsession.merge(company)
        dbsession.flush()
        g_entry, entry = internalsagepayment_module.credit_client(95000000)
        assert entry['compte_tiers'] == 'CAE_TIERS_INTERNE'

    def test_debit_banque(self, internalsagepayment_module):
        g_entry, entry = internalsagepayment_module.debit_banque(95000000)
        assert entry['compte_cg'] == "INTERNAL_BANK_CG_ENCAISSEMENT"
        assert entry['debit'] == 95000000
