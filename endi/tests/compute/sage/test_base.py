from endi.compute.sage.base import (
    double_lines,
    filter_accounting_entry
)


def decoratorfunc(a, b):
    return {'type_': 'A', 'key': 'value', 'num_analytique': 'NUM'}


def test_double_lines():
    res = list(double_lines(decoratorfunc)(None, None))
    assert res == [
        {'type_': 'G', 'key': 'value'},
        {'type_': 'A', 'key': 'value', 'num_analytique': 'NUM'}
        ]


def test_filter_accounting_entry():
    assert filter_accounting_entry({'credit': -5}) == {'debit': 5}
    assert filter_accounting_entry({'debit': 5}) == {'debit': 5}
    assert filter_accounting_entry({'debit': -5}) == {'credit': 5}
    assert filter_accounting_entry({'credit': 5}) == {'credit': 5}
