import pytest

from unittest.mock import MagicMock
from endi.compute.task import (
    LineCompute,
    GroupCompute,
    InvoiceCompute,
)

from endi.models.task.payment import BankRemittance
from endi.tests.tools import Dummy


class DummyLine(Dummy, LineCompute):
    """
        Dummy line model
    """
    tva_object = None

    def get_tva(self):
        return self.tva_object


class DummyGroup(Dummy, GroupCompute):
    pass


class DummyInvoice(Dummy, InvoiceCompute):
    pass


class DummyRemittance(Dummy, BankRemittance):
    """
        Dummy bank remittance model
    """
    def get_analytic_code(self):
        return "ANA"


@pytest.fixture
def app_config(dbsession):
    result = {
        'sage_contribution': '1',
        'sage_rginterne': '1',
        'code_journal': 'CODE_JOURNAL',
        'compte_cg_contribution': 'CG_CONTRIB',
        'compte_rrr': 'CG_RRR',
        'compte_frais_annexes': 'CG_FA',
        'compte_rg_externe': 'CG_RG_EXT',
        'compte_rg_interne': 'CG_RG_INT',
        'compte_cg_banque': 'BANK_CG',
        'compte_cg_tva_rrr': "CG_TVA_RRR",
        "code_tva_rrr": "CODE_TVA_RRR",
        'numero_analytique': 'NUM_ANA',
        'rg_coop': 'CG_RG_COOP',
        'taux_rg_interne': "5",
        'taux_rg_client': "5",
        'contribution_cae': "10",
        'compte_cg_ndf': "CGNDF",
        'code_journal_ndf': "JOURNALNDF",
        'code_journal_waiver_ndf': 'JOURNAL_ABANDON',
        'code_tva_ndf': "TVANDF",
        "compte_cg_waiver_ndf": "COMPTE_CG_WAIVER",
        'receipts_active_tva_module': True,
        'receipts_code_journal': "JOURNAL_RECEIPTS",
        'bookentry_facturation_label_template':
            '{invoice.customer.label} {company.name}',
        'bookentry_contribution_label_template':
            "{invoice.customer.label} {company.name}",
        'bookentry_rg_interne_label_template':
            "RG COOP {invoice.customer.label} {company.name}",
        'bookentry_rg_client_label_template':
            "RG {invoice.customer.label} {company.name}",
        'bookentry_expense_label_template':
            "{beneficiaire}/frais {expense_date:%-m %Y}",
        'bookentry_payment_label_template':
            "{company.name} / Rgt {invoice.customer.label}",
        'bookentry_expense_payment_main_label_template':
            "{beneficiaire_LASTNAME} / REMB FRAIS {expense_date:%B/%Y}",
        'bookentry_expense_payment_waiver_label_template':
            "Abandon de créance {beneficiaire_LASTNAME} {expense_date:%B/%Y}",
        'bookentry_supplier_invoice_label_template':
            "{company.name} / Fact. {supplier.label}",
        'bookentry_supplier_payment_label_template':
            "{company.name} / Rgt {supplier.label}",
        "bookentry_supplier_invoice_user_payment_label_template":
            "{beneficiaire_LASTNAME} / REMB FACT {supplier_invoice.official_number}",
        "bookentry_supplier_invoice_user_payment_waiver_label_template":
            "Abandon de créance {beneficiaire_LASTNAME} {supplier_invoice.official_number}",
        'code_journal_frns': "JOURNAL_FRNS",

        'internalcode_journal': "INTERNAL_JOURNAL",
        'internalnumero_analytique': "INTERNAL_NUM_ANA",
        'internalcompte_frais_annexes': 'INTERNAL_CG_FA',
        'internalcompte_cg_banque': "INTERNAL_BANK_CG",
        'internalbookentry_facturation_label_template': (
            '{invoice.customer.label} {company.name}'
        ),
        'internalcompte_rrr': 'INTERNAL_CG_RRR',
        'internalcompte_cg_contribution': 'INTERNAL_CG_CONTRIB',
        "internalcontribution_cae": '5',
        'internalbookentry_contribution_label_template': (
            'contrib {invoice.customer.label} {company.name}'
        ),
        'internalsage_contribution': '1',
        'internalcae_third_party_customer_account': 'CAE_TIERS_INTERNE',

        "internalcae_third_party_supplier_account": 'CAE_TIERS_INTERNE_FRN',

        'internalbookentry_payment_label_template':
            "{company.name} / Rgt Interne {invoice.customer.label}",
        'internalcode_journal_encaissement': 'INTERNAL_JOURNAL_ENCAISSEMENT',
        'internalbank_general_account': 'INTERNAL_BANK_CG_ENCAISSEMENT',

        'internalcode_journal_frns': 'INTERNAL_FRNS_JOURNAL',
        'internalbookentry_supplier_invoice_label_template': (
            "{company.name} / Fact Interne {supplier.label}"
        ),
        'internalbookentry_supplier_payment_label_template': (
            "{company.name} / Rgt Interne {supplier.label}"
        ),
        'ungroup_supplier_invoices_export': '1'
    }
    from endi.models.config import Config
    for key, value in result.items():
        Config.set(key, value)
    return result


@pytest.fixture
def config_request(pyramid_request, app_config, config):
    pyramid_request.config = app_config
    return pyramid_request


@pytest.fixture
def config_request_with_db(config_request, dbsession):
    config_request.dbsession = dbsession
    return config_request


@pytest.fixture
def dummy_tva_sans_code(mk_tva):
    return mk_tva(
        name="tva_sans_code",
        value=2000,
        default=0,
        compte_cg="TVA0001",
        compte_a_payer="TVAAPAYER0001",
        code=None,
    )


@pytest.fixture
def dummy_tva():
    return MagicMock(
        name="tva2",
        value=700,
        default=0,
        compte_cg="TVA0002",
        code='CTVA0002'
    )


@pytest.fixture
def dummy_tva10(mk_tva):
    return mk_tva(
        name="tva 10%",
        value=1000,
        default=0,
        compte_cg="TVA10",
        code='CTVA10'
    )


@pytest.fixture
def dummy_tva20(mk_tva):
    return mk_tva(
        name="tva 20%",
        value=2000,
        default=0,
        compte_cg="TVA20",
        code='CTVA20'
    )


@pytest.fixture
def def_dummy_tva(mk_tva):
    return mk_tva(
        name="tva1",
        value=1960,
        default=0,
        compte_cg="TVA0001",
        compte_a_payer="TVAAPAYER0001",
        code='CTVA0001'
    )


@pytest.fixture
def dummy_tva_computer(mk_tva):
    return mk_tva(
        name="tva2",
        value=700,
        default=0,
        compte_cg="TVA0002",
        code='CTVA0002'
    )


@pytest.fixture
def company(mk_company):
    return mk_company(
        name="company",
        email="company@c.fr",
        code_compta="COMP_CG",
        contribution=None
    )


@pytest.fixture
def customer(mk_customer, company):
    return mk_customer(
        compte_tiers="CUSTOMER",
        compte_cg='CG_CUSTOMER',
        company=company,
        company_id=company.id
    )


@pytest.fixture
def supplier(mk_supplier, company):
    return mk_supplier(
        compte_tiers="SUPPLIER",
        compte_cg='CG_SUPPLIER',
        company=company,
        company_id=company.id
    )


@pytest.fixture
def bank(mk_bankaccount):
    return mk_bankaccount(
        label="banque",
        code_journal="CODE_JOURNAL_BANK",
        compte_cg="COMPTE_CG_BANK"
    )


@pytest.fixture
def expense_type(mk_expense_type):
    return mk_expense_type(
        code='ETYPE1',
        code_tva='CODETVA',
        compte_tva='COMPTETVA',
        contribution=True,
    )
