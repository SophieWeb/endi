import datetime
import pytest
from endi.compute.sage.invoice import (
    SageFacturation,
    InternalSageFacturation,
    SageContribution,
    InternalSageContribution,
    SageRGInterne,
    SageRGClient,
    CustomBookEntryFactory,
    InternalCustomBookEntryFactory,
    InvoiceExport,
    InternalInvoiceExport,
)
from endi.compute.sage.invoice import (
    SageInvoice,
)
from endi.models.task import (
    TaskLine,
    TaskLineGroup,
    DiscountLine
)
from endi.tests.tools import Dummy
from .base import BaseBookEntryTest


@pytest.fixture
def invoice(
    mk_invoice,
    dbsession,
    def_dummy_tva,
    dummy_tva_computer,
    mk_product,
    customer,
    company,
):
    p1 = mk_product(name="product 1", compte_cg="P0001", tva=def_dummy_tva)
    p2 = mk_product(name="product 2", compte_cg="P0002",
                    tva=dummy_tva_computer)
    invoice = mk_invoice(mode='ht')
    lines = []
    line1 = TaskLine(
        cost=10000000,
        quantity=1,
        tva=def_dummy_tva.value,
        product=p1,
    )
    lines.append(line1)
    line2 = TaskLine(
        cost=10000000,
        quantity=1,
        tva=def_dummy_tva.value,
        product=p1,
    )
    lines.append(line2)
    line3 = TaskLine(
        cost=10000000,
        quantity=1,
        tva=dummy_tva_computer.value,
        product=p2,
    )
    lines.append(line3)
    invoice.company = company
    invoice.customer = customer
    invoice.line_groups = [TaskLineGroup(lines=lines)]
    invoice.default_tva = def_dummy_tva.value
    invoice.expenses_tva = def_dummy_tva.value
    invoice.date = datetime.date(2013, 2, 2)
    invoice.official_number = "INV_001"
    invoice.expenses_ht = 10000000
    invoice.expenses = 10000000
    return invoice


@pytest.fixture
def invoice_bug363(
        def_dummy_tva,
        dummy_tva10,
        company,
        customer,
        mk_product,
        mk_invoice,
):
    prod = mk_product(name="product 2", compte_cg="P0002", tva=dummy_tva10)
    lines = []
    invoice = mk_invoice(mode='ht')

    for cost, qtity in (
        (15000000, 1),
        (2000000, 86),
        (-173010000, 1),
        (10000000, 1),
        (-201845000, 1),
        (4500000, 33),
        (1800000, 74),
        (3500000, 28),
    ):
        lines.append(
            TaskLine(
                cost=cost,
                quantity=qtity,
                tva=dummy_tva10.value,
                product=prod,
            )
        )

    invoice.company = company
    invoice.customer = customer
    invoice.line_groups = [TaskLineGroup(lines=lines)]
    invoice.default_tva = def_dummy_tva.value
    invoice.expenses_tva = def_dummy_tva.value
    invoice.date = datetime.date(2013, 2, 2)
    invoice.official_number = "INV_001"
    invoice.expenses_ht = 0
    invoice.expenses = 0
    return invoice


@pytest.fixture
def invoice_bug400(def_dummy_tva, dummy_tva20, mk_product, mk_invoice):
    prod = mk_product(name="product 2", compte_cg="P0002", tva=dummy_tva20)
    lines = []

    for cost, qtity in (
        (22112500, 1),
    ):
        lines.append(
            TaskLine(
                cost=cost,
                quantity=qtity,
                tva=dummy_tva20.value,
                product=prod,
            )
        )

    invoice = mk_invoice(mode='ht')
    invoice.default_tva = def_dummy_tva.value
    invoice.expenses_tva = def_dummy_tva.value
    invoice.date = datetime.date(2013, 2, 2)
    invoice.official_number = "INV_001"
    invoice.line_groups = [TaskLineGroup(lines=lines)]
    invoice.expenses_ht = 0
    invoice.expenses = 0
    return invoice


@pytest.fixture
def invoice_issue1160(tva, mk_product, mk_invoice, customer, company):
    """
    Credit et debit négatif
    """
    prod = mk_product(name="product 2", compte_cg="P0002", tva=tva)
    lines = []

    for cost, qtity in (
        (-22112500, 1),
    ):
        lines.append(
            TaskLine(
                cost=cost,
                quantity=qtity,
                tva=tva.value,
                product=prod,
            )
        )

    invoice = mk_invoice(mode='ht')
    invoice.company = company
    invoice.customer = customer
    invoice.line_groups = [TaskLineGroup(lines=lines)]
    invoice.date = datetime.date(2019, 3, 29)
    invoice.customer = customer
    invoice.company = company
    invoice.official_number = "INV_001"
    invoice.expenses_ht = 0
    return invoice


@pytest.fixture
def invoice_discount(def_dummy_tva, dummy_tva_computer, invoice):
    discount1 = DiscountLine(
        amount=10000000,
        tva=def_dummy_tva.value,
    )
    discount2 = DiscountLine(
        amount=10000000,
        tva=dummy_tva_computer.value,
    )
    invoice.discounts = [discount1, discount2]
    return invoice


@pytest.fixture
def internalinvoice(
    dbsession,
    mk_internalinvoice,
    mk_product,
    mk_tva,
    mk_task_line,
    mk_discount_line,
    customer,
    company,
):
    tva = mk_tva(name="test", value=0, compte_cg="TVAINT")
    product = mk_product(
        tva=tva,
        name="interne",
        internal=True,
        compte_cg="70400000",
    )
    line = mk_task_line(
        description="presta", cost=100000000, quantity=1, tva=tva.value,
        product=product
    )
    customer.compte_cg = '41100000'
    dbsession.merge(customer)
    dbsession.flush()
    invoice = mk_internalinvoice(customer=customer, company=company)
    invoice.official_number = 'INV_I01'
    invoice.date = datetime.date(2013, 2, 2)
    invoice.line_groups = [TaskLineGroup(lines=[line])]
    invoice.discounts.append(mk_discount_line(amount=5000000, tva=0))
    dbsession.merge(invoice)
    dbsession.flush()
    return invoice


@pytest.fixture
def sageinvoice(def_dummy_tva, invoice, app_config):
    return SageInvoice(
        invoice=invoice,
        config=app_config,
        default_tva=def_dummy_tva,
    )


@pytest.fixture
def internalsageinvoice(def_dummy_tva, internalinvoice, app_config):
    wrapped_invoice = SageInvoice(
        invoice=internalinvoice,
        config=app_config,
        default_tva=def_dummy_tva,
    )
    return wrapped_invoice


@pytest.fixture
def sageinvoice_discount(def_dummy_tva, invoice_discount, app_config):
    return SageInvoice(
        invoice=invoice_discount,
        config=app_config,
        default_tva=def_dummy_tva
    )


@pytest.fixture
def sageinvoice_bug363(def_dummy_tva, invoice_bug363, app_config):
    return SageInvoice(
        invoice=invoice_bug363,
        config=app_config,
        default_tva=def_dummy_tva,
    )


@pytest.fixture
def sageinvoice_bug400(def_dummy_tva, invoice_bug400, app_config):
    return SageInvoice(
        invoice=invoice_bug400,
        config=app_config,
        default_tva=def_dummy_tva,
    )


@pytest.fixture
def internalcustom_module(mk_custom_invoice_book_entry_module):
    return mk_custom_invoice_book_entry_module(
        doctype='internalinvoice',
    )


def test_get_products(sageinvoice):
    sageinvoice.products['1'] = {'test_key': 'test'}
    assert "test_key" in sageinvoice.get_product(
        '1', 'dontcare', 'dontcare', 20
    )
    assert len(
        list(sageinvoice.get_product(
            '2', 'tva_compte_cg', 'tva_code', 20
        ).keys())
    ) == 4


def test_populate_invoice_lines(sageinvoice):
    sageinvoice._populate_invoice_lines()
    sageinvoice._round_products()
    assert len(set(sageinvoice.products.keys()).difference(
        set(['P0001', 'P0002']))
               ) == 0
    assert sageinvoice.products['P0001']['ht'] == 20000000
    assert sageinvoice.products['P0001']['tva'] == 3920000
    assert sageinvoice.products['P0002']['ht'] == 10000000
    assert sageinvoice.products['P0002']['tva'] == 700000


def test_populate_discount_lines(sageinvoice_discount):
    sageinvoice_discount._populate_discounts()
    sageinvoice_discount._round_products()
    assert list(sageinvoice_discount.products.keys()) == ['CG_RRR']
    assert sageinvoice_discount.products['CG_RRR']['code_tva'] == (
        "CODE_TVA_RRR"
    )
    assert sageinvoice_discount.products['CG_RRR']['compte_cg_tva'] == \
        "CG_TVA_RRR"
    assert sageinvoice_discount.products['CG_RRR']['ht'] == 20000000
    assert sageinvoice_discount.products['CG_RRR']['tva'] == 2660000


def test_populate_discount_lines_without_compte_rrr(sageinvoice_discount):
    from endi.compute.sage import MissingData
    # If one compte_cg_tva_rrr is not def
    # No entry should be returned
    sageinvoice_discount.config.pop("compte_rrr")
    with pytest.raises(MissingData):
        sageinvoice_discount._populate_discounts()


def test_populate_discount_lines_without_compte_cg_tva(sageinvoice_discount):
    from endi.compute.sage import MissingData
    # If one compte_cg_tva_rrr is not def
    # No entry should be returned
    sageinvoice_discount.config.pop("compte_cg_tva_rrr")
    with pytest.raises(MissingData):
        sageinvoice_discount._populate_discounts()


def test_populate_discount_lines_without_code_tva(sageinvoice_discount):
    # If the code tva is not def, it should work
    sageinvoice_discount.config.pop("code_tva_rrr")
    sageinvoice_discount._populate_discounts()
    assert list(sageinvoice_discount.products.keys()) != []


def test_round_products(sageinvoice_bug400):
    sageinvoice_bug400._populate_invoice_lines()
    sageinvoice_bug400._round_products()
    assert list(sageinvoice_bug400.products.values())[0]['ht'] == 22113000


def test_populate_expenses(sageinvoice):
    sageinvoice.expense_tva_compte_cg = "TVA0001"
    sageinvoice._populate_expenses()
    sageinvoice._round_products()
    assert list(sageinvoice.products.keys()) == ['CG_FA']
    assert sageinvoice.products['CG_FA']['ht'] == 20000000
    assert sageinvoice.products['CG_FA']['tva'] == 1960000


class TestSageFacturation(BaseBookEntryTest):
    factory = SageFacturation

    def test__has_tva_value(self):
        product = {'tva': 0.5}
        assert SageFacturation._has_tva_value(product)
        product = {'tva': 0.0}
        assert not SageFacturation._has_tva_value(product)
        product = {'tva': -0.5}
        assert SageFacturation._has_tva_value(product)

    def test_credit_totalht(self, sageinvoice, config_request):
        res = {
            'libelle': 'customer company',
            'compte_cg': 'P0001',
            'num_analytique': 'COMP_CG',
            'code_tva': 'CTVA0001',
            'credit': 20000000,
        }
        method = "credit_totalht"
        self._test_product_book_entry(config_request, sageinvoice, method, res)

    def test_credit_tva(self, sageinvoice, config_request):
        res = {
            'libelle': 'customer company',
            'compte_cg': 'TVA0001',
            'num_analytique': 'COMP_CG',
            'code_tva': 'CTVA0001',
            'credit': 3920000
        }
        method = "credit_tva"
        self._test_product_book_entry(config_request, sageinvoice, method, res)

    def test_debit_ttc(self, config_request, sageinvoice):
        method = "debit_ttc"
        res = {
            'libelle': 'customer company',
            'compte_cg': 'CG_CUSTOMER',
            'num_analytique': 'COMP_CG',
            'compte_tiers': 'CUSTOMER',
            'debit': 23920000,
            'echeance': '040313'
        }
        self._test_product_book_entry(config_request, sageinvoice, method, res)

    def test_discount_ht(self, sageinvoice_discount, config_request):
        # REF #307 : https://framagit.org/endi/endi/issues/307
        method = "credit_totalht"
        res = {
            'libelle': "customer company",
            'compte_cg': 'CG_RRR',
            'num_analytique': 'COMP_CG',
            'code_tva': 'CODE_TVA_RRR',
            'debit': 20000000,
        }
        self._test_product_book_entry(
            config_request,
            sageinvoice_discount,
            method,
            res,
            'CG_RRR',
        )

    def test_discount_tva(self, sageinvoice_discount, config_request):
        # REF #307 : https://framagit.org/endi/endi/issues/307
        res = {
            'libelle': 'customer company',
            'compte_cg': 'CG_TVA_RRR',
            'num_analytique': 'COMP_CG',
            'code_tva': 'CODE_TVA_RRR',
            'debit': 1960000 + 700000,
        }
        method = "credit_tva"
        self._test_product_book_entry(
            config_request,
            sageinvoice_discount,
            method,
            res,
            'CG_RRR',
        )

    def test_discount_ttc(self, config_request, sageinvoice_discount):
        # REF #307 : https://framagit.org/endi/endi/issues/307
        method = "debit_ttc"
        res = {
            'libelle': 'customer company',
            'compte_cg': 'CG_CUSTOMER',
            'num_analytique': 'COMP_CG',
            'compte_tiers': 'CUSTOMER',
            'credit': 20000000 + 1960000 + 700000,
            'echeance': '040313',
        }
        self._test_product_book_entry(
            config_request,
            sageinvoice_discount,
            method,
            res,
            'CG_RRR',
        )

    def test_bug363(self, config_request, sageinvoice_bug363):
        res = {
            'libelle': 'customer company',
            'compte_cg': 'TVA10',
            'num_analytique': 'COMP_CG',
            'code_tva': 'CTVA10',
            'credit': 20185000
        }
        method = "credit_tva"
        self._test_product_book_entry(
            config_request,
            sageinvoice_bug363, method, res, "P0002")


class TestSageContribution(BaseBookEntryTest):
    factory = SageContribution

    def test_debit_company(self, config_request, sageinvoice):
        method = "debit_company"
        res = {
            'libelle': 'customer company',
            'compte_cg': 'CG_CONTRIB',
            'num_analytique': 'COMP_CG',
            'debit': 2000000
        }
        self._test_product_book_entry(
            config_request, sageinvoice, method, res)

    def test_credit_company(self, config_request, sageinvoice):
        method = "credit_company"
        res = {
            'libelle': 'customer company',
            'compte_cg': 'BANK_CG',
            'num_analytique': 'COMP_CG',
            'credit': 2000000
        }
        self._test_product_book_entry(config_request, sageinvoice, method, res)

    def test_debit_cae(self, config_request, sageinvoice):
        method = "debit_cae"
        res = {
            'libelle': 'customer company',
            'compte_cg': 'BANK_CG',
            'num_analytique': 'NUM_ANA',
            'debit': 2000000
        }
        self._test_product_book_entry(config_request, sageinvoice, method, res)

    def test_credit_cae(self, config_request, sageinvoice):
        method = "credit_cae"
        res = {
            'libelle': 'customer company',
            'compte_cg': 'CG_CONTRIB',
            'num_analytique': 'NUM_ANA',
            'credit': 2000000
        }
        self._test_product_book_entry(config_request, sageinvoice, method, res)

    def test_discount_line_inversion_debit_entr(
        self, config_request, sageinvoice_discount
    ):
        # REF #333 : https://framagit.org/endi/endi/issues/333
        # Débit et crédit vont dans le sens inverse pour les remises (logique)
        method = "debit_company"
        res = {
            'libelle': 'customer company',
            'compte_cg': 'BANK_CG',
            'num_analytique': 'COMP_CG',
            'debit': 2000000
        }
        self._test_product_book_entry(
            config_request,
            sageinvoice_discount,
            method,
            res,
            'CG_RRR',
        )

    def test_discount_line_inversion_credit_entr(
        self, config_request, sageinvoice_discount
    ):
        # REF #333 : https://framagit.org/endi/endi/issues/333
        # Débit et crédit vont dans le sens inverse pour les remises (logique)
        method = "credit_company"
        res = {
            'libelle': 'customer company',
            'compte_cg': 'CG_CONTRIB',
            'num_analytique': 'COMP_CG',
            'credit': 2000000
        }
        self._test_product_book_entry(
            config_request,
            sageinvoice_discount,
            method,
            res,
            'CG_RRR',
        )

    def test_discount_line_inversion_debit_cae(
        self, config_request, sageinvoice_discount
    ):
        # REF #333 : https://framagit.org/endi/endi/issues/333
        # Débit et crédit vont dans le sens inverse pour les remises (logique)
        method = "debit_cae"
        res = {
            'libelle': 'customer company',
            'compte_cg': 'CG_CONTRIB',
            'num_analytique': 'NUM_ANA',
            'debit': 2000000
        }
        self._test_product_book_entry(
            config_request,
            sageinvoice_discount,
            method,
            res,
            'CG_RRR',
        )

    def test_discount_line_inversion_credit_cae(
        self, config_request, sageinvoice_discount
    ):
        # REF #333 : https://framagit.org/endi/endi/issues/333
        # Débit et crédit vont dans le sens inverse pour les remises (logique)
        method = "credit_cae"
        res = {
            'libelle': 'customer company',
            'compte_cg': 'BANK_CG',
            'num_analytique': 'NUM_ANA',
            'credit': 2000000
        }
        self._test_product_book_entry(
            config_request,
            sageinvoice_discount,
            method,
            res,
            'CG_RRR',
        )


class TestCustomAssurance(BaseBookEntryTest):
    # Amount = 2000 0.05 * somme des ht des lignes + expense_ht
    # Migrate the export module Assurance to a custom module
    def build_factory(self, config_request):
        return CustomBookEntryFactory(
            None,
            config_request,
            Dummy(
                compte_cg_debit='CG_ASSUR',
                compte_cg_credit='CG_ASSUR',
                percentage=5,
                label_template="{client.label} {entreprise.name}"
            )
        )

    def test_debit_company(self, config_request, sageinvoice):
        method = 'debit_company'
        res = {
                'libelle': 'customer company',
                'compte_cg': 'CG_ASSUR',
                'num_analytique': 'COMP_CG',
                'debit': 2000000,
                }
        self._test_invoice_book_entry(config_request, sageinvoice, method, res)

    def test_credit_company(self, config_request, sageinvoice):
        method = 'credit_company'
        res = {
                'libelle': 'customer company',
                'compte_cg': 'BANK_CG',
                'num_analytique': 'COMP_CG',
                'credit': 2000000,
                }
        self._test_invoice_book_entry(config_request, sageinvoice, method, res)

    def test_debit_cae(self, config_request, sageinvoice):
        method = 'debit_cae'
        res = {
                'libelle': 'customer company',
                'compte_cg': 'BANK_CG',
                'num_analytique': 'NUM_ANA',
                'debit': 2000000,
                }
        self._test_invoice_book_entry(config_request, sageinvoice, method, res)

    def test_credit_cae(self, config_request, sageinvoice):
        method = 'credit_cae'
        res = {
                'libelle': 'customer company',
                'compte_cg': 'CG_ASSUR',
                'num_analytique': 'NUM_ANA',
                'credit': 2000000,
                }
        self._test_invoice_book_entry(config_request, sageinvoice, method, res)


class TestCustomCGScop(BaseBookEntryTest):
    # Migrate the export module CGScop to a custom module
    def build_factory(self, config_request):
        return CustomBookEntryFactory(
            None,
            config_request,
            Dummy(
                compte_cg_debit='CG_SCOP',
                compte_cg_credit='CG_DEB',
                percentage=5,
                label_template="{client.label} {entreprise.name}"
            )
        )

    def test_debit_company(self, config_request, sageinvoice):
        method = 'debit_company'
        res = {
                'libelle': 'customer company',
                'compte_cg': 'CG_SCOP',
                'num_analytique': 'COMP_CG',
                'debit': 2000000,
                }
        self._test_invoice_book_entry(config_request, sageinvoice, method, res)

    def test_credit_company(self, config_request, sageinvoice):
        method = 'credit_company'
        res = {
                'libelle': 'customer company',
                'num_analytique': 'COMP_CG',
                'compte_cg': 'BANK_CG',
                'credit': 2000000,
                }
        self._test_invoice_book_entry(config_request, sageinvoice, method, res)

    def test_debit_cae(self, config_request, sageinvoice):
        method = 'debit_cae'
        res = {
                'libelle': 'customer company',
                'compte_cg': 'BANK_CG',
                'num_analytique': 'NUM_ANA',
                'debit': 2000000,
                }
        self._test_invoice_book_entry(config_request, sageinvoice, method, res)

    def test_credit_cae(self, config_request, sageinvoice):
        method = 'credit_cae'
        res = {
                'libelle': 'customer company',
                'compte_cg': 'CG_DEB',
                'num_analytique': 'NUM_ANA',
                'credit': 2000000,
                }
        self._test_invoice_book_entry(config_request, sageinvoice, method, res)


class TestCustomBookEntryFactory(BaseBookEntryTest):
    # Replaced SageContributionOrganic
    libelle = 'Contribution Organic customer company'

    def build_factory(self, config_request):
        return CustomBookEntryFactory(
            None,
            config_request,
            Dummy(
                compte_cg_debit='CG_ORGA',
                compte_cg_credit='CG_DEB_ORGA',
                percentage=5,
                label_template="Contribution Organic {client.label} \
{entreprise.name}"
            )
        )

    def test_debit_company(self, config_request, sageinvoice):
        method = 'debit_company'
        res = {
                'libelle': self.libelle,
                'compte_cg': 'CG_ORGA',
                'num_analytique': 'COMP_CG',
                'debit': 2000000,
                }
        self._test_invoice_book_entry(config_request, sageinvoice, method, res)

    def test_credit_company(self, config_request, sageinvoice):
        method = 'credit_company'
        res = {
                'libelle': self.libelle,
                'num_analytique': 'COMP_CG',
                'compte_cg': 'BANK_CG',
                'credit': 2000000,
                }
        self._test_invoice_book_entry(config_request, sageinvoice, method, res)

    def test_debit_cae(self, config_request, sageinvoice):
        method = 'debit_cae'
        res = {
                'libelle': self.libelle,
                'compte_cg': 'BANK_CG',
                'num_analytique': 'NUM_ANA',
                'debit': 2000000,
                }
        self._test_invoice_book_entry(config_request, sageinvoice, method, res)

    def test_credit_cae(self, config_request, sageinvoice):
        method = 'credit_cae'
        res = {
                'libelle': self.libelle,
                'compte_cg': 'CG_DEB_ORGA',
                'num_analytique': 'NUM_ANA',
                'credit': 2000000,
                }
        self._test_invoice_book_entry(config_request, sageinvoice, method, res)


class TestSageRGInterne(BaseBookEntryTest):
    factory = SageRGInterne

    def test_debit_company(self, config_request, sageinvoice):
        method = "debit_company"
        res = {
            'libelle': 'RG COOP customer company',
            'compte_cg': 'CG_RG_INT',
            'num_analytique': 'COMP_CG',
            'debit': 1196000
        }
        self._test_product_book_entry(config_request, sageinvoice, method, res)

    def test_credit_company(self, config_request, sageinvoice):
        method = "credit_company"
        res = {
            'libelle': 'RG COOP customer company',
            'compte_cg': 'BANK_CG',
            'num_analytique': 'COMP_CG',
            'credit': 1196000
        }
        self._test_product_book_entry(config_request, sageinvoice, method, res)

    def test_debit_cae(self, config_request, sageinvoice):
        method = "debit_cae"
        res = {
            'libelle': 'RG COOP customer company',
            'compte_cg': 'BANK_CG',
            'num_analytique': 'NUM_ANA',
            'debit': 1196000
        }
        self._test_product_book_entry(config_request, sageinvoice, method, res)

    def test_credit_cae(self, config_request, sageinvoice):
        method = "credit_cae"
        res = {
            'libelle': 'RG COOP customer company',
            'compte_cg': 'CG_RG_INT',
            'num_analytique': 'NUM_ANA',
            'credit': 1196000
        }
        self._test_product_book_entry(config_request, sageinvoice, method, res)


class TestSageRGClient(BaseBookEntryTest):
    factory = SageRGClient

    def test_debit_company(self, config_request, sageinvoice):
        method = 'debit_company'
        res = {
                'libelle': 'RG customer company',
                'compte_cg': 'CG_RG_EXT',
                'num_analytique': 'COMP_CG',
                'echeance': '020214',
                'debit': 1196000,
                }
        self._test_product_book_entry(config_request, sageinvoice, method, res)

    def test_credit_company(self, config_request, sageinvoice):
        method = 'credit_company'
        res = {
                'libelle': 'RG customer company',
                'num_analytique': 'COMP_CG',
                'compte_cg': 'CG_CUSTOMER',
                'compte_tiers': 'CUSTOMER',
                'echeance': '020214',
                'credit': 1196000,
                }
        self._test_product_book_entry(config_request, sageinvoice, method, res)


class TestInvoiceExport():
    def get_one(self, request):
        return InvoiceExport(None, request)

    def test_modules(self, config_request, app_config):
        config_request.config = app_config
        exporter = self.get_one(config_request)
        assert len(exporter.modules) == 3
        sage_factories = [SageFacturation, SageContribution, SageRGInterne]
        for fact in sage_factories:
            assert True in [
                isinstance(module, fact) for module in exporter.modules
            ]

    def test_get_item_book_entries(
        self, config_request, app_config, invoice_issue1160, tva
    ):
        config_request.config = app_config
        exporter = self.get_one(config_request)
        for entry in exporter._get_item_book_entries(invoice_issue1160):
            assert entry.get('credit', 0) >= 0
            assert entry.get('debit', 0) >= 0


class TestInternalSageFacturation(BaseBookEntryTest):
    code_journal = 'INTERNAL_JOURNAL'
    factory = InternalSageFacturation

    def test__get_config_value(self, config_request):
        factory = self.build_factory(config_request)
        assert (
            factory._get_config_value('compte_rrr') == 'INTERNAL_CG_RRR'
        )

    def test__has_tva_value(self, config_request):
        factory = self.build_factory(config_request)
        assert not factory._has_tva_value('15')

    def test_credit_totalht(self, internalsageinvoice, config_request):
        res = {
            'libelle': 'customer company',
            'compte_cg': '70400000',
            'num_analytique': 'COMP_CG',
            'code_tva': '',
            'credit': 100000000,
        }
        method = "credit_totalht"
        self._test_product_book_entry(
            config_request, internalsageinvoice, method, res,
            prod_cg='70400000'
        )

    def test_debit_ttc(self, config_request, internalsageinvoice):
        method = "debit_ttc"
        res = {
            'libelle': 'customer company',
            'compte_cg': '41100000',
            'num_analytique': 'COMP_CG',
            'compte_tiers': 'CUSTOMER',
            'debit': 100000000,
            'echeance': '040313'
        }
        self._test_product_book_entry(
            config_request, internalsageinvoice, method, res,
            prod_cg='70400000'
        )

    def test_discount_ht(self, internalsageinvoice, config_request):
        # REF #307 : https://framagit.org/endi/endi/issues/307
        method = "credit_totalht"
        res = {
            'libelle': "customer company",
            'compte_cg': 'INTERNAL_CG_RRR',
            'num_analytique': 'COMP_CG',
            'code_tva': '',
            'debit': 5000000,
        }
        self._test_product_book_entry(
            config_request,
            internalsageinvoice,
            method,
            res,
            'INTERNAL_CG_RRR',
        )

    def test_discount_ttc(self, config_request, internalsageinvoice):
        # REF #307 : https://framagit.org/endi/endi/issues/307
        method = "debit_ttc"
        res = {
            'libelle': 'customer company',
            'compte_cg': '41100000',
            'num_analytique': 'COMP_CG',
            'compte_tiers': 'CUSTOMER',
            'credit': 5000000,
            'echeance': '040313',
        }
        self._test_product_book_entry(
            config_request,
            internalsageinvoice,
            method,
            res,
            'INTERNAL_CG_RRR',
        )


class TestInternalSageContribution(BaseBookEntryTest):
    factory = InternalSageContribution
    code_journal = 'INTERNAL_JOURNAL'

    def test__get_config_value(self, config_request):
        factory = self.build_factory(config_request)
        assert (
            factory._get_config_value('compte_rrr') == 'INTERNAL_CG_RRR'
        )

    def test_get_contribution(self, config_request, company):
        factory = self.build_factory(config_request)
        company.contribution = 10
        factory.company = company
        assert factory.get_contribution() == 5
        company.internalcontribution = 6
        assert factory.get_contribution() == 6

    def test_debit_company(self, config_request, internalsageinvoice):
        method = "debit_company"
        res = {
            'libelle': 'contrib customer company',
            'compte_cg': 'INTERNAL_CG_CONTRIB',
            'num_analytique': 'COMP_CG',
            'debit': 5000000
        }
        self._test_product_book_entry(
            config_request, internalsageinvoice, method, res,
            prod_cg='70400000'
        )

    def test_credit_company(self, config_request, internalsageinvoice):
        method = "credit_company"
        res = {
            'libelle': 'contrib customer company',
            'compte_cg': 'INTERNAL_BANK_CG',
            'num_analytique': 'COMP_CG',
            'credit': 5000000
        }
        self._test_product_book_entry(
            config_request, internalsageinvoice, method, res,
            prod_cg='70400000'
        )

    def test_debit_cae(self, config_request, internalsageinvoice):
        method = "debit_cae"
        res = {
            'libelle': 'contrib customer company',
            'compte_cg': 'INTERNAL_BANK_CG',
            'num_analytique': 'INTERNAL_NUM_ANA',
            'debit': 5000000
        }
        self._test_product_book_entry(
            config_request, internalsageinvoice, method, res,
            prod_cg='70400000'
        )

    def test_credit_cae(self, config_request, internalsageinvoice):
        method = "credit_cae"
        res = {
            'libelle': 'contrib customer company',
            'compte_cg': 'INTERNAL_CG_CONTRIB',
            'num_analytique': 'INTERNAL_NUM_ANA',
            'credit': 5000000
        }
        self._test_product_book_entry(
            config_request, internalsageinvoice, method, res,
            prod_cg='70400000'
        )

    def test_discount_line_inversion_debit_entr(
        self, config_request, internalsageinvoice
    ):
        # REF #333 : https://framagit.org/endi/endi/issues/333
        # Débit et crédit vont dans le sens inverse pour les remises (logique)
        method = "debit_company"
        res = {
            'libelle': 'contrib customer company',
            'compte_cg': 'INTERNAL_BANK_CG',
            'num_analytique': 'COMP_CG',
            'debit': 250000
        }
        self._test_product_book_entry(
            config_request,
            internalsageinvoice,
            method,
            res,
            'INTERNAL_CG_RRR',
        )

    def test_discount_line_inversion_credit_entr(
        self, config_request, internalsageinvoice
    ):
        # REF #333 : https://framagit.org/endi/endi/issues/333
        # Débit et crédit vont dans le sens inverse pour les remises (logique)
        method = "credit_company"
        res = {
            'libelle': 'contrib customer company',
            'compte_cg': 'INTERNAL_CG_CONTRIB',
            'num_analytique': 'COMP_CG',
            'credit': 250000
        }
        self._test_product_book_entry(
            config_request,
            internalsageinvoice,
            method,
            res,
            'INTERNAL_CG_RRR',
        )

    def test_discount_line_inversion_debit_cae(
        self, config_request, internalsageinvoice
    ):
        # REF #333 : https://framagit.org/endi/endi/issues/333
        # Débit et crédit vont dans le sens inverse pour les remises (logique)
        method = "debit_cae"
        res = {
            'libelle': 'contrib customer company',
            'compte_cg': 'INTERNAL_CG_CONTRIB',
            'num_analytique': 'INTERNAL_NUM_ANA',
            'debit': 250000
        }
        self._test_product_book_entry(
            config_request,
            internalsageinvoice,
            method,
            res,
            'INTERNAL_CG_RRR',
        )

    def test_discount_line_inversion_credit_cae(
        self, config_request, internalsageinvoice
    ):
        # REF #333 : https://framagit.org/endi/endi/issues/333
        # Débit et crédit vont dans le sens inverse pour les remises (logique)
        method = "credit_cae"
        res = {
            'libelle': 'contrib customer company',
            'compte_cg': 'INTERNAL_BANK_CG',
            'num_analytique': 'INTERNAL_NUM_ANA',
            'credit': 250000
        }
        self._test_product_book_entry(
            config_request,
            internalsageinvoice,
            method,
            res,
            'INTERNAL_CG_RRR',
        )


class TestInternalCustomBookEntry:
    # Amount = 2000 0.05 * somme des ht des lignes + expense_ht
    # Migrate the export module Assurance to a custom module
    def build_factory(self, config_request):
        return InternalCustomBookEntryFactory(
            None,
            config_request,
            Dummy(
                compte_cg_debit='CG_ASSUR',
                compte_cg_credit='CG_ASSUR',
                percentage=5,
                label_template="{client.label} {entreprise.name}"
            )
        )

    def test_code_journal(self, config_request):
        factory = self.build_factory(config_request)
        assert factory.code_journal == 'INTERNAL_JOURNAL'


class TestInternalExport:
    def test_base(self, config_request):
        exporter = InternalInvoiceExport(None, config_request)
        assert len(exporter.modules) == 2

    def test_issue2762(self, config_request_with_db, internalcustom_module):
        exporter = InternalInvoiceExport(None, config_request_with_db)
        assert len(exporter.modules) == 3
