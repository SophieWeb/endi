
class BaseBookEntryTest():
    factory = None
    code_journal = 'CODE_JOURNAL'

    def build_factory(self, config_request):
        return self.factory(None, config_request)

    def _test_product_book_entry(
            self,
            config_request,
            wrapped_invoice,
            method,
            exp_analytic_line,
            prod_cg='P0001'):
        """
        test a book_entry output (one of a product)
        """
        wrapped_invoice.populate()
        book_entry_factory = self.build_factory(config_request)
        book_entry_factory.set_invoice(wrapped_invoice)
        product = wrapped_invoice.products[prod_cg]

        general_line, analytic_line = getattr(
            book_entry_factory, method
        )(product)

        exp_analytic_line['date'] = '020213'
        exp_analytic_line['num_facture'] = wrapped_invoice.invoice.official_number
        exp_analytic_line['code_journal'] = self.code_journal
        exp_general_line = exp_analytic_line.copy()
        exp_analytic_line['type_'] = 'A'
        exp_general_line['type_'] = 'G'
        exp_general_line.pop('num_analytique', '')

        assert general_line == exp_general_line
        assert analytic_line == exp_analytic_line

    def _test_invoice_book_entry(
        self, config_request, wrapped_invoice, method, exp_analytic_line
    ):
        """
        test a book_entry output (one of a product)
        """
        wrapped_invoice.populate()
        book_entry_factory = self.build_factory(config_request)
        book_entry_factory.set_invoice(wrapped_invoice)

        general_line, analytic_line = getattr(book_entry_factory, method)()

        exp_analytic_line['date'] = '020213'
        exp_analytic_line['num_facture'] = 'INV_001'
        exp_analytic_line['code_journal'] = 'CODE_JOURNAL'
        exp_general_line = exp_analytic_line.copy()
        exp_analytic_line['type_'] = 'A'
        exp_general_line['type_'] = 'G'
        exp_general_line.pop('num_analytique', '')

        assert general_line == exp_general_line
        assert analytic_line == exp_analytic_line
