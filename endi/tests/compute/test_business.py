from pytest import fixture


@fixture
def supplier_invoice_line_no_tva_on_margin(
        mk_supplier_invoice_line,
        expense_type,
        business,
        supplier_invoice,
):
    return mk_supplier_invoice_line(
        supplier_invoice_id=supplier_invoice.id,
        type_id=expense_type.id,
        ht=10,
        tva=1,
        business_id=business.id,
    )


@fixture
def supplier_invoice_line_tva_on_margin(
        mk_supplier_invoice_line,
        expense_type_tva_on_margin,
        business,
        supplier_invoice,
):
    return mk_supplier_invoice_line(
        supplier_invoice_id=supplier_invoice.id,
        type_id=expense_type_tva_on_margin.id,
        ht=7,
        tva=1,
        business_id=business.id,
    )


@fixture
def supplier_invoice_line_other_business(
        mk_supplier_invoice_line,
        expense_type_tva_on_margin,
        business2,
        supplier_invoice,
):
    return mk_supplier_invoice_line(
        supplier_invoice_id=supplier_invoice.id,
        ht=1000,
        tva=100,
        business_id=business2.id,
    )


def test_total_expenses_invoice_lines_only(
        supplier_invoice_line_tva_on_margin,
        supplier_invoice_line_no_tva_on_margin,
        supplier_invoice_line_other_business,
        business,
):
    assert business.total_expenses == 19
    assert business.total_expenses_tva_on_margin == 8
    assert business.total_expenses_no_tva_on_margin == 11


@fixture
def expense_line_no_tva_on_margin(business, mk_expense_line):
    return mk_expense_line(
        ht=1000,
        tva=100,
        business_id=business.id,
    )


@fixture
def expense_line_tva_on_margin(
        business,
        mk_expense_line,
        expense_type_tva_on_margin,
):
    return mk_expense_line(
        expense_type=expense_type_tva_on_margin,
        ht=44,
        tva=4,
        business_id=business.id,
    )


def test_total_expenses_mixed_lines(
        # Supplier invoice
        supplier_invoice_line_tva_on_margin,
        supplier_invoice_line_no_tva_on_margin,

        # Expenses
        expense_line_tva_on_margin,
        expense_line_no_tva_on_margin,

        business,
):
    assert business.total_expenses == 1167
    assert business.total_expenses_tva_on_margin == 56
    assert business.total_expenses_no_tva_on_margin == 1111
