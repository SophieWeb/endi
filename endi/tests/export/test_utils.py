def test_ensure_encoding_bridge():
    from endi.export.utils import ensure_encoding_bridge
    a = '\u0301'
    assert ensure_encoding_bridge(a, 'iso-8859-16') == b'?'
    assert ensure_encoding_bridge(a, 'utf-8') == a

    b = b'a'
    assert ensure_encoding_bridge(b, 'iso-8859-16') == b


def test_format_filename():
    from endi.export.utils import format_filename
    a = "Test 31/12 .pdf .ods"
    assert format_filename(a) == "test-3112-pdf.ods"
