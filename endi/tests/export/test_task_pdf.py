import io
from PyPDF4 import PdfFileReader
from endi.export.task_pdf import (
    task_pdf,
    ensure_task_pdf_persisted,
)


def _at_least_one_page(pdf_buffer):
    pdf_reader = PdfFileReader(pdf_buffer)
    return len(list(pdf_reader.getOutlines())) > 0


def test_pdf_rendering(full_invoice, get_csrf_request_with_db):
    request = get_csrf_request_with_db(context=full_invoice)
    data = task_pdf(full_invoice, request)
    assert isinstance(data, io.BytesIO)
    _at_least_one_page(data)


def test_store_task_pdf(full_invoice, get_csrf_request_with_db):
    request = get_csrf_request_with_db(context=full_invoice)
    ensure_task_pdf_persisted(full_invoice, request)
    assert full_invoice.pdf_file_hash is None
    full_invoice.status = 'valid'
    ensure_task_pdf_persisted(full_invoice, request)
    assert full_invoice.pdf_file_hash is not None

    assert _at_least_one_page(full_invoice.pdf_file.data_obj)
