(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['bulk_file_generation.mustache'] = template({"1":function(container,depth0,helpers,partials,data) {
    return "        <div class=\"alert status\">\n            <span class=\"icon big\"><svg><use href=\"/static/icons/endi.svg#clock\"></use></svg></span>\n            <p>\n	            <b>La génération des fichiers est en attente de démarrage</b>\n            </p>\n        </div>\n";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.running : depth0),{"name":"if","hash":{},"fn":container.program(4, data, 0),"inverse":container.program(6, data, 0),"data":data})) != null ? stack1 : "");
},"4":function(container,depth0,helpers,partials,data) {
    return "        <div class=\"alert status alert-info\">\n            <span class=\"icon big spin\"><svg><use href=\"/static/icons/endi.svg#cog\"></use></svg></span>\n            <p>\n	            <b>La génération des fichiers est en cours</b>\n            </p>\n        </div>\n";
},"6":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.failed : depth0),{"name":"if","hash":{},"fn":container.program(7, data, 0),"inverse":container.program(9, data, 0),"data":data})) != null ? stack1 : "");
},"7":function(container,depth0,helpers,partials,data) {
    return "        <div class=\"alert status alert-danger\">\n            <span class=\"icon big\"><svg><use href=\"/static/icons/endi.svg#danger\"></use></svg></span>\n            <p>\n	            <b>La génération des fichiers a échoué</b>\n            </p>\n        </div>\n";
},"9":function(container,depth0,helpers,partials,data) {
    return "        <div class=\"alert status alert-success\">\n             <span class=\"icon big\"><svg><use href=\"/static/icons/endi.svg#check\"></use></svg></span>\n            <p>\n	            <b>La génération des fichiers est terminée</b>\n            </p>\n        </div>\n";
},"11":function(container,depth0,helpers,partials,data) {
    var stack1, helper, options, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", buffer = 
  "	<div class='content_vertical_padding'>\n		<div class='layout flex two_cols'>\n            <div>\n                <h4>Les fichiers suivants ont été générés</h4>\n                <table class=\"full_width\">\n                    <thead>\n                    <tr>\n                        <th scope=\"col\" class=\"col_text\">Fichier</th>\n                        <th scope=\"col\" class=\"col_text\">Action</th>\n                    </tr>\n                    </thead>\n\n                    <tbody>\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.results_list : depth0),{"name":"each","hash":{},"fn":container.program(12, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                    </tbody>\n                </table>\n            </div>\n			<div>\n				<h4>Avertissements</h4>\n                <p>\n                    "
    + container.escapeExpression(((helper = (helper = helpers.err_message || (depth0 != null ? depth0.err_message : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"err_message","hash":{},"data":data}) : helper)))
    + "\n                </p>\n";
  stack1 = ((helper = (helper = helpers.has_err_message || (depth0 != null ? depth0.has_err_message : depth0)) != null ? helper : alias2),(options={"name":"has_err_message","hash":{},"fn":container.noop,"inverse":container.program(17, data, 0),"data":data}),(typeof helper === alias3 ? helper.call(alias1,options) : helper));
  if (!helpers.has_err_message) { stack1 = helpers.blockHelperMissing.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  return buffer + "			</div>\n	    </div>\n	</div>\n";
},"12":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return "                        <tr>\n                            <td class=\"col_text\">"
    + container.escapeExpression(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "</td>\n                            <td class=\"col_text\">\n                                "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.regenerated : depth0),{"name":"if","hash":{},"fn":container.program(13, data, 0),"inverse":container.program(15, data, 0),"data":data})) != null ? stack1 : "")
    + "\n                            </td>\n                        </tr>\n";
},"13":function(container,depth0,helpers,partials,data) {
    return "Re-généré";
},"15":function(container,depth0,helpers,partials,data) {
    return "Généré";
},"17":function(container,depth0,helpers,partials,data) {
    return "				<em>Aucune erreur n’a été retournée</em>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<h1>"
    + alias4(((helper = (helper = helpers.label || (depth0 != null ? depth0.label : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"label","hash":{},"data":data}) : helper)))
    + "</h1>\n<div class=\"popup_content\">\n	<div class='content_vertical_padding'>\n		<dl class=\"dl-horizontal\">\n			<dt>Identifiant de la tâche</dt><dd>"
    + alias4(((helper = (helper = helpers.jobid || (depth0 != null ? depth0.jobid : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"jobid","hash":{},"data":data}) : helper)))
    + "</dd>\n			<dt>Initialisée le</dt><dd>"
    + alias4(((helper = (helper = helpers.created_at || (depth0 != null ? depth0.created_at : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"created_at","hash":{},"data":data}) : helper)))
    + "</dd>\n			<dt>Mise à jour le</dt><dd>"
    + alias4(((helper = (helper = helpers.updated_at || (depth0 != null ? depth0.updated_at : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"updated_at","hash":{},"data":data}) : helper)))
    + "</dd>\n		</dl>\n	</div>\n	<div class='content_vertical_padding separate_bottom'>\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.waiting : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(3, data, 0),"data":data})) != null ? stack1 : "")
    + "	</div>\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.finished : depth0),{"name":"if","hash":{},"fn":container.program(11, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "</div>\n";
},"useData":true});
templates['csv_import.mustache'] = template({"1":function(container,depth0,helpers,partials,data) {
    return "        <div class=\"alert status\">\n            <span class=\"icon big\"><svg><use href=\"/static/icons/endi.svg#clock\"></use></svg></span>\n            <p>\n	            <b>L’import est en attente de traitement</b>\n            </p>\n";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.running : depth0),{"name":"if","hash":{},"fn":container.program(4, data, 0),"inverse":container.program(6, data, 0),"data":data})) != null ? stack1 : "");
},"4":function(container,depth0,helpers,partials,data) {
    return "        <div class=\"alert status alert-info\">\n            <span class=\"icon big spin\"><svg><use href=\"/static/icons/endi.svg#cog\"></use></svg></span>\n            <p>\n	            <b>L’import est en cours…</b>\n            </p>\n";
},"6":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.failed : depth0),{"name":"if","hash":{},"fn":container.program(7, data, 0),"inverse":container.program(9, data, 0),"data":data})) != null ? stack1 : "");
},"7":function(container,depth0,helpers,partials,data) {
    return "        <div class=\"alert status alert-danger\">\n            <span class=\"icon big\"><svg><use href=\"/static/icons/endi.svg#danger\"></use></svg></span>\n            <p>\n	            <b>L’import a échoué</b>\n            </p>\n";
},"9":function(container,depth0,helpers,partials,data) {
    return "        <div class=\"alert status alert-success\">\n             <span class=\"icon big\"><svg><use href=\"/static/icons/endi.svg#check\"></use></svg></span>\n            <p>\n	            <b>L'import s'est déroulé avec succès</b>\n            </p>\n";
},"11":function(container,depth0,helpers,partials,data) {
    var stack1, helper, options, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression, alias5=helpers.blockHelperMissing, buffer = 
  "	<div class='content_vertical_padding'>\n		<div class='layout flex two_cols'>\n			<div>\n				<h4>Messages</h4>\n				"
    + alias4(((helper = (helper = helpers.message || (depth0 != null ? depth0.message : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"message","hash":{},"data":data}) : helper)))
    + "\n";
  stack1 = ((helper = (helper = helpers.has_message || (depth0 != null ? depth0.has_message : depth0)) != null ? helper : alias2),(options={"name":"has_message","hash":{},"fn":container.noop,"inverse":container.program(12, data, 0),"data":data}),(typeof helper === alias3 ? helper.call(alias1,options) : helper));
  if (!helpers.has_message) { stack1 = alias5.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  buffer += "			</div>\n			<div>\n				<h4>Erreurs</h4>\n				"
    + alias4(((helper = (helper = helpers.err_message || (depth0 != null ? depth0.err_message : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"err_message","hash":{},"data":data}) : helper)))
    + "\n";
  stack1 = ((helper = (helper = helpers.has_err_message || (depth0 != null ? depth0.has_err_message : depth0)) != null ? helper : alias2),(options={"name":"has_err_message","hash":{},"fn":container.noop,"inverse":container.program(14, data, 0),"data":data}),(typeof helper === alias3 ? helper.call(alias1,options) : helper));
  if (!helpers.has_err_message) { stack1 = alias5.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  buffer += "			</div>\n	    </div>\n	</div>\n	<div class=\"content_vertical_padding separate_bottom\">\n		<h4>Télécharger des données</h4>\n		<div class='layout flex two_cols'>\n";
  stack1 = ((helper = (helper = helpers.has_unhandled_datas || (depth0 != null ? depth0.has_unhandled_datas : depth0)) != null ? helper : alias2),(options={"name":"has_unhandled_datas","hash":{},"fn":container.program(16, data, 0),"inverse":container.noop,"data":data}),(typeof helper === alias3 ? helper.call(alias1,options) : helper));
  if (!helpers.has_unhandled_datas) { stack1 = alias5.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  stack1 = ((helper = (helper = helpers.has_errors || (depth0 != null ? depth0.has_errors : depth0)) != null ? helper : alias2),(options={"name":"has_errors","hash":{},"fn":container.program(18, data, 0),"inverse":container.noop,"data":data}),(typeof helper === alias3 ? helper.call(alias1,options) : helper));
  if (!helpers.has_errors) { stack1 = alias5.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  return buffer + "		</div>\n    </div>\n";
},"12":function(container,depth0,helpers,partials,data) {
    return "				<em>Aucun message n’a été retourné</em>\n";
},"14":function(container,depth0,helpers,partials,data) {
    return "				<em>Aucune erreur n’a été retournée</em>\n";
},"16":function(container,depth0,helpers,partials,data) {
    var helper;

  return "			<div>\n				Télécharger les données du fichier qui n’ont pas été importées&nbsp;:\n				<a class='btn btn-warning' href=\""
    + container.escapeExpression(((helper = (helper = helpers.url || (depth0 != null ? depth0.url : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"url","hash":{},"data":data}) : helper)))
    + "?action=unhandled.csv\">Télécharger</a>\n			</div>\n";
},"18":function(container,depth0,helpers,partials,data) {
    var helper;

  return "			<div>\n				Télécharger les lignes du fichier contenant des erreurs&nbsp;:\n				<a class='btn btn-danger' href=\""
    + container.escapeExpression(((helper = (helper = helpers.url || (depth0 != null ? depth0.url : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"url","hash":{},"data":data}) : helper)))
    + "?action=errors.csv\">Télécharger</a>\n			</div>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<h1>Import de données</h1>\n<div class=\"popup_content\">\n	<div class='content_vertical_padding'>\n		<dl class=\"dl-horizontal\">\n			<dt>Identifiant de la tâche</dt><dd>"
    + alias4(((helper = (helper = helpers.jobid || (depth0 != null ? depth0.jobid : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"jobid","hash":{},"data":data}) : helper)))
    + "</dd>\n			<dt>Initialisée le</dt><dd>"
    + alias4(((helper = (helper = helpers.created_at || (depth0 != null ? depth0.created_at : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"created_at","hash":{},"data":data}) : helper)))
    + "</dd>\n			<dt>Mise à jour le</dt><dd>"
    + alias4(((helper = (helper = helpers.updated_at || (depth0 != null ? depth0.updated_at : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"updated_at","hash":{},"data":data}) : helper)))
    + "</dd>\n		</dl>\n	</div>\n	<div class='content_vertical_padding separate_bottom'>\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.waiting : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(3, data, 0),"data":data})) != null ? stack1 : "")
    + "		</div>\n	</div>\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.finished : depth0),{"name":"if","hash":{},"fn":container.program(11, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "</div>\n";
},"useData":true});
templates['file_generation.mustache'] = template({"1":function(container,depth0,helpers,partials,data) {
    return "        <div class=\"alert status\">\n            <span class=\"icon big\"><svg><use href=\"/static/icons/endi.svg#clock\"></use></svg></span>\n            <p>\n            	<b>La génération est en attente de traitement</b>\n            </p>\n";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.running : depth0),{"name":"if","hash":{},"fn":container.program(4, data, 0),"inverse":container.program(6, data, 0),"data":data})) != null ? stack1 : "");
},"4":function(container,depth0,helpers,partials,data) {
    return "        <div class=\"alert status alert-info\">\n            <span class=\"icon big spin\"><svg><use href=\"/static/icons/endi.svg#cog\"></use></svg></span>\n            <p>\n            	<b>La génération est en cours…</b>\n            </p>\n";
},"6":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.failed : depth0),{"name":"if","hash":{},"fn":container.program(7, data, 0),"inverse":container.program(9, data, 0),"data":data})) != null ? stack1 : "");
},"7":function(container,depth0,helpers,partials,data) {
    return "        <div class=\"alert status alert-danger\">\n            <span class=\"icon big\"><svg><use href=\"/static/icons/endi.svg#danger\"></use></svg></span>\n            <p>\n	            <b>La génération de fichier a échoué</b>\n            </p>\n";
},"9":function(container,depth0,helpers,partials,data) {
    return "        <div class=\"alert status alert-success\">\n             <span class=\"icon big\"><svg><use href=\"/static/icons/endi.svg#check\"></use></svg></span>\n            <p>\n	            <b>La génération de fichier s'est déroulée avec succès</b>\n            </p>\n";
},"11":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.filename : depth0),{"name":"if","hash":{},"fn":container.program(12, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"12":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "			<script type='text/javascript'>\n				var win = window.open(\"/cooked/"
    + alias4(((helper = (helper = helpers.filename || (depth0 != null ? depth0.filename : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"filename","hash":{},"data":data}) : helper)))
    + "\", \"_self\");\n				// setTimeout(function(){win.close(); window.close()}, 1000);\n			</script>\n			<br>\n			<a href=\"/cooked/"
    + alias4(((helper = (helper = helpers.filename || (depth0 != null ? depth0.filename : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"filename","hash":{},"data":data}) : helper)))
    + "\" target=\"_blank\" class=\"btn btn-primary\">\n				<svg><use href=\"/static/icons/endi.svg#download\"></use></svg>\n				Télécharger\n			</a>\n";
},"14":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return "	<div class='content_vertical_padding'>\n		<div class='layout flex two_cols'>\n			<div>\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.has_message : depth0),{"name":"if","hash":{},"fn":container.program(15, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "			</div>\n			<div>\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.has_err_message : depth0),{"name":"if","hash":{},"fn":container.program(17, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "			</div>\n		</div>\n	</div>\n";
},"15":function(container,depth0,helpers,partials,data) {
    var helper;

  return "				<h4>Messages</h4>\n				"
    + container.escapeExpression(((helper = (helper = helpers.message || (depth0 != null ? depth0.message : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"message","hash":{},"data":data}) : helper)))
    + "\n";
},"17":function(container,depth0,helpers,partials,data) {
    var helper;

  return "				<h4>Erreurs</h4>\n				"
    + container.escapeExpression(((helper = (helper = helpers.err_message || (depth0 != null ? depth0.err_message : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"err_message","hash":{},"data":data}) : helper)))
    + "\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<h1>\n	Génération de fichier\n	<button class='btn icon only unstyled' onclick=\"win.close();\" title=\"Fermer cette fenêtre\" aria-label=\"Fermer cette fenêtre\">\n		<svg><use href=\"/static/icons/endi.svg#times\"></use></svg>\n	</button>\n</h1>\n<div class=\"popup_content\">\n	<div class='content_vertical_padding'>\n		<dl class=\"dl-horizontal\">\n			<dt>Identifiant de la tâche</dt><dd>"
    + alias4(((helper = (helper = helpers.jobid || (depth0 != null ? depth0.jobid : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"jobid","hash":{},"data":data}) : helper)))
    + "</dd>\n			<dt>Initialisée le</dt><dd>"
    + alias4(((helper = (helper = helpers.created_at || (depth0 != null ? depth0.created_at : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"created_at","hash":{},"data":data}) : helper)))
    + "</dd>\n			<dt>Mise à jour le</dt><dd>"
    + alias4(((helper = (helper = helpers.updated_at || (depth0 != null ? depth0.updated_at : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"updated_at","hash":{},"data":data}) : helper)))
    + "</dd>\n		</dl>\n	</div>\n	<div class='content_vertical_padding separate_bottom'>\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.waiting : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(3, data, 0),"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.finished : depth0),{"name":"if","hash":{},"fn":container.program(11, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "        </div>\n    </div>\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.finished : depth0),{"name":"if","hash":{},"fn":container.program(14, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "</div>\n";
},"useData":true});
templates['mailing.mustache'] = template({"1":function(container,depth0,helpers,partials,data) {
    return "        <div class=\"alert status\">\n            <span class=\"icon big\"><svg><use href=\"/static/icons/endi.svg#clock\"></use></svg></span>\n            <p>\n	            <b>L’envoi est en attente de traitement</b>\n            </p>\n";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.running : depth0),{"name":"if","hash":{},"fn":container.program(4, data, 0),"inverse":container.program(6, data, 0),"data":data})) != null ? stack1 : "");
},"4":function(container,depth0,helpers,partials,data) {
    return "        <div class=\"alert status alert-info\">\n            <span class=\"icon big spin\"><svg><use href=\"/static/icons/endi.svg#cog\"></use></svg></span>\n            <p>\n	            <b>L’envoi est en cours…</b>\n            </p>\n";
},"6":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.failed : depth0),{"name":"if","hash":{},"fn":container.program(7, data, 0),"inverse":container.program(9, data, 0),"data":data})) != null ? stack1 : "");
},"7":function(container,depth0,helpers,partials,data) {
    return "        <div class=\"alert status alert-danger\">\n            <span class=\"icon big\"><svg><use href=\"/static/icons/endi.svg#danger\"></use></svg></span>\n            <p>\n	            <b>L’envoi a échoué</b>\n            </p>\n";
},"9":function(container,depth0,helpers,partials,data) {
    return "        <div class=\"alert status alert-success\">\n             <span class=\"icon big\"><svg><use href=\"/static/icons/endi.svg#check\"></use></svg></span>\n            <p>\n	            <b>L’envoi s'est déroulé avec succès</b>\n            </p>\n";
},"11":function(container,depth0,helpers,partials,data) {
    var stack1, helper, options, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression, alias5=helpers.blockHelperMissing, buffer = 
  "	<div class='content_vertical_padding'>\n		<div class='layout flex two_cols'>\n			<div>\n				<h4>Messages</h4>\n				"
    + alias4(((helper = (helper = helpers.message || (depth0 != null ? depth0.message : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"message","hash":{},"data":data}) : helper)))
    + "\n";
  stack1 = ((helper = (helper = helpers.has_message || (depth0 != null ? depth0.has_message : depth0)) != null ? helper : alias2),(options={"name":"has_message","hash":{},"fn":container.noop,"inverse":container.program(12, data, 0),"data":data}),(typeof helper === alias3 ? helper.call(alias1,options) : helper));
  if (!helpers.has_message) { stack1 = alias5.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  buffer += "			</div>\n			<div>\n				<h4>Erreurs</h4>\n				"
    + alias4(((helper = (helper = helpers.err_message || (depth0 != null ? depth0.err_message : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"err_message","hash":{},"data":data}) : helper)))
    + "\n";
  stack1 = ((helper = (helper = helpers.has_err_message || (depth0 != null ? depth0.has_err_message : depth0)) != null ? helper : alias2),(options={"name":"has_err_message","hash":{},"fn":container.noop,"inverse":container.program(14, data, 0),"data":data}),(typeof helper === alias3 ? helper.call(alias1,options) : helper));
  if (!helpers.has_err_message) { stack1 = alias5.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  return buffer + "			</div>\n	    </div>\n	</div>\n";
},"12":function(container,depth0,helpers,partials,data) {
    return "				<em>Aucun message n’a été retourné</em>\n";
},"14":function(container,depth0,helpers,partials,data) {
    return "				<em>Aucune erreur n’a été retournée</em>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<h1>\n	Envoi de document par mail\n</h1>\n<div class=\"popup_content\">\n	<div class='content_vertical_padding'>\n		<dl class=\"dl-horizontal\">\n			<dt>Identifiant de la tâche</dt><dd>"
    + alias4(((helper = (helper = helpers.jobid || (depth0 != null ? depth0.jobid : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"jobid","hash":{},"data":data}) : helper)))
    + "</dd>\n			<dt>Initialisée le</dt><dd>"
    + alias4(((helper = (helper = helpers.created_at || (depth0 != null ? depth0.created_at : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"created_at","hash":{},"data":data}) : helper)))
    + "</dd>\n			<dt>Mise à jour le</dt><dd>"
    + alias4(((helper = (helper = helpers.updated_at || (depth0 != null ? depth0.updated_at : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"updated_at","hash":{},"data":data}) : helper)))
    + "</dd>\n		</dl>\n	</div>\n	<div class='content_vertical_padding separate_bottom'>\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.waiting : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(3, data, 0),"data":data})) != null ? stack1 : "")
    + "		</div>\n	</div>\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.finished : depth0),{"name":"if","hash":{},"fn":container.program(11, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "</div>\n";
},"useData":true});
})();