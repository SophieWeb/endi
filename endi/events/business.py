import logging

from sqlalchemy.event import listen

from endi.models.training import BusinessBPFData
from endi.models.task.invoice import Invoice
from endi.models.project.business import Business
from endi.models.services.business_status import BusinessStatusService
from endi.utils.datetimes import get_current_year
from endi.events.status_changed import StatusChanged



logger = logging.getLogger(__name__)


class BpfDataModified:
    def __init__(self, request, business_id):
        self.request = request
        self.business_id = business_id


def on_bpf_data_changed(event):
    if isinstance(event, BpfDataModified):
        business = Business.get(event.business_id)
        if business:
            BusinessStatusService.update_bpf_indicator(business)
    elif isinstance(event, StatusChanged):
        if event.status == 'valid':
            if event.node_type in ('invoice', 'cancelinvoice'):
                business = event.node.business
                if business.business_type.bpf_related:
                    BusinessStatusService.update_bpf_indicator(business)



def includeme(config):
    config.add_subscriber(on_bpf_data_changed, BpfDataModified)
    config.add_subscriber(on_bpf_data_changed, StatusChanged)
