"""
Events used while handling expenses :
    Send email

"""
import datetime
import logging

from zope.interface import implementer

from endi_base.mail import format_link
from endi_base.utils.date import (
    format_date,
)
from endi.interfaces import IMailEventWrapper
from endi.utils.strings import (
    format_account,
)

logger = logging.getLogger(__name__)

# Events for which a mail will be sended
EVENTS = {
    "valid": "validée",
    "invalid": "invalidée",
    "paid": "partiellement payée",
    "resulted": "payée",
}

MAIL_TMPL = """
Bonjour {owner},

La note de dépenses de {owner} pour la période {date} a été {status_verb}.

Vous pouvez la consulter ici :
{addr}

Commentaires associés au document :
    {comment}"""


EXPENSE_NOTIFY_STATUS = dict(
    (
        ("invalid", "Invalidée par {0} le {1}"),
        ("valid", "Validée par {0} le {1}"),
        ('paid', "Paiement partiel notifié par {0} le {1}"),
        ('resulted', "Paiement notifié par {0} le {1}")
    )
)


@implementer(IMailEventWrapper)
class ExpenseMailStatusChangedWrapper:

    def __init__(self, event):
        self.event = event
        self.expense = self.event.node
        self.request = event.request
        self.settings = self.request.registry.settings

    @property
    def recipients(self):
        """
            return the recipients' emails
        """
        if self.event.node.user.email:
            email = [self.event.node.user.email]
        else:
            email = []
        return email

    @property
    def sendermail(self):
        """
            Return the sender's email
        """
        if 'mail.default_sender' in self.settings:
            mail = self.settings['mail.default_sender']
        else:
            logger.error(
                "You should set the default mail sender 'mail.default_sender'"
                " in your ini file"
            )
            mail = "Unknown"
        return mail

    def format_expense_notification(self):
        """
        Return a formatted string for expense status notification
        """
        status_str = EXPENSE_NOTIFY_STATUS.get(self.event.status)
        account_label = format_account(self.request.user)
        date_label = format_date(datetime.date.today())

        if status_str is not None:
            return status_str.format(account_label, date_label)
        else:
            return ""

    @property
    def subject(self):
        """
            return the subject of the email
        """
        subject = "Notes de dépense de {0} : {1}".format(
            format_account(self.event.node.user),
            self.format_expense_notification()
        )
        return subject

    def _find_comment(self):
        """
        Collect the latest comment for the current node
        """
        if self.event.comment:
            return self.event.comment
        else:
            status_history = self.expense.statuses
            if len(status_history) > 0:
                return status_history[0].comment
            else:
                return "Aucun"

    @property
    def body(self):
        """
            return the body of the email
        """
        owner = format_account(self.event.node.user)
        date = "{0}/{1}".format(self.event.node.month, self.event.node.year)
        status_verb = get_status_verb(self.event.status)
        addr = self.request.route_url("/expenses/{id}", id=self.event.node.id)
        addr = format_link(self.settings, addr)

        return MAIL_TMPL.format(
            owner=owner,
            addr=addr,
            date=date,
            status_verb=status_verb,
            comment=self._find_comment(),
        )

    @staticmethod
    def get_attachment():
        """
            Return the file data to be sent with the email
        """
        return None

    def is_key_event(self):
        """
            Return True if the new status requires a mail to be sent
        """
        if self.event.status in list(EVENTS.keys()):
            return True
        else:
            return False


def get_status_verb(status):
    """
        Return the verb associated to the current status
    """
    return EVENTS.get(status, "")
