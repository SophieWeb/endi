"""
Handle task (invoice/estimation) related events
"""
import logging
from zope.interface import implementer

from endi.utils.strings import (
    format_status,
    format_account,
)

from endi_base.mail import (
    format_link,
)
from endi.interfaces import IMailEventWrapper
from endi.views.task.utils import get_task_url

logger = logging.getLogger(__name__)

# Events for which a mail will be sended
EVENTS = {
    "valid": "validé",
    "invalid": "invalidé",
    "paid": "partiellement payé",
    "resulted": "payé"
}

SUBJECT_TMPL = "{docname} ({customer}) : {statusstr}"

MAIL_TMPL = """
Bonjour {username},

{docname} {docnumber} du dossier {project} avec le client {customer} \
a été {status_verb}{gender}.

Vous pouvez {determinant} consulter ici :
{addr}

Commentaires associés au document :
    {comment}"""


@implementer(IMailEventWrapper)
class TaskMailStatusChangedWrapper:

    def __init__(self, event):
        self.event = event
        self.request = event.request
        self.status = event.status
        # Silly hack :
        # When a payment is registered, the new status is "paid",
        # if the resulted box has been checked, it's set to resulted later on.
        # So here, we got the paid status, but in reality, the status has
        # already been set to resulted. This hack avoid to send emails with the
        # wrong message
        if self.status == 'paid' and self.event.node.paid_status == 'resulted':
            self.status = 'resulted'
        self.settings = self.request.registry.settings

    @property
    def recipients(self):
        """
            return the recipients' emails
        """
        if self.event.node.company.email:
            email = [self.event.node.company.email]
        elif self.event.node.owner and self.event.node.owner.email:
            email = [self.event.node.owner.email]
        else:
            email = []
        return email

    @property
    def sendermail(self):
        """
            Return the sender's email
        """
        if 'mail.default_sender' in self.settings:
            mail = self.settings['mail.default_sender']
        else:
            logger.error(
                "You should set the default mail sender 'mail.default_sender'"
                " in your ini file"
            )
            mail = "Unknown"
        return mail

    @property
    def subject(self):
        """
            return the subject of the email
        """
        return SUBJECT_TMPL.format(
            docname=self.event.node.name,
            customer=self.event.node.customer.label,
            statusstr=format_status(self.event.node),
        )

    @property
    def body(self):
        """
            return the body of the email
        """
        status_verb = get_status_verb(self.status)

        # If the document is validated, we directly send the link to the pdf
        # file
        if self.status == 'valid':
            suffix = ".pdf"
        else:
            suffix = ""

        addr = get_task_url(
            self.request,
            self.event.node,
            suffix=suffix,
            absolute=True,
        )
        addr = format_link(self.settings, addr)

        docnumber = self.event.node.internal_number.lower()
        customer = self.event.node.customer.label
        project = self.event.node.project.name.capitalize()
        if self.event.node.type_ == 'invoice':
            docname = "La facture"
            gender = "e"
            determinant = "la"
        elif self.event.node.type_ == 'internalinvoice':
            docname = "La facture interne"
            gender = "e"
            determinant = "la"
        elif self.event.node.type_ == 'cancelinvoice':
            docname = "L'avoir"
            gender = ""
            determinant = "le"
        elif self.event.node.type_ == 'estimation':
            docname = "Le devis"
            gender = ""
            determinant = "le"
        elif self.event.node.type_ == 'internalestimation':
            docname = "Le devis interne"
            gender = ""
            determinant = "le"
        if self.event.node.status_comment:
            comment = self.event.node.status_comment
        else:
            comment = "Aucun"

        username = format_account(self.event.node.owner, reverse=False)
        return MAIL_TMPL.format(
                determinant=determinant,
                username=username,
                docname=docname,
                docnumber=docnumber,
                customer=customer,
                project=project,
                status_verb=status_verb,
                gender=gender,
                addr=addr,
                comment=comment)

    def get_attachment(self):
        """
            Return the file data to be sent with the email
        """
        return None

    def is_key_event(self):
        """
        Return True if the new status requires a mail to be sent
        """
        if self.status in list(EVENTS.keys()) \
                and not self.event.node.type_ == 'cancelinvoice':
            return True
        else:
            return False


def get_status_verb(status):
    """
    Return the verb associated to the current status
    """
    return EVENTS.get(status, "")


def on_status_changed_alert_related_business(event):
    """
    Alert the related business on Invoice status change

    :param event: A StatusChanged instance with an Invoice attached
    """
    business = event.node.business
    if business is not None:
        logger.info(
            "+ Status Changed : updating business {} invoicing "
            "status".format(
                business.id
            )
        )
        business.status_service.on_task_status_change(
            business, event.node, event.status
        )
