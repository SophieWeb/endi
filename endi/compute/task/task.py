"""
    Task computing tool
    Used to compute invoice, estimation or cancelinvoice totals
"""
import math
import operator
import logging

from endi.models.tva import Tva
from endi.compute import math_utils
from endi.compute.task import (
    TaskComputeMixin,
    GroupComputeMixin,
    LineComputeMixin,
    DiscountLineMixin,
)

AMOUNT_PRECISION = 5
AMOUNT_RATIO = pow(10, AMOUNT_PRECISION)
# 5€ => 500000 in db format
PAYMENT_EPSILON = 5 * 10 ** AMOUNT_PRECISION
PAYMENT_SUM_EPSILON = 0.1 * 10 ** AMOUNT_PRECISION

logger = logging.getLogger(__name__)


def get_default_tva():
    """
        Return the default tva
    """
    try:
        default_tva = Tva.get_default()
    except Exception:
        default_tva = None

    if default_tva:
        return default_tva.value
    else:
        return 1960


class TaskCompute(TaskComputeMixin):
    """
        class A(TaskCompute):
            pass

        A.total()
    """
    task = None

    def __init__(self, task):
        TaskComputeMixin.__init__(self, task)

    def total_ht(self):
        """
            compute the HT amount
        """
        expenses_ht = getattr(self.task, "expenses_ht") or 0  # TODO refactore
        total_ht = self.groups_total_ht() - \
            self.discount_total_ht() + \
            expenses_ht
        return self.floor(total_ht)

    def get_tvas(self):
        """
            return a dict with the tvas amounts stored by tva
            {1960:450.56, 700:45}
        """
        expense = None

        ret_dict = {}
        for group in self.task.line_groups:
            for key, value in list(group.get_tvas().items()):
                val = ret_dict.get(key, 0)
                val += value
                ret_dict[key] = val

        for discount in self.task.discounts:
            val = ret_dict.get(discount.tva, 0)
            val -= discount.tva_amount()
            ret_dict[discount.tva] = val

        expenses_ht = getattr(self.task, "expenses_ht", 0)
        tva_amount = 0
        if expenses_ht != 0:
            expense = self.get_expense_ht()
            tva_amount = expense.tva_amount()

        if tva_amount != 0 and expense is not None:
            val = ret_dict.get(expense.tva, 0)

            val += expense.tva_amount()
            ret_dict[expense.tva] = val

        for key in ret_dict:
            ret_dict[key] = self.floor(ret_dict[key])
        return ret_dict

    def get_tvas_by_product(self):
        """
        Return tvas stored by product type
        """
        ret_dict = {}
        for group in self.task.line_groups:
            for key, value in list(group.get_tvas_by_product().items()):
                val = ret_dict.get(key, 0)
                val += value
                ret_dict[key] = val

        for discount in self.task.discounts:
            val = ret_dict.get("rrr", 0)
            val += discount.tva_amount()
            ret_dict["rrr"] = val

        expense_ht = getattr(self.task, "expenses_ht", 0)
        tva_amount = 0
        if expense_ht != 0:
            expense = self.get_expense_ht()
            tva_amount = expense.tva_amount()

        if tva_amount > 0:
            val = ret_dict.get("expense", 0)
            val += tva_amount
            ret_dict["expense"] = val

        for key in ret_dict:
            ret_dict[key] = self.floor(ret_dict[key])
        return ret_dict

    def tva_amount(self):
        """
            Compute the sum of the TVAs amount of TVA
        """
        return self.floor(
            sum(tva for tva in list(self.get_tvas().values()))
        )

    def no_tva(self):
        """
            return True if all the tvas are below 0
        """
        ret = True
        for key in self.get_tvas():
            if key >= 0:
                ret = False
                break
        return ret

    def total_ttc(self):
        """
            Compute the TTC total
        """
        return self.total_ht() + self.tva_amount()

    def total(self):
        """
            Compute TTC after tax removing
        """
        return self.total_ttc() + self.expenses_amount()

    def expenses_amount(self):
        """
            return the amount of the expenses
        """
        expenses = self.task.expenses or 0
        result = int(expenses)
        return result

    def get_expenses_tva(self):
        """
            Return the tva for the HT expenses
        """
        expenses_tva = getattr(self.task, "expenses_tva", -1)
        if expenses_tva == -1:
            self.task.expenses_tva = get_default_tva()
        return self.task.expenses_tva

    def get_expense_ht(self):
        """
        Return a line object for the HT expense handling
        """
        # We should not use a TaskLine here, but a TaskLineCompute
        from endi.models.task import TaskLine
        return TaskLine(
            tva=self.get_expenses_tva(),
            cost=self.task.expenses_ht,
            quantity=1,
            mode='ht'
        )

    def tva_ht_parts(self):
        """
        Return a dict with the HT amounts stored by corresponding tva value
        dict(tva=ht_tva_part,)
        for each tva value
        """
        ret_dict = {}
        lines = []
        for group in self.task.line_groups:
            lines.extend(group.lines)
        ret_dict = self.add_ht_by_tva(ret_dict, lines)
        ret_dict = self.add_ht_by_tva(
            ret_dict, self.task.discounts, operator.sub
        )
        expense = self.get_expense_ht()
        if expense.cost != 0:
            ret_dict = self.add_ht_by_tva(ret_dict, [expense])
        return ret_dict

    def tva_ttc_parts(self):
        """
        Return a dict with TTC amounts stored by corresponding tva
        """
        ret_dict = {}
        ht_parts = self.tva_ht_parts()
        tva_parts = self.get_tvas()

        for tva_value, amount in ht_parts.items():
            ret_dict[tva_value] = amount + tva_parts.get(tva_value, 0)
        return ret_dict


class InvoiceCompute:
    """
        Invoice computing object
        Handles payments
    """
    task = None

    def __init__(self, task):
        self.task = task

    def payments_sum(self, year: int = None):
        """
        Return the amount covered by the recorded payments

        :param year: limit the considered payments to this year
        """
        return sum([
            payment.amount for payment in self.task.payments
            if payment.date.year == year or year is None
        ])

    def cancelinvoice_amount(self, year: int = None):
        """
        Return the amount covered by th associated cancelinvoices

        :param year: limit the considered cancel invoices to this year
        """
        result = 0
        for cancelinvoice in self.task.cancelinvoices:
            year_match = year == cancelinvoice.date.year
            if cancelinvoice.status == 'valid' and (
                    year is None or year_match
            ):
                # cancelinvoice total is negative
                result += -1 * cancelinvoice.total()
        return result

    def paid(self, year: int = None):
        """
        return the amount that has already been paid

        :param year: limit the considered payments to one year
        """
        return self.payments_sum(year) + self.cancelinvoice_amount(year)

    def topay(self):
        """
        Return the amount that still need to be paid

        Compute the sum of the payments and what's part of a valid
        cancelinvoice
        """
        result = self.task.total() - self.paid()
        return result

    def tva_paid_parts(self) -> dict:
        """
        return the amounts already paid by tva

        :returns: A dict {tva value: paid amount}
        """
        result = {}
        for payment in self.task.payments:
            if payment.tva is not None:
                key = payment.tva.value
            else:
                key = list(self.task.tva_ht_parts().keys())[0]

            result.setdefault(key, 0)
            result[key] += payment.amount

        return result

    def tva_cancelinvoice_parts(self) -> dict:
        """
        Returns the amounts already paid through cancelinvoices by tva

        :returns: A dict {tva value: canceled amount}
        """
        result = {}
        for cancelinvoice in self.task.cancelinvoices:
            if cancelinvoice.status == 'valid':
                ttc_parts = cancelinvoice.tva_ttc_parts()
                for key, value in list(ttc_parts.items()):
                    if key in result:
                        result[key] += value
                    else:
                        result[key] = value
        return result

    def topay_by_tvas(self) -> dict:
        """
        Returns the amount to pay by tva part

        :returns: A dict {tva value: to pay amount}
        """
        result = {}
        paid_parts = self.tva_paid_parts()
        cancelinvoice_tva_parts = self.tva_cancelinvoice_parts()
        for tva_value, amount in self.task.tva_ttc_parts().items():
            val = amount
            val = val - paid_parts.get(tva_value, 0)
            val = val + cancelinvoice_tva_parts.get(tva_value, 0)
            result[tva_value] = val
        return result

    def round_payment_amount(self, payment_amount):
        """
        Returns a rounded value of a payment.

        :param int payment_amount: Amount in biginteger representation
        """
        return math_utils.floor_to_precision(
            payment_amount,
            precision=2,
        )

    def _get_payment_excess(self, payment_amount, invoice_topay):
        # Is there an excess of payment ?
        payment_excess = None
        if math.fabs(payment_amount) > math.fabs(invoice_topay):
            payment_excess = payment_amount - invoice_topay
            if math.fabs(payment_excess) > PAYMENT_EPSILON:
                raise Exception(
                    "Encaissement supérieur (ou inférieur) de {}€ au montant "
                    "de la facture".format(PAYMENT_EPSILON)
                )
        return payment_excess

    def _is_last_payment(self, payment_amount, invoice_topay):
        """
        Check if the payment amount covers what is to pay

        :rtype: bool
        """
        # Different TVA rates are still to be paid
        if invoice_topay < 0:
            last_payment = (payment_amount <= invoice_topay)
        else:
            last_payment = (payment_amount >= invoice_topay)
        return last_payment

    def _get_single_tva_payment(self, payment_amount, topay_by_tvas):
        """
        Return payment list in case of single tva invoice
        """
        # Round the amount in case the user put a number
        # with more than 2 digits
        payment_amount = self.round_payment_amount(payment_amount)

        tva_value = list(topay_by_tvas)[0][0]
        tva_id = Tva.by_value(tva_value).id

        return [{"tva_id": tva_id, "amount": payment_amount}]

    def _get_payments_by_tva(
        self,
        payment_amount,
        invoice_topay,
        payment_excess,
        topay_by_tvas
    ):
        """
        Split a payment in separate payments by tva

        :rtype: dict
        """
        result = []
        nb_tvas = len(topay_by_tvas)
        last_payment = self._is_last_payment(payment_amount, invoice_topay)

        i_tva = 0
        already_paid = 0
        for tva_value, value in topay_by_tvas:
            i_tva += 1
            tva = Tva.by_value(tva_value)
            ratio = value / invoice_topay

            amount = 0
            if not last_payment:
                if i_tva < nb_tvas:
                    # Tva intermédiaire, on utilise le ratio
                    amount = ratio * payment_amount
                    already_paid += amount
                    # It has to be rounded otherwise last TVA calculation
                    # will be wrong
                    already_paid = self.round_payment_amount(already_paid)
                else:
                    # Pour la dernière tva de la liste, on utilise une
                    # soustraction pur éviter les problèmes d'arrondi
                    amount = payment_amount - already_paid
            else:
                amount = value
                # On distribue également l'excès sur les différents taux de tva
                if payment_excess:
                    excess = payment_excess * ratio
                    amount = amount + excess

            amount = self.round_payment_amount(amount)

            if amount != 0:
                result.append({"tva_id": tva.id, "amount": amount})
        return result

    def compute_payments(self, payment_amount):
        """
        Returns payments corresponding to the payment amount
        If there is just one TVA rate left to be paid in the invoice it
        returns just one payment.
        If there are different TVA rate left to be paid in the invoice
        it returns a payment for each TVA rate

        :param int payment_amount: Amount coming from the UI (in biginteger
        format)

        :rtype: array
        :returns: [{'tva_id': <Tva>.id, 'amount': 123}, ...]
        """
        invoice_topay = self.topay()
        payment_excess = self._get_payment_excess(
            payment_amount, invoice_topay
        )

        topay_by_tvas = self.topay_by_tvas().items()
        nb_tvas = len(topay_by_tvas)

        if nb_tvas == 1:
            result = self._get_single_tva_payment(
                payment_amount, topay_by_tvas
            )
        else:
            result = self._get_payments_by_tva(
                payment_amount,
                invoice_topay,
                payment_excess,
                topay_by_tvas,
            )

        # Return an array of dict: Array({amount: ,tva_id: })
        return result


class EstimationCompute:
    """
        Computing class for estimations
        Adds the ability to compute deposit amounts ...
    """
    task = None

    def __init__(self, task):
        self.task = task

    def get_default_tva(self):
        """
            Silly hack to get a default tva for deposit and intermediary
            payments (configured ttc)
        """
        tvas = list(self.task.get_tvas().keys())
        return tvas[0]

    def deposit_amounts(self):
        """
            Return the lines of the deposit for the different amount of tvas
        """
        ret_dict = {}
        for tva, total_ht in list(self.task.tva_ht_parts().items()):
            ret_dict[tva] = self.task.floor(
                math_utils.percentage(total_ht, self.task.deposit)
            )
        return ret_dict

    def deposit_amount(self):
        """
            Compute the amount of the deposit
        """
        import warnings
        warnings.warn("deprecated", DeprecationWarning)
        if self.task.deposit > 0:
            total = self.task.total_ht()
            return self.task.floor(total * int(self.task.deposit) / 100.0)
        return 0

    def get_nb_payment_lines(self):
        """
            Returns the number of payment lines configured
        """
        return len(self.task.payment_lines)

    def paymentline_amounts(self):
        """
        Compute payment lines amounts in case of equal payment repartition:

            when manualDeliverables is 0

        e.g :

            when the user has selected 3 time-payment

        :returns: A dict describing the payments {'tva1': amount1, 'tva2':
            amount2}
        """
        ret_dict = {}
        totals = self.task.tva_ht_parts()
        deposits = self.deposit_amounts()
        # num_parts set the number of equal parts
        num_parts = self.get_nb_payment_lines()
        for tva, total_ht in list(totals.items()):
            rest = total_ht - deposits[tva]
            line_ht = rest / num_parts
            ret_dict[tva] = line_ht
        return ret_dict

    def manual_payment_line_amounts(self):
        """
            Computes the ht and tva needed to reach each payment line total

            self.payment_lines are configured with TTC amounts


            return a list of dict:
                [{tva1:ht_amount, tva2:ht_amount}]
            each dict represents a configured payment line
        """
        # Cette méthode recompose un paiement qui a été configuré TTC, sous
        # forme de part HT + TVA au regard des différentes tva configurées dans
        # le devis
        ret_data = []
        parts = self.task.tva_ht_parts()
        # On enlève déjà ce qui est inclu dans l'accompte
        for tva, ht_amount in list(self.deposit_amounts().items()):
            parts[tva] -= ht_amount

        for payment in self.task.payment_lines[:-1]:
            payment_ttc = payment.amount
            payment_lines = {}

            items = list(parts.items())
            for tva, total_ht in items:
                payment_ht = math_utils.compute_ht_from_ttc(
                    payment_ttc,
                    tva,
                    False,
                    division_mode=(self.task.mode != 'ttc'),
                )
                if total_ht >= payment_ht:
                    # Le total ht de cette tranche de tva est suffisant pour
                    # recouvrir notre paiement
                    # on la récupère
                    payment_lines[tva] = payment_ht
                    # On enlève ce qu'on vient de prendre de la tranche de tva
                    # pour le calcul des autres paiements
                    parts[tva] = total_ht - payment_ht
                    ret_data.append(payment_lines)
                    break
                else:
                    # On a besoin d'une autre tranche de tva pour atteindre
                    # notre paiement, on prend déjà ce qu'il y a
                    payment_lines[tva] = parts.pop(tva)
                    # On enlève la part qu'on a récupéré dans cette tranche de
                    # tva du total de notre paiement
                    payment_ttc -= total_ht + math_utils.compute_tva(
                        total_ht,
                        tva,
                    )

        # Ce qui reste c'est donc pour notre facture de solde
        sold = parts
        ret_data.append(sold)
        return ret_data

    # Computations for estimation display
    def deposit_amount_ttc(self):
        """
            Return the ttc amount of the deposit (for estimation display)
        """
        from endi.models.task import TaskLine
        if self.task.deposit > 0:
            total_ttc = 0
            for tva, total_ht in list(self.deposit_amounts().items()):
                line = TaskLine(cost=total_ht, tva=tva)
                total_ttc += line.total()
            return self.task.floor(total_ttc)
        return 0

    def paymentline_amount_ttc(self):
        """
            Return the ttc amount of payment (in equal repartition)
        """
        from endi.models.task import TaskLine
        total_ttc = 0
        for tva, total_ht in list(self.paymentline_amounts().items()):
            line = TaskLine(cost=total_ht, tva=tva)
            total_ttc += self.task.floor(line.total())
        return total_ttc

    def sold(self):
        """
            Compute the sold amount to finish on an exact value
            if we divide 10 in 3, we'd like to have something like :
                3.33 3.33 3.34
            (for estimation display)
        """
        from endi.models.task import TaskLine
        result = 0
        total_ttc = self.task.total()
        deposit_ttc = self.deposit_amount_ttc()
        rest = total_ttc - deposit_ttc

        payment_lines_num = self.get_nb_payment_lines()
        if payment_lines_num == 1 or not self.get_nb_payment_lines():
            # No other payment line
            result = rest
        else:
            if self.task.manualDeliverables == 0:
                line_ttc = self.paymentline_amount_ttc()
                result = rest - ((payment_lines_num - 1) * line_ttc)
            else:
                sold_lines = self.manual_payment_line_amounts()[-1]
                result = 0
                for tva, total_ht in list(sold_lines.items()):
                    line = TaskLine(tva=tva, cost=total_ht)
                    result += line.total()

        return result


class GroupCompute(GroupComputeMixin):
    task_line_group = None

    def __init__(self, task_line_group):
        GroupComputeMixin.__init__(self, task_line_group)

    def total_ttc(self):
        """
        Returns the TTC total for this group
        """
        return self.total_ht() + self.tva_amount()


class LineCompute(LineComputeMixin):
    """
        Computing tool for line objects
    """
    task_line = None

    def __init__(self, task_line):
        LineComputeMixin.__init__(self, task_line)

    def unit_ht(self):
        """
        Unit Ht value

        :rtype: float
        """
        return self.task_line.cost

    def total_ht(self):
        """
            Compute the line's total
        """
        cost = self.task_line.cost or 0
        quantity = self._get_quantity()
        return cost * quantity

    def tva_amount(self):
        """
            compute the tva amount of a line
        """
        total_ht = self.total_ht()
        return math_utils.compute_tva(total_ht, self.task_line.tva)

    def total(self):
        """
            Compute the ttc amount of the line
        """
        return self.tva_amount() + self.total_ht()


class DiscountLineCompute(DiscountLineMixin):
    """
        Computing tool for discount_line objects
    """
    discount_line = None

    def __init__(self, discount_line):
        DiscountLineMixin.__init__(self, discount_line)

    def total_ht(self):
        return float(self.discount_line.amount)

    def tva_amount(self):
        """
            compute the tva amount of a line
        """
        total_ht = self.total_ht()
        return math_utils.compute_tva(total_ht, self.discount_line.tva)

    def total(self):
        return self.tva_amount() + self.total_ht()
