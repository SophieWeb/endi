"""
 Task compute methods and attributes for both ht and ttc mode
"""
import operator
from endi.compute import math_utils


class TaskComputeMixin:
    """
    Computing tool for both ttc and ht mode in tasks objects
    """
    __round_floor = False
    task = None

    def __init__(self, task):
        self.task = task

    def floor(self, amount):
        return math_utils.floor_to_precision(amount, self.__round_floor)

    def groups_total_ht(self):
        """
            compute the sum of the task lines total
        """
        return sum(group.total_ht() for group in self.task.line_groups)

    def discount_total_ht(self):
        """
            compute the discount total
        """
        return sum(line.total_ht() for line in self.task.discounts)

    @staticmethod
    def add_ht_by_tva(ret_dict, lines, operation=operator.add):
        """
            Add ht sums by tva to ret_dict for the given lines
        """
        for line in lines:
            val = ret_dict.get(line.get_tva(), 0)
            ht_amount = operation(val, line.total_ht())
            ret_dict[line.get_tva()] = ht_amount
        return ret_dict


class GroupComputeMixin:
    """
    Computing tool for both ttc and ht mode in group objects
    """
    task_line_group = None

    def __init__(self, task_line_group):
        self.task_line_group = task_line_group

    def get_tvas(self):
        """
            return a dict with the tvas amounts stored by tva
            {1960:450.56, 700:45}
        """
        ret_dict = {}
        for line in self.task_line_group.lines:
            val = ret_dict.get(line.tva, 0)
            val += line.tva_amount()
            ret_dict[line.tva] = val
        return ret_dict

    def get_tvas_by_product(self):
        """
            return a dict with the tvas amounts stored by product
            {1960:450.56, 700:45}
        """
        ret_dict = {}
        for line in self.task_line_group.lines:
            val = ret_dict.get(line.product.compte_cg, 0)
            val += line.tva_amount()
            ret_dict[line.product.compte_cg] = val
        return ret_dict

    def tva_amount(self):
        """
        Returns the TVA total for this group
        """
        return sum(tva for tva in list(self.get_tvas().values()))

    def total_ht(self):
        """
        Returns the ht total for this group
        """
        return sum(line.total_ht() for line in self.task_line_group.lines)


class LineComputeMixin:
    """
    Computing tool for both ttc and ht mode in task_line
    """
    task_line = None

    def __init__(self, task_line):
        self.task_line = task_line

    def get_tva(self):
        """
        Return the line task_line tva
        :return: int
        """
        return self.task_line.tva

    def _get_quantity(self):
        """
        Retrieve the configured quantity, returns 1 by default
        """
        quantity = getattr(self.task_line, "quantity", None)
        if quantity is None:
            quantity = 1
        return quantity


class DiscountLineMixin:
    """
    Computing tool for both ttc and ht mode in discount_line
    """
    discount_line = None

    def __init__(self, discount_line):
        self.discount_line = discount_line

    def get_tva(self):
        """
        Return the line discount_line tva
        :return: int
        """
        return self.discount_line.tva




