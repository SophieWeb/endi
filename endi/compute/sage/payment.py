import logging

from zope.interface import implementer

from endi.interfaces import ITreasuryProducer
from endi.compute.math_utils import (
    floor,
    compute_ht_from_ttc,
    compute_tva,
)
from .base import (
    double_lines,
    BaseSageBookEntryFactory,
    format_sage_date,
    filter_accounting_entry,
    MissingData,
)

logger = log = logging.getLogger(__name__)


class SagePaymentBase(BaseSageBookEntryFactory):
    static_columns = (
        'reference',
        'code_journal',
        'date',
        'mode',
        'libelle',
        'type_',
        "num_analytique",
    )

    variable_columns = (
        'compte_cg',
        'compte_tiers',
        'code_taxe',
        'debit',
        'credit',
    )

    _label_template_key = "bookentry_payment_label_template"

    def set_payment(self, payment):
        self.invoice = payment.invoice
        self.payment = payment
        self.company = self.invoice.company
        self.customer = self.invoice.customer

    @property
    def reference(self):
        if self.payment.bank_remittance_id is None:
            return "{}".format(self.invoice.official_number)
        else:
            return "{0}/{1}".format(
                self.invoice.official_number,
                self.payment.bank_remittance_id,
            )

    @property
    def code_journal(self):
        return self.payment.bank.code_journal

    @property
    def date(self):
        return format_sage_date(self.payment.date)

    @property
    def mode(self):
        return self.payment.mode

    @property
    def libelle(self):
        return self.label_template.format(
            company=self.company,
            invoice=self.invoice,
            payment=self.payment,
        )

    @property
    def num_analytique(self):
        """
            Return the analytic number common to all entries in the current
            export module
        """
        return self.company.code_compta


class SagePaymentMain(SagePaymentBase):
    """
    Main module for payment export
    """

    @double_lines
    def credit_client(self, val):
        entry = self.get_base_entry()
        entry.update(
            compte_cg=self.customer.get_general_account(
                prefix=self.config_key_prefix
            ),
            compte_tiers=self.customer.get_third_party_account(
                prefix=self.config_key_prefix
            ),
            credit=val,
        )
        return entry

    def _get_bank_cg(self):
        result = self.payment.bank.compte_cg
        if not result:
            raise MissingData(
                "Code banque non configuré pour l'encaissement {}".format(
                    self.payment.id
                )
            )
        return result

    @double_lines
    def debit_banque(self, val):
        entry = self.get_base_entry()
        entry.update(
            compte_cg=self._get_bank_cg(),
            debit=val,
        )
        return entry

    def _has_remittance(self):
        """
        Renvoie True si une remise en banque est associé au paiement
        """
        return self.payment.bank_remittance_id is not None

    def _should_write_debit(self):
        """
        Test si nous devons entréer une écriture de débit pour ce paiement
        """
        if not self._has_remittance():
            return True
        else:
            group_by_remittance = self.config.get(
                'receipts_group_by_remittance'
            )
            if group_by_remittance in ('0', None):
                return True
        return False

    def yield_entries(self):
        yield self.credit_client(self.payment.amount)
        if self._should_write_debit():
            yield self.debit_banque(self.payment.amount)


class InternalSagePaymentMain(SagePaymentMain):
    config_key_prefix = 'internal'

    @property
    def reference(self):
        return self.invoice.official_number

    @property
    def code_journal(self):
        return self.config.get('internalcode_journal_encaissement')

    @property
    def mode(self):
        return "interne"

    def _should_write_debit(self):
        return True

    def _get_bank_cg(self):
        result = self.config.get('internalbank_general_account', None)
        if not result:
            raise MissingData(
                "Le compte bank des encaissements interne n'est pas configuré"
            )
        return result


class SagePaymentTva(SagePaymentBase):
    """
    Optionnal Tva module
    """
    def get_amount(self):
        """
        Returns the reversed tva amount
        """
        tva_amount = self.payment.tva.value
        ht_value = compute_ht_from_ttc(
            self.payment.amount,
            tva_amount,
            division_mode=(self.invoice.mode != 'ttc')
        )
        tva_value = compute_tva(ht_value, tva_amount)
        return floor(tva_value)

    @double_lines
    def credit_tva(self, total):
        entry = self.get_base_entry()
        entry.update(
            compte_cg=self.payment.tva.compte_a_payer,
            credit=total,
        )
        if self.payment.tva.code:
            entry.update(
                code_taxe=self.payment.tva.code,
            )
        return entry

    @double_lines
    def debit_tva(self, total):
        entry = self.get_base_entry()
        entry.update(
            compte_cg=self.payment.tva.compte_cg,
            debit=total,
        )
        if self.payment.tva.code:
            entry.update(
                code_taxe=self.payment.tva.code,
            )
        return entry

    def yield_entries(self):
        """
        Yield all the entries for the current payment
        """
        total = self.get_amount()
        if total > 0:
            yield self.credit_tva(total)
            yield self.debit_tva(total)


class SagePaymentRemittance(SagePaymentBase):
    """
    Optionnal remittance module
    """
    _handled_remittances = []
    _remittances_amounts = {}

    def __init__(self, *args, **kwargs):
        SagePaymentBase.__init__(self, *args, **kwargs)
        self._handled_remittances = []
        self._remittances_amounts = {}

    def set_remittances_amounts(self, remittances_amounts):
        self._remittances_amounts = remittances_amounts

    @double_lines
    def debit_banque(self):
        remittance = self.payment.bank_remittance
        remittance_id = self.payment.bank_remittance_id
        entry = self.get_base_entry()
        entry.update(
            date=format_sage_date(remittance.remittance_date),
            reference=remittance_id,
            libelle="Remise {}".format(remittance_id),
            compte_cg=self.payment.bank.compte_cg,
            debit=self._remittances_amounts[remittance_id],
            num_analytique=remittance.get_analytic_code(),
        )
        return entry

    def yield_entries(self):
        remittance_id = self.payment.bank_remittance_id
        if remittance_id is not None:
            if remittance_id not in self._handled_remittances:
                self._handled_remittances.append(remittance_id)
                yield self.debit_banque()


@implementer(ITreasuryProducer)
class PaymentExport:
    """
    Export entries following the given path :

        Invoices -> Invoice -> Payments -> Payment
    """
    use_analytic = True
    _default_modules = (SagePaymentMain,)
    _available_modules = {
        "receipts_active_tva_module": SagePaymentTva,
        "receipts_group_by_remittance": SagePaymentRemittance,
    }

    def __init__(self, context, request):
        self.config = request.config
        self.modules = []
        for module in self._default_modules:
            self.modules.append(module(context, request))
        for config_key, module in list(self._available_modules.items()):
            if self.config.get(config_key) == '1':
                self.modules.append(module(context, request))

    def _get_item_book_entries(self, payment):
        """
        Return the receipts entries for the given payment
        """
        for module in self.modules:
            module.set_payment(payment)
            for entry in module.yield_entries():
                gen_line, analytic_line = entry
                yield filter_accounting_entry(gen_line)
                if self.use_analytic:
                    yield filter_accounting_entry(analytic_line)

    def _set_remittances_infos(self, payments):
        """
        Set remittances informations to remittance module (if activated)
        """
        for module in self.modules:
            if type(module) == SagePaymentRemittance:
                br_amounts = {}
                for payment in payments:
                    if payment.bank_remittance_id is not None:
                        remittance_id = payment.bank_remittance_id
                        if remittance_id not in list(br_amounts.keys()):
                            br_amounts[remittance_id] = 0
                        br_amounts[remittance_id] += payment.amount
                module.set_remittances_amounts(br_amounts)

    def get_item_book_entries(self, payment):
        self._set_remittances_infos([payment])
        return list(self._get_item_book_entries(payment))

    def get_book_entries(self, payments):
        result = []
        self._set_remittances_infos(payments)
        for payment in payments:
            result.extend(list(self._get_item_book_entries(payment)))
        return result


class InternalPaymentExport(PaymentExport):
    _default_modules = (InternalSagePaymentMain,)
    _available_modules = {}

    def _set_remittances_infos(self, payments):
        pass
