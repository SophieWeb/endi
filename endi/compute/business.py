class BusinessCompute:
    """
    Works by inheritance, isolates amounts computing of Business class.
    """
    def _total(self, objects, tva_on_margin_only=False):
        if tva_on_margin_only:
            return sum([i.total for i in objects if i.expense_type.tva_on_margin])
        else:
            return sum([i.total for i in objects])

    @property
    def total_expenses(self):
        return (
            self._total(self.base_expense_lines)
            +
            self._total(self.supplier_invoice_lines)
        )

    @property
    def total_expenses_tva_on_margin(self):
        return (
            self._total(self.base_expense_lines, True)
            +
            self._total(self.supplier_invoice_lines, True)
        )

    @property
    def total_expenses_no_tva_on_margin(self):
        return self.total_expenses - self.total_expenses_tva_on_margin
