"""
Third party handling forms schemas and related widgets
"""
import deform
import colander
from collections import OrderedDict
from endi import forms
from endi_base.consts import CIVILITE_OPTIONS
from endi.models.company import Company


def _build_third_party_select_value(third_party):
    """
        return the tuple for building third_party select
    """
    label = third_party.label
    if third_party.code:
        label += " ({0})".format(third_party.code)
    return (third_party.id, label)


def build_third_party_values(third_parties):
    """
        Build human understandable third_party labels
        allowing efficient discrimination

    :param obj third_parties: Iterable (list or Sqlalchemy query)
    :returns: A list of 2-uples
    """
    return [
        _build_third_party_select_value(third_party)
        for third_party in third_parties
    ]


def build_admin_third_party_options(query):
    """
    Format options for admin third_party select widget

    :param obj query: The Sqlalchemy query
    :returns: A list of deform.widget.OptGroup
    """
    query = query.order_by(Company.name)
    values = []
    datas = OrderedDict()

    for item in query:
        datas.setdefault(item.company.name, []).append(
            _build_third_party_select_value(item)
        )

    # All third_parties, grouped by Company
    for company_name, third_parties in list(datas.items()):
        values.append(
            deform.widget.OptGroup(
                company_name,
                *third_parties
            )
        )
    return values


def third_party_after_bind(node, kw):
    """
    After bind method for the third_party model schema

    removes nodes if the user have no rights to edit them

    :param obj node: SchemaNode corresponding to the ThirdParty
    :param dict kw: The bind parameters
    """
    request = kw['request']
    if not request.has_permission('admin_treasury', request.context):
        del node['compte_tiers']
        del node['compte_cg']


def customize_third_party_schema(schema):
    """
    Add common widgets configuration for the third parties forms schema

    :param obj schema: The ThirdParty form schema
    """
    if 'civilite' in schema:
        schema['civilite'].widget = forms.get_select(
            CIVILITE_OPTIONS,
        )
        schema['civilite'].validator = colander.OneOf(
            [a[0] for a in CIVILITE_OPTIONS]
        )
    if 'address' in schema:
        schema['address'].widget = deform.widget.TextAreaWidget(
            cols=25,
            row=1,
        )
    if 'email' in schema:
        schema['email'].validator = forms.mail_validator()
    if 'comments' in schema:
        schema['comments'].widget = deform.widget.TextAreaWidget(
            css_class="col-md-8",
            rows=5,
        )
    if 'compte_cg' in schema:
        schema['compte_cg'].description = (
            "Laisser vide pour utiliser les paramètres de l'enseigne"
        )
        schema['compte_tiers'].description = (
            "Laisser vide pour utiliser les paramètres de l'enseigne"
        )
    return schema
