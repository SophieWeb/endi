import functools

import colander
from colanderalchemy import SQLAlchemySchemaNode
import deform
import deform_extensions

from endi_base.models.base import DBSESSION
from endi.compute.math_utils import integer_to_amount

from endi import forms
from endi.forms.widgets import FixedLenSequenceWidget
from endi.forms.widgets import CleanMappingWidget
from endi.forms.company import company_choice_node
from endi.forms.custom_types import AmountType
from endi.forms.expense import expense_type_choice_node
from endi.forms.files import FileNode
from endi.forms.payments import (
    deferred_payment_mode_widget,
    deferred_payment_mode_validator,
    deferred_bank_account_widget,
    deferred_bank_account_validator,
)
from endi.forms.third_party.supplier import (
    get_deferred_supplier_select_validator,
    globalizable_supplier_choice_node_factory,
    supplier_choice_node_factory,
)
from endi.forms.supply import (
    get_add_edit_line_schema,
    get_list_schema,
)
from endi.forms.supply.supplier_order import (
    get_deferred_supplier_order_select_validator,
    supplier_order_choice_node,
)
from endi.models.supply import (
    get_supplier_invoices_years,
    BaseSupplierInvoicePayment,
    SupplierOrder,
    SupplierInvoice,
    SupplierInvoiceLine,
    SupplierInvoiceSupplierPayment,
)
from endi.models.supply.payment import SupplierInvoiceUserPayment

COMBINED_PAID_STATUS_OPTIONS = (
    ("", "Tous"),
    ("paid", "Les factures soldées"),
    ("supplier_topay", "Fournisseur à payer"),
    ("worker_topay", "Entrepreneur à rembourser"),
)


def get_supplier_invoice_payment_schema(request):
    """
    Returns the schema for payment registration
    """
    payment = request.context
    if isinstance(payment, SupplierInvoiceSupplierPayment):
        schema = SupplierPaymentSchema().clone()
    elif isinstance(payment, SupplierInvoiceUserPayment):
        schema = UserPaymentSchema().clone()

    return schema


def _customize_edit_schema(schema):
    customize = functools.partial(forms.customize_field, schema)

    if 'supplier_orders' in schema:
        # On s'assure qu'on sélectionne une liste de type parmis des existants
        # (et qu'on en rajoute pas à la volée)
        customize(
            'supplier_orders',
            children=forms.get_sequence_child_item(
                SupplierOrder,
                child_attrs=('id', 'name'),
            ),
            validator=forms.DeferredAll(
                same_percentage_supplier_order_validator,
                same_supplier_supplier_order_validator,
                get_deferred_supplier_order_select_validator(
                    multiple=True,
                    required=False,
                ),
            ),
        )
    if 'supplier_id' in schema:
        customize(
            'supplier_id',
            validator=get_deferred_supplier_select_validator(),
            missing=colander.required,
        )

    if 'lines' in schema:
        child_schema = schema['lines'].children[0]
        forms.customize_field(
            child_schema,
            'type_id',
            missing=colander.required
        )
    return schema


def supplier_invoice_edit_validator(self, appstruct):
    supplier_invoice = self.bindings['request'].context
    assert isinstance(supplier_invoice, SupplierInvoice)
    valid = supplier_consistency_validator(appstruct, supplier_invoice)

    if valid is not True:
        raise colander.Invalid(self, valid)
    else:
        return True


def get_supplier_invoice_edit_schema(internal=False):
    """
    Build the supplier invoice edit schema (via rest api)

    If the supplier invoice is internal, only a few fields can be edited

    :param bool internal: Is the edited document internal ?
    """

    if internal:
        schema = SQLAlchemySchemaNode(
            SupplierInvoice,
            excludes=(
                'supplier_orders', 'supplier_id', 'payer_id',
                'date', 'cae_percentage'
            )
        )
    else:
        schema = SQLAlchemySchemaNode(
            SupplierInvoice, validator=supplier_invoice_edit_validator
        )
    schema = _customize_edit_schema(schema)
    return schema


def get_supplier_invoice_list_schema(is_global=False):
    schema = get_list_schema(
        years_func=get_supplier_invoices_years,
        is_global=is_global,
    )
    schema.insert(2, forms.status_filter_node(
        COMBINED_PAID_STATUS_OPTIONS,
        name='combined_paid_status',
        title='Statut de paiement',
    ))
    schema.insert(
        -1,
        colander.SchemaNode(
            colander.String(),
            title='N° de pièce',
            name='official_number',
            missing='',
            widget=deform.widget.TextInputWidget(
                css_class='input-medium search-query'
            ),
            default='',
        )
    )
    return schema


def _invoice_from_request(request) -> SupplierInvoice:
    if type(request.context) is SupplierInvoice:
        supplier_invoice = request.context
    else:
        supplier_invoice = request.context.supplier_invoice
    return supplier_invoice


def _invoice_cae_amount_from_request(request):
    max_amount = _invoice_from_request(request).cae_topay()
    # for edit schema:
    if isinstance(request.context, BaseSupplierInvoicePayment):
        max_amount += request.context.get_amount()
    return max_amount


def _invoice_worker_amount_from_request(request):
    max_amount = _invoice_from_request(request).worker_topay()
    # for edit schema:
    if isinstance(request.context, BaseSupplierInvoicePayment):
        max_amount += request.context.get_amount()
    return max_amount


@colander.deferred
def deferred_payment_cae_amount_validator(node, kw):
    """
    Validate the amount to keep the sum under the of what CAE sohuld pay.
    """
    topay = _invoice_cae_amount_from_request(kw['request'])
    invoice = _invoice_from_request(kw['request'])
    if invoice.total > 0:
        max_value = topay
        min_value = 0
        max_msg = "Le montant payé par la CAE ne doit pas dépasser {}".format(
            integer_to_amount(topay)
        )
        min_msg = "Le montant doit être positif"
    else:
        max_value = 0
        min_value = topay
        max_msg = "Le montant doit être négatif"
        min_msg = "Le montant payé par la CAE ne doit pas dépasser {}".format(
            integer_to_amount(topay)
        )

    return colander.Range(
        min=min_value,
        max=max_value,
        min_err=min_msg,
        max_err=max_msg,
    )


@colander.deferred
def deferred_payment_worker_amount_validator(node, kw):
    """
    Validate the amount to keep the sum under the of what CAE sohuld pay.
    """
    topay = _invoice_worker_amount_from_request(kw['request'])
    invoice = _invoice_from_request(kw['request'])
    if invoice.total > 0:
        max_value = topay
        min_value = 0
        max_msg = (
            "Le montant payé par l'entrepreneur-euse ne doit pas dépasser "
            "{}".format(
                integer_to_amount(topay)
            )
        )
        min_msg = "Le montant doit être positif"
    else:
        max_value = 0
        min_value = topay
        max_msg = "Le montant doit être négatif"
        min_msg = (
            "Le montant payé par l'entrepreneur-euse ne doit pas dépasser "
            "{}".format(integer_to_amount(topay))
        )

    return colander.Range(
        min=min_value,
        max=max_value,
        min_err=min_msg,
        max_err=max_msg,
    )


@colander.deferred
def deferred_payment_cae_amount_default(node, kw):
    return _invoice_cae_amount_from_request(kw['request'])


@colander.deferred
def deferred_payment_worker_amount_default(node, kw):
    return _invoice_worker_amount_from_request(kw['request'])


@colander.Function
def same_percentage_supplier_order_validator(value):
    """
    Validate that all targeted SupplierOrder do use the same per-centage
    """
    suppliers_orders_ids = value
    query = DBSESSION().query(SupplierOrder.cae_percentage)
    query = query.filter(
        SupplierOrder.id.in_(suppliers_orders_ids),
    ).distinct()
    if query.count() > 1:
        return (
            "Toutes les commandes sélectionnées doivent avoir le même "
            + "pourcentage de paiement CAE."
        )
    else:
        return True


@colander.Function
def same_supplier_supplier_order_validator(value):
    """
    Validate that all targeted SupplierOrder are from same supplier
    """
    suppliers_orders_ids = value
    query = DBSESSION().query(SupplierOrder.supplier_id)
    query = query.filter(
        SupplierOrder.id.in_(suppliers_orders_ids),
    ).distinct()
    if query.count() > 1:
        return (
            "Toutes les commandes sélectionnées doivent avoir le même "
            + "fournisseur."
        )
    else:
        return True


def supplier_consistency_validator(
    value: dict,
    supplier_invoice: SupplierInvoice
):
    """
    Not exactly a standard validator

    :param value: supplier_invoice fields.
    """
    supplier_orders = value.get(
        'supplier_orders',
        [i.id for i in supplier_invoice.supplier_orders],
    )
    supplier_id = value.get('supplier_id', supplier_invoice.supplier_id)
    if len(supplier_orders) > 0 and supplier_id is not None:
        query = DBSESSION().query(SupplierOrder.supplier_id).filter_by(
            id=supplier_orders[0],
            supplier_id=supplier_id,
        )
        if not query.first():
            return (
                "Impossible de changer le fournisseur "
                "d'une facture déjà associée à une ou des commandes."
            )
    return True


def get_invoicable_supplier_orders(request):
    company = request.context
    return SupplierOrder.query_for_select(
        valid_only=True,
        invoiced=False,
        company_id=company.id
    )


class SupplierInvoiceAddByOrdersSchema(colander.MappingSchema):
    suppliers_orders_ids = supplier_order_choice_node(
        title='Commande(s) fournisseur',
        multiple=True,
        missing=colander.drop,
        description=(
            "Vous pouvez associer votre facture à une ou "
            + "plusieurs commandes préalablement validées. "
            + "Les lignes des commandes seront importées dans la facture."
        ),
        query_func=get_invoicable_supplier_orders,
        extra_validator=colander.All(
            same_percentage_supplier_order_validator,
            same_supplier_supplier_order_validator,
        ),
    )


def get_supplier_invoice_add_by_supplier_schema():
    schema = SQLAlchemySchemaNode(
        SupplierInvoice,
        includes=['supplier_id'],
    )
    schema['supplier_id'] = supplier_choice_node_factory(
        description="Si le fournisseur manque, l'ajouter d'abord "
        "dans Tiers → Fournisseurs."
    )
    return schema


class BaseSupplierInvoicePaymentSchema(colander.MappingSchema):
    amount = colander.SchemaNode(
        AmountType(),
    )
    come_from = forms.come_from_node()
    date = forms.today_node()

    mode = colander.SchemaNode(
        colander.String(),
        title="Mode de paiement",
        widget=deferred_payment_mode_widget,
        validator=deferred_payment_mode_validator,
    )
    bank_id = colander.SchemaNode(
        colander.Integer(),
        title="Banque",
        widget=deferred_bank_account_widget,
        validator=deferred_bank_account_validator,
    )
    resulted = colander.SchemaNode(
        colander.Boolean(),
        title="Soldé",
        label="",
        description=(
            "Indique que le document est soldé (ne recevra plus de paiement), "
            + "si le montant indiqué correspond au montant de la facture "
            + "fournisseur, celui-ci est soldée automatiquement."
        ),
        missing=False,
        default=False,
    )
    bank_remittance_id = colander.SchemaNode(
        colander.String(),
        title="Référence du paiement",
        description=(
            "Ce champ est un indicateur permettant de retrouver l'opération "
            + "bancaire à laquelle ce décaissement est associé, par exemple "
            + "pour la communiquer à un fournisseur"
        ),
        missing='',
    )


class SupplierPaymentSchema(BaseSupplierInvoicePaymentSchema):
    amount = colander.SchemaNode(
        AmountType(),
        title="Montant du paiement",
        validator=deferred_payment_cae_amount_validator,
        default=deferred_payment_cae_amount_default,
    )


class UserPaymentSchema(BaseSupplierInvoicePaymentSchema):
    amount = colander.SchemaNode(
        AmountType(),
        title="Montant du remboursement",
        validator=deferred_payment_worker_amount_validator,
        default=deferred_payment_worker_amount_default,
    )
    waiver = colander.SchemaNode(
        colander.Boolean(),
        title="Abandon de créance",
        label="",
        description="""Indique que ce paiement correspond à un abandon de créance à la hauteur du
montant indiqué, le mode de paiement, la référence de paiement et la banque
sont alors ignorés""",
        missing=False,
        default=False,
    )


INVOICE_LINE_GRID = (
    (
        ('company_id', 3),
        ('type_id', 4),
        ('description', 3),
        ('ht', 1),
        ('tva', 1),
    ),
)


def get_supplier_invoice_line_dispatch_schema():
    schema = get_add_edit_line_schema(
        SupplierInvoiceLine,
        widget=deform_extensions.GridMappingWidget(
            named_grid=INVOICE_LINE_GRID
        ),
        title='ligne',
        excludes=['type_id'],
    )
    forms.customize_field(
        schema,
        'tva',
        title='TVA',
        validator=colander.Range(min=0),
    )
    forms.customize_field(
        schema,
        'ht',
        title='HT',
        validator=colander.Range(min=0),
    )
    schema.add(
        company_choice_node(
            name="company_id",
            title="Enseigne",
        )
    )
    schema.add(
        expense_type_choice_node(
            name="type_id",
            title="Type",
        )
    )

    return schema


class InvoiceLineSequenceSchema(colander.SequenceSchema):
    lines = get_supplier_invoice_line_dispatch_schema()


def _get_linkable_lines(node, kw):
    business = kw['request'].context
    assert business.__name__ == 'business'
    return SupplierInvoiceLine.linkable(business)


def _get_deferred_supplier_invoice_line_choices(widget_options):
    default_option = widget_options.pop('default_option', None)

    @colander.deferred
    def deferred_supplier_invoice_line_choices(node, kw):
        query = _get_linkable_lines(node, kw)
        # most recent first
        query = query.order_by(
            SupplierInvoice.date.desc(),
            SupplierInvoice.id.desc(),
        )
        values = [(v.id, v.long_label()) for v in query]
        if default_option:
            values.insert(0, default_option)
        return deform.widget.Select2Widget(
            values=values,
            **widget_options
        )
    return deferred_supplier_invoice_line_choices


def supplier_invoice_line_node(multiple=False, **kw):
    widget_options = kw.pop('widget_options', {})
    widget_options.setdefault('default_option', ('', ''))
    return colander.SchemaNode(
        colander.Set() if multiple else colander.Integer(),
        widget=_get_deferred_supplier_invoice_line_choices(widget_options),
        validator=forms.deferred_id_validator(
            _get_linkable_lines,
        ),
        **kw
    )


supplier_invoice_line_choice_node = forms.mk_choice_node_factory(
    supplier_invoice_line_node,
    resource_name='une ligne de facture fournisseur',
)


class SupplierInvoiceLineSeq(colander.SequenceSchema):
    line = supplier_invoice_line_choice_node()


class SupplierInvoiceDispatchSchema(colander.MappingSchema):
    date = forms.today_node()
    invoice_file = FileNode(title="Document")
    supplier_id = globalizable_supplier_choice_node_factory(
        description=(
            "Seuls les fournisseurs présents dans au moins une enseigne "
            + "et avec un n° d'immatriculation renseigné sont proposés. Si "
            + "un fournisseur est manquant, il faudra commencer par le "
            + "saisir dans les fournisseurs d'une enseigne."
        )
    )
    total_ht = colander.SchemaNode(
        AmountType(),
        title="Total HT",
        validator=colander.Range(min=0),
    )

    total_tva = colander.SchemaNode(
        AmountType(),
        title="Total TVA",
        validator=colander.Range(min=0),
    )

    lines = InvoiceLineSequenceSchema(
        title='Lignes de facture',
        widget=deform.widget.SequenceWidget(min_len=1),
    )

    name = colander.SchemaNode(
        colander.String(),
        missing=colander.drop,
        title='Titre de la facture',
        description=(
            'Laisser vide pour « Commande {FOURNISSEUR} du {DATE FACTURE} »'
        ),
    )

    def _validate_sum(self, line_fieldname, total_fieldname, values):
        """
        :rtype list:
        :return: the SchemaNodes with an error
        """
        total = values.get(total_fieldname)
        lines_sum = sum(
            line[line_fieldname]
            for line in values['lines']
        )
        return total == lines_sum

    def validator(self, form, values):
        # the error is not very detailed as the user should already have been
        # noticed front-end side (JS).
        valid = (
            self._validate_sum('ht', 'total_ht', values)
            and
            self._validate_sum('tva', 'total_tva', values)
        )
        if not valid:
            raise colander.Invalid(form, msg='Totaux incohérents')


class ProductSupplierInvoiceLine(colander.MappingSchema):
    """
        A single estimation line
    """
    id = colander.SchemaNode(
        colander.Integer(),
        widget=deform.widget.HiddenWidget(),
        missing="",
        css_class="span0"
    )
    description = colander.SchemaNode(
        colander.String(),
        title="Description",
        widget=deform.widget.TextInputWidget(readonly=True),
        missing='',
        css_class='col-md-3',
    )
    type_id = expense_type_choice_node(
        name="type_id",
        title="Type de dépense",
    )


class ProductSupplierInvoiceLines(colander.SequenceSchema):
    taskline = ProductSupplierInvoiceLine(
        missing="",
        title="",
        widget=CleanMappingWidget(),
    )


class SetTypesSchema(colander.MappingSchema):
    """
        Form schema used to configure Products
    """
    lines = ProductSupplierInvoiceLines(
        widget=FixedLenSequenceWidget(),
        missing="",
        title=''
    )
