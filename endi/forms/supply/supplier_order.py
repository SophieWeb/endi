import colander
import deform
from colanderalchemy import SQLAlchemySchemaNode

from endi import forms
from endi.forms.third_party.supplier import supplier_choice_node_factory
from endi.forms.supply import get_list_schema
from endi.models.supply import (
    get_supplier_orders_years,
    SupplierOrder,
)


INVOICE_STATUS_OPTIONS = [
    ('all', ''),
    ('absent', 'pas de facture'),
    ('present', 'facture (tous statuts)'),
    ('draft', 'facture en brouillon'),
    ('valid', 'facture validée'),
    ('resulted', 'facture soldée'),
]


def get_company_from_request(request):
    from endi.models.company import Company
    from endi.models.supply import SupplierInvoice

    if isinstance(request.context, Company):
        company = request.context
    elif isinstance(request.context, SupplierInvoice):
        company = request.context.company
    else:
        raise ValueError('No company bound to this request')
    return company


def get_supplier_order_edit_schema(internal=False):
    """
    Build a supplier order edition schema

    :param bool internal: Is the edited document internal
    """
    excludes = None
    if internal:
        excludes = ('supplier_id', 'cae_percentage')
    schema = SQLAlchemySchemaNode(SupplierOrder, excludes=excludes)
    return schema


def get_supplier_orders_list_schema(is_global=False):
    schema = get_list_schema(
        years_func=get_supplier_orders_years,
        is_global=is_global,
    )
    schema.insert(2, colander.SchemaNode(
        colander.String(),
        name='invoice_status',
        title="Facture fournisseur",
        widget=deform.widget.SelectWidget(values=INVOICE_STATUS_OPTIONS),
        validator=colander.OneOf([s[0] for s in INVOICE_STATUS_OPTIONS]),
        missing='all',
        default='all',
    ))
    return schema


class SupplierOrderAddSchema(colander.MappingSchema):
    supplier_id = supplier_choice_node_factory(
        description=(
            "Vous pouvez gérer la liste des fournisseurs via "
            + "Tiers : Fournisseurs."
        )
    )


def get_company_supplier_orders_from_request(request):
    company = get_company_from_request(request)
    query = SupplierOrder.query().filter_by(company_id=company.id)
    exclude = "internalsupplier_order"
    query = query.filter(SupplierOrder.type_ != exclude)
    return query


def get_deferred_supplier_order_choices(
    widget_options,
    query_func=get_company_supplier_orders_from_request,
):
    """
    Builds a Select2Widget for selecting SupplierOrder

    :param query_func: a function returning a SupplierOrder query
    :returns: a deferred Select2Widget
    """
    @colander.deferred
    def _get_deferred_supplier_order_choice(node, kw):
        request = kw['request']
        values = [(i.id, i.name) for i in query_func(request)]

        return deform.widget.Select2Widget(
            values=values,
            **widget_options
        )
    return _get_deferred_supplier_order_choice


def get_deferred_supplier_order_select_validator(
    query_func=get_company_supplier_orders_from_request,
    multiple=False,
    required=True,
):
    """
    :returns: A colander deferred validator
    """
    @colander.deferred
    def _deferred_supplier_order_select_validator(node, kw):
        def orders_allin(value):
            request = kw['request']
            if multiple:
                selected_ids = value
            else:
                selected_ids = [value]
            allowed_ids = [i.id for i in query_func(request)]

            if required and (len(selected_ids) == 0):
                return "Veuillez choisir au moins une commande fournisseur"
            for selected_id in selected_ids:
                if selected_id in ("0", 0):
                    return "Veuillez choisir une commande fournisseur"
                elif int(selected_id) not in allowed_ids:
                    return "Choix de commande invalide"
            return True

        return colander.Function(orders_allin)
    return _deferred_supplier_order_select_validator


def supplier_order_node(
        multiple=False,
        query_func=get_company_supplier_orders_from_request,
        extra_validator=None,
        required=True,
        **kw
):
    """
    Builds a node for selecting one or several SupplierOrder

    Takes care of listing options and validating them using the same query.
    """
    widget_options = kw.pop('widget_options', {})
    validator = get_deferred_supplier_order_select_validator(
        query_func=query_func,
        multiple=multiple,
        required=required,
    )
    if extra_validator:
        validators = [validator, extra_validator]
    else:
        validators = [validator]

    return colander.SchemaNode(
        colander.Set() if multiple else colander.Integer(),
        widget=get_deferred_supplier_order_choices(
            widget_options,
            query_func=query_func,
        ),
        validator=forms.DeferredAll(*validators),
        **kw
    )


supplier_order_choice_node = forms.mk_choice_node_factory(
    supplier_order_node,
    resource_name='une commande fournisseur',
    resource_name_plural='de zéro a plusieurs commandes fournisseur',
    required=False,
)
