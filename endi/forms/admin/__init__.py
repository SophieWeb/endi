"""
    Form schemes for administration
"""
import colander
import logging
from typing import Type
import simplejson as json
import deform
import deform_extensions

from colanderalchemy import SQLAlchemySchemaNode

from endi.models.config import Config
from endi.models.competence import (
    CompetenceScale,
)
from endi.models.options import ConfigurableOption
from endi.models.accounting.treasury_measures import TreasuryMeasureType
from endi.models.task.services.invoice_official_number import (
    InvoiceNumberService,
)
from endi.models.expense.services import (
    ExpenseSheetNumberService,
)
from endi.models.supply.services.supplierinvoice_official_number import (
    SupplierInvoiceNumberService,
    InternalSupplierInvoiceNumberService,
)
from endi import forms
from endi.forms.files import ImageNode
from endi.forms.widgets import CleanMappingWidget, CleanSequenceWidget
from endi.utils.image import ImageResizer
from endi.utils.strings import safe_ascii_str


log = logging.getLogger(__name__)


HEADER_RESIZER = ImageResizer(4, 1)


def get_number_template_validator(number_service):
    def validator(node, value):
        try:
            number_service.validate_template(value)
        except ValueError as e:
            raise colander.Invalid(node, str(e))

    return validator


@colander.deferred
def help_text_libelle_comptable(*args):
    """
    Hack to allow dynamic content in a description field description.
    """
    base = "Les variables disponibles \
pour la génération des écritures sont décrites en haut de page."
    maxlength = Config.get_value('accounting_label_maxlength', None)
    if maxlength:
        return "{} NB : les libellés sont tronqués à ".format(base) + \
            "{} caractères au moment de l'export.".format(maxlength) + \
            "Il est possible de changer cette taille dans  " +\
            "Configuration → Logiciel de comptabilité."
    else:
        return base


CONFIGURATION_KEYS = {
    'coop_cgv': {
        "title": "Conditions générales de vente",
        "description": "Les conditions générales sont placées en dernière \
page des documents (devis/factures/avoirs)",
        "widget": forms.richtext_widget(admin=True)
    },
    'coop_pdffootertitle': {
        'title': "Titre du pied de page",
        "widget": deform.widget.TextAreaWidget(rows=4),
    },
    'coop_pdffootertext': {
        'title': "Contenu du pied de page",
        "widget": deform.widget.TextAreaWidget(rows=4),
    },
    'coop_pdffootercourse': {
        'title': "Pied de page spécifique aux formations",
        "description": "Ce contenu ne s'affiche que sur les documents liés à \
des formations",
        "widget": deform.widget.TextAreaWidget(rows=4),
    },
    "coop_estimationheader": {
        "title": "Cadre d’information spécifique (en en-tête des devis)",
        "description": "Permet d’afficher un texte avant la description des \
prestations ex : <span color='red'>Le RIB a changé</span>",
        "widget": deform.widget.TextAreaWidget(rows=4),
    },
    "coop_invoiceheader": {
        "title": "Cadre d’information spécifique (en en-tête des factures)",
        "description": "Permet d’afficher un texte avant la description des \
prestations ex : <span color='red'>Le RIB a changé</span>",
        "widget": deform.widget.TextAreaWidget(rows=4),
    },
    'cae_admin_mail': {
        "title": "Adresse e-mail de contact pour les notifications enDI",
        "description": (
            "Les e-mails de notifications (par ex : retour "
            "sur le traitement de fichiers de trésorerie "
            ") sont envoyés à cette adresse"
        ),
        "validator": forms.mail_validator(),
    },
    'receipts_active_tva_module': {
        "title": "",
        "label": "Activer le module TVA pour les encaissements",
        "description": "Inclue les écritures pour le paiement de la TVA \
sur encaissement",
        "widget": deform.widget.CheckboxWidget(true_val='1', false_val='0')
    },
    'receipts_group_by_remittance': {
        "title": "",
        "label": "Grouper les écritures d'export par remise en banque",
        "description": "Permet d'avoir une seule écriture de banque par \
remise en banque au lieu d'une par encaissement",
        "widget": deform.widget.CheckboxWidget(true_val='1', false_val='0')
    },
    # Configuration comptables ventes
    'code_journal': {
        'title': "Code journal ventes",
        'description': "Le code du journal dans votre logiciel de \
comptabilité",
    },
    'numero_analytique': {
        'title': "Numéro analytique de la CAE",
    },
    'compte_cg_contribution': {
        'title': "Compte CG contribution",
        'description': "Compte CG correspondant à la contribution des \
entrepreneurs à la CAE",
        "section": "Module Contribution",
    },
    "compte_frais_annexes": {
        "title": "Compte de frais annexes",
    },
    "compte_cg_banque": {
        "title": "Compte banque de l'entrepreneur",
    },
    'compte_rrr': {
        "title": "Compte RRR",
        "description": "Compte Rabais, Remises et Ristournes",
        "section": "Configuration des comptes RRR"
    },
    'compte_cg_tva_rrr': {
        "title": "Compte CG de TVA spécifique aux RRR",
        "description": "",
        "section": "Configuration des comptes RRR"
    },
    'code_tva_rrr': {
        "title": "Code de TVA spécifique aux RRR",
        "description": "",
        "section": "Configuration des comptes RRR"
    },
    'bookentry_facturation_label_template': {
        'title': "Gabarit pour les libellés d'écriture",
        'description': help_text_libelle_comptable,
        "section": "Module Facturation",
    },
    'compte_rg_interne': {
        "title": "Compte CG RG Interne",
        "description": "",
        "section": "Module RG Interne",
    },
    'compte_rg_externe': {
        "title": "Compte CG RG Client",
        "description": "",
        "section": "Module RG Client",
    },
    'contribution_cae': {
        "title": "Pourcentage de la contribution",
        "description": "Valeur par défaut de la contribution (nombre entre \
0 et 100). Elle peut être individualisée sur les pages enseigne.",
        "section": "Module Contribution",
    },
    'bookentry_contribution_label_template': {
        'title': "Gabarit pour les libellés d'écriture",
        'description': help_text_libelle_comptable,
        "section": "Module Contribution",
    },
    'taux_rg_interne': {
        "title": "Taux RG Interne",
        "description": "(nombre entre 0 et 100) Requis pour les écritures \
RG Interne",
        "section": "Module RG Interne",
    },
    'taux_rg_client': {
        "title": "Taux RG Client",
        "description": "(nombre entre 0 et 100) Requis pour le module \
d'écriture RG Client",
        "section": "Module RG Client",
    },
    'cae_general_customer_account': {
        'title': "Compte client général de la CAE",
        'description': "Pour tous les clients de toutes les enseignes. une "
        "valeur spécifique pour une enseigne peut être paramétrée au niveau "
        "de l'enseigne. une valeur spécifique à un client peut être paramétrée"
        " dans chaque fiche client",
        'section': "Configuration des comptes clients",
    },
    'cae_third_party_customer_account': {
        'title': "Compte client tiers de la CAE",
        'description': "Pour tous les clients de toutes les enseignes. "
        "une valeur spécifique pour une enseigne peut être paramétrée au "
        "niveau de l'enseigne. une valeur spécifique à un client peut être "
        "paramétrée dans chaque fiche client",
        'section': "Configuration des comptes clients",
    },

    # Facturation interne
    'internalcode_journal': {
        'title': "Code journal ventes",
        'description': "Le code du journal dans votre logiciel de \
comptabilité",
    },
    'internalcode_journal_encaissement': {
        'title': "Code journal pour les encaissements des factures internes",
        'description': "Le code du journal dans votre logiciel de \
comptabilité",
        'section': 'Facturation interne',
    },
    'internalbank_general_account': {
        'title': (
            "Compte général de banque à utiliser pour les encaissements "
            "des factures internes (ventes et fournisseurs)"
        ),
        'description': "Le code du journal dans votre logiciel de \
comptabilité",
        'section': 'Facturation interne',
    },

    'internalnumero_analytique': {
        'title': "Numéro analytique de la CAE",
    },
    "internalcompte_frais_annexes": {
        "title": "Compte de frais annexes",
    },
    "internalcompte_cg_banque": {
        "title": "Compte banque de l'entrepreneur",
    },
    'internalbookentry_facturation_label_template': {
        'title': "Gabarit pour les libellés d'écriture",
        'description': help_text_libelle_comptable,
        "section": "Module Facturation",
    },
    'internalcompte_cg_contribution': {
        'title': "Compte CG contribution",
        'description': "Compte CG correspondant à la contribution des \
entrepreneurs à la CAE",
        "section": "Module Contribution",
    },
    'internalbookentry_contribution_label_template': {
        'title': "Gabarit pour les libellés d'écriture",
        'description': help_text_libelle_comptable,
        "section": "Module Contribution",
    },
    'internalcompte_rrr': {
        "title": "Compte RRR",
        "description": "Compte Rabais, Remises et Ristournes",
        "section": "Configuration des comptes RRR"
    },
    'internalcontribution_cae': {
        "title": "Pourcentage de la contribution",
        "description": "Valeur par défaut de la contribution (nombre entre \
0 et 100). Elle peut être individualisée sur les pages enseigne.",
        "section": "Module Contribution",
    },
    'internalcae_general_customer_account': {
        'title': "Compte client général de la CAE pour les clients internes",
        'description': "Pour tous les clients internes de toutes les "
        "enseignes. une "
        "valeur spécifique pour une enseigne peut être paramétrée au niveau "
        "de l'enseigne. une valeur spécifique à un client peut être paramétrée"
        " dans chaque fiche client",
        'section': "Configuration des comptes clients internes",
    },
    'internalcae_third_party_customer_account': {
        'title': "Compte client tiers de la CAE pour les clients internes",
        'description': "Pour tous les clients internes de toutes les "
        "enseignes. "
        "une valeur spécifique pour une enseigne peut être paramétrée au "
        "niveau de l'enseigne. une valeur spécifique à un client peut être "
        "paramétrée dans chaque fiche client",
        'section': "Configuration des comptes clients internes",
    },
    "sale_use_ttc_mode": {
        "title": "",
        "label": "Proposer le mode TTC aux entrepreneurs ?",
        "description": "Permet d'activer/désactiver le mode TTC proposé "
        "aux entrepreneurs.",
        "section": "Mode de saisie",
        "widget": deform.widget.CheckboxWidget(true_val='1', false_val='0')
    },
    'estimation_validity_duration_default': {
        'title': "Limite de validité des devis par défaut",
        'description': "Limite de validité qui sera reprise automatiquement \
        dans les mentions des devis si l'entrepreneur ne la modifie pas \
        manuellement",
        'section': 'Devis',
    },
    'bookentry_payment_label_template': {
        'title': "Gabarit pour les libellés d'écriture",
        'description': help_text_libelle_comptable,
        "section": "Encaissements",
    },
    'internalbookentry_payment_label_template': {
        'title': "Gabarit pour les libellés d'écriture",
        'description': help_text_libelle_comptable,
        "section": "Facturation interne",
    },
    'bookentry_rg_client_label_template': {
        'title': "Gabarit pour les libellés d'écriture",
        'description': help_text_libelle_comptable,
        "section": "Module RG Client",
    },
    'bookentry_rg_interne_label_template': {
        'title': "Gabarit pour les libellés d'écriture",
        'description': help_text_libelle_comptable,
        "section": "Module RG Interne",
    },
    'bookentry_expense_label_template': {
        'title': "Gabarit pour les libellés d'écriture",
        'description': help_text_libelle_comptable,
    },
    'bookentry_expense_payment_main_label_template': {
        'title': "Gabarit pour les libellés d'écriture",
        'description': help_text_libelle_comptable,
        "section": "Paiement des notes de dépenses",
    },
    'bookentry_expense_payment_waiver_label_template': {
        'title': "Gabarit pour les libellés d'écriture",
        'description': help_text_libelle_comptable,
        "section": "Abandon de créance",
    },
    'sage_contribution': {
        "title": "Module contribution",
        "widget": deform.widget.CheckboxWidget(true_val='1', false_val='0'),
        "section": "Activation des modules d'export Sage",
    },
    'sage_rginterne': {
        "title": "Module RG Interne",
        "widget": deform.widget.CheckboxWidget(true_val='1', false_val='0'),
        "section": "Activation des modules d'export Sage",
    },
    'sage_rgclient': {
        "title": "Module RG Client",
        "widget": deform.widget.CheckboxWidget(true_val='1', false_val='0'),
        "section": "Activation des modules d'export Sage",
    },
    'sage_facturation_not_used': {
        "title": "Module facturation",
        "description": "Activé par défaut",
        "widget": deform.widget.CheckboxWidget(
            template='checkbox_readonly.pt'
        ),
        "section": "Activation des modules d'export Sage",
    },
    #
    'internalsage_contribution': {
        "title": "Module contribution pour la facturation interne",
        "widget": deform.widget.CheckboxWidget(true_val='1', false_val='0'),
        "section": "Activation des modules d'export Sage",
    },
    # NDF
    "code_journal_ndf": {
        "title": "Code journal utilisé pour les notes de dépenses",
    },
    "compte_cg_ndf": {
        "title": "Compte général (classe 4) pour les dépenses dues aux \
entrepreneurs",
        "description": "Notes de dépense et part entrepreneur des factures fournisseur",
    },
    "ungroup_expenses_ndf": {
        "title": "Ne pas grouper les écritures dans les exports des "
        "notes de dépenses",
        "description": "Si cette case est cochée, "
        "lors de l'export comptable des notes de dépenses, pour "
        "chaque entrepreneur, une écriture par dépense sera affichée "
        "(et non pas un total par types de dépense).",
        "widget": deform.widget.CheckboxWidget(true_val='1', false_val='0'),
    },
    "code_journal_waiver_ndf": {
        "title": "Code journal spécifique aux abandons de créance",
        "description": "Code journal utilisé pour l'export des abandons \
de créance, si ce champ n'est pas rempli, le code journal d'export des notes \
de dépense est utilisé. Les autres exports de décaissement utilisent \
    le code journal de la banque concernée.",
        "section": "Abandon de créance",

    },
    "compte_cg_waiver_ndf": {
        "title": "Compte abandons de créance",
        "description": "Compte de comptabilité générale spécifique aux \
abandons de créance dans les notes de dépenses",
        "section": "Abandon de créance",
    },
    "code_tva_ndf": {
        "title": "Code TVA utilisé pour les décaissements",
        "description": "Le code TVA utilisé pour l'export des décaissements",
        "section": "Paiement des notes de dépenses",
    },
    "expensesheet_number_template": {
        "title": "Gabarit du numéro de note de dépense",
        "description": "Peut contenir des caractères (préfixes, "
        "séparateurs… etc), ainsi que des variables et séquences. Ex: "
        "{YYYY}-{SEQYEAR}.",
        "missing": colander.required,
        "validator": get_number_template_validator(ExpenseSheetNumberService),
    },
    "global_expensesheet_sequence_init_value": {
        "title": "Valeur à laquelle on initialise la séquence globale",
        "section": "Séquence globale (SEQGLOBAL)",
        "type": colander.Int(),
        "validator": colander.Range(min=0),
    },
    "year_expensesheet_sequence_init_value": {
        "title": "Valeur à laquelle on initialise la séquence annuelle",
        "section": "Séquence annuelle (SEQYEAR)",
        "type": colander.Int(),
        "validator": colander.Range(min=0),
    },
    "year_expensesheet_sequence_init_date": {
        "title": "Date à laquelle on initialise la séquence annuelle",
        "section": "Séquence annuelle (SEQYEAR)",
        "widget": deform_extensions.CustomDateInputWidget(),
    },
    "month_expensesheet_sequence_init_value": {
        "title": "Valeur à laquelle on initialise la séquence mensuelle",
        "section": "Séquence mensuelle (SEQMONTH)",
        "type": colander.Int(),
        "validator": colander.Range(min=0),
    },
    "month_expensesheet_sequence_init_date": {
        "title": "Date à laquelle on initialise la séquence mensuelle",
        "section": "Séquence mensuelle (SEQMONTH)",
        "widget": deform_extensions.CustomDateInputWidget(),
    },
    "supplierinvoice_number_template": {
        "title": "Gabarit du numéro de facture fournisseur",
        "description": "Peut contenir des caractères (préfixes, "
        "séparateurs… etc), ainsi que des variables et séquences. Ex: "
        "{YYYY}-{SEQYEAR}.",
        "missing": colander.required,
        "validator": get_number_template_validator(
            SupplierInvoiceNumberService
        ),
    },
    "global_supplierinvoice_sequence_init_value": {
        "title": "Valeur à laquelle on initialise la séquence globale",
        "section": "Séquence globale (SEQGLOBAL)",
        "type": colander.Int(),
        "validator": colander.Range(min=0),
    },
    "year_supplierinvoice_sequence_init_value": {
        "title": "Valeur à laquelle on initialise la séquence annuelle",
        "section": "Séquence annuelle (SEQYEAR)",
        "type": colander.Int(),
        "validator": colander.Range(min=0),
    },
    "year_supplierinvoice_sequence_init_date": {
        "title": "Date à laquelle on initialise la séquence annuelle",
        "section": "Séquence annuelle (SEQYEAR)",
        "widget": deform_extensions.CustomDateInputWidget(),
    },
    "month_supplierinvoice_sequence_init_value": {
        "title": "Valeur à laquelle on initialise la séquence mensuelle",
        "section": "Séquence mensuelle (SEQMONTH)",
        "type": colander.Int(),
        "validator": colander.Range(min=0),
    },
    "month_supplierinvoice_sequence_init_date": {
        "title": "Date à laquelle on initialise la séquence mensuelle",
        "section": "Séquence mensuelle (SEQMONTH)",
        "widget": deform_extensions.CustomDateInputWidget(),
    },
    "internalsupplierinvoice_number_template": {
        "title": "Gabarit du numéro de facture fournisseur interne",
        "description": "Peut contenir des caractères (préfixes, "
        "séparateurs… etc), ainsi que des variables et séquences. Ex: "
        "{YYYY}-{SEQYEAR}.",
        "missing": colander.required,
        "validator": get_number_template_validator(
            InternalSupplierInvoiceNumberService
        ),
    },
    "global_internalsupplierinvoice_sequence_init_value": {
        "title": "Valeur à laquelle on initialise la séquence globale",
        "section": "Séquence globale (SEQGLOBAL)",
        "type": colander.Int(),
        "validator": colander.Range(min=0),
    },
    "year_internalsupplierinvoice_sequence_init_value": {
        "title": "Valeur à laquelle on initialise la séquence annuelle",
        "section": "Séquence annuelle (SEQYEAR)",
        "type": colander.Int(),
        "validator": colander.Range(min=0),
    },
    "year_internalsupplierinvoice_sequence_init_date": {
        "title": "Date à laquelle on initialise la séquence annuelle",
        "section": "Séquence annuelle (SEQYEAR)",
        "widget": deform_extensions.CustomDateInputWidget(),
    },
    "month_internalsupplierinvoice_sequence_init_value": {
        "title": "Valeur à laquelle on initialise la séquence mensuelle",
        "section": "Séquence mensuelle (SEQMONTH)",
        "type": colander.Int(),
        "validator": colander.Range(min=0),
    },
    "month_internalsupplierinvoice_sequence_init_date": {
        "title": "Date à laquelle on initialise la séquence mensuelle",
        "section": "Séquence mensuelle (SEQMONTH)",
        "widget": deform_extensions.CustomDateInputWidget(),
    },
    "ungroup_supplier_invoices_export": {
        "title": "Ne pas grouper les écritures dans les exports de factures\
            fournisseurs",
        "description": "Si cette case est cochée, lors de l'export comptable\
            des factures fournisseurs, une écriture par ligne de la facture\
            sera affichée (et non pas un total par types de dépense).",
        "widget": deform.widget.CheckboxWidget(
            true_val='1',
            false_val='0'
        ),
        "section": "Factures fournisseur",
    },
    "treasury_measure_ui": {
        "title": "Indicateur à mettre en évidence",
        "description": "Indicateur qui sera mis en évidence dans l'interface "
        "entrepreneur",
        "widget": forms.get_deferred_model_select(
            TreasuryMeasureType,
            mandatory=True,
            widget_class=deform.widget.RadioChoiceWidget,
        )
    },
    "invoice_number_template": {
        "title": "Gabarit du numéro de facture",
        "description": "Peut contenir des caractères (préfixes, "
        "séparateurs… etc), ainsi que des variables et séquences. Ex: "
        "{YYYY}-{SEQYEAR}.",
        "missing": colander.required,
        "validator": get_number_template_validator(InvoiceNumberService),
    },
    "global_invoice_sequence_init_value": {
        "title": "Valeur à laquelle on initialise la séquence globale",
        "section": "Séquence globale (SEQGLOBAL)",
        "type": colander.Int(),
        "validator": colander.Range(min=0),
    },
    "year_invoice_sequence_init_value": {
        "title": "Valeur à laquelle on initialise la séquence annuelle",
        "section": "Séquence annuelle (SEQYEAR)",
        "type": colander.Int(),
        "validator": colander.Range(min=0),
    },
    "year_invoice_sequence_init_date": {
        "title": "Date à laquelle on initialise la séquence annuelle",
        "section": "Séquence annuelle (SEQYEAR)",
        "widget": deform_extensions.CustomDateInputWidget(),
    },
    "month_invoice_sequence_init_value": {
        "title": "Valeur à laquelle on initialise la séquence mensuelle",
        "section": "Séquence mensuelle (SEQMONTH)",
        "type": colander.Int(),
        "validator": colander.Range(min=0),
    },
    "month_invoice_sequence_init_date": {
        "title": "Date à laquelle on initialise la séquence mensuelle",
        "section": "Séquence mensuelle (SEQMONTH)",
        "widget": deform_extensions.CustomDateInputWidget(),
    },
    "internalinvoice_number_template": {
        "title": "Gabarit du numéro de facture",
        "description": "Peut contenir des caractères (préfixes, "
        "séparateurs… etc), ainsi que des variables et séquences. Ex: "
        "{YYYY}-{SEQYEAR}.",
        "missing": colander.required,
        "validator": get_number_template_validator(InvoiceNumberService),
    },
    "global_internalinvoice_sequence_init_value": {
        "title": "Valeur à laquelle on initialise la séquence globale",
        "section": "Séquence globale (SEQGLOBAL)",
        "type": colander.Int(),
        "validator": colander.Range(min=0),
    },
    "year_internalinvoice_sequence_init_value": {
        "title": "Valeur à laquelle on initialise la séquence annuelle",
        "section": "Séquence annuelle (SEQYEAR)",
        "type": colander.Int(),
        "validator": colander.Range(min=0),
    },
    "year_internalinvoice_sequence_init_date": {
        "title": "Date à laquelle on initialise la séquence annuelle",
        "section": "Séquence annuelle (SEQYEAR)",
        "widget": deform_extensions.CustomDateInputWidget(),
    },
    "month_internalinvoice_sequence_init_value": {
        "title": "Valeur à laquelle on initialise la séquence mensuelle",
        "section": "Séquence mensuelle (SEQMONTH)",
        "type": colander.Int(),
        "validator": colander.Range(min=0),
    },
    "month_internalinvoice_sequence_init_date": {
        "title": "Date à laquelle on initialise la séquence mensuelle",
        "section": "Séquence mensuelle (SEQMONTH)",
        "widget": deform_extensions.CustomDateInputWidget(),
    },
    "accounting_label_maxlength": {
        "title": "Taille maximum des libellés d'écriture (troncature)",
        "description": "enDI tronquera les libellés d'écriture comptable "
        "exportés à cette longueur. Dépend de votre logiciel de comptabilité. "
        "Ex :  30 pour quadra, 35 pour sage, 25 pour ciel. Mettre à zéro "
        "pour désactiver la troncature.",
        "type": colander.Int(),
    },
    "accounting_closure_day": {
        "title": "Jour de la clôture comptable (1-31)",
        "section": "Date de la clôture comptable",
        "type": colander.Int(),
        "validator": colander.Range(1, 31),
    },
    "accounting_closure_month": {
        "title": "Mois de la clôture comptable (1-12)",
        "section": "Date de la clôture comptable",
        "type": colander.Int(),
        "validator": colander.Range(1, 12),
    },
    "cae_business_name": {
        "title": "Raison sociale",
        "description": "Le nom par lequel est désignée votre CAE",
    },
    "cae_legal_status": {
        "title": "Statut juridique",
    },
    "cae_address": {
        "title": "Adresse",
        "widget": deform.widget.TextAreaWidget(rows=2),
    },
    "cae_zipcode": {
        "title": "Code postal",
    },
    "cae_city": {
        "title": "Ville",
    },
    "cae_tel": {
        "title": "Téléphone",
    },
    "cae_contact_email": {
        "title": "Adresse e-mail de contact",
        "description": "Adresse e-mail de contact de la CAE (sera "
        "renseignée à titre informatif dans les métadonnées des factures PDF)",
        "validator": forms.mail_validator()
    },
    "cae_business_identification": {
        "title": "Numéro de SIRET",
    },
    "cae_intercommunity_vat": {
        "title": "Numéro de TVA intracommunautaire",
    },
    "cae_vat_collect_mode": {
        "title": "Mode de gestion de la TVA",
        "widget": deform.widget.SelectWidget(
            values=(
                ('debit', "Sur les débits"),
                ('encaissement', "Sur les encaissements"),
            )
        ),
    },
    # Facture fournisseur
    'code_journal_frns': {
        'title': "Code journal utilisé pour les factures fournisseurs",
    },
    'cae_general_supplier_account': {
        'title': "Compte fournisseur général de la CAE",
        'description': "Pour tous les fournissseurs de toutes les enseignes. "
        "une valeur spécifique pour une enseigne peut être paramétrée au "
        "niveau de l'enseigne. une valeur spécifique à un fournisseur "
        "peut être paramétrée dans chaque fiche fournisseur",
        'section': "Configuration des comptes fournisseurs",
    },
    'cae_third_party_supplier_account': {
        'title': "Compte fournisseur tiers de la CAE",
        'description': "Pour tous les fournissseurs de toutes les enseignes. "
        "une valeur spécifique pour une enseigne peut être paramétrée au "
        "niveau de l'enseigne. une valeur spécifique à un fournisseur "
        "peut être paramétrée dans chaque fiche fournisseur",
        'section': "Configuration des comptes fournisseurs",
    },
    'bookentry_supplier_invoice_label_template': {
        'title': "Gabarit pour les libellés d'écriture",
        'description': help_text_libelle_comptable,
        "section": "Factures fournisseur",
    },
    'bookentry_supplier_payment_label_template': {
        'title': "Gabarit pour les libellés d'écriture (paiements direct fournisseur)",
        'description': help_text_libelle_comptable,
        "section": "Paiements des factures fournisseur",
    },
    "bookentry_supplier_invoice_user_payment_label_template": {
        'title': "Gabarit pour les libellés d'écriture (remboursements entrepreneurs)",
        'description': help_text_libelle_comptable,
        "section": "Paiements des factures fournisseur",
    },
    "bookentry_supplier_invoice_user_payment_waiver_label_template": {
        'title': "Gabarit pour les libellés d'écriture (abandon de créance entrepreneur)",
        'description': help_text_libelle_comptable,
        "section": "Paiements des factures fournisseur",
    },
    # Factures fournisseur internes
    'internalcode_journal_frns': {
        'title': "Code journal utilisé pour les factures et paiements "
        "fournisseurs internes",
    },
    'internalcae_general_supplier_account': {
        'title': "Compte fournisseur général de la CAE",
        'description': "Pour tous les fournissseurs internes de toutes "
        "les enseignes. "
        "une valeur spécifique pour une enseigne peut être paramétrée au "
        "niveau de l'enseigne. une valeur spécifique à un fournisseur "
        "interne peut être paramétrée dans chaque fiche fournisseur",
        'section': "Configuration des comptes fournisseurs",
    },
    'internalcae_third_party_supplier_account': {
        'title': "Compte fournisseur tiers de la CAE",
        'description': "Pour tous les fournissseurs de toutes les enseignes. "
        "une valeur spécifique pour une enseigne peut être paramétrée au "
        "niveau de l'enseigne. une valeur spécifique à un fournisseur "
        "peut être paramétrée dans chaque fiche fournisseur",
        'section': "Configuration des comptes fournisseurs",
    },
    'internalbookentry_supplier_invoice_label_template': {
        'title': "Gabarit pour les libellés d'écriture",
        'description': help_text_libelle_comptable,
        "section": "Factures fournisseur",
    },
    'internalbookentry_supplier_payment_label_template': {
        'title': "Gabarit pour les libellés d'écriture",
        'description': help_text_libelle_comptable,
        "section": "Paiements fournisseur",
    },
    'company_general_ledger_accounts_filter': {
        'title': "Liste des comptes à afficher ou cacher "
    },
}


def get_config_key_schemanode(key, ui_conf):
    """
    Returns a schema node to configure the config 'key'
    This key should appear in the dict here above CONFIGURATION_KEYS
    """
    return colander.SchemaNode(
        ui_conf.get('type', colander.String()),
        title=ui_conf.get('title', key),
        label=ui_conf.get('label', None),
        description=ui_conf.get('description'),
        missing=ui_conf.get('missing', ""),
        name=key,
        widget=ui_conf.get('widget'),
        validator=ui_conf.get('validator', None),
    )


def get_config_schema(keys):
    """
    Returns a schema to configure Config objects

    :param list keys: The list of keys we want to configure (ui informations
    should be provided in the CONFIGURATION_KEYS dict

    :results: A colander Schema to configure the given keys
    :rtype: object colander Schema
    """
    schema = colander.Schema()
    mappings = {}
    index = 0
    for key in keys:
        ui_conf = CONFIGURATION_KEYS.get(key, {})
        node = get_config_key_schemanode(key, ui_conf)

        if "section" in ui_conf:  # This element should be shown in a mapping

            section_title = ui_conf['section']
            section_name = safe_ascii_str(section_title)
            if section_name not in mappings:
                mappings[section_name] = mapping = colander.Schema(
                    title=section_title,
                    name=section_name,
                )
                schema.add(mapping)
            else:
                mapping = mappings[section_name]
            mapping.add(node)
        else:
            schema.insert(index, node)
            index += 1

#    for mapping in mappings.values():
#        schema.add(mapping)
    return schema


def build_config_appstruct(request, keys):
    """
    Build the configuration appstruct regarding the config keys we want to edit

    :param obj request: The pyramid request object (with a config attribute)
    :param list keys: the keys we want to edit
    :returns: A dict storing the configuration values adapted to a schema
    generated by get_config_schema
    """
    appstruct = {}
    for key in keys:
        value = request.config.get(key, "")
        if value:
            ui_conf = CONFIGURATION_KEYS[key]

            if "section" in ui_conf:
                appstruct.setdefault(
                    safe_ascii_str(ui_conf['section']), {}
                )[key] = value
            else:
                appstruct[key] = value
    return appstruct


class ActivityTypeConfig(colander.MappingSchema):
    """
        Schema for the configuration of different activity types
    """
    id = forms.id_node()

    label = colander.SchemaNode(
        colander.String(),
        title="Libellé",
        validator=colander.Length(max=100)
        )


class ActivityTypesSeqConfig(colander.SequenceSchema):
    """
        The sequence Schema associated with the ActivityTypeConfig
    """
    activity_type = ActivityTypeConfig(
        title="Nature du rendez-vous",
        widget=CleanMappingWidget(),
    )


class ActivityModeConfig(colander.MappingSchema):
    label = colander.SchemaNode(
        colander.String(),
        title="libellé",
        validator=colander.Length(max=100)
        )


class ActivityModesSeqConfig(colander.SequenceSchema):
    """
    Sequence schema for activity modes configuration
    """
    activity_mode = ActivityModeConfig(
        title="Mode d'entretien",
        widget=CleanMappingWidget(),
    )


class ActionConfig(colander.MappingSchema):
    id = forms.id_node()
    label = colander.SchemaNode(
        colander.String(),
        title="Sous-titre",
        description="Sous-titre dans la sortie pdf",
        validator=colander.Length(max=100)
        )


class ActivitySubActionSeq(colander.SequenceSchema):
    subaction = ActionConfig(
        title="",
        widget=CleanMappingWidget(),
    )


class ActivityActionConfig(colander.Schema):
    id = forms.id_node()
    label = colander.SchemaNode(
        colander.String(),
        title="Titre",
        description="Titre dans la sortie pdf",
        validator=colander.Length(max=255)
    )
    children = ActivitySubActionSeq(
        title="",
        widget=CleanSequenceWidget(
            add_subitem_text_template="Ajouter un sous-titre",
        )
    )


class ActivityActionSeq(colander.SequenceSchema):
    action = ActivityActionConfig(
        title="Titre",
        widget=CleanMappingWidget(),
    )


class WorkshopInfo3(colander.MappingSchema):
    id = forms.id_node()
    label = colander.SchemaNode(
        colander.String(),
        title="Sous-titre 2",
        description="Sous-titre 2 dans la sortie pdf",
        validator=colander.Length(max=100)
    )


class WorkshopInfo3Seq(colander.SequenceSchema):
    child = WorkshopInfo3(
        title="Sous-titre 2",
        widget=CleanMappingWidget(),
    )


class WorkshopInfo2(colander.Schema):
    id = forms.id_node()
    label = colander.SchemaNode(
        colander.String(),
        title="Sous-titre",
        description="Sous-titre dans la sortie pdf",
        validator=colander.Length(max=255)
    )
    children = WorkshopInfo3Seq(
        title="",
        widget=CleanSequenceWidget(
            add_subitem_text_template="Ajouter un sous-titre 2",
            orderable=True,
        )
    )


class WorkshopInfo2Seq(colander.SequenceSchema):
    child = WorkshopInfo2(
        title="Sous-titre",
        widget=CleanMappingWidget(),
    )


class WorkshopInfo1(colander.Schema):
    id = forms.id_node()
    label = colander.SchemaNode(
        colander.String(),
        title="Titre",
        description="Titre dans la sortie pdf",
        validator=colander.Length(max=255)
    )
    children = WorkshopInfo2Seq(
        title='',
        widget=CleanSequenceWidget(
            add_subitem_text_template="Ajouter un sous-titre",
            orderable=True,
        )
    )


class WorkshopInfo1Seq(colander.SequenceSchema):
    actions = WorkshopInfo1(
        title='Titre',
        widget=CleanMappingWidget(),
    )


class ActivityConfigSchema(colander.Schema):
    """
    The schema for activity types configuration
    """
    header_img = ImageNode(
        title='En-tête des sortie PDF',
        missing=colander.drop,
    )
    footer_img = ImageNode(
        title='Image du pied de page des sorties PDF',
        description="Vient se placer au-dessus du texte du pied de page",
        missing=colander.drop,
    )
    footer = forms.textarea_node(
        title="Texte du pied de page des sorties PDF",
        missing="",
    )
    types = ActivityTypesSeqConfig(
        title="Configuration des natures de rendez-vous",
        widget=CleanSequenceWidget(
            add_subitem_text_template="Ajouter une nature de rendez-vous ",
            orderable=True,
        )
    )
    modes = ActivityModesSeqConfig(
        title="Configuration des modes d'entretien",
        widget=CleanSequenceWidget(
            add_subitem_text_template="Ajouter un mode d'entretien",
            orderable=True,
        )
    )
    actions = ActivityActionSeq(
        title="Configuration des titres disponibles pour la sortie PDF",
        widget=CleanSequenceWidget(
            add_subitem_text_template="Ajouter un titre",
            orderable=True,
        )
    )


class WorkshopConfigSchema(colander.Schema):
    header_img = ImageNode(
        title='En-tête des sortie PDF',
        missing=colander.drop,
    )
    footer_img = ImageNode(
        title='Image du pied de page des sorties PDF',
        description="Vient se placer au-dessus du texte du pied de page",
        missing=colander.drop,
    )
    footer = forms.textarea_node(
        title="Texte du pied de page des sorties PDF",
        missing="",
    )
    actions = WorkshopInfo1Seq(
        title="Configuration des titres disponibles pour la sortie PDF",
        widget=CleanSequenceWidget(
            add_subitem_text_template="Ajouter une titre",
            orderable=True,
        )
    )


def load_filetypes_from_config(config):
    """
        Return filetypes configured in databas
    """
    attached_filetypes = json.loads(config.get('attached_filetypes', '[]'))
    if not isinstance(attached_filetypes, list):
        attached_filetypes = []
    return attached_filetypes


def get_element_by_name(list_, name):
    """
        Return an element from list_ which has the name "name"
    """
    found = None
    for element in list_:
        if element.name == name:
            found = element
    return found


def merge_config_datas(dbdatas, appstruct):
    """
        Merge the datas returned by form validation and the original dbdatas
    """
    flat_appstruct = forms.flatten_appstruct(appstruct)
    for name, value in list(flat_appstruct.items()):
        dbdata = get_element_by_name(dbdatas, name)
        if not dbdata:
            # The key 'name' doesn't exist in the database, adding new one
            dbdata = Config(name=name, value=value)
            dbdatas.append(dbdata)
        else:
            dbdata.value = value
    return dbdatas


def get_sequence_model_admin(model, title="", excludes=(), **kw):
    """
    Return a schema for configuring sequence of models

        model

            The SQLAlchemy model to configure
    """
    node_schema = SQLAlchemySchemaNode(
        model,
        widget=CleanMappingWidget(),
        excludes=excludes,
    )
    node_schema.name = 'data'

    colanderalchemy_config = getattr(model, '__colanderalchemy_config__', {})

    default_widget_options = dict(
        orderable=True,
        min_len=1,
    )
    widget_options = colanderalchemy_config.get('seq_widget_options', {})
    widget_options.update(kw.get('widget_options', {}))

    for key, value in list(widget_options.items()):
        default_widget_options[key] = value

    schema = colander.SchemaNode(colander.Mapping())
    schema.add(
        colander.SchemaNode(
            colander.Sequence(),
            node_schema,
            widget=CleanSequenceWidget(
                **default_widget_options
            ),
            title=title,
            name='datas')
    )

    def dictify(models):
        return {'datas': [node_schema.dictify(model) for model in models]}

    def objectify(datas):
        return [node_schema.objectify(data) for data in datas]

    schema.dictify = dictify
    schema.objectify = objectify
    return schema


class SubCompetenceConfigSchema(colander.MappingSchema):
    id = forms.id_node()
    label = colander.SchemaNode(
        colander.String(),
        title="Libellé",
    )


class SubCompetencesConfigSchema(colander.SequenceSchema):
    subcompetence = SubCompetenceConfigSchema(
        widget=CleanMappingWidget(),
    )


class CompetenceRequirement(colander.MappingSchema):
    deadline_id = forms.id_node()
    deadline_label = colander.SchemaNode(
        colander.String(),
        widget=deform.widget.TextInputWidget(readonly=True),
        title="Pour l'échéance",
        missing=colander.drop,
    )
    scale_id = colander.SchemaNode(
        colander.Integer(),
        title="Niveau requis",
        description="Sera mis en évidence dans l'interface",
        widget=forms.get_deferred_select(CompetenceScale)
    )


class CompetenceRequirementSeq(colander.SequenceSchema):
    requirement = CompetenceRequirement(
        title='',
        widget=CleanMappingWidget(),
    )


@colander.deferred
def deferred_seq_widget(nodex, kw):
    elements = kw['deadlines']
    return CleanSequenceWidget(
        add_subitem_text_template="-",
        min_len=len(elements),
        max_len=len(elements),
    )


@colander.deferred
def deferred_deadlines_default(node, kw):
    """
    Return the defaults to ensure there is a requirement for each configured
    deadline
    """
    return [
        {
            'deadline_label': deadline.label,
            'deadline_id': deadline.id,
        }
        for deadline in kw['deadlines']
    ]


class CompetencePrintConfigSchema(colander.Schema):
    header_img = ImageNode(title='En-tête de la sortie imprimable')


def get_admin_schema(factory):
    """
    Return an edit schema for the given factory

    :param obj factory: A SQLAlchemy model
    :returns: A SQLAlchemySchemaNode schema
    :rtype: class:`SQLAlchemySchemaNode`
    """
    schema = SQLAlchemySchemaNode(factory)
    return schema


def get_admin_configurable_option_schema(
    factory: Type[ConfigurableOption]
) -> SQLAlchemySchemaNode:
    """
    Return an add/edit schema for a factory that is a subclass of the
    ConfigurableOption model
    """
    schema = SQLAlchemySchemaNode(factory, includes=('label',))
    forms.customize_field(schema, 'label', title="Libellé")
    return schema
