"""
    Company form schemas
"""
import logging
from typing import (
    Iterable,
    Union,
)

import colander
import deform
import deform_extensions

from endi.models.company import (
    CompanyActivity,
    Company,
)
from endi.models.user.login import Login
from endi.models.user.user import User
from endi import forms
from endi.forms.custom_types import QuantityType
from endi.forms import (
    files,
    lists,
)
from endi.utils.image import (
    ImageResizer,
    ImageRatio,
)

log = logging.getLogger(__name__)

HEADER_RATIO = ImageRatio(4, 1)
HEADER_RESIZER = ImageResizer(2000, 500)


@colander.deferred
def deferred_edit_adminonly_widget(node, kw):
    """
        return a deferred adminonly edit widget
    """
    request = kw['request']
    if not request.has_permission('admin_company', request.context):
        return deform_extensions.DisabledInput()
    else:
        return deform.widget.TextInputWidget()


@colander.deferred
def deferred_upload_header_widget(node, kw):
    request = kw['request']
    tmpstore = files.SessionDBFileUploadTempStore(
        request,
        filters=[
            HEADER_RATIO.complete,
            HEADER_RESIZER.complete,
        ]
    )
    return files.CustomFileUploadWidget(
        tmpstore,
        show_delete_control=True,
    )


@colander.deferred
def deferred_upload_logo_widget(node, kw):
    request = kw['request']
    tmpstore = files.SessionDBFileUploadTempStore(request)
    return files.CustomFileUploadWidget(
        tmpstore,
        show_delete_control=True,
    )


def remove_admin_fields(schema, kw):
    """
        Remove admin only fields from the company schema
    """
    request = kw['request']
    if not request.has_permission("admin_treasury", request.context):
        del schema['RIB']
        del schema['IBAN']
        del schema['code_compta']
        del schema['contribution']
        del schema['internalcontribution']
        del schema['internal']
        del schema['general_customer_account']
        del schema['third_party_customer_account']
        del schema['general_supplier_account']
        del schema['third_party_supplier_account']
        del schema['internalgeneral_customer_account']
        del schema['internalthird_party_customer_account']
        del schema['internalgeneral_supplier_account']
        del schema['internalthird_party_supplier_account']


@colander.deferred
def deferred_company_datas_select(node, kw):
    values = CompanyActivity.query('id', 'label').all()
    values.insert(0, ('', "- Sélectionner un type d'activité"))
    return deform.widget.SelectWidget(
        values=values
    )


@colander.deferred
def deferred_company_datas_validator(node, kw):
    ids = [entry[0] for entry in CompanyActivity.query('id')]
    return colander.OneOf(ids)


class CompanyActivitySchema(colander.SequenceSchema):
    id = colander.SchemaNode(
        colander.Integer(),
        title="un domaine",
        widget=deferred_company_datas_select,
        validator=deferred_company_datas_validator,
        missing=colander.drop,
    )


class CompanySchema(colander.MappingSchema):
    """
        Company add/edit form schema
    """
    user_id = forms.id_node()
    name = colander.SchemaNode(
        colander.String(),
        widget=deferred_edit_adminonly_widget,
        title='Nom',
        section='Informations publiques',
        validator=forms.max_len_validator(150),
    )

    goal = colander.SchemaNode(
        colander.String(),
        title="Descriptif de l'activité",
        section='Informations publiques',
        validator=forms.max_len_validator(255),
    )

    activities = CompanyActivitySchema(
        title="Domaines d'activité",
        missing=colander.drop,
        section='Informations publiques',
    )

    email = forms.mail_node(
        missing='',
        section='Informations publiques',
    )

    phone = colander.SchemaNode(
        colander.String(),
        title='Téléphone',
        section='Informations publiques',
        validator=forms.max_len_validator(20),
        missing='',
    )

    mobile = colander.SchemaNode(
        colander.String(),
        title='Téléphone portable',
        section='Informations publiques',
        validator=forms.max_len_validator(20),
        missing='',
    )

    address = colander.SchemaNode(
        colander.String(),
        title="Adresse",
        section='Informations publiques',
        missing='',
        validator=forms.max_len_validator(255),
    )

    zip_code = colander.SchemaNode(
        colander.String(),
        title="Code postal",
        section='Informations publiques',
        missing='',
        validator=forms.max_len_validator(20),
    )

    city = colander.SchemaNode(
        colander.String(),
        title="Ville",
        section='Informations publiques',
        missing='',
        validator=forms.max_len_validator(255),
    )

    country = colander.SchemaNode(
        colander.String(),
        title="Pays",
        section='Informations publiques',
        missing='France',
        validator=forms.max_len_validator(150),
    )

    logo = files.ImageNode(
        widget=deferred_upload_logo_widget,
        title="Choisir un logo",
        section="Informations publiques",
        missing=colander.drop,
        description="Ce logo n’est affiché dans vos documents (devis, \
factures…) que si aucun En-tête des fichiers PDF n’est renseigné dans \
la rubrique Personnalisation des documents ci-dessous.  ",
    )
    header = files.ImageNode(
        widget=deferred_upload_header_widget,
        title='En-tête des fichiers PDF',
        section="Personnalisation des documents",
        missing=colander.drop,
        description="Le fichier est idéalement au format 5/1 (par exemple \
1000px x 200 px). Remplace l’en-tête par défaut qui utilise les \
informations publiques. ",
    )

    # Fields specific to the treasury
    internal = colander.SchemaNode(
        colander.Boolean(),
        title='Enseigne interne à la CAE',
        section="Paramètres techniques (compta, gestion)",
        missing='',
        description="""
            À cocher si l'enseigne est utilisé pour abriter l'activité
            interne à la CAE, par opposition avec l'activité des entrepreneurs.
            Vous pouvez aussi configurer les enseignres internes dans
            Configuration → Configuration Générale →
            Enseigne(s) interne(s) à la CAE.
        """,
    )

    code_compta = colander.SchemaNode(
        colander.String(),
        title="Compte analytique",
        section="Paramètres techniques (compta, gestion)",
        description="Compte analytique utilisé dans le logiciel de \
comptabilité",
        missing="",
        validator=forms.max_len_validator(30),
    )

    general_customer_account = colander.SchemaNode(
        colander.String(),
        title="Compte client général",
        section="Paramètres techniques (compta, gestion)",
        description="Laisser vide pour utiliser les paramètres de la "
        "configuration générale""",
        missing="",
        validator=forms.max_len_validator(255),
    )

    third_party_customer_account = colander.SchemaNode(
        colander.String(),
        title="Compte client tiers",
        section="Paramètres techniques (compta, gestion)",
        description="Laisser vide pour utiliser les paramètres de la "
        "configuration générale",
        missing="",
        validator=forms.max_len_validator(255),
    )

    general_supplier_account = colander.SchemaNode(
        colander.String(),
        title="Compte fournisseur général",
        section="Paramètres techniques (compta, gestion)",
        description="Laisser vide pour utiliser les paramètres de la "
        "configuration générale",
        missing="",
        validator=forms.max_len_validator(255),
    )

    third_party_supplier_account = colander.SchemaNode(
        colander.String(),
        title="Compte fournisseur tiers",
        section="Paramètres techniques (compta, gestion)",
        description="Laisser vide pour utiliser les paramètres de la "
        "configuration générale",
        missing="",
        validator=forms.max_len_validator(255),
    )
    internalgeneral_customer_account = colander.SchemaNode(
        colander.String(),
        title="Compte client général pour les clients internes",
        section="Paramètres techniques (compta, gestion)",
        description="Laisser vide pour utiliser les paramètres de la "
        "configuration générale""",
        missing="",
        validator=forms.max_len_validator(255),
    )

    internalthird_party_customer_account = colander.SchemaNode(
        colander.String(),
        title="Compte client tiers pour les clients internes",
        section="Paramètres techniques (compta, gestion)",
        description="Laisser vide pour utiliser les paramètres de la "
        "configuration générale",
        missing="",
        validator=forms.max_len_validator(255),
    )

    internalgeneral_supplier_account = colander.SchemaNode(
        colander.String(),
        title="Compte fournisseur général pour les fournisseurs internes",
        section="Paramètres techniques (compta, gestion)",
        description="Laisser vide pour utiliser les paramètres de la "
        "configuration générale",
        missing="",
        validator=forms.max_len_validator(255),
    )

    internalthird_party_supplier_account = colander.SchemaNode(
        colander.String(),
        title="Compte fournisseur tiers pour les fournisseurs internes",
        section="Paramètres techniques (compta, gestion)",
        description="Laisser vide pour utiliser les paramètres de la "
        "configuration générale",
        missing="",
        validator=forms.max_len_validator(255),
    )

    # bank_account = colander.SchemaNode(
    #     colander.String(),
    #     title="Compte de banque",
    #     description="",
    #     missing="",
    # )

    # custom_insurance_rate = colander.SchemaNode(
    #         QuantityType(),
    #         widget=deform.widget.TextInputWidget(
    #             input_append="%",
    #             css_class="col-md-1"
    #             ),
    #         validator=colander.Range(
    #             min=0,
    #             max=100,
    #             min_err="Veuillez fournir un nombre supérieur à 0",
    #             max_err="Veuillez fournir un nombre inférieur à 100"
    #         ),
    #         title="Taux de Responsabilité Civile Professionnel",
    #         missing=colander.drop,
    #         description="Pourcentage du taux d'assurance professionnelle "
    #                     "de cette enseigne dans la CAE",
    # )

    contribution = colander.SchemaNode(
        QuantityType(),
        widget=deform.widget.TextInputWidget(
            input_append="%",
            css_class="col-md-1"
        ),
        validator=colander.Range(
            min=0,
            max=100,
            min_err="Veuillez fournir un nombre supérieur à 0",
            max_err="Veuillez fournir un nombre inférieur à 100"),
        title="Contribution à la CAE",
        section="Paramètres techniques (compta, gestion)",
        missing=None,
        description="Pourcentage que cette enseigne contribue à la CAE",
    )
    internalcontribution = colander.SchemaNode(
        QuantityType(),
        widget=deform.widget.TextInputWidget(
            input_append="%",
            css_class="col-md-1"
        ),
        validator=colander.Range(
            min=0,
            max=100,
            min_err="Veuillez fournir un nombre supérieur à 0",
            max_err="Veuillez fournir un nombre inférieur à 100"),
        title="Contribution à la CAE pour la facturation interne",
        section="Paramètres techniques (compta, gestion)",
        missing=None,
        description=(
            "Pourcentage que cette enseigne contribue à la CAE lorsqu'elle "
            "facture en interne"
        )
    )

    cgv = forms.textarea_node(
        title="Conditions générales complémentaires",
        section="Personnalisation des documents",
        richwidget=True,
        missing="",
    )
    decimal_to_display = colander.SchemaNode(
        colander.Integer(),
        widget=deform.widget.SelectWidget(
            values=(
                ("2", "2 décimales (1,25 €)"),
                ("5", "5 décimales (1,24952€)"),
            ),
        ),
        title="Nombre de décimales à afficher dans les sous-totaux",
        description="Pour les prix unitaires et les sous-totaux HT, "
        "Indiquez le nombre de décimales à afficher",
        default=2,
        missing=2,
        section="Personnalisation des documents",
    )

    RIB = colander.SchemaNode(
        colander.String(),
        title='RIB',
        section="Paramètres techniques (compta, gestion)",
        missing='',
        validator=forms.max_len_validator(255),
    )

    IBAN = colander.SchemaNode(
        colander.String(),
        title='IBAN',
        section="Paramètres techniques (compta, gestion)",
        missing='',
        validator=forms.max_len_validator(255),
    )

    general_overhead = colander.SchemaNode(
        colander.Float(),
        title="Coefficient de frais généraux",
        section="Coefficients de calcul des études de prix",
        missing=None,
        description="""
        Coefficient de frais généraux utilisé pour le calcul des coûts dans le
        catalogue produit et les études de prix, permet le calcul du coût de
        revient. Permet la définition du coût de revient des prestations
        vendues (ex: 0.2)
        """,
        validator=colander.Range(
            min=0,
            max=9.99,
            min_err='Doit être supérieurà 0',
            max_err="Doit être inférieur à 9.99",
        )
    )
    margin_rate = colander.SchemaNode(
        colander.Float(),
        title="Coefficient de marge",
        section="Coefficients de calcul des études de prix",
        missing=None,
        description="""
        Coefficient de marge utilisé pour le calcul des coûts dans le catalogue
        produit et les études de prix. Permet le calcul du coût intermédiaire
        d'une prestations (ex: 0.1)""",
        validator=colander.Range(
            min=0,
            max=9.99,
            min_err='Doit être supérieurà 0',
            max_err="Doit être inférieur à 9.99",
        )
    )

    come_from = forms.come_from_node()


COMPANYSCHEMA = CompanySchema(after_bind=remove_admin_fields)


def get_deferred_company_choices(widget_options):
    """
    Build a deferred for company selection widget

    avaialble widget_options

        default_option

            A default option that will be inserted in the list

        active_only

            Should we restrict the query to active companies ?

        query_func

            Provide a custom Company query func that returns a list of 2-uples
            (id, label) the function should take a kw parameter. kw are the
            colander schema binding parameters
    """
    default_option = widget_options.pop('default_option', None)
    active_only = widget_options.get('active_only', False)
    query = widget_options.get(
        "query", Company.query_for_select(active_only)
    )
    more_options = widget_options.get('more_options')

    @colander.deferred
    def deferred_company_choices(node, kw):
        """
        return a deferred company selection widget
        """
        if callable(query):
            values = query(kw).all()
        else:
            values = query.all()
        if more_options:
            for option in more_options:
                values.insert(0, option)
        if default_option:
            values.insert(0, default_option)
        return deform.widget.Select2Widget(
            values=values,
            **widget_options
            )
    return deferred_company_choices


def company_node(multiple=False, **kw):
    """
    Return a schema node for company selection
    """
    widget_options = kw.pop('widget_options', {})
    return colander.SchemaNode(
        colander.Set() if multiple else colander.Integer(),
        widget=get_deferred_company_choices(widget_options),
        **kw
    )


company_choice_node = forms.mk_choice_node_factory(
    company_node,
    resource_name='une enseigne',
    resource_name_plural='de zéro à plusieurs enseignes',
)

company_filter_node_factory = forms.mk_filter_node_factory(
    company_node,
    title="Enseigne",
    empty_filter_msg='Toutes',
)


def get_list_schema(company=False):
    """
    Return a schema for filtering companies list
    """
    schema = lists.BaseListsSchema().clone()
    schema['search'].title = "Nom de l'enseigne"
    schema.add(
        colander.SchemaNode(
            colander.Boolean(),
            name='include_inactive',
            title="",
            label="Inclure les enseignes désactivées",
            default=False,
        )
    )
    schema.add(
        colander.SchemaNode(
            colander.Boolean(),
            name='include_internal',
            title="",
            label="Inclure les enseignes internes à la CAE",
            default=False,
        )
    )
    return schema


def get_deferred_company_attr_default(attrname):
    """
    Build a deferred default value returning the value of the company attribute
    attrname

    NB : Expects the request.context to be a company or to have a
    request.context.company

    :param str attrname: Name of the company attribute to retrieve
    :rtype: colander.deferred
    """
    @colander.deferred
    def deferred_value(node, kw):
        context = kw['request'].context
        if isinstance(context, Company):
            value = getattr(context, attrname)
        elif hasattr(context, 'company'):
            value = getattr(context.company, attrname)
        else:
            value = 0
        return value
    return deferred_value


def get_employees_from_request(request) -> Iterable[User]:
    assert isinstance(request.context, Company)
    query = User.query().join(Company.employees).join(User.login)
    query = query.filter(
        Company.id == request.context.id,
        Login.active == True,  # noqa E712
    )
    return query


def get_default_employee_from_request(request) -> Union[User, None]:
    """
    Preselects the employee if there is only one or if it is the currently
    logged user, else no default : up for selection.
    """
    query = get_employees_from_request(request)
    if query.count() > 1:
        # If I am a company user, select me by default
        # May return None else, which is expected
        logged_company_user = query.filter(User.id == request.user.id).first()
        if logged_company_user is None:
            return None
        else:
            return logged_company_user
    else:
        return query.first()
