import colander
from colanderalchemy import SQLAlchemySchemaNode

from endi.forms import customize_field
from endi.forms.lists import BaseListsSchema
from endi.models.project.business import Business


def get_list_schema():
    """
    Return the schema for the project search form
    :rtype: colander.Schema
    """
    schema = BaseListsSchema().clone()

    schema['search'].title = "Nom de l'affaire"

    schema.add(
        colander.SchemaNode(
            colander.Boolean(),
            name='closed',
            title="",
            label="Inclure les affaires terminées",
            missing=True,
        )
    )

    return schema


def get_business_edit_schema():
    """
    Build the businedd edition schema

    :rtype: :class:`colander.Schema`
    """
    schema = SQLAlchemySchemaNode(
        Business,
        includes=('name',)
    )
    customize_field(schema, 'name', title="Nom de l'affaire")
    return schema
