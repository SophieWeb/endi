"""
Accounting module related schemas
"""
import re
import datetime
import colander
import deform
import deform_extensions

from colanderalchemy import SQLAlchemySchemaNode
from sqlalchemy import distinct

from endi_base.models.base import DBSESSION
from endi.compute.parser import NumericStringParser
from endi.models.accounting.operations import AccountingOperation
from endi.models.accounting.treasury_measures import (
    TreasuryMeasureGrid,
    TreasuryMeasureTypeCategory,
)
from endi.models.accounting.income_statement_measures import (
    IncomeStatementMeasureType,
    IncomeStatementMeasureTypeCategory,
    IncomeStatementMeasureGrid,
)
from endi.models.accounting.accounting_closures import (
    AccountingClosure,
)
from endi.models.company import Company
from endi import forms
from endi.forms.custom_types import CsvTuple
from endi.forms.lists import BaseListsSchema
from endi.forms.widgets import CleanMappingWidget
from endi.forms.fields import YearPeriodSchema


class PeriodSchema(colander.MappingSchema):
    """
        A form used to select a period
    """
    start = colander.SchemaNode(
        colander.Date(),
        title="Remontées entre le",
        description="",
        default=datetime.date(datetime.date.today().year, 1, 1),
        missing=colander.drop,
    )
    end = colander.SchemaNode(
        colander.Date(),
        title="et le",
        description="",
        default=datetime.date.today(),
        missing=colander.drop,
    )


class DebitAmountRangeSchema(colander.MappingSchema):
    """
    Used to filter on a range of debit amount
    """
    start = colander.SchemaNode(
        colander.Float(),
        title="Montant au débit entre",
        missing=colander.drop,
        description="",
    )
    end = colander.SchemaNode(
        colander.Float(),
        title="et",
        missing=colander.drop,
        description="",
    )


class CreditAmountRangeSchema(colander.MappingSchema):
    """
    Used to filter on a range of credit amount
    """
    start = colander.SchemaNode(
        colander.Float(),
        title="Montant au crédit entre",
        missing=colander.drop,
        description="",
    )
    end = colander.SchemaNode(
        colander.Float(),
        title="et",
        missing=colander.drop,
        description="",
    )


COMPLEX_TOTAL_HELP = """
    Combiner plusieurs catégories et indicateurs au travers d'opérations
    arithmétiques.
    Les noms des variables (catégories ou indicateurs) doivent être encadrés \
de {}.
    Exemple : {Salaires et Cotisations} + {Charges} / 100.
    Liste des catégories : %s. Liste des indicateurs : %s"""


def get_upload_list_schema():
    """
    Build a schema for Accounting Operation upload listing
    """
    schema = BaseListsSchema().clone()
    del schema['search']

    schema.insert(
        0,
        colander.SchemaNode(
            colander.String(),
            name='filetype',
            title="Type de fichier",
            widget=deform.widget.SelectWidget(
                values=(
                    ('all', 'Tous'),
                    ('general_ledger', "Grand livre"),
                    ('analytical_balance', "Balance analytique"),
                )
            ),
            default='all',
            missing=colander.drop,
        )
    )
    schema.insert(
        0,
        YearPeriodSchema(
            name='period',
            title="",
            widget=CleanMappingWidget(),
            missing=colander.drop,
        )
    )
    return schema


@colander.deferred
def deferred_analytical_account_widget(node, kw):
    """
    Defer analytical widget
    """
    datas = DBSESSION().query(
        distinct(AccountingOperation.analytical_account)
    ).order_by(AccountingOperation.analytical_account).all()
    datas = list(zip(*datas))[0]
    values = list(zip(datas, datas))
    values.insert(0, ('', 'Tous'))
    return deform.widget.Select2Widget(values=values)


@colander.deferred
def deferred_general_account_widget(node, kw):
    """
    Defer analytical widget
    """
    datas = DBSESSION().query(
        distinct(AccountingOperation.general_account)
    ).order_by(AccountingOperation.general_account).all()
    if datas:
        datas = list(zip(*datas))[0]
    values = list(zip(datas, datas))
    values.insert(0, ('', 'Tous'))
    return deform.widget.Select2Widget(values=values)


def _get_company_id_filter(node, kw):
    """
    Defer the company id selection widget
    """
    datas = DBSESSION().query(
        distinct(AccountingOperation.company_id)
    ).all()
    company_ids = list(zip(*datas))[0]
    return Company.id.in_(company_ids)


def get_operation_list_schema():
    """
    Build a schema listing operations
    """
    schema = BaseListsSchema().clone()
    del schema['search']
    schema.insert(
        0,
        colander.SchemaNode(
            colander.Boolean(),
            name='include_associated',
            title="",
            label="Inclure les opérations associées à une enseigne",
            default=True,
            missing=colander.drop,
        )
    )
    schema.insert(
        0,
        colander.SchemaNode(
            colander.String(),
            name='analytical_account',
            title="Compte analytique",
            widget=deferred_analytical_account_widget,
            missing=colander.drop,
        )
    )
    schema.insert(
        0,
        colander.SchemaNode(
            colander.String(),
            name='general_account',
            title="Compte général",
            widget=deferred_general_account_widget,
            missing=colander.drop,
        )
    )
    schema.insert(
        0,
        colander.SchemaNode(
            colander.Integer(),
            name='company_id',
            title="Enseigne",
            widget=forms.get_deferred_model_select(
                Company,
                filters=(_get_company_id_filter,),
                keys=('id', 'name'),
                empty_filter_msg='Toutes',
                widget_class=deform.widget.Select2Widget
            ),
            missing=colander.drop
        )
    )

    return schema


def get_company_general_ledger_operations_list_schema():
    """
    Build a schema for company general ledger listing operations
    """
    schema = BaseListsSchema().clone()
    del schema['search']
    schema.insert(
        0,
        colander.SchemaNode(
            colander.String(),
            name='general_account',
            title="Compte général",
            widget=deferred_general_account_widget,
            missing=colander.drop,
        )
    )
    schema.insert(
        1,
        PeriodSchema(
            name='period',
            title="",
            validator=colander.Function(
                forms.range_validator,
                msg="La date de début doit précéder la date de fin"
            ),
            widget=CleanMappingWidget(),
            missing=colander.drop,
        )
    )
    schema.insert(
        2,
        DebitAmountRangeSchema(
            name='debit',
            title="",
            validator=colander.Function(
                forms.range_validator,
                msg="Le montant de départ doit être inférieur ou égale \
    à celui de la fin"
            ),
            widget=CleanMappingWidget(),
            missing=colander.drop,
        )
    )
    schema.insert(
        3,
        CreditAmountRangeSchema(
            name='credit',
            title="",
            validator=colander.Function(
                forms.range_validator,
                msg="Le montant de départ doit être inférieur ou égale \
    à celui de la fin"
            ),
            widget=CleanMappingWidget(),
            missing=colander.drop,
        )
    )

    return schema


def get_upload_treasury_list_schema():
    """
    Build the schema used to list treasury measure grids by upload
    """
    schema = BaseListsSchema().clone()
    del schema['search']
    schema.insert(
        0,
        colander.SchemaNode(
            colander.Integer(),
            name='company_id',
            title="Enseigne",
            widget=forms.get_deferred_model_select(
                Company,
                filters=(_get_company_id_filter,),
                keys=('id', 'name'),
                empty_filter_msg='Toutes',
                widget_class=deform.widget.Select2Widget
            ),
            missing=colander.drop
        )
    )
    return schema


def get_treasury_measures_list_schema():
    """
    Build the schema used to list treasury measures

    :returns: A form schema
    :rtype: colander.Schema
    """
    schema = BaseListsSchema().clone()
    del schema['search']

    def get_year_options(kw):
        context = kw['request'].context
        if isinstance(context, TreasuryMeasureGrid):
            company_id = TreasuryMeasureGrid.company_id
        else:
            company_id = context.id
        return TreasuryMeasureGrid.get_years(company_id)

    node = forms.year_filter_node(
        name='year',
        query_func=get_year_options,
        title="Année de dépôt",
    )

    schema.insert(0, node)
    return schema


def get_income_statement_measures_list_schema():
    """
    Build the schema used to list treasury measures

    :returns: A form schema
    :rtype: colander.Schema
    """
    schema = BaseListsSchema().clone()
    del schema['search']
    del schema['page']
    del schema['items_per_page']

    def get_year_options(kw):
        cid = kw['request'].context.get_company_id()
        years = IncomeStatementMeasureGrid.get_years(company_id=cid)
        current_year = datetime.date.today().year
        if current_year not in years:
            years.append(current_year)
        return years

    node = forms.year_select_node(
        name='year',
        query_func=get_year_options,
        title=""
    )

    schema.insert(0, node)

    return schema


def get_deferred_widget_categories(category_class):
    """
    Returns a deferred widget used to select one or more categories
    """
    @colander.deferred
    def deferred_categories_widget(node, kw):
        query = DBSESSION().query(
            category_class.label,
            category_class.label,
        )
        choices = query.filter_by(active=True).all()
        return deform.widget.CheckboxChoiceWidget(values=choices)
    return deferred_categories_widget


def get_deferred_complex_total_description(category_class, type_class):
    """
    Returns a deferred description for the complex total configuration
    """
    @colander.deferred
    def deferred_description(node, kw):
        categories = ",".join(
            [
                i.label for i in
                category_class.get_categories(keys=('label',))
            ]
        )
        types = ",".join((
            i.label
            for i in type_class.get_types(keys=('label',))
        ))

        return COMPLEX_TOTAL_HELP % (categories, types)

    return deferred_description

def accounting_closure_year_validator(node, year):
    year_closure = AccountingClosure.query().filter_by(
         year=year
    ).all()

    if year_closure:
        raise colander.Invalid(
            node,
            "L'année de clôture existe déjà."
        )

@colander.deferred
def deferred_label_validator(node, kw):
    """
    Deffered label validator, check whether a type or a category has the same
    label
    """
    context = kw['request'].context

    category_query = DBSESSION().query(IncomeStatementMeasureTypeCategory.label)
    category_query.filter_by(active=True)

    if isinstance(context, IncomeStatementMeasureTypeCategory):
        category_query = category_query.filter(
            IncomeStatementMeasureTypeCategory.id != context.id
        )
    category_labels = [i[0] for i in category_query]

    type_query = DBSESSION().query(IncomeStatementMeasureType.label)
    type_query.filter_by(active=True)

    if isinstance(context, IncomeStatementMeasureType):
        type_query = type_query.filter(
            IncomeStatementMeasureType.id != context.id
        )
    type_labels = [i[0] for i in type_query]

    def label_validator(node, value):
        if ':' in value or '!' in value:
            raise colander.Invalid(
                node,
                "Erreur de syntax (les caractères ':' et '!' sont interdits"
            )

        if value in category_labels:
            raise colander.Invalid(
                node,
                "Une catégories porte déjà ce nom"
            )
        if value in type_labels:
            raise colander.Invalid(
                node,
                "Un type d'indicateurs porte déjà ce nom"
            )
    return label_validator


BRACES_REGEX = re.compile(r'\{([^}]+)\}\s?')


def complex_total_validator(node, value):
    """
    Validate the complex total syntax
    """
    if len(value) > 254:
        raise colander.Invalid(
            node,
            "Ce champ est limité à 255 caractères"
        )

    if value.count('{') != value.count('}'):
        raise colander.Invalid(
            node,
            "Erreur de syntaxe"
        )

    fields = BRACES_REGEX.findall(value)

    format_dict = dict((field, 1) for field in fields)
    try:
        temp = value.format(**format_dict)
    except Exception as err:
        raise colander.Invalid(
            node,
            "Erreur de syntaxe : {0}".format(err)
        )

    parser = NumericStringParser()
    try:
        temp = parser.eval(temp)
    except Exception as err:
        raise colander.Invalid(
            node,
            "Erreur de syntaxe : {0}".format(err)
        )


def get_admin_accounting_measure_type_schema(subclass, total=False):
    """
    Build the schema for accounting measure type edit/add

    Total types are more complex and can be :

        * The sum of categories
        * A list of account prefix (like the common type of measure_types)

    :param class subclass: The child class we want to edit
    (IncomeStatementMeasureTypeCategory or TreasuryMeasureTypeCategory)
    :param bool total: Are we editing a total type ?
    """
    if total:
        if subclass == IncomeStatementMeasureType:
            category_class = IncomeStatementMeasureTypeCategory
        else:
            category_class = TreasuryMeasureTypeCategory

        schema = SQLAlchemySchemaNode(
            subclass,
            includes=(
                "category_id",
                'label',
                "account_prefix",
                'is_total',
                'order',
            ),
        )
        schema['label'].validator = deferred_label_validator
        schema['is_total'].widget = deform.widget.HiddenWidget()
        schema.add_before(
            'account_prefix',
            colander.SchemaNode(
                colander.String(),
                name="total_type",
                title="Cet indicateur est il défini comme :",
                widget=deform_extensions.RadioChoiceToggleWidget(
                    values=(
                        (
                            "categories",
                            "la somme des indicateurs de une ou plusieurs "
                            "catégories ?",
                            "categories",
                        ),
                        (
                            "account_prefix",
                            "un groupement d'écritures ?",
                            "account_prefix",
                        ),
                        (
                            "complex_total",
                            "le résultat d'une formule arithmétique basée sur "
                            "les catégories et les indicateurs ?",
                            "complex_total",
                        ),
                    )
                ),
                missing=colander.drop,
            )
        )
        schema['account_prefix'].missing = ""

        deferred_description = get_deferred_complex_total_description(
            category_class, subclass
        )
        schema.add(
            colander.SchemaNode(
                colander.String(),
                name="complex_total",
                title="Combinaison complexe de catégories et d'indicateurs",
                description=deferred_description,
                validator=complex_total_validator,
                missing=""
            )
        )
        deferred_widget = get_deferred_widget_categories(category_class)

        schema.add(
            colander.SchemaNode(
                CsvTuple(),
                name="categories",
                title="Somme des catégories",
                description="Représentera la somme des catégories "
                "sélectionnées",
                widget=deferred_widget,
            )
        )
    else:
        schema = SQLAlchemySchemaNode(
            subclass,
            excludes=('is_total', "categories")
        )
        schema['label'].validator = deferred_label_validator
    return schema


def get_admin_accounting_type_category_schema(subclass):
    """
    Build the schema for accounting type category add/edit

    :param class subclass: The child class we want to edit
    (IncomeStatementMeasureTypeCategory or TreasuryMeasureTypeCategory)
    """
    schema = SQLAlchemySchemaNode(
        subclass,
        includes=("label", "order")
    )
    schema['label'].validator = deferred_label_validator
    return schema


def get_admin_accounting_closure_schema(subclass):
    """
    Build the schema for accounting closure add

    :param class subclass: The child class we want to edit
    (AcccountingClosure)
    """
    schema = SQLAlchemySchemaNode(
        subclass,
        includes=("year", "active")
    )
    schema['year'].validator = accounting_closure_year_validator
    return schema


def get_add_edit_accounting_operation_schema():
    """
    Build a schema for AccountingOperation add/edit
    """
    excludes = ("id", "upload_id", "company_id")

    schema = SQLAlchemySchemaNode(AccountingOperation, excludes=excludes)
    return schema
