"""
    Form schemas for accounting exports
"""
import colander
import deform

from endi.models.expense.sheet import get_expense_years
from endi.forms.user import (
    contractor_filter_node_factory,
    validator_filter_node_factory,
    issuer_filter_node_factory,
)
from endi.forms.company import company_filter_node_factory
from endi.forms.tasks.invoice import InvoicesRangeSchema
from endi import forms


@colander.deferred
def deferred_category(node, kw):
    return kw.get('prefix', '0')


class CategoryNode(colander.SchemaNode):
    schema_type = colander.String
    widget = deform.widget.HiddenWidget()
    default = deferred_category


class ExportedFieldNode(colander.SchemaNode):
    schema_type = colander.Boolean
    title = "Inclure les éléments déjà exportés ?"
    label = ""
    description = (
        "enDI retient les éléments qui ont déjà été "
        "exportés, vous pouvez décider ici de les inclure"
    )
    default = False
    missing = False


class PeriodSchema(colander.MappingSchema):
    """
        A form used to select a period
    """
    start_date = colander.SchemaNode(colander.Date(), title="Date de début")
    end_date = colander.SchemaNode(
        colander.Date(),
        title="Date de fin",
        missing=forms.deferred_today,
        default=forms.deferred_today
    )
    exported = ExportedFieldNode()

    def validator(self, form, value):
        """
            Validate the period
        """
        if value['start_date'] > value['end_date']:
            exc = colander.Invalid(
                form,
                "La date de début doit précéder la date de fin"
            )
            exc['start_date'] = "Doit précéder la date de fin"
            raise exc


class AllSchema(colander.MappingSchema):
    pass


class InvoiceDoctypeNode(colander.SchemaNode):
    schema_type = colander.String
    widget = deform.widget.RadioChoiceWidget(
        values=(
            ('all', 'Toutes les factures'),
            ('internal', 'Seules les factures internes'),
            ('external', 'Exclure les factures internes'),
        )
    )
    title = ""
    default = "all"
    missing = "all"


class InvoiceNumberSchema(InvoicesRangeSchema):
    """ Extends the date+number selector

    With filter on accountancy export status.
    """
    exported = ExportedFieldNode(insert_before='doctypes')
    doctypes = InvoiceDoctypeNode()
    validator_id = validator_filter_node_factory()


class InvoicePeriodSchema(PeriodSchema):
    doctypes = InvoiceDoctypeNode()
    validator_id = validator_filter_node_factory()


class InvoiceAllSchema(AllSchema):
    title = "Exporter les factures non exportées"
    doctypes = InvoiceDoctypeNode()
    validator_id = validator_filter_node_factory()


class PaymentDoctypeNode(InvoiceDoctypeNode):
    widget = deform.widget.RadioChoiceWidget(
        values=(
            ('all', 'Tous les encaissements'),
            ('internal', 'Seuls les encaissements des factures internes'),
            ('external', 'Exclure les encaissements factures internes'),
        )
    )


class PaymentAllSchema(AllSchema):
    title = "Exporter les encaissements non exportées"
    doctypes = PaymentDoctypeNode()
    issuer_id = issuer_filter_node_factory()


class PaymentPeriodSchema(PeriodSchema):
    title = "Exporter les encaissement des factures sur une période donnée"
    doctypes = PaymentDoctypeNode()
    issuer_id = issuer_filter_node_factory()


class ExpensePaymentPeriodSchema(PeriodSchema):
    title = "Exporter les paiements des notes de dépenses sur la période \
donnée"
    doctypes = PaymentDoctypeNode()
    issuer_id = issuer_filter_node_factory()


class ExpensePaymentAllSchema(AllSchema):
    title = "Exporter les paiements des notes de dépenses non exportés"
    doctypes = PaymentDoctypeNode()
    issuer_id = issuer_filter_node_factory()


class ExpenseAllSchema(AllSchema):
    category = CategoryNode()
    validator_id = validator_filter_node_factory()


class ExpenseSchema(colander.MappingSchema):
    """
    Schema for sage expense export
    """
    user_id = contractor_filter_node_factory()
    year = forms.year_select_node(title="Année", query_func=get_expense_years)
    month = forms.month_select_node(title="Mois")
    exported = ExportedFieldNode()
    category = CategoryNode()
    validator_id = validator_filter_node_factory()


class ExpenseNumberSchema(colander.MappingSchema):
    official_number = colander.SchemaNode(
        colander.String(),
        title="N° de pièce",
        description="N° de pièce de la note de dépenses "
        "(voir sur la page associée)")
    exported = ExportedFieldNode()
    category = CategoryNode()
    validator_id = validator_filter_node_factory()


class SupplierInvoiceDoctypeNode(InvoiceDoctypeNode):
    widget = deform.widget.RadioChoiceWidget(
        values=(
            ('all', 'Toutes les factures fournisseurs'),
            ('internal', 'Seules les factures internes'),
            ('external', 'Exclure les factures internes'),
        )
    )


class SupplierInvoiceAllSchema(AllSchema):
    title = "Exporter les factures fournisseurs non exportées"
    doctypes = SupplierInvoiceDoctypeNode()
    validator_id = validator_filter_node_factory()


class SupplierInvoicePeriodSchema(PeriodSchema):
    title = "Exporter les factures fournisseurs sur une période donnée"
    doctypes = SupplierInvoiceDoctypeNode()
    validator_id = validator_filter_node_factory()


class SupplierInvoiceSchema(colander.MappingSchema):
    """
    Schema for sage supplier invoice export
    """
    title = "Exporter les factures fournisseurs par enseigne"
    company_id = company_filter_node_factory()
    doctypes = SupplierInvoiceDoctypeNode()
    exported = ExportedFieldNode()
    validator_id = validator_filter_node_factory()


class SupplierInvoiceNumberSchema(colander.MappingSchema):
    official_number = colander.SchemaNode(
        colander.Integer(),
        title="N° de pièce",
        description="Numéro de pièce de la facture fournisseur "
        "(voir sur la page associée)")
    exported = ExportedFieldNode()
    validator_id = validator_filter_node_factory()


class SupplierPaymentNumberSchema(colander.MappingSchema):
    official_number = colander.SchemaNode(
        colander.Integer(),
        title="N° de pièce",
        description="Numéro de pièce de la facture fournisseur "
        "(voir sur la page associée)")
    exported = ExportedFieldNode()
    issuer_id = issuer_filter_node_factory()


class SupplierPaymentDoctypeNode(InvoiceDoctypeNode):
    widget = deform.widget.RadioChoiceWidget(
        values=(
            ('all', 'Tous les paiements'),
            (
                'internal',
                'Seuls les paiements des factures fournisseurs internes'
            ),
            (
                'external',
                'Exclure les paiements des factures fournisseurs internes'
            ),
        )
    )


class SupplierPaymentAllSchema(AllSchema):
    title = "Exporter les paiements fournisseurs non exportés"
    doctypes = SupplierPaymentDoctypeNode()
    issuer_id = issuer_filter_node_factory()


class SupplierPaymentPeriodSchema(PeriodSchema):
    title = "Exporter les paiements fournisseurs d'une période donnée"
    doctypes = SupplierPaymentDoctypeNode()
    issuer_id = issuer_filter_node_factory()


class BPFYearSchema(colander.MappingSchema):
    """
    Schema for BPF export (agregate of BusinessBPFData)
    """
    year = forms.year_select_node(
        title="Année",
        query_func=get_expense_years,
    )
    company_id = company_filter_node_factory()
    ignore_missing_data = colander.SchemaNode(
        colander.Boolean(),
        title="Forcer l'export",
        description="Ignorer les éléments dont le BPF n'est pas rempli.",
        default=False,
        missing=False,
    )
