"""
    Task form schemas (estimation, invoice ...)
    Note : since deform doesn't fit our form needs
            (estimation form is complicated and its structure
             is mixing many sqla tables)

    validation process:
        set all the line in the colander expected schema
        validate
        raise error on an adapted way
            build an error dict which is mako understandable

    merging process:
        get the datas
        build a factory
        merge estimation and task object
        commit
        merge all lines
        commit
        merge payment conditions
        commit

    formulaire :

    phase, displayUnits
    lignes de prestation descr, cout qtité, unité, tva
    lignes de remise descr, cout, tva (seulement les tvas utilisées dans
                                                    les lignes au-dessus)
    TOTAL HT
    for tva in used_tva:
        TOTAL TVA tva%
    TTC
    Frais de port
    TOTAL
"""
import logging
import functools

import colander
import deform

from endi.models.config import Config
from endi.models.company import Company
from endi.models.project.phase import Phase
from endi.models.project import Project
from endi.models.project.business import Business
from endi.models.project.types import BusinessType
from endi.models.services.naming import NamingService
from endi.models.third_party.customer import Customer
from endi.models.task import (
    Task,
    TaskLine,
    TaskLineGroup,
)

from endi.forms import today_node
from endi.forms.third_party.customer import customer_choice_node_factory


logger = logging.getLogger(__name__)
DAYS = (
    ('NONE', '-'),
    ('HOUR', 'Heure(s)'),
    ('DAY', 'Jour(s)'),
    ('WEEK', 'Semaine(s)'),
    ('MONTH', 'Mois'),
    ('FEUIL', 'Feuillet(s)'),
    ('PACK', 'Forfait'),
)


MAIN_INFOS_GRID = (
    (('date', 6), ('financial_year', 4), ),
    (('address', 6),),
    (('description', 12),),
    (('workplace', 6), (('start_date', 6)),),
    (('mention_ids', 12),),
    (('display_units', 12),),
    (('display_ttc', 12),),
)


# NEW TASK SCHEMA
# 1 - Project is current context : OK
# 2 - Duplication : Task is current context
# 3 - Customer is current context
# 4 - Company is current context
def _get_tasktype_from_request(request):
    route_name = request.matched_route.name
    try:
        result = request.context.type_
    except AttributeError:  # nontask context
        result = None
        pass  # fallback to the second method

    for predicate in ('estimation', 'cancelinvoice', 'invoice'):
        # Matches estimation and estimations
        if predicate in route_name:
            result = predicate
            break

    if result is None:
        raise ValueError('Cannot guess task type from request')
    return result


@colander.deferred
def deferred_default_name(node, kw):
    """
    Return a default name for the new document
    """
    request = kw['request']
    tasktype = _get_tasktype_from_request(request)
    method = "get_next_{0}_index".format(tasktype)

    if isinstance(request.context, (Project, Business)):
        if isinstance(request.context, Project):
            # New task
            # e.g : project.get_next_invoice_number()
            number = getattr(request.context, method)()
        else:
            number = getattr(request.context.project, method)()

        type_label = NamingService.get_label_for_context(
            tasktype,
            request.context,
        )
        name = f'{type_label} {number}'
    elif isinstance(request.context, Company):
        return ''
    else:
        # Duplication
        name = "Copie de {}".format(request.context.name)
    return name


def _get_project_customers(project):
    """
    Return project customers

    :param obj project: The current project
    :returns: A list of Customer instances
    :rtype: list
    """
    query = Customer.label_query()
    customers = query.filter(
        Customer.projects.any(Project.id == project.id)
    ).order_by(Customer.label)
    return customers


def _get_company_customers(context):
    """
    Return company customers

    :param obj context: The current Pyramid context
    :returns: A list of Customer instances
    :rtype: list
    """
    company_id = context.get_company_id()
    customers = Customer.label_query()
    customers = customers.filter_by(company_id=company_id).all()

    if hasattr(context, "project_id"):
        # Si le contexte est attaché à un dossier, on s'assure que les
        # clients du dossier sont présentés en haut de la liste des
        # clients en utilisant une fonction de tri custom

        project_id = context.project_id

        def sort_pid_first(a, b):
            """
            Sort customers moving the customers belonging to the current
            project
            up in the list
            """
            if project_id in a.get_project_ids():
                return -1
            elif project_id in b.get_project_ids():
                return 1
            else:
                return 0

        customers = sorted(customers, key=functools.cmp_to_key(sort_pid_first))
    return customers


def _get_customers_options(request):
    """
    Retrieve customers that should be presented to the end user

    Regarding the context we return :

        company customers
        project customers
    """
    context = request.context

    if isinstance(context, Project):
        customers = _get_project_customers(context)
    else:
        customers = _get_company_customers(context)

    return customers


@colander.deferred
def deferred_default_customer(node, kw):
    """
    Return a default customer if there is one in the request GET params or if
    there is only one in the project
    """
    request = kw['request']
    res = 0

    if isinstance(request.context, (Company, Project)):
        customers = request.context.customers
        if len(customers) == 1:
            res = customers[0].id

    elif isinstance(request.context, Task):
        res = request.context.customer_id
    return res


def _get_project_choices(projects):
    """
    Format project list to select options

    :param list projects: Project instances
    :returns: A list of 2-uple (id, label)
    :rtype: list
    """
    return [(project.id, project.name) for project in projects]


@colander.deferred
def deferred_project_widget(node, kw):
    """
        return phase select widget
    """
    if isinstance(kw['request'].context, Project):
        wid = deform.widget.HiddenWidget()
    else:
        customer_id = deferred_default_customer(node, kw)

        if customer_id != 0:
            projects = Project.get_customer_projects(customer_id)
        else:
            projects = []

        choices = _get_project_choices(projects)
        wid = deform.widget.SelectWidget(values=choices)
    return wid


@colander.deferred
def deferred_default_project(node, kw):
    """
    Return the default project
    """
    res = None
    request = kw['request']

    if isinstance(request.context, Company):
        if len(request.context.projects) == 1:
            res = request.context.projects[0].id
        else:
            res = colander.null
    elif request.context.type_ == 'project':
        res = request.context.id
    elif isinstance(request.context, Task):
        res = request.context.project.id
    return res


def _get_phases_from_request(request):
    """
    Get the phases from the current project regarding request context
    """
    phases = []
    if isinstance(request.context, Project):
        phases = Phase.query_for_select(request.context.id).all()
    elif hasattr(request.context, 'project_id'):
        # 'invoice', 'cancelinvoice', 'estimation'
        phases = Phase.query_for_select(request.context.project_id).all()
    return phases


@colander.deferred
def deferred_phases_widget(node, kw):
    """
        return phase select widget
    """
    request = kw['request']
    choices = _get_phases_from_request(request)

    choices.insert(0, ("", "Ne pas ranger dans un sous-dossier"))
    wid = deform.widget.SelectWidget(values=choices)
    return wid


@colander.deferred
def deferred_default_phase(node, kw):
    """
    Return the default phase if one is present in the request arguments
    """
    request = kw['request']
    phase_id = request.params.get('phase')
    if phase_id is not None:
        phases = _get_phases_from_request(request)
        if phase_id in [str(phase[0]) for phase in phases]:
            return int(phase_id)
        else:
            return colander.null
    elif hasattr(request.context, 'phase_id'):
        return request.context.phase_id or colander.null
    else:
        return colander.null


def get_business_types_from_request(request):
    """
    Collect available business types allowed for the current user/context

    :param obj request: The current Pyramid request
    """
    context = request.context
    project = None

    if isinstance(context, Project):
        project = context
    elif hasattr(context, "project"):
        project = context.project

    result = []

    if project:
        if project.project_type.default_business_type:
            result.append(project.project_type.default_business_type)

        for business_type in project.business_types:
            if business_type != project.project_type.default_business_type:
                if business_type.allowed(request):
                    result.append(business_type)
    else:
        result = [
            i for i in BusinessType.query_for_select()
            if i.allowed(request)
        ]

    # fix 1065
    if hasattr(context, "business_type") and context.business_type:
        if context.business_type not in result:
            result.append(context.business_type)
    return result


@colander.deferred
def business_type_id_validator(node, kw):
    allowed_ids = [
        i.id
        for i in get_business_types_from_request(kw['request'])
    ]
    return colander.OneOf(allowed_ids)


@colander.deferred
def deferred_business_type_description(node, kw):
    request = kw['request']
    business_types = get_business_types_from_request(request)
    if len(business_types) == 1:
        return ""
    else:
        return "Type d'affaire",


@colander.deferred
def deferred_business_type_widget(node, kw):
    """
    Collect the widget to display for business type selection

    :param node: The node we affect the widget to
    :param dict kw: The colander schema binding dict
    :returns: A SelectWidget or an hidden one
    """
    request = kw['request']
    business_types = get_business_types_from_request(request)
    if len(business_types) == 0:
        return deform.widget.HiddenWidget()
    else:
        return deform.widget.SelectWidget(
            values=[
                (business_type.id, business_type.label)
                for business_type in business_types
            ]
        )


@colander.deferred
def deferred_business_type_default(node, kw):
    """
    Collect the default value to present to the end user
    """
    request = kw['request']
    context = request.context
    if isinstance(context, Project):
        project = context
    elif hasattr(context, "project"):
        project = context.project
    elif context.__name__ == 'company':
        project = None
    else:
        raise KeyError(
            "No project could be found starting from current : %s" % (
                context,
            )
        )

    if project and project.project_type.default_business_type:
        return project.project_type.default_business_type.id
    else:
        return get_business_types_from_request(request)[0].id


class NewTaskSchema(colander.Schema):
    """
    schema used to initialize a new task
    """
    name = colander.SchemaNode(
        colander.String(),
        title="Nom du document",
        description="Ce nom n'apparaît pas dans le document final",
        validator=colander.Length(max=255),
        default=deferred_default_name,
    )
    customer_id = customer_choice_node_factory(
        default=deferred_default_customer,
        query_func=_get_customers_options,
    )
    project_id = colander.SchemaNode(
        colander.Integer(),
        title="Dossier dans lequel insérer le document",
        widget=deferred_project_widget,
        default=deferred_default_project
    )
    phase_id = colander.SchemaNode(
        colander.Integer(),
        title="Sous-dossier dans lequel insérer le document",
        widget=deferred_phases_widget,
        default=deferred_default_phase,
        missing=colander.drop,
    )
    business_type_id = colander.SchemaNode(
        colander.Integer(),
        title="Type d'affaire",
        widget=deferred_business_type_widget,
        default=deferred_business_type_default,
    )


@colander.deferred
def deferred_customer_project_widget(node, kw):
    request = kw['request']
    projects = request.context.customer.projects
    logger.debug("### Getting the projects")
    choices = _get_project_choices(projects)
    wid = deform.widget.SelectWidget(values=choices)
    return wid


def validate_customer_project_phase(form, value):
    """
    Validate that customer project and phase are linked

    :param obj form: The form object
    :param dict value: The submitted values
    """
    customer_id = value['customer_id']
    project_id = value['project_id']
    phase_id = value.get('phase_id')

    if phase_id and not Project.check_phase_id(project_id, phase_id):
        exc = colander.Invalid(
            form,
            "Sous-dossier et dossier ne correspondent pas"
        )
        exc['phase_id'] = "Ne correspond pas au dossier ci-dessus"
        raise exc
    if not Customer.check_project_id(customer_id, project_id):
        exc = colander.Invalid(form, "Client et dossier ne correspondent pas")
        exc['project_id'] = "Ne correspond pas au client ci-dessus"
        raise exc


def get_new_task_schema():
    """
    Return the schema for adding tasks

    :returns: The schema
    """
    return NewTaskSchema(validator=validate_customer_project_phase)


def get_same_year_validator(context):
    year = context.date.year

    def validator(node, value):
        if value.year != year:
            raise colander.Invalid(
                node,
                "Merci de choisir une date dans l'année {}".format(year)
            )
    return validator


def add_date_field_after_bind(schema, kw):
    """
    Add a date edition field if the current user has the appropriate rights
    """
    req = kw['request']
    invoice_template = Config.get_value('invoice_number_template', None)
    ctx_type = req.context.type_
    if req.has_permission('set_date.%s' % ctx_type):
        if 'SEQGLOBAL' in invoice_template:
            validator = None
        elif 'SEQYEAR' in invoice_template:
            validator = get_same_year_validator(req.context)
        else:
            return

        schema.add(
            today_node(
                default=req.context.date,
                name='date',
                validator=validator,
            )
        )


def change_metadatas_fields_after_bind(schema, kw):
    """
    Alter metadatas fields after binding the metadatas modification schema
    """
    add_date_field_after_bind(schema, kw)
    context = kw['request'].context
    clean = False
    if context.type_ == 'estimation':
        if context.business and not context.business.is_void():
            clean = True
    else:
        if context.type_ == 'invoice':
            if context.invoicing_mode == context.PROGRESS_MODE:
                clean = True

    if clean:
        del schema['customer_id']
        del schema['project_id']
        del schema['phase_id']


def get_task_metadatas_edit_schema():
    """
    Return the schema for editing tasks metadatas

    :returns: The schema
    """
    schema = NewTaskSchema().clone()
    schema['customer_id'].widget = deform.widget.HiddenWidget()
    schema['project_id'].widget = deferred_customer_project_widget
    schema['project_id'].title = "Dossier vers lequel déplacer ce document"
    schema['phase_id'].title = "Sous-dossier dans lequel déplacer ce document"
    del schema['business_type_id']
    schema.after_bind = change_metadatas_fields_after_bind
    return schema


class DuplicateSchema(NewTaskSchema):
    """
    schema used to duplicate a task
    """
    customer_id = customer_choice_node_factory(
        query_func=_get_customers_options,
        default=deferred_default_customer,
    )


def get_duplicate_schema():
    """
    Return the schema for task duplication

    :returns: The schema
    """
    return DuplicateSchema(validator=validate_customer_project_phase)


def get_task_from_context(context):
    """
    Return the current task from the given context

    :param obj context: Instance of Task/TaskLine/TaskLineGroup
    :returns: The associated Task object
    """
    result = context
    if isinstance(context, (TaskLine, TaskLineGroup)):
        result = context.task
    return result


def remove_childnode(node, child_node_name):
    """
    Remove a schema childnode
    """
    if child_node_name in node:
        del node[child_node_name]


def has_set_treasury_perm(kw):
    """
    Return True if the current user has set_treasury perms on the current
    context
    :param dict kw: The bind keyword arguments
    """
    result = True
    request = kw.get('request')
    if request is None:
        result = False
    else:
        task = get_task_from_context(request.context)
        if task is None:
            result = False
        else:
            perm = 'set_treasury.%s' % task.type_
            result = request.has_permission(perm)

    return result


def task_after_bind(node, kw):
    """
    After bind methods passed to the task schema

    Remove fields not allowed for edition

    :param obj node: The SchemaNode concerning the Task
    :param dict kw: The bind arguments
    """
    if not has_set_treasury_perm(kw):
        remove_childnode(node, 'financial_year')
