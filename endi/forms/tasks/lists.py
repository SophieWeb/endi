import colander
from endi.forms.custom_types import AmountType


class PeriodSchema(colander.MappingSchema):
    """
        A form used to select a period
    """
    start = colander.SchemaNode(
        colander.Date(),
        title="Émis(e) entre le",
        description="",
        missing=colander.drop,
    )
    end = colander.SchemaNode(
        colander.Date(),
        title="et le",
        description="",
        missing=colander.drop,
    )


class AmountRangeSchema(colander.MappingSchema):
    """
    Used to filter on a range of amount
    """
    start = colander.SchemaNode(
        AmountType(5),
        title="TTC entre",
        missing=colander.drop,
        description="",
    )
    end = colander.SchemaNode(
        AmountType(5),
        title="et",
        missing=colander.drop,
        description="",
    )
