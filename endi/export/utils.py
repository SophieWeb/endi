"""
Export utilities:

    * Tools to build file responses (pdf, xls ...)
"""
import os
import re
import unicodedata
import mimetypes

from endi_base.utils.ascii import (
    force_ascii,
)


def detect_file_mimetype(filename):
    """
        Return the headers adapted to the given filename
    """
    mimetype = mimetypes.guess_type(filename)[0] or "text/plain"
    return mimetype


def write_headers(request, filename, mimetype, encoding=None):
    """
        Write the given headers to the current request
    """
    # Here enforce ascii chars and string object as content type
    mimetype = force_ascii(mimetype)
    request.response.content_type = str(mimetype)
    request.response.charset = encoding
    request.response.headerlist.append(
            ('Content-Disposition',
             'attachment; filename="{0}"'.format(force_ascii(filename))))
    return request


def get_buffer_value(filebuffer):
    """
    Return the content of the given filebuffer, handles the different
    interfaces between opened files and BytesIO containers
    """
    if hasattr(filebuffer, 'getvalue'):
        return filebuffer.getvalue()
    elif hasattr(filebuffer, 'read'):
        return filebuffer.read()
    else:
        raise Exception("Unknown file buffer type")


def ensure_encoding_bridge(filedata, encoding):
    """
    Ensure, if the encoding is not utf-8, that the returned data will not raise
    an encoding error (if the filedata is provided as a string)

    :param filedata: The data we return to the end user (str or bytes)
    :param str encoding: The name of the destination encoding

    :rtype: bytes
    """
    if (
            not isinstance(filedata, bytes)
            and encoding.lower() != 'utf-8'
            and hasattr(filedata, 'encode')
    ):
        # replace remplace les caractères non traités par des ?
        result = filedata.encode(encoding, 'replace')
    else:
        result = filedata
    return result


def write_file_to_request(
    request, filename, buf, mimetype=None, encoding='UTF-8'
):
    """
        Write a buffer as request content
        :param request: Pyramid's request object
        :param filename: The destination filename
        :param buf: The file buffer mostly BytesIO object, should provide a
            getvalue method
        :param mimetype: file mimetype, defaults to autodetection
    """
    # Ref #384 : 'text/plain' is the default stored in the db
    request.response.charset = 'UTF-8'
    if mimetype is None or mimetype == 'text/plain':
        mimetype = detect_file_mimetype(filename)
    request = write_headers(request, filename, mimetype, encoding)

    value = get_buffer_value(buf)
    value = ensure_encoding_bridge(value, encoding)
    request.response.write(value)
    return request


def slugify(value, allow_unicode=False):
    """
    Taken from
    https://github.com/django/django/blob/master/django/utils/text.py
    Convert to ASCII if 'allow_unicode' is False. Convert spaces or repeated
    dashes to single dashes. Remove characters that aren't alphanumerics,
    underscores, or hyphens. Convert to lowercase. Also strip leading and
    trailing whitespace, dashes, and underscores.
    """
    value = str(value)
    value = unicodedata.normalize('NFKC', value)

    if allow_unicode:
        value = value.encode('ascii', 'ignore').decode('ascii')

    value = re.sub(r'[^\w\s-]', '', value.lower())
    return re.sub(r'[-\s]+', '-', value).strip('-_')


def format_filename(filename):
    """
    Format filename to avoid illegal characters

    :rtype: str
    """
    basename, ext = os.path.splitext(filename)
    basename = slugify(basename)
    return "{}{}".format(basename, ext)
