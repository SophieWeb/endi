import logging
import openpyxl
import io
from zope.interface import (
    implementer,
)
from openpyxl.styles import (
    Color,
    fills,
    NamedStyle,
    PatternFill,
    Font,
)
from openpyxl.styles.numbers import (
    FORMAT_NUMBER_00
)
from endi.interfaces import IExporter
from endi.export.utils import write_file_to_request

logger = logging.getLogger(__name__)

Color.LightCyan = "FFE0FFFF"
Color.LightCoral = "FFF08080"
Color.LightGreen = "FF90EE90"
Color.Crimson = "FFDC143C"
Color.header = "FFD9EDF7"
Color.footer = "FFFCF8E3"
Color.highlight = "FFEFFFEF"
EXCEL_NUMBER_FORMAT = '0.00'

TITLE_STYLE = NamedStyle(font=Font(size=16, bold=True), name='endi-title')
HEADER_STYLE = NamedStyle(
    font=Font(bold=True),
    fill=PatternFill(
        fill_type=fills.FILL_SOLID,
        start_color=Color(rgb=Color.header)
    ),
    name='endi-header'
)
BOLD_CELL = NamedStyle(
    font=Font(bold=True),
    name='endi-bold-cell'
)
NUMBER_CELL = NamedStyle(
    number_format=FORMAT_NUMBER_00,
    name='endi-number-cell'
)
HIGHLIGHTED_ROW_STYLE = NamedStyle(
    font=Font(bold=True),
    fill=PatternFill(
        fill_type=fills.FILL_SOLID,
        start_color=Color(rgb=Color.highlight)
    ),
    name='endi-highlight-cell'
)

FOOTER_CELL = NamedStyle(
    font=Font(bold=True),
    fill=PatternFill(
        fill_type=fills.FILL_SOLID,
        start_color=Color(rgb=Color.footer)
    ),
    number_format=FORMAT_NUMBER_00,
    name='endi-footer-cell',
)
LARGE_FOOTER_CELL = NamedStyle(
    font=Font(bold=True, size=16),
    fill=PatternFill(
        fill_type=fills.FILL_SOLID,
        start_color=Color(rgb=Color.footer)
    ),
    number_format=FORMAT_NUMBER_00,
    name='endi-large-footer-cell'
)


@implementer(IExporter)
class XlsExporter:
    title = "Export"

    def __init__(self, worksheet=None, **kw):
        if worksheet is None:
            self.book = openpyxl.workbook.Workbook()
            self.worksheet = self.book.active
            self.worksheet.title = self.title
        else:
            self.worksheet = worksheet
            self.book = worksheet.parent
        self.options = kw
        self.current_line = 1

    def add_title(self, title, width):
        self.worksheet.merge_cells(
            start_row=self.current_line,
            end_row=self.current_line,
            start_column=1,
            end_column=width - 1,
        )
        cell = self.worksheet.cell(row=self.current_line, column=1)
        cell.value = title
        cell.style = TITLE_STYLE
        print(self.worksheet.row_dimensions)
        row_dim = self.worksheet.row_dimensions[self.current_line]
        row_dim.height = 20
        self.current_line += 1

    def add_breakline(self):
        self.current_line += 1

    def _add_row(self, labels, styles=None):
        for col_index, label in enumerate(labels):
            cell = self.worksheet.cell(
                row=self.current_line, column=col_index + 1
            )
            cell.value = label
            if styles:
                cell.style = styles

    def add_headers(self, labels):
        self._add_row(labels, HEADER_STYLE)
        self.current_line += 1

    def add_row(self, labels):
        self._add_row(labels)
        self.current_line += 1

    def add_highlighted_row(self, labels):
        self._add_row(labels, HIGHLIGHTED_ROW_STYLE)
        self.current_line += 1

    def save_book(self, f_buf=None):
        """
        Return a file buffer containing the resulting xls

        :param obj f_buf: A file buffer supporting the write and seek
        methods
        """
        if f_buf is None:
            f_buf = io.BytesIO()
        f_buf.write(openpyxl.writer.excel.save_virtual_workbook(self.book))
        f_buf.seek(0)
        return f_buf

    def render(self, f_buf=None):
        """
        Definitely render the workbook

        :param obj f_buf: A file buffer supporting the write and seek
        methods
        """
        if f_buf is None:
            f_buf = io.BytesIO()

        return self.save_book(f_buf)


def make_excel_view(filename_builder, factory):
    """
        Build an excel view of a model
        :param filename_builder: a callable that take the request as arg and
            return a filename
        :param factory: the Xls factory that should be used to wrap the
            request context the factory should provide a render method
            returning a file like object
    """
    def _view(request):
        """
            the dynamically built view object
        """
        filename = filename_builder(request)
        result = factory(request.context).render()
        request = write_file_to_request(request, filename, result)
        return request.response
    return _view
