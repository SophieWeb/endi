"""
Weasyprint pdf task panel
"""
import logging
from sqla_inspect.py3o import SqlaContext
from endi.models.task import (
    Invoice,
    Estimation,
    CancelInvoice,
)
from endi.models.company import Company


logger = logging.getLogger(__name__)
CompanySerializer = SqlaContext(Company)


def pdf_header_panel(context, request):
    """
    Panel for task pdf file header
    Only shown once in the rendering (not on all pages)
    """
    result = {
        'company': context.company,
        'has_header': False,
        'config': request.config,
        'task': context
    }
    if context.company.header_file:
        result['has_header'] = True
    return result


def pdf_footer_panel(context, request, **kwargs):
    """
    Panel for task pdf file footer

    Show on all pages
    """
    config = request.config
    result = {
        'title': config.get('coop_pdffootertitle'),
        'text': config.get('coop_pdffootertext'),
        'pdf_current_page': '',
        'pdf_page_count': '',
    }
    if context.is_training():
        result['more_text'] = config.get('coop_pdffootercourse')

    if isinstance(context, Estimation):
        number_label = "Devis {}".format(context.internal_number)
    elif context.status == 'valid':
        number_label = "Facture {}".format(context.official_number)
    else:
        number_label = "Document non numéroté"

    result['number'] = number_label

    return {**result, **kwargs}


def pdf_content_panel(context, request, with_cgv=True):
    """
    Panel generating the main html page for a task pdf's output
    """
    tvas = context.get_tvas()
    # Si on a plusieurs TVA dans le document, cela affecte l'affichage
    multiple_tvas = len([val for val in tvas if val]) > 1

    is_tva_on_margin_mode = context.business_type.tva_on_margin

    # Contexte de templating qui sera utilisé pour les mentions et autres
    # textes configurables avec une notion de templating
    tmpl_context = CompanySerializer.compile_obj(context.company)

    # Calcul des nombres de
    if context.display_units:
        column_count = 5
        first_column_colspan = 4
    else:
        column_count = 2
        first_column_colspan = 1

    if multiple_tvas:
        column_count += 1

    if context.display_ttc:
        column_count += 1

    if context.has_line_dates():
        column_count += 1
        first_column_colspan += 1

    show_progress_invoicing = False
    show_previous_invoice = False
    if isinstance(context, Invoice) and \
            context.invoicing_mode == context.PROGRESS_MODE:
        show_progress_invoicing = True
        column_count += 1
        first_column_colspan += 1

        if context.business.has_previous_invoice(context.id):
            show_previous_invoice = True
            column_count += 1
            first_column_colspan += 1

    return dict(
        task=context,
        groups=context.get_groups(),
        project=context.project,
        company=context.project.company,
        multiple_tvas=multiple_tvas,
        tvas=tvas,
        config=request.config,
        mention_tmpl_context=tmpl_context,
        first_column_colspan=first_column_colspan,
        column_count=column_count,
        show_progress_invoicing=show_progress_invoicing,
        show_previous_invoice=show_previous_invoice,
        with_cgv=with_cgv,
        is_tva_on_margin_mode=is_tva_on_margin_mode,
    )


def pdf_task_line_group_panel(
    context,
    request,
    group,
    display_tvas_column,
    column_count,
    first_column_colspan,
    show_progress_invoicing,
    show_previous_invoice,
    is_tva_on_margin_mode,
):
    """
    A panel representing a TaskLineGroup
    """
    display_subtotal = False
    if len(context.get_groups()) > 1:
        display_subtotal = True

    return dict(
        task=context,
        group=group,
        display_subtotal=display_subtotal,
        display_units=context.display_units,
        display_date_column=context.has_line_dates(),
        display_tvas_column=display_tvas_column,
        display_ttc=context.display_ttc,
        column_count=column_count,
        first_column_colspan=first_column_colspan,
        show_progress_invoicing=show_progress_invoicing,
        show_previous_invoice=show_previous_invoice,
        is_tva_on_margin_mode=is_tva_on_margin_mode,
    )


def pdf_task_line_panel(
    context,
    request,
    line,
    display_tvas_column,
    display_date_column,
    column_count,
    first_column_colspan,
    show_previous_invoice,
    show_progress_invoicing,
    is_tva_on_margin_mode,
):
    """
    A panel representing a single TaskLine

    :param obj context: The current task to be rendered
    :param obj line: A taskline
    """
    percentage = None
    invoiced_percentage = None
    unit_ht = None
    quantity = None
    if show_progress_invoicing:
        from endi.models.progress_invoicing import ProgressInvoicingLine
        progress_line = ProgressInvoicingLine.query().filter_by(
            task_line_id=line.id
        ).first()
        percentage = progress_line.percentage
        if percentage is None:
            percentage = 0
        left_percentage = progress_line.status.percent_left
        invoiced_percentage = 100 - left_percentage - percentage

    if context.display_units:
        quantity = line.quantity
        if is_tva_on_margin_mode:
            unit_ht = line.cost * 1.2
        elif context.mode == 'ttc' and context.display_ttc:
            unit_ht = line.cost
        elif show_progress_invoicing:
            quantity = progress_line.status.source_task_line.quantity
            unit_ht = progress_line.status.source_task_line.unit_ht()
        else:
            unit_ht = line.unit_ht()

    return dict(
        task=context,
        line=line,
        display_units=context.display_units,
        display_date_column=display_date_column,
        display_tvas_column=display_tvas_column,
        display_ttc=context.display_ttc,
        column_count=column_count,
        first_column_colspan=first_column_colspan,
        show_progress_invoicing=show_progress_invoicing,
        progress_invoicing_percentage=percentage,
        show_previous_invoice=show_previous_invoice,
        progress_invoicing_invoiced_percentage=invoiced_percentage,
        is_tva_on_margin_mode=is_tva_on_margin_mode,
        unit_ht=unit_ht,
        quantity=quantity,
    )


def pdf_cgv_panel(context, request):
    """
    Panel used to render cgv
    """
    cae_cgv = request.config.get('coop_cgv')
    company_cgv = context.company.cgv
    return dict(
        cae_cgv=cae_cgv,
        company_cgv=company_cgv
    )


def pdf_content_wrapper_panel(context, request):
    """
    Used to wrap the content inside an html page structure
    """
    return dict(task=context)


def includeme(config):
    config.add_panel(
        pdf_header_panel,
        'task_pdf_header',
        renderer="panels/task/pdf/header.mako"
    )
    config.add_panel(
        pdf_footer_panel,
        'task_pdf_footer',
        renderer="panels/task/pdf/footer.mako"
    )
    for document_type in (Estimation, Invoice, CancelInvoice):
        panel_name = "task_pdf_content"
        template = "panels/task/pdf/{0}_content.mako".format(
            document_type.__tablename__
        )
        config.add_panel(
            pdf_content_panel,
            panel_name,
            context=document_type,
            renderer=template
        )

    config.add_panel(
        pdf_content_wrapper_panel,
        'task_pdf_content',
        renderer="panels/task/pdf/content_wrapper.mako"
    )
    config.add_panel(
        pdf_task_line_group_panel,
        'task_pdf_task_line_group',
        renderer="panels/task/pdf/task_line_group.mako",
    )
    config.add_panel(
        pdf_task_line_panel,
        'task_pdf_task_line',
        renderer="panels/task/pdf/task_line.mako",
    )
    config.add_panel(
        pdf_cgv_panel,
        'task_pdf_cgv',
        renderer="panels/task/pdf/cgv.mako",
    )
