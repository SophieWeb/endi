def includeme(config):
    config.include('.activities')
    config.include('.expenses')
    config.include('.suppliers')
    config.include('.tasks')
