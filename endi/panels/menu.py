"""
    Panels for the top main menus

    A common user has his company menu with customers, projects ...
    A manager or an admin has an admin menu and eventually a usermenu if he's
    consulting a company's account

    each user has his own menu with preferences, logout, holidays declaration
"""
import logging
from sqlalchemy import or_
from webhelpers2.html import tags
from webhelpers2.html import HTML
from endi.models.company import Company
from endi.models.services.user import UserPrefsService
from endi.utils.menu import HtmlAppMenuItem
from endi.views.export.routes import BPF_EXPORT_ODS_URL
from endi.views.sale_product.routes import CATALOG_ROUTE

logger = logging.getLogger(__name__)


def get_current_company(request, submenu=False, is_user_company=True):
    """Extract the current company from the request

    - If already retrieved -> request.current_company
    - If company-context request → request.context
    - If non-admin and single-company → that company
    - If non-admin and multi-company and not a company-context request:
       → latest used company

    :param obj request: the pyramid request
    :param bool submenu: Do we ask this for the submenu ?
    """
    if request.dbsession.query(Company.id).count() == 1:
        return request.dbsession.query(Company).first()
    # Try to get the current company object from cache
    company = getattr(request, "current_company", None)
    if company is None:
        # Pas un manager et une seule enseigne
        if len(request.user.active_companies) == 1 and not submenu:
            company = request.user.active_companies[0]
        # The current context provide a get_company_id utility that allows to
        # retrieve the concerned company
        elif hasattr(request, "context"):
            if isinstance(request.context, Company):
                company = request.context
            else:
                cid = None
                if hasattr(request.context, 'company_id'):
                    cid = request.context.company_id
                elif hasattr(request.context, "get_company_id"):
                    cid = request.context.get_company_id()
                if cid is not None:  # case Workshops interne CAE
                    company = Company.get(cid)
        # Usefull for non-admin having several companies:
        # display the menu even if current context is not company related
        if not submenu:
            if company is not None:
                UserPrefsService.set(request, 'last_used_company', company.id)
            else:
                cid = UserPrefsService.get(request, 'last_used_company')
                if cid is not None:  # Prevent empty last_used_company
                    company = Company.get(cid)
            # fallback on a default one : first company
            if company is None and len(request.user.active_companies):
                cid = request.user.active_companies[0].id
                company = Company.get(cid)
        # Place the current company in cache (only for the request's lifecycle)
        request.current_company = company
    return company


def get_companies(request, company=None):
    """
    Retrieve the companies the current user has access to

    :param obj request: The current pyramid request
    :param obj company: The current company
    :returns: The list of companies
    :rtype: list
    """
    companies = []
    if request.has_permission('manage'):
        if company is not None:
            companies = Company.label_query().filter(
                or_(
                    Company.active == True,  # noqa: E712
                    Company.id == company.id
                )
            ).all()
        else:
            companies = Company.label_query().filter(
                Company.active == True
            ).all()
    else:
        companies = request.user.active_companies
    return companies


def get_company_menu(
    request, company, css=None, submenu=True, is_user_company=True
):
    """
    Build the Company related menu
    """
    menu_builder = request.registry.company_menu
    menu = menu_builder.build(
        request,
        context=company,
        user_id=request.user.id,
        company_id=company.id,
        submenu=submenu,
        is_user_company=is_user_company,
        company=company
    )
    menu['css'] = css
    return menu


def get_admin_menus(request):
    """
        Build the admin menu
    """
    menu_builder = request.registry.admin_menu
    menu = menu_builder.build(request, user_id=request.user.id)
    return menu


def company_choice(request, companies, current_company=None):
    """
        Add the company choose menu
    """
    options = tags.Options()
    options.add_option("Sélectionner une enseigne...", '/')
    for company in companies:
        if request.context.__name__ == 'company':
            url = request.current_route_path(id=company.id)
        else:
            url = request.route_path("company", id=company.id)
        name = company.name
        if not company.active:
            name += " (désactivée)"
        options.add_option(name, url)
    if current_company is not None:
        if request.context.__name__ == 'company':
            default = request.current_route_path(id=current_company.id)
        else:
            default = request.route_path("company", id=current_company.id)
    else:
        default = request.current_route_path()
    html_attrs = {
        'class': 'company-search',
        'id': "company-select-menu",
        'accesskey': 'E',
    }
    html_code = HTML.li(
        tags.select("companies", default, options, **html_attrs)
    )
    return HtmlAppMenuItem(html=html_code).build()


def get_usermenu(request):
    """
        Return the user menu (My account, holidays ...)
    """
    menu_builder = request.registry.user_menu
    return menu_builder.build(request, user_id=request.user.id)


def menu_panel(context, request):
    """
    Top menu panel

    Build the top menu dict representation

    :rtype: dict
    """
    # If we've no user in the current request, we don't return anything
    if not getattr(request, 'user'):
        return {}

    menu = None
    if request.has_permission('manage'):
        menu = get_admin_menus(request)
    else:
        current_company = get_current_company(request)
        if current_company:
            menu = get_company_menu(request, current_company, submenu=False)
            companies = get_companies(request, current_company)
            # If there is more than 1 company accessible for the current user,
            # we provide a usefull dropdown menu
            if len(companies) > 1:
                menu['items'].insert(
                    0, company_choice(request, companies, current_company)
                )

    usermenu = get_usermenu(request)

    return {
        'menu': menu,
        'usermenu': usermenu,
    }


def submenu_panel(context, request):
    """
    Submenu panel, build a dict representation of the submenu, if one is
    expected

    :rtype: dict
    """
    # If we've no user in the current request, we don't return anything
    if not getattr(request, 'user'):
        return {}

    # There are no submenus for non admins
    if not request.has_permission('manage'):
        return {}

    current_company = get_current_company(request, submenu=True)
    if not current_company:
        submenu = {
            "items": [
                company_choice(request, get_companies(request))
            ]
        }
        return {"submenu": submenu}

    is_user_company = current_company.employs(request.user.id)
    submenu = get_company_menu(
        request,
        current_company,
        css="nav-pills",
        is_user_company=is_user_company,
    )
    if submenu:
        companies = get_companies(request, current_company)
        # If there is more than 1 company accessible for the current user,
        # we provide a usefull dropdown menu
        if len(companies) > 1:
            submenu['items'].insert(
                0,
                company_choice(request, companies, current_company)
            )
    return {"submenu": submenu}


def includeme(config):
    """
        Pyramid's inclusion mechanism
    """
    config.add_panel(
        menu_panel,
        'menu',
        renderer='/panels/menu.mako',
    )
    config.add_panel(
        submenu_panel,
        'submenu',
        renderer='/panels/menu.mako',
    )
