from typing import Iterable

from endi.models.status import StatusLogEntry


def status_history_panel(
        context,
        request,
        entries: Iterable[StatusLogEntry],
        genre='',
):
    return dict(entries=entries, genre=genre)

def includeme(config):
    config.add_panel(
        status_history_panel,
        "status_history",
        renderer="endi:templates/panels/status/status_history.mako",
    )
