from zope.interface import Interface
from zope.interface import Attribute


class IValidationStatusHolderService(Interface):
    """
    Interface for class that will produce collections of
    ValidationStatusHolderMixin implementors.
    """
    def waiting(self, *classes):
        """
        :return: iterable of instances implementing ValidationStatusHolderMixin
        """
        pass


class ITreasuryProducer(Interface):
    """
    Interface for accounting operation production
    """

    def get_item_entries(self, element):
        pass

    def get_book_entries(self, elements):
        pass


class ITreasurySupplierInvoiceWriter(Interface):
    """
    Interface for the module handling the generation of the tabular export file
    """
    def set_datas(self, lines):
        """
        Set the tabular datas that will be written in the output file
        """
        pass


class ITreasuryInvoiceWriter(Interface):
    """
    Interface for the module handling the generation of the tabular export file
    """
    def set_datas(self, lines):
        """
        Set the tabular datas that will be written in the output file
        """
        pass


class ITreasuryExpenseWriter(Interface):
    """
    Interface for the module handling the generation of the tabular export file
    """
    def set_datas(self, lines):
        """
        Set the tabular datas that will be written in the output file
        """
        pass


class ITreasuryPaymentWriter(Interface):
    """
    Interface for the module handling the generation of the tabular export file
    """
    def set_datas(self, lines):
        """
        Set the tabular datas that will be written in the output file
        """
        pass


class ITreasurySupplierPaymentWriter(Interface):
    """
    Interface for the module handling the generation of the tabular export file
    """
    def set_datas(self, lines):
        """
        Set the tabular datas that will be written in the output file
        """
        pass


class IFileRequirementService(Interface):
    """
    Describe the way a File Requirement service should work
    """
    def populate(parent_object):
        """
        Populate the parent_object with File Requirements
        """
        pass

    def register(sale_node, file_object):
        """
        Register the file_object against the associated indicators
        """
        pass


class IMoneyTask(Interface):
    """
        Interface for task handling money
    """
    def lines_total_ht():
        """
            Return the sum of the document lines
        """

    def total_ht():
        """
            return the HT total of the document
        """

    def discount_total_ht():
        """
            Return the HT discount
        """

    def get_tvas():
        """
            Return a dict with the tva amounts stored by tva reference
        """

    def tva_amount():
        """
            Return the amount of Tva to be paid
        """

    def total_ttc():
        """
            compute the ttc value before expenses
        """

    def total():
        """
            compute the total to be paid
        """

    def expenses_amount():
        """
            return the TTC expenses
        """


class IInvoice(Interface):
    """
        Invoice interface (used to get an uniform invoice list display
        See templates/invoices.mako (under invoice.model) to see the expected
        common informations
    """
    official_number = Attribute("""official number used in sage""")

    def total_ht():
        """
            Return the HT total of the current document
        """

    def tva_amount():
        """
            Return the sum of the tvas
        """

    def total():
        """
            Return the TTC total
        """

    def get_company():
        """
            Return the company this task is related to
        """

    def get_customer():
        """
            Return the customer this document is related to
        """


class IMailEventWrapper(Interface):
    """
    describe the datas expected by the send_mail_from_event tool
    """
    request = Attribute("""The Pyramid request""")
    sendermail = Attribute("""The sender's email address""")
    recipients = Attribute("""List of mail recipients""")
    subject = Attribute("""Subject of the e-mail""")
    body = Attribute("""Body of the e-mail""")

    def is_key_event():
        """
        Check if the associated event should fire a mail emission
        """

    def get_attachment():
        """
        Return a mail attachment or None
        """


class IExporter(Interface):
    def add_title(self, title, width, options=None):
        """
        Add a title to the spreadsheet

        :param str title: The title to display
        :param int width: On how many cells should the title be merged
        :param dict options: Options used to format the cells
        """
        pass

    def add_headers(self, headers):
        """
        Add a header line to the file

        :param list headers: List of header dicts
        """
        pass

    def add_row(self, row_datas, options=None):
        """
        Add a row to the spreadsheet

        :param list row_datas: The datas to display
        :param dict options: Key value options used to format the line
        """
        pass

    def render(self, f_buf=None):
        """
        Render the current spreadsheet to the given file buffer

        :param obj f_buf: File buffer (E.G file('....') or io.BytesIO
        """
        pass


class IPaymentRecordService(Interface):
    def add(self, user, invoice, params):
        """
        Record a new payment instance

        :param obj user: The User asking for recording
        :param obj invoice: The associated invoice object
        :param dict params: params used to generate the payment
        """
        pass

    def update(self, user, payment, params):
        """
        Modify an existing payment

        :param obj user: The User asking for recording
        :param obj invoice: The Payment object
        :param dict params: params used to generate the payment
        """
        pass

    def delete(self, user, payment):
        """
        Delete an existing payment

        :param obj user: The User asking for recording
        :param obj invoice: The Payment object
        """
        pass


class ITaskPdfRenderingService(Interface):
    """
    Service used to render invoice/estimation/cancelinvoice PDF
    including bulk rendering
    """
    def __init__(self, context, request):
        """
        :param obj context: The context that will be rendered
        :param obj request: The current Pyramid request
        """

    def set_task(self, task):
        """
        :param obj task: instance of Task that will replace the current context
        """

    def render_bulk(self, tasks):
        """
        Generates a pdf with the given tasks without the CGV pages

        :param list tasks: List of tasks to render
        :returns: A pdf buffer
        :rtype: :class:`io.BytesIO` instance
        """

    def render(self):
        """
        Generates a pdf output of the current context

        context and request are passed in the __init__ method of the service

        :returns: A pdf buffer
        :rtype: :class:`io.BytesIO` instance
        """

    def filename(self):
        """
        Generates a filename for the PDF output

        :rtype: str
        """


class ITaskPdfStorageService(Interface):
    """
    Service used to persist invoice/estimation/cancelinvoice PDF datas on disk

    Persist the pdf datas if needed :

        When validated
        When rendered if valid and not stored
        When exported to treasury and not stored yet
    """
    def __init__(self, context, request):
        """
        :param obj context: The context that has been rendered
        :param obj request: The current Pyramid request
        """

    def set_task(self, task):
        """
        :param obj task: instance of Task that will replace the current context
        """

    def store_pdf(self, filename, pdf_buffer):
        """
        Store the generate pdf attached to the current context
        Handles all the data integrity stuff

        :param pdf_buffer: The Pdf buffer that should be store in database
        :type pdf_buffer: :class:`io.BytesIO` instance
        """

    def retrieve_pdf(self):
        """
        Retrieve the PDF associated to the current context or None if it's not
        stored yet
        :rtype: :class:`io.BytesIO` instance
        """


class IModuleRegistry(Interface):
    """
    Interface utilisée pour stocker les modules dans le registry pyramid
    """
    pass


class IPluginRegistry(Interface):
    """
    Interface utilisée pour stocker les plugins dans le registry pyramid
    """
    pass
