<div>
    <span class='icon status ${indicator.status}'>
    <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#${status_icon}"></use></svg>
    </span>
    ${indicator.label}
    % if indicator.forced:
        <em>Cet indicateur a été forcé manuellement</em>
    % endif
    % if request.has_permission('force.indicator', indicator):
    &nbsp;
    &nbsp;
    &nbsp;
    <a
        href="${force_url}"
        class='btn icon only negative'
        % if not indicator.forced:
        onclick="return confirm('Êtes-vous sûr de vouloir forcer cet indicateur (il apparaîtra désormais comme valide) ?');"
        title="Forcer cet indicateur"
        aria-label="Forcer cet indicateur"
        % else:
        title="Invalider cet indicateur"
        aria-label="Invalider cet indicateur"
        % endif
        >
            % if not indicator.forced:
           <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#bolt"></use></svg>
            % else:
           <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#redo-alt"></use></svg>
            % endif
    </a>
    % endif
</div>
<div class="content_vertical_padding">
