<div class='header'>
    <h4>Factures & Avoirs</h4>
</div>

<div class="separate_bottom">
    <div class="table_container">
        <table class='hover_table'>
            % if invoices:
            <thead>
                <th scope="col" class="col_status" title="Statut"><span class="screen-reader-text">Statut</span></th>
                <th scope="col" class="col_number">Numéro</th>
                <th scope="col" class="col_text">Nom</th>
                <th scope="col" class="col_text">État</th>
                <th scope="col" class="col_text">Fichiers attachés</th>
                <th scope="col" class="col_actions" title="Actions"><span class="screen-reader-text">Actions</span></th>
            </thead>
            % endif
            <tbody>
                % if invoices:
                    % for invoice in invoices:
                        <% view_url = api.task_url(invoice) %>
                        <tr>
                            <td class="col_status" title="${api.format_status(invoice)}">
                                <span class="icon status ${invoice.global_status}">
                                    <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#${api.status_icon(invoice)}"></use></svg>
                                </span>
                            </td>
                            <td class='col_number' onclick="document.location='${view_url}'">${invoice.official_number}</td>
                            <td class='col_text' onclick="document.location='${view_url}'">${invoice.name}</td>
                            <td class='col_text' onclick="document.location='${view_url}'">${api.format_status(invoice)}</td>
                            <td class="col_text">
                                % if getattr(invoice, 'estimation', None) or getattr(invoice, 'cancelinvoices', None) or getattr(invoice, 'invoice', None):
                                    <ul>
                                        % if getattr(invoice, 'estimation', None):
                                            <li>
                                                Devis ${invoice.estimation.name}
                                                % if invoice.estimation.official_number:
                                                    &nbsp;(${invoice.estimation.official_number})
                                                % endif
                                            </li>
                                        % endif
                                        % if getattr(invoice, 'cancelinvoices', None):
                                            % for cancelinvoice in invoice.cancelinvoices:
                                                <li>
                                                    Avoir ${cancelinvoice.name}
                                                    % if cancelinvoice.official_number:
                                                        &nbsp;(${cancelinvoice.official_number})
                                                    % endif
                                                </li>
                                            % endfor
                                        % endif
                                        % if getattr(invoice, 'invoice', None):
                                            <li>
                                                Facture ${invoice.invoice.name}
                                                % if invoice.invoice.official_number:
                                                    &nbsp;(${invoice.invoice.official_number})
                                                % endif
                                            </li>
                                        % endif
                                    </ul>
                                % endif
                            </td>
                            <td class='col_actions width_one'>
                                ${request.layout_manager.render_panel('menu_dropdown', label="Actions", links=stream_actions(request, invoice))}
                            </td>
                        </tr>
                    % endfor
                %else:
                    <tr>
                        <td colspan="6" class="col_text"><em>Aucune facture n'a été créée</em></td>
                    </tr>
                % endif
            </tbody>
            % if api.has_permission('add.invoice'):
                <tfoot>
                    <td colspan="6" class='col_actions'>
                        <a class='btn' href="${add_url}">
                            <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#plus"></use></svg> Créer une facture
                        </a>
                    </td>
                </tfoot>
            % endif
        </table>
    </div>
</div>
