<div class='header'>
    <h4>Devis</h4>
</div>

<div class="separate_bottom">
    <div class="table_container">
        <table class='hover_table'>
            % if estimations:
            <thead>
                <th scope="col" class="col_status" title="Statut"><span class="screen-reader-text">Statut</span></th>
                <th scope="col" class="col_text">Nom</th>
                <th scope="col" class="col_text">État</th>
                <th scope="col" class="col_text">Fichiers attachés</th>
                <th scope="col" class="col_actions" title="Actions"><span class="screen-reader-text">Actions</span></th>
            </thead>
            % endif
           <tbody>
                % if estimations:
                    % for estimation in estimations:
                        <% view_url = request.route_path('/estimations/{id}', id=estimation.id) %>
                        <tr>
                            <td class="col_status" title="${api.format_estimation_status(estimation)}">
                                <span class="icon status ${estimation.global_status}">
                                    <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#${api.status_icon(estimation)}"></use></svg>
                                </span>
                            </td>
                            <td class='col_text' onclick="document.location='${view_url}'">${estimation.name}</td>
                            <td class='col_text'>${api.format_status(estimation)}</td>
                            <td class='col_text'>
                                % if estimation.invoices:
                                    <ul>
                                    % for invoice in estimation.invoices:
                                        <li>
                                            Facture ${invoice.name}
                                            % if invoice.official_number:
                                                &nbsp;(${invoice.official_number})
                                            % endif
                                        </li>
                                    % endfor
                                    </ul>
                                % endif
                            </td>
                            <td class='col_actions width_one'>
                                ${request.layout_manager.render_panel('menu_dropdown', label="Actions", links=stream_actions(request, estimation))}
                            </td>
                        </tr>
                    % endfor
                %else:
                    <tr>
                        <td colspan="5" class="col_text"><em>Aucun devis n’a été créé</em></td>
                    </tr>
                % endif
            </tbody>
            % if api.has_permission('add.estimation'):
                <tfoot>
                    <td colspan="5" class='col_actions'>
                        <a class='btn' href="${add_url}">
                            <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#plus"></use></svg> Créer un devis
                        </a>
                    </td>
                </tfoot>
            % endif
        </table>
    </div>
</div>
