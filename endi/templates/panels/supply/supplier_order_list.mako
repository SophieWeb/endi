<%namespace file="/base/pager.mako" import="sortable"/>

<table class="hover_table">
    <thead>
        <tr>
			<th scope="col" class="col_status col_text"></th>
            % if is_admin_view:
                <th scope="col" class="col_text">${sortable("Enseigne", "company_id")}</th>
            % endif
            <th scope="col" class="col_date">${sortable("Créé le", "created_at")}</th>
            <th scope="col" class="col_text">${sortable("Nom", "name")}</th>
            % if not is_supplier_view:
                <th scope="col" class="col_text">${sortable("Fournisseur", "supplier")}</th>
            % endif
            <th scope="col" class="col_number">HT</th>
            <th scope="col" class="col_number">TVA</th>
            <th scope="col" class="col_number">TTC</th>
            <th scope="col" class="col_number" title="Part réglée par la CAE">${sortable("Part CAE", "cae_percentage")}</th>
            <th scope="col" class="col_text">${sortable("Facture fournisseur", "supplier_invoice")}</th>
            <th scope="col" class="col_actions" title="Actions"><span class="screen-reader-text">Actions</span></th>
        </tr>
    </thead>
    <tbody>
        % if records:
            % for supplier_order in records:
                <tr class='tableelement' id="${supplier_order.id}">
                    <% url = request.route_path("/suppliers_orders/{id}", id=supplier_order.id) %>
                    <% onclick = "document.location='{url}'".format(url=url) %>

					<td class="col_status" onclick="${onclick}" title="${api.format_status(supplier_order)}">
						<span class="icon status ${supplier_order.status}">
                            ${api.icon(api.status_icon(supplier_order))}
						</span>
					</td>
                    % if is_admin_view:
                        <td class="col_text" >
                            <a href="${request.route_path('company', id=supplier_order.company_id)}">
                                ${supplier_order.company.name}
                            </a>
                        </td>
                    % endif
                    <td onclick="${onclick}" class="col_date" >${api.format_date(supplier_order.created_at)}</td>
                    <td onclick="${onclick}" class="col_text" >
                        ${supplier_order.name}
<!-- Si admin et commande auto-validée
						<br>
						<span class="icon tag positive">                        		
							<svg><use href="${request.static_url('endi:static/icons/endi.svg')}#user-check"></use></svg>
							Auto-validé
						</span>
 -->
                    </td>
                    % if not is_supplier_view:
                        <td onclick="${onclick}" class="col_text" >${supplier_order.supplier.label}</td>
                    % endif
                    <td onclick="${onclick}" class="col_number" >
                        ${api.format_amount(supplier_order.total_ht)}&nbsp;€
                    </td>
                    <td onclick="${onclick}" class="col_number" >
                        ${api.format_amount(supplier_order.total_tva)}&nbsp;€
                    </td>
                    <td onclick="${onclick}" class="col_number" >
                        ${api.format_amount(supplier_order.total)}&nbsp;€
                    </td>
                    <td onclick="${onclick}" class="col_number" >
                        ${supplier_order.cae_percentage}&nbsp;%
                    </td>
                    <td class="col_text">
                        % if supplier_order.supplier_invoice:
                            <a href="${request.route_path('/suppliers_invoices/{id}', id=supplier_order.supplier_invoice_id)}">
                                ${supplier_order.supplier_invoice.name}
                                % if supplier_order.supplier_invoice.paid_status == 'resulted':
                                    (soldée)
                                % elif supplier_order.supplier_invoice.status == 'valid':
                                    (validée)
                                % else:
                                    (brouillon)
                                % endif
                            </a>
                        % else:
                            aucune
                        % endif
                    </td>
                    ${request.layout_manager.render_panel('action_buttons_td', links=stream_actions(supplier_order))}
                </tr>
            % endfor
        % else:
            <tr>
                <td class="col_text"
                    % if is_admin_view:
                    colspan='11'
                    % else:
                    colspan='10'
                    % endif
                >
                    <em>
                        % if is_search_filter_active:
                            Aucune commande fournisseur correspondant à ces critères.
                        % else:
                            Aucune commande fournisseur pour l'instant.
                        % endif
                    </em>
                </td>
            </tr>
        % endif
    </tbody>
</table>
