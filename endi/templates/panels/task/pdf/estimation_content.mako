<%doc>
    estimation panel template
</%doc>
<%inherit file="/panels/task/pdf/content.mako" />
<%namespace file="/base/utils.mako" import="format_text" />

<%def name="table(title, datas, css='')">
	<div class='pdf_mention_block'>
		<h4 class="title ${css}">${title}</h4>
		<p class='content'>${format_text(datas)}</p>
    </div>
</%def>

<%block name='information'>
<div class="pdf_information">
	<div class="info_cols">
		<div class="document_info">
			<h3>${api.overridable_label('estimation', task)} N<span class="screen-reader-text">umér</span><sup>o</sup> <strong>${task.internal_number}</strong></h3>
        </div>
        % if task.customer.tva_intracomm:
        <div class="customer_info">
            <strong>Numéro de TVA Intracommunautaire&nbsp;: </strong><br />${task.customer.tva_intracomm}
        </div>
        % endif
	</div>
	<strong>Objet : </strong>${format_text(task.description)}
	% if config.get('coop_estimationheader'):
		<div class="coop_header">${format_text(config['coop_estimationheader'])}</div>
	% endif
</div>
</%block>

<%block name="notes_and_conditions">
<div class="notes_group">
## LIMITE DE VALIDITE DU DEVIS
% if task.validity_duration:
	<div class='pdf_mention_block validity_duration'>
		<p class="content"><strong>Limite de validité du devis :</strong> ${task.validity_duration}</p>
	</div>
% endif
## DATE DE DEBUT DES PRESTATIONS
% if task.start_date:
	<div class='pdf_mention_block start_date'>
		<p class="content"><strong>Date de début des prestations :</strong> le ${api.format_date(task.start_date, False)}</p>
	</div>
% endif
</div>
## LIEU D'EXECUTION
% if task.workplace:
	<div class='pdf_mention_block workplace'>
		<h4>Lieu d'exécution</h4>
		<p class="content">${format_text(task.workplace)}</p>
	</div>
% endif
## CONDITIONS DE PAIEMENT
% if task.paymentDisplay != "NONE":
    % if task.paymentDisplay == "ALL":
        <% colspan = 3 %>
    % elif task.paymentDisplay == "ALL_NO_DATE":
        <% colspan = 2 %>
    %else:
        <% colspan = 1 %>
    % endif
    <div class='pdf_mention_block payment_conditions'>
		<h4>Conditions de paiement</h4>
    	<div>
			<p>
				${task.payment_conditions}<br />
				% if task.deposit > 0 :
					Un acompte, puis paiement en ${task.get_nb_payment_lines()} fois.
				%else:
					Paiement en ${task.get_nb_payment_lines()} fois.
				%endif
			</p>
			<div>
				<table class='payment_schedule'>
					<tbody>
						% if task.paymentDisplay in ("ALL", "ALL_NO_DATE"):
							## AFFICHAGE DU DETAIL DU PAIEMENT
							## L'acompte à la commande
							% if task.deposit > 0 :
								<tr>
									<td
										% if task.paymentDisplay == "ALL":
											colspan=2
										% endif
										scope='row'
										class='col_text'
										>Acompte à la commande</td>
									<td class='col_number price'>${task.format_amount(task.deposit_amount_ttc(), precision=5)}&nbsp;€</td>
									% if multiple_tvas:
										<td class='col_number tva'>&nbsp;</td>
									% endif
									% if task.display_ttc:
										<td class="col_number price">&nbsp;</td>
									% endif
								</tr>
							% endif
							## Les paiements intermédiaires
							% for line in task.payment_lines[:-1]:
								<tr>
									% if task.paymentDisplay == "ALL":
										<td scope='row' class='col_date'>${api.format_date(line.date)}</td>
									% endif
									<td class='col_text'>${line.description}</td>
									%if task.manualDeliverables == 1:
										<td class='col_number price'>${task.format_amount(line.amount, precision=5)}&nbsp;€</td>
									%else:
										<td class='col_number price'>${task.format_amount(task.paymentline_amount_ttc(), precision=5)}&nbsp;€</td>
									%endif
									% if multiple_tvas:
										<td class='col_number tva'>&nbsp;</td>
									% endif
									% if task.display_ttc:
										<td class="col_number price">&nbsp;</td>
									% endif
								</tr>
							% endfor
							## Le solde (qui doit être calculé séparément pour être sûr de tomber juste)
							<tr>
								% if task.paymentDisplay == "ALL":
									<td scope='row' class='col_date'>
										${api.format_date(task.payment_lines[-1].date)}
									</td>
								% endif
								<td scope='row' class='col_text'>
									${format_text(task.payment_lines[-1].description)}
								</td>
								<td class='col_number price'>
									${task.format_amount(task.sold(), precision=5)}&nbsp;€
								</td>
								% if multiple_tvas:
									<td class='col_number tva'>&nbsp;</td>
								% endif
								% if task.display_ttc:
									<td class="col_number price">&nbsp;</td>
								% endif
							</tr>
						% endif
					</tbody>
				</table>
			</div>
    	</div>
    </div>
%else:
    %if task.payment_conditions:
        ${table("Conditions de paiement", task.payment_conditions)}
    % endif
% endif
</%block>

<%block name="end_document">
<div class="pdf_sign_block">
	<div class="pdf_sign_block_width">
		<h4>${api.overridable_label('signed_agreement', task)}</h4>
		<p class='date'>Le :</p>
		<p class="signature"><em>Signature</em></p>
	</div>
</div>
</%block>
