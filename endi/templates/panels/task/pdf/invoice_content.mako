<%doc>
    invoice panel template
</%doc>
<%inherit file="/panels/task/pdf/content.mako" />
<%namespace file="/base/utils.mako" import="format_text" />

<%def name="table(title, datas, css='')">
	<div class='pdf_mention_block'>
		<h4 class="title ${css}">${title}</h4>
		<p class='content'>${format_text(datas)}</p>
    </div>
</%def>

<%block name='information'>
<div class="pdf_information">
	<div class="info_cols">
		<div class="document_info">
			<h3>Facture N<span class="screen-reader-text">umér</span><sup>o</sup> <strong>${task.official_number}</strong></h3>
			% if task.estimation is not None:
				<strong>Facture associée au devis&nbsp;:</strong> ${task.estimation.internal_number}<br />
			% endif
			(${task.internal_number})
        </div>
        % if task.customer.tva_intracomm:
        <div class="customer_info">
            <strong>Numéro de TVA Intracommunautaire&nbsp;: </strong><br />${task.customer.tva_intracomm}
        </div>
        % endif
	</div>
	<strong>Objet : </strong>${format_text(task.description)}<br />
	% if config.get('coop_invoiceheader'):
		<div class="coop_header">${format_text(config['coop_invoiceheader'])}</div>
	% endif
</div>
</%block>

<%block name="notes_and_conditions">
## DATE DE DEBUT DES PRESTATIONS
% if task.start_date:
	<div class='pdf_mention_block start_date'>
		<p class="content"><strong>Date de début des prestations :</strong> le ${api.format_date(task.start_date, False)}</p>
	</div>
% endif
## LIEU D'EXECUTION
% if task.workplace:
	<div class='pdf_mention_block workplace'>
		<h4>Lieu d'exécution</h4>
		<p class="content">${format_text(task.workplace)}</p>
	</div>
% endif
% if show_previous_invoice:
    <div class='pdf_mention_block'>
    <h4 class="title">Factures émises précédemment</h4>
    <p class='content'>
    <table class='payment_schedule'>
    % for invoice in task.business.invoices:
    % if invoice.id != task.id:
        <tr>
            <td scope='row' class='col_date'>${api.format_date(invoice.date)}</td>
            <td class='col_text'>Facture n°${invoice.official_number}</td>
            <td class='col_number price'>${invoice.format_amount(invoice.total(), precision=5)}&nbsp;€</td>
        </tr>
    % endif
    % endfor
    </table>
    </p>
    </div>
% endif
## CONDITIONS DE PAIEMENT
%if task.payment_conditions:
    ${table("Conditions de paiement", task.payment_conditions)}
% endif

</%block>
