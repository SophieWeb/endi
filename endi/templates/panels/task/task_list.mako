<%namespace file="/base/pager.mako" import="sortable"/>
<%namespace file="/base/utils.mako" import="format_text"/>
<%namespace file="/base/utils.mako" import="format_filelist" />

<% num_columns = len(columns) + 1 %>
<table class="top_align_table hover_table">
	% if records:
    <thead>
        % for column in columns:
            <th scope="col">
                % if column.sortable:
                    ${sortable(column.label, column.sort_key)}
                % else:
                    ${column.label | n}
                % endif
            </th>
        % endfor
        <th scope="col" class="col_actions" title="Actions"><span class="screen-reader-text">Actions</span></th>
    </thead>
    % endif
    <tbody>
        % if records:
            <tr class="row_recap">
                <th scope='row' colspan='${num_columns - 6}' class='col_text'>Total</td>
                <td class='col_number'>${api.format_amount(totalht, precision=5)}&nbsp;€</td>
                <td class='col_number'>${api.format_amount(totaltva, precision=5)}&nbsp;€</td>
                <td class='col_number'>${api.format_amount(totalttc, precision=5)}&nbsp;€</td>
                <td colspan='2'></td>
            </tr>
            % for document in records:
                <% id_ = document.id %>
                <% internal_number = document.internal_number %>
                <% name = document.name %>
                <% ht = document.ht %>
                <% tva = document.tva %>
                <% ttc = document.ttc %>
                <% status = document.global_status %>
                <% paid_status = getattr(document, 'paid_status', 'resulted') %>
                <% date = document.date %>
                <% official_number = document.official_number %>
                % if is_admin_view:
                    <% company = document.get_company() %>
                    <% company_id = company.id %>
                    <% company_name = company.name %>
                % endif
                <% customer_id = document.customer.id %>
                <% customer_label = document.customer.label %>
                <% business_type = document.business_type %>

                <tr class='status tolate-${document.is_tolate()} paid-status-${paid_status} status-${document.status}'>
                    <td class="col_status" title="${api.format_status(document)}">
                        <span class="icon status ${status}">
                            <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#${api.status_icon(document)}"></use></svg>
                        </span>
                    </td>
                    <td class="col_text">${official_number}</td>
                    % if is_admin_view:
                        <td class="col_text">${company_name}</td>
                    % endif
                    <td class="col_date">${api.format_date(date)}</td>
                    <td class="col_text">
                        <a href="${api.task_url(document)}" title='Voir le document'>
                           ${name} (<small>${internal_number}</small>)
                        </a>
                        ${request.layout_manager.render_panel('business_type_label', business_type)}
                        % if not is_admin_view:
                            <% description = document.description %>
                            <small class="description">${format_text(description)}</small>
                        % else:
							% if document.auto_validated:
							<span class="icon tag positive">                        		
								<svg><use href="${request.static_url('endi:static/icons/endi.svg')}#user-check"></use></svg>
								Auto-validé
							</span>
							% endif
                       % endif
                    </td>
                    <td class="col_text invoice_company_name">${customer_label}</td>
                    <td class="col_number"><strong>${api.format_amount(ht, precision=5)}&nbsp;€</strong></td>
                    <td class="col_number">${api.format_amount(tva, precision=5)}&nbsp;€</td>
                    <td class="col_number">${api.format_amount(ttc, precision=5)}&nbsp;€</td>
                    <td class="col_text">
                        % if len(document.payments) == 1 and paid_status == 'resulted':
                            <% payment = document.payments[0] %>
                            <% url = request.route_path('payment', id=payment.id) %>
                            <a href="#!" onclick="window.openPopup('${url}')">
                                Le ${api.format_date(payment.date)}
                                <small>(${api.format_paymentmode(payment.mode)})</small>
                            </a>
                        % elif len(document.payments) > 0:
                            <ul>
                                % for payment in document.payments:
                                    <% url = request.route_path('payment', id=payment.id) %>
                                    <li>
                                        <a href="#!" onclick="window.openPopup('${url}')">
                                            ${api.format_amount(payment.amount, precision=5)}&nbsp;€
                                            le ${api.format_date(payment.date)}
                                            <small>(${api.format_paymentmode(payment.mode)})</small>
                                        </a>
                                    </li>
                                % endfor
                            </ul>
                        % endif
                    </td>
                    <td class="col_text">
                        ${format_filelist(document, actions=False)}
                        % if hasattr(document, 'estimation_id') and document.estimation_id is not None:
                            ${format_filelist(document.estimation, actions=False)}
                        % elif hasattr(document, 'invoice_id') and document.invoice_id is not None:
                            ${format_filelist(document.invoice, actions=False)}
                        % endif
                    </td>
                    ${request.layout_manager.render_panel('action_buttons_td', links=stream_actions(document))}
                </tr>
            % endfor
        % else:
            <tr><td class='col_text' colspan='${num_columns}'><em>Aucune facture n’a été créée</em></td></tr>
        % endif
    </tbody>
	% if records:
    <tfoot>
        <tr class="row_recap">
            <th scope='row' colspan='${num_columns - 6}' class='col_text'>Total</td>
            <td class='col_number'>${api.format_amount(totalht, precision=5)}&nbsp;€</td>
            <td class='col_number'>${api.format_amount(totaltva, precision=5)}&nbsp;€</td>
            <td class='col_number'>${api.format_amount(totalttc, precision=5)}&nbsp;€</td>
            <td colspan='2'></td>
        </tr>
    </tfoot>
    % endif
</table>
