<%inherit file="/workshops/workshop_base.mako" />
<%namespace file="/base/utils.mako" name="utils"/>
<%block name='actionmenucontent'>
<div class='layout flex main_actions'>
	<div role='group'>
		<a class='btn btn-primary'
			href='${request.route_path("workshop.pdf", id=request.context.id)}'
			title='Télécharger la feuille d’émargement globale'
			aria-label='Télécharger la feuille d’émargement globale'>
			<svg><use href="${request.static_url('endi:static/icons/endi.svg')}#file-pdf"></use></svg>
			Feuille d’émargement<span class="no_mobile">&nbsp;globale</span>
		</a>
		<button
			class='btn icon_only_mobile'
			data-target='#edition_form'
			onclick='toggleModal("edition_form"); return false;'
			title='Modifier les données relatives à l’atelier'
			aria-label='Modifier les données relatives à l’atelier'>
			<svg><use href="${request.static_url('endi:static/icons/endi.svg')}#pen"></use></svg>
			Modifier
		</button>
        <% duplicate_workshop_url = request.route_path("workshop", id=request.context.id, _query=dict(action="duplicate")) %>
        <%utils:post_action_btn url="${duplicate_workshop_url}" icon="copy"
          _class="btn icon only"
		  title='Dupliquer cet atelier'
		  aria_label='Dupliquer cet atelier'
        >
        </%utils:post_action_btn>

		<a class='btn icon only'
		   href="${request.route_path('workshop', id=request.context.id, _query=dict(action='attach_file'))}"
		   onclick="return confirm('En quittant cette page vous perdrez toute modification non enregistrées. Voulez-vous continuer ?)"
		   title="Attacher un fichier"
		   aria-label="Attacher un fichier">
			<svg><use href="${request.static_url('endi:static/icons/endi.svg')}#paperclip"></use></svg>
		</a>
	</div>
</div>
</%block>

<%block name="details_modal">
<section
    id="edition_form"
    class="modal_view size_middle"
    % if not formerror:
    style="display: none;"
    % endif
    >
    <div role="dialog" id="edition-forms" aria-modal="true" aria-labelledby="edition-forms_title">
        <div class="modal_layout">
            <header>
                <button class="icon only unstyled close" title="Fermer cette fenêtre" aria-label="Fermer cette fenêtre" onclick="toggleModal('edition_form'); return false;">
                    <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#times"></use></svg>
                </button>
                <h2 id="edition-forms_title">Modifier les données relatives à l’atelier</h2>
            </header>
            <main>
            	${form|n}
            </main>
        </div>
    </div>
</section>

</%block>
<%block name="after_details">
<div class="separate_top content_vertical_padding">
    <h2>
    Émargement
    </h2>
    <form method='POST'
        class="deform  deform" accept-charset="utf-8"
        enctype="multipart/form-data" action="${request.route_path('workshop',\
        id=request.context.id, _query=dict(action='record'))}">

        <input type="hidden" name="__start__" value="attendances:sequence" />
		<ul class='nav nav-tabs'>
			% for timeslot in request.context.timeslots:
				<li \
				% if loop.first:
					class='active' \
				% endif
				>
					<a href='#tab_${timeslot.id}' data-toggle='tab'>
						${timeslot.name}
					</a>
				</li>
			% endfor
		</ul>
		<div class='tab-content'>
		% for timeslot in request.context.timeslots:
			<div class='tab-pane \
				% if loop.first:
					active \
				% endif
					' id='tab_${timeslot.id}'>
				<h3>Émargement de la tranche horaire ${timeslot.name}</h3>
				<p class="content_vertical_padding">
					<strong>Horaires&nbsp;: </strong>  de ${api.format_datetime(timeslot.start_time, timeonly=True)} \
à ${api.format_datetime(timeslot.end_time, timeonly=True)} \
(${timeslot.duration[0]}h${timeslot.duration[1]})
				</p>
				<div class="content_vertical_padding">
					<a class='btn'
						href='${request.route_path("timeslot.pdf", id=timeslot.id)}'
						title='Télécharger la feuille d’émargement pour cette tranche horaire'
						aria-label='Télécharger la feuille d’émargement pour cette tranche horaire'>
					<svg><use href="${request.static_url('endi:static/icons/endi.svg')}#file-pdf"></use></svg>
					Télécharger la feuille d’émargement<span class="no_mobile">&nbsp;pour cette tranche horaire</span>
					</a>
				</div>
				% for attendance in timeslot.sorted_attendances:
				<input type="hidden" name="__start__" value="attendance:mapping" />
				<% participant = attendance.user %>
				<% participant_url = request.route_path('/users/{id}', id=participant.id) %>
				<% status = attendance.status %>

				<% tag_id = "presence_%s_%s" % (timeslot.id, participant.id) %>
				<input type='hidden' name='account_id' value='${participant.id}' />
				<input type='hidden' name='timeslot_id' value='${timeslot.id}' />
				<div class='row form-group timeslot_attendee'>
					<label for="${tag_id}">
						<a href='${participant_url}' title='Voir le compte de ce participant'>
							${api.format_account(participant)}
						</a>
					</label>
					<input type='hidden' value='status:rename' name='__start__' />
					% for index, value  in enumerate(available_status):
					<% val, label = value %>
						<label class='radio-inline' >
							<input id='${tag_id}' name='${tag_id}' type='radio' \
							% if status == val:
								 checked \
							% endif
							value='${val}' />${label}
						 </label>
					% endfor
					<input type='hidden' name='__end__' />
				</div>
				<input type='hidden' name='__end__' value='attendance:mapping' />
				% endfor
			</div>
			% endfor
		</div>
		<input type="hidden" name="__end__" value='attendances:sequence'/>
		<div class='content_vertical_padding'>
			<button id="deformsubmit" class="btn btn-primary" value="submit" type="submit" name="submit" title="Enregistrer l’émargement" aria-label="Enregistrer l’émargement">
				<svg><use href="${request.static_url('endi:static/icons/endi.svg')}#save"></use></svg>
				Enregistrer
			</button>
		</div>
	</form>
</div>
</%block>
