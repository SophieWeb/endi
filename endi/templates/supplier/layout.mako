<%inherit file="/layouts/default.mako" />
<%namespace file="/base/utils.mako" import="format_mail" />
<%namespace file="/base/utils.mako" import="format_phone" />
<%namespace file="/base/utils.mako" import="format_text" />
<%namespace file="/base/utils.mako" import="table_btn" />


<%block name='actionmenucontent'>
<div class='layout flex main_actions'>
    ${request.layout_manager.render_panel('action_buttons', links=main_actions)}
    ${request.layout_manager.render_panel('action_buttons', links=more_actions)}
</div>
</%block>

<%block name='content'>
<div class="layout flex two_cols">
    <div>
        <div class='data_display'>
            <h2>Informations générales</h2>
            <div class='panel-body'>
                % if supplier.is_internal():
                    <h3 class="highlight_title">
                    ${supplier.label.upper()} <em>Enseigne interne à la CAE</em>
                    </h3>
                % else:
                    <h3 class="highlight_title">${supplier.label.upper()}</h3>
                % endif
                % if not supplier.is_internal():
                <div class='layout flex two_cols'>
                    <div><strong>Numéro d'immatriculation</strong></div>
                    <div>
                        % if supplier.registration:
                            ${format_text(supplier.registration)}
                        % else:
                            <em>Non renseigné</em>
                        % endif
                    </div>
                </div>
                % endif
                <div class='layout flex two_cols'>
                    <div><strong>Contact principal</strong></div>
                    <div><address>${format_text(supplier.get_name())}</address></div>
                </div>
                % if not supplier.is_internal() and supplier.function:
                    <div class='layout flex two_cols'>
                        <div><strong>Fonction</strong></div>
                        <div>${format_text(supplier.function)}</div>
                    </div>
                % endif
                <div class='layout flex two_cols'>
                    <div><strong>Adresse Postale</strong></div>
                    <div><address>${format_text(supplier.full_address)}</address></div>
                </div>
                <div class="layout flex two_cols">
                    <div><strong>Addresse électronique</strong></div>
                    <div>
                        %if supplier.email:
                            ${format_mail(supplier.email)}
                        % else:
                            <em>Non renseigné</em>
                        % endif
                    </div>
                </div>
                <div class="layout flex two_cols">
                    <div><strong>Téléphone portable</strong></div>
                    <div>
                        %if supplier.mobile:
                            ${format_phone(supplier.mobile, 'mobile')}
                        %else:
                            <em>Non renseigné</em>
                        %endif
                    </div>
                </div>
                <div class="layout flex two_cols">
                    <div><strong>Téléphone</strong></div>
                    <div>
                        %if supplier.phone:
                            ${format_phone(supplier.phone, 'desk')}
                        %else:
                            <em>Non renseigné</em>
                        %endif
                    </div>
                </div>
                <div class="layout flex two_cols">
                    <div><strong>Fax</strong></div>
                    <div>
                        %if supplier.fax:
                            ${format_phone(supplier.fax, 'fax')}
                        % else:
                            <em>Non renseigné</em>
                        % endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div>
        <div class='data_display'>
            <h2>Informations comptables</h2>
            <div class='panel-body'>
                % if not supplier.is_internal():
                <div class='layout flex two_cols'>
                    <div><strong>TVA intracommunautaire</strong></div>
                    <div>
                        % if supplier.tva_intracomm:
                            ${supplier.tva_intracomm}
                        % else:
                            <em>Non renseigné</em>
                        % endif
                    </div>
                </div>
                % endif
                <% datas = (
                ("Compte CG", supplier.compte_cg),
                ("Compte Tiers", supplier.compte_tiers),) %>
                % for label, value in datas :
                <div class='layout flex two_cols'>
                    <div><strong>${label}</strong></div>
                    <div>
                        % if value:
                            ${value}
                        % else:
                            <em>Non renseigné</em>
                        % endif
                    </div>
                </div>
                % endfor
            </div>
        </div>
    </div>
</div>

<div class='data_display separate_top'>
    <h3>Commentaires</h3>
    % if supplier.comments:
        ${format_text(supplier.comments)}
    %else :
        <em>Aucun commentaire</em>
    % endif
</div>

<div class="data_display separate_top">
    <div class='tabs' id='subview'>
        <%block name='rightblock'>
            ${request.layout_manager.render_panel('tabs', layout.docs_menu)}
        </%block>
    </div>
    <div class='tab-content'>
        <%block name='mainblock'></%block>
    </div>
</div>

</%block>
