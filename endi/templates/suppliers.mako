<%inherit file="${context['main_template'].uri}" />
<%namespace file="/base/utils.mako" import="dropdown_item"/>
<%namespace file="/base/pager.mako" import="pager"/>
<%namespace file="/base/pager.mako" import="sortable"/>
<%namespace file="/base/searchformlayout.mako" import="searchform"/>

<%block name='actionmenucontent'>
<div class='layout flex main_actions'>
    <div role='group'>
        % if api.has_permission('add_supplier'):
            <button class='btn btn-primary' onclick="toggleModal('supplier_add_form'); return false;">
                <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#plus"></use></svg>Ajouter<span class="no_mobile">&nbsp;un fournisseur</span>
            </button>
            <a class='btn' href="${request.route_path('company_suppliers_import_step1', id=request.context.id)}">
                <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#file-import"></use></svg>Importer<span class="no_mobile">&nbsp;des fournisseurs</span>
            </a>
        % endif
    </div>
    <%
    ## We build the link with the current search arguments
    args = request.GET
    url = request.route_path('suppliers.csv', id=request.context.id, _query=args)
    %>
    <a class='btn' href='${url}' title="Export au format CSV">
        <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#file-csv"></use></svg>CSV
    </a>
</div>
</%block>

<%block name='content'>

${searchform()}

<div>
    <div>
        ${records.item_count} Résultat(s)
    </div>
    <div class='table_container'>
        <table class="hover_table">
            <thead>
                <tr>
                    <th scope="col" class="col_date">${sortable("Créé le", "created_at")}</th>
                    <th scope="col">${sortable("Code", "code")}</th>
                    <th scope="col" class="col_text">${sortable("Nom du fournisseur", "label")}</th>
                    <th scope="col" class="col_text">${sortable("Nom du contact principal", "lastname")}</th>
                    <th scope="col" class="col_actions" title="Actions"><span class="screen-reader-text">Actions</span></th>
                </tr>
            </thead>
            <tbody>
                % if records:
                    % for supplier in records:
                        <tr class='tableelement' id="${supplier.id}">
                            <% url = request.route_path("supplier", id=supplier.id) %>
                            <% onclick = "document.location='{url}'".format(url=url) %>
                            <td onclick="${onclick}" class="col_date" >${api.format_date(supplier.created_at)}</td>
                            <td onclick="${onclick}">${supplier.code}</td>
                            <td onclick="${onclick}" class="col_text" >
                                % if supplier.archived:
                                    <small title="Ce fournisseur a été archivé"><span class='icon'><svg><use href="${request.static_url('endi:static/icons/endi.svg')}#archive"></use></svg> Fournisseur archivé</span></small><br />
                                % endif
                                ${supplier.label}
                            </td>
                            <td onclick="${onclick}" class="col_text" >
                                ${supplier.get_name()}
                            </td>
                            ${request.layout_manager.render_panel('action_buttons_td', links=stream_actions(supplier))}
                        </tr>
                    % endfor
                % else:
                    <tr>
                        <td colspan='5' class="col_text">
                            <em>Aucun fournisseur n’a été référencé</em>
                        </td>
                    </tr>
                % endif
            </tbody>
        </table>
	</div>
	${pager(records)}
</div>

<section id="supplier_add_form" class="modal_view size_middle" style="display: none;">
    <div role="dialog" id="supplier-forms" aria-modal="true" aria-labelledby="supplier-forms_title">
        <div class="modal_layout">
            <header>
                <button class="icon only unstyled close" title="Fermer cette fenêtre" aria-label="Fermer cette fenêtre" onclick="toggleModal('supplier_add_form'); return false;">
                    <svg>
                        <use href="${request.static_url('endi:static/icons/endi.svg')}#times"></use>
                    </svg>
                </button>
                <h2 id="supplier-forms_title">Ajouter un fournisseur</h2>
            </header>
            <main>
                <div class="row" id="companyForm">
                    <h3>${forms[0][0]|n}</h3>
                    ${forms[0][1].render()|n}
                </div>
            </main>
        </div>
    </div>
</section>
</%block>

<%block name='footerjs'>
$(function(){
    $('input[name=search]').focus();
});
</%block>
