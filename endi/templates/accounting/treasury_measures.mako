<%inherit file="${context['main_template'].uri}" />
<%namespace file="/base/utils.mako" import="dropdown_item"/>
<%namespace file="/base/pager.mako" import="pager"/>
<%namespace file="/base/pager.mako" import="sortable"/>
<%namespace file="/base/searchformlayout.mako" import="searchform"/>

<%block name='afteractionmenu'>
% if current_grid is not None and current_grid.rows:
    <div class='alert'>
        <h4>
            ${current_grid.label | n}
            % if last_grid.grid.id != current_grid.grid.id:
                <small><a href="${request.route_path("/companies/{id}/accounting/treasury_measure_grids", id=request.context.company.id)}">
                    Voir le dernier état de trésorerie
                </a></small>
            % endif
        </h4>
    </div>
    <% measures = current_grid.rows %>
    <div class='layout flex two_cols'>
        <div class="align_center vertical_align_center">
            <h4>${highlight_entry[0].label | n}</h4>
            <div class='oversized_text'>
                ${api.format_float(highlight_entry[1], precision=2) | n}&nbsp;€
            </div>
        </div>
        <div class='table_container'>
            <table>
                % for typ, value in measures:
                    % if typ.is_total:
                        <tr class="row_recap row_total">
                            <th scope="row" class="col_text">
                                <h3>
                    % else:
                        <tr>
                            <td class="col_text">
                    % endif
                    ${typ.label | n}
                    % if typ.is_total:
                            </h3>
                        </th>
                    % else:
                        </td>
                    % endif
                        <td class='col_number'>${api.format_float(value, precision=2)|n}&nbsp;€</td>
                    </tr>
                % endfor
            </table>
        </div>
    </div>
% else:
    <div class='alert'><h4>Aucun état de trésorerie n'est disponible</h4></div>
% endif
</%block>

<%block name='content'>
<div class='text_block separate_top'>
    <h2>Historique des états de trésorerie</h2>
</div>

${searchform()}

% if records is not None:
<div>
    <div>
        ${records.item_count} Résultat(s)
    </div>
    % if records:
        <div class='table_container limited_width width40'>
            <table class="hover_table">
                <thead>
                    <tr>
                        <th scope="col" class="col_date">${sortable("Date", "date")}</th>
                        <th scope="col" class="col_number">${highlight_entry[0].label | n}</th>
                        <th scope="col" class="col_actions" title="Actions"><span class="screen-reader-text">Actions</span></th>
                    </tr>
                </thead>
                <tbody>
                % for record in records:
                    <tr
                    % if record.id == current_grid.grid.id:
                        class="highlighted"
                    % endif
                    >
                        <% url = request.route_path("/treasury_measure_grids/{id}", id=record.id) %>
                        <% onclick = "document.location='{url}'".format(url=url) %>
                        <td onclick="${onclick}" class="col_date" >
                            ${api.format_date(record.date)}
                        </td>
                        <td onclick="${onclick}" class="col_number" >
                            <% first_measure = record.get_measure_by_type(highlight_entry[0].id) %>
                            % if first_measure is not None:
                                ${api.format_amount(first_measure.value, precision=0)}&nbsp;€
                            % else:
                                -
                            % endif
                        </td>
                        ${request.layout_manager.render_panel('action_buttons_td', links=stream_actions(record))}
                    </tr>
                % endfor
                </tbody>
            </table>
            ${pager(records)}
        % else:
            <div>Aucun élément ne correspond à votre requête</div>
        % endif
    </div>
</div>
% endif
</%block>
