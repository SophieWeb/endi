<%inherit file="${context['main_template'].uri}" />
<%namespace file="/base/utils.mako" import="table_btn"/>
<%namespace file="/base/pager.mako" import="pager"/>
<%namespace file="/base/pager.mako" import="sortable"/>
<%namespace file="/base/searchformlayout.mako" import="searchform"/>

<%block name="actionmenucontent">
% if request.has_permission('admin.activity'):
	<div class='layout flex main_actions'>
		<a class='btn btn-primary' href="${request.route_path('activities', _query={'action': 'new'})}" title="Programmer un nouveau rendez-vous">
			<svg><use href="${request.static_url('endi:static/icons/endi.svg')}#calendar-alt"></use></svg>
			Nouveau<span class="no_mobile">&nbsp;rendez-vous</span>
		</a>
		<div role='group'>
			<%
			args = request.GET
			url_xls = request.route_path('activities.xls', _query=args)
			url_ods = request.route_path('activities.ods', _query=args)
			%>
			<a class='btn' href='${url_xls}' title="Exporter les éléments de la liste au format Excel (xls)">
				<svg><use href="${request.static_url('endi:static/icons/endi.svg')}#file-excel"></use></svg> Excel
			</a>
			<a class='btn' href='${url_ods}' title="Exporter les éléments de la liste au format Open Document (ods)">
				<svg><use href="${request.static_url('endi:static/icons/endi.svg')}#file-spreadsheet"></use></svg> ODS
			</a>
		</div>
	</div>
% endif
</%block>

<%block name='content'>

${searchform()}

% if last_closed_event is not UNDEFINED and last_closed_event is not None:
	<div>
		<h3>Dernières préconisations</h3>
		<blockquote>
			${api.clean_html(last_closed_event.action)|n}
			<footer>le ${api.format_date(last_closed_event.datetime)}</footer>
		</blockquote>
	</div>
% endif

<div>
    <div>
    	${records.item_count} Résultat(s)
    </div>
    <div class='table_container'>
		<table class="hover_table">
			<thead>
				<tr>
					<th scope="col" class="col_status" title="Statut"><span class="screen-reader-text">Statut</span></th>
					<th scope="col" class="col_datetime">${sortable("Horaire", "datetime")}</th>
					<th scope="col" class="col_text">${sortable("Accompagnateur", "conseillers")}</th>
					<th scope="col" class="col_text">Participant(s)</th>
					<th scope="col" class="col_text">Nature du Rdv</th>
					<th scope="col" class="col_text">Mode de Rdv</th>
					<th scope="col" class="col_actions" title="Actions"><span class="screen-reader-text">Actions</span></th>
				</tr>
			</thead>
			<tbody>
				% for activity in records:
					<% url = request.route_path('activity', id=activity.id) %>
					% if request.has_permission('view.activity', activity):
						<% onclick = "document.location='{url}'".format(url=url) %>
					% else :
						<% onclick = "alert(\"Vous n'avez pas accès aux données de ce rendez-vous\");" %>
					% endif
					<tr>
						<td onclick="${onclick}" class="col_status">
							<% status_icon = "clock" %>
							<% status_title = "Rendez-vous programmé" %>
							% if activity.status == "closed":
								<% status_icon = "check" %>
								<% status_title = "Rendez-vous terminé" %>
							% elif activity.status == "cancelled":
								<% status_icon = "times" %>
								<% status_title = "Rendez-vous annulé" %>
							% endif
							<span class="icon status ${activity.status}" title="${status_title}">
								<svg><use href="${request.static_url('endi:static/icons/endi.svg')}#${status_icon}"></use></svg>
							</span>
						</td>
						<td onclick="${onclick}" class="col_datetime">
							${api.format_datetime(activity.datetime)}
						</td>
						<td onclick="${onclick}" class="col_text">
							<ul>
							% for conseiller in activity.conseillers:
								<li>${api.format_account(conseiller)}</li>
							% endfor
							</ul>
						</td>
						<td onclick="${onclick}" class="col_text">
							<ul>
							% for participant in activity.participants:
								<li>${api.format_account(participant)}</li>
							% endfor
							</ul>
						</td>
						<td onclick="${onclick}" class="col_text">
							% if activity.type_object is not None:
								${activity.type_object.label}
							% endif
						</td>
						<td onclick="${onclick}" class="col_text">
							${activity.mode}
						</td>
						<td 
							% if api.has_permission('edit.activity', activity):
							class="col_actions width_three"
							% else:
							class="col_actions width_one"
							% endif
							>
							% if api.has_permission('edit.activity', activity):
								<ul>
									<li>
										<% edit_url = request.route_path('activity', id=activity.id, _query=dict(action="edit")) %>
										${table_btn(edit_url, "Voir/éditer", "Voir / Éditer le rendez-vous", icon='pen')}
									</li>
									<li>
										<% pdf_url = request.route_path("activity.pdf", id=activity.id) %>
										${table_btn(pdf_url, "PDF", "Télécharger la fiche de rendez-vous au format PDF", icon='file-pdf')}
									</li>
									<li>
										<% del_url = request.route_path('activity', id=activity.id, _query=dict(action="delete")) %>
										${table_btn(del_url, "Supprimer", "Supprimer ce rendez-vous", icon='trash-alt', \
										onclick="return confirm('Êtes vous sûr de vouloir supprimer ce rendez-vous ?')", \
										css_class="negative", method='post')}
									</li>
								</ul>
							% else:
								${table_btn(url, "Voir", "Voir le rendez-vous", icon='arrow-right')}
							% endif
						</td>
					</tr>
				% endfor
			</tbody>
		</table>
    </div>
    ${pager(records)}
</div>
</%block>
