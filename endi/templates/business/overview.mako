<%inherit file="${context['main_template'].uri}" />
<%namespace name="utils" file="/base/utils.mako" />
<%block name='mainblock'>
<% business = layout.current_business_object %>
<div class='content_vertical_padding'>
	% if business.closed:
	<div class='alert alert-success'>
		<span class="icon"><svg><use href="${request.static_url('endi:static/icons/endi.svg')}#success"></use></svg></span>
		Cette affaire est clôturée
	</div>
	% endif
    % if switch_invoicing_mode_link:
    ${request.layout_manager.render_panel(switch_invoicing_mode_link.panel_name, context=switch_invoicing_mode_link)}
    % endif
    % for link in invoicing_links:
    ${request.layout_manager.render_panel(link.panel_name, context=link)}
    % endfor
</div>
% if price_studies:
<div class='content_vertical_padding'>
	<h3>Étude de prix</h3>
	<p>
	% for price_study in price_studies:
		Étude :
		<a
			class="link"
			href="${request.route_path('/price_studies/{id}', id=price_study.id)}"
			>
			${price_study.name}
		</a>
        <br />
	% endfor
	</p>
</div>
% endif
<div class='content_vertical_padding'>
	<h3>Devis de référence</h3>
	% if not estimations:
    <p>
		<em>Aucun devis n’est associé à cette affaire</em>
	</p>
    % else:
	<div class='table_container'>
		<table>
			<thead>
            <th scope="col" class='col_text'>
            Nom
            </th>
            <th scope="col" class='col_text'>
            Statut
            </th>
            <th scope="col" class='col_number'>
            HT
            </th>
            <th scope='col' class='col_actions'>
            Actions
            </th>
            </thead>
            <tbody>
        % for estimation in estimations:
        <tr>
        <td class='col_text'>
            <a
                class="link"
                href="${request.route_path('/estimations/{id}', id=estimation.id)}"
                >
                ${estimation.name} (<small>${estimation.internal_number}</small>)
            </a>
        </td>
        <td class='col_text'>
            ${api.format_status(estimation)}
        </td>
        <td class='col_number'>
            ${api.format_amount(estimation.ht, precision=5) | n}&nbsp;€
        </td>
        <td class='col_actions'>
            <div class='btn-group'>
				<a
                    class='btn icon only'
                    href="${api.task_url(estimation)}"
                    title="Voir le devis"
                    aria-label="Voir le devis"
                    >
                    <svg><use href="/static/icons/endi.svg#pen" xlink:href="#pen"></use></svg> Voir
				</a>
                <a
                    class='btn icon only'
                    href="javascript:void(0);"
                    title="PDF"
                    aria-label="Télécharger le pdf"
                    onclick="window.openPopup('${api.task_url(estimation, suffix='.pdf')}')"
                    >
                    <svg><use href="/static/icons/endi.svg#file-pdf" xlink:href="#file-pdf"></use></svg>&nbsp;PDF
                </a>

            </div>
        </td>
            </tr>
        % endfor
        </tbody>
        </table>
    % endif
    % if estimation_add_link:
    ${request.layout_manager.render_panel('post_button', context=estimation_add_link)}
	% endif
</div>
<div class='separate_top content_vertical_padding'>
	% if file_requirements or custom_indicators:
	<h3>Indicateurs</h3>
	% for indicator in custom_indicators:
	${request.layout_manager.render_panel('custom_indicator', indicator=indicator)}
	% endfor
	${request.layout_manager.render_panel('sale_file_requirements', file_requirements=file_requirements)}
	% endif
</div>
% if payment_deadlines:
<div class='content_vertical_padding'>
	<h3>Échéances de paiement</h3>
	<div class='table_container'>
		<table>
			<tbody>
			% for deadline in payment_deadlines:
			<% url = request.route_path(invoice_deadline_route, id=business.id, deadline_id=deadline.id) %>
				<tr>
					% if deadline.deposit:
					<th scope="row" colspan="2">
						Facture d’acompte ${deadline.estimation.deposit} %
					</th>
					% else:
					<th scope="row">
						${deadline.payment_line.description}
					</th>
					<td class="col_number">
						${api.format_amount(deadline.payment_line.amount, precision=5) | n}&nbsp;€
					</td>
					% endif
					% if deadline.invoice_id:
					<td class="col_text">
						<a href="${request.route_path('/invoices/{id}', id=deadline.invoice_id)}">
						${deadline.invoice.name} (${api.format_status(deadline.invoice)})
						</a>
					</td>
						% if not business.closed and business.invoicing_mode == 'classic':
					    <td class="col_actions">
                            <%utils:post_action_btn url="${url}" icon="copy" _class="btn">
		                        Re-générer la facture
                            </%utils:post_action_btn>
						</td>
						% endif
					% elif deadline.invoice_id:
					<td class="col_text">
					</td>
						% if not business.closed and business.invoicing_mode == 'classic':
						 <td class="col_actions">
                             <%utils:post_action_btn url="${url}" icon="copy" _class="btn">
		                         Re-générer la facture
                             </%utils:post_action_btn>
						</td>
						% endif
					% elif not deadline.deposit and deadline.payment_line.date:
					<td class="col_text">
						Facturation prévue le ${api.format_date(deadline.payment_line.date)}
					</td>
						% if not business.closed:
						    <td class="col_actions">
                            <%utils:post_action_btn url="${url}" icon="file-invoice-euro" _class="btn">
		                        Générer la facture
                            </%utils:post_action_btn>
						% endif
					% else:
					<td class="col_text">
						En attente de facturation
					</td>
						% if not business.closed:
						<td class="col_actions">
                            <%utils:post_action_btn url="${url}" icon="file-invoice-euro" _class="btn">
		                        Générer la facture
                            </%utils:post_action_btn>
						</td>
						% endif
					% endif
				</tr>
			% endfor
			</tbody>
		</table>
	</div>
</div>
% endif
% if invoice_list:
<div class='content_vertical_padding'>
	<h3>Factures</h3>
	<div class='table_container'>
		<table>
        <thead>
        <th scope='col' class='col_text'>
        Nom
        </th>
        <th scope='col' class='col_text'>
        Statut
        </th>
        <th scope='col' class='col_number'>
        HT
        </th>
        <th scope='col'>
        </th>
        </thead>
			<tbody>
            % for invoice in invoice_list:
            <tr>
            <td class='col_text'>${invoice.name}</td>
            <td class='col_text'>${api.format_status(invoice)}</td>
            <td class='col_number'>${api.format_amount(invoice.ht, precision=5)}&nbsp;€</td>
            <td class='col_actions'>
            <div class='btn-group'>
				<a
                    class='btn icon only'
                    href="${api.task_url(invoice)}">
                <svg><use href="/static/icons/endi.svg#pen" xlink:href="#pen"></use></svg> Voir
				</a>
                <a
                    class='btn icon only'
                    href="javascript:void(0);"
                    title="PDF"
                    onclick="window.openPopup('${api.task_url(invoice, suffix='.pdf')}')" >
                    <svg><use href="/static/icons/endi.svg#file-pdf" xlink:href="#file-pdf"></use></svg>&nbsp;PDF
                </a>
                </div>
            </td>
            % endfor
            </tbody>
        </table>
    </div>
</div>
%endif
</%block>
