<%inherit file="/layouts/default.mako" />
<%block name='actionmenucontent'>
% if layout.close_button or layout.current_business_object.closed:
<div class='layout flex main_actions'>
	<div role="group">
% endif
% if layout.close_button:
${request.layout_manager.render_panel(layout.close_button.panel_name, context=layout.close_button)}
% endif
% if layout.current_business_object.closed:
    <p>
        <span class="icon"><svg><use href="${request.static_url('endi:static/icons/endi.svg')}#lock"></use></svg></span>
    Cette affaire est clôturée
    </p>
% endif
% if layout.close_button or layout.current_business_object.closed:
	</div>
</div>
% endif
</%block>

<%block name="headtitle">
<h1>
    ${layout.current_business_object.business_type.label}  : ${layout.current_business_object.name}
</h1>
</%block>

<%block name='content'>
<div class="layout flex">
	<h2>
		<span
			class='icon status ${layout.current_business_object.status}'
			% if layout.current_business_object.status == 'success':
			title="Cette affaire est complète"
			aria-label="Cette affaire est complète"
			% else:
			title="Des éléménts sont manquants dans cette affaire"
			aria-label="Des éléménts sont manquants dans cette affaire"
			% endif
			>
			<svg><use href="${request.static_url('endi:static/icons/endi.svg')}#${layout.current_business_object.status}"></use></svg>
		</span>
		${layout.current_business_object.name}
		% if request.has_permission("edit.business", layout.current_business_object):
		<a
			class='btn icon only unstyled'
			href="${layout.edit_url}"
			title="Modifier le nom de cette affaire"
			aria-label="Modifier le nom de cette affaire"
			>
			<svg><use href="${request.static_url('endi:static/icons/endi.svg')}#pen"></use></svg>
		</a>
		% endif
	</h2>
</div>
<div>
    <div class='tabs'>
		<%block name='rightblock'>
			${request.layout_manager.render_panel('tabs', layout.businessmenu)}
		</%block>
    </div>
    <div class='tab-content'>
		<%block name='mainblock'>
			Main
		</%block>
   </div>
</div>
</%block>
