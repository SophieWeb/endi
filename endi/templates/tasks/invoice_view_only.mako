<%inherit file="/tasks/view_only.mako" />
<%namespace file="/base/utils.mako" name="utils" />
<%namespace file="/base/utils.mako" import="format_filelist" />


<%block name='panel_heading'>
    <% invoice = request.context %>
    % if invoice.official_number:
        ${api.overridable_label('invoice', invoice)} N<span class="screen-reader-text">umér</span><sup>o</sup> ${invoice.official_number} (${invoice.name})
    % else:
        <em>${invoice.name}</em>
    % endif
</%block>

<%block name='moretabs'>
    <% invoice = request.context %>
    <li role="presentation">
        <a href="#treasury" aria-control="treasury" role='tab' data-toggle='tab'>
			<span class='icon'>${api.icon('file-spreadsheet')}</span>
        	<span>Comptabilité</span>
        </a>
    </li>
    <li role="presentation">
        <a href="#payment" aria-control="payment" role='tab' data-toggle='tab'>
			<span class='icon'>${api.icon('euro-circle')}</span>
        	<span>Encaissements</span>
        </a>
    </li>
</%block>

<%block name='before_summary'>
    <% invoice = request.context %>
    % if invoice.estimation:
    <div class="separate_bottom content_vertical_padding">
        <h4>
            Devis de référence :
            <a href="${request.route_path('/estimations/{id}.html', id=invoice.estimation.id)}">
                ${invoice.estimation.internal_number}
            </a>
			<a href="${request.route_path('/invoices/{id}/attach_estimation', id=invoice.id)}" class="btn icon only unstyled">
				<svg><use href="${request.static_url('endi:static/icons/endi.svg')}#link"></use></svg>
			</a>
        </h4>
    </div>
    % endif
    % if invoice.cancelinvoices:
    <div class="separate_bottom">
        <ul>
            % for cancelinvoice in invoice.cancelinvoices:
                <li>
					L’avoir (${api.format_cancelinvoice_status(cancelinvoice, full=False)}): \
					<a href="${request.route_path('/cancelinvoices/{id}.html', id=cancelinvoice.id)}">
						${cancelinvoice.internal_number}
						% if cancelinvoice.official_number:
							&nbsp;<small>( ${cancelinvoice.official_number} )</small>
						% endif
					</a> a été généré depuis cette facture.
                </li>
            % endfor
        </ul>
    </div>
    % endif
</%block>

<%block name='after_summary'>
<% invoice = request.context %>
    % if invoice.internal and \
    invoice.supplier_invoice and \
    api.has_permission('view.supplier_invoice', invoice.supplier_invoice):
    <dl class='dl-horizontal'><dt>Facture interne</dt>
    <dd>
    <a
        href='${request.route_path("/suppliers_invoices/{id}", id=invoice.supplier_invoice_id)}'
        title="Voir la facture fournisseur"
        aria-label="Voir la facture fournisseur"
        >
        Voir la facture fournisseur associée
    </a>
    </dd>
    </dl>
    % endif
</%block>

<%block name='moretabs_datas'>
    <% invoice = request.context %>
    <div role="tabpanel" class="tab-pane row" id="treasury">
        <div>
            <div class='alert'>
                Cette facture est rattachée à l’année fiscale ${invoice.financial_year}.
                % if api.has_permission('set_treasury.invoice'):
                    <a class='btn btn-primary' href="${request.route_path('/invoices/{id}/set_treasury', id=invoice.id)}">
                    	<svg><use href="${request.static_url('endi:static/icons/endi.svg')}#pen"></use></svg>
                        Modifier
                    </a>
                % endif
                <br />
                Elle porte le numéro ${invoice.official_number}.
            </div>
            <% url = request.route_path('/export/treasury/invoices/{id}', id=invoice.id, _query={'force': True}) %>
            % if invoice.exported:
                <div class='content_vertical_padding'>
                    <span class="icon status valid"><svg><use href="${request.static_url('endi:static/icons/endi.svg')}#check"></use></svg></span>
                    Cette facture a été exportée vers la comptabilité
                </div>
                % if api.has_permission('admin_treasury'):
                <div class='content_vertical_padding'>
                    <a class='btn' href="${url}">
                        <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#file-export"></use></svg>
                        Forcer la génération d’écritures pour cette facture
                    </a>
                </div>
                % endif
            % else:
                <div class='content_vertical_padding'>
                    <span class="icon status neutral"><svg><use href="${request.static_url('endi:static/icons/endi.svg')}#clock"></use></svg></span>
                    Cette facture n'a pas encore été exportée vers la comptabilité
                </div>
                % if api.has_permission('admin_treasury'):
                <div class='content_vertical_padding'>
                    <a class='btn btn-primary' href="${url}">
                        <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#file-export"></use></svg>
                    	Générer les écritures pour cette facture
                    </a>
                </div>
                % endif
            % endif
        </div>
    </div>
    <div role="tabpanel" class="tab-pane row" id="payment">
        % if api.has_permission('add_payment.invoice'):
        <div class="content_vertical_padding">
            <a class='btn btn-primary' href="${request.route_path('/invoices/{id}/addpayment', id=invoice.id)}">
                <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#plus-circle"></use></svg>
                Enregistrer un encaissement
            </a>
        </div>
        <div class="content_vertical_padding separate_top">
        % else:
        <div class="content_vertical_padding">
        % endif
            <h3>Liste des encaissements</h3>
            % if invoice.payments:
                <% total_payments = 0 %>
                <div class="table_container">
                    <table class="hover_table">
                        <thead>
                            <th scope="col" class="col_date">Date</th>
                            <th scope="col" class="col_text">Mode de règlement</th>
                            % if not invoice.internal:
                            <th scope="col" class="col_text">TVA</th>
                            <th scope="col" class="col_text">N° de remise</th>
                            % endif
                            <th scope="col" class="col_number">Montant</th>
                            <th scope="col" class="col_actions width_one" title="Actions"><span class="screen-reader-text">Actions</span></th>
                        </thead>
                        <tbody>
                            % for payment in invoice.payments:
                                <% url = request.route_path('payment', id=payment.id) %>
                                <tr>
                                    <td class="col_date">${api.format_date(payment.date)}</td>
                                    <td class="col_text">${api.format_paymentmode(payment.mode)}</td>
                                    % if not invoice.internal:
                                    <td class="col_text">
                                        % if payment.tva is not None:
                                            ${payment.tva.name}
                                        % endif
                                    </td>
                                    <td class="col_text">${payment.bank_remittance_id}</td>
                                    % endif
                                    <td class="col_number">${api.format_amount(payment.amount, precision=5)}&nbsp;€</td>
                                    <td class="col_actions width_one">
                                        <a href="${url}" class="btn icon only" title="Voir/Modifier cet encaissement" aria-label="Voir/Modifier cet encaissement">
                                            ${api.icon('arrow-right')}
                                        </a>
                                    </td>
                                </tr>
                                <% total_payments += payment.amount %>
                            % endfor
                        </tbody>
                        <tfoot>
                            <tr class="row_recap">
                                % if invoice.internal:
                                    <th scope="col" class="col_text">Total encaissé</th>
                                % else:
                                <th scope="col" class="col_text" colspan="4">Total encaissé</th>
                                % endif
                                <th scope="col" class="col_number">${api.format_amount(total_payments, precision=5)}&nbsp;€</th>
                                <th scope="col" class="col_actions width_one">&nbsp;</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            % else:
                <em>Aucun encaissement n’a été saisi</em>
            % endif
        </div>
        <div class="content_vertical_padding separate_top">
            <h3>Avoir(s)</h3>
            % if invoice.cancelinvoices:
                <% hasone = False %>
                <ul>
                    % for  cancelinvoice in invoice.cancelinvoices:
                        % if cancelinvoice.status == 'valid':
                            <% hasone = True %>
                            <li>
								L’avoir : \
								<a href="${request.route_path('/cancelinvoices/{id}.html', id=cancelinvoice.id)}">
									${cancelinvoice.internal_number}
									(numéro ${cancelinvoice.official_number})
									d’un montant TTC de ${api.format_amount(cancelinvoice.ttc, precision=5)} €
								</a> a été généré depuis cette facture.
                            </li>
                        % endif
                    % endfor
                </ul>
                % if not hasone:
                    <em>Aucun avoir validé n’est associé à ce document</em>
                % endif
            % else:
                <em>Aucun avoir validé n’est associé à ce document</em>
            % endif
        </div>
    </div>
</%block>
