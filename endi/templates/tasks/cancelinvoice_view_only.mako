<%inherit file="/tasks/view_only.mako" />
<%namespace file="/base/utils.mako" name="utils" />
<%namespace file="/base/utils.mako" import="format_filelist" />

<%block name='panel_heading'>
    <% cancelinvoice = request.context %>
    % if cancelinvoice.official_number:
        Avoir N<span class="screen-reader-text">umér</span><sup>o</sup> ${cancelinvoice.official_number} (${cancelinvoice.name})
    % else:
        <em>${cancelinvoice.name}</em>
    % endif
</%block>

<%block name='moretabs'>
    <% cancelinvoice = request.context %>
    <li role="presentation">
        <a href="#treasury" aria-control="treasury" role='tab' data-toggle='tab'>
			<span class='icon'>${api.icon('file-spreadsheet')}</span>
        	<span>Comptabilité</span>
        </a>
    </li>
</%block>

<%block name='before_summary'>
    <% cancelinvoice = request.context %>
	% if cancelinvoice.invoice:
    <div class="separate_bottom content_vertical_padding">
        <h4>
			Facture de référence :
			<a href="${request.route_path('/invoices/{id}.html', id=cancelinvoice.invoice.id)}">
			    ${cancelinvoice.invoice.internal_number}
			</a>
		</h4>
	</div>
	% else:
	<div class='alert alert-danger'>
		<span class="icon"><svg><use href="${request.static_url('endi:static/icons/endi.svg')}#danger"></use></svg></span>
		Cet avoir n’est attaché à aucune facture (cela ne devrait pas se produire)
	</div>
	% endif
</%block>

<%block name='moretabs_datas'>
    <% cancelinvoice = request.context %>
    <div role="tabpanel" class="tab-pane row" id="treasury">
        <div class='alert'>
            Cet avoir est rattaché à l’année fiscale ${cancelinvoice.financial_year}.
            % if api.has_permission('set_treasury.cancelinvoice'):
                <a class='btn btn-primary' href="${request.route_path('/cancelinvoices/{id}/set_treasury', id=cancelinvoice.id)}" title="Modifier l’année fiscale" aria-label="Modifier l’année fiscale">
                    <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#pen"></use></svg>
                    Modifier
                </a>
            % endif
            <br />
            Il porte le numéro ${cancelinvoice.official_number}.
        </div>
		<% url = request.route_path('/export/treasury/invoices/{id}', id=cancelinvoice.id, _query={'force': True}) %>
		% if cancelinvoice.exported:
            <div class='content_vertical_padding'>
                <span class='icon'><svg><use href="${request.static_url('endi:static/icons/endi.svg')}#check"></use></svg></span>
                Cet avoir a été exporté vers la comptabilité
            </div>
            <div class='content_vertical_padding'>
                <a href="${url}" class='btn'>
                    <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#file-export"></use></svg>
                    Forcer la génération d’écritures pour cet avoir
                </a>
            </div>
        % else:
            <div class='separate_top content_vertical_padding'>
                <span class='icon'><svg><use href="${request.static_url('endi:static/icons/endi.svg')}#clock"></use></svg></span>
                Cet avoir n’a pas encore été exporté vers la comptabilité
            </div>
            % if api.has_permission('admin_treasury'):
            <div class='content_vertical_padding'>
                <a href="${url}" class='btn btn-primary'>
                    <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#file-export"></use></svg>
                    Générer les écritures pour cet avoir
                </a>
            </div>
            % endif
        % endif
    </div>
</%block>
