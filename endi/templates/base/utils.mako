
<%def name="esc(datas)">
    <%text>${</%text>${datas}<%text>}</%text>\
</%def>


<%def name="format_text(data, breaklines=True)">
    <%doc>
        Replace \n with br for html output
    </%doc>
    % if data is not UNDEFINED and data is not None:
        <% text = data %>
        %if breaklines:
            <% text = text.replace('\n', '<br />') %>
        % else:
            <% text = text.replace('\n', '') %>
        %endif
        ${api.clean_html(text)|n}
    %endif
</%def>


<%def name="format_customer(customer, link=True)">
    <%doc>
        Render a customer
    </%doc>
    %if customer is not UNDEFINED and customer is not None:
        % if link:
            <a href="${request.route_path('customer', id=customer.id)}"
                title="Voir le client ${customer.label}">
        % endif
        ${customer.label}
        % if link:
            </a>
        %endif
    %endif
</%def>


<%def name="format_supplier(supplier, link=True)">
    <%doc>
        Render a supplier
    </%doc>
    %if supplier is not UNDEFINED and supplier is not None:
        % if link:
            <a href="${request.route_path('supplier', id=supplier.id)}"
                title="Voir le fournisseur ${supplier.label}">
        % endif
        ${supplier.label}
        % if link:
            </a>
        %endif
    %endif
</%def>


<%def name="format_project(project, link=True)">
    <%doc>
        Render a project
    </%doc>
    %if project is not UNDEFINED and project is not None:
        % if link:
            <a href="${request.route_path('/projects/{id}', id=project.id)}"
                title="Voir le dossier ${project.name}">
        % endif
        ${project.name}
        % if link:
            </a>
        % endif
    %endif
</%def>


<%def name="format_mail(mail)">
    <%doc>
        Render an email address
    </%doc>
    % if mail is not UNDEFINED and mail is not None:
        <a href="mailto:${mail}" title="Envoyer un mail à cette adresse" aria-label="Envoyer un mail à cette adresse">
            <span class="icon">
                ${api.icon('envelope')}
            </span>${mail}
        </a>
    % endif
</%def>


<%def name="format_phone(phone, phone_type)">
    <%doc>
        Render a phone with a phone link
    </%doc>
    % if phone is not UNDEFINED and phone is not None:
        <a class="phone_link" href="tel://${phone}" title="Appeler ce numéro" aria-label="Appeler ce numéro">
	    % if phone_type != 'none': 
	    	<span class="icon">
	    	% if phone_type is 'desk':
		    	${api.icon('phone')}
		    % endif
	    	% if phone_type is 'mobile':
		    	${api.icon('mobile-alt')}
		    % endif
	    	% if phone_type is 'fax':
		    	${api.icon('fax')}
		    % endif
		    </span>
	    % endif
        ${phone}
        </a>
    % endif
</%def>


<%def name="format_address(obj, multiline=False)">
    <% separator = '<br />' if multiline else ', ' %>
    % if obj.address:
        ${obj.address}${separator if obj.city else ''  | n}
        ${obj.zip_code} ${obj.city.upper()}
    % endif
    % if obj.country and obj.country != 'France':
        % if multiline:
            ${separator | n}${obj.country}
        % else:
            (${obj.country})
        % endif
    % endif
</%def>


<%def name="format_company(company)">
    <h3><a href="${request.route_path('company', id=company.id)}">Enseigne ${company.name}</a></h3>
    <p>${company.goal}</p>
    %if company.logo_id:
        <img src="${api.img_url(company.logo_file)}" alt=""  width="250px" />
    %endif
    <dl>
        % if company.email:
            <dt>E-mail</dt>
            <dd>${format_mail(company.email)}</dd>
        % endif
        % for label, attr in (('Téléphone', 'phone'), ('Téléphone portable', 'mobile'),):
            %if getattr(company, attr):
                <dt>${label}</dt>
                <dd>
                	% if attr == 'mobile':
                		${format_phone(getattr(company, attr), 'mobile')}
                	% else:
		                ${format_phone(getattr(company, attr), 'desk')}
                	%endif
                </dd>
            % endif
        % endfor
        % if company.address or company.country or company.city:
            <dt>Adresse</dt>
            <dd>${format_address(company, multiline=False)}</dd>
        % endif
        % if company.activities:
            <dt>Domaine(s) d'activité</dt>
            <dd>
                <ul>
                    % for activity in company.activities:
                        <li>${activity.label}</li>
                    % endfor
                </ul>
            </dd>
        % endif
        % if request.has_permission('admin_treasury'):
            <dt>Code comptable</dt>
            <dd>${company.code_compta or "Non renseigné"}</dd>
            <dt>Compte client général</dt>
            <dd>${company.general_customer_account or "Non renseigné"}</dd>
            <dt>Compte client tiers</dt>
            <dd>${company.third_party_customer_account or "Non renseigné"}</dd>
            <dt>Compte client général interne</dt>
            <dd>${company.internalgeneral_customer_account or "Non renseigné"}</dd>
            <dt>Compte client tiers interne</dt>
            <dd>${company.internalthird_party_customer_account or "Non renseigné"}</dd>
            <dt>Compte fournisseur général</dt>
            <dd>${company.general_supplier_account or "Non renseigné"}</dd>
            <dt>Compte fournisseur tiers</dt>
            <dd>${company.third_party_supplier_account or "Non renseigné"}</dd>

            <dt>Compte fournisseur général interne</dt>
            <dd>${company.internalgeneral_supplier_account or "Non renseigné"}</dd>
            <dt>Compte fournisseur tiers interne</dt>
            <dd>${company.internalthird_party_supplier_account or "Non renseigné"}</dd>

            <dt>Compte de banque</dt>
            <dd>${company.bank_account or "Non renseigné"}</dd>
            <dt>Taux d'assurance professionnelle</dt>
            <dd>${company.custom_insurance_rate or "Non renseigné"}</dd>
            <dt>Contribution à la CAE (en %)</dt>
            <dd>
                % if company.contribution:
                    ${company.contribution}
                % elif request.config.get('contribution_cae'):
                    ${request.config.get('contribution_cae')} (par défaut)
                % else:
                    Non renseigné
                % endif
            </dd>
            <dt>Contribution à la CAE pour la facturation interne(en %)</dt>
            <dd>
                % if company.internalcontribution:
                    ${company.internalcontribution}
                % elif request.config.get('internalcontribution_cae'):
                    ${request.config.get('internalcontribution_cae')} (par défaut)
                % else:
                    Non renseigné
                % endif
            </dd>
            <dt>RIB / IBAN</dt>
            <dd>${company.RIB or 'Non renseigné'} / ${company.IBAN or 'Non renseigné'}</dd>
        % endif
        <dt>Coefficient de frais généraux</dt>
        <dd>${company.general_overhead or 0}</dd>
        <dt>Coefficient de marge</dt>
        <dd>${company.margin_rate or 0}</dd>
        <dt>CGV complémentaires</dt>
        <dd>${company.cgv and 'Renseignées' or "Non renseignées"}</dd>
        <dt>En-tête des documents</dt>
        <dd>${company.header_id and 'Personalisé (image)' or "Par défaut"}</dd>
    </dl>
</%def>


<%def name="format_filelist(parent_node, delete=False, actions=True)">
    % if parent_node is not None:
        % for child in parent_node.children:
            % if loop.first:
                <ul class="file_list">
            % endif
            % if child.type_ == 'file':
                <li>
                <% dl_url = request.route_path('/files/{id}', id=child.id, _query=dict(action='download')) %>
                % if api.has_permission('edit.file', child):
                <% edit_url = request.route_path('/files/{id}', id=child.id) %>
		        <% file_full_description = child.label %>
                    % if actions:
	                    ${child.label}
						<a href="#!" onclick="window.openPopup('${edit_url}');" class="btn icon only unstyled" title="Modifier le fichier ${file_full_description}" aria-label="Modifier le fichier ${file_full_description}">
							<svg><use href="${request.static_url('endi:static/icons/endi.svg')}#pen"></use></svg>
						</a>
						<a href="#!" onclick="window.openPopup('${dl_url}')" class="btn icon only unstyled" title="Télécharger le fichier ${file_full_description}" aria-label="Télécharger le fichier ${file_full_description}">
							<svg><use href="${request.static_url('endi:static/icons/endi.svg')}#download"></use></svg>
						</a>
						% if delete and api.has_permission('delete.file', child):
							<% delete_url = request.route_path('/files/{id}', id=child.id, _query=dict(action='delete')) %>
							<%self:post_action_btn url="${delete_url}" icon="trash-alt" _class="btn icon only unstyled negative"
							title="Supprimer le fichier ${file_full_description}" aria-label="Supprimer le fichier ${file_full_description}" onclick="return confirm('Supprimer le fichier ${file_full_description} ?');">
							</%self:post_action_btn>
						% endif
					% else:
						<a href="#!" onclick="window.openPopup('${dl_url}')" title="Télécharger le fichier ${file_full_description}" aria-label="Télécharger le fichier ${file_full_description}">
							${child.label}
						</a>
					% endif
                % elif api.has_permission('view.file', child):
                    <a href="#!" onclick="window.openPopup('${dl_url}');" title="Télécharger le fichier ${file_full_description}" aria-label="Télécharger le fichier ${file_full_description}">
                        <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#download"></use></svg>
                        ${child.label}
                    </a>
                % endif
                </li>
            % endif
            % if loop.last:
                </ul>
            % endif
        % endfor
    % endif
</%def>


<%def name="format_filetable(documents)">
    <table class="hover_table">
        % if documents != []:
        <thead>
            <th scope="col" class="col_text">Description</th>
            <th scope="col" class="col_text">Nom du fichier</th>
            <th scope="col" class="col_date">Déposé le</th>
            <th scope="col" class="col_actions" title="Actions"><span class="screen-reader-text">Actions</span></th>
        </thead>
        %endif
        <tbody>
            % for child in documents:
                <% dl_url = request.route_path('/files/{id}', id=child.id, _query=dict(action='download')) %>
                <% onclick = "document.location='{dl_url}'".format(dl_url=dl_url) %>
                <tr onclick="${onclick}">
                    <td class="col_text">${child.description}</td>
                    <td class="col_text">${child.name}</td>
                    <td class="col_date">${api.format_date(child.updated_at)}</td>
                    <% actions_col_width = "width_one" %>
                    % if api.has_permission('edit.file', child) and api.has_permission('delete.file', child):
                    	<% actions_col_width = "width_three" %>
                    % elif api.has_permission('edit.file', child) or api.has_permission('delete.file', child):
                    	<% actions_col_width = "width_two" %>
                    %endif
                    <td class="col_actions ${actions_col_width}">
                        % if api.has_permission('edit.file', child):
                            <% edit_url = request.route_path('/files/{id}', id=child.id) %>
                            ${table_btn(edit_url, "Voir/Modifier", "Voir/Modifier ce document", icon="pen", css_class="icon")}
                        % endif
                        ${table_btn(dl_url, "Télécharger", "Télécharger ce document", icon="download", css_class="icon")}
                        % if api.has_permission('delete.file', child):
                            <% message = "Ce fichier sera définitivement supprimé. Êtes-vous sûr de vouloir continuer ?" %>
                            <% del_url = request.route_path('/files/{id}', id=child.id, _query=dict(action='delete')) %>
                            ${table_btn(del_url, "Supprimer", "Supprimer ce document", icon="trash-alt", css_class='icon negative',
                                onclick="return confirm('%s')" % message)}
                        % endif
                    </td>
                </tr>
            % endfor
            % if documents == []:
                <tr><td colspan='6' class="col_text"><em>Aucun document n’est disponible</em></td></tr>
            % endif
        </tbody>
  </table>
</%def>


<%def name="company_disabled_msg()">
	<span class="icon tag caution">${api.icon('danger')} désactivée</span>
</%def>


<%def name="company_internal_msg()">
    <span class="icon tag neutral">${api.icon('info-circle')} interne</span>
</%def>


<%def name="login_disabled_msg()">
	<span class="icon tag caution">${api.icon('lock')} désactivé</span>
</%def>


<%def name="show_tags_label(tags)">
    <br /><span class="icon tag neutral">${api.icon('tag')} 
    % for tag in tags:
        ${tag.label}
    % endfor
    </span>
</%def>


<%def name="company_list_badges(company)">
    % if not company.active:
        ${company_disabled_msg()}
    % endif
    % if company.internal:
        ${company_internal_msg()}
    % endif
</%def>


<%def name="show_project_type_and_business_types_labels(project)">
    % if project.project_type.label and project.project_type.label != "Dossier classique":
        <% business_types = [business_type.label for business_type in project.business_types] %>
        <br /><span class="icon tag neutral" title="${', '.join(business_types)}"><svg><use href="${request.static_url('endi:static/icons/endi.svg')}#tag"></use></svg>  ${project.project_type.label}</span>
    % endif
</%def>


<%def name="post_action_btn(url, icon=None, **tag_attrs)">
    <%doc>
    :param tag_attrs: kwargs that are translated to HTML tag properties, with the following transformations :
      - _class → class
        - underscore are converted to dashes (ex: aria_role → aria-role)
    </%doc>
    <form class="btn-container" action="${url}" method="post">
        ${csrf_hidden_input()}
        <button
            % for k, v in tag_attrs.items():
            <% k = k.replace('_class', 'class').replace('_', '-') %>
            ${k}="${v}"
            % endfor
        >
            % if icon is not None:
                <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#${icon}"></use></svg>
            % endif
            ${caller.body()}
        </button>
    </form>
</%def>


<%def name="table_btn(href, label, title, icon=None, onclick=None, icotext=None, css_class='', method='get')">
    % if method == 'get':
        <a href='${href}'
    % else: # POST
        <form method="post" action="${href}" class="btn-container">
            ${csrf_hidden_input()}
            <button
    % endif
        class='btn icon only ${css_class}' href='${href}' title="${title}" aria-label="${label}"
        % if onclick:
            onclick="${onclick}"
        % endif
    >
    %if icotext:
        <span>${api.clean_html(icotext)|n}</span>
    % endif
    %if icon:
        <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#${icon}"></use></svg>
    %endif
    % if method == 'get':
        </a>
    % else: #POST
            </button>
        </form>
    % endif
</%def>


<%def name="dropdown_item(href, label, title, icon=None, onclick=None, icotext=None, disable=False)">
    <li
    % if disable:
        class='disabled'
    % endif
    >
        <a href='${href}' title="${title}" aria-label="${title}"
            % if onclick:
                onclick="${onclick.replace('\n', '\\n')|n}"
            % endif
            >
            %if icotext:
                <span>${api.clean_html(icotext)|n}</span>
            % endif
            %if icon:
                <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#${icon}"></use></svg>
            %endif
            ${label}
        </a>
    </li>
</%def>


<%def name="definition_list(items)">
    <%doc>
        render a list of elements as a definition_list
        items should be an iterator of (label, values) 2-uple
    </%doc>
    <dl class="dl-horizontal">
        % for label, value in items:
            <dt>${label}</dt>
            <dd>${value}</dd>
        % endfor
    </dl>
</%def>


<%def name="csrf_hidden_input()">
    <input type="hidden" name="csrf_token" value="${get_csrf_token()}" />
</%def>

<%def name="show_amount_or_undefined_string(limit)">
    <%doc>
        render an amount or a string message
    </%doc>
    %if limit is not None:
        ${limit} € HT
    % else:
        Non précisé (pas de limite)
    % endif
</%def>

<%def name="format_js_appoptions(options)">
<%doc>Render the AppOption global object definition</%doc>
var AppOption = {}
% for key, value in options.items():
% if isinstance(value, bool):
AppOption["${key}"] = ${value and 'true' or 'false'};
% elif isinstance(value, (int, float)):
AppOption["${key}"] = ${value};
% else:
AppOption["${key}"] = "${value}";
% endif
% endfor
</%def>
