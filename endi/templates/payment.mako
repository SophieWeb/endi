<%inherit file="${context['main_template'].uri}" />
<%namespace file="/base/utils.mako" import="definition_list" />

<%def name="display_actions()">
% if api.has_permission('edit.payment', request.context) or api.has_permission('delete.payment', request.context):
    <div class="layout flex main_actions">
        <div role='group'>
        % if api.has_permission('edit.payment', request.context):
            <a class='btn btn-primary icon' href='${edit_url}'>
                <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#pen"></use></svg> Modifier
            </a>
        % endif
        % if api.has_permission('delete.payment', request.context):
            <a class='btn icon negative' href='${del_url}' onclick="return confirm('Êtes-vous sûr de vouloir supprimer ce paiement ?');">
                <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#trash-alt"></use></svg> Supprimer
            </a>
        % endif
        </div>
    </div>
% endif
</%def>
<%block name='actionmenucontent'>
% if not request.is_popup:
${display_actions()}
% endif
</%block>

<%block name="content">
% if request.is_popup:
${display_actions()}
% endif
<div>
    <div class='content_vertical_padding'>
    % if request.context.exported:
        <span class="icon status success">
            <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#check"></use></svg>
        </span> Cet ${money_flow_type} a été exporté vers la comptabilité
    % else:
        <span class="icon status neutral">
            <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#clock"></use></svg>
        </span> Cet ${money_flow_type} n’a pas encore été exporté vers la comptabilité
    % endif
    </div>
    <dl class="dl-horizontal">
        <dt>Date</dt><dd>${api.format_date(request.context.date)}</dd>
        <dt>Montant</dt><dd>${api.format_amount(request.context.amount, precision=request.context.precision)}&nbsp;&euro;</dd>
        <dt>Mode de paiement</dt><dd>${request.context.mode}</dd>
        % if hasattr(request.context, 'customer_bank'):
            <dt>Banque du client</dt>
            <dd>
                % if request.context.customer_bank:
                    ${request.context.customer_bank.label}
                % else:
                    <em>Non renseignée</em>
                % endif
            </dd>
        % endif
        % if hasattr(request.context, 'check_number'):
            <dt>Numéro du chèque</dt>
            <dd>
                % if request.context.check_number:
                    ${request.context.check_number}
                % else:
                    <em>Non renseigné</em>
                % endif
            </dd>
        % endif
        % if hasattr(request.context, 'tva'):
            <dt>Tva liée</dt>
            <dd>
                % if request.context.tva:
                    ${request.context.tva.name}
                % else:
                    <em>Non renseignée</em>
                % endif
            </dd>
        % endif
        % if hasattr(request.context, 'bank_remittance_id'):
            % if hasattr(request.context, 'check_number'):
                ## Pour les encaissement on parle de remise
                <dt>Numéro de remise</dt>
            % else:
                ## Pour les paiements ES/frns on parle de référence
                <dt>Référence du paiement</dt>
            % endif
            <dd>
                % if request.context.bank_remittance_id:
                    ${request.context.bank_remittance_id}
                % else:
                    <em>Non renseigné</em>
                % endif
            </dd>
        % endif
        % if hasattr(request.context, 'bank'):
            <dt>Compte bancaire</dt>
            <dd>
                % if request.context.bank:
                    ${request.context.bank.label} (${request.context.bank.compte_cg})
                % else:
                    <em>Non renseignée</em>
                % endif
            </dd>
        % endif
        % if hasattr(request.context, 'user'):
            <dt>Enregistré par</dt><dd>${api.format_account(request.context.user)}</dd>
        % endif
    </dl>
</div>
</%block>
