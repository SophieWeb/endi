<%doc>
    Career path form rendering
</%doc>

<%inherit file="${context['main_template'].uri}" />

<%block name="mainblock">
    ${request.layout_manager.render_panel(
        'help_message_panel',
        parent_tmpl_dict=context.kwargs
    )}
    <div class="limited_width width50">
        ${form|n}
    </div>



    % if files:
	<div class="content_vertical_padding separate_top">
		<h3>Documents attachés à cette étape</h3>
		<table class='table_hover'>
			<thead>
				<th scope="col" class="col_text">Fichier</th>
				<th scope="col" class="col_number" title="Taille du fichier">Taille<span class="screen-reader-text"> du fichier</span></th>
				<th scope="col" class="col_actions" title="Actions"><span class="screen-reader-text">Actions</span></th>
			</thead>
			<tbody>
			% for file in files:
				<% edit_url = request.route_path('userdatas_file', id=request.context.userdatas.id, id2=file.id) %>
				<% del_url = request.route_path('/files/{id}', id=file.id, _query=dict(action='delete')) %>
				<tr>
					<td class="col_text">${file.description}</td>
					<td  class="col_number">${api.human_readable_filesize(file.size)}, modifié le ${api.format_date(file.updated_at)}</td>
					<td class="col_actions width_two">
						<ul>
							<li>
								<a href="${edit_url}" class="btn icon only">
									<svg><use href="${request.static_url('endi:static/icons/endi.svg')}#pen"></use></svg>
									Voir/Modifier
								</a>
							</li>
							<li>
								<a href="${del_url}" class="btn icon only negative" onclick="return confirm('Êtes vous sûr de vouloir supprimer ce document ?')">
									<svg><use href="${request.static_url('endi:static/icons/endi.svg')}#trash-alt"></use></svg>
									Supprimer
								</a>
							</li>
						</ul>
					</td>
				</tr>
			% endfor
			</tbody>
		</table>
	</div>
    % endif


    <script type='text/javascript'>
    $( function() {
        const setParcoursSalaryValue = () => {
            const hourly_rate = $('input[name=taux_horaire]').val();
            const num_hours = $('input[name=num_hours]').val();
            $('input[name=parcours_salary]')
                    .val(hourly_rate * num_hours).attr('readonly', true)
        }
        $('input[name=parcours_salary]').attr('readonly', true)
        $('input[name=taux_horaire], input[name=num_hours]').change(
                function () {
                    setParcoursSalaryValue();
                }
        );
    });
    </script>

</%block>
