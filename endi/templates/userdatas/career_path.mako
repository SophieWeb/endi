<%inherit file="${context['main_template'].uri}" />
<%namespace file="/base/utils.mako" name="utils"/>
<%block name="mainblock">
% if api.has_permission('edit.userdatas'):
	<% add_url = request.current_route_path(_query={'action': 'add_stage'}) %>
	<div class="content_vertical_padding">
		<a class='btn btn-primary' href="${add_url}">
			${api.icon('plus')}
			Ajouter une étape de parcours
		</a>
	</div>
% endif
<div class="table_container">
    <table class='hover_table'>
        <thead><tr>
            <th scope="col" class="col_date">Date</th>
            <th scope="col" class="col_date">&Eacute;chéance</th>
            <th scope="col" class="col_text">&Eacute;tape</th>
            <th scope="col" class="col_text">Nouvelle situation</th>
            <th scope="col" class="col_text">Fichiers rattachés</th>
            <th scope="col" class="col_actions" title="Actions"><span class="screen-reader-text">Actions</span></th>
        </tr></thead>
        <tbody>
            % if career_path:
                % for stage in career_path:
                    <% edit_url = request.route_path('/career_paths/{id}', id=stage.id, _query=dict(action='edit')) %>
                    <% del_url = request.route_path('/career_paths/{id}', id=stage.id, _query=dict(action='delete')) %>
                    <% onclick = "document.location='{edit_url}'".format(edit_url=edit_url) %>
                    <tr>
                        <td class="col_date" onclick="${onclick}" title="Cliquer pour modifier l’étape" aria-label="Cliquer pour modifier l’étape">${api.format_date(stage.start_date)}</td>
                        <td class="col_date" onclick="${onclick}" title="Cliquer pour modifier l’étape" aria-label="Cliquer pour modifier l’étape">${api.format_date(stage.end_date)}</td>
                        <td class="col_text" onclick="${onclick}" title="Cliquer pour modifier l’étape" aria-label="Cliquer pour modifier l’étape">
                            % if stage.career_stage is not None:
                                ${stage.career_stage.name}
                            % endif
                        </td>
                        <td class="col_text" onclick="${onclick}" title="Cliquer pour modifier l’étape" aria-label="Cliquer pour modifier l’étape">
                            % if stage.cae_situation is not None:
                                <strong>${stage.cae_situation.label}</strong>
                            % endif
                        </td>
                        <td class='col_text'>
                            % if stage.files:
                            % for child in stage.files:
                            % if loop.first:
                                <ul>
                            % endif
                                <li>
                                <% file_dl_url = request.route_path('/files/{id}', id=child.id, _query=dict(action='download')) %>
                                % if api.has_permission('edit.file', child):
                                <% file_edit_url = request.route_path('userdatas_file', id=user.userdatas, id2=child.id) %>
                                    ${child.label}
                                    <a href="#!" onclick="window.openPopup('${file_edit_url}');" class="btn icon only unstyled" title="Modifier ce fichier" aria-label="Modifier ce fichier">
                                        ${api.icon('pen')}
                                    </a>
                                    <a href="#!" onclick="window.openPopup('${file_dl_url}')" class="btn icon only unstyled" title="Télécharger ce fichier" aria-label="Télécharger ce fichier">
                                        ${api.icon('download')}
                                    </a>
                                % elif api.has_permission('view.file', child):
                                    <a href="#!" onclick="window.openPopup('${file_dl_url}');" class="btn icon unstyled" title="Télécharger ce fichier" aria-label="Télécharger ce fichier">
                                        ${api.icon('download')}
                                        ${child.label}
                                    </a>
                                % endif
                                </li>
                            % if loop.last:
                                </ul>
                            % endif
                            % endfor
                            % endif
                        </td>
                        <td class="col_actions width_two">
                            <ul>
								<li><a class="btn icon only" href="${edit_url}" title="Modifier cette étape" aria-label="Modifier cette étape">
									${api.icon('pen')}
									</a></li>
								<li>
                                    <%utils:post_action_btn url="${del_url}" icon="trash-alt"
                                      _class="btn icon only negative"
                                      onclick="return confirm('Êtes vous sûr de vouloir supprimer cette étape de parcours ?')"
                                      title="Supprimer cette étape"
                                      aria_label="Supprimer cette étape"
                                    >
                                    </%utils:post_action_btn>
							    </li>
							</ul>
                        </td>
                    </tr>
                % endfor
            % else:
                <tr><td colspan=4 style="text-align:center; padding-top:20px; font-style:italic;">
                    Le parcours de cet entrepreneur est vierge
                </td></tr>
            % endif
        </tbody>
    </table>
</%block>
