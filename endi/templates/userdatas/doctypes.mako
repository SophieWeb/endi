<%inherit file="${context['main_template'].uri}" />
<%namespace file="/base/utils.mako" import="format_filetable" />
<%block name="mainblock">
% if not is_void:
<div class='alert alert-info'>
<span class="icon"><svg><use href="${request.static_url('endi:static/icons/endi.svg')}#info-circle"></use></svg></span>
Cochez ici les documents sociaux que l’entrepreneur a déjà transmis
</div>
<div class='doc_list'>
${form|n}
</div>
% else:
<div class='alert alert-info'>
<span class="icon"><svg><use href="${request.static_url('endi:static/icons/endi.svg')}#info-circle"></use></svg></span>
Aucun type de document n'a été configuré.
% if api.has_permission('admin'):
<br />
Vous pouvez les configurez <a href='#' onclick='window.openPopup("${request.route_path('/admin/userdatas/social_doc_type_option')}");'>dans l'interface de configuration.</a>
% endif
</div>
% endif
</%block>
