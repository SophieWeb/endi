<%inherit file="${context['main_template'].uri}" />
<%namespace file="/base/utils.mako" import="table_btn"/>
<%namespace file="/base/utils.mako" import="company_list_badges"/>
<%namespace file="/base/utils.mako" import="login_disabled_msg"/>
<%namespace file="/base/utils.mako" import="format_phone"/>
<%namespace file="/base/pager.mako" import="pager"/>
<%namespace file="/base/pager.mako" import="sortable"/>
<%namespace file="/base/searchformlayout.mako" import="searchform"/>

<%block name='actionmenucontent'>
% if request.has_permission('add.userdatas') or request.has_permission('add_user') or request.has_permission('manage'):
	<div class='layout flex main_actions'>
		<div role='group'>
			% if request.has_permission('add.userdatas') and request.has_module('userdatas'):
				<a class='btn btn-primary' href="${request.route_path('/userdatas', _query=dict(action='add'))}">
					${api.icon('plus')}Ajouter un entrepreneur
				</a>
			% endif
			% if request.has_permission('add_user'):
				<a class='btn' href="${request.route_path('/users/add/manager')}">
					${api.icon('plus')}Ajouter un permanent
				</a>
			% endif
		</div>
		<div role='group'>
			% if request.has_permission('manage'):
				<a class='btn' title="Voir l'historique des connexions utilisateurs" href="${request.route_path('/users/connections')}">
					${api.icon('chart-line')}Utilisateurs actifs
				</a>
			% endif
		</div>
	</div>
% endif
</%block>

<%block name='content'>

${searchform()}

<div>
    <div>
    	${records.item_count} Résultat(s)
    </div>
    <div class='table_container'>

		<table class="hover_table top_align_table">
			<thead>
				<tr>
					<th scope="col" class="col_text">${sortable("Nom", "name")}</th>
					<th scope="col" class="col_text">Enseigne</th>
					<th scope="col" class="col_text">Activité</th>
					<th scope="col" class="col_text"><span class="icon">${api.icon('envelope')}</span>E-mail</th>
					<th scope="col" class="col_text phone"><span class="icon">${api.icon('phone')}</span>Téléphone</th>
					<th scope="col" class="col_text phone"><span class="icon">${api.icon('mobile-alt')}</span>Téléphone portable</th>
					<th scope="col" class="col_text">Code postal</th>
					% if request.has_permission('manage'):
						<th scope="col" class="col_actions" title="Actions"><span class="screen-reader-text">Actions</span></th>
					% endif
				</tr>
			</thead>
			<tbody>
				% if records:
					% for id, user in records:
						% if user.companies or request.has_permission('manage'):
							<% url = request.route_path('/users/{id}', id=user.id) %>
							<tr>
								<td class="col_text">
									% if request.has_permission('view.user', user):
										<a href="${url}">
											${api.format_account(user)}
											% if user.login is None:
												<small>
													<span class="icon">
														${api.icon('exclamation-circle')}
														Ce compte ne dispose pas d’identifiants
													</span>
												</small>
											% elif not user.login.active:
												<small>${login_disabled_msg()}</small>
											% endif
										</a>
									% else:
										${api.format_account(user)}
									% endif
								</td>
								<td class="col_text">
									% if user.companies:
										<ul class="list-unstyled">
											% for company in user.companies:
												<% company_url = request.route_path('company', id=company.id) %>
												<li>
													% if request.has_permission('view.company', company):
														<a href="${company_url}">
															${company.name}
															% if request.has_permission('admin_company', company):
																<small>${company_list_badges(company)}</small>
															% endif
														</a>
													% else:
														${company.name}
													% endif
												</li>
											% endfor
										</ul>
									% else:
										<em>Aucune enseigne</em>
									% endif
								</td>
								<td class="col_text">
									<ul class="list-unstyled">
										% for company in user.companies:
											% if company.goal and company.goal != "":
												<li>${company.goal}</li>
											% endif
										% endfor
									</ul>
								</td>
								<td class="col_text">
									<ul class="list-unstyled">
										% for company in user.companies:
											% if company.email and company.email != "":
												<li><a href="mailto:${company.email}" title="Envoyer un mail à cette adresse" aria-label="Envoyer un mail à cette adresse">${company.email}</a></li>
											% endif
										% endfor
									</ul>
								</td>
								<td class="col_text phone">
									<ul class="list-unstyled">
										% for company in user.companies:
											% if company.phone and company.phone != "":
												<li>${format_phone(company.phone, 'none')}</li>
											% endif
										% endfor
									</ul>
								</td>
								<td class="col_text phone">
									<ul class="list-unstyled">
										% for company in user.companies:
											% if company.mobile and company.mobile != "":
												<li><a href="tel:${company.mobile}" title="Appeler ce numéro" aria-label="Appeler ce numéro">${company.mobile}</a></li>
											% endif
										% endfor
									</ul>
								</td>
								<td class="col_text">
									<ul class="list-unstyled">
										% for company in user.companies:
											% if company.zip_code and company.zip_code != "":
												<li>${company.zip_code}</li>
											% endif
										% endfor
									</ul>
								</td>
								% if request.has_permission('manage'):
								${request.layout_manager.render_panel('action_buttons_td', links=stream_actions(user))}
								% endif
							</tr>
						% endif
					% endfor
				% else:
					<tr>
						<td colspan='7' class="col_text"><center><em>Aucun utilisateur</em></center></td>
					</tr>
				% endif
			</tbody>
		</table>

	</div>
	${pager(records)}
</div>
</%block>
