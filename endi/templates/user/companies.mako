<%inherit file="${context['main_template'].uri}" />
<%namespace file="/base/utils.mako" import="company_disabled_msg"/>
<%namespace file="/base/utils.mako" import="table_btn"/>
<%namespace file="/base/utils.mako" import="company_list_badges" />
<%block name="mainblock">
% if companies:
<div class="table_container">
    <table class="top_align_table hover_table">
        <thead>
            <th scope="col" class="col_status" title="Statut"><span class="screen-reader-text">Statut</span></th>
            <th scope="col" class="col_text">Nom</th>
            <th scope="col" class="col_text"><span class="icon">${api.icon('envelope')}</span>Adresse e-mail</th>
            <th scope="col" class="col_text">Entrepreneur(s)</th>
            <th scope="col" class="col_actions" title="Actions"><span class="screen-reader-text">Actions</span></th>
        </thead>
        <tbody>
		% for company in companies:
			<% url = request.route_path('company', id=company.id) %>
			<% onclick = "document.location='{url}'".format(url=url) %>
				<tr>
			% if not company.active:
				<td onclick="${onclick}" class="col_status">
					<span class="icon status invalid" title="Enseigne désactivée">
						<svg><use href="${request.static_url('endi:static/icons/endi.svg')}#lock"></use></svg>
					</span>
				</td>
			% else:
				<td onclick="${onclick}" class="col_status">
					<span class="icon status valid" title="Enseigne active">
						<svg><use href="${request.static_url('endi:static/icons/endi.svg')}#check"></use></svg>
					</span>
				</td>
			% endif
				<td onclick="${onclick}" class="col_text">
					${company.name} ( ${company.code_compta} )
					% if not company.active:
					${company_disabled_msg()}
					% endif
				</td>
				<td class="col_text">
					<a href="mailto:${company.email}" title="Envoyer un mail à cette adresse" aria-label="Envoyer un mail à cette adresse">
						${company.email}
					</a>
				</td>
				<td onclick="${onclick}" class="col_text">
					<ul>
						% for employee in company.employees:
							<li>
							<a href="${request.route_path('/users/{id}', id=employee.id)}">
								${api.format_account(employee)}
								</a>
							</li>
						% endfor
					</ul>
				</td>
				<td class='col_actions width_one'>
				${request.layout_manager.render_panel('menu_dropdown', label="Actions", links=stream_actions(company))}
				</td>
			</tr>
		% endfor
        </tbody>
    </table>
</div>
% else:
<div><em>Ce compte n’est rattaché à aucune enseigne</em></div>
% endif
<div class="content_vertical_padding">
<h3>Associer</h3>
<a href="${request.route_path('/users/{id}/companies/associate', id=user.id)}"
	class='btn btn-primary'
	title="Associer à une enseigne existante dans enDI"
	aria-label="Associer à une enseigne existante dans enDI">
	<svg><use href="${request.static_url('endi:static/icons/endi.svg')}#building"></use></svg>
	À une enseigne existante
</a>
<a href="${request.route_path('companies', _query=dict(action='add', user_id=user.id))}"
	class='btn btn-primary'
	title="Associer à une nouvelle enseigne"
	aria-label="Associer à une nouvelle enseigne">
	<svg><use href="${request.static_url('endi:static/icons/endi.svg')}#plus"></use></svg>
	À une nouvelle enseigne
</a>
</div>
</%block>
