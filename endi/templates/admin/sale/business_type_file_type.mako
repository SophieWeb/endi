<%inherit file="${context['main_template'].uri}" />
<%namespace file="/base/utils.mako" import="dropdown_item"/>
<%block name='afteradminmenu'>
</%block>
<%block name='content'>
<div>
	<div class='search_filters'>
		<form class='form-search form-inline' method='GET'>
			<div class='form-group'>
				<label class='control-label' for='business_filter'>Type d'affaire</label>
				<select id='business_filter' class='form-control' name='business'>
					<option value='0'>Tous les types d'affaire</option>
					% for business_type in business_types_all:
						% if business_filter == business_type.id:
							<option value='${business_type.id}' selected>${business_type.label}</option>
						% else:
							<option value='${business_type.id}'>${business_type.label}</option>
						% endif
					% endfor
				</select>
			</div>
				<div class='form-group'>
				<label class='control-label' for='file_filter'>Type de fichier</label>
				<select id='file_filter' class='form-control' name='file'>
					<option value='0'>Tous les types de fichier</option>
					% for file_type in file_types_all:
						% if file_filter == file_type.id:
							<option value='${file_type.id}' selected>${file_type.label}</option>
						% else:
							<option value='${file_type.id}'>${file_type.label}</option>
						% endif
					% endfor
				</select>
			</div>
			<div>
				<button class='btn btn-primary' type='submit'>Rechercher</button>
			</div>
		</form>
	</div>
	<form method='POST'
		class="deform  deform" accept-charset="utf-8"
		enctype="multipart/form-data">
		<input type='hidden' name='__start__' value='items:sequence' />
		% for business_type in business_types:
		<h2>${business_type.label}</h2>
		<div class="table_container separate_bottom">
			<table class='top_align_table'>
				<thead>
					<tr>
						<th scope="col" class="col_text">Type de fichier</th>
						% if business_type.name != 'default':
							<th scope="col" class="col_text">Affaire</th>
						% endif
						<th scope="col" class="col_text">Devis</th>
						<th scope="col" class="col_text">Factures</th>
						<th scope="col" class="col_text">Avoirs</th>
					</tr>
				</thead>
				<tbody>
				% for file_type in file_types:
					<% file_type_items = items.get(file_type.id, {}) %>
					<tr>
						<td class="col_text form">
							<h3>${file_type.label}</h3>
							<div>
								Fichier modèle :
								<% file_type_templates = templates.get(file_type.id, {}) %>
								<% template = file_type_templates.get(business_type.id, {}) %>
								% if template:
									<div>
										<a href="/files/${template['file_id']}?action=download"  title="Télécharger ce fichier" aria-label="Télécharger ce fichier">
											${template['file_name']}
										</a>
										<a href="/files/${template['file_id']}?action=download"  class="btn icon only unstyled" title="Télécharger ce fichier" aria-label="Télécharger ce fichier">
											<svg><use href="${request.static_url('endi:static/icons/endi.svg')}#download"></use></svg>
										</a>
									</div>
									<button class='btn btn-primary negative' name='del_template' type='submit' value='${business_type.id}__${file_type.id}'>
										<svg><use href="${request.static_url('endi:static/icons/endi.svg')}#trash-alt"></use></svg>
										Supprimer ce modèle
									</button>
								% else:
									<div><em>Aucun modèle défini</em></div>
									<button class='btn btn-primary' type='button' onclick='window.openPopup("${add_template_url}?business=${business_type.id}&file=${file_type.id}");'>
										<svg><use href="${request.static_url('endi:static/icons/endi.svg')}#plus"></use></svg>
										Ajouter un modèle
									</button>
								% endif
							</div>
						</td>
						<% btype_items = file_type_items.get(business_type.id, {}) %>
						% for doctype in ('business', 'estimation', 'invoice', 'cancelinvoice'):
							% if business_type.name == 'default' and doctype == 'business':
							<% continue %>
							% endif
						<% requirement_type = btype_items.get(doctype, {}).get('requirement_type', -1) %>
						<% validation = btype_items.get(doctype, {}).get('validation') %>
						<% tag_id = "requirement_type_%s_%s_%s" % (file_type.id, business_type.id, doctype) %>
						<td class="col_text">
							<input type='hidden' name='__start__' value='item:mapping' />
							<input type='hidden' name='file_type_id' value='${file_type.id}' />
							<input type='hidden' name='business_type_id' value='${business_type.id}'/>
							<input type='hidden' name='doctype' value='${doctype}' />
							<input type='hidden' name='__start__' value='requirement_type:rename'>
							<label for='${tag_id}'>Ce type de fichier&nbsp;: </label>
							<div class='radio'>
							<label>
								<input type='radio' name='${tag_id}' value=''
								% if requirement_type == -1:
								checked
								% endif
								/>N’est pas proposé dans le formulaire de dépot de fichier
							</label>
							</div>
							% for option, label in (\
								('optionnal', 'Est proposé dans le formulaire de dépot de fichier'), \
								('recommended', 'Est recommandé'), \
								('mandatory', 'Est requis systématiquement pour la validation'), \
								('business_mandatory', "Est globalement requis dans le/la {0} pour la validation".format(business_type.label)), \
								('project_mandatory', "Est globalement requis dans le dossier pour la validation")):
								% if doctype == 'business' and option == 'mandatory':
								<% continue %>
								% endif

								<div class='radio'>
								<label>
									<input type='radio' name='${tag_id}' value='${option}'
									% if requirement_type == option:
									checked
									% endif
									/>${label}
								</label>
								</div>
							% endfor
							<input type='hidden' name='__end__' value='requirement_type:rename'>
							<hr />
							<div class="checkbox">
							<label>
							  <input type="checkbox" name="validation"
							  % if validation:
							  checked
							  % endif
							  >Ce type de fichier exige une validation par l’équipe d’appui&nbsp;?
							</label>
							</div>
						<input type='hidden' name='__end__' value='item:mapping' />
						</td>
					% endfor
					</tr>
					% endfor
				</tbody>
			</table>
		</div>
		% endfor
		<input type='hidden' name='__end__' value='items:sequence' />
		<div class='form-actions'>
		   <button id="deformsubmit" class="btn btn-primary" value="submit" type="submit" name="submit"> Enregistrer </button>
		</div>
	</form>
</div>
</%block>
