<%doc>
    Admin common page template
</%doc>
<%inherit file="${context['main_template'].uri}" />
<%namespace file="/base/utils.mako" import="format_text"/>
<%block name="afteradminmenu">
% if not message is UNDEFINED and message:
    <div class='alert alert-info'>
        <span class="icon"><svg><use href="${request.static_url('endi:static/icons/endi.svg')}#info-circle"></use></svg></span> 
        ${format_text(message)}
    </div>
% endif
</%block>
<%block name='content'>
${request.layout_manager.render_panel('help_message_panel', parent_tmpl_dict=context.kwargs)}
% if not form is UNDEFINED:
    <div class='limited_width width40'>
    % if request.is_popup:
        <h2>${title}</h2>
        <div>
    	    ${form|n}
        </div>
    % else:
        ${form|n}
    % endif
	</div>
% endif
</%block>
