<%inherit file="${context['main_template'].uri}" />
<%namespace file="/base/pager.mako" import="pager"/>
<%namespace file="/base/searchformlayout.mako" import="searchform"/>

<%block name='actionmenucontent'>
<div class='layout flex main_actions'>

    % if not is_admin and api.has_permission('add.invoice'):
    <a class='btn btn-primary' title="Ajouter une nouvelle facture"
       href="${request.route_path('company_invoices', id=request.context.id, _query=dict(action='add'))}"
    >
        ${api.icon('plus')}
        Ajouter une facture
    </a>
    % endif

    % if api.has_permission('admin_treasury') and is_admin:
        <a class='btn' href='/invoices?action=export_pdf'>
            <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#file-export"></use></svg> Export massif
        </a>
    % endif
    <div role='group'>
        <%
        ## We build the link with the current search arguments
        args = request.GET
        if is_admin:
            url_xls = request.route_path('invoices_export', extension="xls", _query=args)
            url_ods = request.route_path('invoices_export', extension="ods", _query=args)
            url_csv = request.route_path('invoices_export', extension="csv", _query=args)
        else:
            url_xls = request.route_path('company_invoices_export', extension="xls", id=request.context.id, _query=args)
            url_ods = request.route_path('company_invoices_export', extension="ods", id=request.context.id, _query=args)
            url_csv = request.route_path('company_invoices_export', extension="csv", id=request.context.id, _query=args)
        %>
        <a class='btn' onclick="window.openPopup('${url_xls}');" href='javascript:void(0);' title="Export au format Excel (xls)">
            <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#file-excel"></use></svg> Excel
        </a>
        <a class='btn' onclick="window.openPopup('${url_ods}');" href='javascript:void(0);' title="Export au format Open Document (ods)">
            <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#file-spreadsheet"></use></svg> ODS
        </a>
        <a class='btn' onclick="window.openPopup('${url_csv}');" href='javascript:void(0);' title="Export au format csv">
            <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#file-csv"></use></svg> CSV
        </a>
    </div>
</div>
</%block>

<%block name='content'>

${searchform()}

<div>
    <div>
        ${records.item_count} Résultat(s)
    </div>
    <div class='table_container'>
        ${request.layout_manager.render_panel('task_list', records, datatype="invoice", is_admin_view=is_admin)}
    </div>
    ${pager(records)}
</div>
</%block>

<%block name='footerjs'>
## #deformField2_chzn (company_id) and #deformField3_chzn (customer_id) are the tag names
% if is_admin:
    $('#deformField2_chzn').change(function(){$(this).closest('form').submit()});
% endif
$('#deformField3_chzn').change(function(){$(this).closest('form').submit()});
$('select[name=year]').change(function(){$(this).closest('form').submit()});
$('select[name=status]').change(function(){$(this).closest('form').submit()});
</%block>
