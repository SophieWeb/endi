<%inherit file="${context['main_template'].uri}" />
<%namespace file="/base/utils.mako" import="format_text" />
<%namespace file="/base/utils.mako" import="format_filelist" />

<%block name="headtitle">
${request.layout_manager.render_panel('task_title_panel', title=title)}
</%block>

<%block name='actionmenucontent'>
<% expense = request.context %>
<div id="js_actions" class="js_actions"></div>
</%block>

<%block name="beforecontent">
<% expense = request.context %>
<div>
	% if len(status_history) > 0 :
    <div class='expense_header layout flex two_cols'>
    % else :
    <div class='expense_header layout flex'>
    % endif
        <div>
            <h3 class='hidden-print'>
                ${request.layout_manager.render_panel('status_title', context=request.context)}
            </h3>
            <ul class="document_summary content_vertical_padding">
                <li class='hidden-print'>
                    ${api.format_expense_status(expense)}
                </li>
                % if request.has_permission('admin_treasury'):
                    <li>
                    	Numéro de pièce :
                        % if expense.status == 'valid':
                            <strong>${ expense.official_number }</strong>
                        % else:
                            <strong>Ce document n’a pas été validé</strong>
                        % endif
                    </li>
                    <li>
                        % if expense.purchase_exported and expense.expense_exported:
                            Ce document a déjà été exporté vers le
                            logiciel de comptabilité
                        % elif expense.purchase_exported:
                            Les achats déclarés dans ce document ont déjà été
                            exportés vers le logiciel de comptabilité
                        % elif expense.expense_exported:
                            Les frais déclarés dans ce document ont déjà été
                            exportés vers le logiciel de comptabilité
                        %else:
                            Ce document n'a pas encore été exporté vers le logiciel de comptabilité
                        % endif
                    </li>
                    <li>
                    	Entrepreneur : ${api.format_account(expense.user)}</li>
                % endif
                    <li>
                       Km déjà validés cette année : ${api.format_amount(kmlines_current_year)} km ${user_vehicle_information}
                    </li>
                % if expense.payments:
                    <li>
                        Paiement(s) recu(s):
                        <ul>
                            % for payment in expense.payments:
                                <% url = request.route_path('expense_payment', id=payment.id) %>
                                <li>
                                <a href="${url}">
                                    <strong>${api.format_amount(payment.amount)}&nbsp;€</strong>
                                    le ${api.format_date(payment.date)}<br>
                                    <small>${api.format_paymentmode(payment.mode)}
                                    % if payment.bank:
                                        &nbsp;(${payment.bank.label})
                                    % endif
                                     enregistré par ${api.format_account(payment.user)}</small>
                                </a>
                                </li>
                            % endfor
                        </ul>
                    </li>
                % endif
            </ul>
        </div>
        ${request.layout_manager.render_panel('status_history', entries=status_history, genre='e')}
    </div>
	<div class="separate_top content_vertical_padding">
		<h3>
		% if not expense.children:
			Aucun justificatif n’a été déposé
		% else:
			Justificatifs
		% endif
		</h3>
		<div class="content_vertical_padding">
			% if expense.justified:
				<span class='icon status success'>${api.icon('check')}</span>
				Justificatifs reçus
			% endif
		</div>
		${format_filelist(expense)}
		<a href="${request.route_path('/expenses/{id}/addfile', id=expense.id)}" class="btn btn-primary" title="Déposer un justificatif de dépense">
			${api.icon('paperclip')}Ajouter un fichier
		</a>
	</div>
</div>
</%block>

<%block name='content'>
<div id="js-main-area"></div>
</%block>

<%block name='footerjs'>
var AppOption = {};
AppOption['context_url'] = "${context_url}";
AppOption['form_config_url'] = "${form_config_url}";
AppOption['csrf_token'] = "${get_csrf_token()}";
% if request.has_permission("edit.expensesheet"):
    AppOption['edit'] = true;
% else:
    AppOption['edit'] = false;
% endif
</%block>
