<%inherit file="${context['main_template'].uri}" />
<%namespace file="/base/utils.mako" import="format_mail" />
<%namespace file="/base/utils.mako" import="format_phone" />
<%namespace file="/base/utils.mako" import="format_text" />
<%namespace file="/base/utils.mako" import="table_btn" />

<%block name='actionmenucontent'>
<div class="layout flex main_actions">
${request.layout_manager.render_panel('action_buttons', links=actions)}
</div>
</%block>

<%block name='content'>
<div class="layout flex two_cols">
    <div>
        <div class='data_display'>
            <h2>Informations générales</h2>
            <div class='panel-body'>
                % if customer.is_company():
                   <h3 class="highlight_title">Entreprise ${customer.label.upper()}</h3>
                % elif customer.is_internal():
                    <h3 class="highlight_title">${customer.label.upper()} <em>Enseigne interne à la CAE</em></h3>
                % else:
                    <h3 class="highlight_title">${customer.label}</h3>
                % endif
                % if customer.is_company() or customer.is_internal():
                    % if customer.registration:
                        <div class='layout flex two_cols'>
                            <div><strong>Numéro d'immatriculation</strong></div>
                            <div>${customer.registration}</div>
                        </div>
                    % endif
                    <div class='layout flex two_cols'>
                        <div><strong>Contact principal</strong></div>
                        <div>${customer.get_name()}</div>
                    </div>
                    % if customer.function:
                        <div class='layout flex two_cols'>
                            <div><strong>Fonction</strong></div>
                            <div>${format_text(customer.function)}</div>
                        </div>
                    % endif
                % endif
               <div class="layout flex two_cols">
                    <div><strong>Adresse Postale</strong></div>
                    <div><address>${format_text(customer.full_address)}</address></div>
                </div>
                <div class="layout flex two_cols">
                    <div><strong>Addresse électronique</strong></div>
                    <div>
                        %if customer.email:
                            ${format_mail(customer.email)}
                        % else:
                            <em>Non renseigné</em>
                        % endif
                    </div>
                </div>
                <div class="layout flex two_cols">
                    <div><strong>Téléphone portable</strong></div>
                    <div>
                        %if customer.mobile:
                            ${format_phone(customer.mobile, 'mobile')}
                        %else:
                            <em>Non renseigné</em>
                        %endif
                    </div>
                </div>
                <div class="layout flex two_cols">
                    <div><strong>Téléphone</strong></div>
                    <div>
                        %if customer.phone:
                            ${format_phone(customer.phone, 'desk')}
                        %else:
                            <em>Non renseigné</em>
                        %endif
                    </div>
                </div>
                <div class="layout flex two_cols">
                    <div><strong>Fax</strong></div>
                    <div>
                        %if customer.fax:
                            ${format_phone(customer.fax, 'fax')}
                        % else:
                            <em>Non renseigné</em>
                        % endif
                    </div>
                </div>
            </div>
        </div>
        <div class='data_display separate_top'>
            <h2>Informations comptables</h2>
            <div class='panel-body'>
                % if customer.is_company():
                    <% datas = (
                    ("TVA intracommunautaire", customer.tva_intracomm),
                    ("Compte CG", customer.compte_cg),
                    ("Compte Tiers", customer.compte_tiers),) %>
                %else:
                    <% datas = (
                    ("Compte CG", customer.compte_cg),
                    ("Compte Tiers", customer.compte_tiers),) %>
                % endif
                % for label, value in datas :
                <div class='layout flex two_cols'>
                    <div><strong>${label}</strong></div>
                    <div>
                        % if value:
                            ${value}
                        % else:
                            <em>Non renseigné</em>
                        % endif
                    </div>
                </div>
                % endfor
                <div class='layout flex two_cols separate_top'>
                    <div><strong>Total des dépenses HT</strong></div>
                    <div>${api.format_amount(customer.get_total_expenses())}&nbsp;€</div>
                </div>
                <div class='layout flex two_cols separate_bottom'>
                    <div><strong>Total facturé HT</strong></div>
                    <div>${api.format_amount(customer.get_total_income(), precision=5)}&nbsp;€</div>
                </div>
            </div>
        </div>
    </div>
    <div>
        <div class='data_display'>
            <h2>Dossiers</h2>
            <div class='panel-body'>
                <table class="hover_table">
	                %if customer.projects:
                    <thead>
                        <tr>
                            <th scope="col">Code</th>
                            <th scope="col" class="col_text">Nom</th>
                            <th scope="col" class="col_actions" title="Actions"><span class="screen-reader-text">Actions</span></th>
                        </tr>
                    </thead>
                    <tbody>
                        % for project in customer.projects:
							<% url = request.route_path('/projects/{id}', id=project.id) %>
							<% onclick = "document.location='{url}'".format(url=url) %>
                            %if project.archived:
                                <tr class='row_archive' id="${project.id}">
                            %else:
                                <tr id="${project.id}">
                            %endif
                                <td onclick="${onclick}" title="Voir ou modifier ce dossier">${project.code}</td>
                                <td class="col_text" onclick="${onclick}" title="Voir ou modifier ce dossier">
                                    ${project.name}
                                    %if project.archived:
                                        (ce dossier a été archivé)
                                    %endif
                                </td>
                                <td class="col_actions width_one">
                                    <div class='btn-group'>
                                        <button type="button" class="btn icon only dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="Actions" aria-label="Actions">
                                            <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#dots"></use></svg>
                                        </button>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="${url}" title="Voir ou modifier ce dossier" class="btn icon">
                                                <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#arrow-right"></use></svg>&nbsp;Voir ce dossier
                                            </a></li>
                                            %if not project.archived:
                                                <% add_estimation_url = request.route_path('/projects/{id}/estimations', id=project.id, _query={'action': 'add'}) %>
                                                <li><a href="${add_estimation_url}" class="btn icon">
                                                    <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#file-list"></use></svg>&nbsp;Ajouter un devis
                                                </a></li>
                                                <% add_invoice_url = request.route_path('/projects/{id}/invoices', id=project.id, _query={'action': 'add'}) %>
                                                <li><a href="${add_invoice_url}" class="btn icon">
                                                    <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#file-invoice-euro"></use></svg>&nbsp;Ajouter une facture
                                                </a></li>
                                                <% archive_url = request.route_path('/projects/{id}', id=project.id, _query=dict(action='archive')) %>
                                                <li><a href="${archive_url}" class="btn icon" onclick="return confirm('Êtes-vous sûr de vouloir archiver ce dossier ?');">
                                                    <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#archive"></use></svg>&nbsp;Archiver ce dossier
                                                </a></li>
                                            %elif api.has_permission('delete_project', project):
                                                <% delete_url = request.route_path('/projects/{id}', id=project.id, _query=dict(action="delete")) %>
                                                <li><a href="${delete_url}" class="btn icon negative" onclick="return confirm('Êtes-vous sûr de vouloir supprimer définitivement ce dossier ?');">
                                                    <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#trash-alt"></use></svg>&nbsp;Supprimer ce dossier
                                                </a></li>
                                            %endif
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        %endfor
                    </tbody>
                %else:
                    <tbody>
                    	<tr>
                    		<td colspan="3" class="col_text">
                    			<em>Aucun dossier n’a été initié avec ce client</em>
                            </td>
                        </tr>
                    </tbody>
                %endif
                    <tfoot>
                    	<tr>
                    		<td class="col_actions" colspan="3">
								<a class='btn icon' href='${add_project_url}' title="Créer un nouveau dossier avec ce client" aria-label="Créer un nouveau dossier avec ce client">
									<svg><use href="${request.static_url('endi:static/icons/endi.svg')}#folder-plus"></use></svg> Nouveau dossier
								</a>
                    		</td>
                    	</tr>
                    	<tr>
                            <td class="col_text" colspan="3">
								<div class="content_vertical_padding deform_inline_flex" id='add-project-form'>
									${project_form.render()|n}
								</div>
                    		</td>
                    	</tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
<div class='data_display separate_top'>
    <h3>Commentaires</h3>
    % if customer.comments:
        ${format_text(customer.comments)}
    %else :
        <em>Aucun commentaire</em>
    % endif
</div>
</%block>
