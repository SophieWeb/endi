<%inherit file="/layouts/default.mako" />

<%block name="headtitle">
<h1>Dossier : ${layout.current_project_object.name}</h1>
</%block>

<%block name='actionmenucontent'>
</%block>

<%block name='content'>
<div>
    <h2>${layout.current_project_object.name}</h2>
    % if layout.current_project_object.description:
        <p>${layout.current_project_object.description}</p>
    % endif
    <hr />
    <div class="layout flex two_cols">
        <div>
            % if layout.current_project_object.project_type.name != 'default':
                <p><strong>Type de dossier :</strong> ${layout.current_project_object.project_type.label}</p>
            % endif
            % if layout.current_project_object.code:
                <p><strong>Code du dossier :</strong> ${layout.current_project_object.code}</p>
            % endif
            <%block name='projecttitle'>
                <% customers_list = layout.current_project_object.customers %>
                % if len(customers_list) == 1:
                    <p><strong>Client :</strong> ${customers_list[0].label}</p>
                % elif len(customers_list) < 6:
                    <p><strong>Clients :</strong> ${', '.join(layout.customer_labels)}</p>
                % else:
                    <p>
                        <strong>Clients :</strong>
                        <span id="short_customers_list">
                            ${customers_list[0].label}, ${customers_list[1].label}, ${customers_list[2].label}
                            <a href="#" title="Voir tous les clients" onclick="display_full_customers_list();">
                                et ${len(customers_list)-3} autres clients <span class="screen-reader-text">Voir tous les clients</span>
                            </a>
                        </span>
                        <span id="full_customers_list" style="display:none;">
                            ${', '.join(layout.customer_labels)}
                        </span>
                    </p>
                % endif
            </%block>
            % if layout.current_project_object.mode == 'ttc':
                <p>
                    <strong>Mode de calcul :</strong>
                    <span class="icon status mode" title="Mode TTC : vous renseignez les prix TTC et le HT est calculé">
                    	<svg><use href="${request.static_url('endi:static/icons/endi.svg')}#mode-ttc"></use></svg>
                    	<span class="screen-reader-text">Mode TTC : vous renseignez les prix TTC et le HT est calculé</span>
                    </span>
                </p>
            % endif
        </div>
        <div>
            <p><strong>Total des dépenses HT :</strong> ${api.format_amount(layout.current_project_object.get_total_expenses())}&nbsp;€</p>
            <p><strong>Total facturé HT : </strong> ${api.format_amount(layout.current_project_object.get_total_income(), precision=5)}&nbsp;€</p>
        </div>
    </div>
</div>
<div>
    <div class='tabs'>
        <%block name='rightblock'>
        ${request.layout_manager.render_panel('tabs', layout.projectmenu)}
        </%block>
    </div>
    <div class='tab-content'>
        <%block name='mainblock'>
        </%block>
    </div>
</div>

<script>
function display_full_customers_list() {
    document.getElementById('short_customers_list').style.display='none';
    document.getElementById('full_customers_list').style.display='inline';
    return false;
}
</script>
</%block>
