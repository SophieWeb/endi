<%inherit file="${context['main_template'].uri}" />
<%namespace file="/base/pager.mako" import="pager"/>
<%namespace file="/base/searchformlayout.mako" import="searchform"/>

<%block name='actionmenucontent'>
% if request.has_permission("edit.project", layout.current_project_object):
<div class='layout flex main_actions'>
    <div role='group'>
        <a class='btn btn-primary icon' href="${layout.edit_url}">
            <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#pen"></use></svg>Modifier le dossier
        </a>
    </div>
</div>
% endif
</%block>

<%block name='mainblock'>

<div>
	<div class='layout flex two_cols content_vertical_padding separate_bottom_dashed'>
		<div role="group">
	    % if request.has_permission('add.invoice'):
    		<a class='btn btn-primary icon' href='${add_url}'>
		        <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#file-invoice-euro"></use></svg> Créer une facture
    		</a>
	    % endif	
		</div>
		<div role='group' class='align_right'>
			<%
			## We build the link with the current search arguments
			args = request.GET
			url_xls = request.route_path('/projects/{id}/invoices.{extension}', extension='xls', id=request.context.id, _query=args)
			url_ods = request.route_path('/projects/{id}/invoices.{extension}', extension='ods', id=request.context.id, _query=args)
			url_csv = request.route_path('/projects/{id}/invoices.{extension}', extension='csv', id=request.context.id, _query=args)
			%>
			<a class='btn' onclick="window.openPopup('${url_xls}');" href='javascript:void(0);' title="Export au format Excel (xls)" aria-label="Export au format Excel (xls)">
				<svg><use href="${request.static_url('endi:static/icons/endi.svg')}#file-excel"></use></svg> Excel
			</a>
			<a class='btn' onclick="window.openPopup('${url_ods}');" href='javascript:void(0);' title="Export au format Open Document (ods)" aria-label="Export au format Open Document (ods)">
				<svg><use href="${request.static_url('endi:static/icons/endi.svg')}#file-spreadsheet"></use></svg> ODS
			</a>
			<a class='btn' onclick="window.openPopup('${url_csv}');" href='javascript:void(0);' title="Export au format csv" aria-label="Export au format csv">
				<svg><use href="${request.static_url('endi:static/icons/endi.svg')}#file-csv"></use></svg> CSV
			</a>
		</div>
    </div>

    ${searchform()}

    <div>
        ${records.item_count} Résultat(s)
    </div>
    <div class='table_container'>
        ${request.layout_manager.render_panel('task_list', records, datatype="invoice", is_admin_view=is_admin, is_project_view=True)}
    </div>
    ${pager(records)}
</div>

</%block>
