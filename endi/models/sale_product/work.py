import logging
from sqlalchemy import (
    Column,
    Integer,
    ForeignKey,
    String,
)
from sqlalchemy.orm import (
    relationship,
)
from endi_base.models.base import (
    default_table_args,
)
from .services import SaleProductWorkService
from .base import BaseSaleProduct


logger = logging.getLogger(__name__)


class SaleProductWork(BaseSaleProduct):
    """
    Work entity grouping several products
    """
    __tablename__ = 'sale_product_work'
    __table_args__ = default_table_args
    __mapper_args__ = {'polymorphic_identity': __tablename__}
    id = Column(
        Integer,
        ForeignKey('base_sale_product.id', ondelete='cascade'),
        primary_key=True,
    )

    title = Column(String(255))

    items = relationship(
        "WorkItem",
        back_populates="sale_product_work",
        cascade='all, delete-orphan'
    )

    _endi_service = SaleProductWorkService

    def __json__(self, request):
        result = BaseSaleProduct.__json__(self, request)
        result['title'] = self.title
        result['items'] = [{'id': item.id} for item in self.items]
        return result

    def sync_amounts(self, work_only=False):
        return self._endi_service.sync_amounts(self, work_only)

    def duplicate(self):
        result = BaseSaleProduct.duplicate(self)
        result.title = self.title
        for item in self.items:
            result.items.append(item.duplicate())
        return result
