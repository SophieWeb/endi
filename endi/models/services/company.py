"""
Company query service
"""
import datetime
from sqlalchemy import (
    desc,
    and_,
)
from sqlalchemy.orm import (
    load_only,
    selectinload,
)
from sqlalchemy.sql.expression import func

from endi_base.models.base import DBSESSION
from endi_base.models.utils import non_null_sum


class CompanyService:
    @classmethod
    def get_tasks(cls, instance, offset=None, limit=None):
        from endi.models.task import Task
        query = DBSESSION().query(Task)
        query = query.filter(Task.company_id == instance.id)
        query = query.order_by(desc(Task.status_date))
        if offset is not None:
            query = query.offset(offset)
        if limit is not None:
            query = query.limit(limit)
        return query

    @classmethod
    def get_estimations(cls, instance, valid=False):
        from endi.models.task import Estimation
        query = DBSESSION().query(Estimation)
        query = query.filter(Estimation.company_id == instance.id)
        if valid:
            query = query.filter(Estimation.status == 'valid')

        return query

    @classmethod
    def get_invoices(cls, instance, valid=False, not_paid=False):
        from endi.models.task import Invoice
        query = DBSESSION().query(Invoice)
        query = query.filter(Invoice.company_id == instance.id)
        if valid:
            query = query.filter(Invoice.status == 'valid')
        elif not_paid:
            query = query.filter(Invoice.status == 'valid')
            query = query.filter(Invoice.paid_status.in_(('paid', 'waiting')))
        return query

    @classmethod
    def get_cancelinvoices(cls, instance, valid=False):
        from endi.models.task import CancelInvoice
        query = DBSESSION().query(CancelInvoice)
        query = query.filter(CancelInvoice.company_id == instance.id)
        if valid:
            query = query.filter(CancelInvoice.status == 'valid')
        return query

    @classmethod
    def get_customers(cls, instance, year):
        from endi.models.task import Invoice
        from endi.models.third_party.customer import Customer
        query = DBSESSION().query(Customer)
        query = query.filter(Customer.company_id == instance.id)
        query = query.filter(
            Customer.invoices.any(
                and_(
                    Invoice.status == 'valid',
                    Invoice.financial_year == year
                )
            )
        )
        return query

    @classmethod
    def get_late_invoices(cls, instance):
        from endi.models.task import Invoice
        query = cls.get_invoices(instance, not_paid=True)
        key_day = datetime.date.today() - datetime.timedelta(days=45)
        query = query.filter(Invoice.date < key_day)
        query = query.order_by(desc(Invoice.date))
        return query

    @classmethod
    def get_customer_codes_and_names(cls, company):
        """
        Return a query for code and names of customers related to company
        :param company: the company we're working on
        :returns: an orm query loading Customer instances with only the columns
        we want
        :rtype: A Sqlalchemy query object
        """
        from endi.models.third_party.customer import Customer
        query = DBSESSION().query(Customer)
        query = query.options(load_only('code', 'label'))
        query = query.filter(Customer.code != None)  # noqa: E711
        query = query.filter(Customer.company_id == company.id)
        return query.order_by(Customer.code)

    @classmethod
    def get_supplier_codes_and_names(cls, company):
        """
        Return a query for code and names of suppliers related to company
        :param company: the company we're working on
        :returns: an orm query loading Supplier instances with only the columns
        we want
        :rtype: A Sqlalchemy query object
        """
        from endi.models.third_party.supplier import Supplier
        query = DBSESSION().query(Supplier)
        query = query.options(load_only('code', 'label'))
        query = query.filter(Supplier.code != None)  # noqa: E711
        query = query.filter(Supplier.company_id == company.id)
        return query.order_by(Supplier.code)

    @classmethod
    def get_project_codes_and_names(cls, company):
        """
        Return a query for code and names of projects related to company

        :param company: the company we're working on
        :returns: an orm query loading Project instances with only the columns
        we want
        :rtype: A Sqlalchemy query object
        """
        from endi.models.project import Project
        query = DBSESSION().query(Project)
        query = query.options(load_only('code', 'name'))
        query = query.filter(Project.code != None)  # noqa: E711
        query = query.filter(Project.company_id == company.id)
        return query.order_by(Project.code)

    @classmethod
    def get_next_index(cls, company, factory):
        query = DBSESSION.query(func.max(factory.company_index))
        query = query.filter(factory.company_id == company.id)
        max_num = query.first()[0]
        if max_num is None:
            max_num = 0

        return max_num + 1

    @classmethod
    def get_next_estimation_index(cls, company):
        """
        Return the next available sequence index in the given company
        """
        from endi.models.task import Estimation
        return cls.get_next_index(company, Estimation)

    @classmethod
    def get_next_invoice_index(cls, company):
        """
        Return the next available sequence index in the given company
        """
        from endi.models.task import Invoice
        return cls.get_next_index(company, Invoice)

    @classmethod
    def get_next_cancelinvoice_index(cls, company):
        """
        Return the next available sequence index in the given company
        """
        from endi.models.task import CancelInvoice
        return cls.get_next_index(company, CancelInvoice)

    @classmethod
    def get_turnover(cls, company, year):
        """
        Compute the annual turnover for a given company
        """
        from endi.models.task import (
            Task,
        )
        query = DBSESSION.query(non_null_sum(Task.ht))
        query = query.filter(Task.company_id == company.id)
        query = query.filter(func.year(Task.date) == year)
        query = query.filter(Task.status == 'valid')

        return query.scalar()

    @classmethod
    def label_query(cls, company_class):
        query = company_class.query()
        query = query.options(load_only('id', 'name', 'active'))
        return query

    @classmethod
    def query_for_select(cls, company_class, only_active=False):
        query = DBSESSION().query(company_class.id, company_class.name)
        if only_active:
            query = query.filter_by(active=True)
        query = query.order_by(company_class.name.asc())
        return query

    @classmethod
    def get_id_by_analytical_account(cls, company_class, analytical_account):
        result = DBSESSION().query(
            company_class.id
        ).filter_by(
            code_compta=analytical_account
        ).filter_by(active=True).first()
        if result is not None:
            result = result[0]
        return result

    @classmethod
    def query_for_select_with_trainer(cls, company_class):
        """
        Build a query suitable for deform select widgets population

        :param class company_class: The Company class
        :returns: A sqlalchemy query object
        """
        from endi.models.user.user import User
        from endi.models.user.login import Login
        from endi.models.user.group import (
            Group,
            USER_GROUPS
        )
        from endi.models.company import COMPANY_EMPLOYEE
        query = cls.query_for_select(company_class).distinct()
        query = query.join(COMPANY_EMPLOYEE)
        query = query.join(User).join(Login).join(USER_GROUPS).join(Group)
        query = query.filter(Group.name == 'trainer')
        return query

    @classmethod
    def has_group_member(cls, company, group_name):
        """
        Check if the company has a trainer in its employees

        :param obj company: A Company instance
        :param str group_name: The name of the group to check for
        :returns: A boolean
        """
        from endi.models.user.user import User
        from endi.models.user.login import Login
        from endi.models.user.group import (
            Group,
            USER_GROUPS
        )
        from endi.models.company import COMPANY_EMPLOYEE

        query = DBSESSION().query(User.id).join(COMPANY_EMPLOYEE)
        query = query.join(Login).join(USER_GROUPS).join(Group)
        query = query.filter(Group.name == group_name)
        query = query.filter(COMPANY_EMPLOYEE.c.company_id == company.id)
        return query.count() > 0

    @classmethod
    def get_employee_ids(cls, company):
        """
        Collect company user_ids
        """
        from endi.models.company import COMPANY_EMPLOYEE
        query = DBSESSION().query(COMPANY_EMPLOYEE.c.account_id).filter(
            COMPANY_EMPLOYEE.c.company_id == company.id
        )
        return [a[0] for a in query]

    @classmethod
    def employs(cls, company, uid):
        """
        Check if the given company employs User with id uid

        :param obj company: The current Company
        :param int uid: The user id
        :rtype: bool
        """
        from endi.models.company import COMPANY_EMPLOYEE
        query = DBSESSION().query(COMPANY_EMPLOYEE)
        query = query.filter(
            COMPANY_EMPLOYEE.c.company_id == company.id,
            COMPANY_EMPLOYEE.c.account_id == uid
        )
        return query.count() > 0

    @classmethod
    def get_cae_contribution(cls, company_id, prefix=''):
        """
        Renvoie la contribution cae

        :param id: Company id
        :param str prefix: configuration key prefix (ex: internal)
        """
        from endi.models.company import Company
        from endi.models.config import Config

        attrname = prefix + 'contribution'

        contribution = DBSESSION().query(
            getattr(Company, attrname)
        ).filter(
            Company.id == company_id
        ).scalar()

        if contribution is None:
            key = 'contribution_cae'
            key = '{}{}'.format(prefix, key)
            contribution = Config.get_value(key, 0, type_=float)
        return contribution

    @classmethod
    def _get_company_sale_product_ids(cls, company):
        from endi.models.sale_product.base import BaseSaleProduct
        return DBSESSION().query(BaseSaleProduct.id).filter_by(
            company_id=company.id
        )

    @classmethod
    def _get_company_unlocked_work_items(cls, company):
        from endi.models.sale_product.work_item import WorkItem
        return WorkItem.query().filter_by(
            locked=False
        ).filter(
            WorkItem.base_sale_product_id.in_(
                cls._get_company_sale_product_ids(company)
            )
        )

    @classmethod
    def _sync_sale_product_field(cls, company, key, old_value, new_value):
        """
        Update company fields with its sale products and work items
            margin_rate (_margin_rate for work items)
            general_overhead (_general_overhead for work items)

        :param obj company: The Company instance
        :param str key: general_overhead/margin_rate
        :param float old_value: The previous value
        :param float new_value: The new value
        """
        if old_value != new_value:
            from endi.models.sale_product.base import BaseSaleProduct
            from endi.models.sale_product.work_item import WorkItem
            filter_ = getattr(BaseSaleProduct, key).in_([None, old_value])
            BaseSaleProduct.query().filter(filter_).filter_by(
                company_id=company.id
            ).update({key: new_value}, synchronize_session='fetch')

            query = cls._get_company_unlocked_work_items(company)

            # WorkItem uses private attributes
            key = '_%s' % key
            filter_ = getattr(WorkItem, key).in_([None, old_value])
            query.filter(filter_).update(
                {key: new_value}, synchronize_session='fetch'
            )

    @classmethod
    def sync_general_overhead(cls, company, old_value, new_value):
        cls._sync_sale_product_field(
            company, 'general_overhead', old_value, new_value
        )

    @classmethod
    def sync_margin_rate(cls, company, old_value, new_value):
        cls._sync_sale_product_field(
            company, 'margin_rate', old_value, new_value
        )

    @classmethod
    def _get_account(cls, instance, account_label, prefix=''):
        """
        Collect the instance's accounting account for the given label

        :param obj instance: the company
        :param str account_label: The account_label like

            third_party_customer
            general_customer
            general_supplier
            third_party_supplier
        """
        from endi.models.config import Config
        label = "%s%s_account" % (prefix, account_label)
        result = getattr(instance, label)

        if not result:
            cae_label = "%scae_%s_account" % (prefix, account_label)
            result = Config.get_value(cae_label, default="")
            print("Label %s value %s" % (cae_label, result))
        return result

    @classmethod
    def get_general_customer_account(cls, instance, prefix=''):
        return cls._get_account(instance, 'general_customer', prefix)

    @classmethod
    def get_third_party_customer_account(cls, instance, prefix=''):
        return cls._get_account(instance, 'third_party_customer', prefix)

    @classmethod
    def get_general_supplier_account(cls, instance, prefix=''):
        return cls._get_account(instance, 'general_supplier', prefix)

    @classmethod
    def get_third_party_supplier_account(cls, instance, prefix=''):
        return cls._get_account(instance, 'third_party_supplier', prefix)

    @classmethod
    def get_business_nested_options(cls, instance):
        """
        Prepare and return a nested structure to allow cascading selection of
        Customer->Project->Business.

        Example return:
        [
          { # Customer level
            'id': 42,
            'label': 'John SA',
            'projects: [
               'id': 1,
               'label: 'Mon dossier',
               'businesses': [
                 {'id': 22, 'label': 'Commande 1'},
                 {'id': 23, 'label': Formation pate à sel'},
               ],
             ]
          },
        ]

        :rtype list:
        :return: nested structure suitable for <select> building
        """
        from endi.models.third_party.customer import Customer
        from endi.models.project.project import Project
        result = []
        customers = Customer.query().filter(
            Customer.company_id == instance.id,
        ).order_by(Customer.label)
        # Limit the number of SQL requests (performance) :
        customers = customers.options(
            selectinload(Customer.projects).selectinload(Project.businesses),
        )

        for customer in customers.all():
            projects_list = []
            for project in customer.projects:
                businesses_list = []
                for business in project.businesses:
                    business_item = {
                        "id": business.id,
                        "label": business.name
                    }
                    businesses_list.append(business_item)
                project_item = {
                    "id": project.id,
                    "label": project.name,
                    "businesses": businesses_list
                }
                projects_list.append(project_item)
            customer_item = {
                "id": customer.id,
                "label": customer.label,
                "projects": projects_list
            }
            result.append(customer_item)
        return result
