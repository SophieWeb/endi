from endi.models.sequence_number import (
    GlobalSequence,
    MonthCompanySequence,
    MonthSequence,
    SequenceNumber,
    YearSequence,
)

from endi.models.services.official_number import AbstractNumberService


class InvoiceNumberService(AbstractNumberService):
    @classmethod
    def get_sequences_map(cls):
        from endi.models.task.task import Task
        seq_kwargs = dict(
            types=['invoice', 'cancelinvoice'],
            model_class=Task,
        )
        return {
            'SEQGLOBAL': GlobalSequence(
                db_key=SequenceNumber.SEQUENCE_INVOICE_GLOBAL,
                init_value_config_key='global_invoice_sequence_init_value',
                **seq_kwargs,
            ),
            'SEQYEAR': YearSequence(
                db_key=SequenceNumber.SEQUENCE_INVOICE_YEAR,
                init_value_config_key='year_invoice_sequence_init_value',
                init_date_config_key='year_invoice_sequence_init_date',
                **seq_kwargs,
            ),
            'SEQMONTH': MonthSequence(
                db_key=SequenceNumber.SEQUENCE_INVOICE_MONTH,
                init_value_config_key='month_invoice_sequence_init_value',
                init_date_config_key='month_invoice_sequence_init_date',
                **seq_kwargs,
            ),
            'SEQMONTHANA': MonthCompanySequence(
                db_key=SequenceNumber.SEQUENCE_INVOICE_MONTH_COMPANY,
                company_init_date_fieldname='month_company_invoice_'
                'sequence_init_date',
                company_init_value_fieldname='month_company_invoice_'
                'sequence_init_value',
                **seq_kwargs,
            ),
        }

    @classmethod
    def is_already_used(cls, node_id, official_number) -> bool:
        # Imported here to avoid circular dependencies
        from endi.models.task import Task
        query = Task.query().filter(
            Task.type_.in_(['invoice', 'cancelinvoice'])
        )
        query = query.filter(
            Task.official_number == official_number,
            Task.id != node_id,
            Task.legacy_number == False,  # noqa E712
        )
        return query.scalar()


class InternalInvoiceNumberService(AbstractNumberService):
    @classmethod
    def get_sequences_map(cls):
        from endi.models.task.task import Task
        seq_kwargs = dict(
            types=['internalinvoice'],
            model_class=Task,
        )
        return {
            'SEQGLOBAL': GlobalSequence(
                db_key=SequenceNumber.SEQUENCE_INTERNALINVOICE_GLOBAL,
                init_value_config_key='global_internalinvoice_sequence'
                '_init_value',
                **seq_kwargs,
            ),
            'SEQYEAR': YearSequence(
                db_key=SequenceNumber.SEQUENCE_INTERNALINVOICE_YEAR,
                init_value_config_key='year_internalinvoice_sequence_'
                'init_value',
                init_date_config_key='year_internalinvoice_sequence_init_date',
                **seq_kwargs,
            ),
            'SEQMONTH': MonthSequence(
                db_key=SequenceNumber.SEQUENCE_INTERNALINVOICE_MONTH,
                init_value_config_key='month_internalinvoice_sequence'
                '_init_value',
                init_date_config_key='month_internalinvoice_sequence_'
                'init_date',
                **seq_kwargs,
            ),
            'SEQMONTHANA': MonthCompanySequence(
                db_key=SequenceNumber.SEQUENCE_INTERNALINVOICE_MONTH_COMPANY,
                **seq_kwargs,
            ),
        }

    @classmethod
    def is_already_used(cls, node_id, official_number) -> bool:
        # Imported here to avoid circular dependencies
        from endi.models.task import Task
        query = Task.query().filter_by(type_='internalinvoice')
        query = query.filter(
            Task.official_number == official_number,
            Task.id != node_id,
            Task.legacy_number == False,  # noqa E712
        )
        return query.scalar()
