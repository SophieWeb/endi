import logging
import datetime
from sqlalchemy import or_
from sqlalchemy.orm import load_only
from endi_base.models.base import DBSESSION
from endi.models.tva import (
    Tva,
    Product,
)
from endi.utils import strings


logger = logging.getLogger(__name__)


def find_internal_product_and_tva():
    from endi.models.tva import Tva, Product
    internal_tva = Tva.get_internal()
    internal_product_id = None
    internal_tva_value = None
    if internal_tva:
        internal_tva_value = internal_tva.value
        internal_products = Product.get_internal()
        if len(internal_products) == 1:
            internal_product_id = internal_products[0].id
    return internal_tva_value, internal_product_id


class TaskService:
    models = None

    @classmethod
    def get_tva_objects(cls, task_obj):
        """
        :param task_obj: The Task object we want to collect tvas for
        :returns: tva stored by amount
        :rtype: dict
        """
        tva_values = set()
        for group in task_obj.line_groups:
            for line in group.lines:
                tva_values.add(line.tva)

        # Cas des certificats énergie
        # (on peut avoir une remise à 0% de tva indépendamment des taux de tva
        # du document)
        for discount in task_obj.discounts:
            tva_values.add(discount.tva)

        tvas = Tva.query().filter(
            Tva.value.in_(list(tva_values))
        ).all()
        return dict([(tva.value, tva) for tva in tvas])

    @classmethod
    def _query_invoices(cls, task_cls, *args, **kwargs):
        """
        Query invoices

        :param **args: List of fields passed to the Task.query method
        :param **kwargs: Other args

            doctypes

                Options to list all invoices, only internal ones or only
                "external" ones (real invoices)
        """
        from endi.models.task import Invoice, CancelInvoice, InternalInvoice

        query = super(task_cls, task_cls).query(*args)

        doctypes = kwargs.get('doctypes', 'all')
        if doctypes == 'all':
            classes = [Invoice, CancelInvoice, InternalInvoice]
            types = ['invoice', 'cancelinvoice', 'internalinvoice']
            query = query.with_polymorphic(classes)
            query = query.filter(task_cls.type_.in_(types))
        elif doctypes == 'internal':
            classes = [InternalInvoice]
            query = query.with_polymorphic(classes)
            query = query.filter(task_cls.type_ == 'internalinvoice')
        else:
            classes = [Invoice, CancelInvoice]
            types = ['invoice', 'cancelinvoice']
            query = query.with_polymorphic(classes)
            query = query.filter(task_cls.type_.in_(types))

        return query

    @classmethod
    def get_valid_invoices(cls, task_cls, *args, **kwargs):
        query = cls._query_invoices(task_cls, *args, **kwargs)
        query = query.filter(task_cls.status == 'valid')
        return query

    @classmethod
    def get_waiting_estimations(cls, *args):
        from endi.models.task import Estimation
        query = Estimation.query(*args)
        query = query.filter(Estimation.status == 'wait')
        query = query.order_by(Estimation.status_date)
        return query

    @classmethod
    def get_waiting_invoices(cls, task_cls, *args):
        query = cls._query_invoices(task_cls, *args)
        query = query.filter(task_cls.status == 'wait')
        query = query.order_by(task_cls.type_).order_by(task_cls.status_date)
        return query

    @classmethod
    def get_task_class(cls):
        raise NotImplementedError("%s.get_task_class" % cls.__name__)

    @classmethod
    def from_price_study(
        cls, price_study, user, customer, **kwargs
    ):
        from endi.models.task.task import TaskLineGroup
        from endi.models.task.task import DiscountLine
        task_line_groups = []
        for product in price_study.products:
            task_line_groups.append(
                TaskLineGroup.from_price_study_product(product)
            )

        task_class = cls.get_customer_task_factory(customer)
        task = task_class(
            user,
            price_study.company,
            project=price_study.project,
            customer=customer,
            financial_year=datetime.date.today().year,
            price_study_id=price_study.id,
            **kwargs
        )
        task.line_groups = task_line_groups
        task.set_default_validity_duration()

        for discount in price_study.discounts:
            for line in DiscountLine.from_price_study_discount(discount):
                task.discounts.append(line)
        return task

    @classmethod
    def sync_with_price_study(cls, task, price_study):
        from endi_base.models.base import DBSESSION
        from endi.models.task.task import TaskLineGroup
        from endi.models.task.task import DiscountLine

        for group in task.line_groups:
            DBSESSION().delete(group)

        for product in price_study.products:
            task.line_groups.append(
                TaskLineGroup.from_price_study_product(product)
            )

        for discount in task.discounts:
            DBSESSION().delete(discount)

        for discount in price_study.discounts:
            for line in DiscountLine.from_price_study_discount(discount):
                task.discounts.append(line)
        return task

    @classmethod
    def find_task_status_date(cls, taskclass, official_number, year):
        """
        Query the database to retrieve a task with the given number and year
        and returns its status_date

        :param str official_number: The official number
        :param int year: The financial year associated to the invoice
        :returns: The document's status_date
        :rtype: datetime.dateime
        """
        from endi.models.task import Invoice, CancelInvoice
        query = DBSESSION().query(taskclass).with_polymorphic(
            [Invoice, CancelInvoice]
        ).options(load_only('status_date')).filter_by(
            official_number=official_number
        )
        if year:
            query = query.filter(
                or_(
                    Invoice.financial_year == year,
                    CancelInvoice.financial_year == year
                )
            )
        return query.one().status_date

    @classmethod
    def has_price_study(cls, task):
        return task.price_study_id is not None

    @classmethod
    def get_price_study(cls, task):
        return task.price_study

    @classmethod
    def format_amount(
            cls, task, amount, trim=True, grouping=True, precision=2
    ):
        """
        Return a formatted amount in the context of the current task

        if the amount is not supposed to be trimmed, we retrieve the Task's
        decimal_to_show and pass it to the format_amount function

        :param obj task: Instance of class <endi.models.task.Task>
        :param int amount: The amount for format
        """
        display_precision = None
        if not trim:
            display_precision = task.decimal_to_display
        return strings.format_amount(
            amount, trim=trim, grouping=grouping, precision=precision,
            display_precision=display_precision
        )

    @classmethod
    def get_customer_task_factory(cls, customer):
        """
        return the appropriate task factory for the given customer
        """
        from endi.models.task import Task
        return Task

    @classmethod
    def duplicate(cls, original, user, **kw):
        """
        Base duplicate tool common for all invoice/estimation types
        """
        customer = kw['customer']
        company = original.company
        factory = cls.get_customer_task_factory(customer)
        document = factory(user=user, company=company, **kw)

        document.decimal_to_display = company.decimal_to_display
        for field in (
            'description',
            'mode',
            'display_units',
            'display_ttc',
            'expenses_ht',
            'workplace',
            'payment_conditions',
            'notes',
            'start_date',
        ):
            value = getattr(original, field)
            setattr(document, field, value)

        return document

    @classmethod
    def post_duplicate(cls, original, created, user, **kw):
        """
        To be called by subclasses
        """
        # On assure qu'on utilise les tvas "internes" lorsque l'on duplique un
        # document 'externe' vers un client interne à la CAE
        if (
            created.customer.is_internal() and
            not original.customer.is_internal()
        ):
            internal_tva_value, internal_product_id = \
                find_internal_product_and_tva()
            for line in created.all_lines:
                line.tva = internal_tva_value
                line.product_id = internal_product_id
        return created

    @staticmethod
    def query_by_validator_id(cls, validator_id: int, query=None):
        from endi.models.status import StatusLogEntry
        if not query:
            query = cls.query()

        query = query.outerjoin(cls.statuses)
        query = query.filter(
            StatusLogEntry.status == "valid",
            StatusLogEntry.state_manager_key == "status"
        )
        query = query.filter(
            StatusLogEntry.user_id == validator_id
        )
        return query


class TaskLineGroupService:
    @classmethod
    def from_price_study_product(cls, group_class, product):
        from endi.models.price_study.product import PriceStudyProduct
        from endi.models.price_study.work import PriceStudyWork

        from endi.models.task.task import TaskLine

        group = group_class()
        if isinstance(product, PriceStudyProduct):
            group.lines = [
                TaskLine.from_price_study_product(product)
            ]
        elif isinstance(product, PriceStudyWork):
            group.title = product.title
            if product.display_details:
                group.description = product.description
                for item in product.items:
                    group.lines.append(
                        TaskLine.from_price_study_work_item(item)
                    )
            else:
                # On crée une seule ligne directement depuis le ProductWork
                group.lines = [
                    TaskLine.from_price_study_work(product)
                ]

        return group

    @classmethod
    def from_sale_product_work(cls, group_class, product, document=None):
        from endi.models.task.task import TaskLine

        group = group_class()
        group.title = product.title
        group.description = product.description
        for item in product.items:
            group.lines.append(
                TaskLine.from_sale_product_work_item(item, document=document)
            )
        return group

    @classmethod
    def gen_cancelinvoice_group(cls, group, invoicing_mode='classic'):
        from endi.models.task import TaskLineGroup
        result = TaskLineGroup(
            title=group.title,
            description=group.description,
            order=group.order,
        )
        DBSESSION().add(result)
        DBSESSION().flush()
        for line in group.lines:
            new_line = line.gen_cancelinvoice_line(invoicing_mode)
            result.lines.append(new_line)

        if invoicing_mode == 'progress':
            from endi.models.progress_invoicing import (
                ProgressInvoicingGroup,
            )
            pgroup = ProgressInvoicingGroup.find_by_task_line_group(group)
            status_group = pgroup.status
            if pgroup.percentage is not None:
                percentage = -1 * pgroup.percentage
            else:
                percentage = None
            ProgressInvoicingGroup.record_invoicing(
                status_group, result.id, percentage
            )
            status_group.update_percent_left(percentage)

        return result

    @classmethod
    def duplicate(cls, group):
        from endi.models.task import TaskLineGroup
        group = TaskLineGroup(
            title=group.title,
            description=group.description,
            task_id=group.task_id,
            lines=[line.duplicate() for line in group.lines],
            order=group.order,
        )
        return group


class TaskLineService:

    @classmethod
    def from_price_study_product(cls, line_class, product):
        from endi.models.tva import Tva
        result = line_class()
        result.description = product.description
        result.cost = product.ht
        result.unity = product.unity
        result.quantity = product.quantity
        if product.tva:
            result.tva = product.tva.value
        else:
            result.tva = Tva.get_default().value
        result.product_id = product.product_id
        return result

    @classmethod
    def from_price_study_work(cls, line_class, product_work):
        from endi.models.tva import Tva
        result = line_class()
        result.description = product_work.description
        result.cost = product_work.ht
        result.unity = product_work.unity
        result.quantity = product_work.quantity

        tva = product_work.find_common_value('tva')
        if tva:
            result.tva = tva.value
        else:
            result.tva = Tva.get_default().value
        result.product_id = product_work.find_common_value('product_id')
        return result

    @classmethod
    def from_price_study_work_item(cls, line_class, work_item):
        from endi.models.tva import Tva
        result = line_class()
        result.description = work_item.description
        result.cost = work_item.ht
        result.unity = work_item.unity
        result.quantity = work_item.total_quantity
        if work_item.tva:
            result.tva = work_item.tva.value
        else:
            result.tva = Tva.get_default().value
        result.product_id = work_item.product_id
        return result

    @classmethod
    def from_sale_product_work_item(cls, line_class, work_item, document=None):
        result = line_class()
        result.description = work_item.description

        if document:
            mode = document.mode
        else:
            mode = 'ht'

        if mode == 'ht':
            result.cost = work_item.ht
        else:
            result.cost = work_item.total_ttc()
        result.mode = mode
        result.unity = work_item.unity
        result.quantity = work_item.quantity
        cls._set_tva_and_product(result, work_item, document)
        return result

    @classmethod
    def from_sale_product(cls, line_class, sale_product, document=None):
        result = line_class()
        result.description = sale_product.description
        print(document)
        if document:
            print('has mode')
            print(document.mode)
            mode = document.mode
        else:
            mode = 'ht'

        if mode == 'ht':
            result.cost = sale_product.ht
        else:
            result.cost = sale_product.ttc()

        result.mode = mode
        result.unity = sale_product.unity

        result.quantity = 1
        cls._set_tva_and_product(result, sale_product, document)
        return result

    @classmethod
    def _set_internal_tva_and_product(cls, task_line):
        """
        Set the internal product or tva
        """
        internal_tva_value, internal_product_id = \
            find_internal_product_and_tva()
        task_line.tva = internal_tva_value
        task_line.product_id = internal_product_id

    @classmethod
    def _set_tva_and_product(
        cls, task_line, sale_product_entry, document=None
    ):
        if document and document.internal:
            cls._set_internal_tva_and_product(task_line)
        else:
            if sale_product_entry.tva:
                task_line.tva = sale_product_entry.tva.value
                task_line.product_id = sale_product_entry.product_id
            else:
                task_line.tva = Tva.get_default().value

            if task_line.tva and not task_line.product_id:
                task_line.product_id = Product.first_by_tva_value(
                    task_line.tva
                )

    @classmethod
    def duplicate(cls, task_line):
        from endi.models.task import TaskLine
        newone = TaskLine(
            order=task_line.order,
            mode=task_line.mode,
            cost=task_line.cost,
            tva=task_line.tva,
            description=task_line.description,
            quantity=task_line.quantity,
            unity=task_line.unity,
            product_id=task_line.product_id,
        )
        return newone

    @classmethod
    def gen_cancelinvoice_line(cls, task_line, invoicing_mode='classic'):
        result = cls.duplicate(task_line)
        result.cost = -1 * result.cost
        if invoicing_mode == 'progress':
            DBSESSION().add(result)
            DBSESSION().flush()
            from endi.models.progress_invoicing import (
                ProgressInvoicingLine,
            )
            pline = ProgressInvoicingLine.find_by_task_line(task_line)
            status_line = pline.status
            # On crée un nouveau groupe pour enregistrer cette étape
            # d'avancement
            ProgressInvoicingLine.record_invoicing(
                status_line, result.id, -1 * pline.percentage
            )
            status_line.update_percent_left(-1 * pline.percentage)
        return result


class DiscountLineService:
    @classmethod
    def from_price_study_discount(cls, price_study_discount):
        from endi.models.task import DiscountLine

        for tva, ht in list(price_study_discount.ht_by_tva().items()):
            result = DiscountLine()
            result.description = price_study_discount.description
            result.amount = ht
            result.tva = tva.value
            yield result


class InternalProcessService:
    """
    Manage the processing of internal documents inside enDI
    """
    @classmethod
    def _generate_pdf(cls, document, request):
        from endi.export.task_pdf import ensure_task_pdf_persisted
        ensure_task_pdf_persisted(document, request)

    @classmethod
    def _generate_supplier(cls, document, request):
        logger.debug("  + Generating the supplier")
        company = document.company
        customer = document.customer
        if not customer.is_internal() or not customer.source_company:
            logger.error(
                "The customer is not internal or the company "
                "is not the right one"
            )
            raise Exception(
                "This document is not attached to an internal customer "
                "(or it has no source company)"
            )
        from endi_base.models.base import DBSESSION
        from endi.models.third_party.supplier import Supplier
        supplier = Supplier.from_company(company, customer.source_company)
        DBSESSION().merge(supplier)
        DBSESSION().flush()
        logger.debug("  + Done")
        return supplier

    @classmethod
    def _generate_supplier_document(cls, document, request, supplier):
        raise NotImplementedError(
            "InternalProcessService._generate_supplier_document"
        )

    @classmethod
    def sync_with_customer(cls, document, request):
        """
        Synchronize the current internal document creating its counterpart in
        the customer's environment

        :param obj request: The PyramidRequest
        :param obj document: The endi internal Task instance
        """
        # We need to enforce the status of the document before generating a PDF
        document.status = 'valid'
        DBSESSION().merge(document)
        DBSESSION().flush()
        cls._generate_pdf(document, request)

        supplier = cls._generate_supplier(document, request)
        supplier_document = cls._generate_supplier_document(
            document, request, supplier
        )
        return supplier_document
