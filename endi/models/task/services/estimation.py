import logging
from .task import (
    InternalProcessService,
    TaskService,
)


logger = logging.getLogger(__name__)


class EstimationService(TaskService):
    @classmethod
    def get_customer_task_factory(cls, customer):
        from endi.models.task import InternalEstimation
        from endi.models.task import Estimation

        if customer.is_internal():
            factory = InternalEstimation
        else:
            factory = Estimation
        return factory

    @classmethod
    def duplicate(cls, original, user, **kw):
        estimation = super(EstimationService, cls).duplicate(
            original, user, **kw
        )

        for field in (
            'deposit',
            'manualDeliverables',
            'paymentDisplay',
            'validity_duration',
        ):
            value = getattr(original, field)
            setattr(estimation, field, value)

        estimation.line_groups = []
        for group in original.line_groups:
            estimation.line_groups.append(group.duplicate())
        for line in original.payment_lines:
            estimation.payment_lines.append(line.duplicate())
        for line in original.discounts:
            estimation.discounts.append(line.duplicate())
        if original.price_study:
            estimation.price_study = original.price_study.duplicate(
                original.project_id, user.id
            )
        estimation.mentions = original.mentions
        cls.post_duplicate(original, estimation, user, **kw)
        return estimation


class EstimationInvoicingService:
    """
    Service managing invoice generation for estimations
    """
    @classmethod
    def _get_invoice_class(cls):
        """
        Return the Invoice class to use
        We need a relative import to avoid circular imports
        """
        from endi.models.task import Invoice
        return Invoice

    @classmethod
    def _get_common_invoice(cls, estimation, user):
        """
        Prepare a new Invoice related to the given estimation

        :param obj estimation: The estimation we're starting from
        :param obj user: The user generating the new document
        :returns: A new Invoice/InternalInvoice
        :rtype: `class:Invoice`
        """
        factory = cls._get_invoice_class()
        invoice = factory(
            user=user,
            company=estimation.company,
            customer=estimation.customer,
            project=estimation.project,
            phase_id=estimation.phase_id,
            estimation=estimation,
            payment_conditions=estimation.payment_conditions,
            description=estimation.description,
            address=estimation.address,
            workplace=estimation.workplace,
            mentions=estimation.mentions,
            business_type_id=estimation.business_type_id,
            notes=estimation.notes,
            price_study_id=estimation.price_study_id,
            mode=estimation.mode,
            display_ttc=estimation.display_ttc,
            start_date=estimation.start_date,
            decimal_to_display=estimation.decimal_to_display,
        )
        return invoice

    @classmethod
    def _get_task_line(cls, cost, description, tva):
        from endi.models.task.task import TaskLine
        from endi.models.tva import Product
        line = TaskLine(
            cost=cost, description=description, tva=tva, quantity=1
        )
        line.product_id = Product.first_by_tva_value(tva)
        return line

    @classmethod
    def _get_deposit_task_line(cls, cost, tva):
        """
            Return an deposit invoiceline
        """
        description = "Facture d'acompte"
        return cls._get_task_line(cost, description, tva)

    @classmethod
    def _get_deposit_task_lines(cls, estimation):
        """
        Return all deposit invoiceline
        """
        lines = []
        for tva, cost in list(estimation.deposit_amounts().items()):
            line = cls._get_deposit_task_line(cost, tva)
            lines.append(line)
        return lines

    @classmethod
    def gen_deposit_invoice(cls, estimation, user):
        """
        Generate a deposit invoice based on the given estimation

        :param obj estimation: The estimation we're starting from
        :param obj user: The user generating the new document
        :returns: A new Invoice / InternalInvoice
        :rtype: `class:Invoice`
        """
        invoice = cls._get_common_invoice(estimation, user)
        invoice.financial_year = invoice.date.year
        invoice.display_units = 0
        invoice.default_line_group.lines.extend(
            cls._get_deposit_task_lines(estimation)
        )
        return invoice

    @classmethod
    def _get_intermediate_invoiceable_amounts(cls, estimation):
        """
        Collect the amounts that should be invoiced in each intermediate
        payment deadline

        :param obj estimation: The estimation we're working on
        :returns: The amounts to be invoiced in form of a list of dict
        [{tva1: 10, tva2: 15}]
        :rtype: list
        """
        if estimation.manualDeliverables == 1:
            # On fait le calcul globale de tous les paiements et on récupère
            # celui que l'on veut
            payments = estimation.manual_payment_line_amounts()[:-1]
        else:
            divided_amount = estimation.paymentline_amounts()
            # All but not the last one (sold)
            num_payments = len(estimation.payment_lines) - 1
            payments = [divided_amount for i in range(num_payments)]
        return payments

    @classmethod
    def _get_intermediate_task_lines(cls, payment_line, payment_description):
        lines = []
        for tva, cost in list(payment_description.items()):
            line = cls._get_task_line(cost, payment_line.description, tva)
            lines.append(line)
        return lines

    @classmethod
    def gen_intermediate_invoice(cls, estimation, payment_line, user):
        """
        Generate an intermediate invoice based on the given payment_line
        definition

        :param obj estimation: The estimation we're starting from
        :param obj payment_line: The PaymentLine describing the invoice
        :param obj user: The user generating the new document
        :returns: A new Invoice/InternalInvoice object
        :rtype: `class:Invoice`
        """
        line_index = estimation.payment_lines[:-1].index(payment_line)

        invoice = cls._get_common_invoice(estimation, user)
        if invoice.date < payment_line.date:
            invoice.date = payment_line.date
        invoice.financial_year = invoice.date.year
        invoice.display_units = 0

        payments = cls._get_intermediate_invoiceable_amounts(estimation)
        payment_description = payments[line_index]

        invoice.default_line_group.lines.extend(
            cls._get_intermediate_task_lines(
                payment_line,
                payment_description,
            )
        )
        return invoice

    @classmethod
    def _get_all_intermediate_invoiceable_task_lines(cls, estimation):
        """
        Build all intermediate invoiceable task lines including the deposit

        :param obj estimation: The estimation we're working on
        :returns: A list with all task lines
        :rtype: list of `class:TaskLine` instances
        """
        payment_descriptions = cls._get_intermediate_invoiceable_amounts(
            estimation
        )
        payments = estimation.payment_lines[:-1]

        result = []
        if estimation.deposit:
            result.extend(cls._get_deposit_task_lines(estimation))

        for payment, description in zip(payments, payment_descriptions):
            result.extend(
                cls._get_intermediate_task_lines(payment, description)
            )
        return result

    @classmethod
    def gen_sold_invoice(cls, estimation, user):
        """
        Generate a sold invoice based on the given estimation definition

        :param obj estimation: The estimation we're starting from
        :param obj user: The user generating the new document
        :returns: A new Invoice/Internal object
        :rtype: `class:Invoice`
        """
        payment_line = estimation.payment_lines[-1]
        invoice = cls._get_common_invoice(estimation, user)

        if invoice.date < payment_line.date:
            invoice.date = payment_line.date
        invoice.financial_year = invoice.date.year
        invoice.display_units = estimation.display_units
        invoice.expenses_ht = estimation.expenses_ht
        line_groups = []

        for group in estimation.line_groups:
            line_groups.append(group.duplicate())

        # Manage alread invoiced lines
        task_lines = cls._get_all_intermediate_invoiceable_task_lines(
            estimation
        )
        if task_lines:
            if len(line_groups) > 1:
                from endi.models.task.task import TaskLineGroup
                already_invoiced_group = TaskLineGroup()
                line_groups.append(already_invoiced_group)
            else:
                already_invoiced_group = line_groups[0]

            task_lines.reverse()
            current_order = len(already_invoiced_group.lines)
            for line in task_lines:
                current_order += 1
                line.cost = -1 * line.cost
                line.order = current_order
                already_invoiced_group.lines.append(line)

        for discount in estimation.discounts:
            invoice.discounts.append(discount.duplicate())

        invoice.line_groups = line_groups
        return invoice


class InternalEstimationService(EstimationService):
    pass


class InternalEstimationInvoicingService(EstimationInvoicingService):
    @classmethod
    def _get_invoice_class(cls):
        """
        Return the Invoice class to use
        We need a relative import to avoid circular imports
        """
        from endi.models.task import InternalInvoice
        return InternalInvoice


class InternalEstimationProcessService(InternalProcessService):
    @classmethod
    def _generate_supplier_document(cls, document, request, supplier):
        logger.debug(
            "  + Generate a supplier order document for {}".format(document.id)
        )
        from endi_base.models.base import DBSESSION
        from endi.models.supply.internalsupplier_order import (
            InternalSupplierOrder,
        )
        order = InternalSupplierOrder.from_estimation(document, supplier)
        order.supplier = supplier
        DBSESSION().add(order)
        file_ = document.pdf_file.duplicate()
        file_.parent_id = order.id
        DBSESSION().merge(file_)
        document.supplier_order = order
        DBSESSION().merge(document)
        DBSESSION().flush()
        logger.debug("  + Done")
        return order
