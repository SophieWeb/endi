import logging

from sqlalchemy import (
    Column,
    ForeignKey,
)
from sqlalchemy.orm import relationship

from .invoice import Invoice

from .services import (
    InternalInvoiceService,
    InternalInvoiceProcessService,
)
from .actions import DEFAULT_ACTION_MANAGER


logger = logging.getLogger(__name__)


class InternalInvoice(Invoice):
    __tablename__ = 'internalinvoice'
    __mapper_args__ = {'polymorphic_identity': 'internalinvoice', }
    invoice_computer = None
    internal = True
    validation_state_manager = DEFAULT_ACTION_MANAGER['internalinvoice']
    _endi_service = InternalInvoiceService
    _internal_process_service = InternalInvoiceProcessService

    id = Column(
        ForeignKey('invoice.id', ondelete='CASCADE'),
        primary_key=True,
        info={'colanderalchemy': {'exclude': True}}
    )
    supplier_invoice_id = Column(
        ForeignKey('internalsupplier_invoice.id', ondelete='SET NULL'),
    )

    # Relationships
    supplier_invoice = relationship(
        'InternalSupplierInvoice',
        back_populates='source_invoice'
    )

    # Template pour les noms des documents
    _number_tmpl = "{s.company.name} {s.date:%Y-%m} FI{s.company_index}"
    _deposit_name_tmpl = "Facture d'acompte {0}"
    _sold_name_tmpl = "Facture de solde {0}"

    def sync_with_customer(self, request):
        return self._internal_process_service.sync_with_customer(self, request)
