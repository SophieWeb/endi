"""
    The estimation model
"""
import datetime
import logging

from zope.interface import implementer

from sqlalchemy import (
    Column,
    Integer,
    BigInteger,
    String,
    ForeignKey,
    Text,
    Boolean,
    Date,
    desc,
)
from sqlalchemy.orm import (
    relationship,
    deferred,
)
from sqlalchemy.ext.orderinglist import ordering_list
from endi_base.models.base import (
    DBBASE,
    default_table_args,
    DBSESSION,
)
from endi.compute.task import EstimationCompute
from endi.compute.math_utils import integer_to_amount
from endi.interfaces import (
    IMoneyTask,
)
from endi.models.config import Config
from .services import (
    EstimationInvoicingService,
    EstimationService,
)
from endi.models.status import StatusLogEntry

from .actions import (
    DEFAULT_ACTION_MANAGER,
    SIGNED_ACTION_MANAGER,
)
from .services import (
    EstimationInvoicingService,
    EstimationService,
)
from .task import Task

logger = logging.getLogger(__name__)


PAYMENTDISPLAYCHOICES = (
    ('NONE', "Les paiements ne sont pas affichés dans le PDF",),
    ('SUMMARY', "Le résumé des paiements apparaît dans le PDF",),
    (
        'ALL',
        "Le détail des paiements apparaît dans le PDF",
    ),
    (
        'ALL_NO_DATE',
        "Le détail des paiements, sans les dates, apparaît dans le PDF",
    ),
)

ESTIMATION_STATES = (
    ('waiting', "En attente"),
    ('send', "Envoyé au client"),
    ('aborted', 'Annulé'),
    ('signed', 'Signé'),
)


@implementer(IMoneyTask)
class Estimation(Task):
    """
        Estimation Model
    """
    __tablename__ = 'estimation'
    __table_args__ = default_table_args
    __mapper_args__ = {'polymorphic_identity': 'estimation', }
    _endi_service = EstimationService
    _invoicing_service = EstimationInvoicingService
    estimation_computer = None

    id = Column(
        ForeignKey('task.id'),
        primary_key=True,
        info={
            'colanderalchemy': {'exclude': True},
        }
    )
    signed_status = Column(
        String(10),
        default='waiting',
        info={
            'colanderalchemy': {
                'title': 'Statut du devis',
            }
        }
    )
    geninv = Column(
        Boolean(),
        default=False,
        info={
            'colanderalchemy': {"title": "Factures générées ?"},
        }
    )
    # common with only invoices
    deposit = Column(
        Integer,
        info={
            'colanderalchemy': {'title': 'Accompte (en %)'},
        },
        default=0
    )
    manualDeliverables = deferred(
        Column(
            Integer,
            info={
                'colanderalchemy': {
                    'title': "Configuration manuelle des paiements"
                }
            },
            default=0,
        ),
        group='edit',
    )
    paymentDisplay = deferred(
        Column(
            String(20),
            info={
                'colanderalchemy': {'title': "Affichage des paiements"},
            },
            default=PAYMENTDISPLAYCHOICES[0][0]
        ),
        group='edit'
    )
    validity_duration = Column(
        String(50),
        info={
            'colanderalchemy': {
                'title': 'Limite de validité du devis',
            }
        }
    )

    payment_lines = relationship(
        "PaymentLine",
        order_by='PaymentLine.order',
        cascade="all, delete-orphan",
        back_populates='task',
        collection_class=ordering_list('order'),
        info={
            'colanderalchemy': {'title': "Échéances de paiement"},
        }
    )
    invoices = relationship(
        "Invoice",
        primaryjoin="Estimation.id==Invoice.estimation_id",
        order_by="Invoice.date",
        back_populates="estimation",
        info={
            'colanderalchemy': {'exclude': True},
        }
    )
    validation_state_manager = DEFAULT_ACTION_MANAGER['estimation']
    signed_state_manager = SIGNED_ACTION_MANAGER
    _number_tmpl = "{s.company.name} {s.date:%Y-%m} D{s.company_index}"

    def _get_project_index(self, project):
        """
        Return the index of the current object in the associated project
        :param obj project: A Project instance in which we will look to get the
        current doc index
        :returns: The next number
        :rtype: int
        """
        return project.get_next_estimation_index()

    def _get_company_index(self, company):
        """
        Return the index of the current object in the associated company
        :param obj company: A Company instance in which we will look to get the
        current doc index
        :returns: The next number
        :rtype: int
        """
        return company.get_next_estimation_index()

    def set_signed_status(self, status, request, **kw):
        """
        set the signed status of a task through the state machine
        """
        if request.user.id is not None:
            status_record = StatusLogEntry(
                status=status,
                user=request.user,
                comment="",
                state_manager_key='signed_status',
            )
            self.statuses.append(status_record)

        return self.signed_state_manager.process(
            status,
            self,
            request,
            **kw
        )

    def check_signed_status_allowed(self, status, request, **kw):
        return self.signed_state_manager.check_allowed(status, self, request)

    def gen_business(self):
        """
        Generate a business based on this Task

        :returns: A new business instance
        :rtype: :class:`endi.models.project.business.Business`
        """
        business = Task.gen_business(self)
        business.populate_deadlines(self)
        business.populate_indicators()
        return business

    def __repr__(self):
        return (
            "<{s.__class__.__name__} id:{s.id} ({s.status})>".format(s=self)
        )

    def __json__(self, request):
        if self.manualDeliverables == 1:
            payment_times = -1
        else:
            payment_times = len(self.payment_lines)

        result = Task.__json__(self, request)
        result.update(
            dict(
                deposit=self.deposit,
                manual_deliverables=self.manualDeliverables,
                manualDeliverables=self.manualDeliverables,
                paymentDisplay=self.paymentDisplay,
                validity_duration=self.validity_duration,
                payment_times=payment_times,
                payment_lines=[
                    line.__json__(request)
                    for line in self.payment_lines
                ]
            )
        )
        return result

    def gen_deposit_invoice(self, user):
        """
        Generate a deposit invoice based on the current estimation

        :param obj user: User instance, the user generating the document
        :rtype: `class:Invoice`
        """
        return self._invoicing_service.gen_deposit_invoice(self, user)

    def gen_invoice(self, payment_line, user):
        """
        Generate an invoice based on a payment line

        :param obj payment_line: The payment line we ask an Invoice for
        :param obj user: User instance, the user generating the document
        :rtype: `class:Invoice`
        """
        if payment_line == self.payment_lines[-1]:
            self.geninv = True
            DBSESSION().merge(self)
            return self._invoicing_service.gen_sold_invoice(self, user)
        else:
            return self._invoicing_service.gen_intermediate_invoice(
                self, payment_line, user
            )

    def add_default_payment_line(self):
        self.payment_lines = [PaymentLine(description='Solde', amount=0)]
        return self

    @property
    def global_status(self):
        """
        hook on status and paid status to update css classes representing icons
        :return: a String
        """
        if self.signed_status == 'aborted':
            return 'closed'
        return self.status

    def _get_estimation_computer(self):
        """
        Return needed compute class depending on mode value
        :return: an instance of EstimationCompute or EstimationCompute ttc
        """
        if self.estimation_computer is None:
            self.estimation_computer = EstimationCompute(self)
        return self.estimation_computer

    def get_default_tva(self):
        return self._get_estimation_computer().get_default_tva()

    def deposit_amounts(self):
        return self._get_estimation_computer().deposit_amounts()

    def deposit_amount(self):
        return self._get_estimation_computer().deposit_amount()

    def get_nb_payment_lines(self):
        return self._get_estimation_computer().get_nb_payment_lines()

    def paymentline_amounts(self):
        return self._get_estimation_computer().paymentline_amounts()

    def manual_payment_line_amounts(self):
        return self._get_estimation_computer().manual_payment_line_amounts()

    def deposit_amount_ttc(self):
        return self._get_estimation_computer().deposit_amount_ttc()

    def paymentline_amount_ttc(self):
        return self._get_estimation_computer().paymentline_amount_ttc()

    def sold(self):
        return self._get_estimation_computer().sold()

    def set_default_validity_duration(self):
        """
        Set last validity duration used by the company
        or CAE default config if none
        """
        default = Config.get_value("estimation_validity_duration_default")
        query = DBSESSION().query(Estimation.validity_duration)
        query = query.filter(Estimation.company_id == self.company_id)
        query = query.filter(Estimation.validity_duration != None)  # noqa
        query = query.order_by(desc(Estimation.id)).limit(1)
        last_duration = query.scalar()
        if last_duration:
            default = last_duration
        self.validity_duration = default


class PaymentLine(DBBASE):
    """
        payments lines
    """
    __tablename__ = 'estimation_payment'
    __table_args__ = default_table_args
    id = Column(
        Integer,
        primary_key=True,
        nullable=False,
    )
    task_id = Column(
        Integer,
        ForeignKey('estimation.id', ondelete="cascade"),
        info={
            'colanderalchemy': {
                'title': "Identifiant du document",
            }
        },
    )
    order = Column(
        Integer,
        info={'colanderalchemy': {'title': "Ordre"}},
        default=1
    )
    description = Column(
        Text,
        info={'colanderalchemy': {'title': "Description"}},
    )
    amount = Column(
        BigInteger(),
        info={'colanderalchemy': {'title': "Montant"}},
    )
    date = Column(
        Date(),
        info={'colanderalchemy': {"title": "Date"}},
        default=datetime.date.today
    )
    task = relationship(
        "Estimation",
        info={'colanderalchemy': {'exclude': True}},
    )

    def duplicate(self):
        """
            duplicate a paymentline
        """
        return PaymentLine(
            order=self.order,
            amount=self.amount,
            description=self.description,
            date=datetime.date.today(),
        )

    def __repr__(self):
        return "<PaymentLine id:{s.id} task_id:{s.task_id} amount:{s.amount}\
 date:{s.date}".format(s=self)

    def __json__(self, request):
        return dict(
            id=self.id,
            order=self.order,
            index=self.order,
            description=self.description,
            cost=integer_to_amount(self.amount, 5),
            amount=integer_to_amount(self.amount, 5),
            date=self.date.isoformat(),
            task_id=self.task_id,
        )
