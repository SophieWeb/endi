from sqlalchemy import (
    Column,
    ForeignKey,
)
from sqlalchemy.orm import relationship

from .supplier_invoice import (
    SupplierInvoice,
    SupplierInvoiceLine,
)
from .actions import ACTION_MANAGER


class InternalSupplierInvoice(SupplierInvoice):
    __tablename__ = 'internalsupplier_invoice'
    __mapper_args__ = {'polymorphic_identity': 'internalsupplier_invoice'}
    internal = True

    validation_state_manager = ACTION_MANAGER['internalsupplier_invoice']

    id = Column(
        ForeignKey('supplier_invoice.id'),
        primary_key=True,
        info={"colanderalchemy": {'exclude': True}},
    )

    # relationship
    source_invoice = relationship(
        "InternalInvoice",
        uselist=False,
        back_populates="supplier_invoice"
    )
    validation_state_manager = ACTION_MANAGER['internalsupplier_invoice']

    @classmethod
    def from_invoice(cls, invoice, supplier):
        """
        Create an instance based on the given invoice
        """
        instance = cls(
            name="Facture {}".format(invoice.internal_number)
        )
        instance.supplier = supplier
        instance.company = invoice.customer.source_company

        instance.lines.append(SupplierInvoiceLine.from_task(invoice))
        return instance
