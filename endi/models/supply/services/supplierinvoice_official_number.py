from endi.models.sequence_number import (
    GlobalSequence,
    MonthCompanySequence,
    MonthSequence,
    SequenceNumber,
    YearSequence,
)

from endi.models.services.official_number import AbstractNumberService


class SupplierInvoiceNumberService(AbstractNumberService):
    @classmethod
    def get_sequences_map(cls):
        from endi.models.supply.supplier_invoice import SupplierInvoice
        seq_kwargs = dict(
            types=['supplier_invoice'],
            model_class=SupplierInvoice,
        )
        return {
            'SEQGLOBAL': GlobalSequence(
                db_key=SequenceNumber.SEQUENCE_SUPPLIERINVOICE_GLOBAL,
                init_value_config_key='global_supplierinvoice_sequence_'
                'init_value',
                **seq_kwargs,
            ),
            'SEQYEAR': YearSequence(
                db_key=SequenceNumber.SEQUENCE_SUPPLIERINVOICE_YEAR,
                init_value_config_key='year_supplierinvoice_sequence_'
                'init_value',
                init_date_config_key='year_supplierinvoice_sequence_'
                'init_date',
                **seq_kwargs,
            ),
            'SEQMONTH': MonthSequence(
                db_key=SequenceNumber.SEQUENCE_SUPPLIERINVOICE_MONTH,
                init_value_config_key='month_supplierinvoice_sequence_'
                'init_value',
                init_date_config_key='month_supplierinvoice_sequence_'
                'init_date',
                **seq_kwargs,
            ),
            'SEQMONTHANA': MonthCompanySequence(
                db_key=SequenceNumber.SEQUENCE_SUPPLIERINVOICE_MONTH_COMPANY,
                **seq_kwargs,
            ),
        }

    @classmethod
    def is_already_used(cls, node_id, official_number) -> bool:
        # Imported here to avoid circular dependencies
        from endi.models.supply.supplier_invoice import SupplierInvoice

        doc = SupplierInvoice.get(node_id)

        query = SupplierInvoice.query().filter(
            SupplierInvoice.official_number == official_number,
            SupplierInvoice.id != node_id,
            SupplierInvoice.type_ == doc.type_,
        )
        return query.scalar()


class InternalSupplierInvoiceNumberService(AbstractNumberService):
    @classmethod
    def get_sequences_map(cls):
        from endi.models.supply.internalsupplier_invoice import (
            InternalSupplierInvoice,
        )
        seq_kwargs = dict(
            types=['internalsupplier_invoice'],
            model_class=InternalSupplierInvoice,
        )
        return {
            'SEQGLOBAL': GlobalSequence(
                db_key=SequenceNumber.SEQUENCE_INTERNALSUPPLIERINVOICE_GLOBAL,
                init_value_config_key='global_internalsupplierinvoice_sequence'
                '_init_value',
                **seq_kwargs,
            ),
            'SEQYEAR': YearSequence(
                db_key=SequenceNumber.SEQUENCE_INTERNALSUPPLIERINVOICE_YEAR,
                init_value_config_key='year_internalsupplierinvoice_sequence'
                '_init_value',
                init_date_config_key='year_internalsupplierinvoice_sequence'
                '_init_date',
                **seq_kwargs,
            ),
            'SEQMONTH': MonthSequence(
                db_key=SequenceNumber.SEQUENCE_INTERNALSUPPLIERINVOICE_MONTH,
                init_value_config_key='month_internalsupplierinvoice_sequence'
                '_init_value',
                init_date_config_key='month_internalsupplierinvoice_sequence'
                '_init_date',
                **seq_kwargs,
            ),
            'SEQMONTHANA': MonthCompanySequence(
                db_key=SequenceNumber.SEQUENCE_INTERNALSUPPLIERINVOICE_MONTH_COMPANY,  # noqa: E501
                company_init_date_fieldname='month_company_'
                'internalsupplierinvoice_sequence_init_date',
                company_init_value_fieldname='month_company_'
                'internalsupplierinvoice_sequence_init_value',
                **seq_kwargs,
            ),
        }

    @classmethod
    def is_already_used(cls, node_id, official_number) -> bool:
        # Imported here to avoid circular dependencies
        from endi.models.supply.internalsupplier_invoice import (
            InternalSupplierInvoice,
        )

        query = InternalSupplierInvoice.query().filter(
            InternalSupplierInvoice.official_number == official_number,
            InternalSupplierInvoice.id != node_id,
        )
        return query.scalar()
