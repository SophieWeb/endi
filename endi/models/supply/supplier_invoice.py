import datetime

from beaker.cache import cache_region
from sqlalchemy import (
    Boolean,
    Column,
    ForeignKey,
    Integer,
    DateTime,
    extract,
)
from sqlalchemy.orm import (
    relationship,
)

from endi_base.models.base import (
    DBBASE,
    DBSESSION,
    default_table_args,
)
from endi_base.models.mixins import OfficialNumberMixin
from endi_base.utils.date import (
    format_date,
)
from endi.compute.math_utils import (
    integer_to_amount,
)
from endi.compute.supplier_invoice import (
    SupplierInvoiceCompute,
    SupplierInvoiceLineCompute,
)
from endi.exception import BadRequest
from endi.utils import strings
from endi.models.node import Node
from endi.models.status import (
    status_column,
    PaidStatusHolderMixin,
    ValidationStatusHolderMixin,
)
from endi.models.project.mixins import BusinessLinkedModelMixin
from endi.models.supply.mixins import LineModelMixin

from .services.supplier_invoice import (
    SupplierInvoiceService,
    SupplierInvoiceLineService,
)
from .actions import ACTION_MANAGER


class SupplierInvoice(
    OfficialNumberMixin,
    SupplierInvoiceCompute,
    ValidationStatusHolderMixin,
    PaidStatusHolderMixin,
    Node,
):
    """
    A supplier invoice is linked :
    - 1..n SupplierOrder (constraint : same supplier and same percentage)
    - 1..n attachments (can map to multiple invoices form the supplier)
    - 0..n payments
    - 0..n lines (But that has little meaning with zero)
    """
    __tablename__ = 'supplier_invoice'
    __table_args__ = default_table_args
    __mapper_args__ = {'polymorphic_identity': 'supplier_invoice'}
    internal = False
    _endi_service = SupplierInvoiceService
    validation_date_column = 'status_date'

    # Attributes
    id = Column(
        ForeignKey('node.id'),
        primary_key=True,
        info={"colanderalchemy": {'exclude': True}},
    )

    date = Column(
        DateTime(),
        default=datetime.datetime.now,
        info={'colanderalchemy': {'title': "Date de la facture"}},
    )

    supplier_id = Column(
        Integer,
        ForeignKey("supplier.id"),
        info={
            'export': {'exclude': True},
        },
    )

    cae_percentage = Column(
        Integer,
        default=100,
        info={
            'colanderalchemy': {
                'title': 'pourcentage décaissé par la CAE'
            },
        },
    )

    company_id = Column(
        Integer,
        ForeignKey('company.id'),
        info={
            'export': {'exclude': True},
            'colanderalchemy': {'exclude': True},
        },
        nullable=False,
    )

    exported = Column(
        Boolean(),
        info={
            'colanderalchemy': {
                "title": "A déjà été exportée vers le logiciel de "
                "comptabilité ?"
            }
        },
        default=False
    )

    # Relationships
    supplier = relationship(
        "Supplier",
        primaryjoin="Supplier.id==SupplierInvoice.supplier_id",
        back_populates="invoices",
        info={
            'colanderalchemy': {'exclude': True},
            'export': {'exclude': True},
        }
    )

    payments = relationship(
        "BaseSupplierInvoicePayment",
        primaryjoin=(
            'SupplierInvoice.id==BaseSupplierInvoicePayment.'
            'supplier_invoice_id'
        ),
        back_populates='supplier_invoice',
        info={
            'colanderalchemy': {'exclude': True},
            'export': {'exclude': True},
        }
    )

    @property
    def cae_payments(self):
        from endi.models.supply.payment import SupplierInvoiceSupplierPayment
        return [
            p for p in self.payments
            if isinstance(p, SupplierInvoiceSupplierPayment)
        ]

    @property
    def user_payments(self):
        from endi.models.supply.payment import SupplierInvoiceUserPayment
        return [
            p for p in self.payments
            if isinstance(p, SupplierInvoiceUserPayment)
        ]

    # Payments history is shared through paid_status_history
    # So they are not « full-featured statuses »
    worker_paid_status = status_column(default='waiting')
    supplier_paid_status = status_column(default='waiting')

    payer_id = Column(
        Integer,
        ForeignKey("accounts.id"),
    )
    payer = relationship(
        "User",
        primaryjoin="SupplierInvoice.payer_id==User.id",
        info={
            'colanderalchemy': {'exclude': True},
        },
    )

    company = relationship(
        "Company",
        primaryjoin="Company.id==SupplierInvoice.company_id",
        info={
            'colanderalchemy': {'exclude': True},
            'export': {'exclude': True},
        }
    )

    lines = relationship(
        "SupplierInvoiceLine",
        cascade="all, delete-orphan",
        order_by="SupplierInvoiceLine.id",
        info={
            "colanderalchemy": {
                "title": "Entrées",
                "description": "Vous pouvez soit lister le détail de "
                + "votre facture soit vous contenter d'un total global.",
            }
        },
        back_populates='supplier_invoice',
    )

    validation_state_manager = ACTION_MANAGER['supplier_invoice']

    @property
    def global_status(self) -> str:
        """
        Combine .paid_status and .status
        """
        if self.paid_status == 'paid':  # fully paid = resulted
            return 'partial_unpaid'
        else:
            return self.status

    @property
    def supplier_label(self):
        if self.supplier is not None:
            return self.supplier.label
        else:
            return 'indéfini'

    def record_payment(self, payment, force_resulted=False):
        super().record_payment(payment, force_resulted)
        self.check_supplier_resulted(force_resulted)
        self.check_worker_resulted(force_resulted)

    def check_worker_resulted(self, force_resulted: bool):
        self.worker_paid_status = self.get_resulted(
            force_resulted,
            self.worker_topay(),
            self.user_payments,
            self.worker_total
        )

    def check_supplier_resulted(self, force_resulted: bool):
        self.supplier_paid_status = self.get_resulted(
            force_resulted,
            self.cae_topay(),
            self.cae_payments,
            self.cae_total,
        )

    def import_lines_from_order(self, supplier_order):
        """
        Copies all lines from a SupplierOrder
        """
        return self._endi_service.import_lines(
            dest_line_factory=SupplierInvoiceLine,
            src_obj=supplier_order,
            dest_obj=self,
            source_id_attr='source_supplier_order_line_id'
        )

    def get_default_name(self):
        return 'Facture {} du {}'.format(self.supplier.company_name, self.date)

    @classmethod
    def filter_by_year(cls, query, year):
        return cls._endi_service.filter_by_year(cls, query, year)

    def check_validation_status_allowed(self, status, request, **kw):
        # We should use indicators if we get more complicated
        if status in ('valid', 'wait'):
            if self.supplier is None:
                raise BadRequest(
                    "Impossible de valider une facture fournisseur"
                    " dont le fournisseur n'est pas renseigné."
                )

            if self.payer is None and self.cae_percentage < 100:
                raise BadRequest(
                    "Impossible de valider une facture fournisseur avec une "
                    "avance entrepreneur dont "
                    "l'entrepreneur n'est pas renseigné."
                )

        return self.validation_state_manager.check_allowed(
            status,
            self,
            request,
        )

    # FIXME: factorize ?
    def set_validation_status(self, status, request, **kw):
        return self.validation_state_manager.process(
            status,
            self,
            request,
            **kw
        )

    def get_company_id(self):
        # for company detection in menu display
        return self.company_id

    def get_company(self):
        # for dashboard
        return self.company

    # FIXME: use StatusLogEntry (as invoice payment) for logging ?

    def __json__(self, request):
        return dict(
            id=self.id,
            date=self.date,
            name=self.name,
            created_at=self.created_at.isoformat(),
            updated_at=self.updated_at.isoformat(),
            company_id=self.company_id,
            payer_id=self.payer_id,
            payer_name=self.payer.label if self.payer_id else '',
            paid_status=self.paid_status,
            # justified=self.justified,
            status=self.status,
            status_user_id=self.status_user_id,
            status_date=self.status_date.isoformat(),

            # From .supplier_orders
            orders_total=integer_to_amount(self.orders_total),
            orders_cae_total=integer_to_amount(self.orders_cae_total),
            orders_worker_total=integer_to_amount(self.orders_worker_total),
            orders_total_ht=integer_to_amount(self.orders_total_ht),
            orders_total_tva=integer_to_amount(self.orders_total_tva),
            cae_percentage=self.cae_percentage,

            supplier_name=self.supplier_label,
            supplier_id=self.supplier_id,
            lines=[line.__json__(request) for line in self.lines],
            payments=[payment.__json__(request) for payment in self.payments],
            cae_payments=[
                payment.__json__(request) for payment in self.cae_payments
            ],
            user_payments=[
                payment.__json__(request) for payment in self.user_payments
            ],
            attachments=[
                f.__json__(request)for f in self.children if f.type_ == 'file'
            ],
            supplier_orders=[order.id for order in self.supplier_orders],
        )


class SupplierInvoiceLine(
    LineModelMixin,
    BusinessLinkedModelMixin,
    DBBASE,
    SupplierInvoiceLineCompute,
):
    __tablename__ = 'supplier_invoice_line'
    __table_args__ = default_table_args
    _endi_service = SupplierInvoiceLineService
    parent_model = SupplierInvoice

    id = Column(
        Integer,
        primary_key=True,
        info={"colanderalchemy": {'exclude': True}},
    )

    supplier_invoice_id = Column(
        Integer,
        ForeignKey("supplier_invoice.id", ondelete="cascade"),
        nullable=False,
        info={'colanderalchemy': {'exclude': True}}
    )

    supplier_invoice = relationship(
        "SupplierInvoice",
        primaryjoin="SupplierInvoice.id==SupplierInvoiceLine."
        "supplier_invoice_id",
        info={
            'colanderalchemy': {'exclude': True},
            'export': {'exclude': True},
        },
        back_populates='lines'
    )
    source_supplier_order_line_id = Column(
        Integer,
        ForeignKey("supplier_order_line.id", ondelete="SET NULL"),
        nullable=True,  # NULL when created by hand
        info={'colanderalchemy': {'exclude': True}}
    )
    source_supplier_order_line = relationship(
        "SupplierOrderLine",
        info={
            'colanderalchemy': {'exclude': True},
            'export': {'exclude': True},
        }
    )

    expense_type = relationship(
        "ExpenseType",
        uselist=False,
        info={'colanderalchemy': {'exclude': True}}
    )

    def long_label(self):
        return '{} {}€ ({}) − {}'.format(
            self.description,
            strings.format_amount(self.total, grouping=False),
            self.expense_type.label,
            format_date(self.supplier_invoice.date),
        )

    @classmethod
    def linkable(cls, business):
        return cls._endi_service.linkable(cls, business)

    def __json__(self, request):
        ret = super(SupplierInvoiceLine, self).__json__(request)
        ret.update(dict(
            supplier_invoice_id=self.supplier_invoice_id,
        ))
        ret.update(dict(
            BusinessLinkedModelMixin.__json__(self, request),
        ))
        return ret


def get_supplier_invoices_years(kw=None):
    """
    Return a cached query for the years we have invoices configured

    :param kw: is here only for API compatibility
    """
    @cache_region("long_term", "supplier_invoices_years")
    def years():
        """
            return the distinct financial years available in the database
        """
        query = DBSESSION().query(
            extract('year', SupplierInvoice.date).distinct()
        )
        query = query.order_by(SupplierInvoice.date)
        years = [year[0] for year in query]
        current = datetime.date.today().year
        if current not in years:
            years.append(current)
        return years
    return years()
