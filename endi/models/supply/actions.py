from endi_base.models.base import DBSESSION

from endi.models.action_manager import get_validation_state_manager
from endi.models.config import Config
from .services.supplierinvoice_official_number import (
    SupplierInvoiceNumberService,
    InternalSupplierInvoiceNumberService,
)


def internalsupplier_order_valid_callback(
    request, supplier_order, *args, **kwargs
):
    """
    Callback launched after an internal supplier order is validated
    send an email to the supplier
    """
    supplier_order.source_estimation.set_signed_status('signed', request)
    DBSESSION().merge(supplier_order.source_estimation)
    DBSESSION().flush()


def _set_supplier_invoice_official_number(
    request,
    supplier_invoice,
    *args,
    **kwargs
):
    """
    Callback for when sheet turns into valid status
    """
    template = Config.get_value('supplierinvoice_number_template', None)

    assert template is not None, \
        'supplierinvoice_number_template setting should be set'

    if supplier_invoice.official_number is None:
        SupplierInvoiceNumberService.assign_number(supplier_invoice, template)


def _set_internalsupplier_invoice_official_number(
    request,
    supplier_invoice,
    *args,
    **kwargs
):
    """
    Callback for when sheet turns into valid status
    """
    template = Config.get_value(
        'internalsupplierinvoice_number_template', None
    )

    assert template is not None, \
        'internalsupplierinvoice_number_template setting should be set'

    if supplier_invoice.official_number is None:
        InternalSupplierInvoiceNumberService.assign_number(
            supplier_invoice, template
        )


def get_internal_supplier_order_state_manager():
    manager = get_validation_state_manager(
        'supplier_order',
        userid_attr='status_user_id',
        callbacks=dict(valid=internalsupplier_order_valid_callback)
    )
    for item in manager.items:
        item.options['help_text'] = (
            "La validation de cette commande vaut acceptation du devis "
            "associé. Un e-mail de confirmation sera envoyé au fournisseur."
        )
    return manager


ACTION_MANAGER = {
    'supplier_order': get_validation_state_manager(
        'supplier_order',
        userid_attr='status_user_id',
    ),
    'internalsupplier_order': get_internal_supplier_order_state_manager(),
    'supplier_invoice': get_validation_state_manager(
        'supplier_invoice',
        userid_attr='status_user_id',
        callbacks=dict(valid=_set_supplier_invoice_official_number)
    ),
    'internalsupplier_invoice': get_validation_state_manager(
        'supplier_invoice',
        userid_attr='status_user_id',
        callbacks=dict(valid=_set_internalsupplier_invoice_official_number)
    ),
}
