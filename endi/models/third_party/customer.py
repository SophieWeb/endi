"""
    Customer model
"""
import logging

from sqlalchemy import (Column, ForeignKey)
from sqlalchemy.orm import relationship
from endi_base.models.base import default_table_args
from .third_party import ThirdParty
from endi.models.project.project import ProjectCustomer
from .services.customer import CustomerService

log = logging.getLogger(__name__)


class Customer(ThirdParty):
    """
        Customer model
    """
    __tablename__ = 'customer'
    __table_args__ = default_table_args
    __mapper_args__ = {'polymorphic_identity': 'customer', }
    _endi_service = CustomerService

    id = Column(
        ForeignKey('third_party.id'),
        primary_key=True,
        info={
            'colanderalchemy': {'exclude': True},
        }
    )

    company = relationship(
        "Company",
        primaryjoin="Company.id==Customer.company_id",
        back_populates='customers',
        info={
            'colanderalchemy': {'exclude': True},
            'export': {'exclude': True},
        }
    )

    projects = relationship(
        "Project",
        back_populates="customers",
        secondary=ProjectCustomer,
        info={'colanderalchemy': {'exclude': True}},
    )

    estimations = relationship(
        "Estimation",
        primaryjoin="Estimation.customer_id==Customer.id",
        info={
            'colanderalchemy': {'exclude': True},
            'export': {'exclude': True},
        }
    )

    invoices = relationship(
        "Invoice",
        primaryjoin="Invoice.customer_id==Customer.id",
        info={
            'colanderalchemy': {'exclude': True},
            'export': {'exclude': True},
        }
    )

    cancelinvoices = relationship(
        "CancelInvoice",
        primaryjoin="CancelInvoice.customer_id==Customer.id",
        info={
            'colanderalchemy': {'exclude': True},
            'export': {'exclude': True},
        }
    )

    def __json__(self, request):
        """
            :returns: a dict version of the customer object
        """
        projects = [
            project.__json__(request)
            for project in self.projects if not project.archived
        ]
        return dict(
            id=self.id,
            code=self.code,
            comments=self.comments,
            tva_intracomm=self.tva_intracomm,
            registration=self.registration,
            address=self.address,
            zip_code=self.zip_code,
            city=self.city,
            country=self.country,
            phone=self.phone,
            email=self.email,
            lastname=self.lastname,
            firstname=self.firstname,
            name=self.name,
            projects=projects,
            full_address=self.full_address,
            archived=self.archived,
            company_id=self.company_id,
        )

    def has_tasks(self):
        return self._endi_service.count_tasks(self) > 0

    def is_deletable(self):
        """
            Return True if this project could be deleted
        """
        return self.archived and not self.has_tasks()

    @classmethod
    def check_project_id(cls, customer_id, project_id):
        """
        Check if the project and the customer are linked

        :param int customer_id: The customer id
        :param int project_id: The project id
        :returns: True if the customer is attached to the project
        :rtype: bool
        """
        return cls._endi_service.check_project_id(customer_id, project_id)

    def get_project_ids(self):
        return self._endi_service.get_project_ids(self)

    def get_total_expenses(self):
        return self._endi_service.get_total_expenses(self)

    def get_total_income(self):
        return self._endi_service.get_total_income(self)
