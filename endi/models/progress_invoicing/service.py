import logging

from sqlalchemy import not_
from endi_base.models.base import DBSESSION
from endi.compute import math_utils


logger = logging.getLogger(__name__)


class ProgressInvoicingBaseStatusService:

    @classmethod
    def update_percent_left(cls, status, ui_percentage, ref_percentage=None):
        """
        Update the percent left value removing the ui_percentage value and
        using ref_percentage as reference

        :param obj status: ProgressInvoicingLineStatus /
        ProgressInvoicingGroupStatus
        :param float ui_percentage: The percentage to set
        :param float ref_percentage: The percentage that was still to invoice
        before this one
        """
        logger.debug("Update percent left for {}".format(status))
        if ref_percentage is None:
            ref_percentage = status.percent_left

        logger.debug("Ref percentage {}".format(ref_percentage))
        logger.debug("Percentage to add {}".format(ui_percentage))
        if ref_percentage is not None:
            status.percent_left = ref_percentage - ui_percentage
            DBSESSION().merge(status)
        else:
            logger.error(
                "We try to update the percent_left value while it's already "
                "void"
            )

    @classmethod
    def current_percent_left(cls, status):
        """
        Compute the percent left regarding the current status (also when an
        invoice is currently edited)

        :rtype: float or None
        """
        return status.percent_left


class ProgressInvoicingLineStatusService(ProgressInvoicingBaseStatusService):
    """
    Manage the invoicing of the part of a source task line
    """
    @classmethod
    def _total_ht(cls, status):
        """
        Total ht managed by this progress_invoicing status
        """
        result = status.source_task_line.total_ht()
        return result

    @classmethod
    def total_ht_to_invoice(cls, status):
        """
        Return the total ht to invoice

        :param obj status: The ProgressInvoicingLineStatus/
        ProgressInvoicingGroupStatus

        :returns: The total ht to invoice in *10^5 format
        :rtype: int
        """
        return math_utils.percentage(
            cls._total_ht(status),
            status.percent_to_invoice
        )

    @classmethod
    def _total_tva(cls, status):
        """
        Total tva managed by this progress_invoicing status
        """
        return status.source_task_line.tva_amount()

    @classmethod
    def tva_to_invoice(cls, status):
        """
        Compute the tva amount to invoice

        :param obj status: The ProgressInvoicingLineStatus /
        ProgressInvoicingGroupStatus

        :returns: The total tva to invoice in *10^5 format
        :rtype: int
        """
        source_line_tva = cls._total_tva(status)
        return math_utils.percentage(
            source_line_tva,
            status.percent_to_invoice
        )

    @classmethod
    def _total_ttc(cls, status):
        """
        Total ttc managed by this progress_invoicing status
        """
        return status.source_task_line.total()

    @classmethod
    def total_ttc_to_invoice(cls, status):
        """
        Compute the total ttc to invoice

        :param obj status: The ProgressInvoicingLineStatus /
        ProgressInvoicingGroupStatus

        :returns: The total ttc to invoice in *10^5 format
        :rtype: int
        """
        source_line_ttc = cls._total_ttc(status)
        return math_utils.percentage(
            source_line_ttc,
            status.percent_to_invoice
        )

    @classmethod
    def get_or_create(cls, status_class, business, source_task_line, **kwargs):
        """
        Get or create the status class related to the estimation
        "source_task_line"
        """
        result = status_class.query().filter_by(
            source_task_line_id=source_task_line.id
        ).first()

        if result is None:
            result = status_class(
                business=business,
                source_task_line=source_task_line,
                **kwargs
            )
            DBSESSION().add(result)
            DBSESSION().flush()
        return result

    @classmethod
    def get_cost(
            cls, line_status, ui_percentage, percent_left, task_line_id=None
    ):
        """
        Compute the cost of elements to invoice

        :param obj line_status: The current ProgressInvoicingLineStatus
        :param float ui_percentage: The percentage to apply
        :param float percent_left: The percentage to invoice before the
        taskline we work on
        :param int task_line_id: The id of a line that should not be considered
        when running the sold invoice (in case of edition, we don't want to
        count two times the same row
        """
        logger.debug("get cost : {} {} {} {}".format(
            line_status, ui_percentage, percent_left, task_line_id
        ))
        total_ht = cls.total_ht_to_invoice(line_status)
        # Facture de solde, on prend directement ce qu'il reste à facturer
        if percent_left - ui_percentage == 0:
            invoiced = ProgressInvoicingLineService.find_invoiced_total_ht(
                line_status, task_line_id
            )
            logger.debug(
                f"    + Sold invoice : Was already invoiced before {invoiced}"
            )
            result = total_ht - invoiced
        else:
            logger.debug(
                f"   + Retrieve the cost {ui_percentage} % of the total to "
                f"invoice {total_ht}"
            )
            logger.debug("Computing the percentage")
            result = math_utils.percentage(
                total_ht, ui_percentage
            )
        logger.debug(f"Cost for the new task line {result}")
        return result

    @classmethod
    def _get_or_create_task_line(cls, line_status, invoice_id=None):
        """
        create or return a TaskLine for the Invoice with id invoice_id and
        associated to the current progress invoicing status

        :param obj line_status: The ProgressInvoicingLineStatus
        :param int invoice_id: The id of the invoice
        :returns: 2-uple (bool, line) True/False if a new line was generated
        and the TaskLine
        """
        line = None
        if invoice_id is not None:
            from endi.models.progress_invoicing import ProgressInvoicingLine
            line = ProgressInvoicingLine.find_generated_task_line(
                line_status, invoice_id
            )
        if line is None:
            source_line = line_status.source_task_line
            line = source_line.duplicate()
            line.group_id = None
            line.quantity = 1
            DBSESSION().add(line)
            created = True
        else:
            DBSESSION().merge(line)
            created = False
        DBSESSION().flush()
        return created, line

    @classmethod
    def update_or_generate(
        cls, line_status, ui_percentage, invoice_id=None
    ):
        """
        Generate a TaskLine object invoicing the given percentage

        :param obj line_status: The ProgressInvoicingLineStatus
        :param int ui_percentage: The UI percentage (set by end user)
        :param obj existing_line: The line case of update

        :returns: A TaskLine
        """
        logger.info(
            f"Generating a Task line with ui_percentage {ui_percentage}"
        )
        from endi.models.progress_invoicing import ProgressInvoicingLine
        created, line = cls._get_or_create_task_line(
            line_status, invoice_id
        )

        # On enregistre/met à jour les informations sur la ligne générée
        cls._record_invoicing(line_status, line, ui_percentage)

        if created:
            logger.debug("A new task line has been created")
            percent_left = line_status.percent_left
            line.cost = cls.get_cost(
                line_status, ui_percentage, percent_left, line.id
            )
        else:
            # Si on édite, on doit repartir du pourcentage avant cette ligne
            invoiced_percentage = ProgressInvoicingLine\
                .find_invoiced_percentage(
                    line_status, exclude_id=line.id
                )
            percent_left = 100 - invoiced_percentage
            logger.debug(
                f"Editing an existing task line to_invoice_percentage "
                f"{percent_left}"
            )
            line.cost = cls.get_cost(
                line_status, ui_percentage, percent_left, line.id
            )
        # On update le pourcentage restant au niveau des statuts
        cls.update_percent_left(line_status, ui_percentage, percent_left)
        DBSESSION().merge(line)
        DBSESSION().flush()
        return line

    @classmethod
    def _record_invoicing(cls, line_status, line, percentage):
        """
        Record an invoicing Line in database (to be able to go back on it)
        """
        from endi.models.progress_invoicing import ProgressInvoicingLine
        record = ProgressInvoicingLine.record_invoicing(
            line_status, line.id, percentage
        )
        return record

    @classmethod
    def is_completely_invoiced(cls, line_status):
        return line_status.percent_left == 0

    @classmethod
    def current_total_ht_left(cls, line_status):
        """
        Compute the total ht regarding the current status (also when an invoice
        is currently edited)

        :rtype: int
        """
        from endi.models.progress_invoicing import ProgressInvoicingLine
        total_ht_to_invoice = cls.total_ht_to_invoice(line_status)
        invoiced_total_ht = ProgressInvoicingLine.find_invoiced_total_ht(
            line_status
        )
        return total_ht_to_invoice - invoiced_total_ht


class ProgressInvoicingGroupStatusService(ProgressInvoicingBaseStatusService):
    @classmethod
    def total_ht_to_invoice(cls, status):
        """
        Return the total ht to invoice

        :param obj status: The ProgressInvoicingLineStatus/
        ProgressInvoicingGroupStatus

        :returns: The total ht to invoice in *10^5 format
        :rtype: int
        """
        return sum(
            line_status.total_ht_to_invoice()
            for line_status in status.line_statuses
        )

    @classmethod
    def tva_to_invoice(cls, status):
        """
        Compute the tva amount to invoice

        :param obj status: The ProgressInvoicingLineStatus /
        ProgressInvoicingGroupStatus

        :returns: The total tva to invoice in *10^5 format
        :rtype: int
        """
        return sum(
            line_status.tva_to_invoice()
            for line_status in status.line_statuses
        )

    @classmethod
    def total_ttc_to_invoice(cls, status):
        """
        Compute the total ttc to invoice

        :param obj status: The ProgressInvoicingLineStatus /
        ProgressInvoicingGroupStatus

        :returns: The total ttc to invoice in *10^5 format
        :rtype: int
        """
        return sum(
            line_status.total_ttc_to_invoice()
            for line_status in status.line_statuses
        )

    @classmethod
    def get_or_create(
        cls, status_class, business, source_task_line_group, **kwargs
    ):
        result = status_class.query().filter_by(
            source_task_line_group_id=source_task_line_group.id
        ).first()

        if result is None:
            result = status_class(
                business=business,
                source_task_line_group=source_task_line_group,
                **kwargs
            )
            DBSESSION().add(result)
            DBSESSION().flush()
        return result

    @classmethod
    def get_group_percentage(cls, group_status, appstruct):
        """
        Try to retrieve a global percentage to be used when invoicing this
        group

        :param obj group_status: The current ProgressInvoicingGroupStatus
        :param dict appstruct: {<linen id>: <percentage>}
        """
        if not appstruct:
            result = 0

        else:
            result = None
            # 1- on checke si toutes les lignes du groupe sont dans le dict,
            # sinon on a une configuration item par item
            line_ids = [i.id for i in group_status.line_statuses]
            if not set(line_ids).difference(set(appstruct.keys())):
                # 2- on checke si toutes les values sont les mêmes:
                if len(set(appstruct.values())) == 1:
                    result = list(appstruct.values())[0]
        return result

    @classmethod
    def _get_or_create_task_line_group(cls, group_status, invoice_id=None):
        """
        Retrieve or initialize a TaskLineGroup for the group_status and the
        given invoice_id

        :param obj group_status: The ProgressInvoicingGroupStatus
        :param int invoice_id: The id of the Invoice we're working on

        :returns: 2-uple (created, TaskLineGroup instance)
        """
        from endi.models.progress_invoicing import ProgressInvoicingGroup

        group = None
        if invoice_id:
            group = ProgressInvoicingGroup.find_generated_task_line_group(
                group_status, invoice_id
            )
        if group is None:
            from endi.models.task.task import TaskLineGroup
            source_group = group_status.source_task_line_group
            group = TaskLineGroup(
                title=source_group.title,
                description=source_group.description,
                order=source_group.order
            )
            DBSESSION().add(group)
            DBSESSION().flush()
            created = True
        else:
            created = False
        return created, group

    @classmethod
    def _find_percentage_to_invoice(cls, group_status, exclude_id=None):
        """
        Retrieve the already invoiced percentage

        :param obj group_status: The ProgressInvoicingGroupStatus
        :param int exclude_id: The id of the TaskLineGroup to exclude
        :returns: The percentage to be invoiced or None
        """
        from endi.models.progress_invoicing import ProgressInvoicingGroup
        invoiced_percentage = ProgressInvoicingGroup.\
            find_invoiced_percentage(
                group_status, exclude_id=exclude_id
            )
        if invoiced_percentage is not None:
            result = 100 - invoiced_percentage
        else:
            result = None
        return result

    @classmethod
    def get_or_generate(cls, group_status, appstruct, invoice_id=None):
        """
        Generate task line groups with an amount corresponding to the expected
        one

        :param obj group_status: The ProgressInvoicingGroupStatus
        :param dict appstruct: A dictionnary containing the percentage of each
        associated line to be invoiced or a single global percentage
        :param int invoice_id: The id of the Invoice we're working on

        e.g :
            {<line_id>: 5, <line2_id>: 8} Will
            generate lines with 5% et 8%
        """
        logger.debug("Get or generate a new TaskLineGroup")
        created, group = cls._get_or_create_task_line_group(
            group_status, invoice_id
        )
        ref_percentage = None

        ui_group_percentage = cls.get_group_percentage(group_status, appstruct)

        logger.debug("Group created : {}".format(created))
        logger.debug("ui_group_percentage : {}".format(ui_group_percentage))
        if not created:
            # Si on édite, on retrouve le pourcentage facturé jusqu'avant
            # cette facture
            ref_percentage = cls._find_percentage_to_invoice(
                group_status, exclude_id=group.id
            )
            if ref_percentage is None:
                # si par le passé on a "splitté" la facturation à l'avancement
                # du produit composé, on ne veut pas revenir sur un mode groupé
                ui_group_percentage = None

        if ui_group_percentage is not None:
            cls._record_invoicing(group_status, group, ui_group_percentage)

            # Update the percentage to invoice
            cls.update_percent_left(
                group_status,
                ui_group_percentage,
                ref_percentage
            )
            for line_status in group_status.line_statuses:
                task_line = line_status.update_or_generate(
                    ui_group_percentage, invoice_id,
                )
                if task_line not in group.lines:
                    group.lines.append(task_line)
        else:
            cls._record_invoicing(group_status, group, None)
            for line_status in group_status.line_statuses:
                ui_percentage = appstruct.get(line_status.id, 0)
                task_line = line_status.update_or_generate(
                    ui_percentage, invoice_id,
                )
                if task_line not in group.lines:
                    group.lines.append(task_line)

            # On met le percent_left à None car on veut marquer que le groupe
            # n'est plus facturé de manière unifié.
            group_status.percent_left = None
            DBSESSION().merge(group_status)
        DBSESSION().flush()

        return group

    @classmethod
    def _record_invoicing(cls, group_status, group, percentage):
        """
        Record an invoicing Group in database (to be able to go back on it)
        """
        from endi.models.progress_invoicing import ProgressInvoicingGroup
        record = ProgressInvoicingGroup.record_invoicing(
            group_status, group.id, percentage
        )
        return record

    @classmethod
    def is_completely_invoiced(cls, group_status):
        """
        Check if all lines have been invoiced in this this group

        :param obj group_status: The current ProgressInvoicingGroupStatus
        :rtype: bool
        """
        result = True
        for line in group_status.line_statuses:
            result = result and line.is_completely_invoiced()
        return result

    @classmethod
    def current_total_ht_left(cls, group_status):
        """
        Compute the total_ht still to be invoiced regarding the current status

        :param obj group_status
        :rtype: int
        """
        return sum(
            line_status.current_total_ht_left()
            for line_status in group_status.line_statuses
        )


class ProgressInvoicingBaseElementService:
    pass


class ProgressInvoicingLineService(ProgressInvoicingBaseElementService):

    @classmethod
    def find_invoiced_percentage(cls, status, exclude_id=None):
        """
        Sum the already invoiced percentages excluding the TaskLine with
        id exclude_id

        :param obj status: A ProgressInvoicingLineStatus
        :param int exclude_id: The id of a TaskLine

        :returns: float or None
        """
        from endi.models.progress_invoicing import ProgressInvoicingLine
        lines = ProgressInvoicingLine.query().filter_by(
            base_status_id=status.id
        )

        result = 0
        for line in lines:
            if line.task_line_id != exclude_id:
                percentage = line.percentage
                result += percentage
        return result

    @classmethod
    def find_invoiced_total_ht(cls, status, exclude_id=None):
        """
        Compute the total_ht already invoiced

        :param obj status: A ProgressInvoicingLineStatus
        :param int exclude_id: The id of the line that should not be considered
        :rtype: int
        """
        from endi.models.progress_invoicing import ProgressInvoicingLine
        task_line_ids = [
            i[0] for i in DBSESSION().query(
                ProgressInvoicingLine.task_line_id
            ).filter_by(
                base_status_id=status.id
            ).filter(
                not_(
                    ProgressInvoicingLine.task_line_id == exclude_id
                )
            )
        ]
        from endi.models.task import TaskLine

        task_lines = TaskLine.query().filter(TaskLine.id.in_(task_line_ids))

        result = 0
        for task_line in task_lines:
            result += task_line.total_ht()
        return result

    @classmethod
    def find_generated_task_line(cls, line_status, invoice_id):
        """
        Retrieve a task line generated for the given invoice using the
        line_status

        :param obj line_status: a ProgressInvoicingLineStatus instance
        :param int invoice_id: The id of the Invoice

        :returns: A TaskLine or None
        """
        result = cls.find_by_status_and_invoice_id(
            line_status, invoice_id
        )
        if result:
            return result.task_line
        else:
            return None

    @classmethod
    def _find_by_invoice_id(cls, invoice_id):
        from endi.models.task import TaskLine, TaskLineGroup
        from endi.models.progress_invoicing import (
            ProgressInvoicingLine,
        )
        group_ids = [
            i[0]
            for i in DBSESSION().query(
                TaskLineGroup.id
            ).filter_by(task_id=invoice_id)
        ]

        line_ids = [
            i[0]
            for i in DBSESSION().query(
                TaskLine.id
            ).filter(TaskLine.group_id.in_(group_ids))
        ]

        return ProgressInvoicingLine.query().filter(
            ProgressInvoicingLine.task_line_id.in_(line_ids)
        )

    @classmethod
    def find_by_status_and_invoice_id(cls, line_status, invoice_id):
        return cls._find_by_invoice_id(invoice_id).filter_by(
            base_status_id=line_status.id
        ).first()

    @classmethod
    def record_invoicing(cls, status, item_id, percentage):
        from endi.models.progress_invoicing import ProgressInvoicingLine

        result = ProgressInvoicingLine.query().filter_by(
            task_line_id=item_id
        ).first()
        if not result:
            result = ProgressInvoicingLine(
                percentage=percentage,
                task_line_id=item_id,
                status=status
            )
            DBSESSION().add(result)
        else:
            result.percentage = percentage
            DBSESSION().merge(result)
        DBSESSION().flush()
        return result

    @classmethod
    def find_percentage(cls, task_line_id):
        from endi.models.progress_invoicing import ProgressInvoicingLine
        return DBSESSION().query(ProgressInvoicingLine.percentage).filter_by(
            task_line_id=task_line_id
        ).scalar()


class ProgressInvoicingGroupService(ProgressInvoicingBaseElementService):

    @classmethod
    def find_invoiced_percentage(cls, status, exclude_id=None):
        """
        Sum the already invoiced percentages excluding the TaskLineGroup with
        id exclude_id

        :param obj status: A ProgressInvoicingGroupStatus
        :param int exclude_id: The id of a TaskLineGroup

        :returns: float or None
        """
        from endi.models.progress_invoicing import ProgressInvoicingGroup
        groups = ProgressInvoicingGroup.query().filter_by(
            base_status_id=status.id
        )

        result = 0
        for group in groups:
            if group.task_line_group_id != exclude_id:
                percentage = group.percentage
                if percentage is not None:
                    result += percentage
                else:
                    # We already had a group with None percentage, we will not
                    # be able to change that
                    result = None
                    break
        return result

    @classmethod
    def find_generated_task_line_group(cls, group_status, invoice_id):
        """
        Find a TaskLineGroup already generated

        :param obj group_status: The ProgressInvoicingGroupStatus
        :param int invoice_id: The id of the associated Invoice

        :returns: A TaskLineGroup or None
        """
        entry = cls.find_by_status_and_invoice_id(
            group_status, invoice_id
        )
        result = None
        if entry:
            result = entry.task_line_group
        return result

    @classmethod
    def _find_by_invoice_id(cls, invoice_id):
        from endi.models.task import TaskLineGroup
        from endi.models.progress_invoicing import (
            ProgressInvoicingGroup,
        )
        group_ids = [
            i[0]
            for i in DBSESSION().query(
                TaskLineGroup.id
            ).filter_by(task_id=invoice_id)
        ]

        return ProgressInvoicingGroup.query().filter(
            ProgressInvoicingGroup.task_line_group_id.in_(group_ids)
        )

    @classmethod
    def find_by_status_and_invoice_id(cls, line_status, invoice_id):
        return cls._find_by_invoice_id(invoice_id).filter_by(
            base_status_id=line_status.id
        ).first()

    @classmethod
    def record_invoicing(cls, status, item_id, percentage):
        """
        Record an invoiced TaskLineGroup and its percentage

        """
        from endi.models.progress_invoicing import (
            ProgressInvoicingGroup,
        )
        # On cherche une entrée existante
        result = ProgressInvoicingGroup.query().filter_by(
            task_line_group_id=item_id
        ).first()
        if not result:
            result = ProgressInvoicingGroup(
                percentage=percentage,
                task_line_group_id=item_id,
                status=status
            )
            DBSESSION().add(result)
        else:
            result.percentage = percentage
            DBSESSION().merge(result)
        DBSESSION().flush()
        return result
