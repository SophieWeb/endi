"""
Progress invoicing
...................


Progress invoicing is handled through two ways:

    - statuses : Remembers the percentage already invoiced

    - line and groups : Stores the relation between task lines and statuses
"""
import logging
from endi_base.models.base import (
    DBBASE,
    default_table_args
)
from sqlalchemy import (
    Column,
    String,
    Integer,
    ForeignKey,
    Numeric,
)
from sqlalchemy.orm import relationship

from endi_base.models.mixins import TimeStampedMixin
from .service import (
    ProgressInvoicingLineStatusService,
    ProgressInvoicingGroupStatusService,
    ProgressInvoicingLineService,
    ProgressInvoicingGroupService,
)

logger = logging.getLogger(__name__)


TABLE_PREFIX = "progress_invoicing_"


class ProgressInvoicingBaseStatus(DBBASE):
    """
    Base Progress Invoicing Status used to remember which percentage of which
    element has been invoiced yet

    Linked to a business

    Uses two reference percentage

    percent_left

        starts at 100% and correspond to the progress invoicing (discussed with
        the customer)

    percent_to_invoice

        100% - account_percent and correspond to the percentage to be
        considered in the progress invoicing
    """
    __tablename__ = TABLE_PREFIX + "base_status"
    __table_args__ = default_table_args
    __mapper_args__ = {
        'polymorphic_on': 'type_',
        'polymorphic_identity': __tablename__,
    }
    id = Column(
        Integer,
        primary_key=True,
    )
    type_ = Column('type_', String(40), nullable=False)
    business_id = Column(
        ForeignKey('business.id', ondelete='CASCADE')
    )

    # mémorise le pourcentage à facturer après accompte
    percent_to_invoice = Column(Numeric(5, 2, asdecimal=False), default=100)

    # Démarre à 100% et révèle la réalité du terrain (utilisé pour l'UI et
    # transposé sur le real_percent pour les calculs)
    # Réduit avec l'avancement
    percent_left = Column(Numeric(5, 2, asdecimal=False), default=100)

    # Relationships
    business = relationship(
        "Business",
        back_populates="progress_invoicing_statuses"
    )
    invoiced_elements = relationship(
        'ProgressInvoicingBaseElement',
        back_populates="status",
        cascade="all, delete",
        passive_deletes=True
    )

    @classmethod
    def get_or_create(cls, business, source_item, **kwargs):
        """
        Retrieve or create a new status entry for the given source item
        """
        return cls._endi_service.get_or_create(
            cls, business, source_item, **kwargs
        )

    def total_ht_to_invoice(self):
        """
        Compute the total ht to invoice without deposit amount

        :returns: The HT amount in 10^5 format
        :rtype: int
        """
        return self._endi_service.total_ht_to_invoice(self)

    def tva_to_invoice(self):
        """
        Compute the tva to invoice without deposit amount

        :returns: The tva amount in 10^5 format
        :rtype: int
        """
        return self._endi_service.tva_to_invoice(self)

    def total_ttc_to_invoice(self):
        """
        Compute the total ttc to invoice without deposit amount

        :returns: The ttc amount in 10^5 format
        :rtype: int
        """
        return self._endi_service.total_ttc_to_invoice(self)

    def current_percent_left(self):
        """
        Compute the percent left regarding the current status (also when an
        invoice is currently edited)

        :rtype: float or None
        """
        return self._endi_service.current_percent_left(self)

    def current_total_ht_left(self):
        """
        Compute the total ht regarding the current status (also when an invoice
        is currently edited)

        :rtype: int
        """
        return self._endi_service.current_total_ht_left(self)

    def is_completely_invoiced(self):
        return self._endi_service.is_completely_invoiced(self)

    def has_deposit(self):
        """
        Return True if a deposit has been deduced from the original HT amount
        """
        return self.percent_to_invoice != 100

    def update_percent_left(self, ui_percentage, ref_percentage=None):
        return self._endi_service.update_percent_left(
            self, ui_percentage, ref_percentage
        )


class ProgressInvoicingLineStatus(ProgressInvoicingBaseStatus):
    """
    Stores the invoicing status of a TaskLine
    """
    __tablename__ = TABLE_PREFIX + "line_status"
    __table_args__ = default_table_args
    __mapper_args__ = {
        'polymorphic_identity': __tablename__,
    }
    id = Column(
        ForeignKey(TABLE_PREFIX + "base_status", ondelete='CASCADE'),
        primary_key=True
    )

    source_task_line_id = Column(
        ForeignKey('task_line.id', ondelete="CASCADE")
    )
    group_status_id = Column(
        ForeignKey(
            TABLE_PREFIX + "group_status.id", ondelete="CASCADE"
        )
    )

    # Relationships
    source_task_line = relationship('TaskLine')
    group_status = relationship(
        "ProgressInvoicingGroupStatus",
        primaryjoin="ProgressInvoicingGroupStatus.id == "
        "ProgressInvoicingLineStatus.group_status_id",
        back_populates="line_statuses",
    )

    _endi_service = ProgressInvoicingLineStatusService

    def update_or_generate(self, ui_percentage, invoice_id=None):
        """
        Generates the TaskLine with the given percentage

        :param float ui_percentage: The percentage configured by the end user
        :param int invoice_id: The id of the Invoice we generate the task for
        """
        return self._endi_service.update_or_generate(
            self, ui_percentage, invoice_id
        )

    def __str__(self):
        return "<{} id : {} source_task_line_id: {}".format(
            self.__class__.__name__, self.id, self.source_task_line_id
        )


class ProgressInvoicingGroupStatus(ProgressInvoicingBaseStatus):
    """
    Stores the invoicing status of a TaskLineGroup

    three states are interesting on UI side

    0 < percent_left < 100

        There are things to invoice globally within this
        group

    percent_left == 100

        The group has been fully invoiced

    percent_left is None

        The group can't be invoiced directly, all lines should be handled
        individually
    """
    __tablename__ = TABLE_PREFIX + "group_status"
    __table_args__ = default_table_args
    __mapper_args__ = {
        'polymorphic_identity': __tablename__,
    }
    id = Column(
        ForeignKey(TABLE_PREFIX + "base_status", ondelete='CASCADE'),
        primary_key=True
    )

    source_task_line_group_id = Column(
        ForeignKey('task_line_group.id', ondelete='CASCADE')
    )
    # Relationships
    source_task_line_group = relationship('TaskLineGroup')
    line_statuses = relationship(
        "ProgressInvoicingLineStatus",
        primaryjoin="ProgressInvoicingGroupStatus.id == "
        "ProgressInvoicingLineStatus.group_status_id",
        back_populates="group_status",
        cascade="all, delete",
        passive_deletes=True
    )
    _endi_service = ProgressInvoicingGroupStatusService

    def get_or_generate(self, appstruct, invoice_id=None):
        """
        Generates the TaskGroup and its TaskLines with the percentages
        configured in the appstruct
        Update it if one already exists

        :param dict appstruct: Dictionnary containing the percentages to
        be used
        :param int invoice_id: The id of the Invoice we generate the task for
        """
        return self._endi_service.get_or_generate(self, appstruct, invoice_id)

    def __str__(self):
        return "<{} id : {} source_task_line_group_id: {}".format(
            self.__class__.__name__, self.id, self.source_task_line_group_id
        )


class ProgressInvoicingBaseElement(DBBASE, TimeStampedMixin):
    __tablename__ = TABLE_PREFIX + "base_element"
    __table_args__ = default_table_args
    __mapper_args__ = {
        'polymorphic_on': 'type_',
        'polymorphic_identity': __tablename__,
    }
    id = Column(
        Integer,
        primary_key=True,
    )
    type_ = Column('type_', String(30), nullable=False)
    percentage = Column(
        Numeric(5, 2, asdecimal=False),
        nullable=True
    )
    base_status_id = Column(
        ForeignKey(
            TABLE_PREFIX + 'base_status.id', ondelete='CASCADE'
        )
    )
    # Relationships
    status = relationship(
        "ProgressInvoicingBaseStatus",
        back_populates="invoiced_elements"
    )
    _endi_service = None

    @classmethod
    def record_invoicing(cls, status, item_id, percentage):
        """
        Create or update a record of a generated element

        :param obj status: ProgressInvoicingLineStatus or
        ProgressInvoicingGroupStatus

        :param int item_id: The id of the TaskLine or TaskLineGroup
        :param float percentage: The percentage to record

        :returns: A new instance of cls
        """
        cls._endi_service.record_invoicing(status, item_id, percentage)

    @classmethod
    def find_invoiced_percentage(cls, status, exclude_id=None):
        """
        Retrieve which percentage of the taskline/group described by status has
        been invoiced, excluding the exclude_id TaskLine/TaskLineGroup.id

        :param obj status: One of ProgressInvoicingLineStatus,
        ProgressInvoicingGroupStatus
        :param int exclude_id: The id of TaskLine/TaskLineGroup to exclude

        :returns: A float or None
        """
        return cls._endi_service.find_invoiced_percentage(status, exclude_id)

    @classmethod
    def find_by_status_and_invoice_id(cls, status, invoice_id):
        """
        Retrieve a record associated to the given status and the given invoice

        :param obj status: ProgressInvoicingLineStatus /
        ProgressInvoicingGroupStatus
        :param int invoice_id: The id of the invoice for which we already
        generated something

        :returns: An instance of the current cls or None
        """
        return cls._endi_service.find_by_status_and_invoice_id(
            status, invoice_id
        )


class ProgressInvoicingLine(ProgressInvoicingBaseElement):
    """
    Remember which task line has been generated with which percentage

    Allow to handle ProgressInvoicing status update
    """
    __tablename__ = TABLE_PREFIX + "line"
    __table_args__ = default_table_args
    __mapper_args__ = {
        'polymorphic_identity': __tablename__,
    }
    id = Column(
        ForeignKey(TABLE_PREFIX + "base_element", ondelete='CASCADE'),
        primary_key=True
    )
    # NB: à la suppression, on supprime manuellement en mettant à jour
    # l'affaire
    task_line_id = Column(ForeignKey('task_line.id', ondelete="SET NULL"))

    # Relationships
    task_line = relationship('TaskLine')
    _endi_service = ProgressInvoicingLineService

    @classmethod
    def find_generated_task_line(cls, line_status, invoice_id):
        """
        Retrieve a TaskLone generated for the given line_status and associated
        to invoice_id
        """
        return cls._endi_service.find_generated_task_line(
            line_status, invoice_id
        )

    @classmethod
    def find_invoiced_total_ht(cls, line_status, exclude_id=None):
        """
        Compute the total_ht sum of all tasklines generated from line_status

        :param obj line_status: The ProgressInvoicingLineStatus
        :param int exclude_id: The id of a TaskLine we don't want in the sum
        :rtype: int
        """
        return cls._endi_service.find_invoiced_total_ht(
            line_status, exclude_id
        )

    @classmethod
    def find_percentage(cls, task_line_id):
        """
        Retrieve the percentage invoiced for the given task line

        :param int task_line_id: A task line of an invoice with
        progress_invoicing status
        :rtype: float
        """
        return cls._endi_service.find_percentage(task_line_id)

    @classmethod
    def find_by_task_line(cls, task_line):
        """
        Find a ProgressInvoicingLine by its task_line_id
        """
        return cls.query().filter(cls.task_line_id == task_line.id).one()


class ProgressInvoicingGroup(ProgressInvoicingBaseElement):
    """
    Remember which task line has been generated with which percentage

    Allow to handle ProgressInvoicing status update

    """
    __tablename__ = TABLE_PREFIX + "group"
    __table_args__ = default_table_args
    __mapper_args__ = {
        'polymorphic_identity': __tablename__,
    }
    id = Column(
        ForeignKey(TABLE_PREFIX + "base_element", ondelete='CASCADE'),
        primary_key=True
    )
    # NB: à la suppression, on supprime manuellement en mettant à jour
    # l'affaire
    task_line_group_id = Column(
        ForeignKey('task_line_group.id', ondelete="SET NULL")
    )

    # Relationships
    task_line_group = relationship('TaskLineGroup')
    _endi_service = ProgressInvoicingGroupService

    @classmethod
    def find_generated_task_line_group(cls, group_status, invoice_id):
        """
        Retrieve a TaskLineGroup generated for the given line_status and
        associated to invoice_id

        :param obj group_status: The ProgressInvoicingGroupStatus
        :param int invoice_id: The id of the Invoice

        :returns: An instance of TaskLineGroup or None
        """
        return cls._endi_service.find_generated_task_line_group(
            group_status, invoice_id
        )

    @classmethod
    def find_by_task_line_group(cls, task_line_group):
        """
        Find a ProgressInvoicingGroup by its task_line_group_id
        """
        return cls.query().filter(
            cls.task_line_group_id == task_line_group.id
        ).one()
