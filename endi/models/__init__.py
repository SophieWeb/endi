"""
    Regouping all models imports is necessary
    to allow the metadata.create_all function to work well
"""
from endi_base.models.base import DBBASE
from endi_base.models.base import DBSESSION

from . import activity
from . import career_stage
from . import career_path
from . import commercial
from . import company
from . import competence
from . import config
from . import files
from . import holiday
from . import indicators
from . import node
from . import options
from . import payments
from . import statistics
from . import supply
from . import third_party
from . import treasury
from . import tva
from . import task
from . import workshop
from .accounting import operations
from .accounting import treasury_measures
from .accounting import income_statement_measures
from .accounting import accounting_closures
from .expense import sheet
from .expense import types
from .expense import payment
from .project import business
from .project import file_types
from .project import mentions
from .project import phase
# Évite les conflits de chemin lors d'import depuis pshell
from .project import project as p
from .project import types
# Évite les conflits de chemin lors d'import depuis pshell
from .progress_invoicing import progress_invoicing as prinv
from .sale_product import category
from .sale_product import base
# Évite les conflits de chemin lors d'import depuis pshell
from .sale_product import sale_product as s
from .sale_product import work
from .sale_product import work_item
from .sale_product import training
from .price_study import work
from .price_study import work_item
from .price_study import product
from .price_study import discount
# Évite les conflits de chemin lors d'import depuis pshell
from .price_study import price_study as p
# from .sale_product import price_study_work
from .training import trainer
from .training import bpf
# Évite les conflits de chemin lors d'import depuis pshell
from .user import user as u
from .user import login
from .user import group
from .user import userdatas
from endi_celery import models
# Importe systématiquement les modèles des plugins
# Même si ils sont inutilisés
from endi.plugins.sap.models import sap


def adjust_for_engine(engine):
    """
    Ajust the models definitions to fit the current database engine
    :param obj engine: The current engine to be used
    """
    if engine.dialect.name == 'mysql':
        # Mysql does case unsensitive comparison by default
        login.Login.__table__.c.login.type.collation = 'utf8mb4_bin'
