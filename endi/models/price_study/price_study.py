import logging
from sqlalchemy import (
    Column,
    Integer,
    String,
    Text,
    ForeignKey,
    BigInteger,
)
from sqlalchemy.orm import relationship
from sqlalchemy.ext.orderinglist import ordering_list

from endi_base.models.base import (
    DBBASE,
    default_table_args,
)
from endi_base.models.mixins import TimeStampedMixin
from .services import PriceStudyService
from endi.compute.math_utils import integer_to_amount


logger = logging.getLogger()


class PriceStudy(TimeStampedMixin, DBBASE):
    __tablename__ = "price_study"
    __table_args__ = default_table_args
    id = Column(
        Integer,
        primary_key=True,
        info={"colanderalchemy": {'exclude': True}},
    )
    name = Column(
        String(255),
        info={
            'colanderalchemy': {
                'title': "Nom de l’étude de prix",
            },
        },
        nullable=False,
    )
    notes = Column(
        Text(),
        info={'colanderalchemy': {'title': "Notes"}}
    )
    project_id = Column(
        ForeignKey('project.id', ondelete='cascade'),
        nullable=False
    )
    company_id = Column(
        ForeignKey('company.id', ondelete='cascade'),
        nullable=False
    )
    owner_id = Column(
        ForeignKey('accounts.id', ondelete='cascade'),
        nullable=False
    )

    ht = Column(BigInteger(), default=0)

    # Relationships
    company = relationship("Company")
    project = relationship("Project")
    owner = relationship("User")

    # Back_populate relationships
    products = relationship(
        'BasePriceStudyProduct',
        back_populates='study',
        order_by="BasePriceStudyProduct.order",
        collection_class=ordering_list('order'),
        passive_deletes=True,
    )
    discounts = relationship(
        'PriceStudyDiscount',
        back_populates='price_study',
        order_by="PriceStudyDiscount.order",
        collection_class=ordering_list('order'),
        passive_deletes=True,
    )
    estimations = relationship(
        "Task",
        back_populates="price_study",
        primaryjoin="and_(Task.price_study_id==PriceStudy.id, "
        "Task.type_.in_(('estimation', 'internalestimation')))",
        overlaps='invoices, price_study'
    )
    invoices = relationship(
        "Task",
        back_populates="price_study",
        primaryjoin="and_(Task.price_study_id==PriceStudy.id, "
        "Task.type_.in_(('invoice', 'cancelinvoice', 'internalinvoice')))",
        overlaps='estimations, price_study'
    )

    _endi_service = PriceStudyService

    def __json__(self, request):
        return dict(
            id=self.id,
            name=self.name,
            notes=self.notes,
            project_id=self.project_id,
            project_name=self.project.name,
            company_id=self.company_id,
            company_name=self.company.name,
            owner_id=self.owner_id,
            owner_name=self.owner.label,
            ht=integer_to_amount(self.ht, 5),
            tva_parts=dict(
                (
                    tva.id,
                    integer_to_amount(item['tva'], 5)
                )
                for tva, item in self.amounts_by_tva().items()
            ),  # {2000: 1250,25484} ...
            total_ht=integer_to_amount(self.total_ht(), 5),
            total_ttc=integer_to_amount(self.total_ttc(), 5),
            total_ht_before_discount=integer_to_amount(
                self.total_ht_before_discount(), 5
            )
        )

    @classmethod
    def add_filter_with_estimation(cls, query):
        return cls._endi_service.add_filter_with_estimation(query)

    def is_deletable(self):
        """
        Check if this price study can be deleted
        """
        return self._endi_service.is_deletable(self)

    def duplicate(self, project_id, user_id):
        instance = self.__class__()
        instance.name = "Copie de {}".format(self.name)
        instance.notes = self.notes
        instance.company_id = self.company_id
        instance.owner_id = user_id
        instance.project_id = project_id
        instance.ht = self.ht

        for product in self.products:
            instance.products.append(product.duplicate())

        for discount in self.discounts:
            instance.discounts.append(discount.duplicate())
        return instance

    def is_editable(self):
        """
        Check if this price study can be edited (if a valid task is attached)
        :returns: True/False
        """
        return self._endi_service.is_editable(self)

    def can_generate(self):
        """
        Check if this price study can be converted to a task (if no task is
        attached)
        :returns: True/False
        """
        return self._endi_service.can_generate(self)

    def sync_amounts(self):
        """
        Sync amounts
        """
        return self._endi_service.sync_amounts(self)

    def amounts_by_tva(self):
        return self._endi_service.amounts_by_tva(self)

    def discounts_by_tva(self):
        return self._endi_service.discounts_by_tva(self)

    # HT
    def total_ht_before_discount(self):
        return self._endi_service.total_ht_before_discount(self)

    def discount_ht(self):
        return self._endi_service.discount_ht(self)

    def total_ht(self):
        return self._endi_service.total_ht(self)

    # TVA
    def total_tva_before_discount(self):
        return self._endi_service.total_tva_before_discount(self)

    def discount_tva(self):
        return self._endi_service.discount_tva(self)

    def total_tva(self):
        return self._endi_service.tva_amounts(self)

    # TTC
    def total_ttc(self):
        return self._endi_service.total_ttc(self)
