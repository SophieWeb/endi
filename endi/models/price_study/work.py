"""
Models related to price study work management

PriceStudyWork
"""
from endi_base.models.base import default_table_args
from sqlalchemy import (
    Column,
    String,
    Integer,
    ForeignKey,
    Boolean,
)
from sqlalchemy.orm import relationship
from .base import BasePriceStudyProduct
from .services import PriceStudyWorkService
from endi.compute.math_utils import integer_to_amount


class PriceStudyWork(BasePriceStudyProduct):
    """
    price study entity grouping several price study work items

    Can be of two types

    Freely added
    Linked to an existing SaleProductWork

    """
    __tablename__ = 'price_study_work'
    __table_args__ = default_table_args
    __mapper_args__ = {
        'polymorphic_on': 'type_',
        'polymorphic_identity': __tablename__,
    }
    id = Column(
        ForeignKey('base_price_study_product.id', ondelete='CASCADE'),
        primary_key=True
    )
    title = Column(String(255))
    sale_product_work_id = Column(
        Integer,
        ForeignKey('sale_product_work.id'),
        nullable=True
    )
    # Doit-on afficher le détail des prestations dans le document final ?
    display_details = Column(Boolean(), default=True)

    # Relationships
    items = relationship(
        "PriceStudyWorkItem",
        back_populates='price_study_work',
    )
    sale_product_work = relationship("SaleProductWork")

    _endi_service = PriceStudyWorkService

    def __json__(self, request):
        result = BasePriceStudyProduct.__json__(self, request)
        result.update(
            dict(
                sale_product_work_id=self.sale_product_work_id,
                display_details=self.display_details,
                title=self.title,
                items=[{'id': item.id} for item in self.items],
                ttc=self.ttc(),
                flat_cost=integer_to_amount(self.flat_cost(), 5),
            )
        )
        return result

    def sync_from_sale_product(self, sale_product, excludes=()):
        """
        Sync datas with the catalog values
        """
        BasePriceStudyProduct.sync_from_sale_product(
            self, sale_product, excludes
        )
        if 'title' not in excludes and sale_product.title:
            self.title = sale_product.title

        from endi.models.price_study.work_item import PriceStudyWorkItem

        if 'items' not in excludes:
            for item in sale_product.items:
                self.items.append(PriceStudyWorkItem.from_work_item(item))
        return self

    @classmethod
    def from_sale_product(cls, sale_product):
        instance = super(PriceStudyWork, cls).from_sale_product(sale_product)
        instance.sale_product_work_id = sale_product.id
        if sale_product.title:
            instance.title = sale_product.title

        from endi.models.price_study.work_item import PriceStudyWorkItem

        for item in sale_product.items:
            PriceStudyWorkItem.from_work_item(instance, item)
        return instance

    def duplicate(self):
        instance = BasePriceStudyProduct.duplicate(self)
        instance.sale_product_work_id = self.sale_product_work_id
        instance.title = "Copie de {}".format(self.title)
        for item in self.items:
            instance.items.append(item.duplicate(instance))
        return instance

    def sync_quantities(self):
        return self._endi_service.sync_quantities(self)

    def find_common_value(self, attrname):
        """
        Find the value for attrname shared by all work items

        :param str attrname: The name of the attribute
        :raises: Exception if distinct values have been configured
        """
        return self._endi_service.find_common_value(self, attrname)
