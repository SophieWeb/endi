import datetime
import logging

from beaker.cache import cache_region
from sqlalchemy import (
    Column,
    Date,
    Integer,
    String,
    Enum,
    Boolean,
    ForeignKey,
    distinct,
)
from sqlalchemy.orm import (
    relationship,
    backref,
)

from endi_base.models.base import (
    DBBASE,
    DBSESSION,
    default_table_args,
)
from endi_base.utils.date import format_date
from endi_base.models.mixins import (
    OfficialNumberMixin,
)
from endi.models.config import Config
from endi.models.project.mixins import BusinessLinkedModelMixin
from endi.models.status import PaidStatusHolderMixin
from .services import (
    BaseExpenseLineService,
    ExpenseSheetNumberService,
)
from endi.utils import strings
from endi.compute.expense import (
    ExpenseCompute,
    ExpenseLineCompute,
    ExpenseKmLineCompute,
)
from endi.compute.math_utils import (
    integer_to_amount,
)
from endi.models.action_manager import (
    Action,
    ActionManager,
)
from endi.models.node import Node
from endi.models.task.actions import get_validation_state_manager


logger = logging.getLogger(__name__)


def _build_justified_state_manager():
    """
    Return a state manager for setting the justified status attribute on
    ExpenseSheet objects
    """
    manager = ActionManager()
    for status, icon, label, title, css in (
        (
            False,
            'clock',
            "En attente",
            "Aucun justificatif n'a été reçu",
            "btn",
        ),
        (
            True,
            'check',
            "Reçus",
            "Les justificatifs ont bien été reçus",
            "btn",
        ),
    ):
        action = Action(
            status,
            'set_justified.expensesheet',
            status_attr='justified',
            icon=icon,
            label=label,
            title=title,
            css=css,
        )
        manager.add(action)
    return manager


def _set_sheet_official_number(
        request,
        sheet: 'ExpenseSheet',
        *args,
        **kwargs
) -> 'ExpenseSheet':
    """
    Callback for when sheet turns into valid status
    """
    template = Config.get_value('expensesheet_number_template', None)

    assert template is not None, \
        'expensesheet_number_template setting should be set'

    if sheet.official_number is None:
        ExpenseSheetNumberService.assign_number(sheet, template)
    return sheet


class ExpenseSheet(
    OfficialNumberMixin,
    Node,
    ExpenseCompute,
    PaidStatusHolderMixin
):
    """
        Model representing a whole ExpenseSheet
        An expensesheet is related to a company and an employee (one user may
        have multiple expense sheets if it has multiple companies)
        :param company_id: The user's company id
        :param user_id: The user's id
        :param year: The year the expense is related to
        :param month: The month the expense is related to
        :param status: Status of the sheet
        :param status_user: The user related to statuschange
        :param lines: expense lines of this sheet
    """
    __tablename__ = 'expense_sheet'
    __table_args__ = default_table_args
    __mapper_args__ = {'polymorphic_identity': 'expensesheet'}
    validation_date_column = 'status_date'

    id = Column(
        ForeignKey('node.id'),
        primary_key=True,
        info={"colanderalchemy": {'exclude': True}},
    )
    month = Column(
        Integer,
        info={
            "colanderalchemy": {
                "title": "Mois",
            }
        }
    )
    year = Column(
        Integer,
        info={
            'colanderalchemy': {
                "title": "Année",
            }
        }
    )
    justified = Column(
        Boolean(),
        default=False,
        info={
            'colanderalchemy': {
                'title': "Justificatifs reçus",
            }
        }
    )
    status = Column(
        String(10),
        default='draft',
        info={
            'colanderalchemy': {
                "title": "Statut de la note de dépenses",
            }
        }
    )
    status_user_id = Column(
        Integer,
        ForeignKey("accounts.id", ondelete='set null'),
        info={
            'colanderalchemy': {
                "title": "Dernier utilisateur à avoir modifié le document",
            },
            "export": {'exclude': True},
        }
    )
    status_date = Column(
        Date(),
        default=datetime.date.today,
        info={
            'colanderalchemy': {
                "title": "Date du dernier changement de statut",
            },
        }
    )
    purchase_exported = Column(
        Boolean(),
        default=False,
        info={
            'colanderalchemy': {'title': "Les achats ont déjà été exportés ?"},
        }
    )
    expense_exported = Column(
        Boolean(),
        default=False,
        info={
            'colanderalchemy': {'title': "Les frais ont déjà été exportés ?"},
        }
    )
    company_id = Column(
        Integer,
        ForeignKey("company.id", ondelete="cascade"),
        info={
            "colanderalchemy": {'exclude': True}
        }
    )
    user_id = Column(
        Integer,
        ForeignKey("accounts.id"),
        info={
            "colanderalchemy": {'exclude': True}
        }
    )

    # Relationships
    lines = relationship(
        "ExpenseLine",
        back_populates="sheet",
        order_by="ExpenseLine.date",
        info={
            "colanderalchemy": {"title": "Dépenses"}
        },
        cascade="all, delete-orphan",
        passive_deletes=True,
    )
    kmlines = relationship(
        "ExpenseKmLine",
        back_populates="sheet",
        order_by="ExpenseKmLine.date",
        info={
            "colanderalchemy": {"title": "Dépenses kilométriques"}
        },
        cascade="all, delete-orphan",
        passive_deletes=True,
    )
    company = relationship("Company")
    user = relationship(
        "User",
        primaryjoin="ExpenseSheet.user_id==User.id",
        info={
            'colanderalchemy': {'exclude': True},
        },
        backref=backref(
            "expenses",
            order_by="ExpenseSheet.month",
            info={
                'colanderalchemy': {'exclude': True},
                'export': {'exclude': True},
            },
            cascade="all, delete-orphan"
        ),
    )
    status_user = relationship(
        "User",
        primaryjoin="ExpenseSheet.status_user_id==User.id",
        info={
            'colanderalchemy': {'exclude': True},
        }
    )
    statuses = relationship(
        "StatusLogEntry",
        back_populates="node",
        order_by="desc(StatusLogEntry.datetime), desc(StatusLogEntry.id)",
        cascade="all, delete-orphan",
        info={
            'colanderalchemy': {'exclude': True},
        }
    )
    validation_state_manager = get_validation_state_manager(
        data_type='expensesheet',
        userid_attr='status_user_id',
        callbacks=dict(valid=_set_sheet_official_number),
    )
    justified_state_manager = _build_justified_state_manager()

    def __json__(self, request):
        return dict(
            id=self.id,
            name=self.name,
            created_at=self.created_at.isoformat(),
            updated_at=self.updated_at.isoformat(),

            company_id=self.company_id,
            user_id=self.user_id,
            paid_status=self.paid_status,
            justified=self.justified,
            status=self.status,
            status_user_id=self.status_user_id,
            status_date=self.status_date.isoformat(),

            lines=[line.__json__(request) for line in self.lines],
            kmlines=[line.__json__(request) for line in self.kmlines],
            month=self.month,
            month_label=strings.month_name(self.month),
            year=self.year,
            attachments=[
                f.__json__(request)for f in self.children if f.type_ == 'file'
            ]
        )

    @property
    def date(self):
        """
        Date property used to match the official_numbering system

        See : https://framagit.org/endi/endi/-/issues/2596
        """
        return datetime.date(self.year, self.month, 1)

    def set_status(self, status, request, **kw):
        """
        set the status of a task through the state machine
        """
        return self.validation_state_manager.process(
            status,
            self,
            request,
            **kw
        )

    def check_status_allowed(self, status, request, **kw):
        return self.validation_state_manager.check_allowed(
            status, self, request
        )

    def set_justified_status(self, status, request, **kw):
        """
        set the signed status of a task through the state machine
        """
        return self.justified_state_manager.process(
            status,
            self,
            request,
            **kw
        )

    def check_justified_status_allowed(self, status, request, **kw):
        return self.justified_state_manager.check_allowed(
            status, self, request
        )

    def get_company_id(self):
        """
            Return the if of the company associated to this model
        """
        return self.company_id

    def duplicate(self, year, month):
        sheet = ExpenseSheet()
        sheet.month = month
        sheet.year = year
        sheet.user_id = self.user_id
        sheet.company_id = self.company_id
        sheet.lines = [line.duplicate(sheet) for line in self.lines]
        for line in self.kmlines:
            newline = line.duplicate(sheet)
            if newline:
                sheet.kmlines.append(newline)
        return sheet

    @property
    def global_status(self):
        """
        Compile a global status essentially used to match css rules

        :rtype: str
        """
        if self.paid_status == 'paid':
            result = 'partial_unpaid'
        else:
            result = self.status
        return result


class BaseExpenseLine(DBBASE, BusinessLinkedModelMixin):
    """
        Base models for expense lines
        :param type: Column for polymorphic discrimination
        :param date: Date of the expense
        :param description: description of the expense
        :param code: analytic code related to this expense
        :param valid: validation status of the expense
        :param sheet_id: id of the expense sheet this expense is related to
    """
    __tablename__ = 'baseexpense_line'
    __table_args__ = default_table_args
    __mapper_args__ = dict(
        polymorphic_on="type",
        polymorphic_identity="line",
        with_polymorphic='*',
    )
    _endi_service = BaseExpenseLineService
    parent_model = ExpenseSheet

    id = Column(
        Integer,
        primary_key=True,
        info={"colanderalchemy": {'exclude': True}},
    )
    type = Column(
        String(30),
        nullable=False,
        info={'colanderalchemy': {'exclude': True}},
    )
    date = Column(
        Date(),
        default=datetime.date.today,
        info={'colanderalchemy': {'title': "Date"}},
    )
    description = Column(
        String(255),
        info={'colanderalchemy': {'title': "Description"}},
        default="",
    )
    category = Column(
        Enum('1', '2', name='category'),
        default='1'
    )
    valid = Column(
        Boolean(),
        default=True,
        info={"colanderalchemy": {"title": "Valide ?"}}
    )
    type_id = Column(
        Integer,
        info={
            'colanderalchemy': {
                "title": "Type de dépense",
            }
        }
    )
    sheet_id = Column(
        Integer,
        ForeignKey("expense_sheet.id", ondelete="cascade"),
        info={'colanderalchemy': {'exclude': True}}
    )

    expense_type = relationship(
        "ExpenseType",
        primaryjoin='BaseExpenseLine.type_id==ExpenseType.id',
        uselist=False,
        foreign_keys=type_id,
        info={'colanderalchemy': {'exclude': True}}
    )

    def long_label(self):
        return '{} {}€ ({}) − {}'.format(
            self.description,
            strings.format_amount(self.total, grouping=False),
            self.expense_type.label,
            format_date(self.date),
        )

    @classmethod
    def linkable(cls, business):
        return cls._endi_service.linkable(cls, business)

    def __json__(self, request):
        ret = dict(
            id=self.id,
            date=self.date,
            description=self.description,
            category=self.category,
            valid=self.valid,
            type_id=self.type_id,
            sheet_id=self.sheet_id,
            customer_id=self.customer_id,
            project_id=self.project_id,
            business_id=self.business_id,
        )
        ret.update(dict(
            BusinessLinkedModelMixin.__json__(self, request),
        ))
        return ret


class ExpenseLine(BaseExpenseLine, ExpenseLineCompute):
    """
        Common Expense line
    """
    __tablename__ = "expense_line"
    __table_args__ = default_table_args
    __mapper_args__ = dict(polymorphic_identity='expenseline')
    id = Column(
        Integer,
        ForeignKey('baseexpense_line.id', ondelete='cascade'),
        primary_key=True,
        info={'colanderalchemy': {'exclude': True}}
    )
    ht = Column(
        Integer,
        info={
            'colanderalchemy': {
                'title': 'Montant HT',
            }
        },
    )
    tva = Column(
        Integer,
        info={
            'colanderalchemy': {
                'title': 'Montant de la TVA',
            }
        },
    )
    manual_ttc = Column(
        Integer,
        info={
            'colanderalchemy': {
                'title': 'Montant TTC saisi',
            }
        },
        default=0,
    )
    sheet = relationship(
        "ExpenseSheet",
        info={'colanderalchemy': {'exclude': True}},
        back_populates="lines",
    )

    def __json__(self, request):
        res = BaseExpenseLine.__json__(self, request)
        res.update(
            dict(
                ht=integer_to_amount(self.ht, 2, 0),
                tva=integer_to_amount(self.tva, 2, 0),
                manual_ttc=integer_to_amount(self.manual_ttc, 2, 0),
            )
        )
        return res

    def duplicate(self, sheet=None):
        line = ExpenseLine()
        line.description = self.description
        line.category = self.category
        line.type_id = self.type_id
        line.ht = self.ht
        line.tva = self.tva
        line.manual_ttc = self.manual_ttc
        line.customer_id = self.customer_id
        line.project_id = self.project_id
        line.business_id = self.business_id
        if sheet is not None:
            line.sheet_id = sheet.id
        return line


class ExpenseKmLine(BaseExpenseLine, ExpenseKmLineCompute):
    """
        Model representing a specific expense related to kilometric fees
        :param start: starting point
        :param end: endpoint
        :param km: Number of kilometers
    """
    __tablename__ = "expensekm_line"
    __table_args__ = default_table_args
    __mapper_args__ = dict(polymorphic_identity='expensekmline')
    id = Column(
        Integer,
        ForeignKey('baseexpense_line.id', ondelete='cascade'),
        primary_key=True,
        info={'colanderalchemy': {'exclude': True}}
    )
    start = Column(
        String(150),
        default="",
        info={"colanderalchemy": {"title": "Point de départ"}}
    )
    end = Column(
        String(150),
        default="",
        info={"colanderalchemy": {"title": "Point d'arrivée"}}
    )
    km = Column(
        Integer,
        info={
            'colanderalchemy': {
                'title': 'Nombre de kilomètres',
            }
        },
    )
    sheet = relationship(
        "ExpenseSheet",
        uselist=False,
        info={'colanderalchemy': {'exclude': True}},
        back_populates='kmlines',
    )

    def __json__(self, request):
        res = BaseExpenseLine.__json__(self, request)
        res.update(
            dict(
                km=integer_to_amount(self.km),
                start=self.start,
                end=self.end,
                vehicle=self.vehicle,
            )
        )
        return res

    @property
    def vehicle(self):
        return self.expense_type.label

    def duplicate(self, sheet):
        expense_type = self.expense_type.get_by_year(sheet.year)
        line = None

        # Ref #774
        if expense_type is not None:
            line = ExpenseKmLine()
            line.description = self.description
            line.category = self.category
            line.expense_type = self.expense_type.get_by_year(sheet.year)
            line.start = self.start
            line.end = self.end
            line.km = self.km
            line.customer_id = self.customer_id
            line.project_id = self.project_id
            line.business_id = self.business_id
        return line


def get_expense_years(kw):
    """
    Return the list of years that there were some expense configured
    """
    @cache_region("long_term", "expenseyears")
    def expenseyears():
        """
        return distinct expense years available in the database
        """
        query = DBSESSION().query(distinct(ExpenseSheet.year))
        query = query.order_by(ExpenseSheet.year)
        years = [year[0] for year in query]
        current = datetime.date.today().year
        if current not in years:
            years.append(current)
        return years
    return expenseyears()


def get_expense_sheet_name(month, year):
    """
    Return the name of an expensesheet
    """
    return "expense_{0}_{1}".format(month, year)
