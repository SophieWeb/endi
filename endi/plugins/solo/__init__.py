"""
Entry point for endi-solo specific stuff
"""
from endi.models.company import Company
from endi.panels.menu import get_usermenu
from endi.utils.menu import (
    AppMenuDropDown,
    AppMenuItem,
)


def menu_panel(context, request):
    """
    Top menu panel

    Build the top menu dict representation

    :rtype: dict
    """
    # If we've no user in the current request, we don't return anything
    if not getattr(request, 'user'):
        return {}
    company = Company.query().first()
    menu_builder = request.registry.admin_menu
    menu = menu_builder.build(
        request,
        context=company,
        user_id=request.user.id,
        company_id=company.id,
        submenu=False,
        is_user_company=True,
        company=company
    )

    usermenu = get_usermenu(request)

    return {
        'menu': menu,
        'usermenu': usermenu,
    }


def hack_admin_menu(config):
    config.registry.admin_menu.items[0] = \
        config.registry.company_menu.items.pop(0)

    # On déplace l'entrée "Configuration"
    config_menu = config.registry.admin_menu.items.pop(1)
    config.registry.admin_menu.add(config_menu, 'annuaire')
    config.registry.admin_menu.find('annuaire').label = "Administration"

    # On supprime l'entrée "notes de dépenses"
    admin_sale_menu = config.registry.admin_menu.find('sale')
    admin_sale_menu.items.pop()
    # On supprime l'annuaire des entreprises
    annuaire = config.registry.admin_menu.find('annuaire')
    annuaire.items.pop(1)

    # On ajoute un menu "Nouveau"
    config.registry.admin_menu.add(
        AppMenuDropDown(order=1, name='solo', label="Nouveau")
    )
    config.registry.admin_menu.add(
        AppMenuItem(
            order=1,
            label="Devis",
            route_name="company_estimations",
            route_id_key="company_id",
            route_query_params="action=add"
        ),
        "solo",
    )
    config.registry.admin_menu.add(
        AppMenuItem(
            order=2,
            label="Facture",
            route_name="company_invoices",
            route_id_key="company_id",
            route_query_params="action=add"
        ),
        "solo",
    )
    config.registry.admin_menu.add(
        AppMenuItem(
            order=3,
            label="Note de dépenses",
            route_name="user_expenses_shortcut",
            route_id_key="user_id",
        ),
        "solo",
    )

    # Dossiers
    company_sale_menu = config.registry.company_menu.find('sale')
    config.registry.admin_menu.add(company_sale_menu.items[0])
    config.registry.company_menu.remove(company_sale_menu)

    # Clients
    company_third_menu = config.registry.company_menu.find('third_party')
    config.registry.admin_menu.add(company_third_menu.items[0])
    config.registry.company_menu.remove(company_third_menu)

    # Note de dépenses
    expense_menu = config.registry.company_menu.find('supply')
    config.registry.admin_menu.add(expense_menu.items[0], 'sale')
    config.registry.company_menu.remove(expense_menu)

    # Catalogue produit
    catalog = config.registry.company_menu.pop(0)
    config.registry.admin_menu.add(catalog, 'sale')

    # Fiche de l'enseigne et suppression annuaire enseigne
    for item in config.registry.company_menu.items:
        if isinstance(item, AppMenuItem):
            fiche = item
            break
    config.registry.company_menu.remove(fiche)
    config.registry.admin_menu.add(fiche, 'annuaire')


def submenu_panel(context, request):
    return {}


def includeme(config):
    hack_admin_menu(config)
    config.add_panel(
        menu_panel,
        'menu',
        renderer='/panels/menu.mako',
    )
    config.add_panel(
        submenu_panel,
        'submenu',
        renderer='/panels/menu.mako',
    )
