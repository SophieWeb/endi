from endi.views.estimations.rest_api import EstimationRestView
from ..mixins import SAPTaskRestViewMixin


class SAPEstimationRestView(SAPTaskRestViewMixin, EstimationRestView):
    def _more_form_sections(self, sections):
        sections = EstimationRestView._more_form_sections(self, sections)
        sections['composition']['lines']['date'] = {'edit': True}
        sections['composition'].pop('display_units')
        sections['composition'].pop('display_ttc')
        return sections


def add_views(config):
    config.add_view(
        SAPEstimationRestView,
        attr='form_config',
        route_name='/api/v1/estimations/{id}',
        renderer='json',
        request_param="form_config",
        permission='edit.estimation',
        xhr=True,
    )


def includeme(config):
    add_views(config)
