"""6.2.0 Remove unwanted cascade

Revision ID: 65a3bfcfb616
Revises: c4b03f713cae
Create Date: 2021-05-11 17:35:13.201916

"""

# revision identifiers, used by Alembic.
revision = '65a3bfcfb616'
down_revision = 'c4b03f713cae'

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql


def update_database_structure():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint('fk_internalestimation_id', 'internalestimation', type_='foreignkey')
    op.create_foreign_key(op.f('fk_internalestimation_id'), 'internalestimation', 'estimation', ['id'], ['id'])


def migrate_datas():
    from alembic.context import get_bind
    from zope.sqlalchemy import mark_changed
    from endi_base.models.base import DBSESSION

    session = DBSESSION()
    conn = get_bind()

    mark_changed(session)
    session.flush()


def upgrade():
    update_database_structure()
    migrate_datas()


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint(op.f('fk_internalestimation_id'), 'internalestimation', type_='foreignkey')
    op.create_foreign_key('fk_internalestimation_id', 'internalestimation', 'estimation', ['id'], ['id'], ondelete='CASCADE')
