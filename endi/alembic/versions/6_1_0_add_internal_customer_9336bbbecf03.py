"""6.1.0 add internal customer

Revision ID: 9336bbbecf03
Revises: 41143edd69a0
Create Date: 2021-02-03 16:38:29.839618

"""

# revision identifiers, used by Alembic.
revision = '9336bbbecf03'
down_revision = '41143edd69a0'

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql
from endi.alembic.utils import drop_foreign_key_if_exists


def update_database_structure():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('third_party', sa.Column('source_company_id', sa.Integer(), nullable=True))
    op.create_foreign_key(op.f('fk_third_party_source_company_id'), 'third_party', 'company', ['source_company_id'], ['id'], ondelete='SET NULL')
    # ### end Alembic commands ###


def migrate_datas():
    from alembic.context import get_bind
    from zope.sqlalchemy import mark_changed
    from endi_base.models.base import DBSESSION

    session = DBSESSION()
    conn = get_bind()

    mark_changed(session)
    session.flush()


def upgrade():
    update_database_structure()
    migrate_datas()


def downgrade():
    drop_foreign_key_if_exists('third_party', 'fk_third_party_source_company_id')
    op.drop_column('third_party', 'source_company_id')
    # ### commands auto generated by Alembic - please adjust! ###
    # ### end Alembic commands ###
