endi.tests.views package
=============================

Submodules
----------

endi.tests.views.test_activity module
------------------------------------------

.. automodule:: endi.tests.views.test_activity
    :members:
    :undoc-members:
    :show-inheritance:

endi.tests.views.test_admin module
---------------------------------------

.. automodule:: endi.tests.views.test_admin
    :members:
    :undoc-members:
    :show-inheritance:

endi.tests.views.test_auth module
--------------------------------------

.. automodule:: endi.tests.views.test_auth
    :members:
    :undoc-members:
    :show-inheritance:

endi.tests.views.test_commercial module
--------------------------------------------

.. automodule:: endi.tests.views.test_commercial
    :members:
    :undoc-members:
    :show-inheritance:

endi.tests.views.test_company module
-----------------------------------------

.. automodule:: endi.tests.views.test_company
    :members:
    :undoc-members:
    :show-inheritance:

endi.tests.views.test_customer module
------------------------------------------

.. automodule:: endi.tests.views.test_customer
    :members:
    :undoc-members:
    :show-inheritance:

endi.tests.views.test_expense module
-----------------------------------------

.. automodule:: endi.tests.views.test_expense
    :members:
    :undoc-members:
    :show-inheritance:

endi.tests.views.test_forms_company module
-----------------------------------------------

.. automodule:: endi.tests.views.test_forms_company
    :members:
    :undoc-members:
    :show-inheritance:

endi.tests.views.test_forms_custom_types module
----------------------------------------------------

.. automodule:: endi.tests.views.test_forms_custom_types
    :members:
    :undoc-members:
    :show-inheritance:

endi.tests.views.test_forms_customer module
------------------------------------------------

.. automodule:: endi.tests.views.test_forms_customer
    :members:
    :undoc-members:
    :show-inheritance:

endi.tests.views.test_forms_holiday module
-----------------------------------------------

.. automodule:: endi.tests.views.test_forms_holiday
    :members:
    :undoc-members:
    :show-inheritance:

endi.tests.views.test_forms_project module
-----------------------------------------------

.. automodule:: endi.tests.views.test_forms_project
    :members:
    :undoc-members:
    :show-inheritance:

endi.tests.views.test_forms_task module
--------------------------------------------

.. automodule:: endi.tests.views.test_forms_task
    :members:
    :undoc-members:
    :show-inheritance:

endi.tests.views.test_forms_user module
--------------------------------------------

.. automodule:: endi.tests.views.test_forms_user
    :members:
    :undoc-members:
    :show-inheritance:

endi.tests.views.test_forms_utils module
---------------------------------------------

.. automodule:: endi.tests.views.test_forms_utils
    :members:
    :undoc-members:
    :show-inheritance:

endi.tests.views.test_forms_validator module
-------------------------------------------------

.. automodule:: endi.tests.views.test_forms_validator
    :members:
    :undoc-members:
    :show-inheritance:

endi.tests.views.test_forms_widget module
----------------------------------------------

.. automodule:: endi.tests.views.test_forms_widget
    :members:
    :undoc-members:
    :show-inheritance:

endi.tests.views.test_holiday module
-----------------------------------------

.. automodule:: endi.tests.views.test_holiday
    :members:
    :undoc-members:
    :show-inheritance:

endi.tests.views.test_invoice module
-----------------------------------------

.. automodule:: endi.tests.views.test_invoice
    :members:
    :undoc-members:
    :show-inheritance:

endi.tests.views.test_project module
-----------------------------------------

.. automodule:: endi.tests.views.test_project
    :members:
    :undoc-members:
    :show-inheritance:

endi.tests.views.test_render_api module
--------------------------------------------

.. automodule:: endi.tests.views.test_render_api
    :members:
    :undoc-members:
    :show-inheritance:

endi.tests.views.test_taskaction module
--------------------------------------------

.. automodule:: endi.tests.views.test_taskaction
    :members:
    :undoc-members:
    :show-inheritance:

endi.tests.views.test_treasury_files module
------------------------------------------------

.. automodule:: endi.tests.views.test_treasury_files
    :members:
    :undoc-members:
    :show-inheritance:

endi.tests.views.test_user module
--------------------------------------

.. automodule:: endi.tests.views.test_user
    :members:
    :undoc-members:
    :show-inheritance:

endi.tests.views.test_workshop module
------------------------------------------

.. automodule:: endi.tests.views.test_workshop
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: endi.tests.views
    :members:
    :undoc-members:
    :show-inheritance:
