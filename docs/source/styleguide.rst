Guide de style
==============


Gestion des fichiers de style
-----------------------------

Les feuilles de style sont basées sur des sources SASS situées dans le répertoire css_sources.
On peut les compiler automatiquement à chaque modification avec la commande ``make css``.
Les fichiers .sass sont structurés en fragments correspondant aux diverses parties structurelles du site (mise en forme générale, menu, tableaux, etc… ).


Règles générales
----------------

Quand on signale l’absence de contenu (« Pas de produit », « Aucun contenu n’a été ajouté », « Pas de devis »…), mettre le texte correspondant en italique au moyen d’un élément ``<em>``.


Colonnes
--------

2 à 4 Colonnes 
~~~~~~~~~~~~~~

Pour structurer le contenu d’une ``<div>`` en deux à quatre colonnes, appliquer les classes ``layout`` et ``flex``, et une des classes ``two_cols`` ``three_cols`` ou  ``four_cols``. Cela répartit tous les éléments directement contenus dans cette ``<div>`` en 2, 3 ou 4 colonnes de largeur égale.

Pour les structures en 2 colonnes, 4 classes à ajouter à  ``layout flex two_cols`` permettent d’obtenir deux colonnes de largeurs différentes :

- ``third`` colonne de gauche 1/3, colonne de droite 2/3 (``<div class="layout flex two_cols third">``)
- ``third_reverse`` colonne de gauche 2/3, colonne de droite 1/3 (``<div class="layout flex two_cols third_reverse">``)
- ``quarter`` colonne de gauche 1/4, colonne de droite 3/4 (``<div class="layout flex two_cols quarter">``)
- ``quarter_reverse`` colonne de gauche 3/4, colonne de droite 1/4 (``<div class="layout flex two_cols quarter_reverse">``)

Rangs divisés en colonnes
~~~~~~~~~~~~~~~~~~~~~~~~~

Une ``<div>`` considérée comme un rang (particulièrement dans les formulaires) est structurée en colonnes en utilisant les classes ``col-md-1`` (1/12ème) à ``col-md-12`` (12/12èmes, 100%) sur chaque élément du rang. 
La gestion responsive de la largeur des colonnes dispense d’utiliser les classes de type ``col-xs`` ou autre, qui n’ont pas d’effet et n’espacent pas les colonnes de formulaires.


Espacement
----------

Des classes permettent de créer de l’espacement entre blocs en ajoutant du ``padding`` :
Quand un bloc n’est pas séparé visuellement des blocs voisins, on privilégie l’espacement vertical seul pour éviter de perdre les alignements verticaux.

- ``content_vertical_padding`` ajoute une unité d’espacement sur les 2 côtés au-dessus et au-dessous du bloc
- ``content_vertical_double_padding`` ajoute deux unités d’espacement sur les 2 côtés au-dessus et au-dessous du bloc

On peut ajouter une séparation visuelle horizontale :

- ``separate_top`` ajoute une ligne au-dessus du bloc
- ``separate_bottom`` ajoute une ligne au-dessous du bloc
- ``separate_top_dashed`` ajoute une ligne pointillée au-dessus du bloc
- ``separate_bottom_dashed`` ajoute une ligne pointillée au-dessous du bloc


Pour les blocs séparés visuellement par une bordure ou un fond de couleur : 

- ``content_padding`` ajoute une unité d’espacement sur les 4 côtés du bloc
- ``content_double_padding`` ajoute deux unités d’espacement sur les 4 côtés du bloc
- ``content_horizontal_padding`` ajoute une unité d’espacement sur les 2 côtés latéraux du bloc
- ``content_horizontal_double_padding`` ajoute deux unités d’espacement sur les 2 côtés latéraux du bloc


Vues mobile et tablette
-----------------------

Il est possible de masquer certains contenus en fonction de la taille de l’écran, par exemple pour préciser un libellé sur les grands écrans, ou ne pas afficher une colonne de tableau sur mobile.

En ce qui concerne les libellés, ne pas oublier une info-bulle avec le texte complet (les mises en page « petits écrans » et « très petits écrans » sont aussi utilisées par les déficients visuels utilisant l’agrandissement du texte sur écran d’ordinateur).

- Les éléments portant la classe ``no_mobile`` ne seront pas affichés sur les très petits écrans (moins de 31.25rem de large)
- Les éléments portant la classe ``no_tablet`` ne seront pas affichés sur les petits écrans (moins de 50rem de large)

Exemple pour un bouton : 


.. code-block:: html

	<button class="btn btn-primary" title="Programmer un nouveau rendez-vous" onclick="toggleModal('next_activity_form'); return false;">
		<svg><use href="${request.static_url('endi:static/icons/endi.svg')}#plus"></use></svg>
		Nouveau<span class="no_mobile">&nbsp;rendez-vous</span>
	</button>

En ce qui concerne les boutons, on peut aussi plus simplement les marquer pour qu’ils se transforment en bouton à icône seule pour les petits et très petits écrans en ajoutant la classe ``icon_only_tablet`` ou pour les très petits écrans seulement avec ``icon_only_mobile``.


.. code-block:: html

	<button class="btn btn-primary icon_only_mobile" title="Programmer un nouveau rendez-vous" onclick="toggleModal('next_activity_form'); return false;">
		<svg><use href="${request.static_url('endi:static/icons/endi.svg')}#plus"></use></svg>
		Nouveau rendez-vous
	</button>


Impression
----------

Il est possible de masquer certains éléments lors de l’impression en leur ajoutant la classe ``hidden-print``.


Titre d’écran
-------------

Le titre de l’écran (contenu dans le ``<h1>`` dans l’en-tête de l’écran) peut-être précisé par du texte inséré dans une balise ``<small>``.


.. code-block:: html

	<h1>Devis « titre du devis » <small>pour le client Prénom Nom</small></h1>

Quand le titre passe sur deux lignes, le texte de la deuxième ligne est automatiquement affiché dans une police plus petite.


Fil d’Ariane
------------

Le fil d’Ariane décrit le chemin parcouru pour accéder à l’écran en cours.

Il est contenu dans une liste ``<ul class="breadcrumb">``. 

Le premier lien de la liste permet de remonter d’un niveau. Les liens suivants décrivent l’arborescence compléte. Le dernier élément de la liste n’est pas un lien mais le titre de l’écran en cours.


.. code-block:: html

	<ul class="breadcrumb">
		<li class="back_link">
			<a href="chemin-ecran-parent" class="icon" title="Remonter d’un niveau" aria-label="Remonter d’un niveau">${api.icon('arrow-left')}</a>
		</li>
		…
		<li>
			<a href="chemin-ecran-grand-parent">Écran grand parent</a>
		</li>
		<li>
			<a href="chemin-ecran-parent">Écran parent</a>
		</li>
		<li class="active">
			<span>Écran en cours</span>
		</li>
	</ul>



Tableaux
--------

Tableaux ``<table>``
~~~~~~~~~~~~~~~~~~~~

- Les tableaux ``<table>`` sont contenus dans une ``<div class="table_container">`` qui permet de gérer les débordements horizontaux des tableaux sur petits écrans (scroll horizontal plutôt que débordement de page)
- La classe ``top_align_table`` permet d’aligner verticalement le contenu de chaque case du tbody en haut de case. À utiliser pour les tableaux dont certaines cases ont des contenus très longs, pour faciliter la lecture ligne à ligne. Sans cette classe, l’alignement vertical dans les cases est centré par défaut.
- La classe ``hover_table`` permet d’ajouter une couleur au survol à chaque ligne du tableau. À utiliser quand une ou plusieurs actions peuvent être réalisées en cliquant sur une ligne ou sur le contenu des cases autres que ``col_actions``. Cf. :ref:`Navigation au clic sur les lignes de tableaux`
- La classe ``spaced_table`` permet d’ajouter de l’espace au-dessus et au-dessous du tableau.
- La classe ``full_width`` permet de forcer la largeur du tableau à 100% de son contenant.

Quand un tableau est vide, ne pas afficher son en-tête, mais un ``<td class="col_text">`` de toute la largeur du tableau contenant du texte dans un ``<em>`` précisant qu’il n’y a rien à afficher (par exemple « Aucun fichier disponible »)

Rangs ``<tr>``
~~~~~~~~~~~~~~

- La classe ``top_align`` permet d’aligner verticalement le contenu de chaque case du rang en haut de case. Cf. ``top_align_table`` pour l’utilisation.
- La classe ``recap_row`` permet de faire une ligne de synthèse, qui va par exemple contenir les totaux des lignes précédentes.

- Les rangs avec la classe ``<tr class="row_recap">`` permettent d’afficher des totaux ou sous-totaux. Dans les tableaux de listes qui contiennent plus de 10-15 lignes, il est conseillé de répéter ce rang affichant les totaux généraux en début et en fin de tableau, pour faciliter la lecture des totaux sans avoir à faire un défilement vertical.


En-têtes ``<th>``
~~~~~~~~~~~~~~~~~

- Utiliser l’attribut scope pour définir si l’en-tête est un en-tête de colonne (``scope="col"``) ou un en-tête de ligne (``scope="row"``)
- Les titres d’en-tête peuvent être abrégés tout en conservant une bonne accessibilité en ajoutant un ``title`` avec le texte complet sur le ``<th>`` et en plaçant le contenu abrégé dans un ``<span class="screen-reader-text">``. 

.. code-block:: html

	<th scope="col" class="col_number" title="Taux de TVA"><span class="screen-reader-text">Taux de </span>TVA</th>
	

Cases ``<td>``
~~~~~~~~~~~~~~

- La classe ``top_align`` permet d’aligner verticalement le contenu de la case en haut de case. Cf. ``top_align_table`` pour l’utilisation.
- La classe ``archive`` permet de signaler une information concernant un événement passé ou modifié par d’autres cases du tableau.

Types de données
~~~~~~~~~~~~~~~~

Des classes permettent de gérer automatiquement la largeur et l’alignement des colonnes en fonction du type de contenu. Elles sont placées à la fois sur les ``<th>`` et les ``<td>`` de la colonne :

- ``col_text`` pour du contenu textuel, aligné par défaut à gauche. Il est possible d’aligner à droite avec ``col_text align_right``
- ``col_text rich_text`` pour du contenu textuel issu d’un champ de texte mis en forme (TinyMCE).
- ``col_icon`` pour du contenu comprenant une icône suivie par un libellé texte
- ``col_status`` pour du contenu ne comprenant qu’une icône qui reflète un statut Cf. :ref:`Statuts` pour le contenu
- ``col_number`` pour des nombres (alignés à droite)
- ``col_number positive`` pour des nombres positifs (à préfixer par "+ ")
- ``col_number negative`` pour des nombres négatifs (à préfixer par "- ")
- ``col_date`` pour une date (jour, mois, année)
- ``col_datetime`` pour une date et heure
- ``col_text phone`` pour un numéro de téléphone
- ``col_select`` pour les colonnes contenant une case à cocher ou un bouton radio permettant de sélectionner une ligne
- ``col_percentage_graphic`` pour un widget permettant de définir un pourcentage à appliquer à la ligne
- ``col_actions`` pour les boutons d’action Cf. :ref:`Boutons d’action dans les lignes de tableaux`

Ajout de ligne au tableau
~~~~~~~~~~~~~~~~~~~~~~~~~

Quand on peut ajouter une ligne au tableau, le bouton correspondant est placé dans le ``<tfoot>`` du tableau, dans un un ``<td class="col_actions">`` de toute la largeur du tableau. Il s’affiche donc sous le tableau. 


Boutons d’action dans les lignes de tableaux
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- Ces boutons sont toujours des boutons simples (pas de ``class="btn-primary``)
- Quand au maximum 4 actions faciles à différencier graphiquement sont réalisables sur une ligne, les boutons d’action sont insérés directement dans la colonne ``col_actions`` sous forme de boutons icônes.
- Quand il y a plus d’actions où qu’elles ne sont pas différenciables graphiquement, les boutons sont insérés dans un sous-menu qui s’ouvre au clic sur un bouton (icône ``#dots``)
- On peut choisir d’afficher 1 à 3 boutons pour les actions principales puis un sous-menu comprenant les actions secondaires. 
- Les boutons ont la classe ``btn icon only`` sauf dans le cas d’une action destructive où on ajoute la classe ``negative``

Pour éviter que les cases d’actions s’élargissent trop quand l’écran est large, ajouter sur le ``<td class="col_actions">`` une classe correspondant au nombre de boutons présents dans la case :

- ``width_one`` pour 1 bouton ( ``<td class="col_actions width_one">``)
- ``width_two`` pour 2 boutons ( ``<td class="col_actions width_two">``)
- ``width_three`` pour 3 boutons ( ``<td class="col_actions width_three">``)
- ``width_four`` pour 4 boutons ( ``<td class="col_actions width_four">``)


Réordonner des lignes de tableaux
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- Quand on peut réordonner des lignes de tableau, on utilise des boutons avec flèches vers le haut et vers le bas.
- Ces boutons sont placés dans une colonne séparée en fin de ligne (après les actions). Les cases de cette colonne ont la classe ``<td class="col_actions row_ordering sort">``.
- Le bouton pour déplacer vers le bas : ``<button class="btn icon only down">…``
- Le bouton pour déplacer vers le haut : ``<button class="btn icon only up">…``


Navigation au clic sur les lignes de tableaux
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Quand une ligne de tableau concerne un document ou que l’action principale est de consulter une information, on permet la navigation en cliquant sur la ligne ou sur les cases de la ligne.

On ajoute ``onclick="navigation" title="Cliquer pour voir …" aria-label="Cliquer pour voir …"`` (ou … est remplacé par le nom du document ou l’information qui sera affiché·e)

- Quand il n’y a pas de case d’action ou de lien ``<a>`` vers une autre navigation dans la ligne on ajoute ce code sur le ``<tr>``
- Quand il y a une case d’action ou un ``<td>`` contenant un lien ``<a>`` qui provoque une autre action que celle qu’on va ajouter, on ajoute ce code sur les autres ``<td>``.


Indentation des lignes de tableaux
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Pour hiérarchiser des lignes de tableau en les indentant, on positionne en début de ligne un ou plusieurs ``<td class="level_spacer"></td>``.


.. code-block:: html

	<tr>
		<td colspan="3"><h3>Information de niveau 0</h3></td>
	</tr>
	<tr>
		<td class="level_spacer"></td>
		<td colspan="2">Information de niveau 1</td>
	</tr>
	<tr>
		<td class="level_spacer"></td>
		<td class="level_spacer"></td>
		<td colspan="2">Information de niveau 2</td>
	</tr>


Filtres de recherche
--------------------

Dans les formulaires de filtres de recherche, on groupe les divers champs suivant le type de données sur lequel ils portent : 

- Informations temporelles (année, début, fin, durée…)
- Types, numéros et statuts de document
- Informations financières (montant, mode de paiement…)
- Informations client (client, statut client…)
- Auteur (enseigne, accompagnateur…)

Quand ces filtres portent sur une liste, on uniformise autant que possible l’ordre des filtres et des colonnes de la liste pour que le lien entre les deux soit facile à établir.


Pagination et affichage des listes
----------------------------------

Un widget d’information et de pagination permet d’informer l’utilisateur·trice sur le nombre de résultats (``résultats``) et le nombre total de pages (``pages``), de choisir combien d’éléments afficher par page (``afficheur``), et de naviguer de page en page (``pagination``).

- Quand une liste ne comprend pas d’éléments, on affiche la partie ``résultats`` seule au-dessus de la liste.
- Quand une liste comprend des éléments, on affiche ``résultats`` et ``afficheur`` au-dessus et au-dessous de la liste. 
- Si la liste est paginée, on ajoute ``pages`` et ``pagination`` après ``résultats``.


Pop-ins
-------

Les pop-ins sont insérés à l’intérieur d’une balise ``<section class="modal_view">``  qui positionne la pop-in dans l’écran. C’est elle qu’on affiche ou cache avec par exemple ``style="display: none;"``.

Les quatre classes ``size_small``,  ``size_middle``,  ``size_large``  et  ``size_full`` permettent d’obtenir des pop-ins de plus en plus grandes. (``size_full`` prend tout l’écran).

La ``<div role="dialog">`` réfère le titre de la pop-in pour l’accessiblité.

La ``<div class="modal_layout">`` permet la mise en forme du contenu.

Le ``<header>`` contient le bouton de fermeture et le titre de la pop-in (``<h2>`` avec un id référencé par la ``<div role="dialog">``).
            
Pour afficher des onglets, on peut ajouter la zone de navigation  ``<nav>`` optionnelle.
            
Le contenu est placé dans un ``<main>``.

Les boutons d’actions sur l’ensemble de la pop-in sont situés dans le ``<footer>``.


.. code-block:: html

	<section id="customer_add_form" class="modal_view size_middle" style="display: none;">
		<div role="dialog" id="" aria-modal="true" aria-labelledby="customer-forms_title">
			<div class="modal_layout">
				<header>
					<button class="icon only unstyled close" title="Fermer cette fenêtre" aria-label="Fermer cette fenêtre" onclick="toggleModal('customer_add_form'); return false;">
						<svg><use href="${request.static_url('endi:static/icons/endi.svg')}#times"></use></svg>
					</button>
					<h2 id="customer-forms_title">Ajouter un client</h2>
				</header>
				<nav>
					<ul class="nav nav-tabs modal-tabs" role="tablist" aria-label="Type de client">
						<li role="presentation" class="active">
							<a href="#companyForm" aria-controls="companyForm" role="tab" aria-selected="true" id="company">Personne morale</a>
						</li>
						<li role="presentation">
							<a href="#individualForm" aria-controls="individualForm" role="tab" aria-selected="false" id="individual" tabindex="-1">Personne physique</a>
						</li>
					</ul>
				</nav>
				<main>
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane active row" id="companyForm" aria-labelledby="company" tabindex="0">
							<h3>Titre</h3>
							Contenu
						</div>
						<div role="tabpanel" class="tab-pane row" id="individualForm" aria-labelledby="individual" tabindex="0" hidden>
							<h3>Titre</h3>
							Contenu
						</div>
					</div>
				</main>
				<footer>
					<button class='btn btn-primary' {{#if has_file_warning}}disabled aria-disabled='true'{{/if}} type='submit' name='submit' value='{{status}}'>{{label}}</button>
					<button class='btn' data-dismiss='modal'>Annuler</button>
				</footer>
			</div>
		</div>
	</section>


Boutons
-------

Les boutons sont soit des ``<button class="btn">`` soit des ``<a class="btn">``.

Les classes disponibles pour les enrichir sont : 

- ``btn-primary`` pour les boutons majeurs (un seul par groupe de boutons). Quand on a un groupe de boutons, le bouton majeur est toujours à gauche du ou des bouton·s mineur·s.
- ``icon`` pour les boutons qui comprennent une icône.
- ``icon only`` pour les boutons qui ne comprennent qu’une icône. Dans ce cas il faut préciser le sens du bouton à la fois dans son ``title`` et dans son ``aria-label``.
- ``disabled`` pour un bouton inactif
- ``negative`` pour un bouton dont l’action correspond à une suppression, une destruction d’information. Les boutons **« Annuler »** ne prennent la classe ``negative`` que si ils correspondent à une destruction d’informations (pas la fermeture d’une popin ou d’un layer).
- ``active`` pour le bouton sélectionné
- ``unstyled`` pour un bouton non délimité par une bordure. Attention à ce que le bouton soit bien compris comme une action (icône et/ou texte explicite, précision de l’action dans ``title`` et ``aria-label``).
- ``dropdown-toggle`` pour un bouton qui déclenche l’ouverture d’un sous-menu



Barre de boutons
----------------

Les boutons d’action indépendants ou portant sur tout le contenu de la page sont affichés dans la barre de boutons présente en haut d’écran.

- Les boutons peuvent être séparés en groupes (dans des ``<div role="group">``)
- Les actions principales sont affichées en haut à gauche.
- Les actions secondaires sont affichées en haut à droite. Quand elles comportent une action destructive, celle-ci est en fin de ligne à droite.


Formulaires
-----------

Les formulaires complexes sont structurés en blocs (``<fieldset>``).
Ces blocs sont séparés graphiquement avec les classes ``separate_block`` (ombre et espacement) et ``border_left_block`` (bordure colorée à gauche du bloc).

Quand un formulaire ne contient qu’un bloc, ne pas le séparer graphiquement (ne pas utiliser ``separate_block`` et ``border_left_block``).

Quand on veut présenter une donnée non modifiable dans un formulaire, utiliser la structure 


.. code-block:: html

	<span class="label">Nom de la donnée</span>
	<span class="data">Contenu de la donnée</span>

à la place de ``<label><input>``.


Icônes
------

Les icones sont insérées sous la forme d’un fragment SVG. On insère le fichier svg suivi de l’identifiant du fragment à utiliser.

``<svg><use href="/static/icons/endi.svg#identifiant"></use></svg>`` 
ou 
``<svg><use href="${request.static_url('endi:static/icons/endi.svg')}#identifiant"></use></svg>``

On privilégie l’utilisation du helper pour insérer les icônes :

``${api.icon('identifiant')}``


Une liste des icônes utilisées dans enDI et des identifiants correspondants est disponible ci-dessous :

.. raw:: html

    <iframe src="_static/icones.html" height="500px" width="100%"></iframe>
    <p>
    	<a href="_static/icones.html">Lien vers la liste des icônes</a>
    </p>


Icônes de téléchargement
~~~~~~~~~~~~~~~~~~~~~~~~

Pour les boutons permettant de télécharger un fichier :

- Quand le format du fichier est inconnu ou fourni par l’utilisateur·trice, utiliser l’identifiant #download

- Quand le format est connu, utiliser l’identifiant correspondant (#file-pdf, #file-excel, #file-csv,…)


Statuts
-------

Liste des styles et fragments d’icônes correspondant aux divers statuts des documents et actions.
Ne pas oublier d’ajouter ``title`` et ``aria-label`` pour préciser le statut au survol de l’icône et pour les lecteurs d’écran.

+--------------------------+----------------+----------------------------+-----------------------+
| Statut                   | Statut en base | Classe du span             | Fragment svg          |
+==========================+================+============================+=======================+
| **Tous**                                                                                       |
+--------------------------+----------------+----------------------------+-----------------------+
| Brouillon modifié        | draft          | icon status draft          | #pen                  |
+--------------------------+----------------+----------------------------+-----------------------+
| Validation demandée      | caution        | icon status caution        | #clock                |
+--------------------------+----------------+----------------------------+-----------------------+
| Validé(e)                | valid          | icon status valid          | #check-circle         |
+--------------------------+----------------+----------------------------+-----------------------+
| Invalidé(e)              | invalid        | icon status invalid        | #times-circle         |
+--------------------------+----------------+----------------------------+-----------------------+
| Non modifiable           | closed         | icon status closed         | #lock                 |
+--------------------------+----------------+----------------------------+-----------------------+
| **Devis**                                                                                      |
+--------------------------+----------------+----------------------------+-----------------------+
| Annulé, sans suite       | closed         | icon status closed         | #times                |
+--------------------------+----------------+----------------------------+-----------------------+
| Envoyé                   | valid          | icon status valid          | #envelope             |
+--------------------------+----------------+----------------------------+-----------------------+
| Signé                    | valid          | icon status valid          | #check                |
+--------------------------+----------------+----------------------------+-----------------------+
| Factures générées        | valid          | icon status valid          | #euro-sign            |
+--------------------------+----------------+----------------------------+-----------------------+
| **Factures**                                                                                   |
+--------------------------+----------------+----------------------------+-----------------------+
| Payée partiellement      | partial_unpaid | icon status partial_unpaid | #euro-sign            |
+--------------------------+----------------+----------------------------+-----------------------+
| Soldée                   | valid          | icon status valid          | #euro-sign            |
+--------------------------+----------------+----------------------------+-----------------------+
| Non payée depuis         | caution        | icon status caution        | #euro-slash           |
| moins de 45 jours        |                |                            |                       |
+--------------------------+----------------+----------------------------+-----------------------+
| Non payée depuis         | invalid        | icon status invalid        | #euro-slash           |
| plus de 45 jours         |                |                            |                       |
+--------------------------+----------------+----------------------------+-----------------------+
| **Dépenses**                                                                                   |
+--------------------------+----------------+----------------------------+-----------------------+
| En attende de validation | caution        | icon status caution        | #clock                |
+--------------------------+----------------+----------------------------+-----------------------+
| Payée partiellement      | partial_unpaid | icon status partial_unpaid | #euro-sign            |
+--------------------------+----------------+----------------------------+-----------------------+
| Payée intégralement      | valid          | icon status valid          | #euro-sign            |
+--------------------------+----------------+----------------------------+-----------------------+
| Justificatifs reçus      | valid          | icon status valid          | #file-check           |
+--------------------------+----------------+----------------------------+-----------------------+
| **Affaires**                                                                                   |
+--------------------------+----------------+----------------------------+-----------------------+
| En cours                 | valid          | icon status valid          | #list-alt             |
+--------------------------+----------------+----------------------------+-----------------------+
| Clôturée                 | closed         | icon status closed         | #list-check           |
+--------------------------+----------------+----------------------------+-----------------------+
| **Jobs**                                                                                       |
+--------------------------+----------------+----------------------------+-----------------------+
| Planifié                 | planned        | icon status planned        | #clock                |
+--------------------------+----------------+----------------------------+-----------------------+
| Terminé                  | completed      | icon status completed      | #check                |
+--------------------------+----------------+----------------------------+-----------------------+
| Échec                    | failed         | icon status failed         | #exclamation-triangle |
+--------------------------+----------------+----------------------------+-----------------------+
| **Écritures**                                                                                  |
+--------------------------+----------------+----------------------------+-----------------------+
| Associée                 | valid          | icon status valid          | #link                 |
+--------------------------+----------------+----------------------------+-----------------------+
| Non associée             | caution        | icon status caution        | #exclamation-triangle |
+--------------------------+----------------+----------------------------+-----------------------+
| **Commandes**                                                                                  |
+--------------------------+----------------+----------------------------+-----------------------+
| Annulée                  | closed         | icon status closed         | #times                |
+--------------------------+----------------+----------------------------+-----------------------+


Étiquettes
----------

Des étiquettes permettent d’ajouter des informations de classification dans les listes, blocs d’informations, etc… La structure d’une étiquette est du type ``<span class="icon tag neutral"><svg><use href="/static/icons/endi.svg#identifiant"></use></svg> Libellé</span>``. 

Des classes sont disponibles pour signifier le type d’information donnée par l’étiquette.

- ``<span class="icon tag neutral">`` : neutre
- ``<span class="icon tag positive">`` : positive
- ``<span class="icon tag negative">`` : négative
- ``<span class="icon tag caution">`` : avertissement


Postits
-------

Les commentaires et changements de statuts sont affichés dans des postits. Ceux-ci peuvent afficher une icône, un statut, du texte, des étiquettes. Ils sont horodatés en bas avec le nom de l’auteur·autrice du postit ou de l’action suivi de la date.

Ils sont regroupés dans un bloc ``<div class="status_history hidden-print">`` qui comprend un titre ``<h4>`` et une liste ``<ul>`` dont chaque ``<li>`` contient un postit.

La structure du postit est contenue dans un ``<blockquote>``.


.. code-block:: html

	<blockquote class="caution">
		<span class="icon status caution" role="presentation">
			<svg><use href="../static/icons/endi.svg#clock"></use></svg>
		</span>
		<p class="status">
			Validation demandée
		</p>
		<p>
			Voilà c’est complet normalement.
		</p>
		<footer>FERRAND Grégoire le 18/05/2021</footer>
	</blockquote>

Des classes sont disponibles pour signifier le type d’information donnée par le postit.

- ``default`` : pas de type particulier (quand une icône est présente dans le bloc)
- ``draft``, ``closed``, ``neutral``, ``planned`` : neutre
- ``success``, ``valid``, ``completed``, ``partial_unpaid`` : positive
- ``error``, ``invalid``, ``cancelled``, ``failed``, ``unpaid`` : négative
- ``caution``, ``wait``, ``danger`` : avertissement

Les icônes de statuts des postits sont identiques à celles des :ref:`Statuts`.


Accessibilité
-------------

Title et aria-label
~~~~~~~~~~~~~~~~~~~

Sur un ``<button>`` sans contenu textuel (``class="icon only"``), les deux servent : le title comme info-bulle pour les visuels, le aria-label pour les lecteurs d’écran (qui ne lisent souvent pas le title).

Sur un ``<button>`` avec contenu textuel, ils ne servent que s’ils précisent le bouton (par exemple Bouton **« XLS »** avec title et aria-label « Exporter cette liste au format Excel (.xls) » ).
